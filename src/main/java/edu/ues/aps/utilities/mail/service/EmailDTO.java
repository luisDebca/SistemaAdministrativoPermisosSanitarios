package edu.ues.aps.utilities.mail.service;

import java.util.Arrays;

public class EmailDTO implements AbstractEmailDTO{

    private String[] to;
    private String subject;
    private String text;
    private String[] bcc;
    private String[] cc;
    private String[] pathToAttachment;

    public EmailDTO(String[] to, String subject, String text) {
        this.to = to;
        this.subject = subject;
        this.text = text;
    }

    public EmailDTO(String[] to, String subject, String text, String[] cc) {
        this.to = to;
        this.subject = subject;
        this.text = text;
        this.cc = cc;
    }

    public EmailDTO(String[] to, String subject, String text, String[] bcc, String[] cc, String[] pathToAttachment) {
        this.to = to;
        this.subject = subject;
        this.text = text;
        this.bcc = bcc;
        this.cc = cc;
        this.pathToAttachment = pathToAttachment;
    }

    @Override
    public String[] getTo() {
        return to;
    }

    @Override
    public void setTo(String[] to) {
        this.to = to;
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public String getText() {
        return text;
    }

    @Override
    public void setText(String text) {
        this.text = text;
    }

    @Override
    public String[] getBcc() {
        return bcc;
    }

    public void setBcc(String[] bcc) {
        this.bcc = bcc;
    }

    @Override
    public String[] getCc() {
        return cc;
    }

    public void setCc(String[] cc) {
        this.cc = cc;
    }

    @Override
    public String[] getPathToAttachment() {
        return pathToAttachment;
    }

    public void setPathToAttachment(String[] pathToAttachment) {
        this.pathToAttachment = pathToAttachment;
    }

    @Override
    public String toString() {
        return "EmailDTO{" +
                "to=" + Arrays.toString(to) +
                ", subject='" + subject + '\'' +
                ", text='" + text + '\'' +
                ", bcc=" + Arrays.toString(bcc) +
                ", cc=" + Arrays.toString(cc) +
                ", pathToAttachment=" + Arrays.toString(pathToAttachment) +
                '}';
    }
}
