package edu.ues.aps.utilities.mail.service;

public interface AbstractEmailDTO {

    String[] getTo();

    void setTo(String[] to);

    String getSubject();

    void setSubject(String subject);

    String getText();

    void setText(String text);

    String[] getBcc();

    void setBcc(String[] bcc);

    String[] getCc();

    void setCc(String[] cc);

    String[] getPathToAttachment();

    void setPathToAttachment(String[] pathToAttachment);
}
