package edu.ues.aps.utilities.mail.service;

import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.io.FileSystemResource;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Component;
import org.springframework.web.multipart.MultipartFile;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Component
public class EmailSenderService implements EmailService{

    private static final Logger logger = LogManager.getLogger(EmailSenderService.class);

    private final JavaMailSender emailSender;
    private MimeMessage mimeMessage;
    private MimeMessageHelper helper;

    private List<File> tempFiles = new ArrayList<>();

    public EmailSenderService(JavaMailSender emailSender) {
        this.emailSender = emailSender;
    }

    @Async
    @Override
    public void sendSimpleMessage(AbstractEmailDTO dto) {
        try {
            logger.info("MAIL::Sending: sending new simple email with params {}",dto.toString());
            buildMimeMessage(dto);
            emailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Async
    @Override
    public void sendMessageWithAttachment(AbstractEmailDTO dto) {
        try {
            logger.info("MAIL::Sending: sending new mime email with system stored files and params {}",dto.toString());
            buildMimeMessage(dto);
            addSystemStoredFilesToMessage(dto.getPathToAttachment());
            emailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void sendMessageWithAttachment(AbstractEmailDTO dto, MultipartFile[] stream) {
        try {
            logger.info("MAIL::Sending: sending mime simple email with uploaded files and params {}",dto.toString());
            uploadAttachmentFiles(stream);
            buildMimeMessage(dto);
            addUploadedFilesToMessage();
            emailSender.send(mimeMessage);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    private void buildMimeMessage(AbstractEmailDTO dto) throws MessagingException {
        mimeMessage = emailSender.createMimeMessage();
        helper = new MimeMessageHelper(mimeMessage,true);
        helper.setTo(dto.getTo());
        helper.setSubject(dto.getSubject());
        helper.setText(dto.getText(),true);
        helper.setCc(dto.getCc());
        if(dto.getBcc() != null)
            helper.setBcc(dto.getBcc());
    }

    private void addSystemStoredFilesToMessage(String[] pathToFiles) throws MessagingException {
        for (String pathToFile : pathToFiles) {
            FileSystemResource file = new FileSystemResource(new File(pathToFile));
            helper.addAttachment(file.getFilename(), file);
        }
    }

    private void addUploadedFilesToMessage() throws MessagingException {
        for (File file : tempFiles) {
           helper.addAttachment(file.getName(),file);
        }
        emptyTempFiles();
    }

    private void uploadAttachmentFiles(MultipartFile[] stream){
        try {
            for(MultipartFile multipartFile: stream){
                File temp = File.createTempFile(FilenameUtils.getBaseName(multipartFile.getOriginalFilename()),
                        "." + FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
                multipartFile.transferTo(temp);
                tempFiles.add(temp);
            }
        } catch (IOException e) {
            logger.error("ERROR: File upload error");
            e.printStackTrace();
        }
    }

    private void emptyTempFiles(){
        tempFiles.clear();
    }

}
