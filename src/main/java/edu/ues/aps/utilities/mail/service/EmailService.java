package edu.ues.aps.utilities.mail.service;

import org.springframework.web.multipart.MultipartFile;

public interface EmailService {

    void sendSimpleMessage(AbstractEmailDTO dto);

    void sendMessageWithAttachment(AbstractEmailDTO dto);

    void sendMessageWithAttachment(AbstractEmailDTO dto, MultipartFile[] stream);
}
