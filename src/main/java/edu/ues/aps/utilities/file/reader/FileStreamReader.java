package edu.ues.aps.utilities.file.reader;

import edu.ues.aps.utilities.file.directory.ApplicationDefaultDirectoryInformationProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.text.SimpleDateFormat;
import java.util.NoSuchElementException;
import java.util.Scanner;

@Component
public class FileStreamReader implements FileReader {

    private static final Logger logger = LogManager.getLogger(FileStreamReader.class);

    private ApplicationDefaultDirectoryInformationProvider directoryInformationProvider;

    public FileStreamReader(ApplicationDefaultDirectoryInformationProvider directoryInformationProvider) {
        this.directoryInformationProvider = directoryInformationProvider;
    }

    @Override
    public boolean isFileExist(String file_name) {
        return new File(directoryInformationProvider.getSourceDirectoryAbsolutePath() +
                directoryInformationProvider.getSystemFileDelimiter() + file_name).exists();
    }

    @Override
    public String readFileContent(String file_name) {
        String file_content = "";
        if (isFileExist(file_name)) {
            File file = new File(directoryInformationProvider.getSourceDirectoryAbsolutePath() +
                    directoryInformationProvider.getSystemFileDelimiter() + file_name);
            logger.info("File info: name: {}, size (bytes): {}, path: {}, last modification: {}", file.getName(), file.length(), file.getAbsolutePath(), new SimpleDateFormat("MMMM dd, yyyy hh:mm a").format(file.lastModified()));
            file_content = readFile(file);
        }
        return file_content;
    }

    private String readFile(File file) {
        String result = "";
        try {
            FileInputStream inputStream = new FileInputStream(file);
            result = streamToString(inputStream);
            inputStream.close();
        } catch (IOException e) {
            logger.error("File not found or FileInputStream not closed");
            e.printStackTrace();
        }
        return result;
    }

    private String streamToString(InputStream stream) {
        try (Scanner scanner = new Scanner(stream).useDelimiter("\\A")) {
            return scanner.next();
        }catch (NoSuchElementException e){
            return "";
        }
    }
}
