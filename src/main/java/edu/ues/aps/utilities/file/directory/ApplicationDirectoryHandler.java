package edu.ues.aps.utilities.file.directory;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class ApplicationDirectoryHandler implements ApplicationDefaultDirectoryInformationProvider, InitializingBean {

    private final String OPERATIVE_SYSTEM_WINDOWS = "Windows";
    private final String OPERATIVE_SYSTEM_LINUX = "Linux";
    private String SOURCE_DIRECTORY;
    private String UPLOAD_DIRECTORY;
    private boolean is_source_directory_exist = false;
    private boolean is_upload_directory_exist = false;
    private File sources_directory;
    private File uploads_directory;

    private static final Logger logger = LogManager.getLogger(ApplicationDirectoryHandler.class);

    @Override
    public void afterPropertiesSet() throws Exception {
        setSourceDirectoryForOS();
        checkDirectoryIfExist();
        createDirectoryIfNotExist();
    }

    private void setSourceDirectoryForOS() {
        logger.info("Checking operating system...");
        String os = System.getProperty("os.name", "");
        if (os.equalsIgnoreCase(OPERATIVE_SYSTEM_LINUX)) {
            SOURCE_DIRECTORY = System.getProperty("user.home") + "/aps_server/sources";
            UPLOAD_DIRECTORY = System.getProperty("user.home") + "/aps_server/upload";
        } else if (os.startsWith(OPERATIVE_SYSTEM_WINDOWS) ||
                os.toLowerCase().startsWith(OPERATIVE_SYSTEM_WINDOWS.toLowerCase()) || os.startsWith("win")) {
            SOURCE_DIRECTORY = "C:\\aps_server\\sources";
            UPLOAD_DIRECTORY = "C:\\aps_server\\upload";
        } else {
            throw new OperatingSystemNotSupportedException("Operating system [ " + os + "] not supported");
        }
    }

    private void checkDirectoryIfExist() {
        logger.info("Checking if sources_directory exist...");
        is_source_directory_exist = new File(SOURCE_DIRECTORY).exists();
        is_upload_directory_exist = new File(UPLOAD_DIRECTORY).exists();
    }

    private void createDirectoryIfNotExist() {
        sources_directory = new File(SOURCE_DIRECTORY);
        uploads_directory = new File(UPLOAD_DIRECTORY);
        logger.info("Sources directory exist :{}", is_source_directory_exist);
        logger.info("Uploads directory exist :{}", is_source_directory_exist);
        if (!is_source_directory_exist) {
            logger.info("Creating sources_directory...");
            is_source_directory_exist = sources_directory.mkdirs();
            logger.info("Directories created successfully: {}", is_source_directory_exist);
        }
        if (!is_upload_directory_exist) {
            logger.info("Creating uploads_directory...");
            is_upload_directory_exist = uploads_directory.mkdirs();
            logger.info("Directories created successfully: {}", is_source_directory_exist);
        }
    }

    @Override
    public boolean isSourceDirectoryExist() {
        return is_source_directory_exist;
    }

    @Override
    public String getSourceDirectoryAbsolutePath() {
        return sources_directory.getAbsolutePath();
    }

    @Override
    public String getSystemFileDelimiter() {
        String os = System.getProperty("os.name", "");
        if (os.equalsIgnoreCase(OPERATIVE_SYSTEM_LINUX)) {
            return "/";
        } else if (os.startsWith(OPERATIVE_SYSTEM_WINDOWS) ||
                os.toLowerCase().startsWith(OPERATIVE_SYSTEM_WINDOWS.toLowerCase()) ||
                os.startsWith("win")) {
            return "\\";
        } else {
            throw new OperatingSystemNotSupportedException("Operating system not supported");
        }
    }

    @Override
    public File getSources_directory() {
        return sources_directory;
    }

    @Override
    public boolean isUploadDirectoryExist() {
        return is_upload_directory_exist;
    }

    @Override
    public String getUploadDirectoryAbsolutePath() {
        return uploads_directory.getAbsolutePath();
    }

    @Override
    public File getUploads_directory() {
        return uploads_directory;
    }
}
