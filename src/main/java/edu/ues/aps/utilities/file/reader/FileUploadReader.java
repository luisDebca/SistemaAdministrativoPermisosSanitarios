package edu.ues.aps.utilities.file.reader;

import edu.ues.aps.utilities.file.directory.ApplicationDefaultDirectoryInformationProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.File;

@Component
public class FileUploadReader implements UploadReader {

    private static final Logger logger = LogManager.getLogger(FileUploadReader.class);

    private final ApplicationDefaultDirectoryInformationProvider directoryInformationProvider;

    public FileUploadReader(ApplicationDefaultDirectoryInformationProvider directoryInformationProvider) {
        this.directoryInformationProvider = directoryInformationProvider;
    }

    @Override
    public boolean isFileExist(String file_name) {
        return new File(directoryInformationProvider.getUploadDirectoryAbsolutePath() +
                directoryInformationProvider.getSystemFileDelimiter() + file_name).exists();
    }

    @Override
    public File getFile(String file_name) {
        logger.info("Reading file with name {}", file_name);
        File file = new File(directoryInformationProvider.getUploadDirectoryAbsolutePath() +
                directoryInformationProvider.getSystemFileDelimiter() + file_name);
        logger.info("File information\nName: {}\nSize (bites): {}\nPath: {}", file.getName(), file.length(), file.getAbsolutePath());
        return file;
    }
}
