package edu.ues.aps.utilities.file.writer;

public interface FileWriter {

    boolean isFileExist(String file_name);

    String write(String content);

    String write(String file_name, String content);

    String writeFlatName(String file_name, String content);

    void delete(String file_name);

}
