package edu.ues.aps.utilities.file.directory;


public class OperatingSystemNotSupportedException extends RuntimeException{

    public OperatingSystemNotSupportedException() {
    }

    public OperatingSystemNotSupportedException(String message) {
        super(message);
    }
}
