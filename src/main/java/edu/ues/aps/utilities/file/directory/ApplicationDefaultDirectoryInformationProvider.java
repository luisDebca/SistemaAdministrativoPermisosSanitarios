package edu.ues.aps.utilities.file.directory;

import java.io.File;

public interface ApplicationDefaultDirectoryInformationProvider {

    boolean isSourceDirectoryExist();

    String getSourceDirectoryAbsolutePath();

    String getSystemFileDelimiter();

    File getSources_directory();

    boolean isUploadDirectoryExist();

    String getUploadDirectoryAbsolutePath();

    File getUploads_directory();
}
