package edu.ues.aps.utilities.file.writer;

import edu.ues.aps.utilities.file.directory.ApplicationDefaultDirectoryInformationProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class FileStreamWriter implements FileWriter {

    private static final Logger logger = LogManager.getLogger(FileStreamWriter.class);

    private ApplicationDefaultDirectoryInformationProvider directoryInformationProvider;

    public FileStreamWriter(ApplicationDefaultDirectoryInformationProvider directoryInformationProvider) {
        this.directoryInformationProvider = directoryInformationProvider;
    }

    @Override
    public boolean isFileExist(String file_name) {
        File file = new File(directoryInformationProvider.getSourceDirectoryAbsolutePath() +
                directoryInformationProvider.getSystemFileDelimiter() + file_name);
        return file.exists();
    }

    @Override
    public String write(String content) {
        File new_file = createNewFileIfNotExist(directoryInformationProvider.getSourceDirectoryAbsolutePath() + buildFileNameWithDateRegister("output"));
        writeContentIntoFile(content, new_file);
        return new_file.getName();
    }

    @Override
    public String write(String file_name, String content) {
        File new_file = createNewFileIfNotExist(directoryInformationProvider.getSourceDirectoryAbsolutePath() + buildFileNameWithDateRegister(file_name));
        writeContentIntoFile(content, new_file);
        return new_file.getName();
    }

    @Override
    public String writeFlatName(String file_name, String content) {
        File new_file = createNewFileIfNotExist(directoryInformationProvider.getSourceDirectoryAbsolutePath() + buildFileName(file_name));
        writeContentIntoFile(content, new_file);
        return new_file.getName();
    }

    @Override
    public void delete(String file_name) {
        File file = new File(directoryInformationProvider.getSourceDirectoryAbsolutePath() +
                directoryInformationProvider.getSystemFileDelimiter() + file_name);
        boolean is_file_deleted = false;
        if (file.exists()) is_file_deleted = file.delete();
        logger.info("File with name {} was deleted: {}", file_name, is_file_deleted);
    }

    private File createNewFileIfNotExist(String file_name) {
        File new_file = new File(file_name);
        logger.info("File with name: {} exist: {}", new_file.getName(), new_file.exists());
        try {
            if (!new_file.exists()) {
                boolean isFileCreatedSuccessfully = new_file.createNewFile();
                logger.info("File with name {} created successfully: {}", new_file.getName(), isFileCreatedSuccessfully);
                logger.info("File info: \nName: {}, \nSize (bytes): {}, \nPath: {},", new_file.getName(), new_file.length(), new_file.getAbsolutePath());
            }
        } catch (IOException e) {
            logger.error("Error creating new File");
            e.printStackTrace();
        }
        return new_file;
    }

    private void writeContentIntoFile(String content, File file) {
        try {
            FileOutputStream outputStream = new FileOutputStream(file);
            outputStream.write(content.getBytes());
            outputStream.close();
        } catch (FileNotFoundException e) {
            logger.error("File with name {} is not found.", file.getName());
            e.printStackTrace();
        } catch (IOException e) {
            logger.error("Error found trying write file or close output stream.");
            e.printStackTrace();
        }
    }

    private String buildFileName(String file_name) {
        String build_name = directoryInformationProvider.getSystemFileDelimiter();
        build_name += getSimpleFileName(file_name);
        build_name += ".aps";
        return build_name;
    }

    private String buildFileNameWithDateRegister(String file_name) {
        String build_name = directoryInformationProvider.getSystemFileDelimiter();
        build_name += getSimpleFileName(file_name);
        build_name += getDateRegister();
        build_name += ".aps";
        return build_name;
    }

    private String getSimpleFileName(String file_name) {
        if (file_name.contains(".")) return file_name.split("\\.")[0];
        else return file_name;
    }

    private String getDateRegister() {
        LocalDateTime now = LocalDateTime.now();
        return "_" +
                now.getYear() +
                now.getMonthValue() +
                now.getDayOfMonth() +
                now.getHour() +
                now.getMinute() +
                now.getSecond();
    }
}
