package edu.ues.aps.utilities.file.writer;

import edu.ues.aps.utilities.file.directory.ApplicationDefaultDirectoryInformationProvider;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

@Component
public class FileUploadWriter implements UploadWriter {

    private static final Logger logger = LogManager.getLogger(FileUploadWriter.class);

    private final ApplicationDefaultDirectoryInformationProvider directoryInformationProvider;

    public FileUploadWriter(ApplicationDefaultDirectoryInformationProvider directoryInformationProvider) {
        this.directoryInformationProvider = directoryInformationProvider;
    }

    @Override
    public boolean isFileExist(String file_name) {
        return new File(directoryInformationProvider.getUploadDirectoryAbsolutePath() +
                directoryInformationProvider.getSystemFileDelimiter() + file_name).exists();
    }

    @Override
    public String upload(File file) {
        File new_file = new File(directoryInformationProvider.getUploadDirectoryAbsolutePath() +
                directoryInformationProvider.getSystemFileDelimiter() + FilenameUtils.getName(file.getName()));
        logger.info("Upload file:\nName: {}\nSize (bits): {}\nPath: {}", new_file.getName(), new_file.length(), new_file.getAbsolutePath());
        return new_file.getName();
    }

    @Override
    public File upload(String file_name) {
        File new_file = new File(directoryInformationProvider.getUploadDirectoryAbsolutePath() +
                directoryInformationProvider.getSystemFileDelimiter() + buildFileName(file_name));
        logger.info("Upload file:\nName: {}\nSize (bits): {}\nPath: {}", new_file.getName(), new_file.length(), new_file.getAbsolutePath());
        return new_file;
    }

    @Override
    public void rename(File file, String new_name) {
        try {
            File renamed = new File(directoryInformationProvider.getUploadDirectoryAbsolutePath() +
                    directoryInformationProvider.getSystemFileDelimiter() + new_name);
            if (renamed.exists()) {
                throw new IOException("File exists");
            }
            boolean success = file.renameTo(renamed);
            if (!success) {
                logger.error("File was not successfully rename: {} -> {}", file.getName(), renamed.getName());
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(String file_name) {
        if (isFileExist(file_name)) {
            boolean delete = new File(directoryInformationProvider.getUploadDirectoryAbsolutePath() +
                    directoryInformationProvider.getSystemFileDelimiter() + file_name).delete();
            logger.info("File with name {} is deleted: {}", file_name, delete);
        }
    }

    private String buildFileName(String file_name) {
        LocalDateTime now = LocalDateTime.now();
        if (!FilenameUtils.getExtension(file_name).isEmpty()) {
            String original_name = FilenameUtils.getBaseName(file_name);
            String file_type = FilenameUtils.getExtension(file_name);
            return original_name + "$$" +
                    now.getYear() +
                    now.getMonthValue() +
                    now.getDayOfMonth() +
                    now.getHour() +
                    now.getMinute() +
                    now.getSecond() +
                    "." + file_type;
        } else {
            logger.error("File name '{}' is not valid", file_name);
            return file_name;
        }
    }
}
