package edu.ues.aps.utilities.file.reader;

public interface FileReader {

    boolean isFileExist(String file_name);

    String readFileContent(String file_name);
}
