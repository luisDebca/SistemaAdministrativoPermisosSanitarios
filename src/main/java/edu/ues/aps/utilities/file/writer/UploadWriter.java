package edu.ues.aps.utilities.file.writer;

import java.io.File;

public interface UploadWriter {

    boolean isFileExist(String file_name);

    String upload(File file);

    File upload(String file_name);

    void rename(File file, String new_name);

    void delete(String file_name);
}
