package edu.ues.aps.utilities.file.reader;

import java.io.File;

public interface UploadReader {

    boolean isFileExist(String file_name);

    File getFile(String file_name);
}
