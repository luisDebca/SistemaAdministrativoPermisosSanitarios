package edu.ues.aps.utilities.http.response;

public enum ResponseStatusType {

    SUCCESS,
    WARNING,
    ERROR,
    ACCESS_DENIED,
    UNDEFINED
}
