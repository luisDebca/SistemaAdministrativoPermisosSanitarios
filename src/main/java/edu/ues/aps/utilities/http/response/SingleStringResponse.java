package edu.ues.aps.utilities.http.response;

public class SingleStringResponse {

    private ResponseStatusType response_status;
    private String response_message;

    public SingleStringResponse(ResponseStatusType response_status, String response_message) {
        this.response_status = response_status;
        this.response_message = response_message;
    }

    public SingleStringResponse(String response_message) {
        this.response_message = response_message;
        this.response_status = ResponseStatusType.UNDEFINED;
    }

    public SingleStringResponse() {
        this.response_message = "";
        this.response_status = ResponseStatusType.UNDEFINED;
    }

    public String getResponse_status() {
        if(response_status != null){
            return response_status.name();
        }
        return ResponseStatusType.UNDEFINED.name();
    }

    public String getResponse_message() {
        return response_message;
    }

    public SingleStringResponse asSuccessResponse() {
        this.response_status = ResponseStatusType.SUCCESS;
        return this;
    }

    public SingleStringResponse asWarningResponse() {
        this.response_status = ResponseStatusType.WARNING;
        return this;
    }

    public SingleStringResponse asErrorResponse() {
        this.response_status = ResponseStatusType.ERROR;
        return this;
    }

    public SingleStringResponse asDeniedResponse() {
        this.response_status = ResponseStatusType.ACCESS_DENIED;
        return this;
    }
}
