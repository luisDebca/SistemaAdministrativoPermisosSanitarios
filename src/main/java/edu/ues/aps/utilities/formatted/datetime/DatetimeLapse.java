package edu.ues.aps.utilities.formatted.datetime;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

public class DatetimeLapse {

    private static final List<LocalDate> exceptionDays = new ArrayList<>();

    public DatetimeLapse() {
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 1, 1));
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 5, 1));
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 5, 10));
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 9, 15));
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 10, 1));
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 12, 26));
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 12, 27));
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 12, 28));
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 12, 29));
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 12, 30));
        exceptionDays.add(LocalDate.of(LocalDate.now().getYear(), 12, 31));
    }

    public static LocalDateTime addDays(LocalDateTime datetime, int days) {
        if (datetime == null) {
            return LocalDateTime.now();
        }
        int totaldays = 0;
        int addedDays = 0;
        while (totaldays != days) {
            if (isThisDaySaturdayOrSunday(datetime.plusDays(addedDays + 1)) ||
                    isAExceptionDate(datetime.plusDays(addedDays + 1).toLocalDate())) {
                totaldays--;
            }
            totaldays++;
            addedDays++;
        }
        return datetime.plusDays(addedDays);
    }

    private static boolean isThisDaySaturdayOrSunday(LocalDateTime datetime) {
        return datetime.getDayOfWeek() == DayOfWeek.SATURDAY ||
                datetime.getDayOfWeek() == DayOfWeek.SUNDAY;
    }

    private static boolean isAExceptionDate(LocalDate date) {
        return exceptionDays.contains(date);
    }
}
