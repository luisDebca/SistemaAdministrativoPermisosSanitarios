package edu.ues.aps.utilities.formatted.datetime;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class DatetimeUtil {

    private static final String[] mountNames = {"Enero","Febrero","Marzo","Abril","Mayo","Junio","Julio","Agosto","Septiembre","Octubre","Noviembre","Diciembre"};
    private static final String[] numberNames = {
            "Cero","Uno","Dos","Tres","Cuatro","Cinco","Seis","Siete","Ocho","Nueve",
            "Diez","Once","Doce","Trece","Catorce","Quince","Diecis�is","Diecisiete","Dieciocho","Diecinueve",
            "Veinte","Veintiuno","Veintid�s","Veintitr�s","Veinticuatro","Veinticinco","Veintis�is","Veintisiete","Veintiocho","Veinitinueve",
            "Treinta"};
    private static final String[] cardinalNumbers = {"Cero","Diec","Veinti","Treinta","Cuarenta","Cincuenta","Sesenta","Setenta","Ochenta","Noventa"};

    public static final String DATETIME_NOT_DEFINED = "--/--/---- --:--:--";
    public static final String DATE_NOT_DEFINED = "--/--/----";

    public static String getMonthSpanish(int monthValue) {
        return mountNames[monthValue - 1];
    }

    public static String getNumberSpanish(int numberValue){
        if(numberValue <= 30){
            return numberNames[numberValue];
        }else if((numberValue % 10) == 0){
            return cardinalNumbers[numberValue/10];
        }else {
            int dec = numberValue - (numberValue % 10);
            return cardinalNumbers[dec / 10] + " y " + numberNames[numberValue % 10].toLowerCase();
        }
    }

    public static String getYearSpanish(int yearValue){
        return "Dos mil " + getNumberSpanish(yearValue - 2000).toLowerCase();
    }

    public static String parseDatetimeToVerboseDate(LocalDateTime datetime) {
        if(datetime != null){
            return getNumberSpanish(datetime.getHour()) +
                    " horas con " +
                    getNumberSpanish(datetime.getMinute()) +
                    " minutos del d�a " +
                    getNumberSpanish(datetime.getDayOfMonth()) +
                    " de " + getMonthSpanish(datetime.getMonthValue()) + " " +
                    getYearSpanish(datetime.getYear());
        }else{
            return DATETIME_NOT_DEFINED;
        }
    }

    public static String parseDateToVerboseDate(LocalDate date){
        if(date != null){
            return getNumberSpanish(date.getDayOfMonth()) +
                    " de " +
                    getMonthSpanish(date.getMonthValue()) +
                    " de " +
                    getYearSpanish(date.getYear());
        }else{
            return DATE_NOT_DEFINED;
        }
    }

    public static String parseDateToShortFormat(LocalDate date) {
        if(date != null){
            return date.getDayOfMonth() + "/" +
                    date.getMonthValue() + "/" +
                    date.getYear();
        }else{
            return DATE_NOT_DEFINED;
        }
    }

    public static String parseDatetimeToShortFormat(LocalDateTime dateTime){
        if(dateTime != null){
            return dateTime.getDayOfMonth() + "/" +
                    dateTime.getMonthValue() + "/" +
                    dateTime.getYear() + " " +
                    dateTime.getHour() + ":" +
                    dateTime.getMinute() + ":" +
                    dateTime.getSecond();
        }else{
            return DATETIME_NOT_DEFINED;
        }
    }

    public static String parseDatetimeToLongDateFormat(LocalDateTime dateTime){
        if(dateTime != null){
            return dateTime.getDayOfMonth() + " de " +
                    getMonthSpanish(dateTime.getMonthValue()) + " " +
                    dateTime.getYear() + " " +
                    dateTime.getHour() + ":" +
                    dateTime.getMinute() + ":" +
                    dateTime.getSecond();
        }else{
            return DATETIME_NOT_DEFINED;
        }
    }

    public static String parseDateToLongDateFormat(LocalDate date){
        if(date != null){
            return date.getDayOfMonth() + " de " +
                    getMonthSpanish(date.getMonthValue()) + " " +
                    date.getYear();
        }else{
            return DATE_NOT_DEFINED;
        }
    }

    public static String parseDateToISODateFormat(LocalDate date){
        if (date != null){
            return date.getDayOfMonth() + "-" +
                    date.getMonthValue() + "-" +
                    date.getYear();
        }else {
            return DATE_NOT_DEFINED;
        }
    }

    public static String parseDatetimeToHtmlFormat(LocalDateTime localDateTime){
        if(localDateTime != null){
            return localDateTime.getYear() + "/" +
                    localDateTime.getMonthValue() + "/" +
                    localDateTime.getDayOfMonth() + " " +
                    localDateTime.getHour() + ":" +
                    localDateTime.getMinute() + ":" +
                    localDateTime.getSecond();
        }else {
            return DATETIME_NOT_DEFINED;
        }
    }

    public static LocalDateTime addDays(LocalDateTime date, Integer days) {
        if (date == null) {
            return DatetimeLapse.addDays(LocalDateTime.now(), days);
        }
        return DatetimeLapse.addDays(date, days);
    }

}
