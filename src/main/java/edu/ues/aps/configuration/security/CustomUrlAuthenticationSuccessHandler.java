package edu.ues.aps.configuration.security;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.DefaultRedirectStrategy;
import org.springframework.security.web.RedirectStrategy;
import org.springframework.security.web.WebAttributes;
import org.springframework.security.web.authentication.AuthenticationSuccessHandler;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.Collection;

public class CustomUrlAuthenticationSuccessHandler implements AuthenticationSuccessHandler {

    private static final int MAX_INACTIVE_INTERVAL_IN_SECONDS = 600;

    private static final Logger logger = LogManager.getLogger(CustomUrlAuthenticationSuccessHandler.class);

    private RedirectStrategy redirectStrategy = new DefaultRedirectStrategy();


    @Override
    public void onAuthenticationSuccess(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException, ServletException {
        handle(request, response, authentication);
        HttpSession session = request.getSession(false);
        if (session != null) {
            session.setMaxInactiveInterval(MAX_INACTIVE_INTERVAL_IN_SECONDS);
        }
        clearAuthenticationAttributes(request);
    }

    protected void handle(HttpServletRequest request, HttpServletResponse response, Authentication authentication) throws IOException {
        String targetUrl = determineTargetUrl(authentication);
        logger.debug("Redirecting user to home URL " + targetUrl);
        if (response.isCommitted()) {
            logger.debug("Response has already been committed. Unable to redirect to " + targetUrl);
            return;
        }
        redirectStrategy.sendRedirect(request, response, targetUrl);
    }

    protected String determineTargetUrl(Authentication authentication) {
        String redirect_index = "";
        Collection<? extends GrantedAuthority> authorities = authentication.getAuthorities();
        label:
        for (GrantedAuthority grantedAuthority : authorities) {
            switch (grantedAuthority.getAuthority()) {
                case "ROLE_COORDINATOR":
                    redirect_index = "/index/coordinator";
                    break label;
                case "ROLE_ESTABLISHMENT_TECHNICAL":
                case "ROLE_VEHICLE_TECHNICAL":
                case "ROLE_TOBACCO_TECHNICAL":
                    redirect_index = "/index/technical";
                    break label;
                case "ROLE_JURIDICAL":
                    redirect_index = "/index/juridical";
                    break label;
                case "ROLE_VISITOR":
                    redirect_index = "/index/visitor";
                    break label;
                case "ROLE_USER_ADMIN":
                case "ROLE_BD_ADMIN":
                case "ROLE_APP_ADMIN":
                    redirect_index = "/index/admin";
                    break label;
                default:
                    throw new UserRoleNotRegisterException("User role " + grantedAuthority.getAuthority() + " is not valid role");
            }
        }
        return redirect_index;
    }

    protected void clearAuthenticationAttributes(HttpServletRequest request) {
        HttpSession session = request.getSession(false);
        if (session == null) {
            return;
        }
        session.removeAttribute(WebAttributes.AUTHENTICATION_EXCEPTION);
    }

    public void setRedirectStrategy(RedirectStrategy redirectStrategy) {
        this.redirectStrategy = redirectStrategy;
    }
    protected RedirectStrategy getRedirectStrategy() {
        return redirectStrategy;
    }

}
