package edu.ues.aps.configuration.security;

public class UserRoleNotRegisterException extends RuntimeException{

    public UserRoleNotRegisterException() {
    }

    public UserRoleNotRegisterException(String s) {
        super(s);
    }
}
