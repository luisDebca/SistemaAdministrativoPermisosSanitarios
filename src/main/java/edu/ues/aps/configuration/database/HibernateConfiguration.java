package edu.ues.aps.configuration.database;

import org.hibernate.cfg.AvailableSettings;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.*;
import org.springframework.core.env.Environment;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScans(value = {
        @ComponentScan("edu.ues.aps.users.management.repository"),
        @ComponentScan("edu.ues.aps.users.management.service"),
        @ComponentScan("edu.ues.aps.process.area.establishment.repository"),
        @ComponentScan("edu.ues.aps.process.area.establishment.service"),
        @ComponentScan("edu.ues.aps.process.area.vehicle.repository"),
        @ComponentScan("edu.ues.aps.process.area.vehicle.service"),
        @ComponentScan("edu.ues.aps.process.base.repository"),
        @ComponentScan("edu.ues.aps.process.base.service"),
        @ComponentScan("edu.ues.aps.process.requirement.base.repository"),
        @ComponentScan("edu.ues.aps.process.requirement.base.service"),
        @ComponentScan("edu.ues.aps.process.requirement.establishment.repository"),
        @ComponentScan("edu.ues.aps.process.requirement.establishment.service"),
        @ComponentScan("edu.ues.aps.process.record.base.repository"),
        @ComponentScan("edu.ues.aps.process.record.base.service"),
        @ComponentScan("edu.ues.aps.process.legal.base.repository"),
        @ComponentScan("edu.ues.aps.process.legal.establishment.repository"),
        @ComponentScan("edu.ues.aps.process.legal.establishment.service"),
        @ComponentScan("edu.ues.aps.storage.service"),
        @ComponentScan("edu.ues.aps.storage.repository"),
        @ComponentScan("edu.ues.aps.etl")
})
@PropertySource("classpath:application.properties")
public class HibernateConfiguration {

    private final Environment environment;

    @Autowired
    public HibernateConfiguration(Environment environment) {
        this.environment = environment;
    }

    @Bean
    public HibernateTransactionManager transactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory() {
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setPackagesToScan(
                "edu.ues.aps.users.management.model",
                "edu.ues.aps.process.area.establishment.model",
                "edu.ues.aps.process.area.vehicle.model",
                "edu.ues.aps.process.base.model",
                "edu.ues.aps.process.legal.base.model",
                "edu.ues.aps.process.payment.base.model",
                "edu.ues.aps.process.record.base.model",
                "edu.ues.aps.process.requirement.base.model",
                "edu.ues.aps.process.resolution.base.model",
                "edu.ues.aps.process.tracing.base.model",
                "edu.ues.aps.process.settings.model",
                "edu.ues.aps.storage.model"
                );
        sessionFactoryBean.setHibernateProperties(hibernateProperties());
        return sessionFactoryBean;
    }

    @Bean
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName(environment.getRequiredProperty("jdbc.driverClassName"));
        dataSource.setUrl(environment.getRequiredProperty("jdbc.url"));
        dataSource.setUsername(environment.getRequiredProperty("jdbc.username"));
        dataSource.setPassword(environment.getRequiredProperty("jdbc.password"));
        return dataSource;
    }

    private Properties hibernateProperties() {
        Properties properties = new Properties();
        properties.put(AvailableSettings.HBM2DDL_AUTO, environment.getRequiredProperty("hibernate.hbm2ddl.auto"));
        properties.put(AvailableSettings.HBM2DDL_CHARSET_NAME, environment.getRequiredProperty("hibernate.charset"));
        properties.put(AvailableSettings.DIALECT, environment.getRequiredProperty("hibernate.dialect"));
        properties.put(AvailableSettings.FORMAT_SQL,environment.getRequiredProperty("hibernate.sql.format"));
        properties.put(AvailableSettings.C3P0_MIN_SIZE, environment.getRequiredProperty("c3p0.minimum.size"));
        properties.put(AvailableSettings.C3P0_MAX_SIZE, environment.getRequiredProperty("c3p0.maximum.size"));
        properties.put(AvailableSettings.C3P0_ACQUIRE_INCREMENT, environment.getRequiredProperty("c3p0.connections.acquired"));
        properties.put(AvailableSettings.C3P0_TIMEOUT, environment.getRequiredProperty("c3p0.connections.timeout"));
        properties.put(AvailableSettings.C3P0_MAX_STATEMENTS, environment.getRequiredProperty("c3p0.prepared.statement.cache.size"));
        properties.put(AvailableSettings.C3P0_CONFIG_PREFIX + ".initialPoolSize", environment.getRequiredProperty("c3p0.initial.pool.size"));
        return properties;
    }

}
