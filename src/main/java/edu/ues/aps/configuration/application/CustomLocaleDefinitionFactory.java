package edu.ues.aps.configuration.application;

import org.apache.tiles.Definition;
import org.apache.tiles.definition.LocaleDefinitionsFactory;
import org.apache.tiles.definition.NoSuchDefinitionException;
import org.apache.tiles.request.Request;

import java.util.Locale;

public class CustomLocaleDefinitionFactory extends LocaleDefinitionsFactory{

    public Definition getDefinition(String name, Request tilesContext){
        Definition result;
        Locale locale = null;
        result = definitionDao.getDefinition(name, locale);

        if (result != null) {
            result = new Definition(result);
            String parentDefinitionName = result.getExtends();
            while (parentDefinitionName != null) {
                Definition parent = definitionDao.getDefinition(parentDefinitionName, locale);
                if (parent == null) {
                    throw new NoSuchDefinitionException("Cannot findToday definition '" + parentDefinitionName
                            + "' ancestor of '" + result.getName() + "'");
                }
                result.inherit(parent);
                parentDefinitionName = parent.getExtends();
            }
        }
        return result;
    }
}
