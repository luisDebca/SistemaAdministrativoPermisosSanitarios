package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.base.dto.AbstractResolutionReportDTO;

import java.util.List;

public interface ResolutionReportService {

    List<AbstractResolutionReportDTO> getReportDatasource(Long id_resolution);
}
