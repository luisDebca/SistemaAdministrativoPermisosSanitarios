package edu.ues.aps.process.resolution.vehicle.repository;

import edu.ues.aps.process.resolution.base.dto.AbstractMemorandumDTO;
import edu.ues.aps.process.resolution.base.repository.MemorandumDTOFinderRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MemorandumBunchRepository implements MemorandumDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public MemorandumBunchRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractMemorandumDTO> findAll(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.base.dto.MemorandumDTO(" +
                "cv.id,memo.id,concat(concat(cv.request_folder,'-'),cv.request_year)," +
                "concat(concat(concat(concat('06',cv.request_code),cv.request_number),'-'),cv.request_year)," +
                "memo.number,memo.for_name,memo.for_charge,memo.from_name,memo.from_charge,memo.create_on,memo.type) " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.memorandums memo " +
                "where bunch.id = :id " +
                "group by memo.number",AbstractMemorandumDTO.class)
                .setParameter("id",id_bunch)
                .getResultList();
    }
}
