package edu.ues.aps.process.resolution.base.dto;

import java.time.LocalDate;

public class EmptyMemorandumReportDTO implements AbstractMemorandumReportDTO {

    @Override
    public String getMemo_content() {
        return " ";
    }

    @Override
    public void setMemo_content(String memo_content) {
    }

    @Override
    public String getMemo_code() {
        return String.format("%d-3000-DRSM-VP-0", LocalDate.now().getYear());
    }

    @Override
    public String getMemo_creation_date() {
        return "Date not found";
    }

    @Override
    public String getFor_name() {
        return "Name not found";
    }

    @Override
    public String getFor_charge() {
        return "Charge not found";
    }

    @Override
    public String getFrom_name() {
        return "Name not found";
    }

    @Override
    public String getFrom_charge() {
        return "Charge not found";
    }

    @Override
    public String getThrough_name() {
        return "Name not found";
    }

    @Override
    public String getThrough_charge() {
        return "Charge not found";
    }
}
