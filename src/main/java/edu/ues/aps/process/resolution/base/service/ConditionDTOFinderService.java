package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.dto.AbstractConditionDTO;

import java.util.List;

public interface ConditionDTOFinderService {

    List<AbstractConditionDTO> findAll();

    List<AbstractConditionDTO> findAll(CasefileSection section);

    AbstractConditionDTO find(Long id_condition);
}
