package edu.ues.aps.process.resolution.base.report;

public class ControlSheetAuthorizedName {

    private Integer index;
    private String name;

    public ControlSheetAuthorizedName() {
    }

    public ControlSheetAuthorizedName(Integer index, String name) {
        this.index = index;
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }
}
