package edu.ues.aps.process.resolution.vehicle.controller;

import edu.ues.aps.process.resolution.base.model.Memorandum;
import edu.ues.aps.process.resolution.vehicle.service.MemorandumBunchPersistenceService;
import edu.ues.aps.process.settings.model.ReportFormatType;
import edu.ues.aps.process.settings.service.ReportFormatContentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/resolution/memorandum")
public class MemorandumBunchPersistenceController {

    private static final Logger logger = LogManager.getLogger(MemorandumBunchPersistenceController.class);

    private static final String MEMO = "memorandum_bunch_form";

    private final ReportFormatContentService formatService;
    private final MemorandumBunchPersistenceService persistenceService;

    public MemorandumBunchPersistenceController(MemorandumBunchPersistenceService persistenceService, ReportFormatContentService formatService) {
        this.persistenceService = persistenceService;
        this.formatService = formatService;
    }

    @GetMapping("/bunch/{id_bunch}/new")
    public String showMemoDocument(@PathVariable Long id_bunch, ModelMap model){
        logger.info("GET:: Show new memorandum document for bunch {}.",id_bunch);
        model.addAttribute("id_bunch",id_bunch);
        model.addAttribute("memo",new Memorandum());
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.MEMO));
        return MEMO;
    }

    @PostMapping("/bunch/{id_bunch}/new")
    public String saveMemo(@ModelAttribute Memorandum memo, @PathVariable Long id_bunch){
        logger.info("POST:: Save new memorandum for bunch {}.",id_bunch);
        persistenceService.addAll(memo,id_bunch);
        return String.format("redirect:/resolution/memorandum/bunch/%d/all", id_bunch);
    }

    @GetMapping("/bunch/{id_bunch}/memo/{memoNumber}/edit")
    public String showMemoEditDocument(@PathVariable Long id_bunch, @PathVariable String memoNumber, ModelMap model){
        logger.info("GET:: Show edit memorandum {} document for bunch {}.",memoNumber, id_bunch);
        model.addAttribute("id_bunch",id_bunch);
        model.addAttribute("memo",persistenceService.find(id_bunch, memoNumber));
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.MEMO));
        return MEMO;
    }

    @PostMapping("/bunch/{id_bunch}/memo/{memoNumber}/edit")
    public String updateMemoDocument(@PathVariable Long id_bunch, @PathVariable String memoNumber, @ModelAttribute Memorandum memo){
        logger.info("POST:: Update memorandum {} for bunch {}",memoNumber, id_bunch);
        persistenceService.updateAll(memo,id_bunch);
        return String.format("redirect:/resolution/memorandum/bunch/%d/all", id_bunch);
    }

    @GetMapping("/bunch/{id_bunch}/memo/{memo}/delete")
    public String deleteMemorandum(@PathVariable Long id_bunch, @PathVariable String memo){
        logger.info("GET:: Delete memorandum {} form bunch {}",memo, id_bunch);
        persistenceService.removeAll(memo,id_bunch);
        return String.format("redirect:/resolution/memorandum/bunch/%d/all", id_bunch);
    }

}
