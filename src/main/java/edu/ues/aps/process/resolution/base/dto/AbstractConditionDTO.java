package edu.ues.aps.process.resolution.base.dto;

import edu.ues.aps.process.base.model.CasefileSection;

public interface AbstractConditionDTO {

    Long getId_condition();

    String getCondition();

    String getSection();
}
