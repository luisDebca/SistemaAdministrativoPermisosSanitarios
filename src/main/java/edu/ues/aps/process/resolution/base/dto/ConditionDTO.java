package edu.ues.aps.process.resolution.base.dto;

import edu.ues.aps.process.base.model.CasefileSection;

public class ConditionDTO implements AbstractConditionDTO{

    private Long id_condition;
    private String condition;
    private CasefileSection section;

    public ConditionDTO(Long id_condition, String condition, CasefileSection section) {
        this.id_condition = id_condition;
        this.condition = condition;
        this.section = section;
    }

    @Override
    public Long getId_condition() {
        return id_condition;
    }

    @Override
    public String getCondition() {
        return condition;
    }

    @Override
    public String getSection() {
        if(section == null){
            return CasefileSection.INVALID.getCasefileSection();
        }
        return section.getCasefileSection();
    }
}
