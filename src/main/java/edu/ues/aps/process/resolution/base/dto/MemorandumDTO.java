package edu.ues.aps.process.resolution.base.dto;

import edu.ues.aps.process.resolution.base.model.MemorandumType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MemorandumDTO implements AbstractMemorandumDTO {

    private static final String MEMORANDUM_CODE_FORMAT = "-3000-DRSM-VP-";

    private Long id_caseFile;
    private Long id_memo;
    private String folder_number;
    private String request_number;
    private String memo_number;
    private String for_name;
    private String for_charge;
    private String from_name;
    private String from_charge;
    private String through_name;
    private String through_charge;
    private LocalDateTime create_on;
    private MemorandumType type;

    public MemorandumDTO(Long id_memo, String folder_number, String request_number, String memo_number, String for_name, String from_name, LocalDateTime create_on, MemorandumType type) {
        this.id_memo = id_memo;
        this.folder_number = folder_number;
        this.request_number = request_number;
        this.memo_number = memo_number;
        this.for_name = for_name;
        this.from_name = from_name;
        this.create_on = create_on;
        this.type = type;
    }

    public MemorandumDTO(Long id_memo, String folder_number, String request_number, String memo_number, String for_name, String for_charge, String from_name, String from_charge, LocalDateTime create_on, MemorandumType type) {
        this.id_memo = id_memo;
        this.folder_number = folder_number;
        this.request_number = request_number;
        this.memo_number = memo_number;
        this.for_name = for_name;
        this.for_charge = for_charge;
        this.from_name = from_name;
        this.from_charge = from_charge;
        this.create_on = create_on;
        this.type = type;
    }

    public MemorandumDTO(Long id_caseFile, Long id_memo, String folder_number, String request_number, String memo_number, String for_name, String for_charge, String from_name, String from_charge, LocalDateTime create_on, MemorandumType type) {
        this.id_caseFile = id_caseFile;
        this.id_memo = id_memo;
        this.folder_number = folder_number;
        this.request_number = request_number;
        this.memo_number = memo_number;
        this.for_name = for_name;
        this.for_charge = for_charge;
        this.from_name = from_name;
        this.from_charge = from_charge;
        this.create_on = create_on;
        this.type = type;
    }

    public Long getId_caseFile() {
        return id_caseFile;
    }

    public void setId_caseFile(Long id_caseFile) {
        this.id_caseFile = id_caseFile;
    }

    public Long getId_memo() {
        return id_memo;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getMemo_number() {
        if (create_on != null) {
            return String.format("%d%s%s", create_on.getYear(), MEMORANDUM_CODE_FORMAT, memo_number);
        } else {
            return String.format("%d%s%s", LocalDate.now().getYear(), MEMORANDUM_CODE_FORMAT, memo_number);
        }
    }

    public String getNumber(){
        return memo_number;
    }

    public String getFor_name() {
        return for_name;
    }

    public String getFrom_name() {
        return from_name;
    }

    public String getCreate_on() {
        return DatetimeUtil.parseDatetimeToShortFormat(create_on);
    }

    public String getType() {
        return type.name();
    }

    public String getFor_charge() {
        return for_charge;
    }

    public String getFrom_charge() {
        return from_charge;
    }

    public String getThrough_name() {
        return through_name;
    }

    public String getThrough_charge() {
        return through_charge;
    }
}
