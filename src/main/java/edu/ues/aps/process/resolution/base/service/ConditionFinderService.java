package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.model.AbstractCondition;

import java.util.List;

public interface ConditionFinderService {

    List<AbstractCondition> findAll();

    List<AbstractCondition> findAll(CasefileSection section);
}
