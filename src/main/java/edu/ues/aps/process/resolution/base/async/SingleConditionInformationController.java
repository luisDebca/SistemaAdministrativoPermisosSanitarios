package edu.ues.aps.process.resolution.base.async;

import edu.ues.aps.process.resolution.base.dto.AbstractConditionDTO;
import edu.ues.aps.process.resolution.base.service.ConditionDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class SingleConditionInformationController {

    private static final Logger logger = LogManager.getLogger(SingleConditionInformationController.class);

    private final ConditionDTOFinderService finderService;

    public SingleConditionInformationController(ConditionDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/resolution-condition/get-condition-info", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AbstractConditionDTO findCondition(@RequestParam Long id_condition) {
        logger.info("GET:: Condition {} information",id_condition);
        return finderService.find(id_condition);
    }
}
