package edu.ues.aps.process.resolution.vehicle.dto;

public interface AbstractVehicleDiplomaReportDTO {

    String getRequest_number();

    String getResolution_number();

    String getVehicle_license();

    String getContent_type();

    String getVehicle_content();

    String getVehicle_type();

    String getVehicle_address();

    String getOwner_name();

    String getOwner_address();

    String getDuration();

    String getExpiration();

    String getCreation_date();

    String getSignatory_name();

    void setSignatory_name(String signatory_name);

    String getSignatory_charge();

    void setSignatory_charge(String signatory_charge);
}
