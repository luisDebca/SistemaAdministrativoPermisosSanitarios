package edu.ues.aps.process.resolution.base.dto;

public interface AbstractMemorandumReportDTO {

    String getMemo_content();

    void setMemo_content(String memo_content);

    String getMemo_code();

    String getMemo_creation_date();

    String getFor_name();

    String getFor_charge();

    String getFrom_name();

    String getFrom_charge();

    String getThrough_name();

    String getThrough_charge();
}
