package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.base.dto.AbstractMemorandumReportDTO;

import javax.persistence.NoResultException;

public interface MemorandumReportFinderRepository {

    AbstractMemorandumReportDTO findReportDataSource(Long id_memo) throws NoResultException;
}
