package edu.ues.aps.process.resolution.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleRequestDTO;

public interface AbstractVehicleResolutionRequestDTO extends AbstractVehicleRequestDTO {

    Long getMemo_count();

    Boolean getResolution_created();

    String getResolution_date();
}
