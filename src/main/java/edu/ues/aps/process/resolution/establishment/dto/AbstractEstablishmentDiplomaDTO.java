package edu.ues.aps.process.resolution.establishment.dto;

public interface AbstractEstablishmentDiplomaDTO {

    String getRequest_number();

    String getResolution_number();

    String getEstablishment_name();

    String getEstablishment_address();

    String getEstablishment_type();

    String getOwner_name();

    String getCertification_duration_in_years();

    String getCertification_expired();

    String getCurrent_date();

    String getSignatory_name();

    void setSignatory_name(String signatory_name);

    String getSignatory_charge();

    void setSignatory_charge(String signatory_charge);

    Boolean getEmpty_name();

}
