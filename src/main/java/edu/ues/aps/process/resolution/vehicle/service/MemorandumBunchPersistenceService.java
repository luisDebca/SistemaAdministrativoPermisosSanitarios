package edu.ues.aps.process.resolution.vehicle.service;

import edu.ues.aps.process.resolution.base.model.AbstractMemorandum;

public interface MemorandumBunchPersistenceService {

    AbstractMemorandum find(Long id_bunch, String memoNumber);

    void addAll(AbstractMemorandum memo, Long id_bunch);

    void updateAll(AbstractMemorandum memo, Long id_bunch);

    void removeAll(String memoNumber, Long id_bunch);
}
