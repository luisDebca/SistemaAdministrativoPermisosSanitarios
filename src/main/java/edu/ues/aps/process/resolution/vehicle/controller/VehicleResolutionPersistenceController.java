package edu.ues.aps.process.resolution.vehicle.controller;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.model.Resolution;
import edu.ues.aps.process.resolution.base.service.ConditionFinderService;
import edu.ues.aps.process.resolution.vehicle.service.VehicleResolutionInformationProviderService;
import edu.ues.aps.process.resolution.vehicle.service.VehicleResolutionPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/resolution/vehicle/bunch")
public class VehicleResolutionPersistenceController {

    private static final Logger logger = LogManager.getLogger(VehicleResolutionPersistenceController.class);

    private static final String RESOLUTION = "vehicle_resolution_form";

    private final ConditionFinderService conditionFinderService;
    private final VehicleResolutionPersistenceService persistenceService;
    private final VehicleResolutionInformationProviderService informationProviderService;

    public VehicleResolutionPersistenceController(ConditionFinderService conditionFinderService, VehicleResolutionInformationProviderService informationProviderService, VehicleResolutionPersistenceService persistenceService) {
        this.conditionFinderService = conditionFinderService;
        this.informationProviderService = informationProviderService;
        this.persistenceService = persistenceService;
    }

    @GetMapping("/{id_bunch}/new")
    public String showBunchResolutionForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: New resolution for all request in bunch {}.", id_bunch);
        model.addAttribute("resolutions", informationProviderService.findContentResolution(id_bunch));
        model.addAttribute("conditions", conditionFinderService.findAll(CasefileSection.VEHICLE));
        model.addAttribute("inspections", informationProviderService.findResolutionInspection(id_bunch));
        model.addAttribute("resolution", new Resolution());
        return RESOLUTION;
    }

    @PostMapping("/{id_bunch}/new")
    public String saveBunchResolution(@PathVariable Long id_bunch, @ModelAttribute Resolution resolution) {
        logger.info("POST:: Save all resolution for bunch {}.", id_bunch);
        persistenceService.save(id_bunch, resolution);
        return "redirect:/resolution/vehicle/all";
    }

    @GetMapping("/{id_bunch}/edit")
    public String showBunchResolutionUpdateForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Edit resolution for all request in bunch {}.", id_bunch);
        model.addAttribute("resolutions", informationProviderService.findContentResolution(id_bunch));
        model.addAttribute("conditions", conditionFinderService.findAll(CasefileSection.VEHICLE));
        model.addAttribute("inspections", informationProviderService.findResolutionInspection(id_bunch));
        model.addAttribute("resolution", persistenceService.find(id_bunch));
        return RESOLUTION;
    }

    @PostMapping("/{id_bunch}/edit")
    public String updateBunchResolution(@PathVariable Long id_bunch, @ModelAttribute Resolution resolution) {
        logger.info("POST:: Update all resolution for bunch {}.", id_bunch);
        persistenceService.update(id_bunch, resolution);
        return "redirect:/resolution/vehicle/all";
    }

}
