package edu.ues.aps.process.resolution.base.dto;

public class ResolutionFormDTO implements AbstractResolutionFormDTO{

    private String ucsf;
    private String memo;
    private String inspector_name;
    private String inspector_charge;

    public ResolutionFormDTO(String ucsf, String memo, String inspector_name, String inspector_charge) {
        this.ucsf = ucsf;
        this.memo = memo;
        this.inspector_name = inspector_name;
        this.inspector_charge = inspector_charge;
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getMemo() {
        return memo;
    }

    public String getInspector_name() {
        if (inspector_name == null)
            return "";
        return inspector_name;
    }

    public String getInspector_charge() {
        if (inspector_charge == null)
            return "";
        return inspector_charge;
    }
}
