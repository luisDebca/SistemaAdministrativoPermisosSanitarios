package edu.ues.aps.process.resolution.establishment.service;

import edu.ues.aps.process.resolution.base.dto.AbstractControlSheetDTO;
import edu.ues.aps.process.resolution.base.dto.EmptyControlSheetDTO;
import edu.ues.aps.process.resolution.base.repository.ControlSheetReportRepository;
import edu.ues.aps.process.resolution.base.service.ControlSheetReportService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional(readOnly = true)
public class EstablishmentControlSheetReportService implements ControlSheetReportService {

    private final ControlSheetReportRepository reportRepository;

    public EstablishmentControlSheetReportService(ControlSheetReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }

    @Override
    public AbstractControlSheetDTO getReportDatasource(Long id_caseFile) {
        try {
            return reportRepository.getReportDatasource(id_caseFile);
        } catch (NoResultException e) {
            return new EmptyControlSheetDTO();
        }
    }
}
