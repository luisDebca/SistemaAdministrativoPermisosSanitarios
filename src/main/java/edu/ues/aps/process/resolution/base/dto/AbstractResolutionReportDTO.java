package edu.ues.aps.process.resolution.base.dto;

public interface AbstractResolutionReportDTO {

    String getRequest_number();

    String getResolution_number();

    String getResolution_conditions();

    String getCreation_date();

    String getResolution_content();

    void setResolution_content(String resolution_content);

    String getSignatory_name();

    void setSignatory_name(String signatory_name);

    String getSignatory_charge();

    void setSignatory_charge(String signatory_charge);
}
