package edu.ues.aps.process.resolution.establishment.service;

import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentResolutionFormDTO;

import java.util.List;

public interface ResolutionInformationProviderService {

    List<AbstractEstablishmentResolutionFormDTO> findAll(Long id_caseFile);
}
