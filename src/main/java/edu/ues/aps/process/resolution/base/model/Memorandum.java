package edu.ues.aps.process.resolution.base.model;

import edu.ues.aps.process.base.model.Casefile;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "MEMORANDUM")
public class Memorandum implements AbstractMemorandum {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "number", nullable = false, length = 9)
    private String number;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, length = 50)
    private MemorandumType type;

    @Column(name = "for_name", length = 100)
    private String for_name;

    @Column(name = "for_charge", length = 100)
    private String for_charge;

    @Column(name = "from_name", length = 100)
    private String from_name;

    @Column(name = "from_charge", length = 100)
    private String from_charge;

    @Column(name = "through_name", length = 100)
    private String through_name;

    @Column(name = "through_charge", length = 100)
    private String through_charge;

    @Column(name = "subject", length = 100)
    private String subject;

    @Column(name = "created_on")
    private LocalDateTime create_on;

    @Column(name = "document_content", nullable = false, length = 50)
    private String document_content;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_casefile")
    private Casefile casefile;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public MemorandumType getType() {
        return type;
    }

    @Override
    public void setType(MemorandumType type) {
        this.type = type;
    }

    @Override
    public String getFor_name() {
        return for_name;
    }

    @Override
    public void setFor_name(String for_name) {
        this.for_name = for_name;
    }

    @Override
    public String getFor_charge() {
        return for_charge;
    }

    @Override
    public void setFor_charge(String for_charge) {
        this.for_charge = for_charge;
    }

    @Override
    public String getFrom_name() {
        return from_name;
    }

    @Override
    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    @Override
    public String getFrom_charge() {
        return from_charge;
    }

    @Override
    public void setFrom_charge(String from_charge) {
        this.from_charge = from_charge;
    }

    @Override
    public String getThrough_name() {
        return through_name;
    }

    @Override
    public void setThrough_name(String through_name) {
        this.through_name = through_name;
    }

    @Override
    public String getThrough_charge() {
        return through_charge;
    }

    @Override
    public void setThrough_charge(String through_charge) {
        this.through_charge = through_charge;
    }

    @Override
    public String getSubject() {
        return subject;
    }

    @Override
    public void setSubject(String subject) {
        this.subject = subject;
    }

    @Override
    public LocalDateTime getCreate_on() {
        return create_on;
    }

    @Override
    public void setCreate_on(LocalDateTime create_on) {
        this.create_on = create_on;
    }

    @Override
    public String getDocument_content() {
        return document_content;
    }

    @Override
    public void setDocument_content(String document_content) {
        this.document_content = document_content;
    }

    @Override
    public Casefile getCasefile() {
        return casefile;
    }

    @Override
    public void setCasefile(Casefile casefile) {
        this.casefile = casefile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Memorandum that = (Memorandum) o;

        return number.equals(that.number) && type == that.type;
    }

    @Override
    public int hashCode() {
        int result = number.hashCode();
        result = 31 * result + type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Memorandum{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", type=" + type +
                ", for_name='" + for_name + '\'' +
                ", for_charge='" + for_charge + '\'' +
                ", from_name='" + from_name + '\'' +
                ", from_charge='" + from_charge + '\'' +
                ", through_name='" + through_name + '\'' +
                ", through_charge='" + through_charge + '\'' +
                ", create_on=" + create_on +
                ", document_content='" + document_content + '\'' +
                '}';
    }
}
