package edu.ues.aps.process.resolution.establishment.repository;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.model.AbstractCondition;
import edu.ues.aps.process.resolution.base.repository.ConditionFinderRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class EstablishmentConditionRepository implements ConditionFinderRepository {

    private final SessionFactory sessionFactory;

    public EstablishmentConditionRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractCondition findById(Long id_condition) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select condition " +
                "from Condition condition " +
                "where condition.id = :id",AbstractCondition.class)
                .setParameter("id",id_condition)
                .getSingleResult();
    }

    @Override
    public List<AbstractCondition> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select condition " +
                "from Condition condition ", AbstractCondition.class)
                .getResultList();
    }

    @Override
    public List<AbstractCondition> findAll(CasefileSection section) {
        return sessionFactory.getCurrentSession().createQuery("select condition " +
                "from Condition condition " +
                "where condition.section = :establishment",AbstractCondition.class)
                .setParameter("establishment", section)
                .getResultList();
    }
}
