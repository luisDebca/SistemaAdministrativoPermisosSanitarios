package edu.ues.aps.process.resolution.base.dto;

public class EmptyConditionDTO implements AbstractConditionDTO {

    @Override
    public Long getId_condition() {
        return 0L;
    }

    @Override
    public String getCondition() {
        return "Condition not found";
    }

    @Override
    public String getSection() {
        return "Section not found";
    }
}
