package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.base.model.AbstractCondition;
import edu.ues.aps.process.resolution.base.model.EmptyCondition;
import edu.ues.aps.process.resolution.base.repository.ConditionFinderRepository;
import edu.ues.aps.process.resolution.base.repository.ConditionPersistenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
public class ConditionService implements ConditionPersistenceService{

    private final ConditionPersistenceRepository persistenceRepository;

    public ConditionService(ConditionPersistenceRepository persistenceRepository) {
        this.persistenceRepository = persistenceRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractCondition findById(Long id) {
       return persistenceRepository.find(id);
    }

    @Override
    @Transactional
    public void save(AbstractCondition condition) {
        persistenceRepository.save(condition);
    }

    @Override
    @Transactional
    public void update(AbstractCondition condition) {
        AbstractCondition persistentCondition = persistenceRepository.find(condition.getId());
        persistentCondition.setCondition(condition.getCondition());
        persistentCondition.setSection(condition.getSection());
    }

    @Override
    @Transactional
    public void delete(Long id_condition) {
        AbstractCondition toDelete = persistenceRepository.find(id_condition);
        if(toDelete != null)
            persistenceRepository.delete(toDelete);
    }
}
