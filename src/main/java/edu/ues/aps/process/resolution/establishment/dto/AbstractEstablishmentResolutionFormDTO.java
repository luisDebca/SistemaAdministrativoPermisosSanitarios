package edu.ues.aps.process.resolution.establishment.dto;

import edu.ues.aps.process.resolution.base.dto.AbstractResolutionFormDTO;

public interface AbstractEstablishmentResolutionFormDTO extends AbstractResolutionFormDTO {

    String getEstablishment_name();

    String getEstablishment_address();

    String getOwner_name();

}
