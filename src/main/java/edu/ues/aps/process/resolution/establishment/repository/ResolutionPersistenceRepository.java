package edu.ues.aps.process.resolution.establishment.repository;

import edu.ues.aps.process.resolution.base.model.AbstractResolution;

public interface ResolutionPersistenceRepository {

    void merge(AbstractResolution resolution);

    void saveOrUpdate(AbstractResolution resolution);

}
