package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentRequestResolutionDTO;

import java.util.List;

public interface ResolutionProcessRequestFinderRepository {

    List<AbstractEstablishmentRequestResolutionDTO> findRequestForCurrentProcess();
}
