package edu.ues.aps.process.resolution.establishment.dto;

public class EmptyEstablishmentDiplomaDTO implements AbstractEstablishmentDiplomaDTO {

    @Override
    public String getRequest_number() {
        return "Request number not found";
    }

    @Override
    public String getResolution_number() {
        return "Resolution number not found";
    }

    @Override
    public String getEstablishment_name() {
        return "Establishment name not found";
    }

    @Override
    public String getEstablishment_address() {
        return "Establishment address not found";
    }

    @Override
    public String getEstablishment_type() {
        return "Establishment type not found";
    }

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public String getCertification_duration_in_years() {
        return "O";
    }

    @Override
    public String getCertification_expired() {
        return "Date not found";
    }

    @Override
    public String getCurrent_date() {
        return "Date not found";
    }

    @Override
    public String getSignatory_name() {
        return "Not found";
    }

    @Override
    public void setSignatory_name(String signatory_name) {

    }

    @Override
    public String getSignatory_charge() {
        return "Not found";
    }

    @Override
    public void setSignatory_charge(String signatory_charge) {

    }

    @Override
    public Boolean getEmpty_name() {
        return false;
    }
}
