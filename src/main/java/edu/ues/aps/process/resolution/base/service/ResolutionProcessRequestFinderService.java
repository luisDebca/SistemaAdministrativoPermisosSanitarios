package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentRequestResolutionDTO;

import java.util.List;

public interface ResolutionProcessRequestFinderService {

    List<AbstractEstablishmentRequestResolutionDTO> findRequests();
}
