package edu.ues.aps.process.resolution.vehicle.dto;

import edu.ues.aps.process.area.vehicle.model.TransportedContentType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class VehicleResolutionReportDTO implements AbstractVehicleResolutionReportDTO, AbstractVehicleDiplomaReportDTO {

    private String request_number;
    private String resolution_number;
    private CertificationType certification;
    private String vehicle_license;
    private TransportedContentType content_type;
    private String vehicle_type;
    private String vehicle_address;
    private String owner_name;
    private String owner_address;
    private Integer duration;
    private LocalDateTime certification_start;
    private String resolution_content;
    private String resolution_condition;
    private String signatory_name;
    private String signatory_charge;

    public VehicleResolutionReportDTO(String request_number, String resolution_number, CertificationType certification, String vehicle_license, TransportedContentType content_type, String vehicle_type, String vehicle_address, String owner_name, String owner_address, Integer duration, LocalDateTime certification_start, String resolution_content, String resolution_condition) {
        this.request_number = request_number;
        this.resolution_number = resolution_number;
        this.certification = certification;
        this.vehicle_license = vehicle_license;
        this.content_type = content_type;
        this.vehicle_type = vehicle_type;
        this.vehicle_address = vehicle_address;
        this.owner_name = owner_name;
        this.owner_address = owner_address;
        this.duration = duration;
        this.certification_start = certification_start;
        this.resolution_content = resolution_content;
        this.resolution_condition = resolution_condition;
    }

    public VehicleResolutionReportDTO(String request_number, String resolution_number, CertificationType certification, String vehicle_license, TransportedContentType content_type, String vehicle_type, String vehicle_address, String owner_name, String owner_address, Integer duration, LocalDateTime certification_start) {
        this.request_number = request_number;
        this.resolution_number = resolution_number;
        this.certification = certification;
        this.vehicle_license = vehicle_license;
        this.content_type = content_type;
        this.vehicle_type = vehicle_type;
        this.vehicle_address = vehicle_address;
        this.owner_name = owner_name;
        this.owner_address = owner_address;
        this.duration = duration;
        this.certification_start = certification_start;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getResolution_number() {
        return resolution_number;
    }

    public String getCertification() {
        if (certification == null)
            return "";
        if (certification.equals(CertificationType.FIRST)){
            return "el";
        }else if (certification.equals(CertificationType.RENEWAL)){
            return "la" + certification.getCertificationType() + " del";
        }
        return certification.getCertificationType();
    }

    public String getVehicle_license() {
        return vehicle_license;
    }

    public String getContent_type() {
        if(content_type == null)
            return TransportedContentType.INVALID.getTransportedContentType();
        return content_type.getTransportedContentType();
    }

    @Override
    public String getVehicle_content() {
        if (content_type == null)
            return TransportedContentType.INVALID.getTransportedContentType();
        return content_type.getTransportedContentType() + "S";
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public String getVehicle_address() {
        return vehicle_address;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getOwner_address() {
        return owner_address;
    }

    public String getDuration() {
        if (duration == 1) return "UN";
        return DatetimeUtil.getNumberSpanish(duration);
    }

    public String getExpiration() {
        if (certification_start == null)
            return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToVerboseDate(certification_start.toLocalDate().plusYears(duration));
    }

    public String getResolution_content() {
        return resolution_content;
    }

    public void setResolution_content(String resolution_content) {
        this.resolution_content = resolution_content;
    }

    public String getResolution_conditions() {
        return replaceDates(resolution_condition);
    }

    public void setResolution_condition(String resolution_condition) {
        this.resolution_condition = resolution_condition;
    }

    public String getCreation_date() {
        return DatetimeUtil.parseDateToVerboseDate(LocalDate.now());
    }

    public String getSignatory_name() {
        return signatory_name;
    }

    public void setSignatory_name(String signatory_name) {
        this.signatory_name = signatory_name;
    }

    public String getSignatory_charge() {
        return signatory_charge;
    }

    public void setSignatory_charge(String signatory_charge) {
        this.signatory_charge = signatory_charge;
    }

    private String replaceDates(String baseString) {
        if (baseString.contains(".DURACION") || baseString.contains(".DURACI�N")) {
            if (duration > 1) {
                baseString = baseString.replaceAll(".DURACION", DatetimeUtil.getNumberSpanish(duration).toUpperCase() + " A�OS");
                baseString = baseString.replaceAll(".DURACI�N", DatetimeUtil.getNumberSpanish(duration).toUpperCase() + " A�OS");
            } else {
                baseString = baseString.replaceAll(".DURACION", "UN A�O");
                baseString = baseString.replaceAll(".DURACI�N", "UN A�O");
            }
        }
        if (baseString.contains(".FINAL") && certification_start != null) {
            baseString = baseString.replaceAll(".FINAL", DatetimeUtil.parseDateToVerboseDate(certification_start.toLocalDate().plusYears(duration)));
        }
        return baseString;
    }
}
