package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.base.dto.AbstractMemorandumDTO;

import java.util.List;

public interface MemorandumDTOFinderRepository {

    List<AbstractMemorandumDTO> findAll(Long id);
}
