package edu.ues.aps.process.resolution.base.dto;

public interface AbstractResolutionFormDTO {

    String getUcsf();

    String getMemo();

    String getInspector_name();

    String getInspector_charge();
}
