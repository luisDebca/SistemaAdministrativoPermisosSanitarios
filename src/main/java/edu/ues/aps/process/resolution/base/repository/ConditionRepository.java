package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.base.model.AbstractCondition;
import edu.ues.aps.process.resolution.base.model.Condition;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ConditionRepository implements ConditionPersistenceRepository{

    private final SessionFactory sessionFactory;

    public ConditionRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractCondition find(Long id_condition) {
        return sessionFactory.getCurrentSession().get(Condition.class,id_condition);
    }

    @Override
    public void save(AbstractCondition condition) {
        sessionFactory.getCurrentSession().persist(condition);
    }

    @Override
    public void delete(AbstractCondition condition) {
        sessionFactory.getCurrentSession().delete(condition);
    }
}
