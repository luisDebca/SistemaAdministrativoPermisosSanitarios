package edu.ues.aps.process.resolution.establishment.dto;

public class ResolutionFormDTO implements AbstractResolutionFormDTO {

    private String establishment_name;
    private String establishment_address;
    private String owner_name;
    private String ucsf;
    private String memo;
    private String inspector_name;
    private String inspector_charge;

    public ResolutionFormDTO(String establishment_name, String establishment_address, String owner_name, String ucsf, String memo, String inspector_name, String inspector_charge) {
        this.establishment_name = establishment_name;
        this.establishment_address = establishment_address;
        this.owner_name = owner_name;
        this.ucsf = ucsf;
        this.memo = memo;
        this.inspector_name = inspector_name;
        this.inspector_charge = inspector_charge;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getMemo() {
        return memo;
    }

    public String getInspector_name() {
        return inspector_name;
    }

    public String getInspector_charge() {
        return inspector_charge;
    }
}
