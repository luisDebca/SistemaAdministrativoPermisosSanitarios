package edu.ues.aps.process.resolution.base.controller;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.model.Condition;
import edu.ues.aps.process.resolution.base.service.ConditionPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/resolution-condition")
public class ConditionPersistenceController {

    private static final Logger logger = LogManager.getLogger(ConditionPersistenceController.class);

    private static final String CONDITION_FORM = "condition_form";

    private final ConditionPersistenceService persistenceService;

    public ConditionPersistenceController(ConditionPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping("/new")
    public String showNewConditionForm(ModelMap model) {
        logger.info("GET:: Show new resolution condition form");
        model.addAttribute("condition", new Condition());
        return CONDITION_FORM;
    }

    @PostMapping("/new")
    public String saveCondition(@ModelAttribute("condition") String condition_content,
                                @ModelAttribute("section") String section) {
        logger.info("POST:: Save new resolution condition for {}", section);
        Condition condition = new Condition();
        condition.setCondition(condition_content);
        condition.setSection(CasefileSection.valueOf(section));
        persistenceService.save(condition);
        return "redirect:/resolution-condition/list";
    }

    @GetMapping("/{id_condition}/edit")
    public String showConditionEditForm(@PathVariable Long id_condition, ModelMap model) {
        logger.info("GET:: Show resolution condition {} edit form", id_condition);
        model.addAttribute("condition", persistenceService.findById(id_condition));
        return CONDITION_FORM;
    }

    @PostMapping("/{id_condition}/edit")
    public String updateCondition(@ModelAttribute("condition") String condition_content,
                                  @ModelAttribute("section") String section, @PathVariable Long id_condition) {
        logger.info("POST:: Update resolution condition {}", id_condition);
        Condition condition = new Condition();
        condition.setId(id_condition);
        condition.setCondition(condition_content);
        condition.setSection(CasefileSection.valueOf(section));
        persistenceService.update(condition);
        return "redirect:/resolution-condition/list";
    }

    @GetMapping("/{id_condition}/delete")
    public String deleteCondition(@PathVariable Long id_condition) {
        logger.info("GET:: Delete resolution condition {}", id_condition);
        persistenceService.delete(id_condition);
        return "redirect:/resolution-condition/list";
    }
}
