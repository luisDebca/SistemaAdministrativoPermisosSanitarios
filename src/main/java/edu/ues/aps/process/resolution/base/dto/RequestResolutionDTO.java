package edu.ues.aps.process.resolution.base.dto;

public class RequestResolutionDTO implements AbstractRequestResolutionDTO{

    private Long id_casefile;
    private Long id_target;
    private Long id_owner;
    private String casefile_code;
    private String target_name;
    private String target_address;
    private String target_type;
    private String owner_name;
    private Long memorandum_count;
    private boolean resolution_created;

    public RequestResolutionDTO(Long id_casefile, Long id_target, Long id_owner, String casefile_code, String target_name, String target_address, String target_type, String owner_name, Long memorandum_count, boolean resolution_created) {
        this.id_casefile = id_casefile;
        this.id_target = id_target;
        this.id_owner = id_owner;
        this.casefile_code = casefile_code;
        this.target_name = target_name;
        this.target_address = target_address;
        this.target_type = target_type;
        this.owner_name = owner_name;
        this.memorandum_count = memorandum_count;
        this.resolution_created = resolution_created;
    }

    public RequestResolutionDTO(Long id_casefile, Long id_target, Long id_owner, String casefile_code, String target_name, String target_address, String target_type, String owner_name, boolean resolution_created) {
        this.id_casefile = id_casefile;
        this.id_target = id_target;
        this.id_owner = id_owner;
        this.casefile_code = casefile_code;
        this.target_name = target_name;
        this.target_address = target_address;
        this.target_type = target_type;
        this.owner_name = owner_name;
        this.resolution_created = resolution_created;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public Long getId_target() {
        return id_target;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public String getCasefile_code() {
        return casefile_code;
    }

    public String getTarget_name() {
        return target_name;
    }

    public String getTarget_address() {
        return target_address;
    }

    public String getTarget_type() {
        return target_type;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public Long getMemorandum_count() {
        return memorandum_count;
    }

    public boolean isResolution_created() {
        return resolution_created;
    }
}
