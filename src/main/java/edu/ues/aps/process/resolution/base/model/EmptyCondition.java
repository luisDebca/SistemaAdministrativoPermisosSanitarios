package edu.ues.aps.process.resolution.base.model;

import edu.ues.aps.process.base.model.CasefileSection;

import java.util.Collections;
import java.util.List;

public class EmptyCondition implements AbstractCondition{

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getCondition() {
        return "Condition not found";
    }

    @Override
    public void setCondition(String condition) {

    }

    @Override
    public CasefileSection getSection() {
        return CasefileSection.INVALID;
    }

    @Override
    public void setSection(CasefileSection section) {

    }

    @Override
    public List<Resolution> getResolutions() {
        return Collections.emptyList();
    }

    @Override
    public void setResolutions(List<Resolution> resolutions) {

    }
}
