package edu.ues.aps.process.resolution.base.model;

public enum MemorandumType {

    INNER_REPORT,
    INNER_REPORT_FAVORABLE,
    INNER_REPORT_FAVORABLE_DELEGATE,
    INVALID
}
