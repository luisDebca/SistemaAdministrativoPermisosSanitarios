package edu.ues.aps.process.resolution.establishment.service;

import edu.ues.aps.process.resolution.base.dto.AbstractMemorandumDTO;
import edu.ues.aps.process.resolution.base.repository.MemorandumDTOFinderRepository;
import edu.ues.aps.process.resolution.base.service.MemorandumDTOFinderService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class MemorandumEstablishmentService implements MemorandumDTOFinderService {

    private final MemorandumDTOFinderRepository finderRepository;

    public MemorandumEstablishmentService(@Qualifier("memorandumEstablishmentRepository") MemorandumDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractMemorandumDTO> findAll(Long id_caseFile) {
        List<AbstractMemorandumDTO> all = finderRepository.findAll(id_caseFile);
        for (AbstractMemorandumDTO dto : all) {
            dto.setId_caseFile(id_caseFile);
        }
        return all;
    }

}
