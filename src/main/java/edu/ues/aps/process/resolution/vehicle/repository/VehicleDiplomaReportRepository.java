package edu.ues.aps.process.resolution.vehicle.repository;

import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleDiplomaReportDTO;

import java.util.List;

public interface VehicleDiplomaReportRepository {

    List<AbstractVehicleDiplomaReportDTO> findDiploma(Long id_bunch);
}
