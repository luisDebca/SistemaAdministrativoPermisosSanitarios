package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.dto.AbstractConditionDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class ConditionDTORepository implements ConditionDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public ConditionDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractConditionDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.base.dto.ConditionDTO(" +
                "condition.id,condition.condition,condition.section)" +
                "from Condition condition", AbstractConditionDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractConditionDTO> findAll(CasefileSection section) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.base.dto.ConditionDTO(" +
                "condition.id,condition.condition,condition.section)" +
                "from Condition condition " +
                "where condition.section = :section", AbstractConditionDTO.class)
                .setParameter("section",section)
                .getResultList();
    }

    @Override
    public AbstractConditionDTO find(Long id_condition) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.base.dto.ConditionDTO(" +
                "condition.id,condition.condition,condition.section)" +
                "from Condition condition " +
                "where condition.id = :id", AbstractConditionDTO.class)
                .setParameter("id", id_condition)
                .getSingleResult();
    }
}
