package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.base.model.AbstractCondition;

public interface ConditionPersistenceService {

    AbstractCondition findById(Long id_condition);

    void save(AbstractCondition condition);

    void update(AbstractCondition condition);

    void delete(Long id_condition);
}
