package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.base.model.AbstractMemorandum;
import edu.ues.aps.process.resolution.base.model.Memorandum;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

@Repository
public class MemorandumRepository implements MemorandumPersistenceRepository{

    private final SessionFactory sessionFactory;

    public MemorandumRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractMemorandum find(Long id_memorandum) {
        return sessionFactory.getCurrentSession().get(Memorandum.class,id_memorandum);
    }

    @Override
    public AbstractMemorandum find(Long id_caseFile, String memoNumber) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select memo " +
                "from Memorandum memo " +
                "where memo.casefile.id = :id " +
                "and memo.number = :number", AbstractMemorandum.class)
                .setParameter("id",id_caseFile)
                .setParameter("number",memoNumber)
                .getSingleResult();
    }

    @Override
    public Long getMaxMemorandumNumber() {
        Long memoDefault = executeProcedure("getDefaultMemorandumNumber");
        Long memoMax = executeProcedure("getMaxMemorandumNumber");
        return memoDefault >= memoMax ? memoDefault : memoMax;
    }

    private Long executeProcedure(String procedure) {
        StoredProcedureQuery query = sessionFactory.getCurrentSession().createStoredProcedureQuery(procedure).
                registerStoredProcedureParameter("count", Long.class, ParameterMode.OUT);
        query.execute();
        if (query.getOutputParameterValue("count") != null) {
            return (Long) query.getOutputParameterValue("count");
        } else {
            return 0L;
        }
    }
}
