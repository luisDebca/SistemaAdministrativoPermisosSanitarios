package edu.ues.aps.process.resolution.vehicle.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.resolution.vehicle.service.VehicleResolutionReportService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/resolution/vehicle/bunch")
public class VehicleBunchResolutionReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(VehicleBunchResolutionReportController.class);

    private static final String REPORT_NAME = "ResolutionVReport";

    private final VehicleResolutionReportService reportService;

    public VehicleBunchResolutionReportController(VehicleResolutionReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/resolution-report.pdf")
    public String generateReport(@RequestParam Long id_bunch, @RequestParam String signatory_name, @RequestParam String signatory_charge, ModelMap model){
        logger.info("GET:: Generate all resolution for bunch {}.",id_bunch);
        addReportRequiredSources(reportService.getResolution(id_bunch,signatory_name,signatory_charge));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }
}
