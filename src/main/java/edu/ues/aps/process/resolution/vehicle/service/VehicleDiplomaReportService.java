package edu.ues.aps.process.resolution.vehicle.service;

import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleDiplomaReportDTO;

import java.util.List;

public interface VehicleDiplomaReportService {

    List<AbstractVehicleDiplomaReportDTO> findDiploma(Long id_bunch, String signatoryName, String signatoryCharge);
}
