package edu.ues.aps.process.resolution.establishment.controller;

import edu.ues.aps.process.area.base.service.RequestNumberInformationService;
import edu.ues.aps.process.resolution.base.service.MemorandumDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/resolution/memorandum")
public class MemorandumInformationProviderController {

    private static final Logger logger = LogManager.getLogger(MemorandumInformationProviderController.class);

    private static final String MEMORANDUM_LIST = "memorandum_list";

    private final RequestNumberInformationService requestNumberService;
    private final MemorandumDTOFinderService finderService;

    public MemorandumInformationProviderController(@Qualifier("memorandumEstablishmentService") MemorandumDTOFinderService finderService, RequestNumberInformationService requestNumberService) {
        this.finderService = finderService;
        this.requestNumberService = requestNumberService;
    }

    @GetMapping("/{id_caseFile}/list")
    public String showAllMemorandums(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show all memorandums for case file {}", id_caseFile);
        model.addAttribute("memos", finderService.findAll(id_caseFile));
        model.addAttribute("id_caseFile", id_caseFile);
        model.addAttribute("request",requestNumberService.find(id_caseFile));
        return MEMORANDUM_LIST;
    }

}
