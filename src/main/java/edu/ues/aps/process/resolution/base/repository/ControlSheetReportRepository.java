package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.base.dto.AbstractControlSheetDTO;

import javax.persistence.NoResultException;

public interface ControlSheetReportRepository {

    AbstractControlSheetDTO getReportDatasource(Long id_caseFile) throws NoResultException;
}
