package edu.ues.aps.process.resolution.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentRequestDTO;

public interface AbstractEstablishmentRequestResolutionDTO extends AbstractEstablishmentRequestDTO {

    Long getMemo_count();

    Boolean getResolution_created();

    String getResolution_number();

    String getResolution_date();
}
