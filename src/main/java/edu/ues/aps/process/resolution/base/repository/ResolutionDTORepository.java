package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.base.dto.AbstractResolutionReportDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ResolutionDTORepository implements ResolutionReportRepository {

    private final SessionFactory sessionFactory;

    public ResolutionDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractResolutionReportDTO> findReportDatasource(Long id_resolution) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.base.dto.ResolutionReportDTO(" +
                "concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year)," +
                "concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year)," +
                "resolution.content,condition.condition,resolution.resolution_create_on,casefile.certification_duration_in_years) " +
                "from Resolution resolution " +
                "inner join resolution.casefile casefile " +
                "left join resolution.conditions condition " +
                "where resolution.id = :id", AbstractResolutionReportDTO.class)
                .setParameter("id", id_resolution)
                .getResultList();
    }
}
