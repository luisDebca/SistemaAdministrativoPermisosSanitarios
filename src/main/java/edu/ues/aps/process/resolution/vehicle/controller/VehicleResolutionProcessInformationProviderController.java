package edu.ues.aps.process.resolution.vehicle.controller;

import edu.ues.aps.process.resolution.vehicle.service.VehicleResolutionFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/resolution/vehicle")
public class VehicleResolutionProcessInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleResolutionProcessInformationProviderController.class);

    private static final String RESOLUTIONS = "vehicle_resolution_process_list";

    private final VehicleResolutionFinderService finderService;

    public VehicleResolutionProcessInformationProviderController(VehicleResolutionFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showRequestForResolutionProcess(ModelMap model) {
        logger.info("GET:: Show all request for resolution process.");
        model.addAttribute("requests", finderService.findAll());
        return RESOLUTIONS;
    }

}
