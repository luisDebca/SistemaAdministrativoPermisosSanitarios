package edu.ues.aps.process.resolution.vehicle.service;

import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleDiplomaReportDTO;
import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionReportDTO;
import edu.ues.aps.process.resolution.vehicle.repository.VehicleDiplomaReportRepository;
import edu.ues.aps.process.resolution.vehicle.repository.VehicleResolutionReportRepository;
import edu.ues.aps.utilities.file.reader.FileReader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleReportService implements VehicleResolutionReportService, VehicleDiplomaReportService {

    private String fileContent;
    private String fileName;

    private final FileReader reader;
    private final VehicleResolutionReportRepository resolutionReportRepository;
    private final VehicleDiplomaReportRepository diplomaReportRepository;

    public VehicleReportService(FileReader reader, VehicleResolutionReportRepository resolutionReportRepository, VehicleDiplomaReportRepository diplomaReportRepository) {
        this.reader = reader;
        this.resolutionReportRepository = resolutionReportRepository;
        this.diplomaReportRepository = diplomaReportRepository;
    }

    @Override
    public List<AbstractVehicleDiplomaReportDTO> findDiploma(Long id_bunch, String signatoryName, String signatoryCharge) {
        List<AbstractVehicleDiplomaReportDTO> reportDTOS = diplomaReportRepository.findDiploma(id_bunch);
        for (AbstractVehicleDiplomaReportDTO dto: reportDTOS){
            dto.setSignatory_name(signatoryName);
            dto.setSignatory_charge(signatoryCharge);
        }
        return reportDTOS;
    }

    @Override
    public List<AbstractVehicleResolutionReportDTO> getResolution(Long id_bunch, String signatoryName, String signatoryCharge) {
        List<AbstractVehicleResolutionReportDTO> resolution = resolutionReportRepository.findResolution(id_bunch);
        for (AbstractVehicleResolutionReportDTO dto : resolution) {
            readFileContent(dto.getResolution_content());
            dto.setResolution_content(formatResolutionContent(dto));
            dto.setSignatory_name(signatoryName);
            dto.setSignatory_charge(signatoryCharge);
        }
        return resolution;
    }

    private String formatResolutionContent(AbstractVehicleResolutionReportDTO dto) {
        if (!fileContent.isEmpty()) {
            fileContent = fileContent.
                    replaceAll(".TRASPORTE", dto.getContent_type() + "S").
                    replaceAll(".CERTIFICACION", dto.getCertification());
        }
        return fileContent;
    }

    private void readFileContent(String fileName){
        if(!fileName.equals(this.fileName)){
            this.fileName = fileName;
            fileContent = reader.readFileContent(this.fileName);
        }
    }

}
