package edu.ues.aps.process.resolution.establishment.service;

import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentDiplomaDTO;
import edu.ues.aps.process.resolution.establishment.dto.EmptyEstablishmentDiplomaDTO;
import edu.ues.aps.process.resolution.establishment.repository.EstablishmentDiplomaReportRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional(readOnly = true)
public class EstablishmentDiplomaService implements EstablishmentDiplomaReportService{

    private final EstablishmentDiplomaReportRepository reportRepository;

    public EstablishmentDiplomaService(EstablishmentDiplomaReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }

    @Override
    public AbstractEstablishmentDiplomaDTO getReportDatasource(Long id_caseFile) {
        try {
            return reportRepository.getReportDatasource(id_caseFile);
        } catch (NoResultException e) {
            return new EmptyEstablishmentDiplomaDTO();
        }
    }
}
