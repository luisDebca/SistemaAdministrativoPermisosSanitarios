package edu.ues.aps.process.resolution.establishment.service;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.model.AbstractCondition;
import edu.ues.aps.process.resolution.base.repository.ConditionFinderRepository;
import edu.ues.aps.process.resolution.base.service.ConditionFinderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class EstablishmentConditionService implements ConditionFinderService {

    private final ConditionFinderRepository finderRepository;

    public EstablishmentConditionService(ConditionFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractCondition> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public List<AbstractCondition> findAll(CasefileSection section) {
        return finderRepository.findAll(section);
    }
}
