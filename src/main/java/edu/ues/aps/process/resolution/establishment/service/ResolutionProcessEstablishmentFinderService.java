package edu.ues.aps.process.resolution.establishment.service;

import edu.ues.aps.process.resolution.base.repository.ResolutionProcessRequestFinderRepository;
import edu.ues.aps.process.resolution.base.service.ResolutionProcessRequestFinderService;
import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentRequestResolutionDTO;
import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentResolutionFormDTO;
import edu.ues.aps.process.resolution.establishment.repository.ResolutionInformationProviderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ResolutionProcessEstablishmentFinderService implements ResolutionProcessRequestFinderService, ResolutionInformationProviderService {

    private final ResolutionInformationProviderRepository informationProviderRepository;
    private final ResolutionProcessRequestFinderRepository finderRepository;

    public ResolutionProcessEstablishmentFinderService(ResolutionProcessRequestFinderRepository finderRepository, ResolutionInformationProviderRepository informationProviderRepository) {
        this.finderRepository = finderRepository;
        this.informationProviderRepository = informationProviderRepository;
    }

    @Override
    public List<AbstractEstablishmentRequestResolutionDTO> findRequests() {
        return finderRepository.findRequestForCurrentProcess();
    }

    @Override
    public List<AbstractEstablishmentResolutionFormDTO> findAll(Long id_caseFile) {
        return informationProviderRepository.findAll(id_caseFile);
    }
}
