package edu.ues.aps.process.resolution.establishment.dto;

import edu.ues.aps.process.resolution.base.dto.ResolutionFormDTO;

public class EstablishmentResolutionFormDTO extends ResolutionFormDTO implements AbstractEstablishmentResolutionFormDTO {

    private String establishment_name;
    private String establishment_address;
    private String owner_name;

    public EstablishmentResolutionFormDTO(String ucsf, String memo, String inspector_name, String inspector_charge, String establishment_name, String establishment_address, String owner_name) {
        super(ucsf, memo, inspector_name, inspector_charge);
        this.establishment_name = establishment_name;
        this.establishment_address = establishment_address;
        this.owner_name = owner_name;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getOwner_name() {
        return owner_name;
    }

}
