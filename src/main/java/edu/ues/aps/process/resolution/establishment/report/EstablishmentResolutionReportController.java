package edu.ues.aps.process.resolution.establishment.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.resolution.base.dto.AbstractResolutionReportDTO;
import edu.ues.aps.process.resolution.base.service.ResolutionReportService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.List;

@Controller
@RequestMapping("/resolution/establishment")
public class EstablishmentResolutionReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(EstablishmentResolutionReportController.class);

    private static final String REPORT_NAME = "ResolutionEReport";
    private final ResolutionReportService reportService;
    private List<AbstractResolutionReportDTO> resolutionDTO;

    public EstablishmentResolutionReportController(ResolutionReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/resolution-report.pdf")
    public String generateResolutionReport(@RequestParam Long id_resolution, @RequestParam String signatory_name, @RequestParam String signatory_charge, ModelMap model) {
        logger.info("REPORT:: Generate resolution {} report", id_resolution);
        resolutionDTO = reportService.getReportDatasource(id_resolution);
        addSignatures(signatory_name, signatory_charge);
        addReportRequiredSources(resolutionDTO);
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }

    private void addSignatures(String name, String charge) {
        for (AbstractResolutionReportDTO dto : resolutionDTO) {
            dto.setSignatory_name(name);
            dto.setSignatory_charge(charge);
        }
    }
}
