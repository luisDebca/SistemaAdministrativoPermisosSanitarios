package edu.ues.aps.process.resolution.establishment.repository;

import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentDiplomaDTO;

import javax.persistence.NoResultException;

public interface EstablishmentDiplomaReportRepository {

    AbstractEstablishmentDiplomaDTO getReportDatasource(Long id_caseFile) throws NoResultException;
}
