package edu.ues.aps.process.resolution.establishment.controller;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import edu.ues.aps.process.resolution.base.model.Resolution;
import edu.ues.aps.process.resolution.base.service.ConditionFinderService;
import edu.ues.aps.process.resolution.establishment.service.ResolutionFinderService;
import edu.ues.aps.process.resolution.establishment.service.ResolutionPersistenceService;
import edu.ues.aps.process.resolution.establishment.service.ResolutionInformationProviderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/resolution/establishment")
public class EstablishmentResolutionPersistenceController {

    private static final Logger logger = LogManager.getLogger(EstablishmentResolutionPersistenceController.class);

    private static final String RESOLUTION_FORM = "establishment_resolution_form";

    private final ResolutionInformationProviderService informationProviderService;
    private final ResolutionPersistenceService resolutionPersistenceService;
    private final ResolutionFinderService resolutionFinderService;
    private final ConditionFinderService conditionFinderService;
    private final ProcessUserRegisterService registerService;

    public EstablishmentResolutionPersistenceController(ResolutionFinderService resolutionFinderService, ResolutionPersistenceService resolutionPersistenceService, ConditionFinderService conditionFinderService, ProcessUserRegisterService registerService, ResolutionInformationProviderService informationProviderService) {
        this.resolutionFinderService = resolutionFinderService;
        this.resolutionPersistenceService = resolutionPersistenceService;
        this.conditionFinderService = conditionFinderService;
        this.registerService = registerService;
        this.informationProviderService = informationProviderService;
    }

    @GetMapping("/{id_caseFile}/new")
    public String showResolutionForm(@PathVariable Long id_caseFile, ModelMap model, Principal principal) {
        logger.info("GET:: Show new resolution form for case file {}", id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.establishment.resolution.new");
        model.addAttribute("resolution", resolutionFinderService.getNewResolution(id_caseFile, CasefileSection.ESTABLISHMENT));
        model.addAttribute("conditions", conditionFinderService.findAll(CasefileSection.ESTABLISHMENT));
        model.addAttribute("info", informationProviderService.findAll(id_caseFile));
        return RESOLUTION_FORM;
    }

    @PostMapping("/{id_caseFile}/new")
    public String saveResolution(@PathVariable Long id_caseFile, @ModelAttribute Resolution resolution, Principal principal) {
        logger.info("POST:: Save resolution for case file {}", id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.establishment.resolution.save");
        resolutionPersistenceService.save(resolution, id_caseFile);
        return "redirect:/resolution/establishment/";
    }

    @GetMapping("/{id_caseFile}/edit")
    public String showEditResolutionForm(@PathVariable Long id_caseFile, ModelMap model, Principal principal) {
        logger.info("GET:: Show resolution edit form for case file {}", id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.establishment.resolution.edit");
        model.addAttribute("resolution", resolutionFinderService.findById(id_caseFile));
        model.addAttribute("conditions", conditionFinderService.findAll(CasefileSection.ESTABLISHMENT));
        model.addAttribute("info", informationProviderService.findAll(id_caseFile));
        return RESOLUTION_FORM;
    }

    @PostMapping("/{id_caseFile}/edit")
    public String updateResolution(@PathVariable Long id_caseFile, @ModelAttribute Resolution resolution, Principal principal) {
        logger.info("POST:: Update resolution for case file {}", id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.establishment.resolution.update");
        resolutionPersistenceService.update(resolution);
        return "redirect:/resolution/establishment/";
    }
}
