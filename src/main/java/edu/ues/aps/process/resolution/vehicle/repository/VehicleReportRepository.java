package edu.ues.aps.process.resolution.vehicle.repository;

import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleDiplomaReportDTO;
import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionReportDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleReportRepository implements VehicleResolutionReportRepository, VehicleDiplomaReportRepository {

    private final SessionFactory sessionFactory;

    public VehicleReportRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleDiplomaReportDTO> findDiploma(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.vehicle.dto.VehicleResolutionReportDTO(" +
                "concat(concat(concat(concat('06',cv.request_code),cv.request_number),'-'),cv.request_year)," +
                "concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year)," +
                "cv.certification_type,vehicle.license,vehicle.transported_content,vehicle.vehicle_type," +
                "concat(concat(concat(concat(vaddress.details,', '),vaddress.municipality.name),', '),vaddress.municipality.department.name)," +
                "owner.name,(select concat(concat(concat(concat(oaddress.details,', '),oaddress.municipality.name),', '),oaddress.municipality.department.name) from LegalEntity entity inner join entity.address oaddress where entity.id = owner.id)," +
                "cv.certification_duration_in_years,cv.certification_start_on)" +
                "from VehicleBunch bunch " +
                "inner join bunch.address vaddress " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.resolution resolution " +
                "inner join cv.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where bunch.id = :id " +
                "order by cv.id", AbstractVehicleDiplomaReportDTO.class)
                .setParameter("id",id_bunch)
                .getResultList();
    }

    @Override
    public List<AbstractVehicleResolutionReportDTO> findResolution(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.vehicle.dto.VehicleResolutionReportDTO(" +
                "concat(concat(concat(concat('06',cv.request_code),cv.request_number),'-'),cv.request_year)," +
                "concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year)," +
                "cv.certification_type,vehicle.license,vehicle.transported_content,vehicle.vehicle_type," +
                "concat(concat(concat(concat(vaddress.details,', '),vaddress.municipality.name),', '),vaddress.municipality.department.name)," +
                "owner.name,(select concat(concat(concat(concat(oaddress.details,', '),oaddress.municipality.name),', '),oaddress.municipality.department.name) from LegalEntity entity inner join entity.address oaddress where entity.id = owner.id)," +
                "cv.certification_duration_in_years,cv.certification_start_on,resolution.content, condition.condition)" +
                "from VehicleBunch bunch " +
                "inner join bunch.address vaddress " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.resolution resolution " +
                "inner join resolution.conditions condition " +
                "inner join cv.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where bunch.id = :id " +
                "order by cv.id", AbstractVehicleResolutionReportDTO.class)
                .setParameter("id",id_bunch)
                .getResultList();
    }
}
