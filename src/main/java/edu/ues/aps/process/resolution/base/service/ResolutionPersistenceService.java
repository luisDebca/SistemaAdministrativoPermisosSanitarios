package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.base.model.AbstractResolution;

public interface ResolutionPersistenceService {

    void save(AbstractResolution resolution, Long id_caseFile);

    void update(AbstractResolution resolution);
}
