package edu.ues.aps.process.resolution.vehicle.dto;

public interface AbstractVehicleResolutionFormDTO {

    Long getId_caseFile();

    String getNumber();

    void setNumber(String number);

    String getCode();

    String getYear();

    String getLicense();

    String getVehicleType();

    String getContentType();

    String getCertificationType();

    Boolean resolutionCreated();
}
