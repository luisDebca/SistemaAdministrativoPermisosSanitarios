package edu.ues.aps.process.resolution.base.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.resolution.base.service.MemorandumReportFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;

@Controller
@RequestMapping(value = "/memorandum")
public class MemorandumReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(MemorandumReportController.class);

    private static final String REPORT_NAME = "MemoForwardReport";

    private final MemorandumReportFinderService reportService;

    public MemorandumReportController(MemorandumReportFinderService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/memo-report.pdf")
    public String generateMemorandumReport(@RequestParam Long id_memo, ModelMap model) {
        logger.info("REPORT:: Generate memorandum {} report", id_memo);
        addReportRequiredSources(Collections.singletonList(reportService.findReportDataSource(id_memo)));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }
}
