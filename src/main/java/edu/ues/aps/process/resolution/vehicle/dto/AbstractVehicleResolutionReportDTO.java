package edu.ues.aps.process.resolution.vehicle.dto;

public interface AbstractVehicleResolutionReportDTO {

    String getRequest_number();

    String getResolution_number();

    String getCertification();

    String getVehicle_license();

    String getContent_type();

    String getVehicle_type();

    String getVehicle_address();

    String getOwner_name();

    String getOwner_address();

    String getDuration();

    String getExpiration();

    String getResolution_content();

    void setResolution_content(String resolution_content);

    String getResolution_conditions();

    void setResolution_condition(String resolution_condition);

    String getCreation_date();

    String getSignatory_name();

    void setSignatory_name(String signatory_name);

    String getSignatory_charge();

    void setSignatory_charge(String signatory_charge);
}
