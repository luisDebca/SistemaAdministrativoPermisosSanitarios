package edu.ues.aps.process.resolution.vehicle.repository;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionRequestDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleResolutionRequestRepository implements VehicleResolutionRequestFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleResolutionRequestRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleResolutionRequestDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.vehicle.dto.VehicleResolutionRequestDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date," +
                "(select count(memo) from Memorandum memo where memo.casefile.id = cv.id)," +
                "case when resolution is null then false else true end, resolution.resolution_create_on)" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "left join cv.resolution resolution " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehicleResolutionRequestDTO.class)
                .setParameter("id", ProcessIdentifier.REQUEST_RESOLUTION)
                .getResultList();
    }
}
