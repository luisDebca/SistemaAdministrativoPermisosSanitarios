package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.base.dto.AbstractMemorandumReportDTO;
import edu.ues.aps.process.resolution.base.dto.EmptyMemorandumReportDTO;
import edu.ues.aps.process.resolution.base.repository.MemorandumReportFinderRepository;
import edu.ues.aps.utilities.file.reader.FileReader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional(readOnly = true)
public class MemorandumReportService implements MemorandumReportFinderService {

    private final FileReader reader;
    private final MemorandumReportFinderRepository reportRepository;

    public MemorandumReportService(MemorandumReportFinderRepository reportRepository, FileReader reader) {
        this.reportRepository = reportRepository;
        this.reader = reader;
    }

    @Override
    public AbstractMemorandumReportDTO findReportDataSource(Long id_memo) {
        try {
            AbstractMemorandumReportDTO dataSource = reportRepository.findReportDataSource(id_memo);
            dataSource.setMemo_content(reader.readFileContent(dataSource.getMemo_content()));
            return dataSource;
        } catch (NoResultException e) {
            return new EmptyMemorandumReportDTO();
        }
    }
}
