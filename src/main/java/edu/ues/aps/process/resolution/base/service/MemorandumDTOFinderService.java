package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.base.dto.AbstractMemorandumDTO;

import java.util.List;

public interface MemorandumDTOFinderService {

    List<AbstractMemorandumDTO> findAll(Long id);
}
