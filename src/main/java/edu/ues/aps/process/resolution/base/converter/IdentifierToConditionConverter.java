package edu.ues.aps.process.resolution.base.converter;

import edu.ues.aps.process.resolution.base.model.Condition;
import edu.ues.aps.process.resolution.base.service.ConditionPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class IdentifierToConditionConverter implements Converter<String, Condition> {

    private static final Logger logger = LogManager.getLogger(IdentifierToConditionConverter.class);

    private final ConditionPersistenceService persistenceService;

    public IdentifierToConditionConverter(ConditionPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @Override
    public Condition convert(String source) {
        Long id_condition = Long.parseLong(source);
        Condition condition = (Condition) persistenceService.findById(id_condition);
        logger.info("CONVERTER:: Identifier to condition {}", condition.getId());
        return condition;
    }
}
