package edu.ues.aps.process.resolution.vehicle.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.resolution.vehicle.service.VehicleDiplomaReportService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/resolution/vehicle/bunch")
public class VehicleBunchDiplomaReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(VehicleBunchDiplomaReportController.class);

    private static final String REPORT_NAME = "DiplomaVReport";

    private final VehicleDiplomaReportService reportService;

    public VehicleBunchDiplomaReportController(VehicleDiplomaReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/diploma-report.pdf")
    public String generateReport(@RequestParam Long id_bunch, @RequestParam String signatory_name, @RequestParam String signatory_charge, ModelMap model){
        logger.info("GET:: Generate diploma for bunch {}",id_bunch);
        addReportRequiredSources(reportService.findDiploma(id_bunch,signatory_name,signatory_charge));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }
}
