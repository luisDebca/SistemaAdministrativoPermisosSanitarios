package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.base.dto.AbstractMemorandumReportDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class MemorandumReportRepository implements MemorandumReportFinderRepository {

    private final SessionFactory sessionFactory;

    public MemorandumReportRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractMemorandumReportDTO findReportDataSource(Long id_memo) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.base.dto.MemorandumReportDTO(" +
                "memo.number,memo.document_content,memo.create_on,memo.for_name,memo.for_charge," +
                "memo.from_name,memo.from_charge,memo.through_name,memo.through_charge) " +
                "from Memorandum memo " +
                "where memo.id = :id", AbstractMemorandumReportDTO.class)
                .setParameter("id", id_memo)
                .getSingleResult();
    }
}
