package edu.ues.aps.process.resolution.establishment.repository;

import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentDiplomaDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class EstablishmentDiplomaRepository implements EstablishmentDiplomaReportRepository{

    private final SessionFactory sessionFactory;

    public EstablishmentDiplomaRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractEstablishmentDiplomaDTO getReportDatasource(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.establishment.dto.EstablishmentDiplomaDTO(concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year), " +
                "concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year)," +
                "establishment.name,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.type_detail,owner.name,casefile.certification_duration_in_years,casefile.certification_start_on) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.address address " +
                "inner join establishment.owner owner " +
                "inner join casefile.resolution resolution " +
                "where casefile.id = :id",AbstractEstablishmentDiplomaDTO.class)
                .setParameter("id",id_caseFile)
                .getSingleResult();
    }
}
