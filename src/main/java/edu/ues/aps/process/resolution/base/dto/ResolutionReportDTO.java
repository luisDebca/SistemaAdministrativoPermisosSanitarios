package edu.ues.aps.process.resolution.base.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class ResolutionReportDTO implements AbstractResolutionReportDTO {

    private String request_number;
    private String resolution_number;
    private String resolution_content;
    private String resolution_conditions;
    private LocalDateTime creation_date;
    private Integer duration;
    private String signatory_name;
    private String signatory_charge;

    public ResolutionReportDTO(String request_number, String resolution_number, String resolution_content, String resolution_conditions, LocalDateTime creation_date, Integer duration) {
        this.request_number = request_number;
        this.resolution_number = resolution_number;
        this.resolution_content = resolution_content;
        this.resolution_conditions = resolution_conditions;
        this.creation_date = creation_date;
        this.duration = duration;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getResolution_number() {
        return resolution_number;
    }

    public String getResolution_conditions() {
        if (resolution_conditions == null)
            return "";
        return replaceDates(resolution_conditions);
    }

    public String getCreation_date() {
        return DatetimeUtil.parseDatetimeToVerboseDate(creation_date);
    }

    public String getResolution_content() {
        return resolution_content;
    }

    public void setResolution_content(String resolution_content) {
        this.resolution_content = resolution_content;
    }

    public String getSignatory_name() {
        return signatory_name;
    }

    public void setSignatory_name(String signatory_name) {
        this.signatory_name = signatory_name;
    }

    public String getSignatory_charge() {
        return signatory_charge;
    }

    public void setSignatory_charge(String signatory_charge) {
        this.signatory_charge = signatory_charge;
    }

    private String replaceDates(String baseString) {
        if (baseString.contains(".DURACION") || baseString.contains(".DURACI�N")) {
            if (duration > 1) {
                baseString = baseString.replaceAll(".DURACION", DatetimeUtil.getNumberSpanish(duration).toUpperCase() + " A�OS");
                baseString = baseString.replaceAll(".DURACI�N", DatetimeUtil.getNumberSpanish(duration).toUpperCase() + " A�OS");
            } else {
                baseString = baseString.replaceAll(".DURACION", "UN A�O");
                baseString = baseString.replaceAll(".DURACI�N", "UN A�O");
            }
        }
        if (baseString.contains(".FINAL") && creation_date != null) {
            baseString = baseString.replaceAll(".FINAL", DatetimeUtil.parseDateToVerboseDate(creation_date.toLocalDate().plusYears(duration)));
        }
        return baseString;
    }
}
