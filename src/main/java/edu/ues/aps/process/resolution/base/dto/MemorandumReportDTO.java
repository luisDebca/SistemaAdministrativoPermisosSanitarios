package edu.ues.aps.process.resolution.base.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class MemorandumReportDTO implements AbstractMemorandumReportDTO{

    private static final String MEMORANDUM_CODE_FORMAT = "-3000-DRSM-VP-";

    private String memo_code;
    private String memo_content;
    private LocalDateTime memo_creation_date;
    private String for_name;
    private String for_charge;
    private String from_name;
    private String from_charge;
    private String through_name;
    private String through_charge;

    public MemorandumReportDTO(String memo_code, String memo_content, LocalDateTime memo_creation_date, String for_name, String for_charge, String from_name, String from_charge, String through_name, String through_charge) {
        this.memo_code = memo_code;
        this.memo_content = memo_content;
        this.memo_creation_date = memo_creation_date;
        this.for_name = for_name;
        this.for_charge = for_charge;
        this.from_name = from_name;
        this.from_charge = from_charge;
        this.through_name = through_name;
        this.through_charge = through_charge;
    }

    public String getMemo_content() {
        return memo_content;
    }

    public void setMemo_content(String memo_content) {
        this.memo_content = memo_content;
    }

    public String getMemo_code() {
        if(memo_creation_date != null){
            return String.format("%d%s%s", memo_creation_date.getYear(), MEMORANDUM_CODE_FORMAT, memo_code);
        }else{
            return String.format("%d%s%s", LocalDate.now().getYear(), MEMORANDUM_CODE_FORMAT, memo_code);
        }
    }

    public String getMemo_creation_date() {
        if(memo_creation_date != null){
            return DatetimeUtil.parseDateToLongDateFormat(memo_creation_date.toLocalDate());
        }
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    public String getFor_name() {
        return for_name;
    }

    public String getFor_charge() {
        return for_charge;
    }

    public String getFrom_name() {
        return from_name;
    }

    public String getFrom_charge() {
        return from_charge;
    }

    public String getThrough_name() {
        return through_name;
    }

    public String getThrough_charge() {
        return through_charge;
    }
}
