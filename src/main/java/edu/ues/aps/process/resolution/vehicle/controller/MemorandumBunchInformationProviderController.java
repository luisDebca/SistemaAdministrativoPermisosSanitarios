package edu.ues.aps.process.resolution.vehicle.controller;

import edu.ues.aps.process.area.base.service.RequestNumberInformationService;
import edu.ues.aps.process.resolution.base.service.MemorandumDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/resolution/memorandum")
public class MemorandumBunchInformationProviderController {

    private static final Logger logger = LogManager.getLogger(MemorandumBunchInformationProviderController.class);

    private static final String MEMO_LIST = "memorandum_bunch_list";

    private final RequestNumberInformationService requestNumberService;
    private final MemorandumDTOFinderService finderService;

    public MemorandumBunchInformationProviderController(@Qualifier("memorandumBunchService") MemorandumDTOFinderService finderService, RequestNumberInformationService requestNumberService) {
        this.finderService = finderService;
        this.requestNumberService = requestNumberService;
    }

    @GetMapping("/bunch/{id_bunch}/all")
    public String showBunchMemorandumList(@PathVariable Long id_bunch, ModelMap model){
        logger.info("GET:: Show bunch {} memorandums.",id_bunch);
        model.addAttribute("request",requestNumberService.getFirst(id_bunch));
        model.addAttribute("memos",finderService.findAll(id_bunch));
        model.addAttribute("id_bunch",id_bunch);
        return MEMO_LIST;
    }
}
