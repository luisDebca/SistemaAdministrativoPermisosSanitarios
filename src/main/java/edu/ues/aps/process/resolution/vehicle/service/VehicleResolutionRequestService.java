package edu.ues.aps.process.resolution.vehicle.service;

import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionRequestDTO;
import edu.ues.aps.process.resolution.vehicle.repository.VehicleResolutionRequestFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleResolutionRequestService implements VehicleResolutionRequestFinderService {

    private final VehicleResolutionRequestFinderRepository finderRepository;

    public VehicleResolutionRequestService(VehicleResolutionRequestFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractVehicleResolutionRequestDTO> findAll() {
        return finderRepository.findAll();
    }
}
