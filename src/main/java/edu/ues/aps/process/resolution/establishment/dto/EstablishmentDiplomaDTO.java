package edu.ues.aps.process.resolution.establishment.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class EstablishmentDiplomaDTO implements AbstractEstablishmentDiplomaDTO{

    private String request_number;
    private String resolution_number;
    private String establishment_name;
    private String establishment_address;
    private String establishment_type;
    private String owner_name;
    private Integer certification_duration_in_years;
    private LocalDateTime certification_start;
    private String certification_expired;
    private String current_date;
    private String signatory_name;
    private String signatory_charge;

    public EstablishmentDiplomaDTO(String request_number, String resolution_number, String establishment_name, String establishment_address, String establishment_type, String owner_name, Integer certification_duration_in_years, LocalDateTime certification_start) {
        this.request_number = request_number;
        this.resolution_number = resolution_number;
        this.establishment_name = establishment_name;
        this.establishment_address = establishment_address;
        this.establishment_type = establishment_type;
        this.owner_name = owner_name;
        this.certification_duration_in_years = certification_duration_in_years;
        this.certification_start = certification_start;
    }

    @Override
    public String getRequest_number() {
        return request_number;
    }

    public String getResolution_number() {
        return resolution_number;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getEstablishment_type() {
        return establishment_type;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getCertification_duration_in_years() {
        if (certification_duration_in_years == 1) return "UN";
        return DatetimeUtil.getNumberSpanish(certification_duration_in_years).toUpperCase();
    }

    public String getCertification_expired() {
        if (certification_start == null || certification_duration_in_years == null)
            return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToVerboseDate(certification_start.toLocalDate()
                .plusYears(certification_duration_in_years.longValue())).toUpperCase();
    }

    public String getCurrent_date() {
        return DatetimeUtil.parseDateToVerboseDate(LocalDate.now()).toUpperCase();
    }

    public String getSignatory_name() {
        return signatory_name;
    }

    public void setSignatory_name(String signatory_name) {
        this.signatory_name = signatory_name;
    }

    public String getSignatory_charge() {
        return signatory_charge;
    }

    public void setSignatory_charge(String signatory_charge) {
        this.signatory_charge = signatory_charge;
    }

    public Boolean getEmpty_name() {
        if (establishment_name != null){
            if(establishment_name.equalsIgnoreCase("N/A") ||
                    establishment_name.contains("N/A") ||
                    establishment_name.isEmpty()){
                return false;
            }
        }
        return true;
    }
}
