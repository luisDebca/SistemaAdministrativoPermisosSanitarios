package edu.ues.aps.process.resolution.establishment.service;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.service.CaseFilePersistenceService;
import edu.ues.aps.process.resolution.base.model.AbstractResolution;
import edu.ues.aps.process.resolution.base.model.EmptyResolution;
import edu.ues.aps.process.resolution.base.model.Resolution;
import edu.ues.aps.process.resolution.establishment.repository.ResolutionFinderRepository;
import edu.ues.aps.process.resolution.establishment.repository.ResolutionValidatorRepository;
import edu.ues.aps.utilities.file.reader.FileReader;
import edu.ues.aps.utilities.file.writer.FileWriter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;

@Service
public class ResolutionService implements ResolutionPersistenceService, ResolutionFinderService, ResolutionValidatorService {

    private static final String RESOLUTION_PREFIX = "RESOLUTION";

    private final CaseFilePersistenceService caseFilePersistenceService;
    private final ResolutionFinderRepository resolutionFinderRepository;
    private final ResolutionValidatorRepository validatorRepository;
    private final FileWriter fileWriter;
    private final FileReader fileReader;

    public ResolutionService(CaseFilePersistenceService caseFilePersistenceService, ResolutionFinderRepository resolutionFinderRepository, FileWriter fileWriter, FileReader fileReader, ResolutionValidatorRepository validatorRepository) {
        this.caseFilePersistenceService = caseFilePersistenceService;
        this.resolutionFinderRepository = resolutionFinderRepository;
        this.fileWriter = fileWriter;
        this.fileReader = fileReader;
        this.validatorRepository = validatorRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractResolution findById(Long id_resolution) {
        try {
            AbstractResolution resolution = resolutionFinderRepository.findById(id_resolution);
            resolution.setContent(getResolutionFileContent(resolution.getContent()));
            return resolution;
        } catch (NoResultException e) {
            return new EmptyResolution();
        }
    }

    @Override
    @Transactional
    public AbstractResolution getNewResolution(Long id_caseFile, CasefileSection section) {
        AbstractResolution resolution = new Resolution();
        resolution.setResolution_code(resolutionFinderRepository.getCaseFileRequestCode(id_caseFile));
        resolution.setResolution_number(String.valueOf(resolutionFinderRepository.getResolutionMaxNumber(section) + 1));
        resolution.setResolution_year(String.valueOf(LocalDateTime.now().getYear() - 2000));
        return resolution;
    }

    @Override
    @Transactional
    public void save(AbstractResolution resolution, Long id_caseFile) {
        AbstractCasefile caseFile = caseFilePersistenceService.findById(id_caseFile);
        String file_name = saveResolutionContentIntoFile(resolution.getContent(), id_caseFile);
        resolution.setContent(file_name);
        resolution.setResolution_create_on(LocalDateTime.now());
        resolution.setDiploma_create_on(LocalDateTime.now());
        caseFile.addResolution((Resolution) resolution);
        caseFile.setCertification_start_on(LocalDateTime.now());
    }

    @Override
    @Transactional
    public void update(AbstractResolution draft) {
        AbstractResolution resolution = resolutionFinderRepository.findById(draft.getId());
        String file_name = saveResolutionContentIntoFile(draft.getContent(), draft.getId());
        resolution.setContent(file_name);
        if (validateResolutionNumber(draft.getId(), draft.getResolution_number(), draft.getResolution_code(), draft.getResolution_year())) {
            resolution.setResolution_number(draft.getResolution_number());
            resolution.setResolution_code(draft.getResolution_code());
            resolution.setResolution_year(draft.getResolution_year());
        }
    }

    private String saveResolutionContentIntoFile(String resolution_content, Long id_resolution) {
        return fileWriter.writeFlatName(RESOLUTION_PREFIX + id_resolution, resolution_content);
    }

    private String getResolutionFileContent(String file_name) {
        return fileReader.readFileContent(file_name);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean validateResolutionNumber(Long id_caseFile, String number, String code, String year) {
        if (validatorRepository.isUniqueResolutionNumber(id_caseFile, number, code, year) == 1) {
            return true;
        } else {
            return validatorRepository.isUniqueResolutionNumber(number, code, year) == 0;
        }
    }
}
