package edu.ues.aps.process.resolution.establishment.service;

import edu.ues.aps.process.resolution.base.model.AbstractResolution;

public interface ResolutionPersistenceService {

    void save(AbstractResolution resolution, Long id_caseFile);

    void update(AbstractResolution resolution);
}
