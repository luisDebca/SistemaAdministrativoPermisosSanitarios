package edu.ues.aps.process.resolution.vehicle.service;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.service.SingleCaseFileFinderService;
import edu.ues.aps.process.resolution.base.dto.AbstractResolutionFormDTO;
import edu.ues.aps.process.resolution.base.model.AbstractResolution;
import edu.ues.aps.process.resolution.base.model.EmptyResolution;
import edu.ues.aps.process.resolution.base.model.Resolution;
import edu.ues.aps.process.resolution.establishment.repository.ResolutionFinderRepository;
import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionFormDTO;
import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionRequestDTO;
import edu.ues.aps.process.resolution.vehicle.repository.VehicleResolutionFinderRepository;
import edu.ues.aps.process.resolution.vehicle.repository.VehicleResolutionInformationProviderRepository;
import edu.ues.aps.utilities.file.reader.FileReader;
import edu.ues.aps.utilities.file.writer.FileWriter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class VehicleResolutionService implements VehicleResolutionFinderService, VehicleResolutionInformationProviderService, VehicleResolutionPersistenceService {

    private static final String RESOLUTION_PREFIX = "RESOLUTION";

    private final VehicleResolutionInformationProviderRepository informationProviderResolution;
    private final VehicleBunchInformationService bunchInformationService;
    private final ResolutionFinderRepository resolutionFinderRepository;
    private final VehicleResolutionFinderRepository finderRepository;
    private final SingleCaseFileFinderService caseFileFinderService;
    private final FileWriter fileWriter;
    private final FileReader fileReader;

    public VehicleResolutionService(VehicleResolutionFinderRepository finderRepository, VehicleResolutionInformationProviderRepository informationProviderResolution, VehicleBunchInformationService bunchInformationService, SingleCaseFileFinderService caseFileFinderService, FileWriter fileWriter, FileReader fileReader, ResolutionFinderRepository resolutionFinderRepository) {
        this.informationProviderResolution = informationProviderResolution;
        this.resolutionFinderRepository = resolutionFinderRepository;
        this.bunchInformationService = bunchInformationService;
        this.caseFileFinderService = caseFileFinderService;
        this.finderRepository = finderRepository;
        this.fileWriter = fileWriter;
        this.fileReader = fileReader;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AbstractVehicleResolutionRequestDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    @Transactional
    public List<AbstractVehicleResolutionFormDTO> findContentResolution(Long id_bunch) {
        List<AbstractVehicleResolutionFormDTO> contentResolution = informationProviderResolution.findContentResolution(id_bunch);
        Long resolutionNumber = informationProviderResolution.getResolutionMaxNumber() + 1;
        for (AbstractVehicleResolutionFormDTO dto : contentResolution) {
            if (!dto.resolutionCreated())
                dto.setNumber(String.valueOf(resolutionNumber));
        }
        return contentResolution;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AbstractResolutionFormDTO> findResolutionInspection(Long id_bunch) {
        return informationProviderResolution.findResolutionInspection(id_bunch);
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractResolution find(Long id_bunch) {
        List<Long> allCaseFileId = bunchInformationService.findAllCaseFileId(id_bunch);
        if (!allCaseFileId.isEmpty()) {
            AbstractResolution resolution = resolutionFinderRepository.findById(allCaseFileId.get(0));
            resolution.setContent(fileReader.readFileContent(resolution.getContent()));
            return resolution;
        }
        return new EmptyResolution();
    }

    @Override
    @Transactional
    public void save(Long id_bunch, AbstractResolution draft) {
        for (Long id_caseFile : bunchInformationService.findAllCaseFileId(id_bunch)) {
            AbstractCasefile caseFile = caseFileFinderService.find(id_caseFile);
            Resolution resolution = getNewResolution();
            resolution.setResolution_code(caseFile.getRequest_code());
            resolution.setContent(writeResolutionContentFile(draft.getContent(), caseFile.getId()));
            resolution.setConditions(draft.getConditions());
            caseFile.addResolution(resolution);
            caseFile.setCertification_start_on(LocalDateTime.now());
        }
    }

    @Override
    @Transactional
    public void update(Long id_bunch, AbstractResolution draft) {
        for (Long id_caseFile : bunchInformationService.findAllCaseFileId(id_bunch)) {
            AbstractResolution resolution = resolutionFinderRepository.findById(id_caseFile);
            resolution.setConditions(draft.getConditions());
            resolution.setContent(writeResolutionContentFile(draft.getContent(), id_caseFile));
        }
    }

    private Resolution getNewResolution() {
        Resolution resolution = new Resolution();
        resolution.setResolution_create_on(LocalDateTime.now());
        resolution.setDiploma_create_on(LocalDateTime.now());
        resolution.setResolution_number(String.valueOf(informationProviderResolution.getResolutionMaxNumber() + 1));
        resolution.setResolution_year(String.valueOf(LocalDate.now().getYear() - 2000));
        return resolution;
    }

    private String writeResolutionContentFile(String content, Long id_resolution) {
        return fileWriter.writeFlatName(RESOLUTION_PREFIX + id_resolution, content);
    }

}
