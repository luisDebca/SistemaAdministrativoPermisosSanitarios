package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.model.AbstractResolution;

import javax.persistence.NoResultException;

public interface ResolutionFinderRepository {

    AbstractResolution findById(Long id_resolution) throws NoResultException;

    Long getResolutionMaxNumber(CasefileSection section);

    String getCaseFileRequestCode(Long id_caseFile);

}
