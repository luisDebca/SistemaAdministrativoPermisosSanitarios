package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.base.dto.AbstractMemorandumReportDTO;

public interface MemorandumReportFinderService {

    AbstractMemorandumReportDTO findReportDataSource(Long id_memo);
}
