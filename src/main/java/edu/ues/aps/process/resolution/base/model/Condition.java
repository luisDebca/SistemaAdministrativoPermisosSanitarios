package edu.ues.aps.process.resolution.base.model;

import edu.ues.aps.process.base.model.CasefileSection;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "CONDITIONS")
public class Condition implements AbstractCondition{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "condition_content", length = 2500, nullable = false)
    private String condition;

    @Enumerated(EnumType.STRING)
    @Column(name = "section", nullable = false, length = 14)
    private CasefileSection section;

    @ManyToMany(mappedBy = "conditions")
    private List<Resolution> resolutions = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCondition() {
        return condition;
    }

    @Override
    public void setCondition(String condition) {
        this.condition = condition;
    }

    @Override
    public CasefileSection getSection() {
        return section;
    }

    @Override
    public void setSection(CasefileSection section) {
        this.section = section;
    }

    @Override
    public List<Resolution> getResolutions() {
        return resolutions;
    }

    @Override
    public void setResolutions(List<Resolution> resolutions) {
        this.resolutions = resolutions;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Condition condition = (Condition) o;
        return Objects.equals(id, condition.id) &&
                section == condition.section;
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, section);
    }

    @Override
    public String toString() {
        return "Condition{" +
                "id=" + id +
                ", condition='" + condition + '\'' +
                ", section=" + section +
                '}';
    }
}
