package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.base.model.AbstractMemorandum;

import javax.persistence.NoResultException;

public interface MemorandumPersistenceRepository {

    AbstractMemorandum find(Long id_memorandum);

    AbstractMemorandum find(Long id_caseFile, String memoNumber) throws NoResultException;

    Long getMaxMemorandumNumber();

}
