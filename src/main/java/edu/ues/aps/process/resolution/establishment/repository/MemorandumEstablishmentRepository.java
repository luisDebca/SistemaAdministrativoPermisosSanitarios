package edu.ues.aps.process.resolution.establishment.repository;

import edu.ues.aps.process.resolution.base.dto.AbstractMemorandumDTO;
import edu.ues.aps.process.resolution.base.repository.MemorandumDTOFinderRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class MemorandumEstablishmentRepository implements MemorandumDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public MemorandumEstablishmentRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractMemorandumDTO> findAll(Long id) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.base.dto.MemorandumDTO(" +
                "memo.id,concat(concat(casefile.request_folder,'-'),casefile.request_year)," +
                "concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year)," +
                "memo.number,memo.for_name,memo.for_charge,memo.from_name,memo.from_charge,memo.create_on,memo.type) " +
                "from Casefile casefile " +
                "inner join casefile.memorandums memo " +
                "where casefile.id = :id", AbstractMemorandumDTO.class)
                .setParameter("id", id)
                .getResultList();
    }

}
