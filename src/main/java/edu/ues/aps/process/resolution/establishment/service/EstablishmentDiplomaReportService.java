package edu.ues.aps.process.resolution.establishment.service;

import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentDiplomaDTO;

public interface EstablishmentDiplomaReportService {

    AbstractEstablishmentDiplomaDTO getReportDatasource(Long id_caseFile);
}
