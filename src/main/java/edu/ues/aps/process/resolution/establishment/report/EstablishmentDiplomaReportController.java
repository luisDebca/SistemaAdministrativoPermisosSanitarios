package edu.ues.aps.process.resolution.establishment.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentDiplomaDTO;
import edu.ues.aps.process.resolution.establishment.service.EstablishmentDiplomaReportService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;

@Controller
@RequestMapping("/resolution/establishment")
public class EstablishmentDiplomaReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(EstablishmentDiplomaReportController.class);

    private static final String REPORT_NAME = "DiplomaEReport";

    private AbstractEstablishmentDiplomaDTO diplomaDTO;

    private final EstablishmentDiplomaReportService reportService;

    public EstablishmentDiplomaReportController(EstablishmentDiplomaReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/diploma-report.pdf")
    public String generateDiplomaReport(@RequestParam Long id_caseFile, @RequestParam String signatory_name, @RequestParam String signatory_charge, ModelMap model) {
        logger.info("REPORT:: Generate request {} diploma report",id_caseFile);
        diplomaDTO = reportService.getReportDatasource(id_caseFile);
        addSignatory(signatory_name, signatory_charge);
        addReportRequiredSources(Collections.singletonList(diplomaDTO));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }

    private void addSignatory(String name, String charge) {
        diplomaDTO.setSignatory_name(name);
        diplomaDTO.setSignatory_charge(charge);
    }

}
