package edu.ues.aps.process.resolution.base.dto;

public interface AbstractControlSheetDTO {

    String getMemorandum();

    String getRequest_number();

    String getResolution_number();

    String getSection();

    String getUCSF();

    String getOwner_name();

    String getTarget_name();

    String getTarget_address();

    String getP1_name();

    void setP1_name(String p1_name);

    String getP2_name();

    void setP2_name(String p2_name);

    String getP3_name();

    void setP3_name(String p3_name);

    String getP4_name();

    void setP4_name(String p4_name);

    String getP5_name();

    void setP5_name(String p5_name);
}
