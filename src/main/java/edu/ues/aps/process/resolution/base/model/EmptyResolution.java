package edu.ues.aps.process.resolution.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.EmptyCasefile;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class EmptyResolution implements AbstractResolution {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getResolution_number() {
        return "Resolution number not found";
    }

    @Override
    public void setResolution_number(String resolution_number) {

    }

    @Override
    public String getResolution_code() {
        return "Resolution code not found";
    }

    @Override
    public void setResolution_code(String resolution_code) {

    }

    @Override
    public String getResolution_year() {
        return "Resolution year not found";
    }

    @Override
    public void setResolution_year(String resolution_year) {

    }

    @Override
    public LocalDateTime getResolution_create_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public void setResolution_create_on(LocalDateTime resolution_create_on) {

    }

    @Override
    public LocalDateTime getDiploma_create_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public void setDiploma_create_on(LocalDateTime diploma_create_on) {

    }

    @Override
    public String getContent() {
        return "Resolution content not found";
    }

    @Override
    public void setContent(String content) {

    }

    @Override
    public List<Condition> getConditions() {
        return Collections.emptyList();
    }

    @Override
    public void setConditions(List<Condition> conditions) {

    }

    @Override
    public AbstractCasefile getCasefile() {
        return new EmptyCasefile();
    }

    @Override
    public void setCasefile(Casefile casefile) {

    }

    @Override
    public void addCondition(Condition condition) {

    }

    @Override
    public void removeCondition(Condition condition) {

    }

    @Override
    public String toString() {
        return "EmptyResolution{}";
    }
}
