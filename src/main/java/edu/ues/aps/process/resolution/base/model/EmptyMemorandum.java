package edu.ues.aps.process.resolution.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.EmptyCasefile;

import java.time.LocalDateTime;

public class EmptyMemorandum implements AbstractMemorandum {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getNumber() {
        return "Memorandum number not found";
    }

    @Override
    public MemorandumType getType() {
        return MemorandumType.INVALID;
    }

    @Override
    public String getFor_name() {
        return "Memorandum for name not found";
    }

    @Override
    public String getFor_charge() {
        return "Memorandum for charge not found";
    }

    @Override
    public String getFrom_name() {
        return "Memorandum from name not found";
    }

    @Override
    public String getFrom_charge() {
        return "Memorandum from charge not found";
    }

    @Override
    public String getThrough_name() {
        return "Memorandum through name not found";
    }

    @Override
    public String getThrough_charge() {
        return "Memorandum through charge not found";
    }

    @Override
    public LocalDateTime getCreate_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public AbstractCasefile getCasefile() {
        return new EmptyCasefile();
    }

    @Override
    public String getSubject() {
        return "Not found";
    }

    @Override
    public void setSubject(String subject) {

    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setNumber(String number) {

    }

    @Override
    public void setType(MemorandumType type) {

    }

    @Override
    public void setFor_name(String for_name) {

    }

    @Override
    public void setFor_charge(String for_charge) {

    }

    @Override
    public void setFrom_name(String from_name) {

    }

    @Override
    public void setFrom_charge(String from_charge) {

    }

    @Override
    public void setThrough_name(String through_name) {

    }

    @Override
    public void setThrough_charge(String through_charge) {

    }

    @Override
    public void setCreate_on(LocalDateTime create_on) {

    }

    @Override
    public void setCasefile(Casefile casefile) {

    }

    @Override
    public String getDocument_content() {
        return "Memorandum content not found";
    }

    @Override
    public void setDocument_content(String document_content) {

    }

    @Override
    public String toString() {
        return "EmptyMemorandum{}";
    }
}
