package edu.ues.aps.process.resolution.establishment.repository;

import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentResolutionFormDTO;

import java.util.List;

public interface ResolutionInformationProviderRepository {

    List<AbstractEstablishmentResolutionFormDTO> findAll(Long id_caseFile);
}
