package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.base.model.AbstractCondition;

public interface ConditionPersistenceRepository {

    AbstractCondition find(Long id_condition);

    void save(AbstractCondition condition);

    void delete(AbstractCondition condition);
}
