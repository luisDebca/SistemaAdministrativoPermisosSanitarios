package edu.ues.aps.process.resolution.establishment.repository;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.model.AbstractResolution;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

@Repository
public class ResolutionRepository implements ResolutionPersistenceRepository, ResolutionFinderRepository, ResolutionValidatorRepository {

    private final SessionFactory sessionFactory;

    public ResolutionRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void merge(AbstractResolution resolution) {
        sessionFactory.getCurrentSession().merge(resolution);
    }

    @Override
    public void saveOrUpdate(AbstractResolution resolution) {
        sessionFactory.getCurrentSession().save(resolution);
    }

    @Override
    public AbstractResolution findById(Long id_resolution) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select resolution " +
                "from Resolution resolution " +
                "join fetch resolution.conditions conditions " +
                "where resolution.id = :id", AbstractResolution.class)
                .setParameter("id", id_resolution)
                .getSingleResult();
    }

    @Override
    public Long getResolutionMaxNumber(CasefileSection section) {
        Long resolutionDefault = executeProcedure("getDefaultResolutionNumberForEstablishment");
        Long resolutionMax = executeProcedure("getMaxResolutionNumberForEstablishment");
        return resolutionDefault >= resolutionMax ? resolutionDefault : resolutionMax;
    }

    @Override
    public String getCaseFileRequestCode(Long id_caseFile) {
        return sessionFactory.getCurrentSession().createQuery("select casefile.request_code " +
                "from Casefile casefile " +
                "where casefile.id = :id", String.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }

    @Override
    public Long isUniqueResolutionNumber(Long id_caseFile, String number, String code, String year) {
        return sessionFactory.getCurrentSession().createQuery("select count(resolution) " +
                "from Resolution resolution " +
                "where resolution.id = :id " +
                "and  resolution.resolution_number = :number " +
                "and resolution.resolution_code = :code " +
                "and resolution.resolution_year = :year", Long.class)
                .setParameter("id", id_caseFile)
                .setParameter("number", number)
                .setParameter("code", code)
                .setParameter("year", year)
                .getSingleResult();
    }

    @Override
    public Long isUniqueResolutionNumber(String number, String code, String year) {
        return sessionFactory.getCurrentSession().createQuery("select count(resolution) " +
                "from Resolution resolution " +
                "where  resolution.resolution_number = :number " +
                "and resolution.resolution_code = :code " +
                "and resolution.resolution_year = :year", Long.class)
                .setParameter("number", number)
                .setParameter("code", code)
                .setParameter("year", year)
                .getSingleResult();
    }

    private Long executeProcedure(String procedure) {
        StoredProcedureQuery query = sessionFactory.getCurrentSession().createStoredProcedureQuery(procedure).
                registerStoredProcedureParameter("count", Long.class, ParameterMode.OUT);
        query.execute();
        if (query.getOutputParameterValue("count") == null) return 0L;
        return (Long) query.getOutputParameterValue("count");
    }
}
