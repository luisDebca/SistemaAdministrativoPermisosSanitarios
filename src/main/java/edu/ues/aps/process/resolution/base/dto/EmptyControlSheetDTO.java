package edu.ues.aps.process.resolution.base.dto;

import java.time.LocalDate;

public class EmptyControlSheetDTO implements AbstractControlSheetDTO {

    @Override
    public String getMemorandum() {
        return LocalDate.now().getYear() + "-3000-DRSM-VP-0";
    }

    @Override
    public String getRequest_number() {
        return "Request number not found";
    }

    @Override
    public String getResolution_number() {
        return "Resolution number not found";
    }

    @Override
    public String getSection() {
        return "Section not found";
    }

    @Override
    public String getUCSF() {
        return "Ucsf not found";
    }

    @Override
    public String getOwner_name() {
        return "Owner not found";
    }

    @Override
    public String getTarget_name() {
        return "Name not found";
    }

    @Override
    public String getTarget_address() {
        return "Address not found";
    }

    @Override
    public String getP1_name() {
        return "";
    }

    @Override
    public void setP1_name(String p1_name) {

    }

    @Override
    public String getP2_name() {
        return "";
    }

    @Override
    public void setP2_name(String p2_name) {

    }

    @Override
    public String getP3_name() {
        return "";
    }

    @Override
    public void setP3_name(String p3_name) {

    }

    @Override
    public String getP4_name() {
        return "";
    }

    @Override
    public void setP4_name(String p4_name) {

    }

    @Override
    public String getP5_name() {
        return "";
    }

    @Override
    public void setP5_name(String p5_name) {

    }
}
