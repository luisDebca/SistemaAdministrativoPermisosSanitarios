package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.service.CaseFilePersistenceService;
import edu.ues.aps.process.resolution.base.model.AbstractMemorandum;
import edu.ues.aps.process.resolution.base.model.Memorandum;
import edu.ues.aps.process.resolution.base.repository.MemorandumPersistenceRepository;
import edu.ues.aps.utilities.file.reader.FileReader;
import edu.ues.aps.utilities.file.writer.FileWriter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class MemorandumService implements MemorandumPersistenceService {

    private static final String MEMORANDUM_FILE_PREFIX = "MEMO";

    private final CaseFilePersistenceService persistenceService;
    private final MemorandumPersistenceRepository persistenceRepository;
    private final FileWriter fileWriter;
    private final FileReader fileReader;

    public MemorandumService(MemorandumPersistenceRepository persistenceRepository, FileWriter fileWriter, FileReader fileReader, CaseFilePersistenceService persistenceService) {
        this.persistenceRepository = persistenceRepository;
        this.fileWriter = fileWriter;
        this.fileReader = fileReader;
        this.persistenceService = persistenceService;
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractMemorandum find(Long id_memo) {
        AbstractMemorandum memorandum = persistenceRepository.find(id_memo);
        memorandum.setDocument_content(getMemorandumFileContent(memorandum.getDocument_content()));
        return memorandum;
    }

    @Override
    @Transactional
    public void add(AbstractMemorandum memo, Long id_caseFile) {
        AbstractCasefile caseFile = persistenceService.findById(id_caseFile);
        String memo_number = String.valueOf(persistenceRepository.getMaxMemorandumNumber() + 1L);
        String file_name = saveMemorandumContentIntoFile(memo.getDocument_content(), memo_number);
        memo.setNumber(memo_number);
        memo.setCreate_on(LocalDateTime.now());
        memo.setDocument_content(file_name);
        caseFile.addMemorandum((Memorandum) memo);
        persistenceService.merge(caseFile);
    }

    @Override
    @Transactional
    public void update(AbstractMemorandum memo) {
        String file_name = saveMemorandumContentIntoFile(memo.getDocument_content(), memo.getNumber());
        AbstractMemorandum persistentMemo = persistenceRepository.find(memo.getId());
        persistentMemo.setDocument_content(file_name);
        persistentMemo.setFor_name(memo.getFor_name());
        persistentMemo.setFor_charge(memo.getFor_charge());
        persistentMemo.setFrom_name(memo.getFrom_name());
        persistentMemo.setFrom_charge(memo.getFrom_charge());
        persistentMemo.setThrough_name(memo.getThrough_name());
        persistentMemo.setThrough_charge(memo.getThrough_charge());
        persistentMemo.setType(memo.getType());
    }

    @Override
    @Transactional
    public void remove(Long id_memo, Long id_caseFile) {
        AbstractCasefile caseFile = persistenceService.findById(id_caseFile);
        caseFile.removeMemorandum((Memorandum) persistenceRepository.find(id_memo));
    }

    private String saveMemorandumContentIntoFile(String memo_content, String memo_number) {
        String file_name = MEMORANDUM_FILE_PREFIX + memo_number;
        return fileWriter.writeFlatName(file_name, memo_content);
    }

    private String getMemorandumFileContent(String file_name) {
        return fileReader.readFileContent(file_name);
    }
}
