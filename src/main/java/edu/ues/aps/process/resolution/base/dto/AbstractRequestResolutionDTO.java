package edu.ues.aps.process.resolution.base.dto;

public interface AbstractRequestResolutionDTO {

    Long getId_casefile();

    Long getId_target();

    Long getId_owner();

    String getCasefile_code();

    String getTarget_name();

    String getTarget_address();

    String getTarget_type();

    String getOwner_name();

    Long getMemorandum_count();

    boolean isResolution_created();
}
