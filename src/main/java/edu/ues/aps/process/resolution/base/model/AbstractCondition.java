package edu.ues.aps.process.resolution.base.model;

import edu.ues.aps.process.base.model.CasefileSection;

import java.util.List;

public interface AbstractCondition {

    Long getId();

    void setId(Long id);

    String getCondition();

    void setCondition(String condition);

    CasefileSection getSection();

    void setSection(CasefileSection section);

    List<Resolution> getResolutions();

    void setResolutions(List<Resolution> resolutions);
}
