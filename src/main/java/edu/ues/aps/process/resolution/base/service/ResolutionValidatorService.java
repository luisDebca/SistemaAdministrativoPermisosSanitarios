package edu.ues.aps.process.resolution.base.service;

public interface ResolutionValidatorService {

    Boolean validateResolutionNumber(Long id_caseFile, String number, String code, String year);

}
