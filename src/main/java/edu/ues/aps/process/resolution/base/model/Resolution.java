package edu.ues.aps.process.resolution.base.model;

import edu.ues.aps.process.base.model.Casefile;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "RESOLUTION")
public class Resolution implements AbstractResolution {

    @Id
    private Long id;

    @Column(name = "resolution_number", length = 9, nullable = false)
    private String resolution_number;

    @Column(name = "resolution_code", length = 5, nullable = false)
    private String resolution_code;

    @Column(name = "resolution_year", length = 4, nullable = false)
    private String resolution_year;

    @Column(name = "resolution_create_on")
    private LocalDateTime resolution_create_on;

    @Column(name = "diploma_create_on")
    private LocalDateTime diploma_create_on;

    @Column(name = "content",length = 50, nullable = false)
    private String content;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    private Casefile casefile;

    @ManyToMany(cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(name = "RESOLUTION_CONDITIONS",
            joinColumns = @JoinColumn(name = "resolution_id"),
            inverseJoinColumns = @JoinColumn(name = "condition_id"))
    private List<Condition> conditions = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getResolution_number() {
        return resolution_number;
    }

    @Override
    public void setResolution_number(String resolution_number) {
        this.resolution_number = resolution_number;
    }

    @Override
    public String getResolution_code() {
        return resolution_code;
    }

    @Override
    public void setResolution_code(String resolution_code) {
        this.resolution_code = resolution_code;
    }

    @Override
    public String getResolution_year() {
        return resolution_year;
    }

    @Override
    public void setResolution_year(String resolution_year) {
        this.resolution_year = resolution_year;
    }

    @Override
    public LocalDateTime getResolution_create_on() {
        return resolution_create_on;
    }

    @Override
    public void setResolution_create_on(LocalDateTime resolution_create_on) {
        this.resolution_create_on = resolution_create_on;
    }

    @Override
    public LocalDateTime getDiploma_create_on() {
        return diploma_create_on;
    }

    @Override
    public void setDiploma_create_on(LocalDateTime diploma_create_on) {
        this.diploma_create_on = diploma_create_on;
    }

    @Override
    public String getContent() {
        return content;
    }

    @Override
    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public Casefile getCasefile() {
        return casefile;
    }

    @Override
    public void setCasefile(Casefile casefile) {
        this.casefile = casefile;
    }

    @Override
    public List<Condition> getConditions() {
        return conditions;
    }

    @Override
    public void setConditions(List<Condition> conditions) {
        this.conditions = conditions;
    }

    public void addCondition(Condition condition){
        conditions.add(condition);
        condition.getResolutions().add(this);
    }

    public void removeCondition(Condition condition){
        conditions.remove(condition);
        condition.getResolutions().remove(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Resolution that = (Resolution) o;
        return Objects.equals(resolution_number, that.resolution_number) &&
                Objects.equals(resolution_code, that.resolution_code) &&
                Objects.equals(resolution_year, that.resolution_year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(resolution_number, resolution_code, resolution_year);
    }

    @Override
    public String toString() {
        return "Resolution{" +
                "id=" + id +
                ", resolution_number='" + resolution_number + '\'' +
                ", resolution_code='" + resolution_code + '\'' +
                ", resolution_year='" + resolution_year + '\'' +
                ", resolution_create_on=" + resolution_create_on +
                ", diploma_create_on=" + diploma_create_on +
                '}';
    }
}
