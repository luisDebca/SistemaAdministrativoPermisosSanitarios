package edu.ues.aps.process.resolution.vehicle.repository;

import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionRequestDTO;

import java.util.List;

public interface VehicleResolutionRequestFinderRepository {

    List<AbstractVehicleResolutionRequestDTO> findAll();
}
