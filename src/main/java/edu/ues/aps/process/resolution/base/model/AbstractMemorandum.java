package edu.ues.aps.process.resolution.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;

import java.time.LocalDateTime;

public interface AbstractMemorandum {

    Long getId();

    void setId(Long id);

    String getNumber();

    void setNumber(String number);

    MemorandumType getType();

    void setType(MemorandumType type);

    String getFor_name();

    void setFor_name(String for_name);

    String getFor_charge();

    void setFor_charge(String for_charge);

    String getFrom_name();

    void setFrom_name(String from_name);

    String getFrom_charge();

    void setFrom_charge(String from_charge);

    String getThrough_name();

    void setThrough_name(String through_name);

    String getThrough_charge();

    void setThrough_charge(String through_charge);

    String getSubject();

    void setSubject(String subject);

    LocalDateTime getCreate_on();

    void setCreate_on(LocalDateTime create_on);

    AbstractCasefile getCasefile();

    void setCasefile(Casefile casefile);

    String getDocument_content();

    void setDocument_content(String document_content);
}
