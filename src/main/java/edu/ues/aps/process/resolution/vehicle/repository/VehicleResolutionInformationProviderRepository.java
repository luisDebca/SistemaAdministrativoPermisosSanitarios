package edu.ues.aps.process.resolution.vehicle.repository;

import edu.ues.aps.process.resolution.base.dto.AbstractResolutionFormDTO;
import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionFormDTO;

import java.util.List;

public interface VehicleResolutionInformationProviderRepository {

    List<AbstractVehicleResolutionFormDTO> findContentResolution(Long id_bunch);

    List<AbstractResolutionFormDTO> findResolutionInspection(Long id_bunch);

    Long getResolutionMaxNumber();
}
