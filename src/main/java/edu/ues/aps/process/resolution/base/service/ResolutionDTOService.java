package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.base.dto.AbstractResolutionReportDTO;
import edu.ues.aps.process.resolution.base.repository.ResolutionReportRepository;
import edu.ues.aps.utilities.file.reader.FileReader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ResolutionDTOService implements ResolutionReportService{

    private final FileReader fileReader;
    private final ResolutionReportRepository reportRepository;

    public ResolutionDTOService(ResolutionReportRepository reportRepository, FileReader fileReader) {
        this.reportRepository = reportRepository;
        this.fileReader = fileReader;
    }

    @Override
    public List<AbstractResolutionReportDTO> getReportDatasource(Long id_resolution) {
        List<AbstractResolutionReportDTO> datasource = reportRepository.findReportDatasource(id_resolution);
        if(!datasource.isEmpty()){
            String file_content = fileReader.readFileContent(datasource.get(0).getResolution_content());
            for(AbstractResolutionReportDTO dto: datasource){
                dto.setResolution_content(file_content);
            }
        }
        return datasource;
    }
}
