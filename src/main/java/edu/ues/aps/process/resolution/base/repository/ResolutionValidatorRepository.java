package edu.ues.aps.process.resolution.base.repository;

public interface ResolutionValidatorRepository {

    Long isUniqueResolutionNumber(Long id_caseFile, String number, String code, String year);

    Long isUniqueResolutionNumber(String number, String code, String year);
}
