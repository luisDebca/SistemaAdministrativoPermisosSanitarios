package edu.ues.aps.process.resolution.establishment.repository;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.resolution.base.repository.ResolutionProcessRequestFinderRepository;
import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentRequestResolutionDTO;
import edu.ues.aps.process.resolution.establishment.dto.AbstractEstablishmentResolutionFormDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ResolutionProcessEstablishmentFinderRepository implements ResolutionProcessRequestFinderRepository, ResolutionInformationProviderRepository {

    private final SessionFactory sessionFactory;

    public ResolutionProcessEstablishmentFinderRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractEstablishmentRequestResolutionDTO> findRequestForCurrentProcess() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.establishment.dto.EstablishmentRequestResolutionDTO(ce.id ,owner.id,establishment.id,concat(concat(ce.request_folder,'-'),ce.request_year),concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year)," +
                "establishment.name,establishment.type_detail,etype.type,section.type,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,ucsf.name,sibasi.zone,ce.certification_type,ce.creation_date,owner.name," +
                "(select count(memo) from Memorandum memo where memo.casefile.id = ce.id)," +
                "case when resolution is null then false else true end," +
                "concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year)," +
                "resolution.resolution_create_on)" +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "left join establishment.type etype " +
                "left join etype.section section " +
                "left join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "left join ce.resolution resolution " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id", AbstractEstablishmentRequestResolutionDTO.class)
                .setParameter("id", ProcessIdentifier.REQUEST_RESOLUTION)
                .getResultList();
    }

    @Override
    public List<AbstractEstablishmentResolutionFormDTO> findAll(Long id_caseFile) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.establishment.dto.EstablishmentResolutionFormDTO(" +
                "ucsf.name,inspection.memorandum_number,inspection.technical_name,inspection.technical_charge," +
                "establishment.name,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name),owner.name) " +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "inner join establishment.owner owner " +
                "left join establishment.address address " +
                "left join ce.ucsf ucsf " +
                "inner join ce.tracing tracing " +
                "inner join tracing.inspections inspection " +
                "where ce.id = :id", AbstractEstablishmentResolutionFormDTO.class)
                .setParameter("id", id_caseFile)
                .getResultList();
    }
}
