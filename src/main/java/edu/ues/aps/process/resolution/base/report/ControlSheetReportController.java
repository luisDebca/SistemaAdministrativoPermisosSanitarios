package edu.ues.aps.process.resolution.base.report;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.resolution.base.dto.AbstractControlSheetDTO;
import edu.ues.aps.process.resolution.base.service.ControlSheetReportService;
import org.apache.commons.lang.ArrayUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.io.IOException;
import java.util.Collections;

@Controller
@RequestMapping("/resolution/establishment")
public class ControlSheetReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(ControlSheetReportController.class);

    private static final String REPORT_NAME = "ControlSheetEReport";
    private final ControlSheetReportService reportService;
    private AbstractControlSheetDTO controlSheetDTO;
    private ControlSheetAuthorizedName[] selected_names = new ControlSheetAuthorizedName[5];

    public ControlSheetReportController(ControlSheetReportService reportService) {
        this.reportService = reportService;
    }

    @GetMapping("/control-sheet-report.pdf")
    public String generateControlSheetReport(@RequestParam Long id_caseFile, @RequestParam String selected_names_json, ModelMap model) {
        logger.info("REPORT:: Generate control sheet report for case file {}");
        controlSheetDTO = reportService.getReportDatasource(id_caseFile);
        fillSelectedNamesFromJSON(selected_names_json);
        putSelectedNameIntoReportDatasource();
        addReportRequiredSources(Collections.singletonList(controlSheetDTO));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }

    private void fillSelectedNamesFromJSON(String json) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
            selected_names = objectMapper.readValue(json, ControlSheetAuthorizedName[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void putSelectedNameIntoReportDatasource() {
        if (ArrayUtils.isNotEmpty(selected_names)) {
            if (ArrayUtils.getLength(selected_names) >= 1) {
                controlSheetDTO.setP1_name(selected_names[0].getName());
            }
            if (ArrayUtils.getLength(selected_names) >= 2) {
                controlSheetDTO.setP2_name(selected_names[1].getName());
            }
            if (ArrayUtils.getLength(selected_names) >= 3) {
                controlSheetDTO.setP3_name(selected_names[2].getName());
            }
            if (ArrayUtils.getLength(selected_names) >= 4) {
                controlSheetDTO.setP4_name(selected_names[3].getName());
            }
            if (ArrayUtils.getLength(selected_names) >= 5) {
                controlSheetDTO.setP5_name(selected_names[4].getName());
            }
        }
    }
}
