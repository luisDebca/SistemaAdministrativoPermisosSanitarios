package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.dto.AbstractConditionDTO;
import edu.ues.aps.process.resolution.base.dto.EmptyConditionDTO;
import edu.ues.aps.process.resolution.base.repository.ConditionDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ConditionDTOService implements ConditionDTOFinderService {

    private final ConditionDTOFinderRepository finderRepository;

    public ConditionDTOService(ConditionDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractConditionDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public List<AbstractConditionDTO> findAll(CasefileSection section) {
        return finderRepository.findAll(section);
    }

    @Override
    public AbstractConditionDTO find(Long id_condition) {
        try {
            return finderRepository.find(id_condition);
        } catch (NoResultException e) {
            return new EmptyConditionDTO();
        }
    }
}
