package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.model.AbstractCondition;

import javax.persistence.NoResultException;
import java.util.List;

public interface ConditionFinderRepository {

    AbstractCondition findById(Long id_condition) throws NoResultException;

    List<AbstractCondition> findAll();

    List<AbstractCondition> findAll(CasefileSection section);
}
