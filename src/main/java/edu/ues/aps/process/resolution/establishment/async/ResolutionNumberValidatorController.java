package edu.ues.aps.process.resolution.establishment.async;

import edu.ues.aps.process.resolution.establishment.service.ResolutionValidatorService;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.Locale;

@Controller
@RequestMapping("/resolution/validator")
public class ResolutionNumberValidatorController {

    private final MessageSource messageSource;
    private final ResolutionValidatorService validatorService;

    public ResolutionNumberValidatorController(ResolutionValidatorService validatorService, MessageSource messageSource) {
        this.validatorService = validatorService;
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/resolution_number", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateResolutionNumber(@RequestParam Long id_caseFile, @RequestParam String number, @RequestParam String code, @RequestParam String year) {
        return validatorService.validateResolutionNumber(id_caseFile, number, code, year) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("resolution.number", new String[]{String.format("06%s%s-%s", code, number, year)}, Locale.getDefault()));
    }
}
