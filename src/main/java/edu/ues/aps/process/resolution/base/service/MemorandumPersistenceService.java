package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.base.model.AbstractMemorandum;

public interface MemorandumPersistenceService {

    AbstractMemorandum find(Long id_memo);

    void add(AbstractMemorandum memo, Long id_caseFile);

    void update(AbstractMemorandum memo);

    void remove(Long id_memo, Long id_caseFile);
}
