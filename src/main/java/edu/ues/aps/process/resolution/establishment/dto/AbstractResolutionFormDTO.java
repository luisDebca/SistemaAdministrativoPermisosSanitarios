package edu.ues.aps.process.resolution.establishment.dto;

public interface AbstractResolutionFormDTO {

    String getEstablishment_name();

    String getEstablishment_address();

    String getOwner_name();

    String getUcsf();

    String getMemo();

    String getInspector_name();

    String getInspector_charge();
}
