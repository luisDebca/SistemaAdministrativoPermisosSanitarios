package edu.ues.aps.process.resolution.base.controller;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ResolutionsProcessIndexController {

    private static final Logger logger = LogManager.getLogger(ResolutionsProcessIndexController.class);

    private static final String REQUIREMENT_PROCESS_INDEX = "request_resolution_index";

    @GetMapping({"/resolution/index","/resolution/"})
    public String showIndex(ModelMap model){
        logger.info("GET:: Show resolutions index");
        model.addAttribute("process", ProcessIdentifier.REQUEST_RESOLUTION);
        return REQUIREMENT_PROCESS_INDEX;
    }

}
