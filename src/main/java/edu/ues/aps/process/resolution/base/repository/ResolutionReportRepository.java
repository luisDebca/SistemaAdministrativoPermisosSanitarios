package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.resolution.base.dto.AbstractResolutionReportDTO;

import java.util.List;

public interface ResolutionReportRepository {

    List<AbstractResolutionReportDTO> findReportDatasource(Long id_resolution);
}
