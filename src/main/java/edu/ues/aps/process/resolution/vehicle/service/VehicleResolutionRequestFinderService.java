package edu.ues.aps.process.resolution.vehicle.service;

import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionRequestDTO;

import java.util.List;

public interface VehicleResolutionRequestFinderService {

    List<AbstractVehicleResolutionRequestDTO> findAll();
}
