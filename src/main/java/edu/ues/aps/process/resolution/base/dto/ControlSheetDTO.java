package edu.ues.aps.process.resolution.base.dto;

import edu.ues.aps.process.base.model.CasefileSection;

import java.time.LocalDateTime;

public class ControlSheetDTO implements AbstractControlSheetDTO{

    private static final String MEMORANDUM_NUMBER_FORMAT = "-3000-DRSM-VP-";

    private String memorandum;
    private LocalDateTime memorandum_date;
    private String request_number;
    private String resolution_number;
    private CasefileSection section;
    private String ucsf;
    private String owner_name;
    private String target_name;
    private String target_address;
    private String p1_name;
    private String p2_name;
    private String p3_name;
    private String p4_name;
    private String p5_name;

    public ControlSheetDTO(String memorandum, LocalDateTime memorandum_date,String request_number, String resolution_number, CasefileSection section, String ucsf, String owner_name, String target_name, String target_address) {
        this.memorandum = memorandum;
        this.memorandum_date = memorandum_date;
        this.request_number = request_number;
        this.resolution_number = resolution_number;
        this.section = section;
        this.ucsf = ucsf;
        this.owner_name = owner_name;
        this.target_name = target_name;
        this.target_address = target_address;
    }

    public String getMemorandum() {
        if(memorandum_date != null){
            return memorandum_date.getYear() + MEMORANDUM_NUMBER_FORMAT + memorandum;
        }
        return memorandum;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getResolution_number() {
        return resolution_number;
    }

    public String getSection() {
        if(section != null){
            return section.name();
        }
        return CasefileSection.INVALID.name();
    }

    public String getUCSF() {
        return ucsf;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getTarget_name() {
        return target_name;
    }

    public String getTarget_address() {
        return target_address;
    }

    public String getP1_name() {
        if(p1_name == null){
            return "";
        }
        return p1_name;
    }

    public void setP1_name(String p1_name) {
        this.p1_name = p1_name;
    }

    public String getP2_name() {
        if(p2_name == null){
            return "";
        }
        return p2_name;
    }

    public void setP2_name(String p2_name) {
        this.p2_name = p2_name;
    }

    public String getP3_name() {
        if(p3_name == null){
            return "";
        }
        return p3_name;
    }

    public void setP3_name(String p3_name) {
        this.p3_name = p3_name;
    }

    public String getP4_name() {
        if(p4_name == null){
            return "";
        }
        return p4_name;
    }

    public void setP4_name(String p4_name) {
        this.p4_name = p4_name;
    }

    public String getP5_name() {
        if(p5_name == null){
            return "";
        }
        return p5_name;
    }

    public void setP5_name(String p5_name) {
        this.p5_name = p5_name;
    }
}
