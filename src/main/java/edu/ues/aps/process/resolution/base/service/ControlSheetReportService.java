package edu.ues.aps.process.resolution.base.service;

import edu.ues.aps.process.resolution.base.dto.AbstractControlSheetDTO;

public interface ControlSheetReportService {

    AbstractControlSheetDTO getReportDatasource(Long id_caseFile);
}