package edu.ues.aps.process.resolution.vehicle.service;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.service.SingleCaseFileFinderService;
import edu.ues.aps.process.resolution.base.dto.AbstractMemorandumDTO;
import edu.ues.aps.process.resolution.base.model.AbstractMemorandum;
import edu.ues.aps.process.resolution.base.model.EmptyMemorandum;
import edu.ues.aps.process.resolution.base.model.Memorandum;
import edu.ues.aps.process.resolution.base.repository.MemorandumDTOFinderRepository;
import edu.ues.aps.process.resolution.base.repository.MemorandumPersistenceRepository;
import edu.ues.aps.process.resolution.base.service.MemorandumDTOFinderService;
import edu.ues.aps.utilities.file.reader.FileReader;
import edu.ues.aps.utilities.file.writer.FileWriter;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;
import java.util.List;

@Service
public class MemorandumBunchService implements MemorandumDTOFinderService, MemorandumBunchPersistenceService {

    private static final String MEMORANDUM_FILE_PREFIX = "MEMO";

    private final MemorandumPersistenceRepository persistenceRepository;
    private final VehicleBunchInformationService informationService;
    private final SingleCaseFileFinderService caseFileFinderService;
    private final MemorandumDTOFinderRepository finderRepository;
    private final FileWriter fileWriter;
    private final FileReader fileReader;

    public MemorandumBunchService(@Qualifier("memorandumBunchRepository") MemorandumDTOFinderRepository finderRepository, VehicleBunchInformationService informationService, SingleCaseFileFinderService caseFileFinderService, MemorandumPersistenceRepository persistenceRepository, FileReader fileReader, FileWriter fileWriter) {
        this.finderRepository = finderRepository;
        this.informationService = informationService;
        this.caseFileFinderService = caseFileFinderService;
        this.persistenceRepository = persistenceRepository;
        this.fileReader = fileReader;
        this.fileWriter = fileWriter;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AbstractMemorandumDTO> findAll(Long id_bunch) {
        return finderRepository.findAll(id_bunch);
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractMemorandum find(Long id_bunch, String memoNumber) {
        try {
            List<Long> allCaseFileId = informationService.findAllCaseFileId(id_bunch);
            if (allCaseFileId.isEmpty()) return new EmptyMemorandum();
            AbstractMemorandum memorandum = persistenceRepository.find(allCaseFileId.get(0), memoNumber);
            memorandum.setDocument_content(fileReader.readFileContent(memorandum.getDocument_content()));
            return memorandum;
        } catch (NoResultException e) {
            return new EmptyMemorandum();
        }
    }

    @Override
    @Transactional
    public void addAll(AbstractMemorandum memo, Long id_bunch) {
        String memoNumber = String.valueOf(persistenceRepository.getMaxMemorandumNumber() + 1L);
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            Memorandum copy = getMemoCopy(memo);
            copy.setNumber(memoNumber);
            copy.setDocument_content(saveMemoContent(memo.getDocument_content(), memoNumber, id_caseFile));
            caseFileFinderService.find(id_caseFile).addMemorandum(copy);
        }
    }

    @Override
    @Transactional
    public void updateAll(AbstractMemorandum draft, Long id_bunch) {
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            AbstractMemorandum memorandum = persistenceRepository.find(id_caseFile, draft.getNumber());
            memorandum.setFor_name(draft.getFor_name());
            memorandum.setFor_charge(draft.getFor_charge());
            memorandum.setFrom_name(draft.getFrom_name());
            memorandum.setFrom_charge(draft.getFrom_charge());
            memorandum.setThrough_name(draft.getThrough_name());
            memorandum.setThrough_charge(draft.getThrough_charge());
            memorandum.setType(draft.getType());
            memorandum.setDocument_content(saveMemoContent(draft.getDocument_content(), draft.getNumber(), id_caseFile));
        }
    }

    @Override
    @Transactional
    public void removeAll(String memoNumber, Long id_bunch) {
        for (Long id_CaseFile : informationService.findAllCaseFileId(id_bunch)) {
            AbstractCasefile caseFile = caseFileFinderService.find(id_CaseFile);
            caseFile.removeMemorandum((Memorandum) persistenceRepository.find(id_CaseFile, memoNumber));
        }
    }

    private String saveMemoContent(String content, String number, Long id) {
        String fileName = String.format("%s%s_%d", MEMORANDUM_FILE_PREFIX, number, id);
        return fileWriter.writeFlatName(fileName, content);
    }

    private Memorandum getMemoCopy(AbstractMemorandum memo) {
        Memorandum copy = new Memorandum();
        copy.setFor_name(memo.getFor_name());
        copy.setFor_charge(memo.getFor_charge());
        copy.setFrom_name(memo.getFrom_name());
        copy.setFrom_charge(memo.getFrom_charge());
        copy.setThrough_name(memo.getThrough_name());
        copy.setThrough_charge(memo.getThrough_charge());
        copy.setDocument_content(memo.getDocument_content());
        copy.setType(memo.getType());
        copy.setCreate_on(LocalDateTime.now());
        return copy;
    }
}
