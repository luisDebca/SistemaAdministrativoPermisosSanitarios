package edu.ues.aps.process.resolution.establishment.controller;

import edu.ues.aps.process.resolution.base.service.ResolutionProcessRequestFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/resolution/establishment")
public class RequestProcessInformationProviderController {

    private static final Logger logger = LogManager.getLogger(RequestProcessInformationProviderController.class);

    private static final String RESOLUTION_PROCESS = "request_resolution_process_list";

    private final ResolutionProcessRequestFinderService finderService;

    public RequestProcessInformationProviderController(ResolutionProcessRequestFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping({"/","/list"})
    public String showRequestForResolutionProcess(ModelMap model){
        logger.info("GET:: Show requests for resolution process");
        model.addAttribute("requests", finderService.findRequests());
        return RESOLUTION_PROCESS;
    }
}
