package edu.ues.aps.process.resolution.vehicle.repository;

import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionReportDTO;

import java.util.List;

public interface VehicleResolutionReportRepository {

    List<AbstractVehicleResolutionReportDTO> findResolution(Long id_bunch);

}
