package edu.ues.aps.process.resolution.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;

import java.time.LocalDateTime;
import java.util.List;

public interface AbstractResolution {

    Long getId();

    void setId(Long id);

    String getResolution_number();

    void setResolution_number(String resolution_number);

    String getResolution_code();

    void setResolution_code(String resolution_code);

    String getResolution_year();

    void setResolution_year(String resolution_year);

    LocalDateTime getResolution_create_on();

    void setResolution_create_on(LocalDateTime resolution_create_on);

    LocalDateTime getDiploma_create_on();

    void setDiploma_create_on(LocalDateTime diploma_create_on);

    String getContent();

    void setContent(String content);

    List<Condition> getConditions();

    void setConditions(List<Condition> conditions);

    AbstractCasefile getCasefile();

    void setCasefile(Casefile casefile);

    void addCondition(Condition condition);

    void removeCondition(Condition condition);
}
