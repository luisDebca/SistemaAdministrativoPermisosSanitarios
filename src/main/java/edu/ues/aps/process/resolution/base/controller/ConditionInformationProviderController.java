package edu.ues.aps.process.resolution.base.controller;

import edu.ues.aps.process.resolution.base.service.ConditionDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class ConditionInformationProviderController {

    private static final Logger logger = LogManager.getLogger(ConditionInformationProviderController.class);

    private static final String CONDITION_LIST = "condition_list";

    private final ConditionDTOFinderService finderService;

    public ConditionInformationProviderController(ConditionDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/resolution-condition/list")
    public String showConditionList(ModelMap model) {
        logger.info("GET:: Show resolution condition list");
        model.addAttribute("conditions", finderService.findAll());
        return CONDITION_LIST;
    }

}
