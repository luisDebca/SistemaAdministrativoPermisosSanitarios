package edu.ues.aps.process.resolution.vehicle.service;

import edu.ues.aps.process.resolution.base.model.AbstractResolution;

public interface VehicleResolutionPersistenceService {

    AbstractResolution find(Long id_bunch);

    void save(Long id_bunch, AbstractResolution draft);

    void update(Long id_bunch, AbstractResolution draft);

}
