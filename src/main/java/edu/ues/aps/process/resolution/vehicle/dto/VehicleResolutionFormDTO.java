package edu.ues.aps.process.resolution.vehicle.dto;

import edu.ues.aps.process.area.vehicle.model.TransportedContentType;
import edu.ues.aps.process.base.model.CertificationType;

public class VehicleResolutionFormDTO implements AbstractVehicleResolutionFormDTO {

    private Long id_caseFile;
    private String number;
    private String code;
    private String year;
    private String license;
    private String vehicleType;
    private TransportedContentType contentType;
    private CertificationType certificationType;
    private Boolean resolutionCreated;

    public VehicleResolutionFormDTO(Long id_caseFile, CertificationType certificationType,Boolean resolutionCreated, String number, String code, String year, String license, TransportedContentType contentType, String vehicleType) {
        this.id_caseFile = id_caseFile;
        this.certificationType = certificationType;
        this.resolutionCreated = resolutionCreated;
        this.number = number;
        this.code = code;
        this.year = year;
        this.license = license;
        this.contentType = contentType;
        this.vehicleType = vehicleType;
    }

    public Long getId_caseFile() {
        return id_caseFile;
    }

    public String getNumber() {
        return number;
    }

    public void setNumber(String number) {
        this.number = number;
    }

    public String getCode() {
        return code;
    }

    public String getYear() {
        return year;
    }

    public String getLicense() {
        return license;
    }

    public String getVehicleType() {
        return vehicleType;
    }

    public String getContentType() {
        if (contentType == null)
            return TransportedContentType.INVALID.getTransportedContentType();
        return contentType.getTransportedContentType();
    }

    public String getCertificationType() {
        if (certificationType == null)
            return CertificationType.INVALID.getCertificationType();
        return certificationType.getCertificationType();
    }

    @Override
    public Boolean resolutionCreated() {
        return resolutionCreated;
    }
}
