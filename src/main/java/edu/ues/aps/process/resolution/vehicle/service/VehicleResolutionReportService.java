package edu.ues.aps.process.resolution.vehicle.service;

import edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionReportDTO;

import java.util.List;

public interface VehicleResolutionReportService {

    List<AbstractVehicleResolutionReportDTO> getResolution(Long id_bunch, String signatoryName, String signatoryCharge);
}
