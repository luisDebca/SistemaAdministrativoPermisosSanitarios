package edu.ues.aps.process.resolution.base.dto;

public interface AbstractMemorandumDTO {

    Long getId_caseFile();

    void setId_caseFile(Long id_caseFile);

    Long getId_memo();

    String getFolder_number();

    String getRequest_number();

    String getMemo_number();

    String getNumber();

    String getFor_name();

    String getFrom_name();

    String getCreate_on();

    String getType();

    String getFor_charge();

    String getFrom_charge();

    String getThrough_name();

    String getThrough_charge();
}
