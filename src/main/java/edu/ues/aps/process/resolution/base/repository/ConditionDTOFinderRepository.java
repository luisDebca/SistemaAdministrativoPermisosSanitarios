package edu.ues.aps.process.resolution.base.repository;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.dto.AbstractConditionDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface ConditionDTOFinderRepository {

    List<AbstractConditionDTO> findAll();

    List<AbstractConditionDTO> findAll(CasefileSection section);

    AbstractConditionDTO find(Long id_condition) throws NoResultException;

}
