package edu.ues.aps.process.resolution.establishment.service;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.resolution.base.model.AbstractResolution;

public interface ResolutionFinderService {

    AbstractResolution findById(Long id_resolution);

    AbstractResolution getNewResolution(Long id_caseFile, CasefileSection section);

}
