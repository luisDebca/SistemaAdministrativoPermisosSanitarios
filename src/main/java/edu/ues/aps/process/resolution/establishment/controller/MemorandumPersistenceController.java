package edu.ues.aps.process.resolution.establishment.controller;

import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import edu.ues.aps.process.resolution.base.model.Memorandum;
import edu.ues.aps.process.resolution.base.model.MemorandumType;
import edu.ues.aps.process.resolution.base.service.MemorandumPersistenceService;
import edu.ues.aps.process.settings.model.ReportFormatType;
import edu.ues.aps.process.settings.service.ReportFormatContentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/resolution/memorandum")
public class MemorandumPersistenceController {

    private static final Logger logger = LogManager.getLogger(MemorandumPersistenceController.class);

    private static final String MEMORANDUM_FORM = "memorandum_form";

    private final MemorandumPersistenceService persistenceService;
    private final ProcessUserRegisterService registerService;

    @Autowired
    private ReportFormatContentService formatService;

    public MemorandumPersistenceController(MemorandumPersistenceService persistenceService, ProcessUserRegisterService registerService) {
        this.persistenceService = persistenceService;
        this.registerService = registerService;
    }

    @GetMapping("/{id_caseFile}/new")
    public String showNewMemorandumForm(@PathVariable Long id_caseFile, ModelMap model, Principal principal) {
        logger.info("GET:: Show new memorandum form for case file {}", id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.memorandum.new");
        model.addAttribute("memo", new Memorandum());
        model.addAttribute("id_caseFile", id_caseFile);
        model.addAttribute("type", MemorandumType.values());
        model.addAttribute("formats",formatService.getFormats(ReportFormatType.MEMO));
        return MEMORANDUM_FORM;
    }

    @PostMapping("/{id_caseFile}/new")
    public String saveMemorandum(@PathVariable Long id_caseFile, @ModelAttribute Memorandum memorandum, Principal principal) {
        logger.info("POST:: Save new memorandum for case file {}", id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.memorandum.save");
        persistenceService.add(memorandum, id_caseFile);
        return String.format("redirect:/resolution/memorandum/%d/list", id_caseFile);
    }

    @GetMapping("/{id_caseFile}/edit/{id_memo}")
    public String showMemorandumEditForm(@PathVariable Long id_caseFile, @PathVariable Long id_memo, ModelMap model, Principal principal) {
        logger.info("GET:: Show edit memorandum {} form for case file {}", id_memo, id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.memorandum.edit");
        model.addAttribute("memo", persistenceService.find(id_memo));
        model.addAttribute("id_caseFile", id_caseFile);
        model.addAttribute("formats",formatService.getFormats(ReportFormatType.MEMO));
        return MEMORANDUM_FORM;
    }

    @PostMapping("/{id_caseFile}/edit/{id_memo}")
    public String updateMemorandum(@PathVariable Long id_caseFile, @PathVariable Long id_memo,
                                   @ModelAttribute Memorandum memorandum, Principal principal) {
        logger.info("POST:: Update memorandum {} form for case file {}", id_memo, id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.memorandum.update");
        persistenceService.update(memorandum);
        return String.format("redirect:/resolution/memorandum/%d/list", id_caseFile);
    }

    @GetMapping("/{id_caseFile}/delete/{id_memo}")
    public String deleteMemorandum(@PathVariable Long id_caseFile, @PathVariable Long id_memo, Principal principal) {
        logger.info("GET:: Delete memorandum {} from case file {}", id_memo, id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.memorandum.remove");
        persistenceService.remove(id_memo, id_caseFile);
        return String.format("redirect:/resolution/memorandum/%d/list", id_caseFile);
    }
}
