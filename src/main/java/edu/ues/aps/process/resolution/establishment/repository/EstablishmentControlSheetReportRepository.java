package edu.ues.aps.process.resolution.establishment.repository;

import edu.ues.aps.process.resolution.base.dto.AbstractControlSheetDTO;
import edu.ues.aps.process.resolution.base.model.MemorandumType;
import edu.ues.aps.process.resolution.base.repository.ControlSheetReportRepository;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class EstablishmentControlSheetReportRepository implements ControlSheetReportRepository {

    private final SessionFactory sessionFactory;

    public EstablishmentControlSheetReportRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractControlSheetDTO getReportDatasource(Long id_caseFile) throws NoResultException {
        List<AbstractControlSheetDTO> resultList = sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.resolution.base.dto.ControlSheetDTO(" +
                "memo.number,memo.create_on,concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year)," +
                "concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year)," +
                "casefile.section,ucsf.name, owner.name, establishment.name," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join casefile.memorandums memo " +
                "inner join casefile.resolution resolution " +
                "left join casefile.ucsf ucsf " +
                "where casefile.id = :id ", AbstractControlSheetDTO.class)
                .setParameter("id", id_caseFile)
                .getResultList();
        return resultList.get(resultList.size() - 1);
    }
}
