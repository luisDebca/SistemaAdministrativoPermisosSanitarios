package edu.ues.aps.process.legal.vehicle.service;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.dto.EmptyLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.vehicle.dto.AbstractVehicleLegalReview;
import edu.ues.aps.process.legal.vehicle.repository.VehicleLegalReviewDTOFinderRepository;
import edu.ues.aps.process.legal.vehicle.repository.VehicleLegalReviewFinderRepository;
import edu.ues.aps.utilities.file.reader.FileReader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleLegalReviewDTOService implements VehicleLegalReviewFinderService, VehicleLegalReviewDTOFinderService {

    private final VehicleLegalReviewDTOFinderRepository dtoFinderRepository;
    private final VehicleLegalReviewFinderRepository finderRepository;
    private final FileReader reader;

    public VehicleLegalReviewDTOService(VehicleLegalReviewFinderRepository finderRepository, VehicleLegalReviewDTOFinderRepository dtoFinderRepository, FileReader reader) {
        this.finderRepository = finderRepository;
        this.dtoFinderRepository = dtoFinderRepository;
        this.reader = reader;
    }

    @Override
    public List<AbstractVehicleLegalReview> findRequestForFirstReview() {
        return finderRepository.findAllForFistReview();
    }

    @Override
    public List<AbstractVehicleLegalReview> findRequestForFirstResolution() {
        return finderRepository.findAllForFistResolution();
    }

    @Override
    public List<AbstractVehicleLegalReview> findRequestForSecondReview() {
        return finderRepository.findAllForSecondReview();
    }

    @Override
    public AbstractLegalReviewFormDTO find(Long id_bunch, LegalReviewType type) {
        try {
            AbstractLegalReviewFormDTO dto = dtoFinderRepository.find(id_bunch, type);
            dto.setDocument_content(reader.readFileContent(dto.getDocument_content()));
            return dto;
        } catch (NoResultException e) {
            return new EmptyLegalReviewFormDTO();
        }
    }
}
