package edu.ues.aps.process.legal.establishment.repository;

import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.establishment.dto.AbstractEstablishmentLegalRequestReviewDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LegalSecondReviewDTOFinderRepository implements LegalSecondReviewDTORepository{

    private final SessionFactory sessionFactory;

    public LegalSecondReviewDTOFinderRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractEstablishmentLegalRequestReviewDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.legal.establishment.dto.EstablishmentLegalRequestReviewDTO(ce.id ,owner.id,establishment.id,concat(concat(ce.request_folder,'-'),ce.request_year),concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year)," +
                "establishment.name,establishment.type_detail,etype.type,section.type,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,ucsf.name,sibasi.zone,ce.certification_type,ce.creation_date,owner.name," +
                "(select review.result_type from LegalReview review where review.casefile.id = ce.id and review.review_type = :dictum)," +
                "(select review.result_type from LegalReview review where review.casefile.id = ce.id and review.review_type = :resolution)," +
                "(select case when op is null then false else true end from LegalReview op where op.casefile.id = ce.id and op.review_type = :opinion)," +
                "(select op.result_type from LegalReview op where op.casefile.id = ce.id and op.review_type = :opinion)," +
                "concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year))" +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "left join establishment.type etype " +
                "left join etype.section section " +
                "left join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join ce.resolution resolution " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id", AbstractEstablishmentLegalRequestReviewDTO.class)
                .setParameter("id", ProcessIdentifier.SECOND_LEGAL_REVIEW)
                .setParameter("dictum", LegalReviewType.DICTUM)
                .setParameter("resolution", LegalReviewType.RESOLUTION)
                .setParameter("opinion", LegalReviewType.OPINION)
                .getResultList();
    }

}
