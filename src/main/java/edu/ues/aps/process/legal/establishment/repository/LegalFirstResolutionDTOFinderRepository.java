package edu.ues.aps.process.legal.establishment.repository;

import edu.ues.aps.process.legal.establishment.dto.AbstractEstablishmentLegalRequestReviewDTO;

import java.util.List;

public interface LegalFirstResolutionDTOFinderRepository {

    List<AbstractEstablishmentLegalRequestReviewDTO> findAll();
}
