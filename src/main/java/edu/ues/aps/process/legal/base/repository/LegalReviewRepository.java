package edu.ues.aps.process.legal.base.repository;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.legal.base.model.AbstractLegalReview;
import edu.ues.aps.process.legal.base.model.LegalReview;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

@Repository
public class LegalReviewRepository implements LegalReviewPersistenceRepository, LegalReviewFinderRepository {

    private SessionFactory sessionFactory;
    private AbstractCasefile caseFile;

    public LegalReviewRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public AbstractLegalReview findReviewById(Long id_caseFile, LegalReviewType reviewType) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select review " +
                "from LegalReview review " +
                "where review.casefile.id = :id " +
                "and review.review_type = :review_type", AbstractLegalReview.class)
                .setParameter("id", id_caseFile)
                .setParameter("review_type", reviewType)
                .getSingleResult();
    }

    @Override
    public Long getMaxLegalMemorandumNumber() {
        Long memoMax = executeProcedure("getMaxLegalMemorandumNumber");
        Long memoDefault = executeProcedure("getDefaultLegalMemorandumNumber");
        return memoDefault >= memoMax ? memoDefault : memoMax;
    }

    private Long executeProcedure(String procedure) {
        StoredProcedureQuery query = sessionFactory.getCurrentSession().createStoredProcedureQuery(procedure).
                registerStoredProcedureParameter("count", Long.class, ParameterMode.OUT);
        query.execute();
        if (query.getOutputParameterValue("count") != null) {
            return (Long) query.getOutputParameterValue("count");
        } else {
            return 0L;
        }
    }

    @Override
    public void saveReview(Long id_casefile, LegalReview review) {
        findCaseFile(id_casefile);
        if (isLegalReviewTypeUnique(review.getReview_type()))
            caseFile.addLegalReview(review);
        sessionFactory.getCurrentSession().saveOrUpdate(caseFile);

    }

    @Override
    public void updateReview(AbstractLegalReview review) {
        sessionFactory.getCurrentSession().saveOrUpdate(review);
    }

    private void findCaseFile(Long id_caseFile) {
        caseFile = sessionFactory.getCurrentSession().createQuery("select casefile " +
                "from Casefile casefile " +
                "left join fetch casefile.legalReviewls " +
                "where casefile.id = :id", AbstractCasefile.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }

    private boolean isLegalReviewTypeUnique(LegalReviewType type) {
        for (LegalReview review : caseFile.getLegalReviewls()) {
            if (review.getReview_type().equals(type))
                return false;
        }
        return true;
    }
}
