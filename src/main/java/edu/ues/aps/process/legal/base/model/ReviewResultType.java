package edu.ues.aps.process.legal.base.model;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public enum ReviewResultType {

    ADMISSIBILITY("ADMISIBILIDAD"),
    INADMISSIBILITY("INADMISIBILIDAD"),
    PREVENTION("PREVENCIÓN"),
    INVALID("INVALIDO");

    private String reviewResultType;

    ReviewResultType(String type) {
        this.reviewResultType = type;
    }

    public String getReviewResultType() {
        return reviewResultType;
    }

    public static Map<String,String> asMap(){
        return Arrays.stream(values()).filter(e -> !e.equals(INVALID)).collect(Collectors.toMap(ReviewResultType::name,ReviewResultType::getReviewResultType));
    }
}
