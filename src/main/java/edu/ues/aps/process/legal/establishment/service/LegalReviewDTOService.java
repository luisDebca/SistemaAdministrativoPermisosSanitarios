package edu.ues.aps.process.legal.establishment.service;

import edu.ues.aps.process.legal.establishment.dto.AbstractEstablishmentLegalRequestReviewDTO;
import edu.ues.aps.process.legal.establishment.repository.LegalSecondReviewDTORepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class LegalReviewDTOService implements LegalSecondReviewDTOService {

    private final LegalSecondReviewDTORepository dtoRepository;

    public LegalReviewDTOService(LegalSecondReviewDTORepository dtoRepository) {
        this.dtoRepository = dtoRepository;
    }

    @Override
    public List<AbstractEstablishmentLegalRequestReviewDTO> find() {
        return dtoRepository.findAll();
    }
}
