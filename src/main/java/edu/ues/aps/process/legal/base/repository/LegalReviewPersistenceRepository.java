package edu.ues.aps.process.legal.base.repository;

import edu.ues.aps.process.legal.base.model.AbstractLegalReview;
import edu.ues.aps.process.legal.base.model.LegalReview;

public interface LegalReviewPersistenceRepository {

    void saveReview(Long id_casefile, LegalReview review);

    void updateReview(AbstractLegalReview review);

}
