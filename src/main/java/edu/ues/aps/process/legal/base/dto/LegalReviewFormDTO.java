package edu.ues.aps.process.legal.base.dto;

import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;

public class LegalReviewFormDTO implements AbstractLegalReviewFormDTO {

    private Long id_casefile;
    private Long id_review;
    private LegalReviewType review_type;
    private ReviewResultType result_type;
    private String document_content;
    private String to_name;
    private String to_charge;
    private String from_name;
    private String from_charge;

    public LegalReviewFormDTO() {
    }

    public LegalReviewFormDTO(LegalReviewType review_type) {
        this.review_type = review_type;
    }

    public LegalReviewFormDTO(Long id_review, ReviewResultType result_type, String document_content) {
        this.id_review = id_review;
        this.result_type = result_type;
        this.document_content = document_content;
    }

    public LegalReviewFormDTO(Long id_review, ReviewResultType result_type, String document_content, String to_name, String to_charge, String from_name, String from_charge) {
        this.id_review = id_review;
        this.result_type = result_type;
        this.document_content = document_content;
        this.to_name = to_name;
        this.to_charge = to_charge;
        this.from_name = from_name;
        this.from_charge = from_charge;
    }

    @Override
    public Long getId_casefile() {
        return id_casefile;
    }

    @Override
    public void setId_casefile(Long id_casefile) {
        this.id_casefile = id_casefile;
    }

    @Override
    public Long getId_review() {
        return id_review;
    }

    @Override
    public void setId_review(Long id_review) {
        this.id_review = id_review;
    }

    @Override
    public LegalReviewType getReview_type() {
        return review_type;
    }

    public void setReview_type(LegalReviewType review_type) {
        this.review_type = review_type;
    }

    @Override
    public ReviewResultType getResult_type() {
        return result_type;
    }

    @Override
    public void setResult_type(ReviewResultType result_type) {
        this.result_type = result_type;
    }

    @Override
    public String getDocument_content() {
        return document_content;
    }

    @Override
    public void setDocument_content(String document_content) {
        this.document_content = document_content;
    }

    public String getTo_name() {
        return to_name;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    public String getTo_charge() {
        return to_charge;
    }

    public void setTo_charge(String to_charge) {
        this.to_charge = to_charge;
    }

    public String getFrom_name() {
        return from_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    public String getFrom_charge() {
        return from_charge;
    }

    public void setFrom_charge(String from_charge) {
        this.from_charge = from_charge;
    }
}
