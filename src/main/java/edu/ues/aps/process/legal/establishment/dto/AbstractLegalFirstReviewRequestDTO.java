package edu.ues.aps.process.legal.establishment.dto;

import java.util.List;

public interface AbstractLegalFirstReviewRequestDTO {

    Long getId_casefile();

    void setId_casefile(Long id_casefile);

    Long getId_establishment();

    Long getId_owner();

    String getFolder_number();

    String getEstablishment_name();

    String getEstablishment_type();

    String getEstablishment_section();

    String getEstablishment_address();

    String getEstablishment_nit();

    String getOwner_name();

    int getCasefile_certification_duration_in_years();

    String getCasefile_certification_type();

    String getDocument_acceptance_created_on();

    String getDocument_created_on();

    String getUcsf_name();

    String getSibasi_name();

    String getLegal_review_result_type();

    String getLegal_review_created_on();

    List<String> getDocument_observations();

    void setDocument_observations(List<String> document_observations);

    String getBefore_delay_date();

    String getEstablishment_type_detail();

    String getLegal_document();

    void setLegal_document(String legal_document);

    String getResolution_result_type();

    String getResolution_created_on();

    String getResolution_document();
}
