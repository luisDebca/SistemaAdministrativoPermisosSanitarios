package edu.ues.aps.process.legal.vehicle.service;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.area.vehicle.service.VehicleBunchRelationshipService;
import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;
import edu.ues.aps.process.base.model.LegalEntity;
import edu.ues.aps.process.base.model.NaturalEntity;
import edu.ues.aps.process.legal.base.model.ReviewResultType;
import edu.ues.aps.process.legal.base.service.LegalReviewFormResourcesProviderService;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional(readOnly = true)
public class VehicleLegalReviewFormService implements InitializingBean, LegalReviewFormResourcesProviderService {

    private final VehicleBunchInformationService informationService;
    private final VehicleBunchRelationshipService relationshipService;

    private Map<String, Object> map = new HashMap<>();

    public VehicleLegalReviewFormService(VehicleBunchInformationService informationService, VehicleBunchRelationshipService relationshipService) {
        this.informationService = informationService;
        this.relationshipService = relationshipService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        map.put("result_types", ReviewResultType.asMap());
    }

    @Override
    public void setMapValues(Long id) {
        setVerboseDatetime();
        setBunch(id);
        setClientList(id);
        setVehicleList(id);
        setOwner(id);
    }

    @Override
    public Map<String, Object> getMap() {
        return map;
    }

    private void setVerboseDatetime() {
        map.put("verbose_date_time", DatetimeUtil.parseDatetimeToVerboseDate(LocalDateTime.now()));
    }

    private void setBunch(Long id_bunch) {
        map.put("bunch", informationService.findById(id_bunch));
    }

    private void setClientList(Long id_bunch) {
        map.put("client_list", relationshipService.findAllClientsWithNaturalOwner(id_bunch));
    }

    private void setVehicleList(Long id_bunch) {
        map.put("licenses", informationService.findAllLicensesList(id_bunch));
        map.put("vehicles", informationService.findAllLicenses(id_bunch));
        map.put("vehicles_type", informationService.findAllTypes(id_bunch));
        map.put("vehicles_certification", informationService.findAllCertificationTypes(id_bunch));
    }

    private void setOwner(Long id_bunch) {
        Map<String, AbstractOwnerEntityDTO> owner = relationshipService.findOwner(id_bunch);
        if (!owner.isEmpty()) {
            if (owner.get(LegalEntity.class.getSimpleName()).getId_owner() == 0L) {
                map.put("owner", owner.get(NaturalEntity.class.getSimpleName()));
            } else if (owner.get(NaturalEntity.class.getSimpleName()).getId_owner() == 0L) {
                map.put("owner", owner.get(LegalEntity.class.getSimpleName()));
            }
        }
    }
}
