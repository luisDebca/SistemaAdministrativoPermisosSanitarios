package edu.ues.aps.process.legal.base.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.legal.base.dto.AbstractLegalFirstReviewReportDTO;
import edu.ues.aps.process.legal.base.dto.LegalFirstReviewReportDTO;
import org.springframework.ui.ModelMap;

import java.security.Principal;

public abstract class LegalReportController extends AbstractReportController {

    private AbstractLegalFirstReviewReportDTO datasource;

    public abstract String generatePreview(LegalFirstReviewReportDTO review, Principal principal, ModelMap model);

    public abstract String generateReport(Long id_caseFile, Principal principal, ModelMap model);

    public void addUserName(String user_name) {
        datasource.setUser_name(user_name);
    }

    public void addUserCharge(String user_charge) {
        datasource.setUser_charge(user_charge);
    }

    public AbstractLegalFirstReviewReportDTO getDatasource() {
        return datasource;
    }

    public void setDatasource(AbstractLegalFirstReviewReportDTO datasource) {
        this.datasource = datasource;
    }
}
