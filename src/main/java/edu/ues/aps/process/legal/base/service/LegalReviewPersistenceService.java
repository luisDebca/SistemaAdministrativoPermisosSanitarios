package edu.ues.aps.process.legal.base.service;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;

public interface LegalReviewPersistenceService {

    void saveLegalReview(AbstractLegalReviewFormDTO review, Long id);

    void updateLegalReview(AbstractLegalReviewFormDTO review, Long id);
}
