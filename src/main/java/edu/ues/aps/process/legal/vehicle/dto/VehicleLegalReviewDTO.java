package edu.ues.aps.process.legal.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.VehicleRequestDTO;
import edu.ues.aps.process.legal.base.model.ReviewResultType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class VehicleLegalReviewDTO extends VehicleRequestDTO implements AbstractVehicleLegalReview {

    private LocalDateTime review_acceptance;
    private Boolean dictum_created;
    private ReviewResultType dictum_result;
    private Boolean resolution_created;
    private ReviewResultType resolution_result;
    private Boolean opinion_created;
    private ReviewResultType opinion_result;

    public VehicleLegalReviewDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
    }

    public VehicleLegalReviewDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on, LocalDateTime review_acceptance, Boolean dictum_created, ReviewResultType dictum_result) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
        this.review_acceptance = review_acceptance;
        this.dictum_created = dictum_created;
        this.dictum_result = dictum_result;
    }

    public VehicleLegalReviewDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on, LocalDateTime review_acceptance, Boolean dictum_created, ReviewResultType dictum_result, Boolean resolution_created, ReviewResultType resolution_result) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
        this.review_acceptance = review_acceptance;
        this.dictum_created = dictum_created;
        this.dictum_result = dictum_result;
        this.resolution_created = resolution_created;
        this.resolution_result = resolution_result;
    }

    public VehicleLegalReviewDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on, Boolean dictum_created, ReviewResultType dictum_result, Boolean resolution_created, ReviewResultType resolution_result, Boolean opinion_created, ReviewResultType opinion_result) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
        this.dictum_created = dictum_created;
        this.dictum_result = dictum_result;
        this.resolution_created = resolution_created;
        this.resolution_result = resolution_result;
        this.opinion_created = opinion_created;
        this.opinion_result = opinion_result;
    }

    public String getDelay_date() {
        return DatetimeUtil.parseDatetimeToHtmlFormat(DatetimeUtil.addDays(review_acceptance, 5));
    }

    public Boolean getDictum_created() {
        return dictum_created;
    }

    public String getDictum_result() {
        if (dictum_result == null)
            return ReviewResultType.INVALID.getReviewResultType();
        return dictum_result.getReviewResultType();
    }

    public Boolean getResolution_created() {
        return resolution_created;
    }

    public String getResolution_result() {
        if (resolution_result == null)
            return ReviewResultType.INVALID.getReviewResultType();
        return resolution_result.getReviewResultType();
    }

    public Boolean getOpinion_created() {
        return opinion_created;
    }

    public String getOpinion_result() {
        if (opinion_created == null)
            return ReviewResultType.INVALID.getReviewResultType();
        return opinion_result.getReviewResultType();
    }
}
