package edu.ues.aps.process.legal.base.controller;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/legal/second/review")
public class LegalSecondReviewIndexController {

    private static final Logger logger = LogManager.getLogger(LegalSecondReviewIndexController.class);

    private static final String LEGAL_SECOND_REVIEW_INDEX = "opinion_process_index";

    @GetMapping({"/","/index"})
    public String showIndex(ModelMap model){
        logger.info("GET:: Show legal second review index");
        model.addAttribute("process", ProcessIdentifier.SECOND_LEGAL_REVIEW);
        return LEGAL_SECOND_REVIEW_INDEX;
    }
}
