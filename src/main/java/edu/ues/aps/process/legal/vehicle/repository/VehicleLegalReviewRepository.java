package edu.ues.aps.process.legal.vehicle.repository;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.vehicle.dto.AbstractVehicleLegalReview;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class VehicleLegalReviewRepository implements VehicleLegalReviewFinderRepository, VehicleLegalReviewDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleLegalReviewRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleLegalReview> findAllForFistReview() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.legal.vehicle.dto.VehicleLegalReviewDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date,document.acceptance_document_created_on," +
                "(select case when dictum is null then false else true end from LegalReview dictum where dictum.casefile.id = cv.id and dictum.review_type = :review)," +
                "(select dictum.result_type from LegalReview dictum where dictum.casefile.id = cv.id and dictum.review_type = :review))" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "inner join cv.controlDocument document " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehicleLegalReview.class)
                .setParameter("id", ProcessIdentifier.FIRST_LEGAL_REVIEW)
                .setParameter("review", LegalReviewType.DICTUM)
                .getResultList();
    }

    @Override
    public List<AbstractVehicleLegalReview> findAllForFistResolution() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.legal.vehicle.dto.VehicleLegalReviewDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date,document.acceptance_document_created_on," +
                "case when dictum is empty then false else true end,dictum.result_type," +
                "(select case when resolution is null then false else true end from LegalReview resolution where resolution.casefile.id = cv.id and resolution.review_type = :resolution)," +
                "(select resolution.result_type from LegalReview resolution where resolution.casefile.id = cv.id and resolution.review_type = :resolution))" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "left join cv.legalReviewls dictum " +
                "inner join cv.controlDocument document " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "and dictum.review_type = :dictum " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehicleLegalReview.class)
                .setParameter("id", ProcessIdentifier.FIRST_COORDINATOR_REVIEW)
                .setParameter("dictum", LegalReviewType.DICTUM)
                .setParameter("resolution", LegalReviewType.RESOLUTION)
                .getResultList();
    }

    @Override
    public List<AbstractVehicleLegalReview> findAllForSecondReview() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.legal.vehicle.dto.VehicleLegalReviewDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date," +
                "case when dictum is empty then false else true end, dictum.result_type, " +
                "(select case when resolution is null then false else true end from LegalReview resolution where resolution.casefile.id = cv.id and resolution.review_type = :resolution)," +
                "(select resolution.result_type from LegalReview resolution where resolution.casefile.id = cv.id and resolution.review_type = :resolution)," +
                "(select case when opinion is null then false else true end from LegalReview opinion where opinion.casefile.id = cv.id and opinion.review_type = :opinion)," +
                "(select opinion.result_type from LegalReview opinion where opinion.casefile.id = cv.id and opinion.review_type = :opinion))" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "left join cv.legalReviewls dictum " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and dictum.review_type = :dictum " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehicleLegalReview.class)
                .setParameter("id", ProcessIdentifier.SECOND_LEGAL_REVIEW)
                .setParameter("dictum", LegalReviewType.DICTUM)
                .setParameter("resolution", LegalReviewType.RESOLUTION)
                .setParameter("opinion", LegalReviewType.OPINION)
                .getResultList();
    }

    @Override
    public AbstractLegalReviewFormDTO find(Long id_bunch, LegalReviewType type) throws NoResultException {
        List<AbstractLegalReviewFormDTO> resultList = sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO( " +
                "review.id,review.result_type,review.document,review.to_name,review.to_charge,review.from_name,review.from_charge) " +
                "from CasefileVehicle cv " +
                "inner join cv.legalReviewls review " +
                "inner join cv.bunch bunch " +
                "where bunch.id = :id " +
                "and review.review_type = :review_type", AbstractLegalReviewFormDTO.class)
                .setParameter("id", id_bunch)
                .setParameter("review_type", type)
                .getResultList();
        if (resultList.isEmpty()) throw new NoResultException();
        return resultList.get(0);
    }
}
