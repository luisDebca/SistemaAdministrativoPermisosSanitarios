package edu.ues.aps.process.legal.establishment.dto;

import edu.ues.aps.process.legal.base.model.ReviewResultType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.util.Collections;
import java.util.List;

public class EmptyLegalFirstReviewRequestDTO implements AbstractLegalFirstReviewRequestDTO{

    @Override
    public Long getId_casefile() {
        return 0L;
    }

    @Override
    public void setId_casefile(Long id_casefile) {

    }

    @Override
    public Long getId_establishment() {
        return 0L;
    }

    @Override
    public Long getId_owner() {
        return 0L;
    }

    @Override
    public String getFolder_number() {
        return "Folder number not found";
    }

    @Override
    public String getEstablishment_name() {
        return "Establishment name not found";
    }

    @Override
    public String getEstablishment_type() {
        return "Establishment type not found";
    }

    @Override
    public String getEstablishment_section() {
        return "Establishment section not found";
    }

    @Override
    public String getEstablishment_address() {
        return "Establishment address not found";
    }

    @Override
    public String getEstablishment_nit() {
        return "Establishment NIT not found";
    }

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public int getCasefile_certification_duration_in_years() {
        return 0;
    }

    @Override
    public String getCasefile_certification_type() {
        return "Casefile certification type not found";
    }

    @Override
    public String getDocument_acceptance_created_on() {
        return "Document acceptance created on not found";
    }

    @Override
    public String getDocument_created_on() {
        return "Document created on not found";
    }

    @Override
    public String getUcsf_name() {
        return "UCSF name not found";
    }

    @Override
    public String getSibasi_name() {
        return "SIBASI name not found";
    }

    @Override
    public String getLegal_review_result_type() {
        return "Legal review result not found";
    }

    @Override
    public String getLegal_review_created_on() {
        return "Legal review created on not found";
    }

    @Override
    public List<String> getDocument_observations() {
        return Collections.emptyList();
    }

    @Override
    public void setDocument_observations(List<String> document_observations) {

    }

    @Override
    public String getBefore_delay_date() {
        return DatetimeUtil.DATETIME_NOT_DEFINED;
    }

    @Override
    public String getEstablishment_type_detail() {
        return "Establishment type details not found";
    }

    @Override
    public String getLegal_document() {
        return "Legal document content not found";
    }

    @Override
    public void setLegal_document(String legal_document) {

    }

    @Override
    public String getResolution_result_type() {
        return ReviewResultType.INVALID.getReviewResultType();
    }

    @Override
    public String getResolution_created_on() {
        return DatetimeUtil.DATETIME_NOT_DEFINED;
    }

    @Override
    public String getResolution_document() {
        return "Resolution document not found";
    }
}
