package edu.ues.aps.process.legal.vehicle.controller;

import edu.ues.aps.process.legal.vehicle.service.VehicleLegalReviewFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/legal/fist/resolution/vehicle")
public class VehicleLegalFirstResolutionInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleLegalFirstResolutionInformationProviderController.class);

    private static final String ALL_REQUEST = "vehicle_legal_first_resolution_process_list";

    private final VehicleLegalReviewFinderService finderService;

    public VehicleLegalFirstResolutionInformationProviderController(VehicleLegalReviewFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showAllRequest(ModelMap model) {
        logger.info("GET:: Show all request for legal first resolution.");
        model.addAttribute("requests", finderService.findRequestForFirstResolution());
        return ALL_REQUEST;
    }
}
