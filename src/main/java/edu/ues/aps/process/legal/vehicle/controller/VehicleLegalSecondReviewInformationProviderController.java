package edu.ues.aps.process.legal.vehicle.controller;

import edu.ues.aps.process.legal.vehicle.service.VehicleLegalReviewFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/legal-second/review/vehicle")
public class VehicleLegalSecondReviewInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleLegalSecondReviewInformationProviderController.class);

    private static final String REQUESTS = "vehicle_opinion_process_list";

    private final VehicleLegalReviewFinderService finderService;

    @Autowired
    public VehicleLegalSecondReviewInformationProviderController(VehicleLegalReviewFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showAllRequestForSecondLegalReview(ModelMap model) {
        logger.info("GET:: Show all request for vehicle second legal review.");
        model.addAttribute("requests", finderService.findRequestForSecondReview());
        return REQUESTS;
    }
}
