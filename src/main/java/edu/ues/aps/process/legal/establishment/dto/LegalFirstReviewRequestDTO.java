package edu.ues.aps.process.legal.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;
import java.util.List;

public class LegalFirstReviewRequestDTO implements AbstractLegalFirstReviewRequestDTO {

    private Long id_casefile;
    private Long id_establishment;
    private Long id_owner;
    private String folder_number;
    private String establishment_name;
    private String establishment_type;
    private String establishment_section;
    private String establishment_address;
    private String establishment_nit;
    private String establishment_type_detail;
    private String owner_name;
    private int casefile_certification_duration_in_years;
    private CertificationType casefile_certification_type;
    private LocalDateTime document_acceptance_created_on;
    private LocalDateTime document_created_on;
    private String ucsf_name;
    private String sibasi_name;
    private ReviewResultType legal_review_result_type;
    private LocalDateTime legal_review_created_on;
    private String legal_document;
    private ReviewResultType resolution_result_type;
    private LocalDateTime resolution_created_on;
    private String resolution_document;
    private List<String> document_observations;
    private LocalDateTime before_delay_date;

    public LegalFirstReviewRequestDTO(Long id_casefile, Long id_establishment, Long id_owner, String folder_number, String establishment_name, String establishment_type, String establishment_section, String establishment_address, String owner_name, LocalDateTime document_acceptance_created_on, String ucsf_name, String sibasi_name, ReviewResultType legal_review_result_type, LocalDateTime legal_review_created_on) {
        this.id_casefile = id_casefile;
        this.id_establishment = id_establishment;
        this.id_owner = id_owner;
        this.folder_number = folder_number;
        this.establishment_name = establishment_name;
        this.establishment_type = establishment_type;
        this.establishment_section = establishment_section;
        this.establishment_address = establishment_address;
        this.owner_name = owner_name;
        this.document_acceptance_created_on = document_acceptance_created_on;
        this.ucsf_name = ucsf_name;
        this.sibasi_name = sibasi_name;
        this.legal_review_result_type = legal_review_result_type;
        this.legal_review_created_on = legal_review_created_on;
    }

    public LegalFirstReviewRequestDTO(Long id_casefile, Long id_establishment, Long id_owner, String folder_number, String establishment_name, String establishment_type, String establishment_section, String establishment_address, String owner_name, LocalDateTime document_acceptance_created_on, String ucsf_name, String sibasi_name, ReviewResultType legal_review_result_type, LocalDateTime legal_review_created_on, ReviewResultType resolution_result_type) {
        this.id_casefile = id_casefile;
        this.id_establishment = id_establishment;
        this.id_owner = id_owner;
        this.folder_number = folder_number;
        this.establishment_name = establishment_name;
        this.establishment_type = establishment_type;
        this.establishment_section = establishment_section;
        this.establishment_address = establishment_address;
        this.owner_name = owner_name;
        this.document_acceptance_created_on = document_acceptance_created_on;
        this.ucsf_name = ucsf_name;
        this.sibasi_name = sibasi_name;
        this.legal_review_result_type = legal_review_result_type;
        this.legal_review_created_on = legal_review_created_on;
        this.resolution_result_type = resolution_result_type;
    }

    public LegalFirstReviewRequestDTO(Long id_establishment, Long id_owner, String folder_number, String establishment_name, String establishment_type, String establishment_section, String establishment_address, String establishment_nit, String establishment_type_detail, String owner_name, int casefile_certification_duration_in_years, CertificationType casefile_certification_type, LocalDateTime document_acceptance_created_on, LocalDateTime document_created_on, String ucsf_name, String sibasi_name, String resolution_document) {
        this.id_establishment = id_establishment;
        this.id_owner = id_owner;
        this.folder_number = folder_number;
        this.establishment_name = establishment_name;
        this.establishment_type = establishment_type;
        this.establishment_section = establishment_section;
        this.establishment_address = establishment_address;
        this.establishment_nit = establishment_nit;
        this.establishment_type_detail = establishment_type_detail;
        this.owner_name = owner_name;
        this.casefile_certification_duration_in_years = casefile_certification_duration_in_years;
        this.casefile_certification_type = casefile_certification_type;
        this.document_acceptance_created_on = document_acceptance_created_on;
        this.document_created_on = document_created_on;
        this.ucsf_name = ucsf_name;
        this.sibasi_name = sibasi_name;
        this.resolution_document = resolution_document;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public void setId_casefile(Long id_casefile) {
        this.id_casefile = id_casefile;
    }

    public Long getId_establishment() {
        return id_establishment;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_type() {
        return establishment_type;
    }

    public String getEstablishment_section() {
        return establishment_section;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getEstablishment_nit() {
        return establishment_nit;
    }

    public String getEstablishment_type_detail() {
        return establishment_type_detail;
    }


    public String getOwner_name() {
        return owner_name;
    }

    public int getCasefile_certification_duration_in_years() {
        return casefile_certification_duration_in_years;
    }

    public String getCasefile_certification_type() {
        if (casefile_certification_type != null) {
            return casefile_certification_type.getCertificationType();
        } else {
            return CertificationType.INVALID.getCertificationType();
        }
    }

    public String getDocument_acceptance_created_on() {
        if (document_acceptance_created_on != null) {
            return DatetimeUtil.parseDatetimeToLongDateFormat(document_acceptance_created_on);
        } else {
            return DatetimeUtil.DATETIME_NOT_DEFINED;
        }
    }

    public String getDocument_created_on() {
        if (document_created_on != null) {
            return DatetimeUtil.parseDatetimeToLongDateFormat(document_created_on);
        } else {
            return DatetimeUtil.DATETIME_NOT_DEFINED;
        }
    }

    public String getUcsf_name() {
        return ucsf_name;
    }

    public String getSibasi_name() {
        return sibasi_name;
    }

    public String getLegal_review_result_type() {
        if (legal_review_result_type != null) {
            return legal_review_result_type.getReviewResultType();
        } else {
            return ReviewResultType.INVALID.getReviewResultType();
        }
    }

    public String getLegal_review_created_on() {
        if (legal_review_created_on != null) {
            return DatetimeUtil.parseDatetimeToLongDateFormat(legal_review_created_on);
        } else {
            return DatetimeUtil.DATETIME_NOT_DEFINED;
        }
    }

    public String getLegal_document() {
        return legal_document;
    }

    public List<String> getDocument_observations() {
        return document_observations;
    }

    public void setDocument_observations(List<String> document_observations) {
        this.document_observations = document_observations;
    }

    public String getBefore_delay_date() {
        if (document_acceptance_created_on != null) {
            return DatetimeUtil.parseDatetimeToHtmlFormat(document_acceptance_created_on.plusDays(5));
        } else {
            return DatetimeUtil.DATETIME_NOT_DEFINED;
        }
    }

    public String getResolution_result_type() {
        if (resolution_result_type != null) {
            return resolution_result_type.getReviewResultType();
        } else {
            return ReviewResultType.INVALID.getReviewResultType();
        }
    }

    public String getResolution_created_on() {
        if (resolution_created_on != null) {
            return DatetimeUtil.parseDatetimeToLongDateFormat(resolution_created_on);
        } else {
            return DatetimeUtil.DATETIME_NOT_DEFINED;
        }
    }

    public String getResolution_document() {
        return resolution_document;
    }

    public void setLegal_document(String legal_document) {
        this.legal_document = legal_document;
    }
}
