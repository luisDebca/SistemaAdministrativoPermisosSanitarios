package edu.ues.aps.process.legal.base.dto;

import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class LegalFirstReviewReportDTO implements AbstractLegalFirstReviewReportDTO {

    private static final String MEMO_NUMBER_FORMAT = "-3000-DRSM-";
    private static final String LEGAL_NUMBER_FORMAT = "-3000-DRSM-APS-JCO-";

    private ReviewResultType review_result;
    private LegalReviewType review_type;
    private String document_content;
    private String user_name;
    private String user_charge;
    private String memo_number;
    private String to_name;
    private String to_charge;
    private String from_name;
    private String from_charge;
    private LocalDateTime create_on;

    public LegalFirstReviewReportDTO() {
    }

    public LegalFirstReviewReportDTO(String memo_number, String to_name, String to_charge, String from_name, String from_charge, LocalDateTime create_on) {
        this.memo_number = memo_number;
        this.to_name = to_name;
        this.to_charge = to_charge;
        this.from_name = from_name;
        this.from_charge = from_charge;
        this.create_on = create_on;
    }

    @Override
    public String getReview_result() {
        if (review_result == null) return ReviewResultType.INVALID.getReviewResultType();
        return review_result.getReviewResultType();
    }

    @Override
    public void setReview_result(ReviewResultType review_result) {
        this.review_result = review_result;
    }

    @Override
    public String getReview_type() {
        if (review_type == null) return LegalReviewType.INVALID.getReviewType();
        return review_type.getReviewType();
    }

    @Override
    public void setReview_type(LegalReviewType review_type) {
        this.review_type = review_type;
    }

    @Override
    public String getDocument_content() {
        return document_content;
    }

    @Override
    public void setDocument_content(String document_content) {
        this.document_content = document_content;
    }

    @Override
    public String getUser_name() {
        return user_name;
    }

    @Override
    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    @Override
    public String getUser_charge() {
        return user_charge;
    }

    @Override
    public void setUser_charge(String user_charge) {
        this.user_charge = user_charge;
    }

    @Override
    public String getMemo_code() {
        return create_on.getYear() + getCodeFormat() + memo_number;
    }

    @Override
    public String getSubject() {
        if(review_type == null){
            return LegalReviewType.INVALID.getReviewType();
        }
        return review_type.getReviewType().substring(0,1) + review_type.getReviewType().substring(1).toLowerCase() + " Jur�dico";
    }

    @Override
    public String getFor_name() {
        return to_name;
    }

    @Override
    public String getFor_charge() {
        return to_charge;
    }

    @Override
    public String getFrom_name() {
        return from_name;
    }

    @Override
    public String getFrom_charge() {
        return from_charge;
    }

    @Override
    public String getMemo_content() {
        return document_content;
    }

    @Override
    public String getMemo_creation_date() {
        return DatetimeUtil.parseDateToLongDateFormat(LocalDate.now());
    }

    public void setMemo_code(String memo_number) {
        this.memo_number = memo_number;
    }

    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    public void setTo_charge(String to_charge) {
        this.to_charge = to_charge;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    public void setFrom_charge(String from_charge) {
        this.from_charge = from_charge;
    }

    @Override
    public void setCreate_on(LocalDateTime create_on) {
        this.create_on = create_on;
    }

    private String getCodeFormat(){
        if(review_type == null){
            return MEMO_NUMBER_FORMAT;
        }
        return LEGAL_NUMBER_FORMAT;
    }
}
