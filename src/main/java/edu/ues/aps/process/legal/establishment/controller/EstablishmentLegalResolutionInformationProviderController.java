package edu.ues.aps.process.legal.establishment.controller;

import edu.ues.aps.process.legal.establishment.service.LegalFirstReviewDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/legal/first/resolution/establishment")
public class EstablishmentLegalResolutionInformationProviderController {

    private static final Logger logger = LogManager.getLogger(EstablishmentLegalResolutionInformationProviderController.class);

    private static final String LEGAL_FIRST_RESOLUTION_LIST = "legal_first_resolution_process_list";

    private LegalFirstReviewDTOFinderService finderService;

    public EstablishmentLegalResolutionInformationProviderController(LegalFirstReviewDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/list")
    public String showAllRequestForLegalFirstResolution(ModelMap model) {
        logger.info("GET:: Show request for legal resolution process");
        model.addAttribute("requests", finderService.findAllRequestForResolution());
        return LEGAL_FIRST_RESOLUTION_LIST;
    }
}
