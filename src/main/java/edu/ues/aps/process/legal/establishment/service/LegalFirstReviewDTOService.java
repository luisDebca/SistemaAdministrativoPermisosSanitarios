package edu.ues.aps.process.legal.establishment.service;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.dto.EmptyLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.establishment.dto.AbstractEstablishmentLegalRequestReviewDTO;
import edu.ues.aps.process.legal.establishment.repository.LegalFirstResolutionDTOFinderRepository;
import edu.ues.aps.process.legal.establishment.repository.LegalFirstReviewDTOFinderRepository;
import edu.ues.aps.utilities.file.reader.FileReader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class LegalFirstReviewDTOService implements LegalFirstReviewDTOFinderService {

    private final LegalFirstResolutionDTOFinderRepository resolutionFinderRepository;
    private final LegalFirstReviewDTOFinderRepository reviewFinderRepository;
    private final FileReader reader;

    public LegalFirstReviewDTOService(LegalFirstReviewDTOFinderRepository reviewFinderRepository, FileReader reader, LegalFirstResolutionDTOFinderRepository resolutionFinderRepository) {
        this.reviewFinderRepository = reviewFinderRepository;
        this.reader = reader;
        this.resolutionFinderRepository = resolutionFinderRepository;
    }

    @Override
    public List<AbstractEstablishmentLegalRequestReviewDTO> findAllRequestForReview() {
        return reviewFinderRepository.findAll();
    }

    @Override
    public List<AbstractEstablishmentLegalRequestReviewDTO> findAllRequestForResolution() {
        return resolutionFinderRepository.findAll();
    }

    @Override
    public AbstractLegalReviewFormDTO findReview(Long id_caseFile, LegalReviewType type) {
        try {
            AbstractLegalReviewFormDTO legal_review_dto = reviewFinderRepository.findReview(id_caseFile, type);
            legal_review_dto.setId_casefile(id_caseFile);
            legal_review_dto.setReview_type(type);
            if (!legal_review_dto.getDocument_content().isEmpty()) {
                legal_review_dto.setDocument_content(reader.readFileContent(legal_review_dto.getDocument_content()));
            }
            return legal_review_dto;
        } catch (NoResultException e) {
            return new EmptyLegalReviewFormDTO();
        }
    }
}
