package edu.ues.aps.process.legal.vehicle.repository;

import edu.ues.aps.process.legal.vehicle.dto.AbstractVehicleLegalReview;

import java.util.List;

public interface VehicleLegalReviewFinderRepository {

    List<AbstractVehicleLegalReview> findAllForFistReview();

    List<AbstractVehicleLegalReview> findAllForFistResolution();

    List<AbstractVehicleLegalReview> findAllForSecondReview();
}
