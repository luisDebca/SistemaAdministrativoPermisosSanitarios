package edu.ues.aps.process.legal.vehicle.service;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;

public interface VehicleLegalReviewDTOFinderService {

    AbstractLegalReviewFormDTO find(Long id_bunch, LegalReviewType type);
}
