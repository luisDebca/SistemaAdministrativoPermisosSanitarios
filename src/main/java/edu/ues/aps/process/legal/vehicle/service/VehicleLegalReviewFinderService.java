package edu.ues.aps.process.legal.vehicle.service;

import edu.ues.aps.process.legal.vehicle.dto.AbstractVehicleLegalReview;

import java.util.List;

public interface VehicleLegalReviewFinderService {

    List<AbstractVehicleLegalReview> findRequestForFirstReview();

    List<AbstractVehicleLegalReview> findRequestForFirstResolution();

    List<AbstractVehicleLegalReview> findRequestForSecondReview();
}
