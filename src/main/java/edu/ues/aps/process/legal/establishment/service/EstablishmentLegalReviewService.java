package edu.ues.aps.process.legal.establishment.service;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.repository.LegalReviewFinderRepository;
import edu.ues.aps.process.legal.base.repository.LegalReviewPersistenceRepository;
import edu.ues.aps.process.legal.base.service.AbstractLegalReviewPersistenceService;
import edu.ues.aps.process.legal.base.service.LegalReviewPersistenceService;
import edu.ues.aps.utilities.file.writer.FileWriter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EstablishmentLegalReviewService extends AbstractLegalReviewPersistenceService implements LegalReviewPersistenceService {

    public EstablishmentLegalReviewService(FileWriter fileWriter, LegalReviewPersistenceRepository persistenceRepository, LegalReviewFinderRepository finderRepository) {
        super(fileWriter, persistenceRepository, finderRepository);
    }

    @Override
    public void saveLegalReview(AbstractLegalReviewFormDTO review, Long id_caseFile) {
        save(review, id_caseFile);
    }

    @Override
    public void updateLegalReview(AbstractLegalReviewFormDTO review, Long id_caseFile) {
        update(review, id_caseFile);
    }
}
