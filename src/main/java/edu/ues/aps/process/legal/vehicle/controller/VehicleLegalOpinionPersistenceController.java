package edu.ues.aps.process.legal.vehicle.controller;

import edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;
import edu.ues.aps.process.legal.base.service.LegalReviewFormResourcesProviderService;
import edu.ues.aps.process.legal.base.service.LegalReviewPersistenceService;
import edu.ues.aps.process.legal.vehicle.service.VehicleLegalReviewDTOFinderService;
import edu.ues.aps.process.resolution.base.service.MemorandumDTOFinderService;
import edu.ues.aps.process.settings.model.ReportFormatType;
import edu.ues.aps.process.settings.service.ReportFormatContentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/legal-second/review/vehicle")
public class VehicleLegalOpinionPersistenceController {

    private static final Logger logger = LogManager.getLogger(VehicleLegalOpinionPersistenceController.class);

    private static final String OPINION_FORM = "vehicle_opinion_process_form";

    private final LegalReviewFormResourcesProviderService resourcesProviderService;
    private final MemorandumDTOFinderService memorandumFinderService;
    private final LegalReviewPersistenceService persistenceService;
    private final VehicleLegalReviewDTOFinderService finderService;
    private final ReportFormatContentService formatService;

    public VehicleLegalOpinionPersistenceController(@Qualifier("vehicleLegalReviewFormService") LegalReviewFormResourcesProviderService resourcesProviderService, @Qualifier("vehicleLegalReviewService") LegalReviewPersistenceService persistenceService, @Qualifier("memorandumBunchService") MemorandumDTOFinderService memorandumFinderService, VehicleLegalReviewDTOFinderService finderService, ReportFormatContentService formatService) {
        this.resourcesProviderService = resourcesProviderService;
        this.persistenceService = persistenceService;
        this.memorandumFinderService = memorandumFinderService;
        this.finderService = finderService;
        this.formatService = formatService;
    }

    @GetMapping("/bunch/{id_bunch}/new")
    public String showOpinionForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show new opinion for vehicle bunch {}.", id_bunch);
        resourcesProviderService.setMapValues(id_bunch);
        model.addAttribute("review", new LegalReviewFormDTO(LegalReviewType.OPINION));
        model.addAttribute("memos", memorandumFinderService.findAll(id_bunch));
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.OPINION));
        model.addAllAttributes(resourcesProviderService.getMap());
        return OPINION_FORM;
    }

    @PostMapping("/bunch/{id_bunch}/new")
    public String saveOpinionForm(@PathVariable Long id_bunch, @ModelAttribute LegalReviewFormDTO review) {
        logger.info("POST:: Save new opinion for bunch {}.", id_bunch);
        review.setReview_type(LegalReviewType.OPINION);
        review.setResult_type(ReviewResultType.ADMISSIBILITY);
        persistenceService.saveLegalReview(review, id_bunch);
        return "redirect:/legal-second/review/vehicle/all";
    }

    @GetMapping("/bunch/{id_bunch}/edit")
    public String showOpinionEditForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show opinion edit for vehicle bunch {}", id_bunch);
        resourcesProviderService.setMapValues(id_bunch);
        model.addAttribute("review", finderService.find(id_bunch, LegalReviewType.OPINION));
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.OPINION));
        model.addAttribute("memos", memorandumFinderService.findAll(id_bunch));
        model.addAllAttributes(resourcesProviderService.getMap());
        return OPINION_FORM;
    }

    @PostMapping("/bunch/{id_bunch}/edit")
    public String updateOpinionForm(@PathVariable Long id_bunch, @ModelAttribute LegalReviewFormDTO review) {
        logger.info("POST:: Update legal opinion for bunch {}", id_bunch);
        review.setResult_type(ReviewResultType.ADMISSIBILITY);
        persistenceService.updateLegalReview(review, id_bunch);
        return "redirect:/legal-second/review/vehicle/all";
    }
}
