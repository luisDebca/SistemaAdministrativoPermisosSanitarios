package edu.ues.aps.process.legal.base.service;

import edu.ues.aps.process.legal.base.dto.AbstractLegalFirstReviewReportDTO;
import edu.ues.aps.process.legal.base.dto.EmptyLegalReviewReportDTO;
import edu.ues.aps.process.legal.base.dto.LegalFirstReviewReportDTO;
import edu.ues.aps.process.legal.base.model.AbstractLegalReview;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.repository.LegalReviewFinderRepository;
import edu.ues.aps.utilities.file.reader.FileReader;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional(readOnly = true)
public class LegalFirstReviewReportService implements LegalFirstReviewReportFinderService {

    private final LegalReviewFinderRepository finderRepository;
    private final FileReader fileReader;

    public LegalFirstReviewReportService(LegalReviewFinderRepository finderRepository, FileReader fileReader) {
        this.finderRepository = finderRepository;
        this.fileReader = fileReader;
    }

    @Override
    public AbstractLegalFirstReviewReportDTO findReview(Long id_caseFile) {
        return getReviewAsDTO(id_caseFile, LegalReviewType.DICTUM);
    }

    @Override
    public AbstractLegalFirstReviewReportDTO findResolution(Long id_caseFile) {
        return getReviewAsDTO(id_caseFile, LegalReviewType.RESOLUTION);
    }

    @Override
    public AbstractLegalFirstReviewReportDTO findOpinion(Long id_caseFile) {
        return getReviewAsDTO(id_caseFile, LegalReviewType.OPINION);
    }

    private AbstractLegalFirstReviewReportDTO getReviewAsDTO(Long id, LegalReviewType type) {
        try {
            AbstractLegalReview review = finderRepository.findReviewById(id, type);
            AbstractLegalFirstReviewReportDTO dto = new LegalFirstReviewReportDTO();
            dto.setReview_result(review.getResult_type());
            dto.setReview_type(review.getReview_type());
            dto.setTo_name(review.getTo_name());
            dto.setTo_charge(review.getTo_charge());
            dto.setFrom_name(review.getFrom_name());
            dto.setFrom_charge(review.getFrom_charge());
            dto.setMemo_code(review.getNumber());
            dto.setCreate_on(review.getCreate_on());
            dto.setDocument_content(fileReader.readFileContent(review.getDocument()));
            return dto;
        } catch (NoResultException e) {
            return new EmptyLegalReviewReportDTO();
        }
    }
}
