package edu.ues.aps.process.legal.vehicle.controller;

import edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.service.LegalReviewFormResourcesProviderService;
import edu.ues.aps.process.legal.base.service.LegalReviewPersistenceService;
import edu.ues.aps.process.legal.vehicle.service.VehicleLegalReviewDTOFinderService;
import edu.ues.aps.process.settings.model.ReportFormatType;
import edu.ues.aps.process.settings.service.ReportFormatContentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/legal/first/resolution/vehicle")
public class VehicleLegalResolutionPersistenceController {

    private static final Logger logger = LogManager.getLogger(VehicleLegalResolutionPersistenceController.class);

    private static final String RESOLUTION_FORM = "new_vehicle_legal_first_resolution";

    private final ReportFormatContentService formatService;
    private final LegalReviewPersistenceService persistenceService;
    private final VehicleLegalReviewDTOFinderService finderService;
    private final LegalReviewFormResourcesProviderService resourcesProviderService;

    public VehicleLegalResolutionPersistenceController(@Qualifier("vehicleLegalReviewService") LegalReviewPersistenceService persistenceService, @Qualifier("vehicleLegalReviewFormService") LegalReviewFormResourcesProviderService resourcesProviderService, VehicleLegalReviewDTOFinderService finderService, ReportFormatContentService formatService) {
        this.persistenceService = persistenceService;
        this.resourcesProviderService = resourcesProviderService;
        this.finderService = finderService;
        this.formatService = formatService;
    }

    @GetMapping("/bunch/{id_bunch}/new")
    public String showResolutionForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show new resolution for vehicle bunch {}.", id_bunch);
        resourcesProviderService.setMapValues(id_bunch);
        model.addAllAttributes(resourcesProviderService.getMap());
        model.addAttribute("review", new LegalReviewFormDTO(LegalReviewType.RESOLUTION));
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.COORDINATOR));
        return RESOLUTION_FORM;
    }

    @PostMapping("/bunch/{id_bunch}/new")
    public String saveResolution(@PathVariable Long id_bunch, @ModelAttribute LegalReviewFormDTO dto) {
        logger.info("POST:: Save legal resolution for all request in bunch {}.", id_bunch);
        dto.setReview_type(LegalReviewType.RESOLUTION);
        persistenceService.saveLegalReview(dto, id_bunch);
        return "redirect:/legal/fist/resolution/vehicle/all";
    }

    @GetMapping("/bunch/{id_bunch}/edit")
    public String showResolutionEditForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show edit resolution form for vehicle bunch {}.", id_bunch);
        resourcesProviderService.setMapValues(id_bunch);
        model.addAllAttributes(resourcesProviderService.getMap());
        model.addAttribute("review", finderService.find(id_bunch, LegalReviewType.RESOLUTION));
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.COORDINATOR));
        return RESOLUTION_FORM;
    }

    @PostMapping("/bunch/{id_bunch}/edit")
    public String updateResolution(@PathVariable Long id_bunch, @ModelAttribute LegalReviewFormDTO dto) {
        logger.info("POST:: Update resolution for all request in bunch {}.", id_bunch);
        dto.setReview_type(LegalReviewType.RESOLUTION);
        persistenceService.updateLegalReview(dto, id_bunch);
        return "redirect:/legal/fist/resolution/vehicle/all";
    }
}
