package edu.ues.aps.process.legal.base.dto;

import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;

public class EmptyLegalReviewFormDTO implements AbstractLegalReviewFormDTO {

    @Override
    public Long getId_casefile() {
        return 0L;
    }

    @Override
    public Long getId_review() {
        return 0L;
    }

    @Override
    public void setId_review(Long id_review) {

    }

    @Override
    public void setId_casefile(Long id_casefile) {

    }

    @Override
    public ReviewResultType getResult_type() {
        return ReviewResultType.INVALID;
    }

    @Override
    public void setResult_type(ReviewResultType result_type) {

    }

    @Override
    public String getDocument_content() {
        return "Review document content not found";
    }

    @Override
    public void setDocument_content(String document_content) {

    }

    @Override
    public LegalReviewType getReview_type() {
        return LegalReviewType.INVALID;
    }

    @Override
    public void setReview_type(LegalReviewType review_type) {

    }

    @Override
    public String getTo_name() {
        return "Not found";
    }

    @Override
    public void setTo_name(String to_name) {

    }

    @Override
    public String getTo_charge() {
        return "Not found";
    }

    @Override
    public void setTo_charge(String to_charge) {

    }

    @Override
    public String getFrom_name() {
        return "Not found";
    }

    @Override
    public void setFrom_name(String from_name) {

    }

    @Override
    public String getFrom_charge() {
        return "Not found";
    }

    @Override
    public void setFrom_charge(String from_charge) {

    }
}
