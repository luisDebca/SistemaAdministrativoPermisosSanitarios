package edu.ues.aps.process.legal.establishment.controller;

import edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.service.LegalReviewFormResourcesProviderService;
import edu.ues.aps.process.legal.base.service.LegalReviewPersistenceService;
import edu.ues.aps.process.legal.establishment.service.LegalFirstReviewDTOFinderService;
import edu.ues.aps.process.settings.model.ReportFormatType;
import edu.ues.aps.process.settings.service.ReportFormatContentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/legal/first/review/establishment")
public class EstablishmentLegalResolutionPersistenceController {

    private static final Logger logger = LogManager.getLogger(EstablishmentLegalReviewPersistenceController.class);

    private static final String LEGAL_RESOLUTION_FORM = "new_legal_first_resolution";
    private final LegalReviewFormResourcesProviderService resourcesProviderService;
    private final LegalFirstReviewDTOFinderService finderService;
    private final LegalReviewPersistenceService persistenceService;
    private final ReportFormatContentService formatService;

    public EstablishmentLegalResolutionPersistenceController(LegalFirstReviewDTOFinderService finderService, @Qualifier("establishmentLegalReviewService") LegalReviewPersistenceService persistenceService, @Qualifier("establishmentLegalReviewFormService") LegalReviewFormResourcesProviderService resourcesProviderService, ReportFormatContentService formatService) {
        this.finderService = finderService;
        this.persistenceService = persistenceService;
        this.resourcesProviderService = resourcesProviderService;
        this.formatService = formatService;
    }

    @GetMapping("/casefile/{id_caseFile}/new-resolution")
    public String showNewLegalFirstResolutionForm(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show new legal process resolution for case file {}", id_caseFile);
        resourcesProviderService.setMapValues(id_caseFile);
        model.addAllAttributes(resourcesProviderService.getMap());
        model.addAttribute("review", new LegalReviewFormDTO(LegalReviewType.RESOLUTION));
        model.addAttribute("formats",formatService.getFormats(ReportFormatType.COORDINATOR));
        return LEGAL_RESOLUTION_FORM;
    }

    @PostMapping("/casefile/{id_caseFile}/new-resolution")
    public String saveNewLegalFirstResolutionDocument(@ModelAttribute LegalReviewFormDTO resolution, @PathVariable Long id_caseFile) {
        logger.info("POST:: Save legal resolution for case file {}", id_caseFile);
        resolution.setReview_type(LegalReviewType.RESOLUTION);
        persistenceService.saveLegalReview(resolution, id_caseFile);
        return "redirect:/legal/first/resolution/establishment/list";
    }

    @GetMapping("/casefile/{id_caseFile}/edit-resolution")
    public String showEditLegalFirstResolutionForm(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show case file {} legal first resolution edit form", id_caseFile);
        resourcesProviderService.setMapValues(id_caseFile);
        model.addAllAttributes(resourcesProviderService.getMap());
        model.addAttribute("review", finderService.findReview(id_caseFile, LegalReviewType.RESOLUTION));
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.COORDINATOR));
        return LEGAL_RESOLUTION_FORM;
    }

    @PostMapping("/casefile/{id_caseFile}/edit-resolution")
    public String updateLegalResolutionDocumentContent(@ModelAttribute LegalReviewFormDTO resolution, @PathVariable Long id_caseFile) {
        logger.info("POST:: Update case file {} legal first resolution", id_caseFile);
        resolution.setReview_type(LegalReviewType.RESOLUTION);
        persistenceService.updateLegalReview(resolution, id_caseFile);
        return "redirect:/legal/first/resolution/establishment/list";
    }
}
