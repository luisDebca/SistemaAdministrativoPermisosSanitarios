package edu.ues.aps.process.legal.base.report;

import edu.ues.aps.process.legal.base.dto.LegalFirstReviewReportDTO;
import edu.ues.aps.process.legal.base.service.LegalFirstReviewReportFinderService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Collections;

@Controller
@RequestMapping("/legal/first/resolution")
public class LegalFirstReviewResolutionReportController extends LegalReportController {

    private static final Logger logger = LogManager.getLogger(LegalFirstReviewResolutionReportController.class);

    private static final String REPORT_NAME = "LegalResolutionReport";

    private final LegalFirstReviewReportFinderService finderService;
    private final SinglePersonalInformationService personalInformationService;

    public LegalFirstReviewResolutionReportController(SinglePersonalInformationService personalInformationService, LegalFirstReviewReportFinderService finderService) {
        this.personalInformationService = personalInformationService;
        this.finderService = finderService;
    }

    @Override
    @GetMapping("/report-preview.pdf")
    public String generatePreview(LegalFirstReviewReportDTO review, Principal principal, ModelMap model) {
        logger.info("GET:: Generate legal resolution report preview.");
        setDatasource(review);
        addUserInformation(principal.getName());
        addReportRequiredSources(Collections.singletonList(getDatasource()));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }

    @Override
    @GetMapping("/report.pdf")
    public String generateReport(Long id_caseFile, Principal principal, ModelMap model) {
        logger.info("REPORT:: Generate first legal resolution {} report", id_caseFile);
        setDatasource(finderService.findResolution(id_caseFile));
        addUserInformation(principal.getName());
        addReportRequiredSources(Collections.singletonList(getDatasource()));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }

    private void addUserInformation(String principal) {
        addUserName(personalInformationService.getFullName(principal));
        addUserCharge(personalInformationService.getCharge(principal));
    }

}
