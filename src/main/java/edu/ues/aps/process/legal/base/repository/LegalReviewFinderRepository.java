package edu.ues.aps.process.legal.base.repository;

import edu.ues.aps.process.legal.base.model.AbstractLegalReview;
import edu.ues.aps.process.legal.base.model.LegalReviewType;

import javax.persistence.NoResultException;

public interface LegalReviewFinderRepository {

    AbstractLegalReview findReviewById(Long id_caseFile, LegalReviewType reviewType) throws NoResultException;

    Long getMaxLegalMemorandumNumber();
}
