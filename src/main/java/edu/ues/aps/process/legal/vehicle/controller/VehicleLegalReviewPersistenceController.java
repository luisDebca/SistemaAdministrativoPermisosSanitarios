package edu.ues.aps.process.legal.vehicle.controller;

import edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.service.LegalReviewFormResourcesProviderService;
import edu.ues.aps.process.legal.base.service.LegalReviewPersistenceService;
import edu.ues.aps.process.legal.vehicle.service.VehicleLegalReviewDTOFinderService;
import edu.ues.aps.process.settings.model.ReportFormatType;
import edu.ues.aps.process.settings.service.ReportFormatContentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/legal/first/review/vehicle")
public class VehicleLegalReviewPersistenceController {

    private static final Logger logger = LogManager.getLogger(VehicleLegalReviewPersistenceController.class);

    private static final String REVIEW_FORM = "vehicle_new_legal_first_review";

    private final ReportFormatContentService formatService;
    private final VehicleLegalReviewDTOFinderService finderService;
    private final LegalReviewPersistenceService persistenceService;
    private final LegalReviewFormResourcesProviderService resourcesProviderService;

    public VehicleLegalReviewPersistenceController(@Qualifier("vehicleLegalReviewFormService") LegalReviewFormResourcesProviderService resourcesProviderService, @Qualifier("vehicleLegalReviewService") LegalReviewPersistenceService persistenceService, VehicleLegalReviewDTOFinderService finderService, ReportFormatContentService formatService) {
        this.resourcesProviderService = resourcesProviderService;
        this.persistenceService = persistenceService;
        this.finderService = finderService;
        this.formatService = formatService;
    }

    @GetMapping("/bunch/{id_bunch}/new")
    public String showReviewForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show new review for vehicle bunch {}", id_bunch);
        resourcesProviderService.setMapValues(id_bunch);
        model.addAllAttributes(resourcesProviderService.getMap());
        model.addAttribute("review", new LegalReviewFormDTO());
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.DICTUM));
        return REVIEW_FORM;
    }

    @PostMapping("/bunch/{id_bunch}/new")
    public String saveReview(@PathVariable Long id_bunch, @ModelAttribute LegalReviewFormDTO dto) {
        logger.info("POST:: Save legal review for all request in bunch {}.", id_bunch);
        dto.setReview_type(LegalReviewType.DICTUM);
        persistenceService.saveLegalReview(dto, id_bunch);
        return "redirect:/legal/first/review/vehicle/all";
    }

    @GetMapping("/bunch/{id_bunch}/edit")
    public String showReviewEditForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show edit review for vehicle bunch {}", id_bunch);
        resourcesProviderService.setMapValues(id_bunch);
        model.addAllAttributes(resourcesProviderService.getMap());
        model.addAttribute("review", finderService.find(id_bunch, LegalReviewType.DICTUM));
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.DICTUM));
        return REVIEW_FORM;
    }

    @PostMapping("/bunch/{id_bunch}/edit")
    public String updateReview(@PathVariable Long id_bunch, @ModelAttribute LegalReviewFormDTO dto) {
        logger.info("POST:: Update legal review for all request in bunch {}.", id_bunch);
        dto.setReview_type(LegalReviewType.DICTUM);
        persistenceService.saveLegalReview(dto, id_bunch);
        return "redirect:/legal/first/review/vehicle/all";
    }
}
