package edu.ues.aps.process.legal.base.dto;

import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;

import java.time.LocalDateTime;

public class EmptyLegalReviewReportDTO implements AbstractLegalFirstReviewReportDTO {

    @Override
    public String getReview_result() {
        return ReviewResultType.INVALID.getReviewResultType();
    }

    @Override
    public String getDocument_content() {
        return "This is a empty document, request file content not found in server.";
    }

    @Override
    public String getUser_name() {
        return "Not found";
    }

    @Override
    public void setReview_result(ReviewResultType review_result) {

    }

    @Override
    public void setDocument_content(String document_content) {

    }

    @Override
    public void setUser_name(String user_name) {

    }

    @Override
    public String getUser_charge() {
        return "Not found";
    }

    @Override
    public void setUser_charge(String user_charge) {

    }

    @Override
    public String getReview_type() {
        return LegalReviewType.INVALID.getReviewType();
    }

    @Override
    public void setReview_type(LegalReviewType review_type) {

    }

    @Override
    public String getMemo_code() {
        return "2000-3000-DRSM-APS-0000";
    }

    @Override
    public String getSubject() {
        return null;
    }

    @Override
    public String getFor_name() {
        return null;
    }

    @Override
    public String getFor_charge() {
        return null;
    }

    @Override
    public String getFrom_name() {
        return null;
    }

    @Override
    public String getFrom_charge() {
        return null;
    }

    @Override
    public String getMemo_content() {
        return "Empty";
    }

    @Override
    public String getMemo_creation_date() {
        return "--/--/----";
    }

    @Override
    public void setMemo_code(String memo_number) {

    }

    @Override
    public void setTo_name(String to_name) {

    }

    @Override
    public void setTo_charge(String to_charge) {

    }

    @Override
    public void setFrom_name(String from_name) {

    }

    @Override
    public void setFrom_charge(String from_charge) {

    }

    @Override
    public void setCreate_on(LocalDateTime create_on) {

    }
}
