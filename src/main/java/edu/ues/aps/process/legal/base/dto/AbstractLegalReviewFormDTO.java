package edu.ues.aps.process.legal.base.dto;

import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;

public interface AbstractLegalReviewFormDTO {

    Long getId_casefile();

    void setId_casefile(Long id_casefile);

    Long getId_review();

    void setId_review(Long id_review);

    LegalReviewType getReview_type();

    void setReview_type(LegalReviewType review_type);

    ReviewResultType getResult_type();

    void setResult_type(ReviewResultType result_type);

    String getDocument_content();

    void setDocument_content(String document_content);

    String getTo_name();

    void setTo_name(String to_name);

    String getTo_charge();

    void setTo_charge(String to_charge);

    String getFrom_name();

    void setFrom_name(String from_name);

    String getFrom_charge();

    void setFrom_charge(String from_charge);
}
