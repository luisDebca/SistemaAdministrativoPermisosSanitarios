package edu.ues.aps.process.legal.base.service;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.AbstractLegalReview;
import edu.ues.aps.process.legal.base.model.LegalReview;
import edu.ues.aps.process.legal.base.repository.LegalReviewFinderRepository;
import edu.ues.aps.process.legal.base.repository.LegalReviewPersistenceRepository;
import edu.ues.aps.utilities.file.writer.FileWriter;

import java.time.LocalDateTime;

public abstract class AbstractLegalReviewPersistenceService {

    private final FileWriter fileWriter;
    private final LegalReviewPersistenceRepository persistenceRepository;
    private final LegalReviewFinderRepository finderRepository;

    public AbstractLegalReviewPersistenceService(FileWriter fileWriter, LegalReviewPersistenceRepository persistenceRepository, LegalReviewFinderRepository finderRepository) {
        this.fileWriter = fileWriter;
        this.persistenceRepository = persistenceRepository;
        this.finderRepository = finderRepository;
    }

    public void save(AbstractLegalReviewFormDTO review, Long id_caseFile) {
        String file_name = saveLegalReviewFile(review);
        LegalReview first_review = buildLegalReview(review);
        first_review.setDocument(file_name);
        persistenceRepository.saveReview(id_caseFile, first_review);
    }

    public void update(AbstractLegalReviewFormDTO review, Long id_caseFile) {
        AbstractLegalReview persistence = finderRepository.findReviewById(id_caseFile, review.getReview_type());
        String new_file_name = updateLegalReviewFile(persistence, id_caseFile, review.getDocument_content());
        persistence.setDocument(new_file_name);
        persistence.setResult_type(review.getResult_type());
        persistence.setTo_name(review.getTo_name());
        persistence.setTo_charge(review.getTo_charge());
        persistence.setFrom_name(review.getFrom_name());
        persistence.setFrom_charge(review.getFrom_charge());
        persistenceRepository.updateReview(persistence);
    }

    private LegalReview buildLegalReview(AbstractLegalReviewFormDTO dto) {
        LegalReview review = new LegalReview();
        review.setId(dto.getId_review());
        review.setResult_type(dto.getResult_type());
        review.setReview_type(dto.getReview_type());
        review.setCreate_on(LocalDateTime.now());
        review.setTo_name(dto.getTo_name());
        review.setTo_charge(dto.getTo_charge());
        review.setFrom_name(dto.getFrom_name());
        review.setFrom_charge(dto.getFrom_charge());
        review.setNumber(String.valueOf(finderRepository.getMaxLegalMemorandumNumber() + 1L));
        return review;
    }

    private String saveLegalReviewFile(AbstractLegalReviewFormDTO dto) {
        String file_name = dto.getReview_type().name() + dto.getId_casefile();
        return fileWriter.write(file_name, dto.getDocument_content());
    }

    private String updateLegalReviewFile(AbstractLegalReview review, Long id_caseFile, String document_content) {
        if (fileWriter.isFileExist(review.getDocument())) {
            fileWriter.delete(review.getDocument());
        }
        return fileWriter.write(String.format("%s%d", review.getReview_type().name(), id_caseFile), document_content);
    }

}
