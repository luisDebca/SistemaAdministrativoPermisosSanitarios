package edu.ues.aps.process.legal.vehicle.repository;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;

import javax.persistence.NoResultException;

public interface VehicleLegalReviewDTOFinderRepository {

    AbstractLegalReviewFormDTO find(Long id_bunch, LegalReviewType type) throws NoResultException;
}
