package edu.ues.aps.process.legal.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;

import java.time.LocalDateTime;

public interface AbstractLegalReview {

    Long getId();

    void setId(Long id);

    String getNumber();

    void setNumber(String number);

    String getTo_name();

    void setTo_name(String to_name);

    String getTo_charge();

    void setTo_charge(String to_charge);

    String getFrom_name();

    void setFrom_name(String from_name);

    String getFrom_charge();

    void setFrom_charge(String from_charge);

    LegalReviewType getReview_type();

    void setReview_type(LegalReviewType review_type);

    ReviewResultType getResult_type();

    void setResult_type(ReviewResultType result_type);

    LocalDateTime getCreate_on();

    void setCreate_on(LocalDateTime create_on);

    String getDocument();

    void setDocument(String document_content);

    AbstractCasefile getCasefile();

    void setCasefile(Casefile casefile);
}
