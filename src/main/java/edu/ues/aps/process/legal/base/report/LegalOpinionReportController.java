package edu.ues.aps.process.legal.base.report;

import edu.ues.aps.process.legal.base.dto.LegalFirstReviewReportDTO;
import edu.ues.aps.process.legal.base.service.LegalFirstReviewReportFinderService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;
import java.util.Collections;

@Controller
@RequestMapping("/legal/second/review")
public class LegalOpinionReportController extends LegalReportController {

    private static final Logger logger = LogManager.getLogger(LegalOpinionReportController.class);

    private static final String REPORT_NAME = "MemoSubjectReport";

    private final LegalFirstReviewReportFinderService finderService;
    private final SinglePersonalInformationService personalInformationService;

    public LegalOpinionReportController(LegalFirstReviewReportFinderService finderService, SinglePersonalInformationService personalInformationService) {
        this.finderService = finderService;
        this.personalInformationService = personalInformationService;
    }

    @Override
    @GetMapping("/opinion-report-preview.pdf")
    public String generatePreview(LegalFirstReviewReportDTO review, Principal principal, ModelMap model) {
        logger.info("REPORT:: Generate opinion review preview.");
        setDatasource(review);
        addUserInformation(principal.getName());
        addReportRequiredSources(Collections.singletonList(getDatasource()));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }

    @Override
    @GetMapping("/opinion-report.pdf")
    public String generateReport(Long id_caseFile, Principal principal, ModelMap model) {
        logger.info("REPORT:: Generate opinion review report for case file {}.",id_caseFile);
        setDatasource(finderService.findOpinion(id_caseFile));
        addUserInformation(principal.getName());
        addReportRequiredSources(Collections.singletonList(getDatasource()));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }

    private void addUserInformation(String principal) {
        addUserName(personalInformationService.getFullName(principal));
        addUserCharge(personalInformationService.getCharge(principal));
    }
}
