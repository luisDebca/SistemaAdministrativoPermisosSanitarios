package edu.ues.aps.process.legal.base.model;

public enum LegalReviewType {

    DICTUM("DICTAMEN"),
    OPINION("OPINIÓN"),
    RESOLUTION("RESOLUCIÓN"),
    INVALID("INVALIDO");

    private String reviewType;

    LegalReviewType(String reviewType) {
        this.reviewType = reviewType;
    }

    public String getReviewType() {
        return reviewType;
    }
}
