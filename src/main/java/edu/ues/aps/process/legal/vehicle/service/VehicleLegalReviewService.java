package edu.ues.aps.process.legal.vehicle.service;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.repository.LegalReviewFinderRepository;
import edu.ues.aps.process.legal.base.repository.LegalReviewPersistenceRepository;
import edu.ues.aps.process.legal.base.service.AbstractLegalReviewPersistenceService;
import edu.ues.aps.process.legal.base.service.LegalReviewPersistenceService;
import edu.ues.aps.utilities.file.writer.FileWriter;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class VehicleLegalReviewService extends AbstractLegalReviewPersistenceService implements LegalReviewPersistenceService {

    private final VehicleBunchInformationService informationService;

    public VehicleLegalReviewService(FileWriter fileWriter, LegalReviewPersistenceRepository persistenceRepository, LegalReviewFinderRepository finderRepository, VehicleBunchInformationService informationService) {
        super(fileWriter, persistenceRepository, finderRepository);
        this.informationService = informationService;
    }

    @Override
    public void saveLegalReview(AbstractLegalReviewFormDTO review, Long id_bunch) {
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            review.setId_casefile(id_caseFile);
            save(review, id_caseFile);
        }
    }

    @Override
    public void updateLegalReview(AbstractLegalReviewFormDTO review, Long id_bunch) {
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            review.setId_casefile(id_caseFile);
            update(review, id_caseFile);
        }
    }
}
