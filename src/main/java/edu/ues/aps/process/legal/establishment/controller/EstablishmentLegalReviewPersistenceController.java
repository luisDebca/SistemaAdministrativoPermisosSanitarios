package edu.ues.aps.process.legal.establishment.controller;

import edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.service.LegalReviewFormResourcesProviderService;
import edu.ues.aps.process.legal.base.service.LegalReviewPersistenceService;
import edu.ues.aps.process.legal.establishment.service.LegalFirstReviewDTOFinderService;
import edu.ues.aps.process.settings.model.ReportFormatType;
import edu.ues.aps.process.settings.service.ReportFormatContentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/legal/first/review/establishment")
public class EstablishmentLegalReviewPersistenceController {

    private static final Logger logger = LogManager.getLogger(EstablishmentLegalReviewPersistenceController.class);

    private static final String LEGAL_FIRST_REVIEW_FORM = "new_legal_first_review";

    private final LegalFirstReviewDTOFinderService finderService;
    private final LegalReviewPersistenceService persistenceService;
    private final ReportFormatContentService formatService;
    private final LegalReviewFormResourcesProviderService resourcesProviderService;

    public EstablishmentLegalReviewPersistenceController(LegalFirstReviewDTOFinderService finderService, @Qualifier("establishmentLegalReviewService") LegalReviewPersistenceService persistenceService, @Qualifier("establishmentLegalReviewFormService") LegalReviewFormResourcesProviderService resourcesProviderService, ReportFormatContentService formatService) {
        this.finderService = finderService;
        this.persistenceService = persistenceService;
        this.resourcesProviderService = resourcesProviderService;
        this.formatService = formatService;
    }

    @GetMapping("/casefile/{id_caseFile}/new-review")
    public String showNewLegalFirstReviewForm(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show new legal first review form for case file {}", id_caseFile);
        resourcesProviderService.setMapValues(id_caseFile);
        model.addAllAttributes(resourcesProviderService.getMap());
        model.addAttribute("review", new LegalReviewFormDTO(LegalReviewType.DICTUM));
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.DICTUM));
        return LEGAL_FIRST_REVIEW_FORM;
    }

    @PostMapping("/casefile/{id_caseFile}/new-review")
    public String saveNewLegalFirstReviewDocument(@ModelAttribute LegalReviewFormDTO review, @PathVariable Long id_caseFile) {
        logger.info("POST:: Save legal first review for case file {}: ", id_caseFile);
        review.setReview_type(LegalReviewType.DICTUM);
        persistenceService.saveLegalReview(review, id_caseFile);
        return "redirect:/legal/first/review/establishment/list";
    }

    @GetMapping("/casefile/{id_caseFile}/edit-review")
    public String showEditLegalFirstReviewForm(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show case file {} legal first review edit form", id_caseFile);
        resourcesProviderService.setMapValues(id_caseFile);
        model.addAllAttributes(resourcesProviderService.getMap());
        model.addAttribute("review", finderService.findReview(id_caseFile, LegalReviewType.DICTUM));
        model.addAttribute("formats", formatService.getFormats(ReportFormatType.DICTUM));
        return LEGAL_FIRST_REVIEW_FORM;
    }

    @PostMapping("/casefile/{id_caseFile}/edit-review")
    public String updateLegalReviewDocumentContent(@ModelAttribute LegalReviewFormDTO review, @PathVariable Long id_caseFile) {
        logger.info("POST:: Update case file {} legal first review", id_caseFile);
        review.setReview_type(LegalReviewType.DICTUM);
        persistenceService.updateLegalReview(review, id_caseFile);
        return "redirect:/legal/first/review/establishment/list";
    }

}
