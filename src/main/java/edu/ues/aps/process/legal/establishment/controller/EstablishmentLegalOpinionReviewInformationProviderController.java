package edu.ues.aps.process.legal.establishment.controller;

import edu.ues.aps.process.legal.establishment.service.LegalSecondReviewDTOService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/legal/second/review/establishment")
public class EstablishmentLegalOpinionReviewInformationProviderController {

    private static final Logger logger = LogManager.getLogger(EstablishmentLegalOpinionReviewInformationProviderController.class);

    private static final String SECOND_LEGAL_REVIEW = "establishment_opinion_process_list";

    private final LegalSecondReviewDTOService dtoService;

    public EstablishmentLegalOpinionReviewInformationProviderController(LegalSecondReviewDTOService dtoService) {
        this.dtoService = dtoService;
    }

    @GetMapping("/list")
    public String showSecondLegalReviewRequest(ModelMap model){
        logger.info("GET:: Show legal second request review.");
        model.addAttribute("requests",dtoService.find());
        return SECOND_LEGAL_REVIEW;
    }
}
