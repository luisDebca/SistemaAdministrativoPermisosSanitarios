package edu.ues.aps.process.legal.establishment.repository;

import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.establishment.dto.AbstractEstablishmentLegalRequestReviewDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class LegalFirstResolutionDTORepository implements LegalFirstResolutionDTOFinderRepository{

    private final SessionFactory sessionFactory;

    public LegalFirstResolutionDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractEstablishmentLegalRequestReviewDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.legal.establishment.dto.EstablishmentLegalRequestReviewDTO(ce.id ,owner.id,establishment.id,concat(concat(ce.request_folder,'-'),ce.request_year),concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year)," +
                "establishment.name,establishment.type_detail,etype.type,section.type,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,ucsf.name,sibasi.zone,ce.certification_type,ce.creation_date,owner.name,document.acceptance_document_created_on," +
                "(select review.result_type from LegalReview review where review.casefile.id = ce.id and review.review_type = :dictum)," +
                "(select case when resolution is null then false else true end from LegalReview resolution where resolution.casefile.id = ce.id and resolution.review_type = :resolution)," +
                "(select review.result_type from LegalReview review where review.casefile.id = ce.id and review.review_type = :resolution))" +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "left join establishment.type etype " +
                "left join etype.section section " +
                "left join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join ce.controlDocument document " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id", AbstractEstablishmentLegalRequestReviewDTO.class)
                .setParameter("id", ProcessIdentifier.FIRST_COORDINATOR_REVIEW)
                .setParameter("dictum", LegalReviewType.DICTUM)
                .setParameter("resolution", LegalReviewType.RESOLUTION)
                .getResultList();
    }
}
