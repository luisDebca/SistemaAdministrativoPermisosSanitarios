package edu.ues.aps.process.legal.base.model;

import edu.ues.aps.process.base.model.Casefile;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "LEGAL_REVIEW")
public class LegalReview implements AbstractLegalReview {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "number", length = 8, nullable = false)
    private String number;

    @Column(name = "to_name", length = 150)
    private String to_name;

    @Column(name = "to_charge", length = 100)
    private String to_charge;

    @Column(name = "from_name", length = 150)
    private String from_name;

    @Column(name = "from_charge", length = 100)
    private String from_charge;

    @Enumerated(EnumType.STRING)
    @Column(name = "review_type", nullable = false, length = 10)
    private LegalReviewType review_type;

    @Enumerated(EnumType.STRING)
    @Column(name = "result_type", nullable = false, length = 15)
    private ReviewResultType result_type;

    @Column(name = "create_on", nullable = false)
    private LocalDateTime create_on;

    @Column(name = "document", length = 50)
    private String document;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_casefile")
    private Casefile casefile;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public String getTo_name() {
        return to_name;
    }

    @Override
    public void setTo_name(String to_name) {
        this.to_name = to_name;
    }

    @Override
    public String getTo_charge() {
        return to_charge;
    }

    @Override
    public void setTo_charge(String to_charge) {
        this.to_charge = to_charge;
    }

    @Override
    public String getFrom_name() {
        return from_name;
    }

    @Override
    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    @Override
    public String getFrom_charge() {
        return from_charge;
    }

    @Override
    public void setFrom_charge(String from_charge) {
        this.from_charge = from_charge;
    }

    @Override
    public LegalReviewType getReview_type() {
        return review_type;
    }

    @Override
    public void setReview_type(LegalReviewType review_type) {
        this.review_type = review_type;
    }

    @Override
    public ReviewResultType getResult_type() {
        return result_type;
    }

    @Override
    public void setResult_type(ReviewResultType result_type) {
        this.result_type = result_type;
    }

    @Override
    public LocalDateTime getCreate_on() {
        return create_on;
    }

    @Override
    public void setCreate_on(LocalDateTime create_on) {
        this.create_on = create_on;
    }

    @Override
    public String getDocument() {
        return document;
    }

    @Override
    public void setDocument(String document_content) {
        this.document = document_content;
    }

    @Override
    public Casefile getCasefile() {
        return casefile;
    }

    @Override
    public void setCasefile(Casefile casefile) {
        this.casefile = casefile;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        LegalReview that = (LegalReview) o;

        return review_type == that.review_type && result_type == that.result_type && (create_on != null ? create_on.equals(that.create_on) : that.create_on == null);
    }

    @Override
    public int hashCode() {
        int result = review_type.hashCode();
        result = 31 * result + result_type.hashCode();
        result = 31 * result + (create_on != null ? create_on.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "LegalReview{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", to_name='" + to_name + '\'' +
                ", to_charge='" + to_charge + '\'' +
                ", from_name='" + from_name + '\'' +
                ", from_charge='" + from_charge + '\'' +
                ", review_type=" + review_type +
                ", result_type=" + result_type +
                ", create_on=" + create_on +
                ", document='" + document + '\'' +
                '}';
    }
}
