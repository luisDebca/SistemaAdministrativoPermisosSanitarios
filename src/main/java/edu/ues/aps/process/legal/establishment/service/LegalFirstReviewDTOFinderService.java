package edu.ues.aps.process.legal.establishment.service;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.establishment.dto.AbstractEstablishmentLegalRequestReviewDTO;

import java.util.List;

public interface LegalFirstReviewDTOFinderService {

    List<AbstractEstablishmentLegalRequestReviewDTO> findAllRequestForReview();

    List<AbstractEstablishmentLegalRequestReviewDTO> findAllRequestForResolution();

    AbstractLegalReviewFormDTO findReview(Long id_caseFile, LegalReviewType type);
}
