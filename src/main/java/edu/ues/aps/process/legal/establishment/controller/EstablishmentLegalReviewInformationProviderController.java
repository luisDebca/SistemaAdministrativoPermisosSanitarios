package edu.ues.aps.process.legal.establishment.controller;

import edu.ues.aps.process.legal.establishment.service.LegalFirstReviewDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/legal/first/review/establishment")
public class EstablishmentLegalReviewInformationProviderController {

    private static final Logger logger = LogManager.getLogger(EstablishmentLegalReviewInformationProviderController.class);

    private static final String LEGAL_REVIEW_LIST = "legal_first_review_process_list";

    private LegalFirstReviewDTOFinderService finderService;

    public EstablishmentLegalReviewInformationProviderController(LegalFirstReviewDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/list")
    public String showAllRequestWithFirstLegalReviewProcess(ModelMap model) {
        logger.info("GET:: Show all request for legal first review process");
        model.addAttribute("requests", finderService.findAllRequestForReview());
        return LEGAL_REVIEW_LIST;
    }

}
