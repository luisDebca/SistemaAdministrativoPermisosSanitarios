package edu.ues.aps.process.legal.vehicle.controller;

import edu.ues.aps.process.legal.vehicle.service.VehicleLegalReviewFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/legal/first/review/vehicle")
public class VehicleLegalFirstReviewInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleLegalFirstReviewInformationProviderController.class);

    private static final String ALL_REQUEST = "vehicle_legal_first_review_process_list";

    private final VehicleLegalReviewFinderService finderService;

    public VehicleLegalFirstReviewInformationProviderController(VehicleLegalReviewFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showAllRequest(ModelMap model) {
        logger.info("GET:: Show all request for first legal review process.");
        model.addAttribute("requests", finderService.findRequestForFirstReview());
        return ALL_REQUEST;
    }


}
