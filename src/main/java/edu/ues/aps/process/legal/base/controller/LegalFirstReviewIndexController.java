package edu.ues.aps.process.legal.base.controller;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/legal/first/review")
public class LegalFirstReviewIndexController {

    private static final Logger logger = LogManager.getLogger(LegalFirstReviewIndexController.class);

    private static final String LEGAL_FIRST_REVIEW_INDEX = "legal-first-review-index";

    @GetMapping({"/", "/index"})
    public String showFirstReviewIndex(ModelMap model) {
        logger.info("GET:: Show Legal first review index");
        model.addAttribute("process", ProcessIdentifier.FIRST_LEGAL_REVIEW);
        return LEGAL_FIRST_REVIEW_INDEX;
    }
}
