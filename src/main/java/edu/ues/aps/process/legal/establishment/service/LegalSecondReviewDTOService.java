package edu.ues.aps.process.legal.establishment.service;

import edu.ues.aps.process.legal.establishment.dto.AbstractEstablishmentLegalRequestReviewDTO;

import java.util.List;

public interface LegalSecondReviewDTOService {

    List<AbstractEstablishmentLegalRequestReviewDTO> find();
}
