package edu.ues.aps.process.legal.establishment.dto;

public interface AbstractLegalSecondReviewRequestDTO {

    Long getId_caseFile();

    Long getId_establishment();

    Long getId_owner();

    String getFolder_number();

    String getRequest_number();

    String getCertification_type();

    String getUcsf();

    String getSibasi();

    String getEstablishment_name();

    String getEstablishment_section();

    String getEstablishment_type();

    String getEstablishment_address();

    String getOwner_name();

    Boolean getOpinion_created();

    String getOpinion_result();
}
