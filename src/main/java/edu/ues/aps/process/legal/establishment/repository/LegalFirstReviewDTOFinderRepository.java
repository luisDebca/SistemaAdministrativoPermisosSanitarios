package edu.ues.aps.process.legal.establishment.repository;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.establishment.dto.AbstractEstablishmentLegalRequestReviewDTO;
import edu.ues.aps.process.legal.establishment.dto.AbstractLegalFirstReviewRequestDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface LegalFirstReviewDTOFinderRepository {

    List<AbstractEstablishmentLegalRequestReviewDTO> findAll();

    AbstractLegalFirstReviewRequestDTO findReviewInformation(Long id_caseFile) throws NoResultException;

    AbstractLegalReviewFormDTO findReview(Long id_caseFile, LegalReviewType type) throws NoResultException;

}
