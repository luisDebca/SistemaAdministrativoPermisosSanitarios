package edu.ues.aps.process.legal.base.service;

import edu.ues.aps.process.legal.base.dto.AbstractLegalFirstReviewReportDTO;

public interface LegalFirstReviewReportFinderService {

    AbstractLegalFirstReviewReportDTO findReview(Long id_caseFile);

    AbstractLegalFirstReviewReportDTO findResolution(Long id_caseFile);

    AbstractLegalFirstReviewReportDTO findOpinion(Long id_caseFile);
}
