package edu.ues.aps.process.legal.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleRequestDTO;

public interface AbstractVehicleLegalReview extends AbstractVehicleRequestDTO {

    String getDelay_date();

    Boolean getDictum_created();

    String getDictum_result();

    Boolean getResolution_created();

    String getResolution_result();

    Boolean getOpinion_created();

    String getOpinion_result();
}
