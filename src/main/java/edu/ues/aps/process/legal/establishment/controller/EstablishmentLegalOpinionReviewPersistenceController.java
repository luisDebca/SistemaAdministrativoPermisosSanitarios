package edu.ues.aps.process.legal.establishment.controller;

import edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;
import edu.ues.aps.process.legal.base.service.LegalReviewFormResourcesProviderService;
import edu.ues.aps.process.legal.base.service.LegalReviewPersistenceService;
import edu.ues.aps.process.legal.establishment.service.LegalFirstReviewDTOFinderService;
import edu.ues.aps.process.resolution.base.service.MemorandumDTOFinderService;
import edu.ues.aps.process.settings.model.ReportFormatType;
import edu.ues.aps.process.settings.service.ReportFormatContentService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/legal/second/review/establishment")
public class EstablishmentLegalOpinionReviewPersistenceController {

    private static final Logger logger = LogManager.getLogger(EstablishmentLegalOpinionReviewPersistenceController.class);

    private static final String OPINION = "establishment_opinion_process_form";

    private final LegalReviewFormResourcesProviderService resourcesProviderService;
    private final ReportFormatContentService reportFormatContentService;
    private final MemorandumDTOFinderService memorandumFinderService;
    private final LegalReviewPersistenceService persistenceService;
    private final LegalFirstReviewDTOFinderService finderService;

    public EstablishmentLegalOpinionReviewPersistenceController(@Qualifier("establishmentLegalReviewFormService") LegalReviewFormResourcesProviderService resourcesProviderService, @Qualifier("memorandumEstablishmentService") MemorandumDTOFinderService memorandumFinderService, @Qualifier("establishmentLegalReviewService") LegalReviewPersistenceService persistenceService, LegalFirstReviewDTOFinderService finderService, ReportFormatContentService reportFormatContentService) {
        this.resourcesProviderService = resourcesProviderService;
        this.memorandumFinderService = memorandumFinderService;
        this.persistenceService = persistenceService;
        this.finderService = finderService;
        this.reportFormatContentService = reportFormatContentService;
    }

    @GetMapping("/case-file/{id_caseFile}/opinion/new")
    public String showOpinionForm(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show new opinion for case file {}.", id_caseFile);
        resourcesProviderService.setMapValues(id_caseFile);
        model.addAllAttributes(resourcesProviderService.getMap());
        model.addAttribute("opinion", new LegalReviewFormDTO(LegalReviewType.OPINION));
        model.addAttribute("formats",reportFormatContentService.getFormats(ReportFormatType.OPINION));
        model.addAttribute("memos", memorandumFinderService.findAll(id_caseFile));
        return OPINION;
    }

    @PostMapping("/case-file/{id_caseFile}/opinion/new")
    public String saveOpinion(@PathVariable Long id_caseFile, @ModelAttribute LegalReviewFormDTO opinion) {
        logger.info("POST:: Save new opinion for case file {}.", id_caseFile);
        opinion.setReview_type(LegalReviewType.OPINION);
        opinion.setResult_type(ReviewResultType.ADMISSIBILITY);
        persistenceService.saveLegalReview(opinion, id_caseFile);
        return "redirect:/legal/second/review/establishment/list";
    }

    @GetMapping("/case-file/{id_caseFile}/opinion/edit")
    public String editOpinionForm(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show edit opinion for case file {}.", id_caseFile);
        resourcesProviderService.setMapValues(id_caseFile);
        model.addAllAttributes(resourcesProviderService.getMap());
        model.addAttribute("opinion", finderService.findReview(id_caseFile, LegalReviewType.OPINION));
        model.addAttribute("formats",reportFormatContentService.getFormats(ReportFormatType.OPINION));
        model.addAttribute("memos", memorandumFinderService.findAll(id_caseFile));
        return OPINION;
    }

    @PostMapping("/case-file/{id_caseFile}/opinion/edit")
    public String updateOpinion(@PathVariable Long id_caseFile, @ModelAttribute LegalReviewFormDTO opinion) {
        logger.info("POST:: Update opinion for case file {}.", id_caseFile);
        opinion.setReview_type(LegalReviewType.OPINION);
        persistenceService.updateLegalReview(opinion, id_caseFile);
        return "redirect:/legal/second/review/establishment/list";
    }

}
