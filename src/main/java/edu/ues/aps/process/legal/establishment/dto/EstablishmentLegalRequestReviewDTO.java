package edu.ues.aps.process.legal.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.EstablishmentRequestDTO;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class EstablishmentLegalRequestReviewDTO extends EstablishmentRequestDTO implements AbstractEstablishmentLegalRequestReviewDTO {

    private LocalDateTime review_acceptance;
    private Boolean dictum_created;
    private ReviewResultType dictum_result;
    private Boolean resolution_created;
    private ReviewResultType resolution_result;
    private Boolean opinion_created;
    private ReviewResultType opinion_result;
    private String resolution_number;

    public EstablishmentLegalRequestReviewDTO(Long id_caseFile, Long id_owner, Long id_establishment, String folder_number, String request_number, String establishment_name, String establishment_type_detail, String establishment_type, String establishment_section, String establishment_address, String establishment_nit, String UCSF, String SIBASI, CertificationType certification_type, LocalDateTime caseFile_createOn, String owner_name, LocalDateTime review_acceptance, Boolean dictum_created, ReviewResultType dictum_result) {
        super(id_caseFile, id_owner, id_establishment, folder_number, request_number, establishment_name, establishment_type_detail, establishment_type, establishment_section, establishment_address, establishment_nit, UCSF, SIBASI, certification_type, caseFile_createOn, owner_name);
        this.review_acceptance = review_acceptance;
        this.dictum_created = dictum_created;
        this.dictum_result = dictum_result;
    }

    public EstablishmentLegalRequestReviewDTO(Long id_caseFile, Long id_owner, Long id_establishment, String folder_number, String request_number, String establishment_name, String establishment_type_detail, String establishment_type, String establishment_section, String establishment_address, String establishment_nit, String UCSF, String SIBASI, CertificationType certification_type, LocalDateTime caseFile_createOn, String owner_name, LocalDateTime review_acceptance, ReviewResultType dictum_result, Boolean resolution_created, ReviewResultType resolution_result) {
        super(id_caseFile, id_owner, id_establishment, folder_number, request_number, establishment_name, establishment_type_detail, establishment_type, establishment_section, establishment_address, establishment_nit, UCSF, SIBASI, certification_type, caseFile_createOn, owner_name);
        this.review_acceptance = review_acceptance;
        this.dictum_result = dictum_result;
        this.resolution_created = resolution_created;
        this.resolution_result = resolution_result;
    }

    public EstablishmentLegalRequestReviewDTO(Long id_caseFile, Long id_owner, Long id_establishment, String folder_number, String request_number, String establishment_name, String establishment_type_detail, String establishment_type, String establishment_section, String establishment_address, String establishment_nit, String UCSF, String SIBASI, CertificationType certification_type, LocalDateTime caseFile_createOn, String owner_name, ReviewResultType dictum_result, ReviewResultType resolution_result, Boolean opinion_created, ReviewResultType opinion_result, String resolution_number) {
        super(id_caseFile, id_owner, id_establishment, folder_number, request_number, establishment_name, establishment_type_detail, establishment_type, establishment_section, establishment_address, establishment_nit, UCSF, SIBASI, certification_type, caseFile_createOn, owner_name);
        this.dictum_result = dictum_result;
        this.resolution_result = resolution_result;
        this.opinion_created = opinion_created;
        this.opinion_result = opinion_result;
        this.resolution_number = resolution_number;
    }

    public String getDelay() {
        if (review_acceptance == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDatetimeToHtmlFormat(DatetimeUtil.addDays(review_acceptance, 5));
    }

    public Boolean getDictum_created() {
        return dictum_created;
    }

    public String getDictum_result() {
        if (dictum_result == null) return ReviewResultType.INVALID.getReviewResultType();
        return dictum_result.getReviewResultType();
    }

    public Boolean getResolution_created() {
        return resolution_created;
    }

    public String getResolution_result() {
        if (resolution_result == null) return ReviewResultType.INVALID.getReviewResultType();
        return resolution_result.getReviewResultType();
    }

    public Boolean getOpinion_created() {
        return opinion_created;
    }

    public String getOpinion_result() {
        if (opinion_result == null) return ReviewResultType.INVALID.getReviewResultType();
        return opinion_result.getReviewResultType();
    }

    public String getResolution_number() {
        return resolution_number;
    }
}
