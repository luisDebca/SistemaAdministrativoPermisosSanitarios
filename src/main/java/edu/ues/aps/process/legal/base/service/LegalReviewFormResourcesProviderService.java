package edu.ues.aps.process.legal.base.service;

import java.util.Map;

public interface LegalReviewFormResourcesProviderService {

    void setMapValues(Long id);

    Map<String, Object> getMap();
}
