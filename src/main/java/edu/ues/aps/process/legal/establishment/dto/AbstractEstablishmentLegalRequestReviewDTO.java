package edu.ues.aps.process.legal.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentRequestDTO;

public interface AbstractEstablishmentLegalRequestReviewDTO extends AbstractEstablishmentRequestDTO {

    String getDelay();

    Boolean getDictum_created();

    String getDictum_result();

    Boolean getResolution_created();

    String getResolution_result();

    Boolean getOpinion_created();

    String getOpinion_result();

    String getResolution_number();

}
