package edu.ues.aps.process.legal.base.dto;

import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;

import java.time.LocalDateTime;

public interface AbstractLegalFirstReviewReportDTO {

    String getReview_result();

    String getDocument_content();

    String getUser_name();

    void setReview_result(ReviewResultType review_result);

    void setDocument_content(String document_content);

    void setUser_name(String user_name);

    String getUser_charge();

    void setUser_charge(String user_charge);

    String getReview_type();

    void setReview_type(LegalReviewType review_type);

    String getMemo_code();

    String getSubject();

    String getFor_name();

    String getFor_charge();

    String getFrom_name();

    String getFrom_charge();

    String getMemo_content();

    String getMemo_creation_date();

    void setMemo_code(String memo_number);

    void setTo_name(String to_name);

    void setTo_charge(String to_charge);

    void setFrom_name(String from_name);

    void setFrom_charge(String from_charge);

    void setCreate_on(LocalDateTime create_on);

}
