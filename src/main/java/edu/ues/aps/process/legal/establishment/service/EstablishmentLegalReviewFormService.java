package edu.ues.aps.process.legal.establishment.service;

import edu.ues.aps.process.base.service.ClientDTOFinderService;
import edu.ues.aps.process.legal.base.model.ReviewResultType;
import edu.ues.aps.process.legal.base.service.LegalReviewFormResourcesProviderService;
import edu.ues.aps.process.legal.establishment.dto.AbstractLegalFirstReviewRequestDTO;
import edu.ues.aps.process.legal.establishment.dto.EmptyLegalFirstReviewRequestDTO;
import edu.ues.aps.process.legal.establishment.repository.LegalFirstReviewDTOFinderRepository;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.Map;

@Service
@Transactional(readOnly = true)
public class EstablishmentLegalReviewFormService implements InitializingBean, LegalReviewFormResourcesProviderService {

    private final LegalFirstReviewDTOFinderRepository finderRepository;
    private final ClientDTOFinderService clientFinderService;
    private Map<String, Object> map = new HashMap<>();

    public EstablishmentLegalReviewFormService(LegalFirstReviewDTOFinderRepository finderRepository, ClientDTOFinderService clientFinderService) {
        this.finderRepository = finderRepository;
        this.clientFinderService = clientFinderService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        map.put("result_types", ReviewResultType.asMap());
    }

    @Override
    public void setMapValues(Long id) {
        setRequestInformation(id);
        setVerboseDatetime();
    }

    @Override
    public Map<String, Object> getMap() {
        return map;
    }

    private void setRequestInformation(Long id_caseFile) {
        try {
            AbstractLegalFirstReviewRequestDTO requestInformation = finderRepository.findReviewInformation(id_caseFile);
            requestInformation.setId_casefile(id_caseFile);
            setClientList(requestInformation.getId_owner());
            map.put("request", requestInformation);
        } catch (NoResultException e) {
            map.put("request", new EmptyLegalFirstReviewRequestDTO());
        }
    }

    private void setVerboseDatetime() {
        map.put("verbose_date_time", DatetimeUtil.parseDatetimeToVerboseDate(LocalDateTime.now()));
    }

    private void setClientList(Long id_owner) {
        map.put("client_list", clientFinderService.findAllWithNaturalOwner(id_owner));
    }

}
