package edu.ues.aps.process.legal.establishment.repository;

import edu.ues.aps.process.legal.base.dto.AbstractLegalReviewFormDTO;
import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.legal.establishment.dto.AbstractEstablishmentLegalRequestReviewDTO;
import edu.ues.aps.process.legal.establishment.dto.AbstractLegalFirstReviewRequestDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class LegalFirstReviewDTORepository implements LegalFirstReviewDTOFinderRepository {

    private SessionFactory sessionFactory;

    public LegalFirstReviewDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractEstablishmentLegalRequestReviewDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.legal.establishment.dto.EstablishmentLegalRequestReviewDTO(ce.id ,owner.id,establishment.id,concat(concat(ce.request_folder,'-'),ce.request_year),concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year)," +
                "establishment.name,establishment.type_detail,etype.type,section.type,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,ucsf.name,sibasi.zone,ce.certification_type,ce.creation_date,owner.name,document.acceptance_document_created_on," +
                "(select case when dictum is null then false else true end from LegalReview dictum where dictum.casefile.id = ce.id and dictum.review_type = :review_type)," +
                "(select review.result_type from LegalReview review where review.casefile.id = ce.id and review.review_type = :review_type))" +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "left join establishment.type etype " +
                "left join etype.section section " +
                "left join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join ce.controlDocument document " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id", AbstractEstablishmentLegalRequestReviewDTO.class)
                .setParameter("id", ProcessIdentifier.FIRST_LEGAL_REVIEW)
                .setParameter("review_type",LegalReviewType.DICTUM)
                .getResultList();
    }

    @Override
    public AbstractLegalFirstReviewRequestDTO findReviewInformation(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.legal.establishment.dto.LegalFirstReviewRequestDTO(" +
                "establishment.id,owner.id,concat(concat(casefile.request_folder,'-'),casefile.request_year ),establishment.name,establishment.type.type,establishment.type.section.type," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,establishment.type_detail,owner.name,casefile.certification_duration_in_years,casefile.certification_type,document.acceptance_document_created_on," +
                "document.control_document_created_on,ucsf.name,ucsf.sibasi.zone,concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year)) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join casefile.controlDocument document " +
                "left join casefile.ucsf ucsf " +
                "left join casefile.resolution resolution " +
                "where casefile.id = :id ",AbstractLegalFirstReviewRequestDTO.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }

    @Override
    public AbstractLegalReviewFormDTO findReview(Long id_caseFile, LegalReviewType type) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO( " +
                "review.id,review.result_type,review.document,review.to_name,review.to_charge,review.from_name,review.from_charge) " +
                "from Casefile casefile " +
                "inner join casefile.legalReviewls review " +
                "where casefile.id = :id " +
                "and review.review_type = :review_type", AbstractLegalReviewFormDTO.class)
                .setParameter("id", id_caseFile)
                .setParameter("review_type",type)
                .getSingleResult();
    }
}
