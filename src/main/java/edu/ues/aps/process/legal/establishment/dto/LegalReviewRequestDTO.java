package edu.ues.aps.process.legal.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.process.legal.base.model.ReviewResultType;

public class LegalReviewRequestDTO implements AbstractLegalSecondReviewRequestDTO {

    private Long id_caseFile;
    private Long id_establishment;
    private Long id_owner;
    private String folder_number;
    private String request_number;
    private CertificationType certification_type;
    private String ucsf;
    private String sibasi;
    private String establishment_name;
    private String establishment_section;
    private String establishment_type;
    private String establishment_address;
    private String owner_name;
    private Boolean opinion_created;
    private ReviewResultType opinion_result;

    //For legal second review request list
    public LegalReviewRequestDTO(Long id_caseFile, Long id_establishment, Long id_owner, String folder_number, String request_number, CertificationType certification_type, String ucsf, String sibasi, String establishment_name, String establishment_section, String establishment_type, String establishment_address, String owner_name, Boolean opinion_created, ReviewResultType opinion_result) {
        this.id_caseFile = id_caseFile;
        this.id_establishment = id_establishment;
        this.id_owner = id_owner;
        this.folder_number = folder_number;
        this.request_number = request_number;
        this.certification_type = certification_type;
        this.ucsf = ucsf;
        this.sibasi = sibasi;
        this.establishment_name = establishment_name;
        this.establishment_section = establishment_section;
        this.establishment_type = establishment_type;
        this.establishment_address = establishment_address;
        this.owner_name = owner_name;
        this.opinion_created = opinion_created;
        this.opinion_result = opinion_result;
    }

    public Long getId_caseFile() {
        return id_caseFile;
    }

    public Long getId_establishment() {
        return id_establishment;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getCertification_type() {
        if (certification_type != null) {
            return certification_type.getCertificationType();
        }
        return CertificationType.INVALID.getCertificationType();
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getSibasi() {
        return sibasi;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_section() {
        return establishment_section;
    }

    public String getEstablishment_type() {
        return establishment_type;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public Boolean getOpinion_created() {
        return opinion_created;
    }

    public String getOpinion_result() {
        if (opinion_result != null) {
            return opinion_result.getReviewResultType();
        }
        return ReviewResultType.INVALID.getReviewResultType();
    }
}
