package edu.ues.aps.process.legal.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.EmptyCasefile;

import java.time.LocalDateTime;

public class EmptyLegalReview implements AbstractLegalReview {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getTo_name() {
        return "Not found";
    }

    @Override
    public String getNumber() {
        return "0000";
    }

    @Override
    public void setNumber(String number) {

    }

    @Override
    public void setTo_name(String to_name) {

    }

    @Override
    public String getTo_charge() {
        return "Not found";
    }

    @Override
    public void setTo_charge(String to_charge) {

    }

    @Override
    public String getFrom_name() {
        return "Not found";
    }

    @Override
    public void setFrom_name(String from_name) {

    }

    @Override
    public String getFrom_charge() {
        return "Not found";
    }

    @Override
    public void setFrom_charge(String from_charge) {

    }

    @Override
    public LegalReviewType getReview_type() {
        return LegalReviewType.INVALID;
    }

    @Override
    public ReviewResultType getResult_type() {
        return ReviewResultType.INVALID;
    }

    @Override
    public LocalDateTime getCreate_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public String getDocument() {
        return "Legal review document content not found";
    }

    @Override
    public AbstractCasefile getCasefile() {
        return new EmptyCasefile();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setReview_type(LegalReviewType review_type) {

    }

    @Override
    public void setResult_type(ReviewResultType result_type) {

    }

    @Override
    public void setCreate_on(LocalDateTime create_on) {

    }

    @Override
    public void setDocument(String document_content) {

    }

    @Override
    public void setCasefile(Casefile casefile) {

    }

    @Override
    public String toString() {
        return "EmptyLegalReview{}";
    }
}
