package edu.ues.aps.process.delivery.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentRequestDTO;

public interface AbstractEstablishmentCompletedRequestDTO extends AbstractEstablishmentRequestDTO {

    String getResolution_number();

    Integer getDuration_years();

    String getCertification_start();

    String getCertification_end();

    Boolean getUser_delivery();

    String getCertification_status();

}
