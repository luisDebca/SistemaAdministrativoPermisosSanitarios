package edu.ues.aps.process.delivery.establishment.service;

import edu.ues.aps.process.delivery.establishment.dto.AbstractEstablishmentCompletedRequestDTO;

import java.util.List;

public interface EstablishmentCompletedRequestFinderService {

    List<AbstractEstablishmentCompletedRequestDTO> findAllRequest();
}
