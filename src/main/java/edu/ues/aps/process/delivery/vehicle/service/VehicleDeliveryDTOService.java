package edu.ues.aps.process.delivery.vehicle.service;

import edu.ues.aps.process.delivery.vehicle.dto.AbstractVehicleDeliveryDTO;
import edu.ues.aps.process.delivery.vehicle.repository.VehicleDeliveryDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleDeliveryDTOService implements VehicleDeliveryDTOFinderService {

    private final VehicleDeliveryDTOFinderRepository finderRepository;

    public VehicleDeliveryDTOService(VehicleDeliveryDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractVehicleDeliveryDTO> findAll() {
        return finderRepository.findAll();
    }
}
