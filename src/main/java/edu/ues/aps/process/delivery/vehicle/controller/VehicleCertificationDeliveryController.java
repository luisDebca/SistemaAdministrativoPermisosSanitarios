package edu.ues.aps.process.delivery.vehicle.controller;

import edu.ues.aps.process.delivery.vehicle.service.VehicleDeliveryDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/request/delivery/vehicle")
public class VehicleCertificationDeliveryController {

    private static final Logger logger = LogManager.getLogger(VehicleCertificationDeliveryController.class);

    private static final String DELIVERY = "request_delivery_vehicle_list";

    private final VehicleDeliveryDTOFinderService finderService;

    public VehicleCertificationDeliveryController(VehicleDeliveryDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showAllCertificationDelivery(ModelMap model) {
        logger.info("GET:: Show all vehicle certification deliveries.");
        model.addAttribute("requests", finderService.findAll());
        return DELIVERY;
    }
}
