package edu.ues.aps.process.delivery.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.EstablishmentRequestDTO;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class EstablishmentCompletedRequestDTO extends EstablishmentRequestDTO implements AbstractEstablishmentCompletedRequestDTO {

    private String resolution_number;
    private Integer duration_years;
    private LocalDateTime certification_start;
    private Boolean user_delivery;
    private CasefileStatusType certification_status;

    public EstablishmentCompletedRequestDTO(Long id_caseFile, Long id_owner, Long id_establishment, String folder_number, String request_number, String establishment_name, String establishment_type_detail, String establishment_type, String establishment_section, String establishment_address, String establishment_nit, String UCSF, String SIBASI, CertificationType certification_type, LocalDateTime caseFile_createOn, String owner_name, String resolution_number, Integer duration_years, LocalDateTime certification_start, Boolean user_delivery, CasefileStatusType certification_status) {
        super(id_caseFile, id_owner, id_establishment, folder_number, request_number, establishment_name, establishment_type_detail, establishment_type, establishment_section, establishment_address, establishment_nit, UCSF, SIBASI, certification_type, caseFile_createOn, owner_name);
        this.resolution_number = resolution_number;
        this.duration_years = duration_years;
        this.certification_start = certification_start;
        this.user_delivery = user_delivery;
        this.certification_status = certification_status;
    }

    public String getResolution_number() {
        return resolution_number;
    }

    public Integer getDuration_years() {
        return duration_years;
    }

    public String getCertification_start() {
        if (certification_start == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(certification_start.toLocalDate());
    }

    public String getCertification_end() {
        if (certification_start == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(certification_start.toLocalDate().plusYears(duration_years));
    }

    public Boolean getUser_delivery() {
        return certification_status.equals(CasefileStatusType.VALID);
    }

    public String getCertification_status() {
        if (certification_status == null) return CasefileStatusType.INVALID.getStatus_type();
        return certification_status.getStatus_type();
    }
}
