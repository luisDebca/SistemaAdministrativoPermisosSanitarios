package edu.ues.aps.process.delivery.establishment.service;

import edu.ues.aps.process.delivery.establishment.dto.AbstractEstablishmentCompletedRequestDTO;
import edu.ues.aps.process.delivery.establishment.repository.EstablishmentCompletedRequestFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class EstablishmentCompletedRequestService implements EstablishmentCompletedRequestFinderService {

    private final EstablishmentCompletedRequestFinderRepository finderRepository;

    public EstablishmentCompletedRequestService(EstablishmentCompletedRequestFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractEstablishmentCompletedRequestDTO> findAllRequest() {
        return finderRepository.findAllRequest();
    }
}
