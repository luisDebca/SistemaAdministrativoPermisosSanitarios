package edu.ues.aps.process.delivery.vehicle.service;

import edu.ues.aps.process.delivery.vehicle.dto.AbstractVehicleDeliveryDTO;

import java.util.List;

public interface VehicleDeliveryDTOFinderService {

    List<AbstractVehicleDeliveryDTO> findAll();
}
