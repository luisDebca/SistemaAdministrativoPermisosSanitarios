package edu.ues.aps.process.delivery.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.VehicleRequestDTO;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class VehicleDeliveryDTO extends VehicleRequestDTO implements AbstractVehicleDeliveryDTO {

    private Integer duration;
    private LocalDateTime certification_start;
    private LocalDateTime certification_end;
    private CasefileStatusType certification_status;

    public VehicleDeliveryDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on, Integer duration, LocalDateTime certification_start, CasefileStatusType certification_status) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
        this.duration = duration;
        this.certification_start = certification_start;
        this.certification_status = certification_status;
    }

    public Integer getDuration() {
        return duration;
    }

    public String getCertification_start() {
        if (certification_start == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(certification_start.toLocalDate());
    }

    public String getCertification_end() {
        if (certification_start == null || duration ==  null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(certification_start.toLocalDate().plusYears(duration));
    }

    public String getCertification_status() {
        if (certification_status == null) return CertificationType.INVALID.getCertificationType();
        return certification_status.getStatus_type();
    }

    public Boolean isUserDelivered(){
        return certification_status.equals(CasefileStatusType.VALID);
    }
}
