package edu.ues.aps.process.delivery.vehicle.repository;

import edu.ues.aps.process.delivery.vehicle.dto.AbstractVehicleDeliveryDTO;

import java.util.List;

public interface VehicleDeliveryDTOFinderRepository {

    List<AbstractVehicleDeliveryDTO> findAll();
}
