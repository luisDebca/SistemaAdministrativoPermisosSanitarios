package edu.ues.aps.process.delivery.base.async;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.delivery.base.service.RequestCompletionPersistenceService;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
@RequestMapping("/request/delivery")
public class RequestProcessCompletionPersistenceController {

    private static final Logger logger = LogManager.getLogger(RequestProcessCompletionPersistenceController.class);

    private final ProcessUserRegisterService registerService;
    private final VehicleBunchInformationService informationService;
    private final RequestCompletionPersistenceService persistenceService;

    public RequestProcessCompletionPersistenceController(ProcessUserRegisterService registerService, RequestCompletionPersistenceService persistenceService, VehicleBunchInformationService informationService) {
        this.registerService = registerService;
        this.persistenceService = persistenceService;
        this.informationService = informationService;
    }

    @GetMapping(value = "/case-file/complete-process", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse caseFileToCertification(@RequestParam Long id_caseFile, Principal principal) {
        logger.info("GET:: Finalize case file {} process.", id_caseFile);
        persistenceService.passCaseFileToCertifications(id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.completion");
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @GetMapping(value = "/bunch/complete-process", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse bunchToCertification(@RequestParam Long id_bunch, Principal principal) {
        logger.info("GET:: Finalize bunch {} process", id_bunch);
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            persistenceService.passCaseFileToCertifications(id_caseFile);
            registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.completion");
        }
        return new SingleStringResponse("OK").asSuccessResponse();
    }
}
