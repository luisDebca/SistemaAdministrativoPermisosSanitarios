package edu.ues.aps.process.delivery.base.service;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.delivery.base.repository.RequestCompletionPersistenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RequestCompletionService implements RequestCompletionPersistenceService {

    private final RequestCompletionPersistenceRepository persistenceRepository;

    public RequestCompletionService(RequestCompletionPersistenceRepository persistenceRepository) {
        this.persistenceRepository = persistenceRepository;
    }

    @Override
    public void passCaseFileToCertifications(Long id_caseFile) {
        persistenceRepository.updateStatus(id_caseFile, CasefileStatusType.VALID);
    }
}
