package edu.ues.aps.process.delivery.establishment.repository;

import edu.ues.aps.process.delivery.establishment.dto.AbstractEstablishmentCompletedRequestDTO;

import java.util.List;

public interface EstablishmentCompletedRequestFinderRepository {

    List<AbstractEstablishmentCompletedRequestDTO> findAllRequest();
}
