package edu.ues.aps.process.delivery.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleRequestDTO;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

public interface AbstractVehicleDeliveryDTO extends AbstractVehicleRequestDTO {

    Integer getDuration();

    String getCertification_start();

    String getCertification_end();

    String getCertification_status();

    Boolean isUserDelivered();

}
