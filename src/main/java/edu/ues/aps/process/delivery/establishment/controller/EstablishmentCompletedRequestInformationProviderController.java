package edu.ues.aps.process.delivery.establishment.controller;

import edu.ues.aps.process.delivery.establishment.service.EstablishmentCompletedRequestFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/request/delivery/establishment")
public class EstablishmentCompletedRequestInformationProviderController {

    private static final Logger logger = LogManager.getLogger(EstablishmentCompletedRequestInformationProviderController.class);

    private static final String ALL_REQUEST = "request_delivery_establishment_list";

    private final EstablishmentCompletedRequestFinderService finderService;

    public EstablishmentCompletedRequestInformationProviderController(EstablishmentCompletedRequestFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showRequestDeliveryIndex(ModelMap model) {
        logger.info("GET:: Show all completed request for establishment.");
        model.addAttribute("requests", finderService.findAllRequest());
        return ALL_REQUEST;
    }

}
