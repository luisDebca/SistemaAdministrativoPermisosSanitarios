package edu.ues.aps.process.delivery.base.service;

public interface RequestCompletionPersistenceService {

    void passCaseFileToCertifications(Long id_caseFile);
}
