package edu.ues.aps.process.delivery.vehicle.repository;

import edu.ues.aps.process.delivery.vehicle.dto.AbstractVehicleDeliveryDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleDeliveryDTORepository implements VehicleDeliveryDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleDeliveryDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleDeliveryDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.delivery.vehicle.dto.VehicleDeliveryDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date, cv.certification_duration_in_years, cv.certification_start_on, cv.status)" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehicleDeliveryDTO.class)
                .setParameter("id", ProcessIdentifier.CERTIFICATION_STACK)
                .getResultList();
    }
}
