package edu.ues.aps.process.delivery.base.repository;

import edu.ues.aps.process.base.model.CasefileStatusType;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RequestCompletionRepository implements RequestCompletionPersistenceRepository {

    private final SessionFactory sessionFactory;

    public RequestCompletionRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void updateStatus(Long id_caseFile, CasefileStatusType status) {
        sessionFactory.getCurrentSession().createQuery("update Casefile cf set cf.status = :st where cf.id = :id")
                .setParameter("st", status)
                .setParameter("id", id_caseFile)
                .executeUpdate();
    }
}
