package edu.ues.aps.process.delivery.base.repository;

import edu.ues.aps.process.base.model.CasefileStatusType;

public interface RequestCompletionPersistenceRepository {

    void updateStatus(Long id_caseFile, CasefileStatusType status);
}
