package edu.ues.aps.process.delivery.base.controller;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/request/delivery")
public class RequestDeliveryIndexController {

    private static final Logger logger = LogManager.getLogger(RequestDeliveryIndexController.class);

    private static final String INDEX = "request_delivery_index";

    @GetMapping("/index")
    public String showRequestDeliveryIndex(ModelMap model){
        logger.info("GET:: Show all request completed for user delivery.");
        model.addAttribute("process", ProcessIdentifier.CERTIFICATION_STACK);
        return INDEX;
    }
}
