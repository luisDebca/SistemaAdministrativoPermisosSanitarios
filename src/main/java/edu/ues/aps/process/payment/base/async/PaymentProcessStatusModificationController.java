package edu.ues.aps.process.payment.base.async;

import edu.ues.aps.process.payment.base.service.PaymentProcessPersistenceService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;

import java.security.Principal;

public abstract class PaymentProcessStatusModificationController {

    private final PaymentProcessPersistenceService persistenceService;

    public PaymentProcessStatusModificationController(PaymentProcessPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public abstract SingleStringResponse setPaymentDeliveryStatus(Long id, Principal principal);

    public abstract SingleStringResponse setPaymentCancellationStatus(Long id, Principal principal);

    public void setDeliveryDate(Long id) {
        persistenceService.setDeliveryDate(id);
    }

    public void setCancelledStatus(Long id) {
        persistenceService.setPaymentCancelled(id);
    }

}
