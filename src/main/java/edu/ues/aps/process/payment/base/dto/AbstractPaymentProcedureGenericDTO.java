package edu.ues.aps.process.payment.base.dto;

public interface AbstractPaymentProcedureGenericDTO {

    Long getId_casefile();

    void setId_casefile(Long id_casefile);

    String getSubject();

    void setSubject(String subject);

    String getSubject_address();

    void setSubject_address(String subject_address);

    String getOwner_name();

    void setOwner_name(String owner_name);

    String getPayment_rate_estimated_description();

    void setPayment_rate_estimated_description(String payment_rate_estimated_description);

    Double getPayment_rate_estimated_cost();

    void setPayment_rate_estimated_cost(Double payment_rate_estimated_cost);
}
