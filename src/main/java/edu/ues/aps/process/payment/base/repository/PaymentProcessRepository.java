package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.payment.base.model.AbstractPaymentProcedure;
import edu.ues.aps.process.payment.base.model.PaymentProcedure;
import edu.ues.aps.process.payment.base.model.PaymentRate;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

@Repository
public class PaymentProcessRepository implements PaymentProcessPersistenceRepository {

    private final SessionFactory sessionFactory;

    public PaymentProcessRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void savePaymentProcedure(Long id_caseFile, PaymentProcedure procedure) throws NoResultException {
        AbstractCasefile caseFile = sessionFactory.getCurrentSession().get(Casefile.class, id_caseFile);
        caseFile.addPaymentProcedure(procedure);
        sessionFactory.getCurrentSession().saveOrUpdate(caseFile);
        sessionFactory.getCurrentSession().flush();
    }

    @Override
    public void updatePaymentProcedure(Long id_caseFile, String rate_code) throws NoResultException {
        PaymentRate rate = sessionFactory.getCurrentSession().createQuery("select rate " +
                "from PaymentRate rate where rate.code = :code", PaymentRate.class)
                .setParameter("code", rate_code)
                .getSingleResult();
        AbstractPaymentProcedure procedure = sessionFactory.getCurrentSession().get(PaymentProcedure.class, id_caseFile);
        procedure.setRate(rate);
        sessionFactory.getCurrentSession().saveOrUpdate(procedure);
    }

    @Override
    public void setDeliveryDate(Long id_caseFile) {
        sessionFactory.getCurrentSession().createQuery("update PaymentProcedure payment " +
                "set payment.delivered_on = current_timestamp " +
                "where payment.id = :id")
                .setParameter("id", id_caseFile)
                .executeUpdate();
    }

    @Override
    public void setPaymentCancelled(Long id_caseFile) {
        sessionFactory.getCurrentSession().createQuery("update PaymentProcedure payment " +
                "set payment.cancelled = true, payment.cancelled_on = current_timestamp " +
                "where payment.id = :id")
                .setParameter("id", id_caseFile)
                .executeUpdate();
    }

    @Override
    public Long getLastPaymentNumber() {
        Long paymentDefault = executeProcedure("getDefaultPaymentNumber");
        Long paymentMax = executeProcedure("getMaxPaymentNumber");
        return paymentDefault >= paymentMax ? paymentDefault : paymentMax;
    }

    private Long executeProcedure(String procedure) {
        StoredProcedureQuery query = sessionFactory.getCurrentSession().createStoredProcedureQuery(procedure).
                registerStoredProcedureParameter("count", Long.class, ParameterMode.OUT);
        query.execute();
        if (query.getOutputParameterValue("count") != null) {
            return (Long) query.getOutputParameterValue("count");
        } else {
            return 0L;
        }
    }
}
