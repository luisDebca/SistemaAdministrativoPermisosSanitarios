package edu.ues.aps.process.payment.base.service;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentRateDTO;
import edu.ues.aps.process.payment.base.repository.PaymentRateDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class PaymentRateDTOService implements PaymentRateDTOFinderService{

    private PaymentRateDTOFinderRepository finderRepository;

    public PaymentRateDTOService(PaymentRateDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractPaymentRateDTO> findAllPaymentRates() {
        return finderRepository.findAll();
    }
}
