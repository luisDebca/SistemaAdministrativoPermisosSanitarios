package edu.ues.aps.process.payment.vehicle.repository;

import edu.ues.aps.process.payment.vehicle.dto.AbstractVehiclePaymentProcessDTO;

import java.util.List;

public interface VehiclePaymentProcessFinderRepository {

    List<AbstractVehiclePaymentProcessDTO> findAllRequestWithPaymentDeliveryPending();

    List<AbstractVehiclePaymentProcessDTO> findAllRequestWithPaymentCancellationPending();

}
