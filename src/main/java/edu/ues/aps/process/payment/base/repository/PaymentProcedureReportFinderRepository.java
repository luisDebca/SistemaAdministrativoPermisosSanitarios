package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentReportDTO;

public interface PaymentProcedureReportFinderRepository<N> {

    Class<N> appliesTo();

    AbstractPaymentReportDTO getReportInformation(Long id_caseFile);
}
