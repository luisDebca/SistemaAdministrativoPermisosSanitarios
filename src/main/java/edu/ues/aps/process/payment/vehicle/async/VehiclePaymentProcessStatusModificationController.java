package edu.ues.aps.process.payment.vehicle.async;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.payment.base.async.PaymentProcessStatusModificationController;
import edu.ues.aps.process.payment.base.service.PaymentProcessPersistenceService;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
@RequestMapping("/payment/bunch")
public class VehiclePaymentProcessStatusModificationController extends PaymentProcessStatusModificationController {

    private static final Logger logger = LogManager.getLogger(VehiclePaymentProcessStatusModificationController.class);

    private final ProcessUserRegisterService registerService;
    private final VehicleBunchInformationService informationService;

    public VehiclePaymentProcessStatusModificationController(PaymentProcessPersistenceService persistenceService, ProcessUserRegisterService registerService, VehicleBunchInformationService informationService) {
        super(persistenceService);
        this.registerService = registerService;
        this.informationService = informationService;
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/check-payment-procedure-delivery-date", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setPaymentDeliveryStatus(Long id_bunch, Principal principal) {
        logger.info("GET:: Set all payment delivery date in bunch {}.", id_bunch);
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            setDeliveryDate(id_caseFile);
            registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.payment.delivered");
        }
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/check-payment-procedure-cancellation", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setPaymentCancellationStatus(Long id_bunch, Principal principal) {
        logger.info("GET:: Set all payment cancelled status in bunch {}.", id_bunch);
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            setCancelledStatus(id_caseFile);
            registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.payment.cancellation");
        }
        return new SingleStringResponse("OK").asSuccessResponse();
    }
}
