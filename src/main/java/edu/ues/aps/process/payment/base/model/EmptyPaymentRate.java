package edu.ues.aps.process.payment.base.model;

import java.util.Collections;
import java.util.List;

public class EmptyPaymentRate implements AbstractPaymentRate{

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getCode() {
        return "Code not found";
    }

    @Override
    public void setCode(String code) {

    }

    @Override
    public String getDescription() {
        return "Payment rate not found";
    }

    @Override
    public void setDescription(String description) {

    }

    @Override
    public double getMinimum_value() {
        return 0;
    }

    @Override
    public void setMinimum_value(double minimun_value) {

    }

    @Override
    public double getMaximum_value() {
        return 0;
    }

    @Override
    public void setMaximum_value(double maximun_value) {

    }

    @Override
    public double getCost() {
        return 0;
    }

    @Override
    public void setCost(double cost) {

    }

    @Override
    public List<PaymentProcedure> getProcedures() {
        return Collections.emptyList();
    }

    @Override
    public void setProcedures(List<PaymentProcedure> procedures) {

    }
}
