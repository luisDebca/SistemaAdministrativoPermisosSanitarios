package edu.ues.aps.process.payment.base.service;

import edu.ues.aps.process.payment.base.model.PaymentRate;
import edu.ues.aps.process.payment.base.repository.PaymentRateCrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PaymentRateService implements PaymentRateFinderService {

    private PaymentRateCrudRepository crudRepository;

    public PaymentRateService(PaymentRateCrudRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @Override
    public PaymentRate findRateByCode(String rate_code) {
        return crudRepository.findByCode(rate_code);
    }
}
