package edu.ues.aps.process.payment.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class PaymentProcessDTO implements AbstractPaymentProcessDTO {

    private Long id_casefile;
    private Long id_establishment;
    private Long id_owner;
    private String folder_number;
    private String establishment_name;
    private String establishment_address;
    private String establishment_type;
    private String establishment_section;
    private Double establishment_capital;
    private String establishment_type_details;
    private String owner_name;
    private CertificationType casefile_certification_type;
    private LocalDateTime legal_first_resolution_created_on;
    private LocalDateTime payment_cancelled_on;
    private LocalDateTime payment_created_on;
    private LocalDateTime payment_delivered_on;
    private double payment_cost;
    private boolean cancelled;
    private boolean date_for_payment_expired;
    private boolean payment_created;

    public PaymentProcessDTO(Long id_establishment, Long id_owner, String folder_number, String establishment_name, String establishment_address, String establishment_type, String establishment_section, Double establishment_capital, String establishment_type_details, String owner_name, CertificationType casefile_certification_type) {
        this.id_establishment = id_establishment;
        this.id_owner = id_owner;
        this.folder_number = folder_number;
        this.establishment_name = establishment_name;
        this.establishment_address = establishment_address;
        this.establishment_type = establishment_type;
        this.establishment_section = establishment_section;
        this.establishment_capital = establishment_capital;
        this.establishment_type_details = establishment_type_details;
        this.owner_name = owner_name;
        this.casefile_certification_type = casefile_certification_type;
    }

    public PaymentProcessDTO(Long id_casefile, Long id_establishment, Long id_owner, String folder_number, String establishment_name, String establishment_address, String establishment_type, String establishment_section, String owner_name, LocalDateTime legal_first_resolution_created_on, boolean payment_created,LocalDateTime payment_delivered_on) {
        this.id_casefile = id_casefile;
        this.id_establishment = id_establishment;
        this.id_owner = id_owner;
        this.folder_number = folder_number;
        this.establishment_name = establishment_name;
        this.establishment_address = establishment_address;
        this.establishment_type = establishment_type;
        this.establishment_section = establishment_section;
        this.owner_name = owner_name;
        this.legal_first_resolution_created_on = legal_first_resolution_created_on;
        this.payment_created = payment_created;
        this.payment_delivered_on = payment_delivered_on;
    }

    public PaymentProcessDTO(Long id_casefile, Long id_establishment, Long id_owner, String folder_number, String establishment_name, String establishment_address, String establishment_type, String establishment_section, String owner_name, LocalDateTime payment_cancelled_on, LocalDateTime payment_created_on, LocalDateTime payment_delivered_on, double payment_cost, boolean cancelled) {
        this.id_casefile = id_casefile;
        this.id_establishment = id_establishment;
        this.id_owner = id_owner;
        this.folder_number = folder_number;
        this.establishment_name = establishment_name;
        this.establishment_address = establishment_address;
        this.establishment_type = establishment_type;
        this.establishment_section = establishment_section;
        this.owner_name = owner_name;
        this.payment_cancelled_on = payment_cancelled_on;
        this.payment_created_on = payment_created_on;
        this.payment_delivered_on = payment_delivered_on;
        this.payment_cost = payment_cost;
        this.cancelled = cancelled;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public Long getId_establishment() {
        return id_establishment;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getEstablishment_type() {
        return establishment_type;
    }

    public String getEstablishment_section() {
        return establishment_section;
    }

    public Double getEstablishment_capital() {
        return establishment_capital;
    }

    public String getEstablishment_type_details() {
        return establishment_type_details;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getCasefile_certification_type() {
        if (casefile_certification_type == null)
            return CertificationType.INVALID.getCertificationType();
        else
            return casefile_certification_type.getCertificationType();
    }

    public String getLegal_first_resolution_created_on() {
        return DatetimeUtil.parseDatetimeToHtmlFormat(legal_first_resolution_created_on);
    }

    public String getPayment_cancelled_on() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(payment_cancelled_on);
    }

    public String getPayment_created_on() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(payment_created_on);
    }

    public String getPayment_delivered_on() {
        return DatetimeUtil.parseDatetimeToHtmlFormat(payment_delivered_on);
    }

    public double getPayment_cost() {
        return payment_cost;
    }

    public boolean isDelivery(){
        return payment_delivered_on != null;
    }

    public boolean isCancelled() {
        return cancelled;
    }

    public boolean isDate_for_payment_expired() {
        return date_for_payment_expired;
    }

    public boolean isPayment_created() {
        return payment_created;
    }

    public void setDate_for_payment_expired(boolean date_for_payment_expired) {
        this.date_for_payment_expired = date_for_payment_expired;
    }

    public String getLegal_first_resolution_30days_date() {
        return DatetimeUtil.parseDatetimeToHtmlFormat(legal_first_resolution_created_on.plusDays(30));
    }

    public String getPayment_delivered_30days_date() {
        return DatetimeUtil.parseDatetimeToHtmlFormat(payment_delivered_on.plusDays(30));
    }
}
