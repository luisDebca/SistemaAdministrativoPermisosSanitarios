package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.base.model.AbstractCasefile;

import java.time.LocalDateTime;

public interface PaymentProcedureQuantityRepository {

    Long getPaymentProcedureCountSince(Class casefile_class, LocalDateTime since);

    Double getRaisedMoneyFromPaymentProcessSince(Class casefile_class, LocalDateTime since);

}
