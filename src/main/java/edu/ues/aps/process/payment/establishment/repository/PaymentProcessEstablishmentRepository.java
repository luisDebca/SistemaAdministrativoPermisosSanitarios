package edu.ues.aps.process.payment.establishment.repository;

import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.payment.establishment.dto.AbstractEstablishmentPaymentRequestDTO;
import edu.ues.aps.process.payment.establishment.dto.AbstractPaymentProcessDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.requirement.establishment.dto.AbstractEstablishmentControlDocumentDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class PaymentProcessEstablishmentRepository implements PaymentProcessFinderRepository{

    private SessionFactory sessionFactory;

    public PaymentProcessEstablishmentRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractPaymentProcessDTO findPaymentProcessInformation(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.payment.establishment.dto.PaymentProcessDTO(" +
                "establishment.id,owner.id,establishment.name,concat(concat(casefile.request_folder,'-'),casefile.request_year) ," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.type.type,establishment.type.section.type,establishment.capital,establishment.type_detail,owner.name,casefile.certification_type) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.address address " +
                "inner join establishment.owner owner " +
                "where casefile.id = :id",AbstractPaymentProcessDTO.class)
                .setParameter("id",id_caseFile)
                .getSingleResult();
    }

    @Override
    public List<AbstractEstablishmentPaymentRequestDTO> findAllRequestFromPaymentProcessPendingDelivery() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.payment.establishment.dto.EstablishmentPaymentRequestDTO(ce.id ,owner.id,establishment.id,concat(concat(ce.request_folder,'-'),ce.request_year),concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year)," +
                "establishment.name,establishment.type_detail,etype.type,section.type,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,ucsf.name,sibasi.zone,ce.certification_type,ce.creation_date,owner.name," +
                "(select resolution.create_on from LegalReview resolution where resolution.casefile.id = ce.id and resolution.review_type = :review)," +
                "payment.create_on,payment.delivered_on)" +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "left join establishment.type etype " +
                "left join etype.section section " +
                "left join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "left join ce.paymentProcedure payment " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id", AbstractEstablishmentPaymentRequestDTO.class)
                .setParameter("id", ProcessIdentifier.PAYMENT_PROCEDURE_START)
                .setParameter("review",LegalReviewType.RESOLUTION)
                .getResultList();
    }

    @Override
    public List<AbstractEstablishmentPaymentRequestDTO> findAllRequestFromPaymentProcessPendingCancellation() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.payment.establishment.dto.EstablishmentPaymentRequestDTO(ce.id ,owner.id,establishment.id,concat(concat(ce.request_folder,'-'),ce.request_year),concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year)," +
                "establishment.name,establishment.type_detail,etype.type,section.type,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,ucsf.name,sibasi.zone,ce.certification_type,ce.creation_date,owner.name," +
                "(select resolution.create_on from LegalReview resolution where resolution.casefile.id = ce.id and resolution.review_type = :review)," +
                "payment.create_on,payment.delivered_on,payment.cancelled_on,payment.expired_payment_date,payment.cancelled,rate.cost)" +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "left join establishment.type etype " +
                "left join etype.section section " +
                "left join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join ce.paymentProcedure payment " +
                "inner join payment.rate rate " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id", AbstractEstablishmentPaymentRequestDTO.class)
                .setParameter("id", ProcessIdentifier.PAYMENT_PROCEDURE_END)
                .setParameter("review",LegalReviewType.RESOLUTION)
                .getResultList();
    }
}
