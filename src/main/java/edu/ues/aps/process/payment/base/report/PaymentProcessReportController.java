package edu.ues.aps.process.payment.base.report;

import edu.ues.aps.process.base.model.Client;
import edu.ues.aps.process.base.model.ClientType;
import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.base.repository.NaturalOwnerFinderRepository;
import edu.ues.aps.process.base.service.OwnerDTOFinderService;
import edu.ues.aps.process.base.service.SingleClientInformationService;
import edu.ues.aps.process.payment.base.dto.AbstractPaymentReportDTO;
import edu.ues.aps.process.payment.base.service.PaymentProcedureReportInfoFinderService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import javax.persistence.NoResultException;
import java.security.Principal;
import java.util.Collections;

@Controller
@RequestMapping("/payment")
public class PaymentProcessReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(PaymentProcessReportController.class);

    private static final String REPORT_NAME = "PaymentProcedureReport";

    private final SinglePersonalInformationService personalInformationService;
    private final PaymentProcedureReportInfoFinderService finderService;
    private final SingleClientInformationService clientInformationService;
    private final OwnerDTOFinderService ownerFinderRepository;

    public PaymentProcessReportController(PaymentProcedureReportInfoFinderService finderService, SingleClientInformationService clientInformationService, SinglePersonalInformationService personalInformationService, OwnerDTOFinderService ownerFinderRepository) {
        this.finderService = finderService;
        this.clientInformationService = clientInformationService;
        this.personalInformationService = personalInformationService;
        this.ownerFinderRepository = ownerFinderRepository;
    }

    @GetMapping("/payment-procedure-report.pdf")
    public String generatePaymentProcessReport(@RequestParam Long id_caseFile, @RequestParam Long id_client, @RequestParam String client_type, Principal principal, ModelMap model) {
        logger.info("REPORT:: Generate payment procedure report for case file {}", id_caseFile);
        AbstractPaymentReportDTO reportDTO = finderService.findPaymentProcessReportInfo(id_caseFile);
        reportDTO.setClient_name(getClientName(getClientType(client_type), id_client));
        reportDTO.setClient_telephone(getClientPhone(getClientType(client_type), id_client));
        reportDTO.setUser_name(personalInformationService.getFullName(principal.getName()));
        addReportRequiredSources(Collections.singletonList(reportDTO));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }

    private String getClientName(ClientType type, Long id) {
        try {
            if (type.equals(ClientType.PROPIETARIO)) {
                return ownerFinderRepository.findNaturalEntity(id).getOwner_name();
            } else {
                return clientInformationService.getClientName(id);
            }
        } catch (NoResultException e) {
            return "N/A";
        }
    }

    private String getClientPhone(ClientType type, Long id) {
        try {
            if (type.equals(ClientType.PROPIETARIO)) {
                return ownerFinderRepository.findNaturalEntity(id).getOwner_telephone();
            } else {
                return clientInformationService.getClientPhoneNumber(id);
            }
        } catch (NoResultException e) {
            return "N/A";
        }
    }

    private ClientType getClientType(String type) {
        for (ClientType item : ClientType.values()) {
            if (item.getClientType().equalsIgnoreCase(type))
                return item;
        }
        return ClientType.INVALID;
    }
}
