package edu.ues.aps.process.payment.base.service;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentRateDTO;

import java.util.List;

public interface PaymentRateDTOFinderService {

    List<AbstractPaymentRateDTO> findAllPaymentRates();
}
