package edu.ues.aps.process.payment.establishment.service;

import edu.ues.aps.process.payment.establishment.dto.AbstractEstablishmentPaymentRequestDTO;
import edu.ues.aps.process.payment.establishment.dto.AbstractPaymentProcessDTO;

import java.util.List;

public interface PaymentProcessFinderService {

    AbstractPaymentProcessDTO findPaymentProcessCaseFileInformation(Long id_caseFile);

    List<AbstractEstablishmentPaymentRequestDTO> findAllRequestFromPaymentProcessWithPaymentPendingDelivery();

    List<AbstractEstablishmentPaymentRequestDTO> findAllRequestFromPaymentProcessWithPaymentPendingCancellation();

}
