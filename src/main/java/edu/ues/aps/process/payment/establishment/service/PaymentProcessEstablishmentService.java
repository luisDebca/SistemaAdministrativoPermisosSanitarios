package edu.ues.aps.process.payment.establishment.service;

import edu.ues.aps.process.payment.establishment.dto.AbstractEstablishmentPaymentRequestDTO;
import edu.ues.aps.process.payment.establishment.dto.AbstractPaymentProcessDTO;
import edu.ues.aps.process.payment.establishment.dto.EmptyPaymentProcessDTO;
import edu.ues.aps.process.payment.establishment.repository.PaymentProcessFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class PaymentProcessEstablishmentService implements PaymentProcessFinderService{

    private PaymentProcessFinderRepository finderRepository;

    public PaymentProcessEstablishmentService(PaymentProcessFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractPaymentProcessDTO findPaymentProcessCaseFileInformation(Long id_caseFile) {
        try {
            return finderRepository.findPaymentProcessInformation(id_caseFile);
        }catch (NoResultException e){
            return new EmptyPaymentProcessDTO();
        }
    }

    @Override
    public List<AbstractEstablishmentPaymentRequestDTO> findAllRequestFromPaymentProcessWithPaymentPendingCancellation() {
        return finderRepository.findAllRequestFromPaymentProcessPendingCancellation();
    }

    @Override
    public List<AbstractEstablishmentPaymentRequestDTO> findAllRequestFromPaymentProcessWithPaymentPendingDelivery() {
        return finderRepository.findAllRequestFromPaymentProcessPendingDelivery();
    }
}
