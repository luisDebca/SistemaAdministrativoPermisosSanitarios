package edu.ues.aps.process.payment.base.dto;

public interface AbstractPaymentRateDTO {

    Long getRate_id();

    void setRate_id(Long rate_id);

    String getRate_code();

    void setRate_code(String rate_code);

    String getRate_description();

    void setRate_description(String rate_description);

    Double getRate_cost();

    void setRate_cost(Double rate_cost);

    Double getRate_maximum();

    void setRate_maximum(Double rate_maximum);

    Double getRate_minimum();

    void setRate_minimum(Double rate_minimum);

}
