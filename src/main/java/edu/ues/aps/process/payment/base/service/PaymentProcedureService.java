package edu.ues.aps.process.payment.base.service;

import edu.ues.aps.process.area.establishment.model.CasefileEstablishment;
import edu.ues.aps.process.area.vehicle.model.CasefileVehicle;
import edu.ues.aps.process.payment.base.dto.AbstractPaymentProcedureGenericDTO;
import edu.ues.aps.process.payment.base.repository.PaymentProcedureQuantityRepository;
import edu.ues.aps.process.payment.base.repository.PaymentProcedureSummaryRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;


@Service
@Transactional(readOnly = true)
public class PaymentProcedureService implements PaymentProcedureRequestQuantityInformationService, PaymentProcedureRequestSummaryService {

    private PaymentProcedureSummaryRepository summaryRepository;
    private PaymentProcedureQuantityRepository quantityRepository;

    public PaymentProcedureService(PaymentProcedureSummaryRepository summaryRepository, PaymentProcedureQuantityRepository quantityRepository) {
        this.summaryRepository = summaryRepository;
        this.quantityRepository = quantityRepository;
    }

    @Override
    public List<AbstractPaymentProcedureGenericDTO> findFirstFiveRequestForPaymentEstablishmentProcessOnToday() {
        return summaryRepository.FindPaymentProcedureSummaryListForEstablishmentsSince(
                LocalDateTime.now().withHour(0).withMinute(0).withSecond(0),5);
    }

    @Override
    public List<AbstractPaymentProcedureGenericDTO> findFirstFiveRequestForPaymentVehicleProcessOnToday() {
        return summaryRepository.FindPaymentProcedureSummaryListForVehiclesSince(
                LocalDateTime.now().withHour(0).withMinute(0).withSecond(0),5);
    }

    @Override
    public Long getPaymentProcedureTotalForEstablishmentsInThisYear() {
        return quantityRepository.getPaymentProcedureCountSince(CasefileEstablishment.class,
                LocalDateTime.now().withDayOfYear(1).withMonth(1).withHour(0).withMinute(0).withSecond(0));
    }

    @Override
    public Long getPaymentProcedureTotalForVehiclesInThisYear() {
        return quantityRepository.getPaymentProcedureCountSince(CasefileVehicle.class,
                LocalDateTime.now().withDayOfYear(1).withMonth(1).withHour(0).withMinute(0).withSecond(0));
    }

    @Override
    public Double getPaymentProcedureRaisedMoneyForEstablishmentsInThisYear() {
        return quantityRepository.getRaisedMoneyFromPaymentProcessSince(CasefileEstablishment.class,
                LocalDateTime.now().withDayOfYear(1).withMonth(1).withHour(0).withMinute(0).withSecond(0));
    }

    @Override
    public Double getPaymentProcedureRaisedMoneyForVehiclesInThisYear() {
        return quantityRepository.getRaisedMoneyFromPaymentProcessSince(CasefileVehicle.class,
                LocalDateTime.now().withDayOfYear(1).withMonth(1).withHour(0).withMinute(0).withSecond(0));
    }

    @Override
    public Long getPaymentProcedureTotalForEstablishmentsInThisMonth() {
        return quantityRepository.getPaymentProcedureCountSince(CasefileEstablishment.class,
                LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0));
    }

    @Override
    public Long getPaymentProcedureTotalForVehiclesInThisMonth() {
        return quantityRepository.getPaymentProcedureCountSince(CasefileVehicle.class,
                LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0));
    }

    @Override
    public Double getPaymentProcedureRaisedMoneyForEstablishmentInThisMonth() {
        return quantityRepository.getRaisedMoneyFromPaymentProcessSince(CasefileEstablishment.class,
                LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0));
    }

    @Override
    public Double getPaymentProcedureRaisedMoneyForVehiclesInThisMonth() {
        return quantityRepository.getRaisedMoneyFromPaymentProcessSince(CasefileVehicle.class,
                LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0));
    }
}
