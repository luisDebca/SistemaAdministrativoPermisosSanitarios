package edu.ues.aps.process.payment.vehicle.controller;

import edu.ues.aps.process.payment.vehicle.service.VehiclePaymentProcessFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/payment/vehicle")
public class VehiclePaymentProcessInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehiclePaymentProcessInformationProviderController.class);

    private static final String DELIVERY_PENDING = "vehicle_payment_procedure_pending_delivery_list";
    private static final String CANCELLING_PENDING = "vehicle_payment_procedure_pending_cancellation_list";

    private final VehiclePaymentProcessFinderService finderService;

    public VehiclePaymentProcessInformationProviderController(VehiclePaymentProcessFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/pending/delivery")
    public String showAllRequestPendingDelivery(ModelMap model) {
        logger.info("GET:: Show all request for payment process pending delivery.");
        model.addAttribute("requests", finderService.findAllRequestWithPaymentDeliveryPending());
        return DELIVERY_PENDING;
    }

    @GetMapping("/pending/cancellation")
    public String showAllRequestPendingCancellation(ModelMap model) {
        logger.info("GET:: Show all request for payment process pending cancellation.");
        model.addAttribute("requests", finderService.findAllRequestWithPaymentCancellationPending());
        return CANCELLING_PENDING;
    }
}
