package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.area.establishment.model.CasefileEstablishment;
import edu.ues.aps.process.payment.base.dto.AbstractPaymentReportDTO;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class PaymentProcessReportForEstablishmentRepository implements PaymentProcedureReportFinderRepository<CasefileEstablishment> {

    private final SessionFactory sessionFactory;

    public PaymentProcessReportForEstablishmentRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Class<CasefileEstablishment> appliesTo() {
        return CasefileEstablishment.class;
    }

    @Override
    public AbstractPaymentReportDTO getReportInformation(Long id_caseFile) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.payment.base.dto.PaymentProcedureReportDTO(" +
                "owner.name," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "owner.telephone,payment.number,rate.code,payment.create_on) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join casefile.paymentProcedure payment " +
                "inner join payment.rate rate " +
                "inner join establishment.address address " +
                "inner join establishment.owner owner " +
                "where casefile.id = :id", AbstractPaymentReportDTO.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }
}
