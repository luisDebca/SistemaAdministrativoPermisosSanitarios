package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentProcedureGenericDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

@Repository
public class PaymentProcedureRepository implements PaymentProcedureQuantityRepository, PaymentProcedureSummaryRepository{

    private SessionFactory sessionFactory;

    public PaymentProcedureRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Long getPaymentProcedureCountSince(Class caseFile_class, LocalDateTime since) {
        return sessionFactory.getCurrentSession().createQuery("select count(casefile) " +
                "from Casefile casefile " +
                "inner join casefile.paymentProcedure payment " +
                "where type(casefile) = :casefile_class " +
                "and payment.create_on > :since",Long.class)
                .setParameter("casefile_class", caseFile_class)
                .setParameter("since",since)
                .getSingleResult();
    }

    @Override
    public Double getRaisedMoneyFromPaymentProcessSince(Class caseFile_class, LocalDateTime since) {
        return sessionFactory.getCurrentSession().createQuery("select sum(rate.cost) " +
                "from Casefile casefile " +
                "inner join casefile.paymentProcedure payment " +
                "inner join payment.rate rate " +
                "where type(casefile) = :casefile_class " +
                "and payment.create_on > :since",Double.class)
                .setParameter("casefile_class",caseFile_class)
                .setParameter("since",since)
                .getSingleResult();
    }

    @Override
    public List<AbstractPaymentProcedureGenericDTO> FindPaymentProcedureSummaryListForEstablishmentsSince(LocalDateTime since, int results_limit) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.payment.base.dto.PaymentProcedureGenericDTO(" +
                "casefile.id,establishment.name,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "owner.name)" +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.type type " +
                "inner join establishment.owner owner " +
                "inner join establishment.address address " +
                "left join casefile.paymentProcedure payment " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where record.start_on > :since " +
                "and record.process.identifier = :identifier " +
                "and record.active = true",AbstractPaymentProcedureGenericDTO.class)
                .setParameter("since",since)
                .setParameter("identifier", ProcessIdentifier.PAYMENT_PROCEDURE_START)
                .setMaxResults(results_limit)
                .getResultList();
    }

    @Override
    public List<AbstractPaymentProcedureGenericDTO> FindPaymentProcedureSummaryListForVehiclesSince(LocalDateTime since, int results_limit) {
        return Collections.emptyList();
    }
}
