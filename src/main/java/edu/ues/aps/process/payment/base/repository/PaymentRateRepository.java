package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentRateDTO;
import edu.ues.aps.process.payment.base.model.PaymentRate;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class PaymentRateRepository implements PaymentRateCrudRepository, PaymentRateDTOFinderRepository {

    private SessionFactory sessionFactory;

    public PaymentRateRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public PaymentRate findByCode(String rate_code) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select rate " +
                "from PaymentRate rate " +
                "where rate.code = :rate_code", PaymentRate.class)
                .setParameter("rate_code", rate_code)
                .getSingleResult();
    }

    @Override
    public List<AbstractPaymentRateDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.payment.base.dto.PaymentRateDTO(" +
                "rate.id,rate.code,rate.description,rate.cost,rate.maximum_value,rate.minimum_value) " +
                "from PaymentRate rate", AbstractPaymentRateDTO.class)
                .getResultList();
    }
}
