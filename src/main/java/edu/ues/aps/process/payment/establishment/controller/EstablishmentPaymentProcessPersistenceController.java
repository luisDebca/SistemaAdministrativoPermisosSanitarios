package edu.ues.aps.process.payment.establishment.controller;

import edu.ues.aps.process.payment.base.dto.PaymentRateDTO;
import edu.ues.aps.process.payment.base.service.PaymentPersistenceService;
import edu.ues.aps.process.payment.base.service.PaymentRateDTOFinderService;
import edu.ues.aps.process.payment.establishment.service.PaymentProcessFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/payment/establishment")
public class EstablishmentPaymentProcessPersistenceController {

    private static final Logger logger = LogManager.getLogger(EstablishmentPaymentProcessPersistenceController.class);

    private static final String PAYMENT_VALUE_ASSIGNATION_FORM = "payment_procedure_form";

    private final PaymentRateDTOFinderService dtoFinderService;
    private final PaymentPersistenceService paymentPersistenceService;
    private final PaymentProcessFinderService informationProviderService;

    public EstablishmentPaymentProcessPersistenceController(PaymentRateDTOFinderService dtoFinderService, PaymentProcessFinderService informationProviderService, @Qualifier("establishmentPaymentPersistenceService") PaymentPersistenceService paymentPersistenceService) {
        this.dtoFinderService = dtoFinderService;
        this.informationProviderService = informationProviderService;
        this.paymentPersistenceService = paymentPersistenceService;
    }

    @GetMapping({"/casefile/{id_caseFile}/rate/assignation", "/casefile/{id_caseFile}/rate/edit"})
    public String showPaymentAssignationForm(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show payment rate assignation for casefile {}", id_caseFile);
        model.addAttribute("request_info", informationProviderService.findPaymentProcessCaseFileInformation(id_caseFile));
        model.addAttribute("rates", dtoFinderService.findAllPaymentRates());
        model.addAttribute("payment_rate", new PaymentRateDTO());
        return PAYMENT_VALUE_ASSIGNATION_FORM;
    }

    @PostMapping("/casefile/{id_caseFile}/rate/assignation")
    public String savePaymentProcedure(@PathVariable Long id_caseFile, @ModelAttribute PaymentRateDTO payment_rate) {
        logger.info("POST:: Save case file  {} payment rate.", id_caseFile);
        if (payment_rate.getRate_code() == null) {
            return PAYMENT_VALUE_ASSIGNATION_FORM;
        }
        paymentPersistenceService.save(id_caseFile, payment_rate.getRate_code());
        return "redirect:/payment/establishment/pending/delivery";
    }

    @PostMapping("/casefile/{id_caseFile}/rate/edit")
    public String updatePaymentProcedure(@PathVariable Long id_caseFile, @ModelAttribute PaymentRateDTO payment_rate) {
        logger.info("POST:: Update case file  {} payment rate.", id_caseFile);
        if (payment_rate.getRate_code() == null) {
            return PAYMENT_VALUE_ASSIGNATION_FORM;
        }
        paymentPersistenceService.update(id_caseFile, payment_rate.getRate_code());
        return "redirect:/payment/establishment/pending/delivery";
    }

}
