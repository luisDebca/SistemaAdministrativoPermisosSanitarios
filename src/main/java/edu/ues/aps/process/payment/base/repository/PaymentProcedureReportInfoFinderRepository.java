package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentReportDTO;

import javax.persistence.NoResultException;

public interface PaymentProcedureReportInfoFinderRepository {

    AbstractPaymentReportDTO findReportInformation(Long id_casefile) throws NoResultException;

}
