package edu.ues.aps.process.payment.vehicle.service;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.payment.base.service.PaymentRateDTOFinderService;
import edu.ues.aps.process.payment.vehicle.dto.AbstractVehiclePaymentProcessDTO;
import edu.ues.aps.process.payment.vehicle.repository.VehiclePaymentProcessFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true)
public class VehiclePaymentProcessService implements VehiclePaymentProcessFinderService, VehiclePaymentProcessResourceProviderService {

    private final VehicleBunchInformationService informationService;
    private final PaymentRateDTOFinderService paymentRateFinderService;
    private final VehiclePaymentProcessFinderRepository finderRepository;
    private Map<String, Object> map = new HashMap<>();

    public VehiclePaymentProcessService(VehiclePaymentProcessFinderRepository finderRepository, VehicleBunchInformationService informationService, PaymentRateDTOFinderService paymentRateFinderService) {
        this.finderRepository = finderRepository;
        this.informationService = informationService;
        this.paymentRateFinderService = paymentRateFinderService;
    }

    @Override
    public List<AbstractVehiclePaymentProcessDTO> findAllRequestWithPaymentDeliveryPending() {
        return finderRepository.findAllRequestWithPaymentDeliveryPending();
    }

    @Override
    public List<AbstractVehiclePaymentProcessDTO> findAllRequestWithPaymentCancellationPending() {
        return finderRepository.findAllRequestWithPaymentCancellationPending();
    }

    @Override
    public void setMapValues(Long id) {
        setBunch(id);
        setVehicleList(id);
        setPaymentRate();
    }

    @Override
    public Map<String, Object> getMap() {
        return map;
    }

    private void setBunch(Long id_bunch) {
        map.put("bunch", informationService.findById(id_bunch));
    }

    private void setVehicleList(Long id_bunch) {
        map.put("licenses", informationService.findAllLicensesList(id_bunch));
    }

    private void setPaymentRate() {
        map.put("rates", paymentRateFinderService.findAllPaymentRates());
    }
}
