package edu.ues.aps.process.payment.vehicle.repository;

import edu.ues.aps.process.legal.base.model.LegalReviewType;
import edu.ues.aps.process.payment.vehicle.dto.AbstractVehiclePaymentProcessDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehiclePaymentProcessRepository implements VehiclePaymentProcessFinderRepository {

    private final SessionFactory sessionFactory;

    public VehiclePaymentProcessRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehiclePaymentProcessDTO> findAllRequestWithPaymentDeliveryPending() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.payment.vehicle.dto.VehiclePaymentProcessDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date," +
                "(select review.create_on from LegalReview review where review.casefile.id = cv.id and review.review_type = :resolution)," +
                "payment.create_on,payment.delivered_on)" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "left join cv.paymentProcedure payment " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehiclePaymentProcessDTO.class)
                .setParameter("id", ProcessIdentifier.PAYMENT_PROCEDURE_START)
                .setParameter("resolution", LegalReviewType.RESOLUTION)
                .getResultList();
    }

    @Override
    public List<AbstractVehiclePaymentProcessDTO> findAllRequestWithPaymentCancellationPending() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.payment.vehicle.dto.VehiclePaymentProcessDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date," +
                "(select review.create_on from LegalReview review where review.casefile.id = cv.id and review.review_type = :resolution)," +
                "payment.create_on,payment.delivered_on,payment.cancelled_on,payment.expired_payment_date,payment.cancelled,rate.cost)" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "left join cv.paymentProcedure payment " +
                "left join payment.rate rate " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehiclePaymentProcessDTO.class)
                .setParameter("id", ProcessIdentifier.PAYMENT_PROCEDURE_END)
                .setParameter("resolution", LegalReviewType.RESOLUTION)
                .getResultList();
    }
}
