package edu.ues.aps.process.payment.base.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "PAYMENT_RATE")
public class PaymentRate implements AbstractPaymentRate{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "code", length = 6, unique = true)
    private String code;

    @Column(name = "description", length = 250, nullable = false)
    private String description;

    @Column(name = "minimum_value")
    private double minimum_value;

    @Column(name = "maximum_value")
    private double maximum_value;

    @Column(name = "cost", nullable = false)
    private double cost;

    @OneToMany(mappedBy = "rate", cascade = CascadeType.ALL)
    private List<PaymentProcedure> procedures = new ArrayList<>();

    public PaymentRate() {
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getCode() {
        return code;
    }

    @Override
    public void setCode(String code) {
        this.code = code;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    public double getMinimum_value() {
        return minimum_value;
    }

    public void setMinimum_value(double minimun_value) {
        this.minimum_value = minimun_value;
    }

    public double getMaximum_value() {
        return maximum_value;
    }

    public void setMaximum_value(double maximun_value) {
        this.maximum_value = maximun_value;
    }

    @Override
    public double getCost() {
        return cost;
    }

    @Override
    public void setCost(double cost) {
        this.cost = cost;
    }

    @Override
    public List<PaymentProcedure> getProcedures() {
        return procedures;
    }

    @Override
    public void setProcedures(List<PaymentProcedure> procedures) {
        this.procedures = procedures;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentRate that = (PaymentRate) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(code, that.code);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, code);
    }

    @Override
    public String toString() {
        return "PaymentRate{" +
                "id=" + id +
                ", code='" + code + '\'' +
                ", description='" + description + '\'' +
                ", minimum_value=" + minimum_value +
                ", maximum_value=" + maximum_value +
                ", cost=" + cost +
                '}';
    }
}
