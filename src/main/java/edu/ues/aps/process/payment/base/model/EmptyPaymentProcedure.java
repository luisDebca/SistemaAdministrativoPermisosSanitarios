package edu.ues.aps.process.payment.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.EmptyCasefile;

import java.time.LocalDateTime;

public class EmptyPaymentProcedure implements AbstractPaymentProcedure {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getNumber() {
        return "Payment procedure number not found";
    }

    @Override
    public LocalDateTime getCreate_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public LocalDateTime getDelivered_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public LocalDateTime getCancelled_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public Boolean getCancelled() {
        return false;
    }

    @Override
    public Boolean getExpired_payment_date() {
        return false;
    }

    @Override
    public Boolean is_cancelled() {
        return false;
    }

    @Override
    public Boolean is_expired_payment_data() {
        return false;
    }

    @Override
    public AbstractCasefile getCasefile() {
        return new EmptyCasefile();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setNumber(String number) {

    }

    @Override
    public void setCreate_on(LocalDateTime create_on) {

    }

    @Override
    public void setDelivered_on(LocalDateTime delivered_on) {

    }

    @Override
    public void setCancelled_on(LocalDateTime cancelled_on) {

    }

    @Override
    public void setCancelled(Boolean cancelled) {

    }

    @Override
    public void setExpired_payment_date(Boolean expired_payment_date) {

    }

    @Override
    public void setCasefile(Casefile casefile) {

    }

    @Override
    public EmptyPaymentRate getRate() {
        return new EmptyPaymentRate();
    }

    @Override
    public void setRate(PaymentRate rate) {
    }

    @Override
    public String toString() {
        return "EmptyPaymentProcedure{}";
    }
}
