package edu.ues.aps.process.payment.base.service;

import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.repository.CaseFileTypeFinderRepository;
import edu.ues.aps.process.payment.base.dto.AbstractPaymentReportDTO;
import edu.ues.aps.process.payment.base.dto.EmptyPaymentProcedureReportDTO;
import edu.ues.aps.process.payment.base.repository.PaymentProcedureReportFinderRepository;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true)
public class PaymentProcedureReportService implements InitializingBean, PaymentProcedureReportInfoFinderService {

    private final List<PaymentProcedureReportFinderRepository> finderRepositories;
    private final CaseFileTypeFinderRepository typeFinderRepository;

    private Map<Class<? extends Casefile>,PaymentProcedureReportFinderRepository> finderMap = new HashMap<>();

    public PaymentProcedureReportService(List<PaymentProcedureReportFinderRepository> finderRepositories, CaseFileTypeFinderRepository typeFinderRepository) {
        this.finderRepositories = finderRepositories;
        this.typeFinderRepository = typeFinderRepository;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (PaymentProcedureReportFinderRepository finder: finderRepositories){
            finderMap.put(finder.appliesTo(),finder);
        }
    }

    @Override
    public AbstractPaymentReportDTO findPaymentProcessReportInfo(Long id_caseFile) {
        try {
            Class type = typeFinderRepository.getType(id_caseFile);
            return finderMap.get(type).getReportInformation(id_caseFile);
        } catch (NoResultException e) {
            return new EmptyPaymentProcedureReportDTO();
        }
    }
}
