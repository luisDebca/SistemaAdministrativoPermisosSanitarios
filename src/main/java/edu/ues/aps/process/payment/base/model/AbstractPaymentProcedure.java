package edu.ues.aps.process.payment.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;

import java.time.LocalDateTime;

public interface AbstractPaymentProcedure {

    Long getId();

    void setId(Long id);

    String getNumber();

    void setNumber(String number);

    LocalDateTime getCreate_on();

    void setCreate_on(LocalDateTime create_on);

    LocalDateTime getDelivered_on();

    void setDelivered_on(LocalDateTime delivered_on);

    LocalDateTime getCancelled_on();

    void setCancelled_on(LocalDateTime cancelled_on);

    Boolean getCancelled();

    Boolean is_cancelled();

    void setCancelled(Boolean cancelled);

    Boolean getExpired_payment_date();

    Boolean is_expired_payment_data();

    void setExpired_payment_date(Boolean expired_payment_date);

    AbstractCasefile getCasefile();

    void setCasefile(Casefile casefile);

    AbstractPaymentRate getRate();

    void setRate(PaymentRate rate);
}
