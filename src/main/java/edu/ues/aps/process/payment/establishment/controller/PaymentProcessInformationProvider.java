package edu.ues.aps.process.payment.establishment.controller;

import edu.ues.aps.process.payment.establishment.service.PaymentProcessFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/payment/establishment")
public class PaymentProcessInformationProvider {

    private static final Logger logger = LogManager.getLogger(PaymentProcessInformationProvider.class);

    private static final String ESTABLISHMENT_PAYMENT_FOR_CREATION_AND_DELIVERY_LIST = "establishment_payment_procedure_pending_delivery_list";
    private static final String ESTABLISHMENT_PAYMENT_FOR_CANCELLATION_LIST = "establishment_payment_procedure_pending_cancellation_list";

    private PaymentProcessFinderService finderService;

    public PaymentProcessInformationProvider(PaymentProcessFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/pending/delivery")
    public String showPaymentProcessRequestWithPaymentPendingDelivery(ModelMap model){
        logger.info("GET:: Show all request for payment process delivery");
        model.addAttribute("requests",finderService.findAllRequestFromPaymentProcessWithPaymentPendingDelivery());
        return ESTABLISHMENT_PAYMENT_FOR_CREATION_AND_DELIVERY_LIST;
    }

    @GetMapping("/pending/cancellation")
    public String showPaymentProcessRequestWithPaymentPendingCancellation(ModelMap model){
        logger.info("GET:: Show all request for payment process cancellation");
        model.addAttribute("requests",finderService.findAllRequestFromPaymentProcessWithPaymentPendingCancellation());
        return ESTABLISHMENT_PAYMENT_FOR_CANCELLATION_LIST;
    }
}
