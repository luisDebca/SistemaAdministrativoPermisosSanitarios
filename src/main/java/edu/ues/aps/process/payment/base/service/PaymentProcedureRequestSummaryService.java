package edu.ues.aps.process.payment.base.service;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentProcedureGenericDTO;

import java.util.List;

public interface PaymentProcedureRequestSummaryService {

    List<AbstractPaymentProcedureGenericDTO> findFirstFiveRequestForPaymentEstablishmentProcessOnToday();

    List<AbstractPaymentProcedureGenericDTO> findFirstFiveRequestForPaymentVehicleProcessOnToday();

}
