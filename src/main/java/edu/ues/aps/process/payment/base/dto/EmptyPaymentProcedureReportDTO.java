package edu.ues.aps.process.payment.base.dto;

import edu.ues.aps.process.payment.establishment.dto.AbstractPaymentProcessDTO;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class EmptyPaymentProcedureReportDTO implements AbstractPaymentReportDTO {

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public String getOwner_address() {
        return "Owner name not found";
    }

    @Override
    public String getOwner_telephone() {
        return "Owner telephone not found";
    }

    @Override
    public String getPayment_number() {
        return "000000";
    }

    @Override
    public String getPayment_code() {
        return "u01";
    }

    @Override
    public String getPayment_procedure_creation_date() {
        return LocalDate.of(2000,1,1).toString();
    }

    @Override
    public String getClient_name() {
        return "Client name not found";
    }

    @Override
    public String getClient_telephone() {
        return "Client_telephone not found";
    }

    @Override
    public String getUser_name() {
        return "User name not found";
    }

    @Override
    public String getPayment_count() {
        return "00";
    }

    @Override
    public void setUser_name(String user_name) {

    }

    @Override
    public void setClient_name(String client_name) {

    }

    @Override
    public void setClient_telephone(String client_telephone) {

    }

    @Override
    public String toString() {
        return "EmptyPaymentProcedureReportDTO{}";
    }
}
