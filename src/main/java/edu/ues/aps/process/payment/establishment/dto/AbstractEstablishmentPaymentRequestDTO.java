package edu.ues.aps.process.payment.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentRequestDTO;

public interface AbstractEstablishmentPaymentRequestDTO extends AbstractEstablishmentRequestDTO {

    String getDelay_date();

    String getCreated_on();

    String getDelivery_on();

    String getCancelled_on();

    Boolean getCreated();

    Boolean getDelivery();

    Boolean getExpired();

    Boolean getCancelled();

    Double getPayment_cost();
}
