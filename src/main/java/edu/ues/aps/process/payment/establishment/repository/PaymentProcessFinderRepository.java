package edu.ues.aps.process.payment.establishment.repository;

import edu.ues.aps.process.payment.establishment.dto.AbstractEstablishmentPaymentRequestDTO;
import edu.ues.aps.process.payment.establishment.dto.AbstractPaymentProcessDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface PaymentProcessFinderRepository {

    AbstractPaymentProcessDTO findPaymentProcessInformation(Long id_casefile) throws NoResultException;

    List<AbstractEstablishmentPaymentRequestDTO> findAllRequestFromPaymentProcessPendingDelivery();

    List<AbstractEstablishmentPaymentRequestDTO> findAllRequestFromPaymentProcessPendingCancellation();
}
