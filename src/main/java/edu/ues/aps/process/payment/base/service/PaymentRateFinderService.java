package edu.ues.aps.process.payment.base.service;

import edu.ues.aps.process.payment.base.model.PaymentRate;

public interface PaymentRateFinderService {

    PaymentRate findRateByCode(String rate_code);
}
