package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.area.vehicle.model.CasefileVehicle;
import edu.ues.aps.process.payment.base.dto.AbstractPaymentReportDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class PaymentProcessReportForVehicleRepository implements PaymentProcedureReportFinderRepository<CasefileVehicle> {

    private final SessionFactory sessionFactory;

    public PaymentProcessReportForVehicleRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Class<CasefileVehicle> appliesTo() {
        return CasefileVehicle.class;
    }

    @Override
    public AbstractPaymentReportDTO getReportInformation(Long id_caseFile) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.payment.base.dto.PaymentProcedureReportDTO(" +
                "owner.name,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "bunch.client_presented_telephone,payment.number, rate.code,payment.create_on," +
                "(select count(c) from CasefileVehicle c where c.bunch.id = bunch.id)) " +
                "from CasefileVehicle cv " +
                "inner join cv.bunch bunch " +
                "inner join bunch.address address " +
                "inner join cv.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "inner join cv.paymentProcedure payment " +
                "inner join payment.rate rate " +
                "where cv.id = :id",AbstractPaymentReportDTO.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }
}
