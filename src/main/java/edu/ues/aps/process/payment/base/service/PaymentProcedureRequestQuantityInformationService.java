package edu.ues.aps.process.payment.base.service;

public interface PaymentProcedureRequestQuantityInformationService {

    Long getPaymentProcedureTotalForEstablishmentsInThisYear();

    Long getPaymentProcedureTotalForVehiclesInThisYear();

    Long getPaymentProcedureTotalForEstablishmentsInThisMonth();

    Long getPaymentProcedureTotalForVehiclesInThisMonth();

    Double getPaymentProcedureRaisedMoneyForEstablishmentsInThisYear();

    Double getPaymentProcedureRaisedMoneyForVehiclesInThisYear();

    Double getPaymentProcedureRaisedMoneyForEstablishmentInThisMonth();

    Double getPaymentProcedureRaisedMoneyForVehiclesInThisMonth();
}
