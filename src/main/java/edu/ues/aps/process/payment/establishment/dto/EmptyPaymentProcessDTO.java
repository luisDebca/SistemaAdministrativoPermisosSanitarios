package edu.ues.aps.process.payment.establishment.dto;

import java.time.LocalDateTime;

public class EmptyPaymentProcessDTO implements AbstractPaymentProcessDTO{

    @Override
    public Long getId_casefile() {
        return 0L;
    }

    @Override
    public Long getId_establishment() {
        return 0L;
    }

    @Override
    public Long getId_owner() {
        return 0L;
    }

    @Override
    public String getFolder_number() {
        return "Folder number not found";
    }

    @Override
    public String getEstablishment_name() {
        return "Establishment name not found";
    }

    @Override
    public String getEstablishment_address() {
        return "Establishment address not found";
    }

    @Override
    public String getEstablishment_type() {
        return "Establishment type not found";
    }

    @Override
    public String getEstablishment_section() {
        return "Establishment section not found";
    }

    @Override
    public Double getEstablishment_capital() {
        return 0.0;
    }

    @Override
    public String getEstablishment_type_details() {
        return "Establishment type provider for user is not found";
    }

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public String getCasefile_certification_type() {
        return "Casefile certification type not found";
    }

    @Override
    public String getLegal_first_resolution_created_on() {
        return LocalDateTime.of(2000,1,1,0,0,0).toString();
    }

    @Override
    public String getPayment_cancelled_on() {
        return LocalDateTime.of(2000,1,1,0,0,0).toString();
    }

    @Override
    public String getPayment_created_on() {
        return LocalDateTime.of(2000,1,1,0,0,0).toString();
    }

    @Override
    public String getPayment_delivered_on() {
        return LocalDateTime.of(2000,1,1,0,0,0).toString();
    }

    @Override
    public double getPayment_cost() {
        return 0.0;
    }

    @Override
    public boolean isDelivery() {
        return false;
    }

    @Override
    public boolean isCancelled() {
        return false;
    }

    @Override
    public boolean isDate_for_payment_expired() {
        return false;
    }

    @Override
    public boolean isPayment_created() {
        return false;
    }

    @Override
    public void setDate_for_payment_expired(boolean date_for_payment_expired) {

    }

    @Override
    public String getLegal_first_resolution_30days_date() {
        return LocalDateTime.of(2000,1,1,0,0,0).toString();
    }

    @Override
    public String getPayment_delivered_30days_date() {
        return LocalDateTime.of(2000,1,1,0,0,0).toString();
    }
}
