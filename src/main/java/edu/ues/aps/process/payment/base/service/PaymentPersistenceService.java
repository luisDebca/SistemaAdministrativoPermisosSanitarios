package edu.ues.aps.process.payment.base.service;

public interface PaymentPersistenceService {

    void save(Long id, String payment_rate);

    void update(Long id, String payment_rate);
}
