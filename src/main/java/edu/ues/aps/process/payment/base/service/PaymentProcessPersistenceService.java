package edu.ues.aps.process.payment.base.service;

public interface PaymentProcessPersistenceService {

    void setDeliveryDate(Long id_caseFile);

    void setPaymentCancelled(Long id_caseFile);

}
