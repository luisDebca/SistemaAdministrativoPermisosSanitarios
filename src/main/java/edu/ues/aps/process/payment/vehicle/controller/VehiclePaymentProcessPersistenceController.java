package edu.ues.aps.process.payment.vehicle.controller;

import edu.ues.aps.process.payment.base.dto.PaymentRateDTO;
import edu.ues.aps.process.payment.base.service.PaymentPersistenceService;
import edu.ues.aps.process.payment.vehicle.service.VehiclePaymentProcessResourceProviderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/payment/vehicle")
public class VehiclePaymentProcessPersistenceController {

    private static final Logger logger = LogManager.getLogger(VehiclePaymentProcessPersistenceController.class);

    private static final String PAYMENT = "vehicle_payment_procedure_form";

    private final PaymentPersistenceService persistenceService;
    private final VehiclePaymentProcessResourceProviderService resourceProviderService;

    public VehiclePaymentProcessPersistenceController(VehiclePaymentProcessResourceProviderService resourceProviderService, @Qualifier("vehiclePaymentPersistenceService") PaymentPersistenceService persistenceService) {
        this.resourceProviderService = resourceProviderService;
        this.persistenceService = persistenceService;
    }

    @GetMapping("/bunch/{id_bunch}/new")
    public String showNewPaymentForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show new payment document for bunch {}.", id_bunch);
        resourceProviderService.setMapValues(id_bunch);
        model.addAllAttributes(resourceProviderService.getMap());
        model.addAttribute("payment_rate", new PaymentRateDTO());
        return PAYMENT;
    }

    @PostMapping("/bunch/{id_bunch}/new")
    public String savePaymentDocument(@PathVariable Long id_bunch, @ModelAttribute PaymentRateDTO dto) {
        logger.info("POST:: Save new payment for all request in bunch {}", id_bunch);
        persistenceService.save(id_bunch, dto.getRate_code());
        return "redirect:/payment/vehicle/pending/delivery";
    }

    @GetMapping("/bunch/{id_bunch}/edit")
    public String showEditPaymentForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show edit payment document for bunch {}.", id_bunch);
        resourceProviderService.setMapValues(id_bunch);
        model.addAllAttributes(resourceProviderService.getMap());
        model.addAttribute("payment_rate", new PaymentRateDTO());
        return PAYMENT;
    }

    @PostMapping("/bunch/{id_bunch}/edit")
    public String updatePaymentDocument(@PathVariable Long id_bunch, @ModelAttribute PaymentRateDTO dto) {
        logger.info("POST:: Update payment document for all request in bunch {}.", id_bunch);
        persistenceService.update(id_bunch, dto.getRate_code());
        return "redirect:/payment/vehicle/pending/delivery";
    }
}
