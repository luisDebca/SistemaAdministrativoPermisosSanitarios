package edu.ues.aps.process.payment.base.service;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentReportDTO;

public interface PaymentProcedureReportInfoFinderService {

    AbstractPaymentReportDTO findPaymentProcessReportInfo(Long id_caseFile);
}
