package edu.ues.aps.process.payment.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.EstablishmentRequestDTO;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class EstablishmentPaymentRequestDTO extends EstablishmentRequestDTO implements AbstractEstablishmentPaymentRequestDTO {

    private LocalDateTime resolution_date;
    private LocalDateTime created_on;
    private LocalDateTime delivery_on;
    private LocalDateTime cancelled_on;
    private Boolean expired;
    private Boolean cancelled;
    private Double payment_cost;

    public EstablishmentPaymentRequestDTO(Long id_caseFile, Long id_owner, Long id_establishment, String folder_number, String request_number, String establishment_name, String establishment_type_detail, String establishment_type, String establishment_section, String establishment_address, String establishment_nit, String UCSF, String SIBASI, CertificationType certification_type, LocalDateTime caseFile_createOn, String owner_name, LocalDateTime resolution_date, LocalDateTime created_on, LocalDateTime delivery_on) {
        super(id_caseFile, id_owner, id_establishment, folder_number, request_number, establishment_name, establishment_type_detail, establishment_type, establishment_section, establishment_address, establishment_nit, UCSF, SIBASI, certification_type, caseFile_createOn, owner_name);
        this.resolution_date = resolution_date;
        this.created_on = created_on;
        this.delivery_on = delivery_on;
    }

    public EstablishmentPaymentRequestDTO(Long id_caseFile, Long id_owner, Long id_establishment, String folder_number, String request_number, String establishment_name, String establishment_type_detail, String establishment_type, String establishment_section, String establishment_address, String establishment_nit, String UCSF, String SIBASI, CertificationType certification_type, LocalDateTime caseFile_createOn, String owner_name, LocalDateTime resolution_date, LocalDateTime created_on, LocalDateTime delivery_on, LocalDateTime cancelled_on, Boolean expired, Boolean cancelled, Double payment_cost) {
        super(id_caseFile, id_owner, id_establishment, folder_number, request_number, establishment_name, establishment_type_detail, establishment_type, establishment_section, establishment_address, establishment_nit, UCSF, SIBASI, certification_type, caseFile_createOn, owner_name);
        this.resolution_date = resolution_date;
        this.created_on = created_on;
        this.delivery_on = delivery_on;
        this.cancelled_on = cancelled_on;
        this.expired = expired;
        this.cancelled = cancelled;
        this.payment_cost = payment_cost;
    }

    public String getDelay_date() {
        if (resolution_date == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDatetimeToHtmlFormat(DatetimeUtil.addDays(resolution_date, 30));
    }

    public String getCreated_on() {
        if (created_on == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(resolution_date.toLocalDate());
    }

    public String getDelivery_on() {
        if (delivery_on == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(delivery_on.toLocalDate());
    }

    public String getCancelled_on() {
        if (cancelled_on == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(cancelled_on.toLocalDate());
    }

    public Boolean getCreated() {
        return created_on != null;
    }

    public Boolean getDelivery() {
        return delivery_on != null;
    }

    public Boolean getExpired() {
        return expired;
    }

    public Boolean getCancelled() {
        return cancelled;
    }

    public Double getPayment_cost() {
        return payment_cost;
    }
}
