package edu.ues.aps.process.payment.base.controller;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/payment")
public class PaymentProcedureIndexController {

    private static final Logger logger = LogManager.getLogger(PaymentProcedureIndexController.class);

    private static final String PAYMENT_INDEX = "payment_index";

    public PaymentProcedureIndexController() {
    }

    @GetMapping({"/", "/index"})
    public String showPaymentProcedureIndex(ModelMap model) {
        logger.info("GET:: Show payment process index.");
        model.addAttribute("process", ProcessIdentifier.PAYMENT_PROCEDURE_START);
        return PAYMENT_INDEX;
    }
}
