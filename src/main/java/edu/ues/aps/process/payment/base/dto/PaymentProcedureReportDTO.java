package edu.ues.aps.process.payment.base.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class PaymentProcedureReportDTO implements AbstractPaymentReportDTO {

    private String owner_name;
    private String owner_address;
    private String owner_telephone;
    private String payment_number;
    private String payment_code;
    private Long payment_count;
    private LocalDateTime payment_procedure_creation_date;
    private String client_name;
    private String client_telephone;
    private String user_name;

    public PaymentProcedureReportDTO(String owner_name, String owner_address, String owner_telephone, String payment_number, String payment_code, LocalDateTime payment_procedure_creation_date) {
        this.owner_name = owner_name;
        this.owner_address = owner_address;
        this.owner_telephone = owner_telephone;
        this.payment_number = payment_number;
        this.payment_code = payment_code;
        this.payment_procedure_creation_date = payment_procedure_creation_date;
    }

    public PaymentProcedureReportDTO(String owner_name, String owner_address, String owner_telephone, String payment_number, String payment_code, LocalDateTime payment_procedure_creation_date, Long payment_count) {
        this.owner_name = owner_name;
        this.owner_address = owner_address;
        this.owner_telephone = owner_telephone;
        this.payment_number = payment_number;
        this.payment_code = payment_code;
        this.payment_procedure_creation_date = payment_procedure_creation_date;
        this.payment_count = payment_count;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getOwner_address() {
        return owner_address;
    }

    public String getOwner_telephone() {
        return owner_telephone;
    }

    public String getPayment_number() {
        return payment_number;
    }

    public String getPayment_code() {
        return payment_code.toLowerCase();
    }

    public String getPayment_count() {
        if (payment_count == null) return String.valueOf(0L);
        return String.valueOf(payment_count);
    }

    public String getPayment_procedure_creation_date() {
        if (payment_procedure_creation_date != null) {
            return DatetimeUtil.parseDateToLongDateFormat(payment_procedure_creation_date.toLocalDate());
        }
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    public String getClient_name() {
        return client_name;
    }

    public String getClient_telephone() {
        return client_telephone;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public void setClient_telephone(String client_telephone) {
        this.client_telephone = client_telephone;
    }
}
