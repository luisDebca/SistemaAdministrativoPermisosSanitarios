package edu.ues.aps.process.payment.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleRequestDTO;

public interface AbstractVehiclePaymentProcessDTO extends AbstractVehicleRequestDTO {

    String getDelay_date();

    String getCreated_on();

    String getDelivery_on();

    String getCancelled_on();

    Boolean getCreated();

    Boolean getDelivery();

    Boolean getExpired();

    Boolean getCancelled();

    Double getPayment_cost();
}
