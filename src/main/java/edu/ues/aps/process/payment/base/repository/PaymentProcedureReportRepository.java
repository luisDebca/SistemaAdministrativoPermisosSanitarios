package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentReportDTO;
import edu.ues.aps.process.payment.base.dto.EmptyPaymentProcedureReportDTO;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class PaymentProcedureReportRepository implements PaymentProcedureReportInfoFinderRepository {

    private final SessionFactory sessionFactory;

    public PaymentProcedureReportRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractPaymentReportDTO findReportInformation(Long id_casefile) throws NoResultException {
        sessionFactory.getCurrentSession().createQuery("select type(casefile) " +
                "from Casefile casefile where casefile.id = :id")
                .setParameter("id", id_casefile)
                .getSingleResult();
        return new EmptyPaymentProcedureReportDTO();
    }
}
