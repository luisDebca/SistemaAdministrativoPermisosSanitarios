package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentProcedureGenericDTO;

import java.time.LocalDateTime;
import java.util.List;

public interface PaymentProcedureSummaryRepository {

    List<AbstractPaymentProcedureGenericDTO> FindPaymentProcedureSummaryListForEstablishmentsSince(LocalDateTime since,int results_limit);

    List<AbstractPaymentProcedureGenericDTO> FindPaymentProcedureSummaryListForVehiclesSince(LocalDateTime since, int results_limit);
}
