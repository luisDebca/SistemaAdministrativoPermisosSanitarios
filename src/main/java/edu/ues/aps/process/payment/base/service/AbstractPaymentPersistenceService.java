package edu.ues.aps.process.payment.base.service;

import edu.ues.aps.process.payment.base.model.PaymentProcedure;
import edu.ues.aps.process.payment.base.repository.PaymentProcessPersistenceRepository;
import edu.ues.aps.process.payment.base.repository.PaymentRateCrudRepository;

import java.time.LocalDateTime;

public abstract class AbstractPaymentPersistenceService {

    private final PaymentRateCrudRepository rateCrudRepository;
    private final PaymentProcessPersistenceRepository persistenceRepository;

    public AbstractPaymentPersistenceService(PaymentRateCrudRepository rateCrudRepository, PaymentProcessPersistenceRepository persistenceRepository) {
        this.rateCrudRepository = rateCrudRepository;
        this.persistenceRepository = persistenceRepository;
    }

    public void savePaymentDocument(Long id_caseFile, String rate_code) {
        persistenceRepository.savePaymentProcedure(id_caseFile, buildProcedure(rate_code));
    }

    public void updatePaymentDocument(Long id_caseFile, String rate_code) {
        persistenceRepository.updatePaymentProcedure(id_caseFile, rate_code);
    }

    private PaymentProcedure buildProcedure(String rate_code) {
        PaymentProcedure procedure = new PaymentProcedure();
        Long current_payment_number = persistenceRepository.getLastPaymentNumber();
        if (current_payment_number == null) {
            current_payment_number = 1L;
        } else {
            current_payment_number += 1;
        }
        procedure.setNumber(String.valueOf(current_payment_number));
        procedure.setCancelled(false);
        procedure.setCreate_on(LocalDateTime.now());
        procedure.setExpired_payment_date(false);
        procedure.setRate(rateCrudRepository.findByCode(rate_code));
        return procedure;
    }
}
