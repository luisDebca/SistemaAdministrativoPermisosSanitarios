package edu.ues.aps.process.payment.base.async;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentProcedureGenericDTO;
import edu.ues.aps.process.payment.base.service.PaymentProcedureRequestSummaryService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class PaymentProcedureTodayRequestInformationProviderController {

    private static final Logger logger = LogManager.getLogger(PaymentProcedureTodayRequestInformationProviderController.class);

    private PaymentProcedureRequestSummaryService summaryService;

    public PaymentProcedureTodayRequestInformationProviderController(PaymentProcedureRequestSummaryService summaryService) {
        this.summaryService = summaryService;
    }

    @GetMapping(value = "/find-first-five-payment-request-for-establishment-entered-today", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractPaymentProcedureGenericDTO> findFirstFivePaymentRequestForEstablishmentEnteredToday() {
        logger.info("GET:: Find today entry request for establishment payment process");
        return summaryService.findFirstFiveRequestForPaymentEstablishmentProcessOnToday();
    }

    @GetMapping(value = "/find-first-five-payment-request-for-vehicles-entered-today", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractPaymentProcedureGenericDTO> findFirstFivePaymentRequestForVehiclesEnteredToday() {
        logger.info("GET:: Find today entry request for vehicle payment process");
        return summaryService.findFirstFiveRequestForPaymentVehicleProcessOnToday();
    }

}
