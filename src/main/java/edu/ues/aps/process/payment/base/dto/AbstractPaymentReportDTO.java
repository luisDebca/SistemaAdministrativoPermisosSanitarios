package edu.ues.aps.process.payment.base.dto;

public interface AbstractPaymentReportDTO {

    String getOwner_name();

    String getOwner_address();

    String getOwner_telephone();

    String getPayment_number();

    String getPayment_code();

    String getPayment_procedure_creation_date();

    String getPayment_count();

    String getClient_name();

    String getClient_telephone();

    String getUser_name();

    void setUser_name(String user_name);

    void setClient_name(String client_name);

    void setClient_telephone(String client_telephone);
}
