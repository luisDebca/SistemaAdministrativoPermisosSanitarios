package edu.ues.aps.process.payment.base.dto;

public class PaymentProcedureGenericDTO implements AbstractPaymentProcedureGenericDTO{

    private Long id_casefile;
    private String subject;
    private String subject_address;
    private String owner_name;
    private String payment_rate_estimated_description;
    private Double payment_rate_estimated_cost;

    public PaymentProcedureGenericDTO(Long id_casefile, String subject, String subject_address, String owner_name) {
        this.id_casefile = id_casefile;
        this.subject = subject;
        this.subject_address = subject_address;
        this.owner_name = owner_name;
    }

    public PaymentProcedureGenericDTO(Long id_casefile, String subject, String subject_address, String owner_name, String payment_rate_estimated_description, Double payment_rate_estimated_cost) {
        this.id_casefile = id_casefile;
        this.subject = subject;
        this.subject_address = subject_address;
        this.owner_name = owner_name;
        this.payment_rate_estimated_description = payment_rate_estimated_description;
        this.payment_rate_estimated_cost = payment_rate_estimated_cost;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public void setId_casefile(Long id_casefile) {
        this.id_casefile = id_casefile;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject_address() {
        return subject_address;
    }

    public void setSubject_address(String subject_address) {
        this.subject_address = subject_address;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public String getPayment_rate_estimated_description() {
        return payment_rate_estimated_description;
    }

    public void setPayment_rate_estimated_description(String payment_rate_estimated_description) {
        this.payment_rate_estimated_description = payment_rate_estimated_description;
    }

    public Double getPayment_rate_estimated_cost() {
        return payment_rate_estimated_cost;
    }

    public void setPayment_rate_estimated_cost(Double payment_rate_estimated_cost) {
        this.payment_rate_estimated_cost = payment_rate_estimated_cost;
    }
}
