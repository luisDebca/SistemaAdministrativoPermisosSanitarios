package edu.ues.aps.process.payment.base.model;

import java.util.List;

public interface AbstractPaymentRate {

    Long getId();

    void setId(Long id);

    String getCode();

    void setCode(String code);

    String getDescription();

    void setDescription(String description);

    double getMinimum_value();

    void setMinimum_value(double minimun_value);

    double getMaximum_value();

    void setMaximum_value(double maximun_value);

    double getCost();

    void setCost(double cost);

    List<PaymentProcedure> getProcedures();

    void setProcedures(List<PaymentProcedure> procedures);
}
