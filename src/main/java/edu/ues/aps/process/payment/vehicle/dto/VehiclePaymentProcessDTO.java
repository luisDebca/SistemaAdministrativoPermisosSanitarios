package edu.ues.aps.process.payment.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.VehicleRequestDTO;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class VehiclePaymentProcessDTO extends VehicleRequestDTO implements AbstractVehiclePaymentProcessDTO {

    private LocalDateTime resolution_date;
    private LocalDateTime created_on;
    private LocalDateTime delivery_on;
    private LocalDateTime cancelled_on;
    private Boolean expired;
    private Boolean cancelled;
    private Double payment_cost;

    public VehiclePaymentProcessDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
    }

    public VehiclePaymentProcessDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on, LocalDateTime resolution_date, LocalDateTime created_on, LocalDateTime delivery_on) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
        this.resolution_date = resolution_date;
        this.created_on = created_on;
        this.delivery_on = delivery_on;
    }

    public VehiclePaymentProcessDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on, LocalDateTime resolution_date, LocalDateTime created_on, LocalDateTime delivery_on, LocalDateTime cancelled_on, Boolean expired, Boolean cancelled, Double payment_cost) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
        this.resolution_date = resolution_date;
        this.created_on = created_on;
        this.delivery_on = delivery_on;
        this.cancelled_on = cancelled_on;
        this.expired = expired;
        this.cancelled = cancelled;
        this.payment_cost = payment_cost;
    }

    public String getDelay_date() {
        if (resolution_date == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDatetimeToHtmlFormat(DatetimeUtil.addDays(resolution_date, 30));
    }

    public String getCreated_on() {
        if (created_on == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(resolution_date.toLocalDate());
    }

    public String getDelivery_on() {
        if (delivery_on == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(delivery_on.toLocalDate());
    }

    public String getCancelled_on() {
        if (cancelled_on == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(cancelled_on.toLocalDate());
    }

    public Boolean getCreated() {
        return created_on != null;
    }

    public Boolean getDelivery() {
        return delivery_on != null;
    }

    public Boolean getExpired() {
        return expired;
    }

    public Boolean getCancelled() {
        return cancelled;
    }

    public Double getPayment_cost() {
        return payment_cost;
    }
}