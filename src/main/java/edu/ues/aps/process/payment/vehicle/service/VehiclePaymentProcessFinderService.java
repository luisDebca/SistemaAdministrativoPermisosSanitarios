package edu.ues.aps.process.payment.vehicle.service;

import edu.ues.aps.process.payment.vehicle.dto.AbstractVehiclePaymentProcessDTO;

import java.util.List;

public interface VehiclePaymentProcessFinderService {

    List<AbstractVehiclePaymentProcessDTO> findAllRequestWithPaymentDeliveryPending();

    List<AbstractVehiclePaymentProcessDTO> findAllRequestWithPaymentCancellationPending();
}
