package edu.ues.aps.process.payment.establishment.service;

import edu.ues.aps.process.payment.base.repository.PaymentProcessPersistenceRepository;
import edu.ues.aps.process.payment.base.repository.PaymentRateCrudRepository;
import edu.ues.aps.process.payment.base.service.AbstractPaymentPersistenceService;
import edu.ues.aps.process.payment.base.service.PaymentPersistenceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class EstablishmentPaymentPersistenceService extends AbstractPaymentPersistenceService implements PaymentPersistenceService {

    public EstablishmentPaymentPersistenceService(PaymentRateCrudRepository rateCrudRepository, PaymentProcessPersistenceRepository persistenceRepository) {
        super(rateCrudRepository, persistenceRepository);
    }

    @Override
    public void save(Long id, String payment_rate) {
        savePaymentDocument(id, payment_rate);
    }

    @Override
    public void update(Long id, String payment_rate) {
        updatePaymentDocument(id, payment_rate);
    }
}
