package edu.ues.aps.process.payment.establishment.dto;

public interface AbstractPaymentProcessDTO {

    Long getId_casefile();

    Long getId_establishment();

    Long getId_owner();

    String getFolder_number();

    String getEstablishment_name();

    String getEstablishment_address();

    String getEstablishment_type();

    String getEstablishment_section();

    Double getEstablishment_capital();

    String getEstablishment_type_details();

    String getOwner_name();

    String getCasefile_certification_type();

    String getLegal_first_resolution_created_on();

    String getPayment_cancelled_on();

    String getPayment_created_on();

    String getPayment_delivered_on();

    double getPayment_cost();

    boolean isDelivery();

    boolean isCancelled();

    boolean isDate_for_payment_expired();

    boolean isPayment_created();

    void setDate_for_payment_expired(boolean date_for_payment_expired);

    String getLegal_first_resolution_30days_date();

    String getPayment_delivered_30days_date();
}
