package edu.ues.aps.process.payment.establishment.async;

import edu.ues.aps.process.payment.base.async.PaymentProcessStatusModificationController;
import edu.ues.aps.process.payment.base.service.PaymentProcessPersistenceService;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
@RequestMapping("/payment")
public class EstablishmentPaymentProcessStatusModificationController extends PaymentProcessStatusModificationController {

    private static final Logger logger = LogManager.getLogger(EstablishmentPaymentProcessStatusModificationController.class);

    private final ProcessUserRegisterService registerService;

    public EstablishmentPaymentProcessStatusModificationController(PaymentProcessPersistenceService persistenceService, ProcessUserRegisterService registerService) {
        super(persistenceService);
        this.registerService = registerService;
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/check-payment-procedure-delivery-date", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setPaymentDeliveryStatus(@RequestParam Long id_caseFile, Principal principal) {
        logger.info("GET:: Set payment procedure delivered date for caseFile {}", id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.payment.delivered");
        setDeliveryDate(id_caseFile);
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @Override
    @RequestMapping(method = RequestMethod.GET, value = "/check-payment-procedure-cancellation", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setPaymentCancellationStatus(@RequestParam Long id_caseFile, Principal principal) {
        logger.info("GET:: Set payment {} procedure cancelled status true", id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.payment.cancellation");
        setCancelledStatus(id_caseFile);
        return new SingleStringResponse("OK").asSuccessResponse();
    }
}
