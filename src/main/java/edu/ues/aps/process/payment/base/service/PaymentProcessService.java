package edu.ues.aps.process.payment.base.service;

import edu.ues.aps.process.payment.base.repository.PaymentProcessPersistenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PaymentProcessService implements  PaymentProcessPersistenceService{

    private final PaymentProcessPersistenceRepository persistenceRepository;

    public PaymentProcessService(PaymentProcessPersistenceRepository persistenceRepository) {
        this.persistenceRepository = persistenceRepository;
    }

    @Override
    public void setDeliveryDate(Long id_caseFile) {
        persistenceRepository.setDeliveryDate(id_caseFile);
    }

    @Override
    public void setPaymentCancelled(Long id_caseFile) {
        persistenceRepository.setPaymentCancelled(id_caseFile);
    }

}
