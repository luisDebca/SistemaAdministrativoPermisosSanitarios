package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.payment.base.model.PaymentRate;

import javax.persistence.NoResultException;

public interface PaymentRateCrudRepository {

    PaymentRate findByCode(String rate_code) throws NoResultException;

}
