package edu.ues.aps.process.payment.base.model;

import edu.ues.aps.process.base.model.Casefile;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "PAYMENT_PROCEDURE")
public class PaymentProcedure implements AbstractPaymentProcedure {

    @Id
    private Long id;

    @Column(name = "number", nullable = false, unique = true, length = 8)
    private String number;

    @Column(name = "create_on")
    private LocalDateTime create_on;

    @Column(name = "delivered_on")
    private LocalDateTime delivered_on;

    @Column(name = "cancelled_on")
    private LocalDateTime cancelled_on;

    @Column(name = "is_cancelled")
    private Boolean cancelled;

    @Column(name = "is_expired_payment_date")
    private Boolean expired_payment_date;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    private Casefile casefile;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_rate")
    private PaymentRate rate;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getNumber() {
        return number;
    }

    @Override
    public void setNumber(String number) {
        this.number = number;
    }

    @Override
    public LocalDateTime getCreate_on() {
        return create_on;
    }

    @Override
    public void setCreate_on(LocalDateTime create_on) {
        this.create_on = create_on;
    }

    @Override
    public LocalDateTime getDelivered_on() {
        return delivered_on;
    }

    @Override
    public void setDelivered_on(LocalDateTime delivered_on) {
        this.delivered_on = delivered_on;
    }

    @Override
    public LocalDateTime getCancelled_on() {
        return cancelled_on;
    }

    @Override
    public void setCancelled_on(LocalDateTime cancelled_on) {
        this.cancelled_on = cancelled_on;
    }

    @Override
    public Boolean getCancelled() {
        return cancelled;
    }

    @Override
    public Boolean is_cancelled() {
        return cancelled;
    }

    @Override
    public void setCancelled(Boolean cancelled) {
        this.cancelled = cancelled;
    }

    @Override
    public Boolean getExpired_payment_date() {
        return expired_payment_date;
    }

    @Override
    public Boolean is_expired_payment_data() {
        return expired_payment_date;
    }

    @Override
    public void setExpired_payment_date(Boolean expired_payment_date) {
        this.expired_payment_date = expired_payment_date;
    }

    @Override
    public Casefile getCasefile() {
        return casefile;
    }

    @Override
    public void setCasefile(Casefile casefile) {
        this.casefile = casefile;
    }

    public PaymentRate getRate() {
        return rate;
    }

    public void setRate(PaymentRate rate) {
        this.rate = rate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PaymentProcedure that = (PaymentProcedure) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(number, that.number);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, number);
    }

    @Override
    public String toString() {
        return "PaymentProcedure{" +
                "id=" + id +
                ", number='" + number + '\'' +
                ", create_on=" + create_on +
                ", delivered_on=" + delivered_on +
                ", cancelled_on=" + cancelled_on +
                ", cancelled=" + cancelled +
                ", expired_payment_date=" + expired_payment_date +
                '}';
    }
}
