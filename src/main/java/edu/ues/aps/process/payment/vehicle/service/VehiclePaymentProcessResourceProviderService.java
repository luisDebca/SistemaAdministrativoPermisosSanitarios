package edu.ues.aps.process.payment.vehicle.service;

import java.util.Map;

public interface VehiclePaymentProcessResourceProviderService {

    void setMapValues(Long id);

    Map<String, Object> getMap();
}
