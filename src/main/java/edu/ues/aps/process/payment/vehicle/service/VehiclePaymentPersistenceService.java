package edu.ues.aps.process.payment.vehicle.service;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.payment.base.repository.PaymentProcessPersistenceRepository;
import edu.ues.aps.process.payment.base.repository.PaymentRateCrudRepository;
import edu.ues.aps.process.payment.base.service.AbstractPaymentPersistenceService;
import edu.ues.aps.process.payment.base.service.PaymentPersistenceService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class VehiclePaymentPersistenceService extends AbstractPaymentPersistenceService implements PaymentPersistenceService {

    private final VehicleBunchInformationService informationService;

    public VehiclePaymentPersistenceService(PaymentRateCrudRepository rateCrudRepository, PaymentProcessPersistenceRepository persistenceRepository, VehicleBunchInformationService informationService) {
        super(rateCrudRepository, persistenceRepository);
        this.informationService = informationService;
    }

    @Override
    public void save(Long id, String payment_rate) {
        for (Long id_caseFile : informationService.findAllCaseFileId(id)) {
            savePaymentDocument(id_caseFile, payment_rate);
        }
    }

    @Override
    public void update(Long id, String payment_rate) {
        for (Long id_caseFile : informationService.findAllCaseFileId(id)) {
            updatePaymentDocument(id_caseFile, payment_rate);
        }
    }
}
