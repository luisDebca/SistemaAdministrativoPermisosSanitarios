package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.payment.base.model.PaymentProcedure;

import javax.persistence.NoResultException;

public interface PaymentProcessPersistenceRepository {

    void savePaymentProcedure(Long id_casefile, PaymentProcedure procedure);

    void updatePaymentProcedure(Long id_casefile, String rate_code);

    void setDeliveryDate(Long id_caseFile);

    void setPaymentCancelled(Long id_caseFile);

    Long getLastPaymentNumber();
}
