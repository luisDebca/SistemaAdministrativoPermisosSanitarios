package edu.ues.aps.process.payment.base.dto;

public class PaymentRateDTO implements AbstractPaymentRateDTO {

    private Long rate_id;
    private String rate_code;
    private String rate_description;
    private Double rate_cost;
    private Double rate_maximum;
    private Double rate_minimum;

    public PaymentRateDTO() {
    }

    public PaymentRateDTO(Long rate_id, String rate_code, String rate_description, Double rate_cost, Double rate_maximum, Double rate_minimum) {
        this.rate_id = rate_id;
        this.rate_code = rate_code;
        this.rate_description = rate_description;
        this.rate_cost = rate_cost;
        this.rate_maximum = rate_maximum;
        this.rate_minimum = rate_minimum;
    }

    public Long getRate_id() {
        return rate_id;
    }

    public void setRate_id(Long rate_id) {
        this.rate_id = rate_id;
    }

    public String getRate_code() {
        return rate_code;
    }

    public void setRate_code(String rate_code) {
        this.rate_code = rate_code;
    }

    public String getRate_description() {
        return rate_description;
    }

    public void setRate_description(String rate_description) {
        this.rate_description = rate_description;
    }

    public Double getRate_cost() {
        return rate_cost;
    }

    public void setRate_cost(Double rate_cost) {
        this.rate_cost = rate_cost;
    }

    public Double getRate_maximum() {
        return rate_maximum;
    }

    public void setRate_maximum(Double rate_maximum) {
        this.rate_maximum = rate_maximum;
    }

    public Double getRate_minimum() {
        return rate_minimum;
    }

    public void setRate_minimum(Double rate_minimum) {
        this.rate_minimum = rate_minimum;
    }
}
