package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.process.payment.base.dto.AbstractPaymentRateDTO;

import java.util.List;

public interface PaymentRateDTOFinderRepository {

    List<AbstractPaymentRateDTO> findAll();
}
