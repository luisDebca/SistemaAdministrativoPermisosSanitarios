package edu.ues.aps.process.approval.vehicle.controller;

import edu.ues.aps.process.approval.vehicle.service.VehicleRequestExternalReviewFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/external/review/vehicle")
public class VehicleExternalRequestReviewInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleExternalRequestReviewInformationProviderController.class);

    private static final String REQUESTS = "external_review_vehicle_list";

    private final VehicleRequestExternalReviewFinderService finderService;

    public VehicleExternalRequestReviewInformationProviderController(VehicleRequestExternalReviewFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showAllRequestForVehicleExternalReview(ModelMap model) {
        logger.info("GET:: Show all vehicle request for external review.");
        model.addAttribute("requests", finderService.findAll());
        return REQUESTS;
    }
}
