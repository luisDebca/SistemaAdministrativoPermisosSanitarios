package edu.ues.aps.process.approval.vehicle.repository;

import edu.ues.aps.process.approval.vehicle.dto.AbstractVehicleRequestExternalReviewDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleRequestExternalReviewRepository implements VehicleRequestExternalReviewFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleRequestExternalReviewRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleRequestExternalReviewDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.approval.vehicle.dto.VehicleRequestExternalReviewDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date)" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehicleRequestExternalReviewDTO.class)
                .setParameter("id", ProcessIdentifier.REQUEST_REVIEWS)
                .getResultList();
    }
}
