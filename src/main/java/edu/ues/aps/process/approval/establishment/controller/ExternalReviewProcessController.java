package edu.ues.aps.process.approval.establishment.controller;

import edu.ues.aps.process.approval.establishment.service.ExternalReviewFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/external/review/establishment")
public class ExternalReviewProcessController {

    private static final Logger logger = LogManager.getLogger(ExternalReviewProcessController.class);

    private static final String EXTERNAL_REVIEW_LIST = "external_review_establishment_list";

    private final ExternalReviewFinderService finderService;

    public ExternalReviewProcessController(ExternalReviewFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/list")
    public String showRequestForExternalReviewProcess(ModelMap model){
        logger.info("GET:: Show all request for external review process.");
        model.addAttribute("requests",finderService.findAll());
        return EXTERNAL_REVIEW_LIST;
    }

}
