package edu.ues.aps.process.approval.establishment.service;

import edu.ues.aps.process.approval.establishment.dto.AbstractEstablishmentExternalReviewProcessDTO;

import java.util.List;

public interface ExternalReviewFinderService {

    List<AbstractEstablishmentExternalReviewProcessDTO> findAll();
}
