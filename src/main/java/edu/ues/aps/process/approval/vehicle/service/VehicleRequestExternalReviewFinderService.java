package edu.ues.aps.process.approval.vehicle.service;

import edu.ues.aps.process.approval.vehicle.dto.AbstractVehicleRequestExternalReviewDTO;

import java.util.List;

public interface VehicleRequestExternalReviewFinderService {

    List<AbstractVehicleRequestExternalReviewDTO> findAll();
}
