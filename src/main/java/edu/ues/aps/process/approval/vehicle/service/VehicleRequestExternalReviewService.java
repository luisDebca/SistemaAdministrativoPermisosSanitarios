package edu.ues.aps.process.approval.vehicle.service;

import edu.ues.aps.process.approval.vehicle.dto.AbstractVehicleRequestExternalReviewDTO;
import edu.ues.aps.process.approval.vehicle.repository.VehicleRequestExternalReviewFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleRequestExternalReviewService implements VehicleRequestExternalReviewFinderService {

    private final VehicleRequestExternalReviewFinderRepository finderRepository;

    public VehicleRequestExternalReviewService(VehicleRequestExternalReviewFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractVehicleRequestExternalReviewDTO> findAll() {
        return finderRepository.findAll();
    }
}
