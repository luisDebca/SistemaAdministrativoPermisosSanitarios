package edu.ues.aps.process.approval.establishment.repository;

import edu.ues.aps.process.approval.establishment.dto.AbstractEstablishmentExternalReviewProcessDTO;

import java.util.List;

public interface ExternalReviewFinderRepository {

    List<AbstractEstablishmentExternalReviewProcessDTO> findAll();
}
