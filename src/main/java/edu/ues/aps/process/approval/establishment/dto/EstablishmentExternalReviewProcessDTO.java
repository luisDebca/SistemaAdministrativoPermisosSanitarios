package edu.ues.aps.process.approval.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.EstablishmentRequestDTO;
import edu.ues.aps.process.base.model.CertificationType;

import java.time.LocalDateTime;

public class EstablishmentExternalReviewProcessDTO extends EstablishmentRequestDTO implements AbstractEstablishmentExternalReviewProcessDTO {

    private String resolution_number;

    public EstablishmentExternalReviewProcessDTO(Long id_caseFile, Long id_owner, Long id_establishment, String folder_number, String request_number, String establishment_name, String establishment_type_detail, String establishment_type, String establishment_section, String establishment_address, String establishment_nit, String UCSF, String SIBASI, CertificationType certification_type, LocalDateTime caseFile_createOn, String owner_name, String resolution_number) {
        super(id_caseFile, id_owner, id_establishment, folder_number, request_number, establishment_name, establishment_type_detail, establishment_type, establishment_section, establishment_address, establishment_nit, UCSF, SIBASI, certification_type, caseFile_createOn, owner_name);
        this.resolution_number = resolution_number;
    }

    public String getResolution_number() {
        return resolution_number;
    }
}
