package edu.ues.aps.process.approval.vehicle.repository;

import edu.ues.aps.process.approval.vehicle.dto.AbstractVehicleRequestExternalReviewDTO;

import java.util.List;

public interface VehicleRequestExternalReviewFinderRepository {

    List<AbstractVehicleRequestExternalReviewDTO> findAll();
}
