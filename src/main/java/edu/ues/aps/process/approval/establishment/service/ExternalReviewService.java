package edu.ues.aps.process.approval.establishment.service;

import edu.ues.aps.process.approval.establishment.dto.AbstractEstablishmentExternalReviewProcessDTO;
import edu.ues.aps.process.approval.establishment.repository.ExternalReviewFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ExternalReviewService implements ExternalReviewFinderService {

    private final ExternalReviewFinderRepository finderRepository;

    public ExternalReviewService(ExternalReviewFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractEstablishmentExternalReviewProcessDTO> findAll() {
        return finderRepository.findAll();
    }
}
