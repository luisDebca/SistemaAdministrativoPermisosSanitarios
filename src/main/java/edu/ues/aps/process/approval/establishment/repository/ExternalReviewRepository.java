package edu.ues.aps.process.approval.establishment.repository;

import edu.ues.aps.process.approval.establishment.dto.AbstractEstablishmentExternalReviewProcessDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ExternalReviewRepository implements ExternalReviewFinderRepository {

    private final SessionFactory sessionFactory;

    public ExternalReviewRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractEstablishmentExternalReviewProcessDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.approval.establishment.dto.EstablishmentExternalReviewProcessDTO(ce.id ,owner.id,establishment.id,concat(concat(ce.request_folder,'-'),ce.request_year),concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year)," +
                "establishment.name,establishment.type_detail,etype.type,section.type,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,ucsf.name,sibasi.zone,ce.certification_type,ce.creation_date,owner.name," +
                "concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year))" +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "left join establishment.type etype " +
                "left join etype.section section " +
                "left join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join ce.resolution resolution " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id", AbstractEstablishmentExternalReviewProcessDTO.class)
                .setParameter("id", ProcessIdentifier.REQUEST_REVIEWS)
                .getResultList();
    }
}
