package edu.ues.aps.process.approval.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleRequestDTO;

public interface AbstractVehicleRequestExternalReviewDTO extends AbstractVehicleRequestDTO {
}
