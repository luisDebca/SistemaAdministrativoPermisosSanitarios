package edu.ues.aps.process.approval.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentRequestDTO;

public interface AbstractEstablishmentExternalReviewProcessDTO extends AbstractEstablishmentRequestDTO {

    String getResolution_number();

}
