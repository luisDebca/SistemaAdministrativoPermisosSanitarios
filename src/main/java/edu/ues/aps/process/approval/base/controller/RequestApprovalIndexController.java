package edu.ues.aps.process.approval.base.controller;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RequestApprovalIndexController {

    private static final Logger logger = LogManager.getLogger(RequestApprovalIndexController.class);

    private static final String EXTERNAL_REVIEW_INDEX = "external_review_index";

    @GetMapping({"/external/review/", "/external/review/index"})
    public String showCaseFileExternalReviewIndex(ModelMap model) {
        logger.info("GET:: Show caseFile resolution review index");
        model.addAttribute("process", ProcessIdentifier.REQUEST_REVIEWS);
        return EXTERNAL_REVIEW_INDEX;
    }
}
