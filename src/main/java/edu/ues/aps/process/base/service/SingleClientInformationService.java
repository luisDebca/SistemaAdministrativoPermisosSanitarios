package edu.ues.aps.process.base.service;

public interface SingleClientInformationService {

    String getClientName(Long id_client);

    String getClientPhoneNumber(Long id_client);

}
