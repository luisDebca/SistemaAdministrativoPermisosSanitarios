package edu.ues.aps.process.base.model;

import java.util.Collections;
import java.util.List;

public class EmptyClient implements AbstractClient {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getName() {
        return "Client name not found";
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public String getNationally() {
        return "Client nationally not found";
    }

    @Override
    public void setNationally(String nationally) {

    }

    @Override
    public String getDUI() {
        return "Client DUI not found";
    }

    @Override
    public void setDUI(String DUI) {

    }

    @Override
    public String getPassport() {
        return "Client passport not found";
    }

    @Override
    public void setPassport(String passport) {

    }

    @Override
    public String getResident_card() {
        return "Client resident card not found";
    }

    @Override
    public void setResident_card(String resident_card) {

    }

    @Override
    public String getEmail() {
        return "Client email not found";
    }

    @Override
    public void setEmail(String email) {

    }

    @Override
    public String getTelephone() {
        return "Client telephone not found";
    }

    @Override
    public void setTelephone(String telephone) {

    }

    @Override
    public List<OwnerClient> getOwners() {
        return Collections.emptyList();
    }

    @Override
    public void setOwners(List<OwnerClient> owners) {

    }

    @Override
    public String toString() {
        return "EmptyClient{}";
    }
}
