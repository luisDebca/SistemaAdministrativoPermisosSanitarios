package edu.ues.aps.process.base.dto;

public interface AbstractOwnerEntityDTO {

    Long getId_owner();

    String getOwner_name();

    String getOwner_nit();

    String getOwner_email();

    String getOwner_telephone();

    String getOwner_fax();

    String getClass_type();

    Long getEstablishmentCount();

    Long getVehicleCount();

    Long getClient_count();

}
