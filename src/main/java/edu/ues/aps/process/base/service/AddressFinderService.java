package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractMapDTO;

import java.util.List;

public interface AddressFinderService {

    List<AbstractMapDTO> findAllMunicipalities();

    List<AbstractMapDTO> findAllDepartments();

    List<AbstractMapDTO> findAllMunicipalitiesFromDepartment(Long id_department);
}
