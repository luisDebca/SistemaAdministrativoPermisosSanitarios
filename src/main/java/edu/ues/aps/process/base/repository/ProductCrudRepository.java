package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.Product;

import javax.persistence.NoResultException;
import java.util.List;

public interface ProductCrudRepository {

    List<Product> findAllProductsFromOwner(Long id_owner);

    void delete(Long id_product) throws NoResultException;
}
