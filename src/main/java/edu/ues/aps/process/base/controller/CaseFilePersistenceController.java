package edu.ues.aps.process.base.controller;

import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.service.CaseFilePersistenceService;
import edu.ues.aps.process.base.service.SingleCaseFileFinderService;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;

@Controller
@RequestMapping("/caseFile")
public class CaseFilePersistenceController {

    private static final Logger logger = LogManager.getLogger(CaseFilePersistenceController.class);

    private static final String CASE_FILE_EDIT = "casefile_edit";

    private final SingleCaseFileFinderService finderService;
    private final CaseFilePersistenceService persistenceService;
    private final ProcessUserRegisterService registerService;

    public CaseFilePersistenceController(SingleCaseFileFinderService finderService, CaseFilePersistenceService persistenceService, ProcessUserRegisterService registerService) {
        this.finderService = finderService;
        this.persistenceService = persistenceService;
        this.registerService = registerService;
    }

    @GetMapping("/{id_caseFile}/edit")
    public String showCaseFileEdit(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show caseFile {} edit form", id_caseFile);
        model.addAttribute("casefile", finderService.find(id_caseFile));
        return CASE_FILE_EDIT;
    }

    @PostMapping("/{id_caseFile}/edit")
    public String updateCaseFile(@PathVariable Long id_caseFile, @ModelAttribute Casefile caseFile, Principal principal) {
        logger.info("POST:: Update caseFile {} properties", id_caseFile);
        registerService.addProcessUser(caseFile.getId(), principal.getName(), "process.record.casefile.edit");
        persistenceService.update(caseFile);
        return String.format("redirect:/caseFile/%d/info", caseFile.getId());
    }

}
