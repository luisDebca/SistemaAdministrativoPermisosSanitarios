package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractBaseDTO;

import javax.persistence.NoResultException;

public interface BaseFinderRepository {

    AbstractBaseDTO findById(Long id_caseFile) throws NoResultException;

}
