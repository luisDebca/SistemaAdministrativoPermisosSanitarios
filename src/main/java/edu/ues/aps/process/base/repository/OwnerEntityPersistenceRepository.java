package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractOwner;

public interface OwnerEntityPersistenceRepository {

    void save(AbstractOwner owner);

    void update(AbstractOwner owner);

    void merge(AbstractOwner owner);
}
