package edu.ues.aps.process.base.dto;

public class OwnerEntityDTO implements AbstractOwnerEntityDTO {

    private Long id_owner;
    private String owner_name;
    private String owner_nit;
    private String owner_email;
    private String owner_telephone;
    private String owner_fax;
    private Class class_type;
    private Long establishment_count;
    private Long vehicle_count;
    private Long client_count;

    //For owner index result list
    public OwnerEntityDTO(Long id_owner, String owner_name, String owner_nit, String owner_email, String owner_telephone, String owner_fax, Class class_type) {
        this.id_owner = id_owner;
        this.owner_name = owner_name;
        this.owner_nit = owner_nit;
        this.owner_email = owner_email;
        this.owner_telephone = owner_telephone;
        this.owner_fax = owner_fax;
        this.class_type = class_type;
    }

    //For owner information
    public OwnerEntityDTO(Long id_owner, String owner_name, String owner_nit, String owner_email, String owner_telephone, String owner_fax, Class class_type, Long establishment_count, Long vehicle_count) {
        this.id_owner = id_owner;
        this.owner_name = owner_name;
        this.owner_nit = owner_nit;
        this.owner_email = owner_email;
        this.owner_telephone = owner_telephone;
        this.owner_fax = owner_fax;
        this.class_type = class_type;
        this.establishment_count = establishment_count;
        this.vehicle_count = vehicle_count;
    }

    @Override
    public Long getId_owner() {
        return id_owner;
    }

    @Override
    public String getOwner_name() {
        return owner_name;
    }

    @Override
    public String getOwner_nit() {
        return owner_nit;
    }

    @Override
    public String getOwner_email() {
        return owner_email;
    }

    @Override
    public String getOwner_telephone() {
        return owner_telephone;
    }

    @Override
    public String getOwner_fax() {
        return owner_fax;
    }

    @Override
    public String getClass_type() {
        return class_type.getSimpleName();
    }

    @Override
    public Long getEstablishmentCount() {
        return establishment_count;
    }

    @Override
    public Long getVehicleCount() {
        return vehicle_count;
    }

    @Override
    public Long getClient_count() {
        return client_count;
    }
}
