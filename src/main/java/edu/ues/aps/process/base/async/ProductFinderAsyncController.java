package edu.ues.aps.process.base.async;

import edu.ues.aps.process.base.dto.AbstractProductDTO;
import edu.ues.aps.process.base.service.ProductDTOFinderService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ProductFinderAsyncController {

    private ProductDTOFinderService finderService;

    public ProductFinderAsyncController(ProductDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/find-all-products-from-legal-entity", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractProductDTO> findAllProductsFromLegalEntity(@RequestParam("id") Long id_owner) {
        return finderService.findAllProductsFromLegalEntity(id_owner);
    }
}
