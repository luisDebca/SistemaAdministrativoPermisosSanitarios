package edu.ues.aps.process.base.model;

public interface AbstractPlace {

    Long getId();

    void setId(Long id);

    String getOrganization_name();

    void setOrganization_name(String organization_name);

    String getSection_name();

    void setSection_name(String section_name);

    String getBusiness_description();

    void setBusiness_description(String business_description);

    String getAddress();

    void setAddress(String address);

    String getMunicipality();

    void setMunicipality(String municipality);

    String getDepartment();

    void setDepartment(String department);

    String getDepartment_code();

    void setDepartment_code(String department_code);

    String getZone();

    void setZone(String zone);

    String getTelephone();

    void setTelephone(String telephone);

    String getEmail();

    void setEmail(String email);

    String getFax();

    void setFax(String fax);

    String getAvailability();

    void setAvailability(String availability);
}
