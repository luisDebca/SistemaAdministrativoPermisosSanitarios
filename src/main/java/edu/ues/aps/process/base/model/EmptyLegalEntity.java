package edu.ues.aps.process.base.model;

import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.area.vehicle.model.Vehicle;

import java.util.Collections;
import java.util.List;

public class EmptyLegalEntity implements AbstractLegalEntity {

    @Override
    public List<Product> getProducts() {
        return Collections.emptyList();
    }

    @Override
    public void setProducts(List<Product> products) {

    }

    @Override
    public void addProduct(Product product) {

    }

    @Override
    public void removeProduct(Product product) {

    }

    @Override
    public AbstractAddress getAddress() {
        return new EmptyAddress();
    }

    @Override
    public void setAddress(Address address) {

    }

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getName() {
        return "Legal entity name not found";
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public String getNit() {
        return "Legal entity NIT not found";
    }

    @Override
    public void setNit(String nit) {

    }

    @Override
    public String getEmail() {
        return "Legal entity email not found";
    }

    @Override
    public void setEmail(String email) {

    }

    @Override
    public String getTelephone() {
        return "Legal entity telephone not found";
    }

    @Override
    public void setTelephone(String telephone) {

    }

    @Override
    public String getFAX() {
        return "Legal entity FAX not found";
    }

    @Override
    public void setFAX(String fax) {

    }

    @Override
    public List<Establishment> getEstablishments() {
        return Collections.emptyList();
    }

    @Override
    public void setEstablishments(List<Establishment> establishments) {

    }

    @Override
    public void addEstablishment(Establishment establishment) {

    }

    @Override
    public void removeEstablishment(Establishment establishment) {

    }

    @Override
    public List<Vehicle> getVehicles() {
        return Collections.emptyList();
    }

    @Override
    public void setVehicles(List<Vehicle> vehicles) {

    }

    @Override
    public void addVehicle(Vehicle vehicle) {

    }

    @Override
    public List<OwnerClient> getClients() {
        return Collections.emptyList();
    }

    @Override
    public void setClients(List<OwnerClient> clients) {

    }

    @Override
    public void addClient(Client client, ClientType type) {

    }

    @Override
    public void removeClient(Client client, ClientType type) {

    }
}
