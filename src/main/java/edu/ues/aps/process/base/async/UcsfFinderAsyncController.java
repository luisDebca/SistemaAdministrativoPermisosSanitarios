package edu.ues.aps.process.base.async;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import edu.ues.aps.process.base.service.UcsfDTOFinderService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class UcsfFinderAsyncController {

    private UcsfDTOFinderService finderService;

    public UcsfFinderAsyncController(UcsfDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/find-all-ucsf-from-sibasi", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractMapDTO> findAllUcsfFromSibasi(@RequestParam("id") Long id_sibasi) {
        return finderService.findAllUcsfFromSibasi(id_sibasi);
    }
}
