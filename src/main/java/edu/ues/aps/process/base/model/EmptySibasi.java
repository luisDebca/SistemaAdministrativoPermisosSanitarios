package edu.ues.aps.process.base.model;

import java.util.Collections;
import java.util.List;

public class EmptySibasi implements AbstractSibasi {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getZone() {
        return "SIBASI zone not found";
    }

    @Override
    public List<Ucsf> getUcsfList() {
        return Collections.emptyList();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setZone(String zone) {

    }

    @Override
    public void setUcsfList(List<Ucsf> ucsfList) {

    }

    @Override
    public String toString() {
        return "EmptySibasi{}";
    }
}
