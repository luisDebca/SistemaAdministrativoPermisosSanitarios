package edu.ues.aps.process.base.dto;

public interface AbstractLegalEntityDTO extends AbstractOwnerEntityDTO {

    String getOwner_address();

    Long getProductCount();
}
