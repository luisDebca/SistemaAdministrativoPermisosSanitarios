package edu.ues.aps.process.base.model;

import java.util.List;

public interface AbstractLegalEntity extends AbstractOwner {

    List<Product> getProducts();

    void setProducts(List<Product> products);

    void addProduct(Product product);

    void removeProduct(Product product);

    AbstractAddress getAddress();

    void setAddress(Address address);

}
