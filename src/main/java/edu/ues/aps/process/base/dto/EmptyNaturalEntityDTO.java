package edu.ues.aps.process.base.dto;

public class EmptyNaturalEntityDTO extends AbstractEmptyOwnerEntity implements AbstractNaturalEntityDTO {

    @Override
    public String getOwner_dui() {
        return "Owner DUI not found";
    }

    @Override
    public String getOwner_nationally() {
        return "Owner nationality not found";
    }

    @Override
    public String getOwner_passport() {
        return "Owner passport not found";
    }

    @Override
    public String getOwner_resident_card() {
        return "Owner resident card not found";
    }

    @Override
    public String getClass_type() {
        return "Class not found";
    }
}
