package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import edu.ues.aps.process.base.repository.SibasiDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class SibasiDTOService implements SibasiDTOFinderService {

    private SibasiDTOFinderRepository finderRepository;

    public SibasiDTOService(SibasiDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractMapDTO> findAll() {
        return finderRepository.findAll();
    }
}
