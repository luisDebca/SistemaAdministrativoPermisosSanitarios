package edu.ues.aps.process.base.dto;

public interface AbstractNaturalEntityDTO extends AbstractOwnerEntityDTO {

    String getOwner_dui();

    String getOwner_nationally();

    String getOwner_passport();

    String getOwner_resident_card();
}
