package edu.ues.aps.process.base.model;

import javax.persistence.*;

@Entity
@Table(name = "PRODUCT")
public class Product implements AbstractProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type", length = 50)
    private String type;

    @Column(name = "sanitary_registration", length = 15)
    private String sanitary;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_legalEntity")
    private LegalEntity legalEntity;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String getSanitary() {
        return sanitary;
    }

    @Override
    public void setSanitary(String sanitary) {
        this.sanitary = sanitary;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public LegalEntity getLegalEntity() {
        return legalEntity;
    }

    @Override
    public void setLegalEntity(LegalEntity legalEntity) {
        this.legalEntity = legalEntity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Product product = (Product) o;

        return type.equals(product.type) && sanitary.equals(product.sanitary) && name.equals(product.name);
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + sanitary.hashCode();
        result = 31 * result + name.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Product{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", sanitary='" + sanitary + '\'' +
                ", name='" + name + '\'' +
                "} " + super.toString();
    }
}
