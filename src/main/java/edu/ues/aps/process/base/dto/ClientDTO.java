package edu.ues.aps.process.base.dto;

import edu.ues.aps.process.base.model.ClientType;

public class ClientDTO implements AbstractClientDTO{

    private Long id_owner;
    private Long id_client;
    private ClientType type;
    private ClientType clientType;
    private String client_name;
    private String client_nationally;
    private String client_DUI;
    private String client_passport;
    private String client_resident_card;
    private String client_email;
    private String client_telephone;

    public ClientDTO() {
    }

    public ClientDTO(Long id_client, ClientType type, String client_name) {
        this.id_client = id_client;
        this.type = type;
        this.client_name = client_name;
    }

    public ClientDTO(Long id_client, ClientType type, String client_name, String client_email, String client_telephone) {
        this.id_client = id_client;
        this.type = type;
        this.clientType = type;
        this.client_name = client_name;
        this.client_email = client_email;
        this.client_telephone = client_telephone;
    }

    public ClientDTO(Long id_client, String client_name, String client_nationally, String client_DUI, String client_passport, String client_resident_card, String client_email, String client_telephone) {
        this.id_client = id_client;
        this.client_name = client_name;
        this.client_nationally = client_nationally;
        this.client_DUI = client_DUI;
        this.client_passport = client_passport;
        this.client_resident_card = client_resident_card;
        this.client_email = client_email;
        this.client_telephone = client_telephone;
    }

    public ClientDTO(Long id_client, ClientType type, String client_name, String client_nationally, String client_DUI, String client_passport, String client_resident_card, String client_email, String client_telephone) {
        this.id_client = id_client;
        this.type = type;
        this.clientType = type;
        this.client_name = client_name;
        this.client_nationally = client_nationally;
        this.client_DUI = client_DUI;
        this.client_passport = client_passport;
        this.client_resident_card = client_resident_card;
        this.client_email = client_email;
        this.client_telephone = client_telephone;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public Long getId_client() {
        return id_client;
    }

    public String getType() {
        if(type == null){
            return ClientType.INVALID.getClientType();
        }
        return type.getClientType();
    }

    public ClientType getClientType(){
        return clientType;
    }

    public String getClient_name() {
        return client_name;
    }

    public String getClient_nationally() {
        return client_nationally;
    }

    public String getClient_DUI() {
        return client_DUI;
    }

    public String getClient_passport() {
        return client_passport;
    }

    public String getClient_resident_card() {
        return client_resident_card;
    }

    public String getClient_email() {
        return client_email;
    }

    public String getClient_telephone() {
        return client_telephone;
    }

    public void setId_owner(Long id_owner) {
        this.id_owner = id_owner;
    }

    public void setId_client(Long id_client) {
        this.id_client = id_client;
    }

    public void setType(ClientType type) {
        this.type = type;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public void setClient_nationally(String client_nationally) {
        this.client_nationally = client_nationally;
    }

    public void setClient_DUI(String client_DUI) {
        this.client_DUI = client_DUI;
    }

    public void setClient_passport(String client_passport) {
        this.client_passport = client_passport;
    }

    public void setClient_resident_card(String client_resident_card) {
        this.client_resident_card = client_resident_card;
    }

    public void setClient_email(String client_email) {
        this.client_email = client_email;
    }

    public void setClient_telephone(String client_telephone) {
        this.client_telephone = client_telephone;
    }

    public void setClientType(ClientType clientType) {
        this.clientType = clientType;
    }
}
