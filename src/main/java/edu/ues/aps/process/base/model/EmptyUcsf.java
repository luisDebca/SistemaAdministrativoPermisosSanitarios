package edu.ues.aps.process.base.model;

import java.util.Collections;
import java.util.List;

public class EmptyUcsf implements AbstractUcsf {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getName() {
        return "UCSF name not found";
    }

    @Override
    public AbstractSibasi getSibasi() {
        return new EmptySibasi();
    }

    @Override
    public List<Casefile> getCasefiles() {
        return Collections.emptyList();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setName(String name) {

    }

    @Override
    public void setSibasi(Sibasi sibasi) {

    }

    @Override
    public void setCasefiles(List<Casefile> casefiles) {

    }

    @Override
    public String toString() {
        return "EmptyUcsf{}";
    }
}
