package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractClientDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class ClientDTORepository implements ClientDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public ClientDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractClientDTO find(Long id_client) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.ClientDTO(" +
                "client.id,client.name,client.nationally,client.DUI,client.passport,client.resident_card,client.email,client.telephone) " +
                "from Client client " +
                "where client.id = :id", AbstractClientDTO.class)
                .setParameter("id", id_client)
                .getSingleResult();
    }

    @Override
    public List<AbstractClientDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.ClientDTO(" +
                "client.id,client.name,client.nationally,client.DUI,client.passport,client.resident_card,client.email,client.telephone) " +
                "from Client client ", AbstractClientDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractClientDTO> findAll(Long id_owner) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.ClientDTO(" +
                "client.id,ownerclients.id.type,client.name,client.nationally,client.DUI,client.passport,client.resident_card,client.email,client.telephone) " +
                "from Client client " +
                "inner join client.owners ownerclients " +
                "inner join ownerclients.owner owner " +
                "where owner.id = :id", AbstractClientDTO.class)
                .setParameter("id", id_owner)
                .getResultList();
    }
}
