package edu.ues.aps.process.base.model;

import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.area.vehicle.model.Vehicle;

import java.util.List;

public interface AbstractOwner {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    String getNit();

    void setNit(String nit);

    String getEmail();

    void setEmail(String email);

    String getTelephone();

    void setTelephone(String telephone);

    String getFAX();

    void setFAX(String fax);

    List<Establishment> getEstablishments();

    void setEstablishments(List<Establishment> establishments);

    void addEstablishment(Establishment establishment);

    void removeEstablishment(Establishment establishment);

    List<Vehicle> getVehicles();

    void setVehicles(List<Vehicle> vehicles);

    void addVehicle(Vehicle vehicle);

    List<OwnerClient> getClients();

    void setClients(List<OwnerClient> clients);

    void addClient(Client client, ClientType type);

    void removeClient(Client client, ClientType type);
}
