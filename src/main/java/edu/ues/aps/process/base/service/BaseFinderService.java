package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractBaseDTO;

public interface BaseFinderService {

    AbstractBaseDTO findById(Long id_caseFile);
}
