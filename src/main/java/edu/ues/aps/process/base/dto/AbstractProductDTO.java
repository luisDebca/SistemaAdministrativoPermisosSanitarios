package edu.ues.aps.process.base.dto;

public interface AbstractProductDTO {

    Long getId_product();

    String getProduct_type();

    String getProduct_sanitary();

    String getProduct_name();
}
