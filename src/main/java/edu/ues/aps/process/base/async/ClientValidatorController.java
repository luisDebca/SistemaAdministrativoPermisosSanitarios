package edu.ues.aps.process.base.async;

import edu.ues.aps.process.base.service.ClientValidatorService;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.Locale;

@Controller
@RequestMapping("/client")
public class ClientValidatorController {

    private final ClientValidatorService validatorService;
    private final MessageSource messageSource;

    public ClientValidatorController(ClientValidatorService validatorService, MessageSource messageSource) {
        this.validatorService = validatorService;
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/validate-dui", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateDUI(@RequestParam Long id_client, @RequestParam String dui) {
        return validatorService.isUniqueDUI(id_client, dui) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("client.dui", new String[]{dui}, Locale.getDefault()));
    }

    @GetMapping(value = "/validate-passport", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validatePassport(@RequestParam Long id_client, @RequestParam String passport) {
        return validatorService.isUniquePassport(id_client, passport) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("client.passport", new String[]{passport}, Locale.getDefault()));
    }

    @GetMapping(value = "/validate-resident-card", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateResidentCard(@RequestParam Long id_client, @RequestParam String rc) {
        return validatorService.isUniqueResidentCard(id_client, rc) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("client.residentCard", new String[]{rc}, Locale.getDefault()));
    }
}
