package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractProductDTO;
import edu.ues.aps.process.base.repository.ProductDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ProductDTOService implements ProductDTOFinderService {

    private ProductDTOFinderRepository finderRepository;

    public ProductDTOService(ProductDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractProductDTO> findAllProductsFromLegalEntity(Long id_owner) {
        return finderRepository.findAllProductsFromLegalEntity(id_owner);
    }
}
