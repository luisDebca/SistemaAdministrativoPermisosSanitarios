package edu.ues.aps.process.base.model;

public enum CasefileStatusType {

    IN_PROCESS("En proceso"),
    VALID("Valido"),
    EXPIRED("Expirado"),
    INVALID("Invalido");

    private String status_type;

    CasefileStatusType(String status_type) {
        this.status_type = status_type;
    }

    public String getStatus_type() {
        return status_type;
    }
}
