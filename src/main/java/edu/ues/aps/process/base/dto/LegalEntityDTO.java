package edu.ues.aps.process.base.dto;

import edu.ues.aps.process.base.model.LegalEntity;

public class LegalEntityDTO implements AbstractLegalEntityDTO {

    private Long id_owner;
    private String owner_name;
    private String owner_nit;
    private String owner_email;
    private String owner_telephone;
    private String owner_fax;
    private String owner_address;
    private Long establishment_count;
    private Long vehicle_count;
    private Long product_count;
    private Long client_count;

    public LegalEntityDTO(Long id_owner, String owner_name, String owner_nit, String owner_email, String owner_telephone, String owner_fax, String owner_address) {
        this.id_owner = id_owner;
        this.owner_name = owner_name;
        this.owner_nit = owner_nit;
        this.owner_email = owner_email;
        this.owner_telephone = owner_telephone;
        this.owner_fax = owner_fax;
        this.owner_address = owner_address;
    }

    public LegalEntityDTO(Long id_owner, String owner_name, String owner_nit, String owner_email, String owner_telephone, String owner_fax, String owner_address, Long establishment_count, Long vehicle_count, Long product_count, Long client_count) {
        this.id_owner = id_owner;
        this.owner_name = owner_name;
        this.owner_nit = owner_nit;
        this.owner_email = owner_email;
        this.owner_telephone = owner_telephone;
        this.owner_fax = owner_fax;
        this.owner_address = owner_address;
        this.establishment_count = establishment_count;
        this.vehicle_count = vehicle_count;
        this.product_count = product_count;
        this.client_count = client_count;
    }

    @Override
    public Long getId_owner() {
        return id_owner;
    }

    @Override
    public String getOwner_name() {
        return owner_name;
    }

    @Override
    public String getOwner_nit() {
        return owner_nit;
    }

    @Override
    public String getOwner_email() {
        return owner_email;
    }

    @Override
    public String getOwner_telephone() {
        return owner_telephone;
    }

    @Override
    public String getOwner_fax() {
        return owner_fax;
    }

    @Override
    public String getOwner_address() {
        return owner_address;
    }

    @Override
    public String getClass_type() {
        return LegalEntity.class.getSimpleName();
    }

    @Override
    public Long getProductCount() {
        return product_count;
    }

    @Override
    public Long getEstablishmentCount() {
        return establishment_count;
    }

    @Override
    public Long getVehicleCount() {
        return vehicle_count;
    }

    @Override
    public Long getClient_count() {
        if (client_count == null){
            return 0L;
        }
        return client_count;
    }
}
