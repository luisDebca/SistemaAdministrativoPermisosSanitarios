package edu.ues.aps.process.base.dto;

public class EmptyLegalEntityDTO extends AbstractEmptyOwnerEntity implements AbstractLegalEntityDTO {

    @Override
    public String getOwner_address() {
        return "Legal entity address not found";
    }

    @Override
    public String getClass_type() {
        return "Class not found";
    }

    @Override
    public Long getProductCount() {
        return 0L;
    }

}
