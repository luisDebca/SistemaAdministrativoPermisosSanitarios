package edu.ues.aps.process.base.service;

public interface OwnerValidatorService {

    Boolean isUniqueName(Long id_owner, String name);

    Boolean isUniqueNIT(Long id_owner, String nit);

    Boolean isUniqueDUI(Long id_owner, String dui);

    Boolean isUniquePassport(Long id_owner, String passport);

    Boolean isUniqueResidentCard(Long id_owner, String residentCard);
}
