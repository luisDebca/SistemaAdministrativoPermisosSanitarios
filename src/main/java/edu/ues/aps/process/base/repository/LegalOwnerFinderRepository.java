package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractOwner;
import edu.ues.aps.process.base.model.EmptyOwner;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class LegalOwnerFinderRepository implements OwnerFinderRepository{

    private SessionFactory sessionFactory;

    public LegalOwnerFinderRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractOwner findOwnerById(Long id_owner) throws NoResultException{
        try{
            return sessionFactory.getCurrentSession().createQuery("select owner " +
                    "from LegalEntity owner " +
                    "join fetch owner.address address " +
                    "join fetch address.municipality municipality " +
                    "join fetch municipality.department " +
                    "left join fetch owner.products " +
                    "where owner.id = :id",AbstractOwner.class)
                    .setParameter("id",id_owner)
                    .getSingleResult();
        }catch (NoResultException e){
            return new EmptyOwner();
        }
    }

    @Override
    public AbstractOwner findOwnerByCasefileId(Long id_casefile) throws NoResultException{
        try{
            return sessionFactory.getCurrentSession().createQuery("select owner " +
                    "from LegalEntity owner " +
                    "inner join owner.establishments establishment " +
                    "inner join establishment.casefiles casefile " +
                    "join fetch owner.address address " +
                    "join fetch address.municipality municipality " +
                    "join fetch municipality.department " +
                    "join fetch owner.products " +
                    "where casefile.id = :id",AbstractOwner.class)
                    .setParameter("id",id_casefile)
                    .getSingleResult();
        }catch (NoResultException e){
            return new EmptyOwner();
        }
    }
}
