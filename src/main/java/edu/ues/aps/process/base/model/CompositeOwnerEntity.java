package edu.ues.aps.process.base.model;

import java.util.ArrayList;
import java.util.List;

public class CompositeOwnerEntity {

    private AbstractOwner composite_owner;
    private List<AbstractOwner> distinct_owners = new ArrayList<>();

    public CompositeOwnerEntity(List<AbstractOwner> ownerList) {
        if(!ownerList.isEmpty()){
            AbstractOwner distinct = ownerList.get(0);
            distinct_owners.add(distinct);
            for(AbstractOwner owner: ownerList){
                if(!owner.equals(distinct)){
                    distinct_owners.add(owner);
                    distinct = owner;
                }
            }
            if(distinct_owners.size() == 1){
                composite_owner = distinct_owners.get(0);
            }else {
                composite_owner = new OwnerEntity();
                for(AbstractOwner owner: distinct_owners){
                    composite_owner.setName((composite_owner.getName() == null ? "" : composite_owner.getName() + "||") + owner.getName());
                    composite_owner.setNit((composite_owner.getNit() == null ? "" : composite_owner.getNit() + "||") + owner.getNit());
                    composite_owner.setEmail((composite_owner.getEmail() == null ? "" : composite_owner.getEmail() + "||") + owner.getEmail());
                    composite_owner.setFAX((composite_owner.getFAX() ==  null ? "" : composite_owner.getFAX() + "||") + owner.getFAX());
                    composite_owner.setTelephone((composite_owner.getTelephone() == null ? "" : composite_owner.getTelephone() + "||") + owner.getTelephone());
                }
            }
        }else {
            composite_owner = new EmptyOwner();
        }
    }

    public AbstractOwner getComposite_owner() {
        return composite_owner;
    }

    public List<AbstractOwner> getDistinct_owners() {
        return distinct_owners;
    }

    public int getCompositeNumber(){
        return distinct_owners.size();
    }
}
