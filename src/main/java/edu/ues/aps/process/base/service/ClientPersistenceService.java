package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.ClientType;
import edu.ues.aps.process.base.dto.AbstractClientDTO;

public interface ClientPersistenceService {

    AbstractClientDTO find(Long id_client);

    AbstractClientDTO find(Long id_owner, Long id_client, ClientType type);

    void update(AbstractClientDTO dto);

    void update(AbstractClientDTO dto, ClientType type);

    void save(Long id_owner, AbstractClientDTO dto);

    void delete(Long id_client);

    void delete(Long id_owner, Long id_client, ClientType type);

}
