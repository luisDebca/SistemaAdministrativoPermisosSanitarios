package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractLegalEntity;
import edu.ues.aps.process.base.model.AbstractNaturalEntity;

import javax.persistence.NoResultException;

public interface OwnerEntityFinderRepository {

    AbstractLegalEntity getLegalEntity(Long id_owner) throws NoResultException;

    AbstractNaturalEntity getNaturalEntity(Long id_owner) throws NoResultException;
}
