package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.AbstractOwner;

public interface OwnerFinderService {

    AbstractOwner findById(Long id_owner);

}
