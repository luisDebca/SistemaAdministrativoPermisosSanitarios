package edu.ues.aps.process.base.model;

public interface AbstractNaturalEntity extends AbstractOwner {

    String getDUI();

    void setDUI(String DUI);

    String getNationally();

    void setNationally(String nationally);

    String getPassport();

    void setPassport(String passport);

    String getResidentCard();

    void setResidentCard(String residentCard);
}
