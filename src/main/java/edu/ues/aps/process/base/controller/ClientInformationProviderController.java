package edu.ues.aps.process.base.controller;

import edu.ues.aps.process.base.service.ClientDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class ClientInformationProviderController {

    private static final Logger logger = LogManager.getLogger(ClientInformationProviderController.class);

    private static final String ALL = "client_list";
    private static final String CLIENT_LIST = "owner_client_list";
    private static final String CLIENT_INFO = "client_info";

    private final ClientDTOFinderService finderService;

    public ClientInformationProviderController(ClientDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/client/all")
    public String showAllClients(ModelMap model) {
        logger.info("GET:: Show all clients.");
        model.addAttribute("clients", finderService.findAll());
        return ALL;
    }

    @GetMapping("/owner/{id_owner}/client/all")
    public String showAllClientsForOwner(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show all clients for owner {}.", id_owner);
        model.addAttribute("id_owner", id_owner);
        model.addAttribute("clients", finderService.findAll(id_owner));
        return CLIENT_LIST;
    }

    @GetMapping({"/owner/{id_owner}/client/{id_client}/info", "/client/{id_client}/info"})
    public String showClientInformation(@PathVariable(required = false) Long id_owner, @PathVariable Long id_client, ModelMap model) {
        logger.info("GET: Show client {} information.", id_client);
        model.addAttribute("id_owner", id_owner);
        model.addAttribute("client", finderService.find(id_client));
        return CLIENT_INFO;
    }
}
