package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractMapDTO;

import java.util.List;

public interface UcsfDTOFinderService {

    List<AbstractMapDTO> findAll();

    List<AbstractMapDTO> findAllUcsfFromSibasi(Long id_sibasi);
}
