package edu.ues.aps.process.base.model;

import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.area.vehicle.model.Vehicle;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity
@Table(name = "OWNER_ENTITY")
@Inheritance(strategy = InheritanceType.JOINED)
public class OwnerEntity implements AbstractOwner {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "NIT", length = 17, unique = true)
    private String nit;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "telephone", length = 15)
    private String telephone;

    @Column(name = "FAX", length = 15)
    private String FAX;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner", orphanRemoval = true)
    private List<Establishment> establishments = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner", orphanRemoval = true)
    private List<Vehicle> vehicles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "owner", orphanRemoval = true)
    private List<OwnerClient> clients = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getNit() {
        return nit;
    }

    @Override
    public void setNit(String nit) {
        if (nit == null) {
            this.nit = null;
        } else {
            if (nit.isEmpty()) {
                this.nit = null;
            } else {
                this.nit = nit;
            }
        }
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getTelephone() {
        return telephone;
    }

    @Override
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String getFAX() {
        return FAX;
    }

    @Override
    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    @Override
    public List<Establishment> getEstablishments() {
        return establishments;
    }

    @Override
    public void setEstablishments(List<Establishment> establishments) {
        this.establishments = establishments;
    }

    @Override
    public void addEstablishment(Establishment establishment) {
        establishments.add(establishment);
        establishment.setOwner(this);
    }

    @Override
    public void removeEstablishment(Establishment establishment) {
        establishments.remove(establishment);
        establishment.setOwner(null);
    }

    @Override
    public List<Vehicle> getVehicles() {
        return vehicles;
    }

    @Override
    public void setVehicles(List<Vehicle> vehicles) {
        this.vehicles = vehicles;
    }

    @Override
    public void addVehicle(Vehicle vehicle) {
        vehicles.add(vehicle);
        vehicle.setOwner(this);
    }

    @Override
    public List<OwnerClient> getClients() {
        return clients;
    }

    @Override
    public void setClients(List<OwnerClient> clients) {
        this.clients = clients;
    }

    @Override
    public void addClient(Client client, ClientType type) {
        OwnerClient ownerClient = new OwnerClient(this, client, type);
        clients.add(ownerClient);
        client.getOwners().add(ownerClient);
    }

    @Override
    public void removeClient(Client client, ClientType type) {
        for (Iterator<OwnerClient> iterator = clients.iterator(); iterator.hasNext(); ) {
            OwnerClient ownerClient = iterator.next();
            if (ownerClient.getOwner().equals(this) &&
                    ownerClient.getClient().equals(client) &&
                    ownerClient.getId().getType().equals(type)) {
                iterator.remove();
                ownerClient.getClient().getOwners().remove(ownerClient);
                ownerClient.setClient(null);
                ownerClient.setOwner(null);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OwnerEntity that = (OwnerEntity) o;
        return name.equals(that.name) && nit.equals(that.nit);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + nit.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "OwnerEntity{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", nit='" + nit + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                ", FAX='" + FAX + '\'' +
                '}';
    }
}
