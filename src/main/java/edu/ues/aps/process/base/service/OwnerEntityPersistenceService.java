package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.*;

public interface OwnerEntityPersistenceService {

    void save(AbstractOwner owner);

    void update(AbstractNaturalEntity owner);

    void update(AbstractLegalEntity owner);
}
