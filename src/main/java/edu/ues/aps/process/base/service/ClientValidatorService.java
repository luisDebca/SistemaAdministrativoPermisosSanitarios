package edu.ues.aps.process.base.service;

public interface ClientValidatorService {

    Boolean isUniqueDUI(Long id_client, String dui);

    Boolean isUniquePassport(Long id_client, String passport);

    Boolean isUniqueResidentCard(Long id_client, String rc);
}
