package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractClient;
import edu.ues.aps.process.base.model.ClientType;

public interface ClientPersistenceRepository {

    AbstractClient find(Long id_client);

    AbstractClient find(Long id_owner, Long id_client, ClientType type);

    void save(AbstractClient client);

    void delete(AbstractClient client);

    void delete(Long id_owner, Long id_client, ClientType type);

    void updateType(Long id_owner, Long id_client, ClientType old_type, ClientType new_type);
}
