package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.AbstractCasefile;

public interface CaseFilePersistenceService {

    AbstractCasefile findById(Long id_caseFile);

    void saveOrUpdate(AbstractCasefile caseFile);

    void merge(AbstractCasefile caseFile);

    void update(AbstractCasefile caseFile);

}
