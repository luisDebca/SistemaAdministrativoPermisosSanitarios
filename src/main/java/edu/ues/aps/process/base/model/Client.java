package edu.ues.aps.process.base.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CLIENT")
public class Client implements AbstractClient {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 150)
    private String name;

    @Column(name = "nationally", length = 50)
    private String nationally;

    @Column(name = "DUI", unique = true, length = 11)
    private String DUI;

    @Column(name = "passport", unique = true, length = 15)
    private String passport;

    @Column(name = "resident_card", unique = true, length = 15)
    private String resident_card;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "telephone", length = 15)
    private String telephone;

    @OneToMany(mappedBy = "client", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<OwnerClient> owners = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getNationally() {
        return nationally;
    }

    @Override
    public void setNationally(String nationally) {
        this.nationally = nationally;
    }

    @Override
    public String getDUI() {
        return DUI;
    }

    @Override
    public void setDUI(String DUI) {
        if (DUI == null) {
            this.DUI = null;
        } else {
            if (DUI.isEmpty()) {
                this.DUI = null;
            } else {
                this.DUI = DUI;
            }
        }
    }

    @Override
    public String getPassport() {
        return passport;
    }

    @Override
    public void setPassport(String passport) {
        if (passport == null) {
            this.passport = null;
        } else {
            if (passport.isEmpty()) {
                this.passport = null;
            } else {
                this.passport = passport;
            }
        }
    }

    @Override
    public String getResident_card() {
        return resident_card;
    }

    @Override
    public void setResident_card(String resident_card) {
        if (resident_card == null) {
            this.resident_card = null;
        } else {
            if (resident_card.isEmpty()) {
                this.resident_card = null;
            } else {
                this.resident_card = resident_card;
            }
        }
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getTelephone() {
        return telephone;
    }

    @Override
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public List<OwnerClient> getOwners() {
        return owners;
    }

    @Override
    public void setOwners(List<OwnerClient> owners) {
        this.owners = owners;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Client client = (Client) o;

        return name.equals(client.name) && (DUI != null ? DUI.equals(client.DUI) : client.DUI == null) && (passport != null ? passport.equals(client.passport) : client.passport == null) && (resident_card != null ? resident_card.equals(client.resident_card) : client.resident_card == null);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + (DUI != null ? DUI.hashCode() : 0);
        result = 31 * result + (passport != null ? passport.hashCode() : 0);
        result = 31 * result + (resident_card != null ? resident_card.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", nationally='" + nationally + '\'' +
                ", DUI='" + DUI + '\'' +
                ", passport='" + passport + '\'' +
                ", resident_card='" + resident_card + '\'' +
                ", email='" + email + '\'' +
                ", telephone='" + telephone + '\'' +
                '}';
    }
}
