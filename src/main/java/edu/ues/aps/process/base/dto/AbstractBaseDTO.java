package edu.ues.aps.process.base.dto;

public interface AbstractBaseDTO {

    Long getId_caseFile();

    Long getId_owner();

    Long getId_target();

    Long getId_group();

    String getFolder();

    String getRequest();

    String getResolution();

    String getOwner();

    String getTarget();

    String getSection();

    String getSectionType();

    String getCertification();

    String getStatus();

    void setId_caseFile(Long id_caseFile);

    void setId_owner(Long id_owner);

    void setId_target(Long id_target);

    void setOwner(String owner);

    void setTarget(String target);

    void setId_group(Long id_group);
}
