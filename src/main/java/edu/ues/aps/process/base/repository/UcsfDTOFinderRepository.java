package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractMapDTO;

import java.util.List;

public interface UcsfDTOFinderRepository {

    List<AbstractMapDTO> findAll();

    List<AbstractMapDTO> findAllUcsfFromSibasi(Long id_sibasi);
}
