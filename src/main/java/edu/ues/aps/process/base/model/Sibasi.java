package edu.ues.aps.process.base.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SIBASI")
public class Sibasi implements AbstractSibasi {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "zone", nullable = false, unique = true, length = 25)
    private String zone;

    @OneToMany(mappedBy = "sibasi", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Ucsf> ucsfList = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getZone() {
        return zone;
    }

    @Override
    public void setZone(String zone) {
        this.zone = zone;
    }

    @Override
    public List<Ucsf> getUcsfList() {
        return ucsfList;
    }

    @Override
    public void setUcsfList(List<Ucsf> ucsfList) {
        this.ucsfList = ucsfList;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Sibasi sibasi = (Sibasi) o;

        return zone.equals(sibasi.zone);
    }

    @Override
    public int hashCode() {
        return zone.hashCode();
    }

    @Override
    public String toString() {
        return "Sibasi{" +
                "id=" + id +
                ", zone='" + zone + '\'' +
                '}';
    }
}
