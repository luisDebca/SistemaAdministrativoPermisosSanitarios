package edu.ues.aps.process.base.model;

public enum ClientType {

    LEGAL("REPRESENTANTE LEGAL"),
    APODERADO("APODERADO"),
    AUTORIZADO("PERSONA AUTORIZADA"),
    PROPIETARIO("PROPIETARIO"),
    INVALID("INVALIDO");

    private String clientType;

    ClientType(String type) {
        this.clientType = type;
    }

    public String getClientType() {
        return clientType;
    }
}
