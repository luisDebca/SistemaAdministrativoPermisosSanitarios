package edu.ues.aps.process.base.service;

public interface CaseFileRequiredReviewValidatorService {

    void validateCaseFile(Long id_caseFile);

    void validateUCSFDelivery(Long id_caseFile);
}
