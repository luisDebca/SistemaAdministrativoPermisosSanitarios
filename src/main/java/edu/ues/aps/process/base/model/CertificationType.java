package edu.ues.aps.process.base.model;

public enum CertificationType {

    FIRST("Primera vez"), RENEWAL("Renovación"), OUTDATED("Fuera de fecha"), INVALID("Ivalido");

    private String certificationType;

    CertificationType(String type) {
        this.certificationType = type;
    }

    public String getCertificationType() {
        return certificationType;
    }
}
