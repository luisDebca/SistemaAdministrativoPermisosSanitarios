package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractLegalEntity;
import edu.ues.aps.process.base.model.AbstractOwner;
import edu.ues.aps.process.base.model.OwnerEntity;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class OwnerRepository implements OwnerCrudRepository{

    private SessionFactory sessionFactory;

    public OwnerRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractLegalEntity findLegalEntityById(Long id) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select entity " +
                "from LegalEntity entity " +
                "left join fetch entity.address address " +
                "join fetch address.municipality municipality " +
                "join fetch municipality.department department " +
                "where entity.id = :id", AbstractLegalEntity.class)
                .setParameter("id", id)
                .getSingleResult();
    }

    @Override
    public void save(AbstractOwner owner) {
        sessionFactory.getCurrentSession().persist(owner);
    }

    @Override
    public void update(AbstractOwner owner) {
        sessionFactory.getCurrentSession().saveOrUpdate(owner);
    }

}
