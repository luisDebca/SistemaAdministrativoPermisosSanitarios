package edu.ues.aps.process.base.dto;

import edu.ues.aps.process.base.model.NaturalEntity;

public class NaturalEntityDTO implements AbstractNaturalEntityDTO {

    private Long id_owner;
    private String owner_name;
    private String owner_nit;
    private String owner_email;
    private String owner_telephone;
    private String owner_fax;
    private String owner_dui;
    private String owner_nationally;
    private String owner_passport;
    private String owner_resident_card;
    private Long establishment_count;
    private Long vehicle_count;
    private Long client_count;

    public NaturalEntityDTO(Long id_owner, String owner_name, String owner_nit, String owner_email, String owner_telephone, String owner_fax, String owner_dui, String owner_nationally, String owner_passport, String owner_resident_card) {
        this.id_owner = id_owner;
        this.owner_name = owner_name;
        this.owner_nit = owner_nit;
        this.owner_email = owner_email;
        this.owner_telephone = owner_telephone;
        this.owner_fax = owner_fax;
        this.owner_dui = owner_dui;
        this.owner_nationally = owner_nationally;
        this.owner_passport = owner_passport;
        this.owner_resident_card = owner_resident_card;
    }

    public NaturalEntityDTO(Long id_owner, String owner_name, String owner_nit, String owner_email, String owner_telephone, String owner_fax, String owner_dui, String owner_nationally, String owner_passport, String owner_resident_card, Long establishment_count, Long vehicle_count, Long client_count) {
        this.id_owner = id_owner;
        this.owner_name = owner_name;
        this.owner_nit = owner_nit;
        this.owner_email = owner_email;
        this.owner_telephone = owner_telephone;
        this.owner_fax = owner_fax;
        this.owner_dui = owner_dui;
        this.owner_nationally = owner_nationally;
        this.owner_passport = owner_passport;
        this.owner_resident_card = owner_resident_card;
        this.establishment_count = establishment_count;
        this.vehicle_count = vehicle_count;
        this.client_count = client_count;
    }

    @Override
    public Long getId_owner() {
        return id_owner;
    }

    @Override
    public String getOwner_name() {
        return owner_name;
    }

    @Override
    public String getOwner_nit() {
        return owner_nit;
    }

    @Override
    public String getOwner_email() {
        return owner_email;
    }

    @Override
    public String getOwner_telephone() {
        return owner_telephone;
    }

    @Override
    public String getOwner_fax() {
        return owner_fax;
    }

    @Override
    public String getOwner_dui() {
        return owner_dui;
    }

    @Override
    public String getOwner_nationally() {
        return owner_nationally;
    }

    @Override
    public String getOwner_passport() {
        return owner_passport;
    }

    @Override
    public String getOwner_resident_card() {
        return owner_resident_card;
    }

    @Override
    public String getClass_type() {
        return NaturalEntity.class.getSimpleName();
    }

    @Override
    public Long getEstablishmentCount() {
        return establishment_count;
    }

    @Override
    public Long getVehicleCount() {
        return vehicle_count;
    }

    @Override
    public Long getClient_count() {
        if(client_count == null){
            return 0L;
        }
        return client_count;
    }
}
