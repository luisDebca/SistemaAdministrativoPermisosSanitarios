package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;

import java.util.List;

public interface OwnerFinderByParameterService {

    List<AbstractOwnerEntityDTO> findOwnerBy(String parameter);

    AbstractOwnerEntityDTO findOwnerById(Long id_owner);
}
