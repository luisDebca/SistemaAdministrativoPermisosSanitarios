package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractCasefile;

public interface CaseFilePersistenceRepository {

    AbstractCasefile findById(Long id_caseFile);

    void merge(AbstractCasefile caseFile);

    void update(AbstractCasefile caseFile);

    void saveOrUpdate(AbstractCasefile caseFile);
}
