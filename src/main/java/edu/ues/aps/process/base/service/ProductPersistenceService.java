package edu.ues.aps.process.base.service;

import javax.persistence.NoResultException;

public interface ProductPersistenceService {

    void delete(Long id_product) throws NoResultException;
}
