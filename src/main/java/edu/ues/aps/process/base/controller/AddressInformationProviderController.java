package edu.ues.aps.process.base.controller;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import edu.ues.aps.process.base.service.AddressFinderService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@ControllerAdvice
@RequestMapping({
        "/owner/legal-edit-*",
        "/owner/*/vehicle/casefile/new",
        "/owner/*/vehicle/casefile/*/edit",
        "owner/*/establishment/new",
        "/establishment/*/edit",
        "/owner/*/establishment/*/edit",
        "/owner/*/vehicle/casefile/bunch/*/edit",
        "/vehicle/case-file/bunch/*/edit"})
public class AddressInformationProviderController implements InitializingBean {

    private List<AbstractMapDTO> departments;
    private List<AbstractMapDTO> municipalities;
    private AddressFinderService finderService;

    public AddressInformationProviderController(AddressFinderService finderService) {
        this.finderService = finderService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        departments = finderService.findAllDepartments();
        municipalities = finderService.findAllMunicipalities();
    }

    @ModelAttribute("departments")
    public List<AbstractMapDTO> getDepartments() {
        return departments;
    }

    @ModelAttribute("municipalities")
    public List<AbstractMapDTO> getMunicipalities() {
        return municipalities;
    }
}
