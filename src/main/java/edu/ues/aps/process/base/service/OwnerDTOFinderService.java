package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractLegalEntityDTO;
import edu.ues.aps.process.base.dto.AbstractNaturalEntityDTO;
import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;

import java.util.List;

public interface OwnerDTOFinderService {

    AbstractOwnerEntityDTO findById(Long id_owner);

    AbstractLegalEntityDTO findLegalEntity(Long id_owner);

    AbstractNaturalEntityDTO findNaturalEntity(Long id_owner);

    List<AbstractOwnerEntityDTO> findAllOwners();
}
