package edu.ues.aps.process.base.model;

import java.util.List;

public interface AbstractDepartment {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    List<Municipality> getMunicipalities();

    void setMunicipalities(List<Municipality> municipalities);
}
