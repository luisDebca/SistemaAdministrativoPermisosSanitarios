package edu.ues.aps.process.base.model;

import javax.persistence.*;

@Entity
@Table(name = "ADDRESS")
public class Address implements AbstractAddress {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "details", nullable = false, length = 500)
    private String details;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_municipality")
    private Municipality municipality;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getDetails() {
        return details;
    }

    @Override
    public void setDetails(String details) {
        this.details = details;
    }

    @Override
    public Municipality getMunicipality() {
        return municipality;
    }

    @Override
    public void setMunicipality(Municipality municipality) {
        this.municipality = municipality;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Address address = (Address) o;

        return details.equals(address.details);
    }

    @Override
    public int hashCode() {
        return details.hashCode();
    }

    @Override
    public String toString() {
        return "Address{" +
                "id=" + id +
                ", details='" + details + '\'' +
                '}';
    }
}
