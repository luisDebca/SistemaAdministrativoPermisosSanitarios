package edu.ues.aps.process.base.model;

import java.util.List;

public interface AbstractMunicipality {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    AbstractDepartment getDepartment();

    void setDepartment(Department department);

    List<Address> getAddresses();

    void setAddresses(List<Address> addresses);
}
