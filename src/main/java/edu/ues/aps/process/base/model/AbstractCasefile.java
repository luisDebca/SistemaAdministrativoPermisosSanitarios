package edu.ues.aps.process.base.model;

import edu.ues.aps.process.legal.base.model.LegalReview;
import edu.ues.aps.process.payment.base.model.AbstractPaymentProcedure;
import edu.ues.aps.process.payment.base.model.PaymentProcedure;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.model.Process;
import edu.ues.aps.process.requirement.base.model.AbstractRequirementControlDocument;
import edu.ues.aps.process.requirement.base.model.RequirementControlDocument;
import edu.ues.aps.process.resolution.base.model.AbstractResolution;
import edu.ues.aps.process.resolution.base.model.Memorandum;
import edu.ues.aps.process.resolution.base.model.Resolution;
import edu.ues.aps.process.tracing.base.model.AbstractTracing;
import edu.ues.aps.process.tracing.base.model.Tracing;

import java.time.LocalDateTime;
import java.util.List;

public interface AbstractCasefile {

    Long getId();

    void setId(Long id);

    String getRequest_number();

    void setRequest_number(String case_file_number);

    String getRequest_code();

    void setRequest_code(String request_code);

    String getRequest_year();

    void setRequest_year(String request_year);

    Boolean getValidate();

    void setValidate(Boolean validate);

    Boolean getApply_payment();

    void setApply_payment(Boolean apply_payment);

    CertificationType getCertification_type();

    void setCertification_type(CertificationType certification_type);

    Integer getCertification_duration_in_years();

    void setCertification_duration_in_years(Integer certification_duration_in_years);

    LocalDateTime getCertification_start_on();

    void setCertification_start_on(LocalDateTime certification_start_on);

    AbstractUcsf getUcsf();

    void setUcsf(Ucsf ucsf);

    List<CasefileProcess> getProcesses();

    void setProcesses(List<CasefileProcess> processes);

    AbstractTracing getTracing();

    void setTracing(Tracing tracing);

    List<LegalReview> getLegalReviewls();

    void setLegalReviewls(List<LegalReview> legalReviewls);

    AbstractPaymentProcedure getPaymentProcedure();

    void setPaymentProcedure(PaymentProcedure paymentProcedure);

    List<Memorandum> getMemorandums();

    void setMemorandums(List<Memorandum> memorandums);

    AbstractRequirementControlDocument getControlDocument();

    void setControlDocument(RequirementControlDocument controlDocument);

    void addControlDocument(RequirementControlDocument document);

    void removeControlDocument();

    void addProcessRecord(Process process);

    AbstractResolution getResolution();

    void setResolution(Resolution resolution);

    void addResolution(Resolution resolution);

    void addLegalReview(LegalReview legalReview);

    void removeLegalReview(LegalReview legalReview);

    Integer getCertificationDurationInYears();

    void addPaymentProcedure(PaymentProcedure procedure);

    void removePaymentProcedure();

    CasefileSection getSection();

    void setSection(CasefileSection section);

    void addTracing(Tracing tracing);

    CasefileStatusType getStatus();

    void setStatus(CasefileStatusType status);

    LocalDateTime getCreation_date();

    void setCreation_date(LocalDateTime creation_date);

    String getRequest_folder();

    void setRequest_folder(String request_folder);

    void addProcessRecord(CasefileProcess record);

    void addMemorandum(Memorandum memo);

    void removeMemorandum(Memorandum memo);

    Boolean getUcsf_delivery();

    void setUcsf_delivery(Boolean ucsf_delivery);

}
