package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractProductDTO;
import edu.ues.aps.process.base.model.Product;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class ProductRepository implements ProductCrudRepository, ProductDTOFinderRepository {

    private SessionFactory sessionFactory;

    public ProductRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<Product> findAllProductsFromOwner(Long id_owner) {
        return sessionFactory.getCurrentSession().createQuery("select product " +
                "from Product product " +
                "inner join fetch product.legalEntity owner " +
                "where owner.id = :id", Product.class)
                .setParameter("id", id_owner)
                .getResultList();
    }

    @Override
    public List<AbstractProductDTO> findAllProductsFromLegalEntity(Long id_owner) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.ProductDTO(product.id,product.type,product.sanitary,product.name) " +
                "from Product product " +
                "inner join product.legalEntity owner " +
                "where owner.id = :id", AbstractProductDTO.class)
                .setParameter("id", id_owner)
                .getResultList();
    }

    @Override
    public void delete(Long id_product) throws NoResultException {
        Product product = sessionFactory.getCurrentSession().createQuery("select product " +
                "from Product product " +
                "where product.id = :id", Product.class)
                .setParameter("id", id_product)
                .getSingleResult();
        sessionFactory.getCurrentSession().delete(product);
    }
}
