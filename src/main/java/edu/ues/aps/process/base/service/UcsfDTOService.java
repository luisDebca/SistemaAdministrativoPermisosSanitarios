package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import edu.ues.aps.process.base.repository.UcsfDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class UcsfDTOService implements UcsfDTOFinderService {

    private UcsfDTOFinderRepository finderRepository;

    public UcsfDTOService(UcsfDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractMapDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public List<AbstractMapDTO> findAllUcsfFromSibasi(Long id_sibasi) {
        return finderRepository.findAllUcsfFromSibasi(id_sibasi);
    }
}
