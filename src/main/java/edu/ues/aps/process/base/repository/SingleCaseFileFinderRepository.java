package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractCasefile;

import javax.persistence.NoResultException;

public interface SingleCaseFileFinderRepository {

    AbstractCasefile find(Long id_caseFile) throws NoResultException;
}
