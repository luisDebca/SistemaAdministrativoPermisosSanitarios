package edu.ues.aps.process.base.repository;

public interface CaseFileRequiredReviewValidatorRepository {

    void validateCaseFile(Long id_caseFile);

    void validateUCSFDelivery(Long id_caseFile);
}
