package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractMapDTO;

import java.util.List;

public interface SibasiDTOFinderRepository {

    List<AbstractMapDTO> findAll();
}
