package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.ClientDTO;
import edu.ues.aps.process.base.dto.EmptyClientDTO;
import edu.ues.aps.process.base.model.AbstractClient;
import edu.ues.aps.process.base.model.Client;
import edu.ues.aps.process.base.model.ClientType;
import edu.ues.aps.process.base.repository.ClientPersistenceRepository;
import edu.ues.aps.process.base.repository.ClientValidatorRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class ClientService implements ClientPersistenceService, SingleClientInformationService, ClientValidatorService {

    private final OwnerClientAdderService adderService;
    private final ClientValidatorRepository validatorRepository;
    private final ClientPersistenceRepository persistenceRepository;

    private AbstractClient client;

    public ClientService(OwnerClientAdderService adderService, ClientPersistenceRepository persistenceRepository, ClientValidatorRepository validatorRepository) {
        this.adderService = adderService;
        this.persistenceRepository = persistenceRepository;
        this.validatorRepository = validatorRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractClientDTO find(Long id_client) {
        try {
            return toDTO(persistenceRepository.find(id_client));
        } catch (Exception e) {
            return new EmptyClientDTO();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractClientDTO find(Long id_owner, Long id_client, ClientType type) {
        try {
            return toDTO(persistenceRepository.find(id_owner, id_client, type));
        } catch (Exception e) {
            return new EmptyClientDTO();
        }
    }

    @Override
    @Transactional
    public void save(Long id_owner, AbstractClientDTO dto) {
        client = new Client();
        setClient(dto);
        persistenceRepository.save(client);
        adderService.add(client, dto.getClientType(), id_owner);
    }

    @Override
    @Transactional
    public void update(AbstractClientDTO dto) {
        client = persistenceRepository.find(dto.getId_client());
        setClient(dto);
    }

    @Override
    @Transactional
    public void update(AbstractClientDTO dto, ClientType type) {
        client = persistenceRepository.find(dto.getId_owner(), dto.getId_client(), type);
        if (!dto.getClientType().equals(type)) {
            persistenceRepository.updateType(dto.getId_owner(), dto.getId_client(), type, dto.getClientType());
        }
        setClient(dto);
    }

    @Override
    @Transactional
    public void delete(Long id_client) {
        try {
            persistenceRepository.delete(persistenceRepository.find(id_client));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional
    public void delete(Long id_owner, Long id_client, ClientType type) {
        try {
            persistenceRepository.delete(id_owner, id_client, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public String getClientName(Long id_client) {
        return persistenceRepository.find(id_client).getName();
    }

    @Override
    @Transactional(readOnly = true)
    public String getClientPhoneNumber(Long id_client) {
        return persistenceRepository.find(id_client).getTelephone();
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueDUI(Long id_client, String dui) {
        if (validatorRepository.isDUIUnique(id_client, dui) == 1) {
            return true;
        } else {
            return validatorRepository.isDUIUnique(dui) == 0;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniquePassport(Long id_client, String passport) {
        if (validatorRepository.isPassportUnique(id_client, passport) == 1) {
            return true;
        } else {
            return validatorRepository.isPassportUnique(passport) == 0;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueResidentCard(Long id_client, String rc) {
        if (validatorRepository.isResidentCard(id_client, rc) == 1) {
            return true;
        } else {
            return validatorRepository.isResidentCard(rc) == 0;
        }
    }

    private void setClient(AbstractClientDTO dto) {
        client.setName(dto.getClient_name());
        client.setDUI(dto.getClient_DUI());
        client.setEmail(dto.getClient_email());
        client.setNationally(dto.getClient_nationally());
        client.setPassport(dto.getClient_passport());
        client.setResident_card(dto.getClient_resident_card());
        client.setTelephone(dto.getClient_telephone());
    }

    private AbstractClientDTO toDTO(AbstractClient client) {
        AbstractClientDTO dto = new ClientDTO();
        dto.setId_client(client.getId());
        dto.setClient_name(client.getName());
        dto.setClient_DUI(client.getDUI());
        dto.setClient_email(client.getEmail());
        dto.setClient_nationally(client.getNationally());
        dto.setClient_passport(client.getPassport());
        dto.setClient_resident_card(client.getResident_card());
        dto.setClient_telephone(client.getTelephone());
        if (!client.getOwners().isEmpty()) {
            dto.setType(client.getOwners().get(0).getId().getType());
        }
        return dto;
    }


}
