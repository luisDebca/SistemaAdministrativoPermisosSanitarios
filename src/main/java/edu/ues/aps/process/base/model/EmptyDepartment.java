package edu.ues.aps.process.base.model;

import java.util.Collections;
import java.util.List;

public class EmptyDepartment implements AbstractDepartment {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getName() {
        return "Department name not found";
    }

    @Override
    public List<Municipality> getMunicipalities() {
        return Collections.emptyList();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setName(String name) {

    }

    @Override
    public void setMunicipalities(List<Municipality> municipalities) {

    }

    @Override
    public String toString() {
        return "EmptyDepartment{}";
    }
}
