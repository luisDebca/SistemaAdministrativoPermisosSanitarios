package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.AbstractLegalEntity;
import edu.ues.aps.process.base.model.AbstractNaturalEntity;

public interface SingleOwnerEntityFinderService {

    AbstractNaturalEntity findNaturalEntity(Long id_owner);

    AbstractLegalEntity findLegalEntity(Long id_owner);
}
