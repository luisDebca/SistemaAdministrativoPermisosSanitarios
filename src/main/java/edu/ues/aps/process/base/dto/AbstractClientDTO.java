package edu.ues.aps.process.base.dto;

import edu.ues.aps.process.base.model.ClientType;

public interface AbstractClientDTO {

    Long getId_owner();

    Long getId_client();

    String getType();

    ClientType getClientType();

    String getClient_name();

    String getClient_nationally();

    String getClient_DUI();

    String getClient_passport();

    String getClient_resident_card();

    String getClient_email();

    String getClient_telephone();

    void setId_client(Long id_client);

    void setType(ClientType type);

    void setClient_name(String client_name);

    void setClient_nationally(String client_nationally);

    void setClient_DUI(String client_DUI);

    void setClient_passport(String client_passport);

    void setClient_resident_card(String client_resident_card);

    void setClient_email(String client_email);

    void setClient_telephone(String client_telephone);

    void setId_owner(Long id_owner);

    void setClientType(ClientType clientType);
}
