package edu.ues.aps.process.base.model;

import edu.ues.aps.process.legal.base.model.LegalReview;
import edu.ues.aps.process.payment.base.model.AbstractPaymentProcedure;
import edu.ues.aps.process.payment.base.model.EmptyPaymentProcedure;
import edu.ues.aps.process.payment.base.model.PaymentProcedure;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.model.Process;
import edu.ues.aps.process.requirement.base.model.AbstractRequirementControlDocument;
import edu.ues.aps.process.requirement.base.model.EmptyRequirementControlDocument;
import edu.ues.aps.process.requirement.base.model.RequirementControlDocument;
import edu.ues.aps.process.resolution.base.model.AbstractResolution;
import edu.ues.aps.process.resolution.base.model.EmptyResolution;
import edu.ues.aps.process.resolution.base.model.Memorandum;
import edu.ues.aps.process.resolution.base.model.Resolution;
import edu.ues.aps.process.tracing.base.model.AbstractTracing;
import edu.ues.aps.process.tracing.base.model.EmptyTracing;
import edu.ues.aps.process.tracing.base.model.Tracing;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class EmptyCasefile implements AbstractCasefile {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public Boolean getApply_payment() {
        return false;
    }

    @Override
    public void setApply_payment(Boolean apply_payment) {

    }

    @Override
    public CertificationType getCertification_type() {
        return CertificationType.INVALID;
    }

    @Override
    public void setCertification_type(CertificationType certification_type) {

    }

    @Override
    public Integer getCertification_duration_in_years() {
        return 0;
    }

    @Override
    public void setCertification_duration_in_years(Integer certification_duration_in_years) {

    }

    @Override
    public LocalDateTime getCertification_start_on() {
        return LocalDateTime.of(2000, 1, 1, 0, 0);
    }

    @Override
    public void setCertification_start_on(LocalDateTime certification_start_on) {

    }

    @Override
    public List<Memorandum> getMemorandums() {
        return Collections.emptyList();
    }

    @Override
    public void setMemorandums(List<Memorandum> memorandums) {

    }

    @Override
    public List<CasefileProcess> getProcesses() {
        return Collections.emptyList();
    }

    @Override
    public void setProcesses(List<CasefileProcess> processes) {

    }

    @Override
    public String getRequest_number() {
        return "Casefile request number not found";
    }

    @Override
    public void setRequest_number(String case_file_number) {

    }

    @Override
    public String getRequest_code() {
        return "Casefile request code not found";
    }

    @Override
    public void setRequest_code(String request_code) {

    }

    @Override
    public String getRequest_year() {
        return "Casefile request year not found";
    }

    @Override
    public void setRequest_year(String request_year) {

    }

    @Override
    public AbstractUcsf getUcsf() {
        return new EmptyUcsf();
    }

    @Override
    public void setUcsf(Ucsf ucsf) {

    }

    @Override
    public AbstractTracing getTracing() {
        return new EmptyTracing();
    }

    @Override
    public void setTracing(Tracing tracing) {

    }

    @Override
    public List<LegalReview> getLegalReviewls() {
        return Collections.emptyList();
    }

    @Override
    public void setLegalReviewls(List<LegalReview> legalReviewls) {

    }

    @Override
    public AbstractPaymentProcedure getPaymentProcedure() {
        return new EmptyPaymentProcedure();
    }

    @Override
    public void setPaymentProcedure(PaymentProcedure paymentProcedure) {

    }

    @Override
    public AbstractRequirementControlDocument getControlDocument() {
        return new EmptyRequirementControlDocument();
    }

    @Override
    public void setControlDocument(RequirementControlDocument controlDocument) {

    }

    @Override
    public void addProcessRecord(Process process) {

    }

    @Override
    public void addControlDocument(RequirementControlDocument document) {

    }

    @Override
    public void removeControlDocument() {

    }

    @Override
    public AbstractResolution getResolution() {
        return new EmptyResolution();
    }

    @Override
    public void setResolution(Resolution resolution) {

    }

    @Override
    public void addResolution(Resolution resolution) {

    }

    @Override
    public void addLegalReview(LegalReview legalReview) {

    }

    @Override
    public void removeLegalReview(LegalReview legalReview) {

    }

    @Override
    public Integer getCertificationDurationInYears() {
        return 0;
    }

    @Override
    public void addPaymentProcedure(PaymentProcedure procedure) {

    }

    @Override
    public void removePaymentProcedure() {

    }

    @Override
    public CasefileSection getSection() {
        return CasefileSection.INVALID;
    }

    @Override
    public void setSection(CasefileSection section) {

    }

    @Override
    public void addTracing(Tracing tracing) {

    }

    @Override
    public CasefileStatusType getStatus() {
        return CasefileStatusType.INVALID;
    }

    @Override
    public void setStatus(CasefileStatusType status) {

    }

    @Override
    public LocalDateTime getCreation_date() {
        return LocalDateTime.of(2000, 1, 1, 0, 0);
    }

    @Override
    public void setCreation_date(LocalDateTime creation_date) {

    }

    @Override
    public String getRequest_folder() {
        return "Case File folder not found";
    }

    @Override
    public void setRequest_folder(String request_folder) {

    }

    @Override
    public void addProcessRecord(CasefileProcess record) {

    }

    @Override
    public void addMemorandum(Memorandum memo) {

    }

    @Override
    public void removeMemorandum(Memorandum memo) {

    }

    @Override
    public Boolean getValidate() {
        return false;
    }

    @Override
    public void setValidate(Boolean validate) {

    }

    @Override
    public Boolean getUcsf_delivery() {
        return false;
    }

    @Override
    public void setUcsf_delivery(Boolean ucsf_delivery) {

    }

    @Override
    public String toString() {
        return "Empty Case file{}";
    }
}
