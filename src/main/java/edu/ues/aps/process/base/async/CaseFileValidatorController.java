package edu.ues.aps.process.base.async;

import edu.ues.aps.process.base.service.CaseFileValidatorService;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.Locale;

@Controller
@RequestMapping("/caseFile/validator")
public class CaseFileValidatorController {

    private final MessageSource messageSource;
    private final CaseFileValidatorService validatorService;

    public CaseFileValidatorController(CaseFileValidatorService validatorService, MessageSource messageSource) {
        this.validatorService = validatorService;
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/folder_number", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateFolderNumber(@RequestParam Long id_caseFile, @RequestParam String number, @RequestParam String year) {
        return validatorService.isUniqueFolderNumber(id_caseFile, number, year) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("case-file.folder", new String[]{String.format("%s-%s", number, year)}, Locale.getDefault()));
    }

    @GetMapping(value = "/request_number", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateRequestNumber(@RequestParam Long id_caseFile, @RequestParam String code, @RequestParam String number, @RequestParam String year) {
        return validatorService.isUniqueRequestNumber(id_caseFile, code, number, year) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("case-file.request", new String[]{String.format("06%s%s-%s", code, number, year)}, Locale.getDefault()));
    }
}
