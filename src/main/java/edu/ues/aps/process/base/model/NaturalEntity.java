package edu.ues.aps.process.base.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Table;

@Entity
@Table(name = "NATURAL_ENTITY")
public class NaturalEntity extends OwnerEntity implements AbstractNaturalEntity {

    @Column(name = "DUI", unique = true, length = 10)
    private String DUI;

    @Column(name = "nationally", length = 50)
    private String nationally;

    @Column(name = "passport", unique = true, length = 15)
    private String passport;

    @Column(name = "resident_card", unique = true, length = 15)
    private String residentCard;

    @Override
    public String getDUI() {
        return DUI;
    }

    @Override
    public void setDUI(String DUI) {
        if (DUI == null) {
            this.DUI = null;
        } else {
            if (DUI.isEmpty()) {
                this.DUI = null;
            } else {
                this.DUI = DUI;
            }
        }
    }

    @Override
    public String getNationally() {
        return nationally;
    }

    @Override
    public void setNationally(String nationally) {
        this.nationally = nationally;
    }

    @Override
    public String getPassport() {
        return passport;
    }

    @Override
    public void setPassport(String passport) {
        if (passport == null) {
            this.passport = null;
        } else {
            if (passport.isEmpty()) {
                this.passport = null;
            } else {
                this.passport = passport;
            }
        }
    }

    @Override
    public String getResidentCard() {
        return residentCard;
    }

    @Override
    public void setResidentCard(String residentCard) {
        if (residentCard == null) {
            this.residentCard = null;
        } else {
            if (residentCard.isEmpty()) {
                this.residentCard = null;
            } else {
                this.residentCard = residentCard;
            }
        }
    }

    @Override
    public String toString() {
        return "NaturalEntity{" +
                "DUI='" + DUI + '\'' +
                ", nationally='" + nationally + '\'' +
                ", passport='" + passport + '\'' +
                ", residentCard='" + residentCard + '\'' +
                "} " + super.toString();
    }
}
