package edu.ues.aps.process.base.controller;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import edu.ues.aps.process.base.service.SibasiDTOFinderService;
import edu.ues.aps.process.base.service.UcsfDTOFinderService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@ControllerAdvice
@RequestMapping({"/caseFile/*/edit", "/establishment/*/casefile/outdated/new"})
public class SibasiUcsfInformationProviderController implements InitializingBean {

    private List<AbstractMapDTO> sibasi_list;
    private List<AbstractMapDTO> ucsf_list;
    private SibasiDTOFinderService sibasiDTOFinderService;
    private UcsfDTOFinderService ucsfDTOFinderService;

    public SibasiUcsfInformationProviderController(SibasiDTOFinderService sibasiDTOFinderService, UcsfDTOFinderService ucsfDTOFinderService) {
        this.sibasiDTOFinderService = sibasiDTOFinderService;
        this.ucsfDTOFinderService = ucsfDTOFinderService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        sibasi_list = sibasiDTOFinderService.findAll();
        ucsf_list = ucsfDTOFinderService.findAll();
    }

    @ModelAttribute("sibasi_list")
    public List<AbstractMapDTO> getSibasi_list() {
        return sibasi_list;
    }

    @ModelAttribute("ucsf_list")
    public List<AbstractMapDTO> getUcsf_list() {
        return ucsf_list;
    }
}
