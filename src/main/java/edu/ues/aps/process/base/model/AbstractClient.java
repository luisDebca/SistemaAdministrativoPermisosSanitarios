package edu.ues.aps.process.base.model;

import java.util.List;

public interface AbstractClient {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    String getNationally();

    void setNationally(String nationally);

    String getDUI();

    void setDUI(String DUI);

    String getPassport();

    void setPassport(String passport);

    String getResident_card();

    void setResident_card(String resident_card);

    String getEmail();

    void setEmail(String email);

    String getTelephone();

    void setTelephone(String telephone);

    List<OwnerClient> getOwners();

    void setOwners(List<OwnerClient> owners);
}
