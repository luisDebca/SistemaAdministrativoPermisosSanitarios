package edu.ues.aps.process.base.controller;

import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;
import edu.ues.aps.process.base.model.AbstractOwner;
import edu.ues.aps.process.base.model.LegalEntity;
import edu.ues.aps.process.base.model.NaturalEntity;
import edu.ues.aps.process.base.service.OwnerDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/owner")
public class OwnerInformationProviderController {

    private static final Logger logger = LogManager.getLogger(OwnerInformationProviderController.class);

    private static final String OWNER_INDEX = "owner_index";
    private static final String OWNER_LIST_TEMPLATE = "owner_list";
    private static final String LEGAL_OWNER_INFO = "legal_owner_information";
    private static final String NATURAL_OWNER_INFO = "natural_owner_information";

    private final OwnerDTOFinderService finderService;

    public OwnerInformationProviderController(OwnerDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping({"/", "/index"})
    public String showIndex() {
        logger.info("GET:: Show owner index");
        return OWNER_INDEX;
    }

    @GetMapping("/{id_owner}")
    public String showOwnerInfo(@PathVariable Long id_owner){
        logger.info("GET:: Show owner information");
        AbstractOwnerEntityDTO owner = finderService.findById(id_owner);
        if(owner.getClass_type().equals(NaturalEntity.class.getSimpleName())){
            return String.format("redirect:/owner/natural/%d", id_owner);
        }else if(owner.getClass_type().equals(LegalEntity.class.getSimpleName())){
            return String.format("redirect:/owner/legal/%d", id_owner);
        }else{
            throw  new OwnerTypeNotFoundException();
        }
    }

    @GetMapping("legal/{id_owner}")
    public String showLegalOwnerInfo(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show Legal Owner {} information", id_owner);
        model.addAttribute("owner", finderService.findLegalEntity(id_owner));
        return LEGAL_OWNER_INFO;
    }

    @GetMapping("natural/{id_owner}")
    public String showNaturalOwnerInfo(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show Natural Owner {} information", id_owner);
        model.addAttribute("owner", finderService.findNaturalEntity(id_owner));
        return NATURAL_OWNER_INFO;
    }

    @GetMapping("/list")
    public String showAllOwners(ModelMap model) {
        logger.info("GET:: Show all owner list");
        model.addAttribute("owners", finderService.findAllOwners());
        return OWNER_LIST_TEMPLATE;
    }
}
