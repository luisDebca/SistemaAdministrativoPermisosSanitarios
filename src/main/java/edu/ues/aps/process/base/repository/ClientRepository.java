package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractClient;
import edu.ues.aps.process.base.model.Client;
import edu.ues.aps.process.base.model.ClientType;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ClientRepository implements ClientPersistenceRepository, ClientValidatorRepository {

    private final SessionFactory sessionFactory;

    public ClientRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractClient find(Long id_client) {
        return sessionFactory.getCurrentSession().get(Client.class, id_client);
    }

    @Override
    public AbstractClient find(Long id_owner, Long id_client, ClientType type) {
        return sessionFactory.getCurrentSession().createQuery("select client " +
                "from Client client " +
                "join fetch client.owners ownerclients " +
                "join ownerclients.owner owner " +
                "where client.id = :id_client " +
                "and ownerclients.id.type = :ctype " +
                "and owner.id = :id_owner", AbstractClient.class)
                .setParameter("ctype", type)
                .setParameter("id_owner", id_owner)
                .setParameter("id_client", id_client)
                .getSingleResult();
    }

    @Override
    public void save(AbstractClient client) {
        sessionFactory.getCurrentSession().persist(client);
    }

    @Override
    public void delete(AbstractClient client) {
        sessionFactory.getCurrentSession().delete(client);
    }

    @Override
    public void delete(Long id_owner, Long id_client, ClientType type) {
        sessionFactory.getCurrentSession().createQuery("delete from OwnerClient oc " +
                "where oc.id.owner_id = :owner_id " +
                "and oc.id.client_id = :client_id " +
                "and oc.id.type = :ctype")
                .setParameter("owner_id", id_owner)
                .setParameter("client_id", id_client)
                .setParameter("ctype", type)
                .executeUpdate();
    }

    @Override
    public void updateType(Long id_owner, Long id_client, ClientType old_type, ClientType new_type) {
        sessionFactory.getCurrentSession().createQuery("update OwnerClient oc " +
                "set oc.id.type = :new_type " +
                "where oc.id.client_id = :client_id " +
                "and oc.id.owner_id = :owner_id " +
                "and oc.id.type = :old_type")
                .setParameter("new_type", new_type)
                .setParameter("old_type", old_type)
                .setParameter("client_id", id_client)
                .setParameter("owner_id", id_owner)
                .executeUpdate();
    }

    @Override
    public Long isDUIUnique(Long id_client, String dui) {
        return sessionFactory.getCurrentSession().createQuery("select count(client) " +
                "from Client client " +
                "where client.id = :id " +
                "and client.DUI = :dui", Long.class)
                .setParameter("id", id_client)
                .setParameter("dui", dui)
                .getSingleResult();
    }

    @Override
    public Long isPassportUnique(Long id_client, String passport) {
        return sessionFactory.getCurrentSession().createQuery("select count(client) " +
                "from Client client " +
                "where client.id = :id " +
                "and client.passport = :passport", Long.class)
                .setParameter("id", id_client)
                .setParameter("passport", passport)
                .getSingleResult();
    }

    @Override
    public Long isResidentCard(Long id_client, String rc) {
        return sessionFactory.getCurrentSession().createQuery("select count(client) " +
                "from Client client " +
                "where client.id = :id " +
                "and client.resident_card = :rc", Long.class)
                .setParameter("id", id_client)
                .setParameter("rc", rc)
                .getSingleResult();
    }

    @Override
    public Long isDUIUnique(String dui) {
        return sessionFactory.getCurrentSession().createQuery("select count(client) " +
                "from Client client " +
                "where client.DUI = :dui", Long.class)
                .setParameter("dui", dui)
                .getSingleResult();
    }

    @Override
    public Long isPassportUnique(String passport) {
        return sessionFactory.getCurrentSession().createQuery("select count(client) " +
                "from Client client " +
                "where client.passport = :passport", Long.class)
                .setParameter("passport", passport)
                .getSingleResult();
    }

    @Override
    public Long isResidentCard(String rc) {
        return sessionFactory.getCurrentSession().createQuery("select count(client) " +
                "from Client client " +
                "where client.resident_card = :rc", Long.class)
                .setParameter("rc", rc)
                .getSingleResult();
    }
}
