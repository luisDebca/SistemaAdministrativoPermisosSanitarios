package edu.ues.aps.process.base.model;

public interface AbstractAddress {

    Long getId();

    void setId(Long id);

    String getDetails();

    void setDetails(String details);

    AbstractMunicipality getMunicipality();

    void setMunicipality(Municipality municipality);
}
