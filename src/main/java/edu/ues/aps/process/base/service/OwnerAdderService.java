package edu.ues.aps.process.base.service;

import edu.ues.aps.process.area.establishment.model.AbstractEstablishment;
import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.area.vehicle.model.AbstractVehicle;
import edu.ues.aps.process.area.vehicle.model.Vehicle;
import edu.ues.aps.process.base.model.AbstractClient;
import edu.ues.aps.process.base.model.AbstractOwner;
import edu.ues.aps.process.base.model.Client;
import edu.ues.aps.process.base.model.ClientType;
import edu.ues.aps.process.base.repository.OwnerEntityPersistenceRepository;
import edu.ues.aps.process.base.repository.OwnerFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class OwnerAdderService implements OwnerEstablishmentAdderService, OwnerVehicleAdderService, OwnerClientAdderService {

    private final OwnerEntityPersistenceRepository persistenceRepository;
    private final List<OwnerFinderRepository> finderRepositories;

    private AbstractOwner owner;

    public OwnerAdderService(OwnerEntityPersistenceRepository persistenceRepository, List<OwnerFinderRepository> finderRepositories) {
        this.persistenceRepository = persistenceRepository;
        this.finderRepositories = finderRepositories;
    }

    @Override
    @Transactional
    public void add(AbstractEstablishment establishment, Long id_owner) {
        setOwnerFromRepositories(id_owner);
        if(owner.getNit() != null) establishment.setNIT(owner.getNit());
        owner.addEstablishment((Establishment) establishment);
        persistenceRepository.merge(owner);
    }

    @Override
    @Transactional
    public void add(List<AbstractVehicle> vehicles, Long id_owner) {
        setOwnerFromRepositories(id_owner);
        for (AbstractVehicle vehicle : vehicles) {
            owner.addVehicle((Vehicle) vehicle);
        }
        persistenceRepository.merge(owner);
    }

    @Override
    @Transactional
    public void add(AbstractVehicle vehicle, Long id_owner) {
        setOwnerFromRepositories(id_owner);
        owner.addVehicle((Vehicle) vehicle);
        persistenceRepository.merge(owner);
    }

    @Override
    @Transactional
    public void add(AbstractClient client, ClientType type, Long id_owner) {
        setOwnerFromRepositories(id_owner);
        owner.addClient((Client) client, type);
        persistenceRepository.update(owner);
    }

    private void setOwnerFromRepositories(Long id_owner) {
        for (OwnerFinderRepository repository : finderRepositories) {
            AbstractOwner entity = repository.findOwnerById(id_owner);
            if (entity.getId() != 0)
                owner = entity;
        }
    }
}
