package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractLegalEntityDTO;
import edu.ues.aps.process.base.dto.AbstractNaturalEntityDTO;
import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface OwnerDTOFinderRepository {

    List<AbstractOwnerEntityDTO> findAllOwners();

    AbstractOwnerEntityDTO findById(Long id_owner) throws NoResultException;

    List<AbstractOwnerEntityDTO> findOwnerByIdentifier(String parameter);

    AbstractLegalEntityDTO findLegalEntityById(Long id_owner) throws NoResultException;

    AbstractNaturalEntityDTO findNaturalEntityById(Long id_owner) throws NoResultException;
}
