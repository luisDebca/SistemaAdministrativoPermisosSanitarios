package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractLegalEntityDTO;
import edu.ues.aps.process.base.dto.AbstractNaturalEntityDTO;
import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;
import edu.ues.aps.process.base.dto.OwnerEntityDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class OwnerDTORepository implements OwnerDTOFinderRepository {

    private SessionFactory sessionFactory;

    public OwnerDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractLegalEntityDTO findLegalEntityById(Long id_owner) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.LegalEntityDTO(entity.id,entity.name,entity.nit,entity.email,entity.telephone,entity.FAX," +
                "concat(concat(concat(concat(address.details,', '),municipality.name),', '),department.name)," +
                "(select count(estbl) from Establishment estbl where estbl.owner.id = entity.id)," +
                "(select count(vehicle) from Vehicle vehicle where vehicle.owner.id = entity.id)," +
                "(select count(product) from Product product where product.legalEntity.id = entity.id)," +
                "(select count(client) from Client client inner join client.owners oc inner join oc.owner owner where owner.id = entity.id group by owner.id)) " +
                "from LegalEntity entity " +
                "left join entity.address address " +
                "left join address.municipality municipality " +
                "left join municipality.department department " +
                "where entity.id = :id", AbstractLegalEntityDTO.class)
                .setParameter("id", id_owner)
                .getSingleResult();
    }

    @Override
    public AbstractNaturalEntityDTO findNaturalEntityById(Long id_owner) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.NaturalEntityDTO(entity.id,entity.name,entity.nit,entity.email," +
                "entity.telephone,entity.FAX,entity.DUI,entity.nationally,entity.passport,entity.residentCard," +
                "(select count(estbl) from Establishment estbl where estbl.owner.id = entity.id)," +
                "(select count(vehicle) from Vehicle vehicle where vehicle.owner.id = entity.id)," +
                "(select count(client) from Client client inner join client.owners oc inner join oc.owner owner where owner.id = entity.id group by owner.id)) " +
                "from NaturalEntity entity " +
                "where entity.id = :id", AbstractNaturalEntityDTO.class)
                .setParameter("id", id_owner)
                .getSingleResult();
    }

    @Override
    public AbstractOwnerEntityDTO findById(Long id_owner) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.OwnerEntityDTO(owner.id,owner.name,owner.nit,owner.email,owner.telephone,owner.FAX,type(owner)," +
                "(select count(estbl) from Establishment estbl where estbl.owner.id = owner.id)," +
                "(select count(vehicle) from Vehicle vehicle where vehicle.owner.id = owner.id))" +
                "from OwnerEntity owner " +
                "where owner.id = :id ", OwnerEntityDTO.class)
                .setParameter("id", id_owner)
                .getSingleResult();
    }

    @Override
    public List<AbstractOwnerEntityDTO> findAllOwners() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.OwnerEntityDTO( " +
                "owner.id,owner.name,owner.nit,owner.email,owner.telephone,owner.FAX,type(owner)," +
                "(select count(estbl) from Establishment estbl where estbl.owner.id = owner.id)," +
                "(select count(vehicle) from Vehicle vehicle where vehicle.owner.id = owner.id))" +
                "from OwnerEntity owner ", AbstractOwnerEntityDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractOwnerEntityDTO> findOwnerByIdentifier(String parameter) {
        List<AbstractOwnerEntityDTO> result = sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.OwnerEntityDTO(" +
                "owner.id,owner.name,owner.nit,owner.email,owner.telephone,owner.FAX,type(owner)) " +
                "from OwnerEntity owner " +
                "where owner.name like concat(concat('%',:param),'%') " +
                "or owner.nit like concat(concat('%',:param),'%') ", AbstractOwnerEntityDTO.class)
                .setParameter("param", parameter)
                .getResultList();
        result.addAll(sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.OwnerEntityDTO(owner.id,owner.name,owner.nit,owner.email,owner.telephone,owner.FAX,type(owner)) " +
                "from LegalEntity owner inner join owner.address address where address.details like concat(concat('%',:param),'%')", AbstractOwnerEntityDTO.class)
                .setParameter("param", parameter).getResultList());
        return result;
    }
}
