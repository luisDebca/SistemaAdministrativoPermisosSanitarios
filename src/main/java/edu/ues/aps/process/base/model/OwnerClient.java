package edu.ues.aps.process.base.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Objects;

@Entity
@Table(name="OWNER_CLIENT")
public class OwnerClient {

    @EmbeddedId
    private OwnerClientId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("owner_id")
    private OwnerEntity owner;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("client_id")
    private Client client;

    public OwnerClient() {
    }

    public OwnerClient(OwnerEntity owner, Client client, ClientType type) {
        this.owner = owner;
        this.client = client;
        this.id = new OwnerClientId(owner.getId(),client.getId(),type);
    }

    public OwnerClientId getId() {
        return id;
    }

    public void setId(OwnerClientId id) {
        this.id = id;
    }

    public OwnerEntity getOwner() {
        return owner;
    }

    public void setOwner(OwnerEntity owner) {
        this.owner = owner;
    }

    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        OwnerClient that = (OwnerClient) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "OwnerClient{" +
                '}';
    }

    @Embeddable
    public static class OwnerClientId implements Serializable {

        @Column(name = "id_owner")
        private Long owner_id;

        @Column(name = "id_client")
        private Long client_id;

        @Enumerated(EnumType.STRING)
        @Column(name = "client_type", nullable = false, length = 50)
        private ClientType type;

        public OwnerClientId() {
        }

        public OwnerClientId(Long owner_id, Long client_id, ClientType type) {
            this.owner_id = owner_id;
            this.client_id = client_id;
            this.type = type;
        }

        public Long getOwner_id() {
            return owner_id;
        }

        public Long getClient_id() {
            return client_id;
        }

        public ClientType getType() {
            return type;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            OwnerClientId that = (OwnerClientId) o;
            return Objects.equals(owner_id, that.owner_id) &&
                    Objects.equals(client_id, that.client_id) &&
                    type == that.type;
        }

        @Override
        public int hashCode() {
            return Objects.hash(owner_id, client_id, type);
        }
    }
}
