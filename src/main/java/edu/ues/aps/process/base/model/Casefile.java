package edu.ues.aps.process.base.model;

import edu.ues.aps.process.legal.base.model.LegalReview;
import edu.ues.aps.process.payment.base.model.PaymentProcedure;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.model.Process;
import edu.ues.aps.process.requirement.base.model.RequirementControlDocument;
import edu.ues.aps.process.resolution.base.model.Memorandum;
import edu.ues.aps.process.resolution.base.model.Resolution;
import edu.ues.aps.process.tracing.base.model.Tracing;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "CASEFILE")
@Inheritance(strategy = InheritanceType.JOINED)
public class Casefile implements AbstractCasefile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "request_number", length = 9)
    private String request_number;

    @Column(name = "request_code", length = 5)
    private String request_code;

    @Column(name = "request_year", length = 4)
    private String request_year;

    @Column(name = "request_folder", length = 9)
    private String request_folder;

    @Column(name = "apply_payment", nullable = false)
    private Boolean apply_payment;

    @Column(name = "validate")
    private Boolean validate;

    @Column(name = "ucsf_delivery")
    private Boolean ucsf_delivery;

    @Enumerated(EnumType.STRING)
    @Column(name = "certification_type", nullable = false, length = 10)
    private CertificationType certification_type;

    @Enumerated(EnumType.STRING)
    @Column(name = "section", nullable = false, length = 14)
    private CasefileSection section;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", length = 12, nullable = false)
    private CasefileStatusType status;

    @Column(name = "certification_duration_in_years", nullable = false)
    private Integer certification_duration_in_years;

    @Column(name = "certification_start_on")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime certification_start_on;

    @Column(name = "creation_date", nullable = false)
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime creation_date;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_ucsf")
    private Ucsf ucsf;

    @OneToMany(mappedBy = "casefile", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CasefileProcess> processes = new ArrayList<>();

    @OneToOne(mappedBy = "casefile", cascade = CascadeType.ALL, orphanRemoval = true)
    private RequirementControlDocument controlDocument;

    @OneToMany(mappedBy = "casefile", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<LegalReview> legalReviewls = new ArrayList<>();

    @OneToMany(mappedBy = "casefile", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Memorandum> memorandums = new ArrayList<>();

    @OneToOne(mappedBy = "casefile", cascade = CascadeType.ALL, orphanRemoval = true)
    private PaymentProcedure paymentProcedure;

    @OneToOne(mappedBy = "casefile", cascade = CascadeType.ALL, orphanRemoval = true)
    private Tracing tracing;

    @OneToOne(mappedBy = "casefile", cascade = CascadeType.ALL, orphanRemoval = true)
    private Resolution resolution;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getRequest_number() {
        return request_number;
    }

    @Override
    public void setRequest_number(String case_file_number) {
        this.request_number = case_file_number;
    }

    public Boolean getValidate() {
        return validate;
    }

    public void setValidate(Boolean validate) {
        this.validate = validate;
    }

    @Override
    public Boolean getApply_payment() {
        return apply_payment;
    }

    @Override
    public void setApply_payment(Boolean apply_payment) {
        this.apply_payment = apply_payment;
    }

    @Override
    public CertificationType getCertification_type() {
        return certification_type;
    }

    @Override
    public void setCertification_type(CertificationType certification_type) {
        this.certification_type = certification_type;
    }

    @Override
    public CasefileSection getSection() {
        return section;
    }

    @Override
    public void setSection(CasefileSection section) {
        this.section = section;
    }

    @Override
    public Integer getCertification_duration_in_years() {
        return certification_duration_in_years;
    }

    @Override
    public void setCertification_duration_in_years(Integer certification_duration_in_years) {
        this.certification_duration_in_years = certification_duration_in_years;
    }

    @Override
    public LocalDateTime getCertification_start_on() {
        return certification_start_on;
    }

    @Override
    public void setCertification_start_on(LocalDateTime certification_start_on) {
        this.certification_start_on = certification_start_on;
    }

    @Override
    public Boolean getUcsf_delivery() {
        return ucsf_delivery;
    }

    @Override
    public void setUcsf_delivery(Boolean ucsf_delivery) {
        this.ucsf_delivery = ucsf_delivery;
    }

    @Override
    public Ucsf getUcsf() {
        return ucsf;
    }

    @Override
    public void setUcsf(Ucsf ucsf) {
        this.ucsf = ucsf;
    }

    @Override
    public String getRequest_folder() {
        return request_folder;
    }

    @Override
    public void setRequest_folder(String request_folder) {
        this.request_folder = request_folder;
    }

    @Override
    public List<CasefileProcess> getProcesses() {
        return processes;
    }

    @Override
    public void setProcesses(List<CasefileProcess> processes) {
        this.processes = processes;
    }

    @Override
    public Tracing getTracing() {
        return tracing;
    }

    @Override
    public void setTracing(Tracing tracing) {
        this.tracing = tracing;
    }

    @Override
    public List<LegalReview> getLegalReviewls() {
        return legalReviewls;
    }

    @Override
    public void setLegalReviewls(List<LegalReview> legalReviewls) {
        this.legalReviewls = legalReviewls;
    }

    @Override
    public void addLegalReview(LegalReview legalReview) {
        legalReviewls.add(legalReview);
        legalReview.setCasefile(this);
    }

    @Override
    public void removeLegalReview(LegalReview legalReview) {
        legalReviewls.remove(legalReview);
        legalReview.setCasefile(null);
    }

    @Override
    public PaymentProcedure getPaymentProcedure() {
        return paymentProcedure;
    }

    @Override
    public void setPaymentProcedure(PaymentProcedure paymentProcedure) {
        this.paymentProcedure = paymentProcedure;
    }

    @Override
    public List<Memorandum> getMemorandums() {
        return memorandums;
    }

    @Override
    public void setMemorandums(List<Memorandum> memorandums) {
        this.memorandums = memorandums;
    }

    @Override
    public RequirementControlDocument getControlDocument() {
        return controlDocument;
    }

    @Override
    public void setControlDocument(RequirementControlDocument controlDocument) {
        this.controlDocument = controlDocument;
    }

    @Override
    public void addControlDocument(RequirementControlDocument document) {
        this.controlDocument = document;
        document.setCasefile(this);
    }

    @Override
    public void removeControlDocument() {
        if (controlDocument != null) {
            controlDocument.setCasefile(null);
        }
        controlDocument = null;
    }

    @Override
    public String getRequest_code() {
        return request_code;
    }

    @Override
    public void setRequest_code(String request_code) {
        this.request_code = request_code;
    }

    @Override
    public String getRequest_year() {
        return request_year;
    }

    @Override
    public void setRequest_year(String request_year) {
        this.request_year = request_year;
    }

    @Override
    public void addProcessRecord(Process process) {
        CasefileProcess record = new CasefileProcess(this, process);
        record.setStart_on(LocalDateTime.now());
        if (!processes.isEmpty()) {
            processes.get(processes.size() - 1).setActive(false);
            processes.get(processes.size() - 1).setFinished_on(record.getStart_on().minusSeconds(1));
        }
        record.setActive(true);
        processes.add(record);
        process.getCasefiles().add(record);
    }

    @Override
    public void addProcessRecord(CasefileProcess record) {
        processes.add(record);
    }

    @Override
    public void addMemorandum(Memorandum memo) {
        memorandums.add(memo);
        memo.setCasefile(this);
    }

    @Override
    public void removeMemorandum(Memorandum memo) {
        memorandums.remove(memo);
        memo.setCasefile(null);
    }

    @Override
    public Resolution getResolution() {
        return resolution;
    }

    @Override
    public void setResolution(Resolution resolution) {
        this.resolution = resolution;
    }

    @Override
    public void addResolution(Resolution resolution) {
        this.resolution = resolution;
        resolution.setCasefile(this);
    }

    @Override
    public Integer getCertificationDurationInYears() {
        return 3;
    }

    @Override
    public void addPaymentProcedure(PaymentProcedure procedure) {
        procedure.setCasefile(this);
        this.paymentProcedure = procedure;
    }

    @Override
    public void removePaymentProcedure() {
        this.paymentProcedure = null;
    }

    @Override
    public void addTracing(Tracing tracing) {
        this.tracing = tracing;
        tracing.setCasefile(this);
    }

    @Override
    public CasefileStatusType getStatus() {
        return status;
    }

    @Override
    public void setStatus(CasefileStatusType status) {
        this.status = status;
    }

    @Override
    public LocalDateTime getCreation_date() {
        return creation_date;
    }

    @Override
    public void setCreation_date(LocalDateTime creation_date) {
        this.creation_date = creation_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Casefile casefile = (Casefile) o;
        return Objects.equals(id, casefile.id) &&
                Objects.equals(request_number, casefile.request_number) &&
                Objects.equals(request_code, casefile.request_code) &&
                Objects.equals(request_year, casefile.request_year);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, request_number, request_code, request_year);
    }

    @Override
    public String toString() {
        return "Casefile{" +
                "id=" + id +
                ", request_number='" + request_number + '\'' +
                ", request_code='" + request_code + '\'' +
                ", request_year='" + request_year + '\'' +
                ", request_folder='" + request_folder + '\'' +
                ", apply_payment=" + apply_payment +
                ", certification_type=" + certification_type +
                ", section=" + section +
                ", status=" + status +
                ", certification_duration_in_years=" + certification_duration_in_years +
                ", certification_start_on=" + certification_start_on +
                ", creation_date=" + creation_date +
                '}';
    }
}
