package edu.ues.aps.process.base.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "LEGAL_ENTITY")
public class LegalEntity extends OwnerEntity implements AbstractLegalEntity {

    @OneToMany(mappedBy = "legalEntity", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Product> products = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "LEGAL_ENTITY_ADDRESS",
            joinColumns = @JoinColumn(name = "id_entity", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_address", referencedColumnName = "id"))
    private Address address;

    @Override
    public List<Product> getProducts() {
        return products;
    }

    @Override
    public void setProducts(List<Product> products) {
        this.products = products;
    }

    @Override
    public void addProduct(Product product) {
        products.add(product);
        product.setLegalEntity(this);
    }

    @Override
    public void removeProduct(Product product) {
        product.setLegalEntity(null);
        this.products.remove(product);
    }

    @Override
    public Address getAddress() {
        return address;
    }

    @Override
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "LegalEntity{} " + super.toString();
    }
}
