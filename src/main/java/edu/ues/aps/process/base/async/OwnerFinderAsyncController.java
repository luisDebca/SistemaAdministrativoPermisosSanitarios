package edu.ues.aps.process.base.async;

import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;
import edu.ues.aps.process.base.service.OwnerFinderByParameterService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class OwnerFinderAsyncController {

    private final OwnerFinderByParameterService finderByParameterService;

    public OwnerFinderAsyncController(OwnerFinderByParameterService finderByParameterService) {
        this.finderByParameterService = finderByParameterService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/find-owner-by-identifier", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractOwnerEntityDTO> findOwnerByIdentifier(@RequestParam String param){
        return finderByParameterService.findOwnerBy(param);
    }

    @RequestMapping(method = RequestMethod.GET, value = {"/find-owner-by-id","/owner/find-by-id"}, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AbstractOwnerEntityDTO findOwnerById(@RequestParam Long id_owner){
        return finderByParameterService.findOwnerById(id_owner);
    }
}
