package edu.ues.aps.process.base.dto;

public class EmptyMapDTO implements AbstractMapDTO {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getValue() {
        return "No value found";
    }
}
