package edu.ues.aps.process.base.dto;

public interface AbstractMapDTO {

    Long getId();

    String getValue();
}
