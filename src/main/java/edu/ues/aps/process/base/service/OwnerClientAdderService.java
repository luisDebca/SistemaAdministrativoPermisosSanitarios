package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.AbstractClient;
import edu.ues.aps.process.base.model.ClientType;

public interface OwnerClientAdderService {

    void add(AbstractClient client, ClientType type, Long id_owner);
}
