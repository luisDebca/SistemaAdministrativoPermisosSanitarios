package edu.ues.aps.process.base.controller;

public class OwnerTypeNotFoundException extends RuntimeException{

    public OwnerTypeNotFoundException() {
    }

    public OwnerTypeNotFoundException(String message) {
        super(message);
    }
}
