package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class AddressRepository implements AddressFinderRepository {

    private SessionFactory sessionFactory;

    public AddressRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractMapDTO> findAllDepartments() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.MapDTO(department.id,department.name)" +
                "from Department department", AbstractMapDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractMapDTO> findAllMunicipalities() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.MapDTO(municipality.id,municipality.name) " +
                "from Municipality  municipality", AbstractMapDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractMapDTO> findAllMunicipalitiesFromDepartment(Long id_department) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.MapDTO(municipality.id,municipality.name) " +
                "from Municipality municipality " +
                "inner join municipality.department department " +
                "where department.id = :id", AbstractMapDTO.class)
                .setParameter("id", id_department)
                .getResultList();
    }

}
