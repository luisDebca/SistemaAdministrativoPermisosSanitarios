package edu.ues.aps.process.base.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "UCSF")
public class Ucsf implements AbstractUcsf {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, unique = true, length = 50)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_sibasi")
    private Sibasi sibasi;

    @OneToMany(mappedBy = "ucsf", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Casefile> casefiles = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public Sibasi getSibasi() {
        return sibasi;
    }

    @Override
    public void setSibasi(Sibasi sibasi) {
        this.sibasi = sibasi;
    }

    @Override
    public List<Casefile> getCasefiles() {
        return casefiles;
    }

    @Override
    public void setCasefiles(List<Casefile> casefiles) {
        this.casefiles = casefiles;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Ucsf ucsf = (Ucsf) o;

        return name.equals(ucsf.name);
    }

    @Override
    public int hashCode() {
        return name.hashCode();
    }

    @Override
    public String toString() {
        return "Ucsf{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
