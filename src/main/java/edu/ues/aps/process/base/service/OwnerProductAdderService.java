package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.Product;

import java.util.List;

public interface OwnerProductAdderService {

    void addProducts(List<Product> products, Long id_owner);
}
