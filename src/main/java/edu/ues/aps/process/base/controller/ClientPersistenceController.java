package edu.ues.aps.process.base.controller;

import edu.ues.aps.process.base.model.ClientType;
import edu.ues.aps.process.base.dto.ClientDTO;
import edu.ues.aps.process.base.service.ClientPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class ClientPersistenceController {

    private static final Logger logger = LogManager.getLogger(ClientPersistenceController.class);

    private static final String NEW_FORM = "client_new";
    private static final String EDIT_FORM = "client_edit";

    private final ClientPersistenceService persistenceService;

    public ClientPersistenceController(ClientPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping("/client/{id_client}/edit")
    public String showClientEdit(@PathVariable Long id_client, ModelMap model) {
        logger.info("GET:: Show client {} edit form.", id_client);
        model.addAttribute("client", persistenceService.find(id_client));
        return EDIT_FORM;
    }

    @PostMapping("/client/{id_client}/edit")
    public String updateClient(@ModelAttribute ClientDTO dto, @PathVariable Long id_client) {
        logger.info("POST:: Update client {}.", id_client);
        dto.setId_client(id_client);
        persistenceService.update(dto);
        return "redirect:/client/all";
    }

    @GetMapping("/owner/{id_owner}/client/new")
    public String showNewClient(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show new client form for owner {}.", id_owner);
        model.addAttribute("id_owner", id_owner);
        model.addAttribute("client", new ClientDTO());
        return NEW_FORM;
    }

    @PostMapping("/owner/{id_owner}/client/new")
    public String saveNewClient(@PathVariable Long id_owner, @ModelAttribute ClientDTO dto) {
        logger.info("POST:: Save new client for owner {}.", id_owner);
        dto.setId_owner(id_owner);
        persistenceService.save(id_owner, dto);
        return String.format("redirect:/owner/%d/client/all", id_owner);
    }

    @GetMapping("/owner/{id_owner}/client/{id_client}/type/{type}/edit")
    public String showClientEdit(@PathVariable Long id_owner, @PathVariable Long id_client, @PathVariable ClientType type, ModelMap model) {
        logger.info("GET:: Show client {} as {} edit form.", id_client, type.getClientType());
        model.addAttribute("id_owner", id_owner);
        model.addAttribute("client", persistenceService.find(id_owner, id_client, type));
        return EDIT_FORM;
    }

    @PostMapping("/owner/{id_owner}/client/{id_client}/type/{type}/edit")
    public String updateClient(@PathVariable Long id_owner, @PathVariable Long id_client, @PathVariable ClientType type, @ModelAttribute ClientDTO dto) {
        logger.info("POST:: Update client {} as {}.", id_client, type.getClientType());
        dto.setId_owner(id_owner);
        dto.setId_client(id_client);
        persistenceService.update(dto, type);
        return String.format("redirect:/owner/%d/client/all", id_owner);
    }

    @GetMapping("/client/{id_client}/delete")
    public String deleteClient(@PathVariable Long id_client) {
        logger.info("GET:: Delete client {}.", id_client);
        persistenceService.delete(id_client);
        return "redirect:/client/all";
    }

    @GetMapping("/owner/{id_owner}/client/{id_client}/type/{type}/remove")
    public String remove(@PathVariable Long id_owner, @PathVariable Long id_client, @PathVariable ClientType type) {
        logger.info("GET:: Remove client {} as {} from owner {} list", id_client, type, id_owner);
        persistenceService.delete(id_owner, id_client, type);
        return String.format("redirect:/owner/%d/client/all", id_owner);
    }

}
