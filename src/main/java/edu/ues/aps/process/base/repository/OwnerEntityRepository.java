package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractLegalEntity;
import edu.ues.aps.process.base.model.AbstractNaturalEntity;
import edu.ues.aps.process.base.model.AbstractOwner;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class OwnerEntityRepository implements OwnerEntityPersistenceRepository, OwnerEntityFinderRepository, OwnerValidatorRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public OwnerEntityRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractLegalEntity getLegalEntity(Long id_owner) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select owner " +
                "from LegalEntity owner " +
                "left join fetch owner.address address " +
                "join fetch address.municipality municipality " +
                "join fetch municipality.department dep " +
                "where owner.id = :id",AbstractLegalEntity.class)
                .setParameter("id",id_owner)
                .getSingleResult();
    }

    @Override
    public AbstractNaturalEntity getNaturalEntity(Long id_owner) throws NoResultException{
        return sessionFactory.getCurrentSession().createQuery("select owner " +
                "from NaturalEntity owner " +
                "where owner.id = :id",AbstractNaturalEntity.class)
                .setParameter("id",id_owner)
                .getSingleResult();
    }

    @Override
    public void save(AbstractOwner owner) {
        sessionFactory.getCurrentSession().persist(owner);
    }

    @Override
    public void merge(AbstractOwner owner) {
        sessionFactory.getCurrentSession().merge(owner);
    }

    @Override
    public void update(AbstractOwner owner) {
        sessionFactory.getCurrentSession().update(owner);
    }

    @Override
    public Long isUniqueName(String name) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from OwnerEntity owner " +
                "where owner.name = :name", Long.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public Long isUniqueNIT(String nit) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from OwnerEntity owner " +
                "where owner.nit = :nit", Long.class)
                .setParameter("nit", nit)
                .getSingleResult();
    }

    @Override
    public Long isUniqueDUI(String dui) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from NaturalEntity owner " +
                "where owner.DUI = :dui", Long.class)
                .setParameter("dui", dui)
                .getSingleResult();
    }

    @Override
    public Long isUniquePassport(String passport) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from NaturalEntity owner " +
                "where owner.passport = :passport", Long.class)
                .setParameter("passport", passport)
                .getSingleResult();
    }

    @Override
    public Long isUniqueResidentCard(String residentCard) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from NaturalEntity owner " +
                "where owner.residentCard = :card", Long.class)
                .setParameter("card", residentCard)
                .getSingleResult();
    }

    @Override
    public Long isUniqueName(Long id_owner, String name) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from OwnerEntity owner " +
                "where owner.id = :id " +
                "and owner.name = :name", Long.class)
                .setParameter("id", id_owner)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public Long isUniqueNIT(Long id_owner, String nit) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from OwnerEntity owner " +
                "where owner.id = :id " +
                "and owner.nit = :nit", Long.class)
                .setParameter("id", id_owner)
                .setParameter("nit", nit)
                .getSingleResult();
    }

    @Override
    public Long isUniqueDUI(Long id_owner, String dui) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from NaturalEntity owner " +
                "where owner.id = :id " +
                "and owner.DUI = :dui", Long.class)
                .setParameter("id", id_owner)
                .setParameter("dui", dui)
                .getSingleResult();
    }

    @Override
    public Long isUniquePassport(Long id_owner, String passport) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from NaturalEntity owner " +
                "where owner.id = :id " +
                "and owner.passport = :passport", Long.class)
                .setParameter("id", id_owner)
                .setParameter("passport", passport)
                .getSingleResult();
    }

    @Override
    public Long isUniqueResidentCard(Long id_owner, String residentCard) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from NaturalEntity owner " +
                "where owner.id = :id " +
                "and owner.residentCard = :card", Long.class)
                .setParameter("id", id_owner)
                .setParameter("card", residentCard)
                .getSingleResult();
    }
}
