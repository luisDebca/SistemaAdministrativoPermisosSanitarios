package edu.ues.aps.process.base.model;

public enum CasefileSection {

    ESTABLISHMENT("Establecimientos"),
    VEHICLE("VehÝculos"),
    TOBACCO("Tabaco"),
    INVALID("Invalido");

    private String casefileSection;

    CasefileSection(String casefileSection) {
        this.casefileSection = casefileSection;
    }

    public String getCasefileSection() {
        return casefileSection;
    }
}
