package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractClientDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface ClientDTOFinderRepository {

    AbstractClientDTO find(Long id_client) throws NoResultException;

    List<AbstractClientDTO> findAll();

    List<AbstractClientDTO> findAll(Long id_owner);
}
