package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.*;
import edu.ues.aps.process.base.repository.OwnerDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class OwnerDTOService implements OwnerDTOFinderService, OwnerFinderByParameterService {

    private final OwnerDTOFinderRepository finderRepository;

    public OwnerDTOService(OwnerDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractOwnerEntityDTO findById(Long id_owner) {
        try {
            return finderRepository.findById(id_owner);
        } catch (NoResultException e) {
            return new EmptyOwnerEntityDTO();
        }
    }

    @Override
    public List<AbstractOwnerEntityDTO> findAllOwners() {
        return finderRepository.findAllOwners();
    }

    @Override
    public AbstractOwnerEntityDTO findOwnerById(Long id_owner) {
        return finderRepository.findById(id_owner);
    }

    @Override
    public AbstractLegalEntityDTO findLegalEntity(Long id_owner) {
        try {
            return finderRepository.findLegalEntityById(id_owner);
        } catch (NoResultException e) {
            return new EmptyLegalEntityDTO();
        }
    }

    @Override
    public AbstractNaturalEntityDTO findNaturalEntity(Long id_owner) {
        try {
            return finderRepository.findNaturalEntityById(id_owner);
        } catch (NoResultException e) {
            return new EmptyNaturalEntityDTO();
        }
    }

    @Override
    public List<AbstractOwnerEntityDTO> findOwnerBy(String parameter) {
        return finderRepository.findOwnerByIdentifier(parameter);
    }
}
