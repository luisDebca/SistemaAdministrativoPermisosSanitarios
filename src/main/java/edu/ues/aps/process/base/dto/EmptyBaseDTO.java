package edu.ues.aps.process.base.dto;

public class EmptyBaseDTO implements AbstractBaseDTO {

    @Override
    public Long getId_caseFile() {
        return 0L;
    }

    @Override
    public Long getId_owner() {
        return 0L;
    }

    @Override
    public Long getId_target() {
        return 0L;
    }

    @Override
    public Long getId_group() {
        return 0L;
    }

    @Override
    public String getFolder() {
        return "Request folder not found";
    }

    @Override
    public String getRequest() {
        return "Request number not found";
    }

    @Override
    public String getResolution() {
        return "Resolution number not found";
    }

    @Override
    public String getOwner() {
        return "Owner not found";
    }

    @Override
    public String getTarget() {
        return "Target not found";
    }

    @Override
    public String getSection() {
        return "Section not found";
    }

    @Override
    public String getSectionType() {
        return "INVALID";
    }

    @Override
    public String getCertification() {
        return "Certification not found";
    }

    @Override
    public String getStatus() {
        return "Status not found";
    }

    @Override
    public void setId_caseFile(Long id_caseFile) {

    }

    @Override
    public void setId_owner(Long id_owner) {

    }

    @Override
    public void setId_target(Long id_target) {

    }

    @Override
    public void setOwner(String owner) {

    }

    @Override
    public void setTarget(String target) {

    }

    @Override
    public void setId_group(Long id_group) {

    }
}
