package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractLegalEntity;
import edu.ues.aps.process.base.model.AbstractNaturalEntity;
import edu.ues.aps.process.base.model.AbstractOwner;

import javax.persistence.NoResultException;

public interface OwnerCrudRepository {

    AbstractLegalEntity findLegalEntityById(Long id) throws NoResultException;

    void save(AbstractOwner owner);

    void update(AbstractOwner owner);
}
