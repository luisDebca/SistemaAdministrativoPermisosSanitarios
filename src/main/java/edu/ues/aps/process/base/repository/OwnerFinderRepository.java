package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractOwner;

import javax.persistence.NoResultException;

public interface OwnerFinderRepository {

    AbstractOwner findOwnerById(Long id_owner) throws NoResultException;

    AbstractOwner findOwnerByCasefileId(Long id_casefile) throws NoResultException;
}
