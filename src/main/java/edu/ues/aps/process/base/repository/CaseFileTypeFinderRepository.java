package edu.ues.aps.process.base.repository;

import javax.persistence.NoResultException;

public interface CaseFileTypeFinderRepository {

    Class getType(Long id_caseFile) throws NoResultException;
}
