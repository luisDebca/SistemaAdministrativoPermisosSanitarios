package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.Product;
import edu.ues.aps.process.base.repository.ProductCrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional
public class ProductService implements ProductFinderService, ProductPersistenceService {

    private ProductCrudRepository productCrudRepository;

    public ProductService(ProductCrudRepository productCrudRepository) {
        this.productCrudRepository = productCrudRepository;
    }

    @Override
    public List<Product> findAllProductsFromLegalEntity(Long id_entity) {
        return productCrudRepository.findAllProductsFromOwner(id_entity);
    }

    @Override
    public void delete(Long id_product) throws NoResultException {
        productCrudRepository.delete(id_product);
    }
}
