package edu.ues.aps.process.base.service;

import edu.ues.aps.process.area.vehicle.model.AbstractVehicle;

import java.util.List;

public interface OwnerVehicleAdderService {

    void add(List<AbstractVehicle> vehicles, Long id_owner);

    void add(AbstractVehicle vehicle, Long id_owner);
}
