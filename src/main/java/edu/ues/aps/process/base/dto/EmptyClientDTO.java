package edu.ues.aps.process.base.dto;

import edu.ues.aps.process.base.model.ClientType;

public class EmptyClientDTO implements AbstractClientDTO {

    @Override
    public Long getId_owner() {
        return 0L;
    }

    @Override
    public Long getId_client() {
        return 0L;
    }

    @Override
    public String getType() {
        return "Client type not found";
    }

    @Override
    public String getClient_name() {
        return "Client name not found";
    }

    @Override
    public String getClient_nationally() {
        return "Client nationally not found";
    }

    @Override
    public String getClient_DUI() {
        return "Client DUI not found";
    }

    @Override
    public String getClient_passport() {
        return "Client passport not found";
    }

    @Override
    public String getClient_resident_card() {
        return "Client resident card not found";
    }

    @Override
    public String getClient_email() {
        return "Client email not found";
    }

    @Override
    public String getClient_telephone() {
        return "Client phone not found";
    }

    @Override
    public ClientType getClientType() {
        return ClientType.INVALID;
    }

    @Override
    public void setId_client(Long id_client) {

    }

    @Override
    public void setType(ClientType type) {

    }

    @Override
    public void setClient_name(String client_name) {

    }

    @Override
    public void setClient_nationally(String client_nationally) {

    }

    @Override
    public void setClient_DUI(String client_DUI) {

    }

    @Override
    public void setClient_passport(String client_passport) {

    }

    @Override
    public void setClient_resident_card(String client_resident_card) {

    }

    @Override
    public void setClient_email(String client_email) {

    }

    @Override
    public void setClient_telephone(String client_telephone) {

    }

    @Override
    public void setId_owner(Long id_owner) {

    }

    @Override
    public void setClientType(ClientType clientType) {

    }
}
