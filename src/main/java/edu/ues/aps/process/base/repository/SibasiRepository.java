package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SibasiRepository implements SibasiDTOFinderRepository {

    private SessionFactory sessionFactory;

    public SibasiRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractMapDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.MapDTO(sibasi.id,sibasi.zone) " +
                "from Sibasi sibasi", AbstractMapDTO.class)
                .getResultList();
    }
}
