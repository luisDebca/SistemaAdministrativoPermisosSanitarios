package edu.ues.aps.process.base.model;

import java.util.Collections;
import java.util.List;

public class EmptyMunicipality implements AbstractMunicipality {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getName() {
        return "Municipality name not found";
    }

    @Override
    public AbstractDepartment getDepartment() {
        return new EmptyDepartment();
    }

    @Override
    public List<Address> getAddresses() {
        return Collections.emptyList();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setName(String name) {

    }

    @Override
    public void setDepartment(Department department) {

    }

    @Override
    public void setAddresses(List<Address> addresses) {

    }

    @Override
    public String toString() {
        return "EmptyMunicipality{}";
    }
}
