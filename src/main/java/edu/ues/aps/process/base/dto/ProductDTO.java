package edu.ues.aps.process.base.dto;

public class ProductDTO implements AbstractProductDTO {

    private Long id_product;
    private String product_type;
    private String product_sanitary;
    private String product_name;

    public ProductDTO(Long id_product, String product_type, String product_sanitary, String product_name) {
        this.id_product = id_product;
        this.product_type = product_type;
        this.product_sanitary = product_sanitary;
        this.product_name = product_name;
    }

    public Long getId_product() {
        return id_product;
    }

    public String getProduct_type() {
        return product_type;
    }

    public String getProduct_sanitary() {
        return product_sanitary;
    }

    public String getProduct_name() {
        return product_name;
    }
}
