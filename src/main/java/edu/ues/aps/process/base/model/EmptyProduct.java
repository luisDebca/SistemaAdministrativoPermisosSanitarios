package edu.ues.aps.process.base.model;

public class EmptyProduct implements AbstractProduct {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getType() {
        return "Product type not found";
    }

    @Override
    public String getSanitary() {
        return "Product sanitary not found";
    }

    @Override
    public String getName() {
        return "Product name not found";
    }

    @Override
    public AbstractLegalEntity getLegalEntity() {
        return new EmptyLegalEntity();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setType(String type) {

    }

    @Override
    public void setSanitary(String sanitary) {

    }

    @Override
    public void setName(String name) {

    }

    @Override
    public void setLegalEntity(LegalEntity legalEntity) {

    }

    @Override
    public String toString() {
        return "EmptyProduct{}";
    }
}
