package edu.ues.aps.process.base.async;

import edu.ues.aps.process.base.service.OwnerValidatorService;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.Locale;

@Controller
@RequestMapping("/owner")
public class OwnerValidatorController {

    private final MessageSource messageSource;
    private final OwnerValidatorService validatorService;

    public OwnerValidatorController(OwnerValidatorService validatorService, MessageSource messageSource) {
        this.validatorService = validatorService;
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/validate-name", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateName(@RequestParam Long id_owner, @RequestParam String name) {
        return validatorService.isUniqueName(id_owner, name) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("owner.name", new String[]{name}, Locale.getDefault()));
    }

    @GetMapping(value = "/validate-nit", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateNIT(@RequestParam Long id_owner, @RequestParam String nit) {
        return validatorService.isUniqueNIT(id_owner, nit) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("owner.nit", new String[]{nit}, Locale.getDefault()));
    }

    @GetMapping(value = "/validate-dui", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateDUI(@RequestParam Long id_owner, @RequestParam String dui) {
        return validatorService.isUniqueDUI(id_owner, dui) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("owner.dui", new String[]{dui}, Locale.getDefault()));
    }

    @GetMapping(value = "/validate-passport", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validatePassport(@RequestParam Long id_owner, @RequestParam String passport) {
        return validatorService.isUniquePassport(id_owner, passport) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("owner.passport", new String[]{passport}, Locale.getDefault()));
    }

    @GetMapping(value = "/validate-resident-card", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateResidentCard(@RequestParam Long id_owner, @RequestParam String residentCard) {
        return validatorService.isUniqueResidentCard(id_owner, residentCard) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("owner.residentCard", new String[]{residentCard}, Locale.getDefault()));
    }
}
