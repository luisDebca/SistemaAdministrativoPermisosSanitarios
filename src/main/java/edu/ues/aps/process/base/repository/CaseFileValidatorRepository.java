package edu.ues.aps.process.base.repository;

public interface CaseFileValidatorRepository {

    Long isUniqueFolderNumber(String number, String year);

    Long isUniqueFolderNumber(Long id_caseFile, String number, String year);

    Long isUniqueRequestNumber(String code, String number, String year);

    Long isUniqueRequestNumber(Long id_caseFile, String code, String number, String year);
}
