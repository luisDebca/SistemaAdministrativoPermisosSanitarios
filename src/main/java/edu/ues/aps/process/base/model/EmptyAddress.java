package edu.ues.aps.process.base.model;

public class EmptyAddress implements AbstractAddress {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getDetails() {
        return "Address details not found";
    }

    @Override
    public AbstractMunicipality getMunicipality() {
        return new EmptyMunicipality();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setDetails(String details) {

    }

    @Override
    public void setMunicipality(Municipality municipality) {

    }

    @Override
    public String toString() {
        return "EmptyAddress{}";
    }
}
