package edu.ues.aps.process.base.async;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import edu.ues.aps.process.base.service.AddressFinderService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class AddressInformationProviderAsyncController {

    private AddressFinderService addressFinderService;

    public AddressInformationProviderAsyncController(AddressFinderService addressFinderService) {
        this.addressFinderService = addressFinderService;
    }

    @GetMapping(value = "/find-all.municipalities-from-department", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractMapDTO> findAllMunicipalitiesFromDepartment(@RequestParam("id") Long id_department) {
        return addressFinderService.findAllMunicipalitiesFromDepartment(id_department);
    }
}
