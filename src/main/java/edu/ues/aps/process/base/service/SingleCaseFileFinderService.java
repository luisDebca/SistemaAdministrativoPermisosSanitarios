package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.AbstractCasefile;

public interface SingleCaseFileFinderService {

    AbstractCasefile find(Long id_caseFile);
}
