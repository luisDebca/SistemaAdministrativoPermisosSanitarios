package edu.ues.aps.process.base.report;

import org.springframework.ui.ModelMap;

public abstract class AbstractReportController {

    public static final String PDF_FORMAT = "pdf";
    public static final String CSV_FORMAT = "csv";
    public static final String XLS_FORMAT = "xls";
    public static final String HTML_FORMAT = "html";

    private ModelMap model = new ModelMap();

    public void addReportRequiredSources(Object o) {
        addReportLocation();
        addReportFormat(PDF_FORMAT);
        addDataSource(o);
    }

    public void addReportRequiredSources(Object o, String format) {
        addReportLocation();
        addReportFormat(format);
        addDataSource(o);
    }

    public void addReportLocation() {
        model.addAttribute("REPORTS_DIR", this.getClass().getResource("/jasperreports/").getPath());
    }

    public void addReportFormat(String format) {
        model.addAttribute("format", format);
    }

    public void addDataSource(Object o) {
        model.addAttribute("datasource", o);
    }

    public ModelMap getModel() {
        return model;
    }

    public void setModel(ModelMap model) {
        this.model = model;
    }
}
