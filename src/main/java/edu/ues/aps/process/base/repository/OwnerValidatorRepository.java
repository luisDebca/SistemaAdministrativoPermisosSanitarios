package edu.ues.aps.process.base.repository;

public interface OwnerValidatorRepository {

    Long isUniqueName(String name);

    Long isUniqueNIT(String nit);

    Long isUniqueDUI(String dui);

    Long isUniquePassport(String passport);

    Long isUniqueResidentCard(String residentCard);

    Long isUniqueName(Long id_owner, String name);

    Long isUniqueNIT(Long id_owner, String nit);

    Long isUniqueDUI(Long id_owner, String dui);

    Long isUniquePassport(Long id_owner, String passport);

    Long isUniqueResidentCard(Long id_owner, String residentCard);
}
