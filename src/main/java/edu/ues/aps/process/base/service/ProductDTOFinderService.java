package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractProductDTO;

import java.util.List;

public interface ProductDTOFinderService {

    List<AbstractProductDTO> findAllProductsFromLegalEntity(Long id_owner);
}
