package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class CaseFileRepository implements CaseFileTypeFinderRepository, SingleCaseFileFinderRepository, CaseFilePersistenceRepository, CaseFileRequiredReviewValidatorRepository, CaseFileValidatorRepository {

    private final SessionFactory sessionFactory;

    public CaseFileRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Class getType(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select type(casefile) " +
                "from Casefile casefile where casefile.id = :id", Class.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }

    @Override
    public AbstractCasefile find(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select casefile " +
                "from Casefile casefile " +
                "left join fetch casefile.ucsf ucsf " +
                "left join fetch ucsf.sibasi sibasi " +
                "where casefile.id = :id", AbstractCasefile.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }

    @Override
    public void merge(AbstractCasefile caseFile) {
        sessionFactory.getCurrentSession().merge(caseFile);
    }

    @Override
    public void update(AbstractCasefile caseFile) {
        sessionFactory.getCurrentSession().update(caseFile);
    }

    @Override
    public void saveOrUpdate(AbstractCasefile caseFile) {
        sessionFactory.getCurrentSession().saveOrUpdate(caseFile);
    }

    @Override
    public AbstractCasefile findById(Long id_caseFile) {
        return sessionFactory.getCurrentSession().get(Casefile.class, id_caseFile);
    }

    @Override
    public void validateCaseFile(Long id_caseFile) {
        sessionFactory.getCurrentSession().createQuery("update Casefile set validate = true where id = :id")
                .setParameter("id", id_caseFile)
                .executeUpdate();
    }

    @Override
    public void validateUCSFDelivery(Long id_caseFile) {
        sessionFactory.getCurrentSession().createQuery("update Casefile set ucsf_delivery = true where id = :id")
                .setParameter("id", id_caseFile)
                .executeUpdate();
    }

    @Override
    public Long isUniqueFolderNumber(Long id_caseFile, String number, String year) {
        return sessionFactory.getCurrentSession().createQuery("select count(cf) " +
                "from Casefile cf " +
                "where cf.id = :id " +
                "and cf.request_folder = :folder " +
                "and cf.request_year = :year", Long.class)
                .setParameter("id", id_caseFile)
                .setParameter("folder", number)
                .setParameter("year", year)
                .getSingleResult();
    }

    @Override
    public Long isUniqueRequestNumber(Long id_caseFile, String code, String number, String year) {
        return sessionFactory.getCurrentSession().createQuery("select count(cf) " +
                "from Casefile cf " +
                "where cf.id = :id " +
                "and cf.request_code = :code " +
                "and cf.request_number = :number " +
                "and cf.request_year = :year", Long.class)
                .setParameter("id", id_caseFile)
                .setParameter("code", code)
                .setParameter("number", number)
                .setParameter("year", year)
                .getSingleResult();
    }

    @Override
    public Long isUniqueFolderNumber(String number, String year) {
        return sessionFactory.getCurrentSession().createQuery("select count(cf) " +
                "from Casefile cf " +
                "where cf.request_folder = :folder " +
                "and cf.request_year = :year", Long.class)
                .setParameter("folder", number)
                .setParameter("year", year)
                .getSingleResult();
    }

    @Override
    public Long isUniqueRequestNumber(String code, String number, String year) {
        return sessionFactory.getCurrentSession().createQuery("select count(cf) " +
                "from Casefile cf " +
                "where cf.request_code = :code " +
                "and cf.request_number = :number " +
                "and cf.request_year = :year", Long.class)
                .setParameter("code", code)
                .setParameter("number", number)
                .setParameter("year", year)
                .getSingleResult();
    }
}
