package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.Product;

import java.util.List;

public interface ProductFinderService {

    List<Product> findAllProductsFromLegalEntity(Long id_entity);
}
