package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.*;
import edu.ues.aps.process.base.repository.OwnerEntityFinderRepository;
import edu.ues.aps.process.base.repository.OwnerEntityPersistenceRepository;
import edu.ues.aps.process.base.repository.OwnerFinderRepository;
import edu.ues.aps.process.base.repository.OwnerValidatorRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
public class OwnerEntityService implements OwnerFinderService, OwnerEntityPersistenceService, SingleOwnerEntityFinderService, OwnerProductAdderService, OwnerValidatorService {

    private final OwnerEntityPersistenceRepository persistenceRepository;
    private final List<OwnerFinderRepository> finderRepositories;
    private final OwnerEntityFinderRepository finderRepository;
    private final OwnerValidatorRepository validatorRepository;
    private AbstractOwner owner;

    public OwnerEntityService(OwnerEntityPersistenceRepository persistenceRepository, OwnerEntityFinderRepository finderRepository, List<OwnerFinderRepository> finderRepositories, OwnerValidatorRepository validatorRepository) {
        this.persistenceRepository = persistenceRepository;
        this.finderRepository = finderRepository;
        this.finderRepositories = finderRepositories;
        this.validatorRepository = validatorRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractOwner findById(Long id_owner) {
        for (OwnerFinderRepository repository : finderRepositories) {
            isFoundThenAssignToThisOwner(repository.findOwnerById(id_owner));
        }
        return owner;
    }


    private void isFoundThenAssignToThisOwner(AbstractOwner found_owner) {
        if (found_owner.getId() != 0) {
            this.owner = found_owner;
        }
    }

    @Override
    @Transactional
    public void save(AbstractOwner owner) {
        persistenceRepository.save(owner);
    }

    @Override
    @Transactional
    public void update(AbstractNaturalEntity owner) {
        persistenceRepository.update(owner);
    }

    @Override
    @Transactional
    public void update(AbstractLegalEntity owner) {
        persistenceRepository.update(owner);
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractNaturalEntity findNaturalEntity(Long id_owner) {
        try {
            return finderRepository.getNaturalEntity(id_owner);
        } catch (NoResultException e) {
            return new EmptyNaturalEntity();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractLegalEntity findLegalEntity(Long id_owner) {
        try {
            return finderRepository.getLegalEntity(id_owner);
        } catch (NoResultException e) {
            return new EmptyLegalEntity();
        }
    }

    @Override
    @Transactional
    public void addProducts(List<Product> products, Long id_owner) {
        AbstractLegalEntity entity = finderRepository.getLegalEntity(id_owner);
        for (AbstractProduct product : products) {
            entity.addProduct((Product) product);
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueName(Long id_owner, String name) {
        if (validatorRepository.isUniqueName(id_owner, name) == 1) {
            return true;
        }
        return validatorRepository.isUniqueName(name) == 0;
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueNIT(Long id_owner, String nit) {
        if (validatorRepository.isUniqueNIT(id_owner, nit) == 1) {
            return true;
        }
        return validatorRepository.isUniqueNIT(nit) == 0;
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueDUI(Long id_owner, String dui) {
        if (validatorRepository.isUniqueDUI(id_owner, dui) == 1) {
            return true;
        }
        return validatorRepository.isUniqueDUI(dui) == 0;
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniquePassport(Long id_owner, String passport) {
        if (validatorRepository.isUniquePassport(id_owner, passport) == 1) {
            return true;
        }
        return validatorRepository.isUniquePassport(passport) == 0;
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueResidentCard(Long id_owner, String residentCard) {
        if (validatorRepository.isUniqueResidentCard(id_owner, residentCard) == 1) {
            return true;
        }
        return validatorRepository.isUniqueResidentCard(residentCard) == 0;
    }
}
