package edu.ues.aps.process.base.dto;

public abstract class AbstractEmptyOwnerEntity implements AbstractOwnerEntityDTO {

    @Override
    public Long getId_owner() {
        return 0L;
    }

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public String getOwner_nit() {
        return "Owner NIT not found";
    }

    @Override
    public String getOwner_email() {
        return "Owner email not found";
    }

    @Override
    public String getOwner_telephone() {
        return "Owner telephone not found";
    }

    @Override
    public String getOwner_fax() {
        return "Owner fax not found";
    }

    @Override
    public String getClass_type() {
        return "Class not found";
    }

    @Override
    public Long getEstablishmentCount() {
        return 0L;
    }

    @Override
    public Long getVehicleCount() {
        return 0L;
    }

    @Override
    public Long getClient_count() {
        return 0L;
    }
}
