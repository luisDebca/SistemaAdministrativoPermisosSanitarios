package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import edu.ues.aps.process.base.repository.AddressFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class AddressService implements AddressFinderService {

    private AddressFinderRepository finderRepository;

    public AddressService(AddressFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractMapDTO> findAllMunicipalities() {
        return finderRepository.findAllMunicipalities();
    }

    @Override
    public List<AbstractMapDTO> findAllDepartments() {
        return finderRepository.findAllDepartments();
    }

    @Override
    public List<AbstractMapDTO> findAllMunicipalitiesFromDepartment(Long id_department) {
        return finderRepository.findAllMunicipalitiesFromDepartment(id_department);
    }

}
