package edu.ues.aps.process.base.async;

import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.service.ClientDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/owner/client")
public class ClientInformationProviderAsyncController {

    private static final Logger logger = LogManager.getLogger(ClientInformationProviderAsyncController.class);

    private final ClientDTOFinderService finderService;

    public ClientInformationProviderAsyncController(ClientDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/find-all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractClientDTO> findAllClientsFromEstablishment(@RequestParam("id") Long id_owner) {
        logger.info("GET:: Find establishment {} client list", id_owner);
        return finderService.findAll(id_owner);
    }

    @GetMapping(value = "/find-all-with-natural-owner", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractClientDTO> findAllClientsWithNaturalOwnerFromEstablishment(@RequestParam("id") Long id_owner) {
        logger.info("GET:: Find all clients and natural owner from establishment {}", id_owner);
        return finderService.findAllWithNaturalOwner(id_owner);
    }

}
