package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractProductDTO;

import java.util.List;

public interface ProductDTOFinderRepository {

    List<AbstractProductDTO> findAllProductsFromLegalEntity(Long id_owner);
}
