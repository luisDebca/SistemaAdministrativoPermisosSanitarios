package edu.ues.aps.process.base.model;

import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.area.vehicle.model.Vehicle;

import java.util.Collections;
import java.util.List;

public class EmptyNaturalEntity implements AbstractNaturalEntity {

    @Override
    public String getDUI() {
        return "Natural entity DUI not found";
    }

    @Override
    public void setDUI(String DUI) {

    }

    @Override
    public String getNationally() {
        return "Natural entity Nationally not found";
    }

    @Override
    public void setNationally(String nationally) {

    }

    @Override
    public String getPassport() {
        return "Natural entity passport not found";
    }

    @Override
    public void setPassport(String passport) {

    }

    @Override
    public String getResidentCard() {
        return "Natural entity resident card not found";
    }

    @Override
    public void setResidentCard(String residentCard) {

    }

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getName() {
        return "Natural entity name not found";
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public String getNit() {
        return "Natural entity NIT not found";
    }

    @Override
    public void setNit(String nit) {

    }

    @Override
    public String getEmail() {
        return "Natural entity email not found";
    }

    @Override
    public void setEmail(String email) {

    }

    @Override
    public String getTelephone() {
        return "Natural entity telephone not found";
    }

    @Override
    public void setTelephone(String telephone) {

    }

    @Override
    public String getFAX() {
        return "Natural entity FAX not found";
    }

    @Override
    public void setFAX(String fax) {

    }

    @Override
    public List<Establishment> getEstablishments() {
        return Collections.emptyList();
    }

    @Override
    public void setEstablishments(List<Establishment> establishments) {

    }

    @Override
    public void addEstablishment(Establishment establishment) {

    }

    @Override
    public void removeEstablishment(Establishment establishment) {

    }

    @Override
    public List<Vehicle> getVehicles() {
        return Collections.emptyList();
    }

    @Override
    public void setVehicles(List<Vehicle> vehicles) {

    }

    @Override
    public void addVehicle(Vehicle vehicle) {

    }

    @Override
    public List<OwnerClient> getClients() {
        return Collections.emptyList();
    }

    @Override
    public void setClients(List<OwnerClient> clients) {

    }

    @Override
    public void addClient(Client client, ClientType type) {

    }

    @Override
    public void removeClient(Client client, ClientType type) {

    }
}
