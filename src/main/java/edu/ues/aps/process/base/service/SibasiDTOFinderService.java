package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractMapDTO;

import java.util.List;

public interface SibasiDTOFinderService {

    List<AbstractMapDTO> findAll();
}
