package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractMapDTO;

import java.util.List;

public interface AddressFinderRepository {

    List<AbstractMapDTO> findAllDepartments();

    List<AbstractMapDTO> findAllMunicipalities();

    List<AbstractMapDTO> findAllMunicipalitiesFromDepartment(Long id_department);

}
