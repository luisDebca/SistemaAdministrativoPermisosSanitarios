package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractBaseDTO;
import edu.ues.aps.process.base.repository.BaseFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class BaseService implements BaseFinderService {

    private final BaseFinderRepository finderRepository;

    public BaseService(BaseFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractBaseDTO findById(Long id_caseFile) {
        return finderRepository.findById(id_caseFile);
    }
}
