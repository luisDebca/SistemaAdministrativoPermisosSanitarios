package edu.ues.aps.process.base.service;

import edu.ues.aps.process.area.establishment.model.AbstractEstablishment;

public interface OwnerEstablishmentAdderService {

    void add(AbstractEstablishment establishment, Long id_owner);
}
