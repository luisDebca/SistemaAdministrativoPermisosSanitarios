package edu.ues.aps.process.base.model;

import java.util.List;

public interface AbstractUcsf {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    AbstractSibasi getSibasi();

    void setSibasi(Sibasi sibasi);

    List<Casefile> getCasefiles();

    void setCasefiles(List<Casefile> casefiles);
}
