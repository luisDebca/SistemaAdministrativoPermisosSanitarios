package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.area.vehicle.model.Vehicle;
import edu.ues.aps.process.base.dto.AbstractBaseDTO;
import edu.ues.aps.process.base.model.CasefileSection;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class BaseRepository implements BaseFinderRepository {

    private final SessionFactory sessionFactory;

    public BaseRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    private AbstractBaseDTO result;

    @Override
    public AbstractBaseDTO findById(Long id_caseFile) throws NoResultException {
        result = sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.BaseDTO(" +
                "concat(concat(cf.request_folder,'-'),cf.request_year)," +
                "concat(concat(concat(cf.request_number,cf.request_code),'-'),cf.request_year)," +
                "concat(concat(concat(resolution.resolution_number,resolution.resolution_code),'-'),resolution.resolution_year)," +
                "cf.section,cf.certification_type,cf.status) " +
                "from Casefile cf " +
                "left join cf.resolution resolution " +
                "where cf.id = :id", AbstractBaseDTO.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
        result.setId_caseFile(id_caseFile);
        addEstablishmentInformation();
        addVehicleInformation();
        return result;
    }

    private void addEstablishmentInformation() throws NoResultException{
        if(result.getSection().equals(CasefileSection.ESTABLISHMENT.getCasefileSection())){
            Establishment establishment = sessionFactory.getCurrentSession().createQuery("select establishment " +
                    "from Establishment establishment " +
                    "join fetch establishment.owner owner " +
                    "join fetch establishment.casefiles cf " +
                    "where cf.id = :id", Establishment.class)
                    .setParameter("id", result.getId_caseFile())
                    .getSingleResult();
            result.setId_target(establishment.getId());
            result.setTarget(establishment.getName());
            result.setId_owner(establishment.getOwner().getId());
            result.setOwner(establishment.getOwner().getName());
            result.setId_group(0L);
        }
    }

    private void addVehicleInformation() throws NoResultException {
        if(result.getSection().equals(CasefileSection.VEHICLE.getCasefileSection())){
            Vehicle vehicle = sessionFactory.getCurrentSession().createQuery("select vehicle " +
                    "from Vehicle vehicle " +
                    "join fetch vehicle.owner owner " +
                    "join fetch vehicle.casefile_list cf " +
                    "join fetch cf.bunch bunch " +
                    "where cf.id = :id", Vehicle.class)
                    .setParameter("id", result.getId_caseFile())
                    .getSingleResult();
            result.setId_target(vehicle.getId());
            result.setTarget(vehicle.getLicense());
            result.setId_owner(vehicle.getOwner().getId());
            result.setOwner(vehicle.getOwner().getName());
            if (!vehicle.getCasefile_list().isEmpty())
                result.setId_group(vehicle.getCasefile_list().get(0).getBunch().getId());
        }
    }
}
