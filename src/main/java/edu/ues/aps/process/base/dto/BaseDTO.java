package edu.ues.aps.process.base.dto;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;

public class BaseDTO implements AbstractBaseDTO {


    private Long id_caseFile;
    private Long id_owner;
    private Long id_target;
    private Long id_group;
    private String folder;
    private String request;
    private String resolution;
    private String owner;
    private String target;
    private CasefileSection section;
    private CertificationType certification;
    private CasefileStatusType status;

    public BaseDTO(String folder, String request, String resolution, CasefileSection section, CertificationType certification, CasefileStatusType status) {
        this.folder = folder;
        this.request = request;
        this.resolution = resolution;
        this.section = section;
        this.certification = certification;
        this.status = status;
    }

    public Long getId_caseFile() {
        return id_caseFile;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public Long getId_target() {
        return id_target;
    }

    public Long getId_group() {
        return id_group;
    }

    public String getFolder() {
        return folder;
    }

    public String getRequest() {
        return request;
    }

    public String getResolution() {
        return resolution;
    }

    public String getOwner() {
        return owner;
    }

    public String getTarget() {
        return target;
    }

    public String getSection() {
        if (section == null)
            return CasefileSection.INVALID.getCasefileSection();
        return section.getCasefileSection();
    }

    @Override
    public String getSectionType() {
        if (section == null)
            return CasefileSection.INVALID.name();
        return section.name();
    }

    public String getCertification() {
        if (certification == null)
            return CertificationType.INVALID.getCertificationType();
        return certification.getCertificationType();
    }

    public String getStatus() {
        if (status == null)
            return CasefileStatusType.INVALID.getStatus_type();
        return status.getStatus_type();
    }

    public void setId_caseFile(Long id_caseFile) {
        this.id_caseFile = id_caseFile;
    }

    public void setId_owner(Long id_owner) {
        this.id_owner = id_owner;
    }

    public void setId_target(Long id_target) {
        this.id_target = id_target;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public void setId_group(Long id_group) {
        this.id_group = id_group;
    }
}
