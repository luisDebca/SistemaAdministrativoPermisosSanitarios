package edu.ues.aps.process.base.controller;

import edu.ues.aps.process.base.model.LegalEntity;
import edu.ues.aps.process.base.model.NaturalEntity;
import edu.ues.aps.process.base.service.OwnerEntityPersistenceService;
import edu.ues.aps.process.base.service.SingleOwnerEntityFinderService;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
public class OwnerPersistenceController {

    private static final Logger logger = LogManager.getLogger(OwnerPersistenceController.class);

    private static final String LEGAL_OWNER_NEW = "legal_owner_new";
    private static final String NATURAL_OWNER_NEW = "natural_owner_new";
    private static final String LEGAL_OWNER_EDITION = "legal_owner_edition_view";
    private static final String NATURAL_OWNER_EDITION = "natural_owner_edition_view";

    private final OwnerEntityPersistenceService persistenceService;
    private final SingleOwnerEntityFinderService finderService;
    private final ProcessUserRegisterService registerService;

    public OwnerPersistenceController(OwnerEntityPersistenceService persistenceService, SingleOwnerEntityFinderService finderService, ProcessUserRegisterService registerService) {
        this.persistenceService = persistenceService;
        this.finderService = finderService;
        this.registerService = registerService;
    }

    @GetMapping("/owner/natural/new")
    public String showNaturalOwnerNewForm(ModelMap model) {
        logger.info("GET:: Show natural owner new form");
        model.addAttribute("owner", new NaturalEntity());
        return NATURAL_OWNER_NEW;
    }

    @GetMapping("/owner/legal/new")
    public String showLegalOwnerNewForm(ModelMap model) {
        logger.info("GET:: Show legal owner new form");
        model.addAttribute("owner", new LegalEntity());
        return LEGAL_OWNER_NEW;
    }

    @PostMapping("/owner/natural/new")
    public String saveNaturalOwner(@ModelAttribute NaturalEntity owner, Principal principal) {
        logger.info("POST:: Save new natural owner: {}", owner.toString());
        registerService.addProcessUser(principal.getName(), "process.owner.new", new String[]{owner.toString()});
        persistenceService.save(owner);
        return "redirect:/owner/";
    }

    @PostMapping("/owner/legal/new")
    public String saveLegalOwner(@ModelAttribute LegalEntity owner, Principal principal) {
        logger.info("POST:: Save new legal owner: {}", owner.toString());
        registerService.addProcessUser(principal.getName(), "process.owner.new", new String[]{owner.toString()});
        if (owner.getAddress().getDetails().isEmpty()) {
            owner.setAddress(null);
        }
        persistenceService.save(owner);
        return "redirect:/owner/";
    }

    @GetMapping("/owner/natural/{id_owner}/edit")
    public String showNaturalOwnerEditForm(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show natural owner {} edit from", id_owner);
        model.addAttribute("owner", finderService.findNaturalEntity(id_owner));
        return NATURAL_OWNER_EDITION;
    }

    @GetMapping("/owner/legal/{id_owner}/edit")
    public String showLegalOwnerEditForm(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show legal owner {} edit from", id_owner);
        model.addAttribute("owner", finderService.findLegalEntity(id_owner));
        return LEGAL_OWNER_EDITION;
    }

    @PostMapping("/owner/natural/{id_owner}/edit")
    public String updateNaturalEntity(@ModelAttribute NaturalEntity owner, @PathVariable Long id_owner, Principal principal) {
        logger.info("POST:: Update natural owner: {}", owner.toString());
        registerService.addProcessUser(principal.getName(), "process.owner.edit", new String[]{String.valueOf(id_owner), owner.toString()});
        persistenceService.update(owner);
        return "redirect:/owner/natural/" + id_owner;
    }

    @PostMapping("/owner/legal/{id_owner}/edit")
    public String updateLegalEntity(@ModelAttribute LegalEntity owner, @PathVariable Long id_owner, Principal principal) {
        registerService.addProcessUser(principal.getName(), "process.owner.edit", new String[]{String.valueOf(id_owner), owner.toString()});
        logger.info("POST:: Update legal owner: {}", owner.toString());
        persistenceService.update(owner);
        return "redirect:/owner/legal/" + id_owner;
    }

}
