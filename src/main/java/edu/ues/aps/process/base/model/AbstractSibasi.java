package edu.ues.aps.process.base.model;

import java.util.List;

public interface AbstractSibasi {

    Long getId();

    void setId(Long id);

    String getZone();

    void setZone(String zone);

    List<Ucsf> getUcsfList();

    void setUcsfList(List<Ucsf> ucsfList);
}
