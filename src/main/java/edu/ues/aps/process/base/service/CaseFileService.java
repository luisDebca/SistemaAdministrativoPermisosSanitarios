package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Ucsf;
import edu.ues.aps.process.base.repository.CaseFilePersistenceRepository;
import edu.ues.aps.process.base.repository.CaseFileRequiredReviewValidatorRepository;
import edu.ues.aps.process.base.repository.CaseFileValidatorRepository;
import edu.ues.aps.process.base.repository.SingleCaseFileFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
public class CaseFileService implements SingleCaseFileFinderService, CaseFilePersistenceService, CaseFileRequiredReviewValidatorService, CaseFileValidatorService {

    private final CaseFileValidatorRepository validatorRepository;
    private final SingleCaseFileFinderRepository finderRepository;
    private final CaseFilePersistenceRepository persistenceRepository;
    private final CaseFileRequiredReviewValidatorRepository reviewValidatorRepository;

    public CaseFileService(SingleCaseFileFinderRepository finderRepository, CaseFilePersistenceRepository persistenceRepository, CaseFileRequiredReviewValidatorRepository reviewValidatorRepository, CaseFileValidatorRepository validatorRepository) {
        this.finderRepository = finderRepository;
        this.persistenceRepository = persistenceRepository;
        this.reviewValidatorRepository = reviewValidatorRepository;
        this.validatorRepository = validatorRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractCasefile find(Long id_caseFile) {
        return finderRepository.find(id_caseFile);
    }

    @Override
    @Transactional
    public void update(AbstractCasefile draft) {
        AbstractCasefile caseFile = finderRepository.find(draft.getId());
        caseFile.setRequest_folder(draft.getRequest_folder());
        caseFile.setRequest_number(draft.getRequest_number());
        caseFile.setRequest_code(draft.getRequest_code());
        caseFile.setRequest_year(draft.getRequest_year());
        caseFile.setCertification_type(draft.getCertification_type());
        caseFile.setCertification_duration_in_years(draft.getCertification_duration_in_years());
        caseFile.setApply_payment(draft.getApply_payment());
        caseFile.setUcsf((Ucsf) draft.getUcsf());
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractCasefile findById(Long id_caseFile) {
        return persistenceRepository.findById(id_caseFile);
    }

    @Override
    @Transactional
    public void saveOrUpdate(AbstractCasefile caseFile) {
        persistenceRepository.saveOrUpdate(caseFile);
    }

    @Override
    @Transactional
    public void merge(AbstractCasefile caseFile) {
        persistenceRepository.merge(caseFile);
    }

    @Override
    @Transactional
    public void validateCaseFile(Long id_caseFile) {
        reviewValidatorRepository.validateCaseFile(id_caseFile);
    }

    @Override
    @Transactional
    public void validateUCSFDelivery(Long id_caseFile) {
        reviewValidatorRepository.validateUCSFDelivery(id_caseFile);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueFolderNumber(Long id_caseFile, String number, String year) {
        if (validatorRepository.isUniqueFolderNumber(id_caseFile, number, year) == 1) {
            return true;
        } else {
            return validatorRepository.isUniqueFolderNumber(number, year) == 0;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueRequestNumber(Long id_caseFile, String code, String number, String year) {
        if (validatorRepository.isUniqueRequestNumber(id_caseFile, code, number, year) == 1) {
            return true;
        } else {
            return validatorRepository.isUniqueRequestNumber(code, number, year) == 0;
        }
    }
}
