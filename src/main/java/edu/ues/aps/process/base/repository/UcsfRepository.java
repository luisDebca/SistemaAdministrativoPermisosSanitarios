package edu.ues.aps.process.base.repository;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class UcsfRepository implements UcsfDTOFinderRepository {

    private SessionFactory sessionFactory;

    public UcsfRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractMapDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.MapDTO(ucsf.id,ucsf.name) " +
                "from Ucsf ucsf", AbstractMapDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractMapDTO> findAllUcsfFromSibasi(Long id_sibasi) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.MapDTO(ucsf.id,ucsf.name) " +
                "from Ucsf ucsf " +
                "inner join ucsf.sibasi sibasi " +
                "where sibasi.id = :id", AbstractMapDTO.class)
                .setParameter("id", id_sibasi)
                .getResultList();
    }
}
