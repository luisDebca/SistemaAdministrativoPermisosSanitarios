package edu.ues.aps.process.base.model;

public interface AbstractProduct {

    Long getId();

    void setId(Long id);

    String getType();

    void setType(String type);

    String getSanitary();

    void setSanitary(String sanitary);

    String getName();

    void setName(String name);

    AbstractLegalEntity getLegalEntity();

    void setLegalEntity(LegalEntity legalEntity);
}
