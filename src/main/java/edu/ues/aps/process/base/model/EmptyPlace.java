package edu.ues.aps.process.base.model;

public class EmptyPlace implements AbstractPlace {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getOrganization_name() {
        return "Not found";
    }

    @Override
    public void setOrganization_name(String organization_name) {

    }

    @Override
    public String getSection_name() {
        return "Not found";
    }

    @Override
    public void setSection_name(String section_name) {

    }

    @Override
    public String getBusiness_description() {
        return "Not found";
    }

    @Override
    public void setBusiness_description(String business_description) {

    }

    @Override
    public String getAddress() {
        return "Not found";
    }

    @Override
    public void setAddress(String address) {

    }

    @Override
    public String getMunicipality() {
        return "Not found";
    }

    @Override
    public void setMunicipality(String municipality) {

    }

    @Override
    public String getDepartment() {
        return "Not found";
    }

    @Override
    public void setDepartment(String department) {

    }

    @Override
    public String getDepartment_code() {
        return "Not found";
    }

    @Override
    public void setDepartment_code(String department_code) {

    }

    @Override
    public String getZone() {
        return "Not found";
    }

    @Override
    public void setZone(String zone) {

    }

    @Override
    public String getTelephone() {
        return "Not found";
    }

    @Override
    public void setTelephone(String telephone) {

    }

    @Override
    public String getEmail() {
        return "Not found";
    }

    @Override
    public void setEmail(String email) {

    }

    @Override
    public String getFax() {
        return "Not found";
    }

    @Override
    public void setFax(String fax) {

    }

    @Override
    public String getAvailability() {
        return "Not found";
    }

    @Override
    public void setAvailability(String availability) {

    }
}
