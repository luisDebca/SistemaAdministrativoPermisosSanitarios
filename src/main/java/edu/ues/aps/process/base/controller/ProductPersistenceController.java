package edu.ues.aps.process.base.controller;

import edu.ues.aps.process.base.model.AbstractLegalEntity;
import edu.ues.aps.process.base.model.LegalEntity;
import edu.ues.aps.process.base.service.OwnerProductAdderService;
import edu.ues.aps.process.base.service.ProductFinderService;
import edu.ues.aps.process.base.service.SingleOwnerEntityFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/owner")
public class ProductPersistenceController {

    private static final Logger logger = LogManager.getLogger(ProductPersistenceController.class);

    private static final String LEGAL_ENTITY_NEW_PRODUCT_LIST = "products_new";

    private final SingleOwnerEntityFinderService finderService;
    private final OwnerProductAdderService adderService;
    private final ProductFinderService productFinderService;

    public ProductPersistenceController(ProductFinderService productFinderService, SingleOwnerEntityFinderService finderService, OwnerProductAdderService adderService) {
        this.productFinderService = productFinderService;
        this.finderService = finderService;
        this.adderService = adderService;
    }

    @GetMapping("/{id_owner}/product/add")
    public String addNewProductsToLegalEntity(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show owner {} products.", id_owner);
        AbstractLegalEntity legalEntity = finderService.findLegalEntity(id_owner);
        legalEntity.setProducts(productFinderService.findAllProductsFromLegalEntity(id_owner));
        model.addAttribute("owner", legalEntity);
        return LEGAL_ENTITY_NEW_PRODUCT_LIST;
    }

    @PostMapping("/{id_owner}/product/add")
    public String saveLegalEntityProductList(@ModelAttribute LegalEntity owner, @PathVariable Long id_owner) {
        logger.info("POST: Update owner {} product list.", id_owner);
        adderService.addProducts(owner.getProducts(), id_owner);
        return "redirect:/owner/legal/" + id_owner;
    }

}
