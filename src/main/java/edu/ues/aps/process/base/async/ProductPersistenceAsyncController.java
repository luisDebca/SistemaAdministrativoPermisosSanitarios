package edu.ues.aps.process.base.async;

import edu.ues.aps.process.base.service.ProductPersistenceService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.NoResultException;

@Controller
public class ProductPersistenceAsyncController {

    private ProductPersistenceService persistenceService;

    public ProductPersistenceAsyncController(ProductPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping(value = "/delete-product-by-id", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse deleteProductById(@RequestParam("id") Long id_product) {
        try {
            persistenceService.delete(id_product);
            return new SingleStringResponse().asSuccessResponse();
        } catch (NoResultException e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

}
