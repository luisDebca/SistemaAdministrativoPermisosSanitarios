package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractClientDTO;

import java.util.List;

public interface ClientDTOFinderService {

    AbstractClientDTO find(Long id_client);

    List<AbstractClientDTO> findAll();

    List<AbstractClientDTO> findAll(Long id_owner);

    List<AbstractClientDTO> findAllWithNaturalOwner(Long id_owner);

}
