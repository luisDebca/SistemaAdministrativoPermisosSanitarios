package edu.ues.aps.process.base.service;

public interface CaseFileValidatorService {

    Boolean isUniqueFolderNumber(Long id_caseFile, String number, String year);

    Boolean isUniqueRequestNumber(Long id_caseFile, String code, String number, String year);
}
