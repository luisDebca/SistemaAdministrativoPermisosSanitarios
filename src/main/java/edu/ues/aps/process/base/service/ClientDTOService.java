package edu.ues.aps.process.base.service;

import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.ClientDTO;
import edu.ues.aps.process.base.dto.EmptyClientDTO;
import edu.ues.aps.process.base.model.AbstractNaturalEntity;
import edu.ues.aps.process.base.model.ClientType;
import edu.ues.aps.process.base.repository.ClientDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class ClientDTOService implements ClientDTOFinderService {

    private final SingleOwnerEntityFinderService ownerFinderService;
    private final ClientDTOFinderRepository finderRepository;

    public ClientDTOService(ClientDTOFinderRepository finderRepository, SingleOwnerEntityFinderService ownerFinderService) {
        this.finderRepository = finderRepository;
        this.ownerFinderService = ownerFinderService;
    }

    @Override
    public AbstractClientDTO find(Long id_client) {
        try {
            return finderRepository.find(id_client);
        } catch (NoResultException e) {
            return new EmptyClientDTO();
        }
    }

    @Override
    public List<AbstractClientDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public List<AbstractClientDTO> findAll(Long id_owner) {
        return finderRepository.findAll(id_owner);
    }

    @Override
    public List<AbstractClientDTO> findAllWithNaturalOwner(Long id_owner) {
        List<AbstractClientDTO> dtos = new ArrayList<>();
        AbstractClientDTO ownerAsClientDTO = getNaturalOwnerAsClientDTO(id_owner);
        if(ownerAsClientDTO.getId_client() != 0L){
            dtos.add(ownerAsClientDTO);
        }
        dtos.addAll(finderRepository.findAll(id_owner));
        return  dtos;
    }

    private AbstractClientDTO getNaturalOwnerAsClientDTO(Long id_owner){
        AbstractNaturalEntity naturalEntity = ownerFinderService.findNaturalEntity(id_owner);
        ClientDTO dto = new ClientDTO();
        dto.setId_client(naturalEntity.getId());
        dto.setClient_name(naturalEntity.getName());
        dto.setType(ClientType.PROPIETARIO);
        dto.setClientType(ClientType.PROPIETARIO);
        dto.setClient_DUI(naturalEntity.getDUI());
        dto.setClient_nationally(naturalEntity.getNationally());
        dto.setClient_email(naturalEntity.getEmail());
        dto.setClient_telephone(naturalEntity.getTelephone());
        dto.setClient_passport("");
        dto.setClient_resident_card(" ");
        return dto;
    }
}
