package edu.ues.aps.process.base.dto;

public class MapDTO implements AbstractMapDTO {

    private Long id;

    private String value;

    public MapDTO(Long id, String value) {
        this.id = id;
        this.value = value;
    }

    public Long getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
}
