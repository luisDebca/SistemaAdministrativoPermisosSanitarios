package edu.ues.aps.process.base.model;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "PLACE")
public class Place implements AbstractPlace{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 150, nullable = false)
    private String organization_name;

    @Column(length = 150, nullable = false)
    private String section_name;

    @Column(length = 1000)
    private String business_description;

    @Column(length = 500, nullable = false)
    private String address;

    @Column(length = 75,nullable = false)
    private String municipality;

    @Column(length = 50, nullable = false)
    private String department;

    @Column(length = 2)
    private String department_code;

    @Column(length = 25)
    private String zone;

    @Column(length = 15)
    private String telephone;

    @Column(length = 150)
    private String email;

    @Column(length = 15)
    private String fax;

    @Column(length = 100)
    private String availability;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getOrganization_name() {
        return organization_name;
    }

    @Override
    public void setOrganization_name(String organization_name) {
        this.organization_name = organization_name;
    }

    @Override
    public String getSection_name() {
        return section_name;
    }

    @Override
    public void setSection_name(String section_name) {
        this.section_name = section_name;
    }

    @Override
    public String getBusiness_description() {
        return business_description;
    }

    @Override
    public void setBusiness_description(String business_description) {
        this.business_description = business_description;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String getMunicipality() {
        return municipality;
    }

    @Override
    public void setMunicipality(String municipality) {
        this.municipality = municipality;
    }

    @Override
    public String getDepartment() {
        return department;
    }

    @Override
    public void setDepartment(String department) {
        this.department = department;
    }

    @Override
    public String getDepartment_code() {
        return department_code;
    }

    @Override
    public void setDepartment_code(String department_code) {
        this.department_code = department_code;
    }

    @Override
    public String getZone() {
        return zone;
    }

    @Override
    public void setZone(String zone) {
        this.zone = zone;
    }

    @Override
    public String getTelephone() {
        return telephone;
    }

    @Override
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getFax() {
        return fax;
    }

    @Override
    public void setFax(String fax) {
        this.fax = fax;
    }

    public String getAvailability() {
        return availability;
    }

    public void setAvailability(String availability) {
        this.availability = availability;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Place place = (Place) o;
        return Objects.equals(id, place.id) &&
                Objects.equals(organization_name, place.organization_name) &&
                Objects.equals(section_name, place.section_name) &&
                Objects.equals(business_description, place.business_description) &&
                Objects.equals(availability, place.availability);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, organization_name, section_name, business_description, availability);
    }

    @Override
    public String toString() {
        return "Place{" +
                "id=" + id +
                ", organization_name='" + organization_name + '\'' +
                ", section_name='" + section_name + '\'' +
                ", business_description='" + business_description + '\'' +
                ", address='" + address + '\'' +
                ", municipality='" + municipality + '\'' +
                ", department='" + department + '\'' +
                ", department_code='" + department_code + '\'' +
                ", zone='" + zone + '\'' +
                ", telephone='" + telephone + '\'' +
                ", email='" + email + '\'' +
                ", fax='" + fax + '\'' +
                ", disponibility='" + availability + '\'' +
                '}';
    }
}
