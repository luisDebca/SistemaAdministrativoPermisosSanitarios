package edu.ues.aps.process.base.repository;

public interface ClientValidatorRepository {

    Long isDUIUnique(Long id_client, String dui);

    Long isPassportUnique(Long id_client, String passport);

    Long isResidentCard(Long id_client, String rc);

    Long isDUIUnique(String dui);

    Long isPassportUnique(String passport);

    Long isResidentCard(String rc);
}
