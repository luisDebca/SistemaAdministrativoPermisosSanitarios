package edu.ues.aps.process.area.establishment.dto;

public class EmptyEstablishmentDTO implements AbstractEstablishmentDTO {

    @Override
    public Long getEstablishment_id() {
        return 0L;
    }

    @Override
    public Long getOwner_id() {
        return 0L;
    }

    @Override
    public String getEstablishment_name() {
        return "Establishment name not found";
    }

    @Override
    public String getEstablishment_owner_name() {
        return "Establishment owner not found";
    }

    @Override
    public String getEstablishment_type() {
        return "Establishment type not found";
    }

    @Override
    public String getEstablishment_type_detail() {
        return "Establishment type detail not found";
    }

    @Override
    public String getCommercial_register() {
        return "Establishment commercial register not found";
    }

    @Override
    public Double getEstablishment_capital() {
        return 0.0;
    }

    @Override
    public String getEstablishment_nit() {
        return "Establishment NIT not found";
    }

    @Override
    public String getEstablishment_email() {
        return "Establishment email not found";
    }

    @Override
    public String getEstablishment_telephone() {
        return "Establishment telephone not found";
    }

    @Override
    public String getEstablishment_fax() {
        return "Establishment FAX not found";
    }

    @Override
    public Integer getEstablishment_male_employee_count() {
        return 0;
    }

    @Override
    public Integer getEstablishment_female_employee_count() {
        return 0;
    }

    @Override
    public String getEstablishment_operations() {
        return "Establishment operations not found";
    }

    @Override
    public String getEstablishment_address() {
        return "Establishment address not found";
    }

    @Override
    public String getEstablishment_telephone_extra() {
        return "Establishment telephone not found";
    }

    @Override
    public Boolean isActive() {
        return false;
    }
}
