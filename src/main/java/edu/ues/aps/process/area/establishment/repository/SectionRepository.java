package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class SectionRepository implements SectionDTOFinderRepository {

    private SessionFactory sessionFactory;

    public SectionRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractMapDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.MapDTO(section.id,section.type) " +
                "from Section section", AbstractMapDTO.class)
                .getResultList();
    }
}
