package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.base.dto.AbstractSummaryDTO;
import edu.ues.aps.process.area.base.repository.SummaryCountRepository;
import edu.ues.aps.process.area.base.repository.SummaryDTOFinderRepository;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class VehicleSummaryDTOFinderRepository implements SummaryDTOFinderRepository, SummaryCountRepository {

    private final SessionFactory sessionFactory;

    public VehicleSummaryDTOFinderRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public CasefileSection appliesTo() {
        return CasefileSection.VEHICLE;
    }

    @Override
    public Long getCount(ProcessIdentifier processIdentifier, LocalDateTime start, LocalDateTime end) {
        return (long) sessionFactory.getCurrentSession().createQuery("select count(bunch) " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "and record.start_on > :start " +
                "and record.start_on < :end " +
                "group by bunch.id", Long.class)
                .setParameter("id", processIdentifier)
                .setParameter("start", start)
                .setParameter("end", end)
                .getResultList().size();
    }

    @Override
    public List<AbstractSummaryDTO> find(ProcessIdentifier processIdentifier, LocalDateTime start, LocalDateTime end) {
        List<AbstractSummaryDTO> resultList = sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.base.dto.SummaryDTO(" +
                "bunch.id,concat(concat(cv.request_folder,'-'),cv.request_year),owner.name,vehicle.license," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name) ) " +
                "from VehicleBunch bunch " +
                "left join bunch.address address " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "and record.start_on > :start " +
                "and record.start_on < :end " +
                "group by bunch.id " +
                "order by bunch.id desc ", AbstractSummaryDTO.class)
                .setParameter("id", processIdentifier)
                .setParameter("start", start)
                .setParameter("end", end)
                .setMaxResults(5)
                .getResultList();
        for (AbstractSummaryDTO dto : resultList) {
            dto.setTarget(getLicenseList(dto.getId()));
        }
        return resultList;
    }

    private String getLicenseList(Long id_bunch) {
        StringBuilder result = new StringBuilder();
        List<String> licenses = sessionFactory.getCurrentSession().createQuery("select vehicle.license " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "where bunch.id = :id", String.class)
                .setParameter("id", id_bunch)
                .getResultList();
        for (String license : licenses) {
            result.append(license).append(", ");
        }
        return result.toString();
    }
}
