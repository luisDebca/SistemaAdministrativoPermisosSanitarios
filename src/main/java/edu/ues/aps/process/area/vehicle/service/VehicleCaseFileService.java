package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.base.repository.CaseFileRequestCountRepository;
import edu.ues.aps.process.area.vehicle.dto.VehicleBunchFormDTO;
import edu.ues.aps.process.area.vehicle.model.*;
import edu.ues.aps.process.area.vehicle.repository.VehicleBunchPersistenceRepository;
import edu.ues.aps.process.area.vehicle.repository.VehiclePersistenceRepository;
import edu.ues.aps.process.base.model.*;
import edu.ues.aps.process.base.service.OwnerFinderService;
import edu.ues.aps.process.base.service.SingleCaseFileFinderService;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.record.base.service.CaseFileProcessRegisterService;
import org.apache.commons.lang.RandomStringUtils;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

@Service
@Transactional
public class VehicleCaseFileService implements VehicleCaseFilePersistenceService, VehicleBunchCaseFilePersistenceService {

    private final VehicleBunchInformationService bunchInformationService;
    private final SingleCaseFileFinderService caseFileFinderService;
    private final VehicleBunchPersistenceRepository bunchRepository;
    private final VehiclePersistenceRepository vehicleRepository;
    private final CaseFileRequestCountRepository countRepository;
    private final CaseFileProcessRegisterService registerService;
    private final OwnerFinderService ownerFinderService;

    private VehicleBunch vehicleBunch;
    private String folder_number;

    public VehicleCaseFileService(VehicleBunchPersistenceRepository bunchRepository, VehiclePersistenceRepository vehicleRepository, OwnerFinderService ownerFinderService, CaseFileProcessRegisterService registerService, @Qualifier("caseFileVehicleStoreProcedureRepository") CaseFileRequestCountRepository countRepository, VehicleBunchInformationService bunchInformationService, SingleCaseFileFinderService caseFileFinderService) {
        this.bunchRepository = bunchRepository;
        this.vehicleRepository = vehicleRepository;
        this.ownerFinderService = ownerFinderService;
        this.registerService = registerService;
        this.countRepository = countRepository;
        this.bunchInformationService = bunchInformationService;
        this.caseFileFinderService = caseFileFinderService;
    }

    @Override
    public void saveBunch(VehicleBunchFormDTO dto) {
        makeBunch(dto);
        getNewFolderNumber();
        addAllVehiclesToNewBunch(dto.getSelected_vehicles());
        addAllNewsVehiclesToBunch(dto.getVehicle_count() - dto.getSelected_vehicles().size(), dto.getId_owner());
    }

    @Override
    public void updateAllCaseFiles(Long id_bunch, Casefile draft) {
        for (Long id_caseFile : bunchInformationService.findAllCaseFileId(id_bunch)) {
            AbstractCasefile caseFile = caseFileFinderService.find(id_caseFile);
            caseFile.setValidate(false);
            caseFile.setApply_payment(draft.getApply_payment());
            caseFile.setRequest_code(draft.getRequest_code());
            caseFile.setRequest_year(draft.getRequest_year());
            caseFile.setCertification_duration_in_years(draft.getCertification_duration_in_years());
            caseFile.setCertification_type(draft.getCertification_type());
            caseFile.setUcsf(draft.getUcsf());
        }
    }

    private void makeBunch(VehicleBunchFormDTO dto) {
        vehicleBunch = new VehicleBunch();
        vehicleBunch.setClient_presented_name(dto.getClient_presented_name());
        vehicleBunch.setClient_presented_telephone(dto.getClient_presented_telephone());
        vehicleBunch.setDistribution_company(dto.getDistribution_company());
        vehicleBunch.setDistribution_destination(dto.getDistribution_destination());
        vehicleBunch.setMarketing_place(dto.getMarketing_place());
        vehicleBunch.setVehicle_count(dto.getVehicle_count());
        vehicleBunch.setAddress(dto.getAddress());
        bunchRepository.save(vehicleBunch);
    }

    private void getNewFolderNumber() {
        folder_number = String.valueOf(countRepository.getMaxRequestFolderCount() + 1);
    }

    private void addAllVehiclesToNewBunch(List<Long> vehicles) {
        for (Long id_vehicle : vehicles) {
            AbstractVehicle vehicle = vehicleRepository.findByID(id_vehicle);
            addVehicle(vehicle);
        }
    }

    private void addAllNewsVehiclesToBunch(Integer vehicles_number, Long id_owner) {
        AbstractOwner owner = ownerFinderService.findById(id_owner);
        for (int i = 0; i < vehicles_number; i++) {
            Vehicle vehicle = makeTemporalVehicle();
            owner.addVehicle(vehicle);
            vehicleRepository.save(vehicle);
            addVehicle(vehicle);
        }
    }

    private void addVehicle(AbstractVehicle vehicle) {
        CasefileVehicle caseFile = makeCaseFile();
        assignRequestCode(vehicle.getTransported_content(),caseFile);
        vehicle.addCasefile(caseFile);
        vehicleBunch.addCasefile(caseFile);
        registerService.addProcess(caseFile, ProcessIdentifier.PROCESS_START);
    }

    private void assignRequestCode(TransportedContentType type, Casefile cf){
        if (!type.equals(TransportedContentType.INVALID)){
            switch (type){
                case PERISHABLE:
                    cf.setRequest_code("TAP");
                    break;
                case NON_PERISHABLE:
                    cf.setRequest_code("TANP");
                    break;
                    default:
                        cf.setRequest_code("");
            }
        }
    }

    private Vehicle makeTemporalVehicle() {
        Vehicle tempVehicle = new Vehicle();
        tempVehicle.setActive(false);
        tempVehicle.setVehicle_type("");
        tempVehicle.setEmployees_number(0);
        tempVehicle.setLicense(RandomStringUtils.randomAlphanumeric(10));
        tempVehicle.setTransported_content(TransportedContentType.INVALID);
        return tempVehicle;
    }

    private CasefileVehicle makeCaseFile() {
        CasefileVehicle case_file = new CasefileVehicle();
        case_file.setApply_payment(true);
        case_file.setValidate(false);
        case_file.setUcsf_delivery(false);
        case_file.setRequest_folder(folder_number);
        case_file.setSection(CasefileSection.VEHICLE);
        case_file.setCertification_duration_in_years(1);
        case_file.setCreation_date(LocalDateTime.now());
        case_file.setStatus(CasefileStatusType.IN_PROCESS);
        case_file.setCertification_type(CertificationType.FIRST);
        case_file.setRequest_year(String.valueOf(LocalDateTime.now().getYear() - 2000));
        return case_file;
    }
}
