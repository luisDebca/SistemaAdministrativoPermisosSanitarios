package edu.ues.aps.process.area.base.service;

import edu.ues.aps.process.area.base.dto.AbstractRequestProcessIndexSummaryDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;

import java.util.List;

public interface IndexSummaryService {

    List<AbstractRequestProcessIndexSummaryDTO> findAllFromToday(ProcessIdentifier identifier);

    List<AbstractRequestProcessIndexSummaryDTO> findAllFromThisWeek(ProcessIdentifier identifier);

    List<AbstractRequestProcessIndexSummaryDTO> findAllFromThisMonth(ProcessIdentifier identifier);

    Long countFromToday(ProcessIdentifier identifier);

    Long countFromThisWeek(ProcessIdentifier identifier);

    Long countFromThisMonth(ProcessIdentifier identifier);
}
