package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.VehicleFoldersReportDTO;
import edu.ues.aps.process.area.vehicle.repository.VehicleFoldersReportDTORepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class VehicleFoldersReportClaseService implements VehicleFoldersReportService{

    private final VehicleFoldersReportDTORepository dataRepository;

    public VehicleFoldersReportClaseService(VehicleFoldersReportDTORepository dataRepository) {
        this.dataRepository = dataRepository;
    }

    @Override
    public VehicleFoldersReportDTO findDataReport(Long id_bunch) {
        return dataRepository.findDataReport(id_bunch);
    }
}
