package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.base.dto.AbstractMapDTO;

import java.util.List;

public interface SectionDTOFinderService {

    List<AbstractMapDTO> findAll();
}
