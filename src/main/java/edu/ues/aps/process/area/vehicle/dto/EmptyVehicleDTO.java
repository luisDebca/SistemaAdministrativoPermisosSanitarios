package edu.ues.aps.process.area.vehicle.dto;

public class EmptyVehicleDTO implements AbstractVehicleDTO {

    @Override
    public Long getId_vehicle() {
        return 0L;
    }

    @Override
    public Long getId_owner() {
        return 0L;
    }

    @Override
    public String getVehicle_license() {
        return "0000-0000";
    }

    @Override
    public Integer getVehicle_employees_number() {
        return 0;
    }

    @Override
    public String getVehicle_transported_content() {
        return "Transported content not found";
    }

    @Override
    public String getVehicle_type() {
        return "Vehicle type not found";
    }

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public Boolean getActive() {
        return false;
    }
}
