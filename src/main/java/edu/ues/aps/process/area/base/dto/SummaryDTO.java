package edu.ues.aps.process.area.base.dto;

public class SummaryDTO implements AbstractSummaryDTO {

    private Long id;
    private String folder;
    private String request;
    private String resolution;
    private String owner;
    private String target;
    private String address;

    public SummaryDTO(Long id, String folder, String request, String resolution, String owner, String target, String address) {
        this.id = id;
        this.folder = folder;
        this.request = request;
        this.resolution = resolution;
        this.owner = owner;
        this.target = target;
        this.address = address;
    }

    public SummaryDTO(Long id, String folder, String owner, String target, String address) {
        this.id = id;
        this.folder = folder;
        this.owner = owner;
        this.target = target;
        this.address = address;
    }

    public Long getId() {
        return id;
    }

    public String getFolder() {
        return folder;
    }

    public String getRequest() {
        if (request == null) return "N/A";
        if (request.isEmpty()) return "N/A";
        return request;
    }

    public String getResolution() {
        if (resolution == null) return "N/A";
        if (resolution.isEmpty()) return "N/A";
        return resolution;
    }

    public String getOwner() {
        return owner;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getAddress() {
        return address;
    }
}
