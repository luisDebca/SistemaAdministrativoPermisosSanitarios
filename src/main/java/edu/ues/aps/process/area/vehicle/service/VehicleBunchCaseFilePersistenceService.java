package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.base.model.Casefile;

public interface VehicleBunchCaseFilePersistenceService {

    void updateAllCaseFiles(Long id_bunch, Casefile caseFile);
}
