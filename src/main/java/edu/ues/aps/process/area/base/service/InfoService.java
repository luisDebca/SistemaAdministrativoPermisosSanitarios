package edu.ues.aps.process.area.base.service;


import edu.ues.aps.process.area.base.dto.AbstractInfo;
import edu.ues.aps.process.area.base.dto.EmptyInfoDTO;
import edu.ues.aps.process.area.base.repository.InfoFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional(readOnly = true)
public class InfoService implements InfoFinderService {

    private final InfoFinderRepository finderRepository;

    public InfoService(InfoFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractInfo getInfo(Long id_caseFile) {
        try {
            AbstractInfo info = finderRepository.getInfo(id_caseFile);
            info.setId(id_caseFile);
            return info;
        } catch (NoResultException e) {
            return new EmptyInfoDTO();
        }
    }
}
