package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO;
import edu.ues.aps.process.area.vehicle.dto.EmptyVehicleDTO;
import edu.ues.aps.process.area.vehicle.repository.VehicleBunchRelationshipRepository;
import edu.ues.aps.process.area.vehicle.repository.VehicleDTOFinderRepository;
import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;
import edu.ues.aps.process.base.dto.EmptyLegalEntityDTO;
import edu.ues.aps.process.base.dto.EmptyNaturalEntityDTO;
import edu.ues.aps.process.base.model.LegalEntity;
import edu.ues.aps.process.base.model.NaturalEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true)
public class VehicleDTOService implements VehicleDTOFinderService, VehicleBunchRelationshipService {

    private final VehicleBunchRelationshipRepository relationshipRepository;
    private final VehicleDTOFinderRepository finderRepository;

    public VehicleDTOService(VehicleDTOFinderRepository finderRepository, VehicleBunchRelationshipRepository relationshipRepository) {
        this.finderRepository = finderRepository;
        this.relationshipRepository = relationshipRepository;
    }

    @Override
    public List<AbstractVehicleDTO> findAllFromOwner(Long id_owner) {
        return finderRepository.findAllFromOwner(id_owner);
    }

    @Override
    public List<AbstractVehicleDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public AbstractVehicleDTO findByID(Long id_vehicle) {
        try {
            return finderRepository.findByID(id_vehicle);
        } catch (NoResultException e) {
            return new EmptyVehicleDTO();
        }
    }

    @Override
    public List<AbstractVehicleDTO> findByPath(String path) {
        return finderRepository.findByPath(path);
    }

    @Override
    public List<AbstractVehicleDTO> findAllVehicles(Long id_bunch) {
        return finderRepository.findAllVehicles(id_bunch);
    }

    @Override
    public List<AbstractClientDTO> findAllClientsWithNaturalOwner(Long id_bunch) {
        return relationshipRepository.findAllClientsWithNaturalOwner(id_bunch);
    }

    @Override
    public List<AbstractClientDTO> findAllClients(Long id_bunch) {
        return relationshipRepository.findAllClients(id_bunch);
    }

    @Override
    public List<AbstractCaseFileDTO> findAllCaseFiles(Long id_bunch) {
        return relationshipRepository.findAllCaseFiles(id_bunch);
    }

    @Override
    public Map<String,AbstractOwnerEntityDTO> findOwner(Long id_bunch) {
        Map<String,AbstractOwnerEntityDTO> map = new HashMap<>();
        map.put(LegalEntity.class.getSimpleName(), getLegalOwner(id_bunch));
        map.put(NaturalEntity.class.getSimpleName(), getNaturalOwner(id_bunch));
        return map;
    }

    private AbstractOwnerEntityDTO getLegalOwner(Long id_bunch) {
        try {
            return relationshipRepository.findLegalOwner(id_bunch);
        } catch (NoResultException e) {
            return new EmptyLegalEntityDTO();
        }
    }

    private AbstractOwnerEntityDTO getNaturalOwner(Long id_bunch) {
        try {
            return relationshipRepository.findNaturalOwner(id_bunch);
        } catch (NoResultException e) {
            return new EmptyNaturalEntityDTO();
        }
    }
}
