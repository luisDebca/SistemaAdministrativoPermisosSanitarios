package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.model.AbstractEstablishment;

import javax.persistence.NoResultException;

public interface EstablishmentFinderRepository {

    AbstractEstablishment findById(Long id_establishment) throws NoResultException;

}
