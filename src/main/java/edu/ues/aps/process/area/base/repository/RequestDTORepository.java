package edu.ues.aps.process.area.base.repository;

import edu.ues.aps.process.area.base.dto.AbstractRequestDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class RequestDTORepository implements RequestDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public RequestDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractRequestDTO find(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.base.dto.RequestDTO(" +
                "concat(concat(cf.request_folder,'-'),cf.request_year)," +
                "concat(concat(concat(concat('06',cf.request_code),cf.request_number),'-'),cf.request_year)," +
                "concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year)) " +
                "from Casefile cf " +
                "left join cf.resolution resolution " +
                "where cf.id = :id",AbstractRequestDTO.class)
                .setParameter("id",id_caseFile)
                .getSingleResult();
    }

    @Override
    public List<AbstractRequestDTO> findAll(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.base.dto.RequestDTO(" +
                "concat(concat(cf.request_folder,'-'),cf.request_year)," +
                "concat(concat(concat(concat('06',cf.request_code),cf.request_number),'-'),cf.request_year)," +
                "concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year)) " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cf " +
                "left join cf.resolution resolution " +
                "where bunch.id = :id",AbstractRequestDTO.class)
                .setParameter("id",id_bunch)
                .getResultList();
    }
}
