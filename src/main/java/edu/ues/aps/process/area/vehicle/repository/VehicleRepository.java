package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.model.AbstractVehicle;
import edu.ues.aps.process.area.vehicle.model.Vehicle;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class VehicleRepository implements VehiclePersistenceRepository, VehicleValidatorRepository{

    private final SessionFactory sessionFactory;

    public VehicleRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractVehicle findByID(Long id_vehicle) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select vehicle " +
                "from Vehicle vehicle " +
                "where vehicle.id = :id",AbstractVehicle.class)
                .setParameter("id",id_vehicle)
                .getSingleResult();
    }

    @Override
    public void save(AbstractVehicle vehicle) {
        sessionFactory.getCurrentSession().persist(vehicle);
    }

    @Override
    public void delete(AbstractVehicle vehicle) {
        sessionFactory.getCurrentSession().delete(vehicle);
    }

    @Override
    public Long isLicenseUnique(String license) {
        return sessionFactory.getCurrentSession().createQuery("select count(vehicle.license) " +
                "from Vehicle vehicle " +
                "where vehicle.license = :lic", Long.class)
                .setParameter("lic",license)
                .getSingleResult();
    }

    @Override
    public Boolean isLicenseEquals(String license, Long id_vehicle) {
        return sessionFactory.getCurrentSession().createQuery("select " +
                "case when vehicle.license = :lic then true else false end " +
                "from Vehicle vehicle " +
                "where vehicle.id = :id",Boolean.class)
                .setParameter("id",id_vehicle)
                .setParameter("lic",license)
                .getSingleResult();
    }
}
