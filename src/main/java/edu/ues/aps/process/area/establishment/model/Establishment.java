package edu.ues.aps.process.area.establishment.model;

import edu.ues.aps.process.base.model.Address;
import edu.ues.aps.process.base.model.OwnerEntity;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.Digits;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "ESTABLISHMENT")
@DynamicUpdate
public class Establishment implements AbstractEstablishment {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name", nullable = false, length = 100)
    private String name;

    @Column(name = "commercial_register", length = 25)
    private String commercial_register;

    @Column(name = "capital")
    @Digits(integer = 10, fraction = 2)
    private Double capital;

    @Column(name = "NIT", length = 17)
    private String NIT;

    @Column(name = "telephone", length = 15)
    private String telephone;

    @Column(name = "telephone_extra", length = 15)
    private String telephone_extra;

    @Column(name = "FAX", length = 15)
    private String FAX;

    @Column(name = "email", length = 100)
    private String email;

    @Column(name = "male_employee_count")
    private Integer maleEmployeeCount;

    @Column(name = "female_employee_count")
    private Integer femaleEmployeeCount;

    @Column(name = "operations", length = 150)
    private String operations;

    @Column(name = "type_detail", length = 50)
    private String type_detail;

    @Column(name = "is_active", nullable = false)
    private boolean active;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_type")
    private EstablishmentType type;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_owner")
    private OwnerEntity owner;

    @OneToMany(mappedBy = "establishment", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CasefileEstablishment> casefiles = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "ESTABLISHMENT_ADDRESS",
            joinColumns = @JoinColumn(name = "id_establishment", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "id_address", referencedColumnName = "id"))
    private Address address;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getName() {
        return name;
    }

    @Override
    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String getCommercial_register() {
        return commercial_register;
    }

    @Override
    public void setCommercial_register(String commercial_register) {
        this.commercial_register = commercial_register;
    }

    @Override
    public Double getCapital() {
        return capital;
    }

    @Override
    public void setCapital(Double capital) {
        this.capital = capital;
    }

    @Override
    public String getNIT() {
        return NIT;
    }

    @Override
    public void setNIT(String NIT) {
        this.NIT = NIT;
    }

    @Override
    public String getTelephone() {
        return telephone;
    }

    @Override
    public void setTelephone(String telephone) {
        this.telephone = telephone;
    }

    @Override
    public String getTelephone_extra() {
        return telephone_extra;
    }

    @Override
    public void setTelephone_extra(String telephone_extra) {
        this.telephone_extra = telephone_extra;
    }

    @Override
    public String getFAX() {
        return FAX;
    }

    @Override
    public void setFAX(String FAX) {
        this.FAX = FAX;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public Integer getMaleEmployeeCount() {
        return maleEmployeeCount;
    }

    @Override
    public void setMaleEmployeeCount(Integer maleEmployeeCount) {
        this.maleEmployeeCount = maleEmployeeCount;
    }

    @Override
    public Integer getFemaleEmployeeCount() {
        return femaleEmployeeCount;
    }

    @Override
    public void setFemaleEmployeeCount(Integer femaleEmployeeCount) {
        this.femaleEmployeeCount = femaleEmployeeCount;
    }

    @Override
    public String getOperations() {
        return operations;
    }

    @Override
    public void setOperations(String operations) {
        this.operations = operations;
    }

    @Override
    public String getType_detail() {
        return type_detail;
    }

    @Override
    public void setType_detail(String type_detail) {
        this.type_detail = type_detail;
    }

    @Override
    public EstablishmentType getType() {
        return type;
    }

    @Override
    public void setType(EstablishmentType type) {
        this.type = type;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public Address getAddress() {
        return address;
    }

    @Override
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public OwnerEntity getOwner() {
        return owner;
    }

    @Override
    public void setOwner(OwnerEntity owner) {
        this.owner = owner;
    }

    @Override
    public List<CasefileEstablishment> getCasefiles() {
        return casefiles;
    }

    @Override
    public void setCasefiles(List<CasefileEstablishment> casefiles) {
        this.casefiles = casefiles;
    }

    @Override
    public void addCasefile(CasefileEstablishment casefile) {
        casefiles.add(casefile);
        casefile.setEstablishment(this);
    }

    @Override
    public void removeCasefile(CasefileEstablishment casefile) {
        casefiles.remove(casefile);
        casefile.setEstablishment(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Establishment that = (Establishment) o;

        return name.equals(that.name) && NIT.equals(that.NIT);
    }

    @Override
    public int hashCode() {
        int result = name.hashCode();
        result = 31 * result + NIT.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Establishment{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", commercial_register='" + commercial_register + '\'' +
                ", capital=" + capital +
                ", NIT='" + NIT + '\'' +
                ", telephone='" + telephone + '\'' +
                ", telephone_extra='" + telephone_extra + '\'' +
                ", FAX='" + FAX + '\'' +
                ", email='" + email + '\'' +
                ", maleEmployeeCount=" + maleEmployeeCount +
                ", femaleEmployeeCount=" + femaleEmployeeCount +
                ", operations='" + operations + '\'' +
                ", type_detail='" + type_detail + '\'' +
                ", active=" + active +
                '}';
    }
}
