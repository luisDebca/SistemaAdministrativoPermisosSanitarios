package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.base.repository.CaseFileRequestCountRepository;
import edu.ues.aps.process.area.establishment.model.*;
import edu.ues.aps.process.area.establishment.repository.EstablishmentFinderRepository;
import edu.ues.aps.process.area.establishment.repository.EstablishmentValidatorRepository;
import edu.ues.aps.process.base.model.*;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.record.base.service.CaseFileProcessRegisterService;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;

@Service
public class EstablishmentService implements EstablishmentPersistenceService, EstablishmentCaseFileAdderService, EstablishmentValidatorService, EstablishmentFinderService {

    private final EstablishmentFinderRepository finderRepository;
    private final CaseFileProcessRegisterService processRegisterService;
    private final EstablishmentValidatorRepository validatorRepository;
    private final CaseFileRequestCountRepository requestCountRepository;

    public EstablishmentService(@Qualifier("caseFileEstablishmentStoredProcedureRepository") CaseFileRequestCountRepository requestCountRepository, EstablishmentValidatorRepository validatorRepository, CaseFileProcessRegisterService processRegisterService, EstablishmentFinderRepository finderRepository) {
        this.requestCountRepository = requestCountRepository;
        this.validatorRepository = validatorRepository;
        this.processRegisterService = processRegisterService;
        this.finderRepository = finderRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractEstablishment findById(Long id_establishment) {
        try {
            return finderRepository.findById(id_establishment);
        } catch (NoResultException e) {
            return new EmptyEstablishment();
        }
    }

    @Override
    @Transactional
    public void setActive(Long id_establishment) {
        AbstractEstablishment establishment = finderRepository.findById(id_establishment);
        establishment.setActive(!establishment.isActive());
    }

    @Override
    @Transactional
    public AbstractCasefile add(CertificationType type, Long id_establishment) {
        try {
            AbstractEstablishment establishment = finderRepository.findById(id_establishment);
            CasefileEstablishment caseFile = buildCaseFile(type);
            caseFile.setRequest_folder(String.valueOf(requestCountRepository.getMaxRequestFolderCount() + 1L));
            establishment.addCasefile(caseFile);
            processRegisterService.addProcess(caseFile, ProcessIdentifier.PROCESS_START);
            return caseFile;
        } catch (NoResultException e) {
            return new EmptyCasefile();
        }
    }

    @Override
    @Transactional
    public AbstractCasefile add(AbstractCasefileEstablishment caseFile, Long id_establishment) {
        try {
            AbstractEstablishment establishment = finderRepository.findById(id_establishment);
            caseFile.setCertification_type(CertificationType.OUTDATED);
            caseFile.setSection(CasefileSection.ESTABLISHMENT);
            establishment.addCasefile((CasefileEstablishment) caseFile);
            processRegisterService.addProcess(caseFile, ProcessIdentifier.PROCESS_START);
            return caseFile;
        } catch (NoResultException e) {
            return new EmptyCasefile();
        }
    }

    @Override
    @Transactional
    public void update(AbstractEstablishment establishment) {
        AbstractEstablishment persistent = finderRepository.findById(establishment.getId());
        persistent.setActive(establishment.isActive());
        persistent.setCapital(establishment.getCapital());
        persistent.setCommercial_register(establishment.getCommercial_register());
        persistent.setEmail(establishment.getEmail());
        persistent.setFAX(establishment.getFAX());
        persistent.setFemaleEmployeeCount(establishment.getFemaleEmployeeCount());
        persistent.setMaleEmployeeCount(establishment.getMaleEmployeeCount());
        persistent.setName(establishment.getName());
        persistent.setNIT(establishment.getNIT());
        persistent.setOperations(establishment.getOperations());
        persistent.setTelephone(establishment.getTelephone());
        persistent.setTelephone_extra(establishment.getTelephone_extra());
        persistent.setType_detail(establishment.getType_detail());
        persistent.setType((EstablishmentType) establishment.getType());
        persistent.getAddress().setDetails(establishment.getAddress().getDetails());
        persistent.getAddress().setMunicipality((Municipality) establishment.getAddress().getMunicipality());
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueName(String name) {
        return validatorRepository.isUniqueName(name).equals(0L);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueAddress(String address) {
        return validatorRepository.isUniqueAddress(address).equals(0L);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueRC(String rc) {
        return validatorRepository.isUniqueRC(rc).equals(0L);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueNIT(String nit) {
        return validatorRepository.isUniqueNIT(nit).equals(0L);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueName(String name, Long id_establishment) {
        if (validatorRepository.isNameEquals(name, id_establishment)) {
            return true;
        } else {
            return validatorRepository.isUniqueName(name) == 0;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueAddress(String address, Long id_establishment) {
        if (validatorRepository.isAddressEquals(address, id_establishment)) {
            return true;
        } else {
            return validatorRepository.isUniqueAddress(address) == 0;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueRC(String rc, Long id_establishment) {
        if (validatorRepository.isRCEquals(rc, id_establishment)) {
            return true;
        } else {
            return validatorRepository.isUniqueRC(rc) == 0;
        }
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueNIT(String nit, Long id_establishment) {
        if (validatorRepository.is_NITEquals(nit, id_establishment)) {
            return true;
        } else {
            return validatorRepository.isUniqueNIT(nit) == 0;
        }
    }

    private CasefileEstablishment buildCaseFile(CertificationType type) {
        CasefileEstablishment caseFile = new CasefileEstablishment();
        caseFile.setCreation_date(LocalDateTime.now());
        caseFile.setApply_payment(true);
        caseFile.setValidate(false);
        caseFile.setUcsf_delivery(false);
        caseFile.setCertification_duration_in_years(3);
        caseFile.setCertification_type(type);
        caseFile.setSection(CasefileSection.ESTABLISHMENT);
        caseFile.setStatus(CasefileStatusType.IN_PROCESS);
        caseFile.setRequest_year(String.valueOf(LocalDateTime.now().getYear() - 2000));
        return caseFile;
    }
}
