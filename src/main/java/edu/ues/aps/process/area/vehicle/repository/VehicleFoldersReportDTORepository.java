package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.dto.VehicleFoldersReportDTO;


public interface VehicleFoldersReportDTORepository {

    VehicleFoldersReportDTO findDataReport(Long id_bunch);
}
