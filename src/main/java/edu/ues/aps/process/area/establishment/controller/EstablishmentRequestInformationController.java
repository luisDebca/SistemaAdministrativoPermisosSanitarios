package edu.ues.aps.process.area.establishment.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/caseFile/establishment")
public class EstablishmentRequestInformationController {

    private static final Logger logger = LogManager.getLogger(EstablishmentRequestInformationController.class);

    @GetMapping("/on-process")
    public String showActiveRequest(){
        return "";
    }

    @GetMapping("/completed")
    public String showCompletedRequest(){
        return "";
    }

    @GetMapping("/rejected")
    public String showRejectedRequest(){
        return "";
    }

    @GetMapping("/cancelled")
    public String showCancelledRequest(){
        return "";
    }

    @GetMapping("/expired")
    public String showExpiredRequest(){
        return "";
    }

}
