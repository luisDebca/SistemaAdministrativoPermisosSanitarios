package edu.ues.aps.process.area.establishment.async;

import edu.ues.aps.process.area.establishment.service.EstablishmentValidatorService;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.Locale;

@Controller
@RequestMapping("/establishment/validator")
public class EstablishmentValidatorController {

    private final MessageSource messageSource;
    private final EstablishmentValidatorService validatorService;

    public EstablishmentValidatorController(EstablishmentValidatorService validatorService, MessageSource messageSource) {
        this.validatorService = validatorService;
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/name", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateEstablishmentNameUnique(@RequestParam String name) {
        return validatorService.isUniqueName(name) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("establishment.name", new String[]{name}, Locale.getDefault()));
    }

    @GetMapping(value = "/name-update", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateEstablishmentNameUnique(@RequestParam String name, @RequestParam Long id_establishment) {
        return validatorService.isUniqueName(name, id_establishment) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("establishment.name", new String[]{name}, Locale.getDefault()));
    }

    @GetMapping(value = "/address", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateEstablishmentAddressUnique(@RequestParam String address) {
        return validatorService.isUniqueAddress(address) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("establishment.address", new String[]{address}, Locale.getDefault()));
    }

    @GetMapping(value = "/address-update", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateEstablishmentAddressUnique(@RequestParam String address, @RequestParam Long id_establishment) {
        return validatorService.isUniqueAddress(address, id_establishment) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("establishment.address", new String[]{address}, Locale.getDefault()));
    }

    @GetMapping(value = "/nit", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateEstablishmentNITUnique(@RequestParam String nit) {
        return validatorService.isUniqueNIT(nit) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("establishment.nit", new String[]{nit}, Locale.getDefault()));
    }

    @GetMapping(value = "/nit-update", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateEstablishmentNITUnique(@RequestParam String nit, @RequestParam Long id_establishment) {
        return validatorService.isUniqueNIT(nit, id_establishment) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("establishment.nit", new String[]{nit}, Locale.getDefault()));
    }

    @GetMapping(value = "/rc", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateEstablishmentRC(@RequestParam String rc) {
        return validatorService.isUniqueRC(rc) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("establishment.rc", new String[]{rc}, Locale.getDefault()));
    }

    @GetMapping(value = "/rc-update", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateEstablishmentRC(@RequestParam String rc, @RequestParam Long id_establishment) {
        return validatorService.isUniqueRC(rc, id_establishment) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("establishment.rc", new String[]{rc}, Locale.getDefault()));
    }
}
