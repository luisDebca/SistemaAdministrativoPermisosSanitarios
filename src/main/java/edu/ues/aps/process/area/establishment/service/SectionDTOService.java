package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.repository.SectionDTOFinderRepository;
import edu.ues.aps.process.base.dto.AbstractMapDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class SectionDTOService implements SectionDTOFinderService {

    private SectionDTOFinderRepository finderRepository;

    public SectionDTOService(SectionDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractMapDTO> findAll() {
        return finderRepository.findAll();
    }
}
