package edu.ues.aps.process.area.establishment.dto;

public interface AbstractCasefileInformationDTO {

    Long getId_owner();

    Long getId_establishment();

    Long getId_casefile();

    boolean getCasefile_apply_payment();

    int getCasefile_duration();

    String getCasefile_start_on();

    String getCasefile_start_on_html();

    String getCasefile_type();

    String getCasefile_code();

    String getCasefile_section();

    String getCasefile_status();

    String getCasefile_date();

    String getSibasi();

    String getUcsf();

    String getEstablishment_name();

    String getOwner_name();

    String getCurrent_process();

    String getCurrent_process_type();

    String getFolder_number();
}
