package edu.ues.aps.process.area.establishment.dto;

public interface AbstractRequestOnProcessValidityReport {

    String getClient_name();

    String getOwner_name();

    String getEstablishment_name();

    String getEstablishment_address();

    String getRequest_process_start();

    String getRequest_number();

    String getRequest_type();

    String getCurrent_date();

    String getUser_name();

    String getUser_charge();

    void setClient_name(String client_name);

    void setUser_name(String user_name);

    void setUser_charge(String user_charge);
}
