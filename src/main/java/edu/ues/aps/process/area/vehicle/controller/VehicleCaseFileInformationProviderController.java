package edu.ues.aps.process.area.vehicle.controller;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchCaseFileDTOFinderService;
import edu.ues.aps.process.area.vehicle.service.VehicleCaseFileDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class VehicleCaseFileInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleCaseFileInformationProviderController.class);

    private static final String CASEFILE_OWNER_LIST = "owner_vehicle_caseFile_list";
    private static final String CASEFILE_LIST = "vehicle_request_list";

    private final VehicleCaseFileDTOFinderService finderService;
    private final VehicleBunchCaseFileDTOFinderService bunchFinderService;

    public VehicleCaseFileInformationProviderController(VehicleBunchCaseFileDTOFinderService bunchFinderService, VehicleCaseFileDTOFinderService finderService) {
        this.bunchFinderService = bunchFinderService;
        this.finderService = finderService;
    }

    @GetMapping("/owner/{id_owner}/vehicle/casefile/list")
    public String showVehicleCaseFileList(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show owner vehicle casefile list");
        model.addAttribute("case_files", bunchFinderService.findAll(id_owner));
        return CASEFILE_OWNER_LIST;
    }

    @GetMapping("/vehicle/{id_vehicle}/caseFile/list")
    public String showAllCaseFilesForVehicle(ModelMap model, @PathVariable Long id_vehicle){
        logger.info("GET:: Show all caseFile for vehicle {}.",id_vehicle);
        model.addAttribute("id_vehicle",id_vehicle);
        model.addAttribute("caseFiles",finderService.findAll(id_vehicle));
        return CASEFILE_LIST;
    }
}
