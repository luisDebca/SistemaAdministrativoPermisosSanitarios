package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.dto.AbstractCasefileInformationDTO;

import java.util.List;

public interface EstablishmentCaseFileInformationDTORepository {

    List<AbstractCasefileInformationDTO> findAll(Long id_establishment);

}
