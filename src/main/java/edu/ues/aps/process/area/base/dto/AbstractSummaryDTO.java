package edu.ues.aps.process.area.base.dto;

public interface AbstractSummaryDTO {

    Long getId();

    String getFolder();

    String getRequest();

    String getResolution();

    String getOwner();

    String getTarget();

    void setTarget(String target);

    String getAddress();
}
