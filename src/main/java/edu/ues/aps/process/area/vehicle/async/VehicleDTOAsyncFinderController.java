package edu.ues.aps.process.area.vehicle.async;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO;
import edu.ues.aps.process.area.vehicle.service.VehicleDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class VehicleDTOAsyncFinderController {

    private static final Logger logger = LogManager.getLogger(VehicleDTOAsyncFinderController.class);

    private final VehicleDTOFinderService finderService;

    public VehicleDTOAsyncFinderController(VehicleDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/vehicle/find-by-path", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractVehicleDTO> findAllVehicleWithPath(@RequestParam String path){
        logger.info("GET:: Find all vehicle by path {}",path);
        return finderService.findByPath(path);
    }

    @GetMapping(value = "/vehicle/find-by-id", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AbstractVehicleDTO findVehicle(@RequestParam Long id_vehicle){
        logger.info("GET:: Find vehicle by id {}",id_vehicle);
        return finderService.findByID(id_vehicle);
    }
}
