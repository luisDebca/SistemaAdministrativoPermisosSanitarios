package edu.ues.aps.process.area.establishment.model;

import java.util.Collections;
import java.util.List;

public class EmptyEstablishmentType implements AbstractEstablishmentType {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getType() {
        return "Establishment type not found";
    }

    @Override
    public AbstractSection getSection() {
        return new EmptySection();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setType(String type) {

    }

    @Override
    public void setSection(Section section) {

    }

    @Override
    public void setEstablishments(List<Establishment> establishments) {

    }

    @Override
    public List<Establishment> getEstablishments() {
        return Collections.emptyList();
    }

    @Override
    public String toString() {
        return "EmptyEstablishmentType{}";
    }
}
