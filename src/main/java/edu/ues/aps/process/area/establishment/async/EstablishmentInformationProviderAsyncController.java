package edu.ues.aps.process.area.establishment.async;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO;
import edu.ues.aps.process.area.establishment.service.EstablishmentDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class EstablishmentInformationProviderAsyncController {

    private static final Logger logger = LogManager.getLogger(EstablishmentInformationProviderAsyncController.class);

    private EstablishmentDTOFinderService finderService;

    public EstablishmentInformationProviderAsyncController(EstablishmentDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/find-establishment-full-details", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AbstractEstablishmentDTO findEstablishmentFullDetails(@RequestParam("id") Long id_establishment) {
        logger.info("GET:: Find establishment {} details",id_establishment);
        return finderService.findById(id_establishment);
    }
}
