package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO;
import edu.ues.aps.process.area.vehicle.repository.VehicleCaseFileDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleCaseFileDTOService implements VehicleCaseFileDTOFinderService {

    private final VehicleCaseFileDTOFinderRepository finderRepository;

    public VehicleCaseFileDTOService(VehicleCaseFileDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractVehicleCaseFileDTO> findAll(Long id_vehicle) {
        return finderRepository.findAll(id_vehicle);
    }
}
