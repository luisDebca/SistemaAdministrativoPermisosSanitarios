package edu.ues.aps.process.area.base.service;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;

public interface SummaryCountService {

    Long getTodayCount(ProcessIdentifier identifier, CasefileSection section);

    Long getWeekCount(ProcessIdentifier identifier, CasefileSection section);

    Long getMonthCount(ProcessIdentifier identifier, CasefileSection section);
}
