package edu.ues.aps.process.area.base.service;

import edu.ues.aps.process.area.base.dto.AbstractInfo;

public interface InfoFinderService {

    AbstractInfo getInfo(Long id_caseFile);
}
