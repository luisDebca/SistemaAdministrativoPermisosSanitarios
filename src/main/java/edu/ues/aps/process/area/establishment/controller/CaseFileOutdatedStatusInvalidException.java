package edu.ues.aps.process.area.establishment.controller;

public class CaseFileOutdatedStatusInvalidException extends RuntimeException{

    public CaseFileOutdatedStatusInvalidException() {
    }

    public CaseFileOutdatedStatusInvalidException(String s) {
        super(s);
    }
}
