package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.dto.AbstractRequestOnProcessValidityReport;

public interface RequestProcessValidatorReportService {

    AbstractRequestOnProcessValidityReport getReportDataSource(Long id_caseFile);
}
