package edu.ues.aps.process.area.establishment.dto;

public interface AbstractEstablishmentRequestDTO {

    Long getId_caseFile();

    void setId_caseFile(Long id_caseFile);

    Long getId_owner();

    void setId_owner(Long id_owner);

    Long getId_establishment();

    void setId_establishment(Long id_establishment);

    String getFolder_number();

    String getRequest_number();

    String getEstablishment_name();

    String getEstablishment_type_detail();

    String getEstablishment_type();

    String getEstablishment_section();

    String getEstablishment_address();

    String getEstablishment_nit();

    String getUCSF();

    String getSIBASI();

    String getCertification_type();

    String getCaseFile_createOn();

    String getOwner_name();
}
