package edu.ues.aps.process.area.base.repository;

import edu.ues.aps.process.base.model.CasefileSection;

public interface CaseFileRequestCountRepository {

    Long getMaxRequestNumberCount();

    Long getMaxRequestFolderCount();

    CasefileSection appliesTo();

    default Long validateQueryResult(Object result){
        if(result != null){
            return (Long) result;
        }else{
            return 0L;
        }
    }

}
