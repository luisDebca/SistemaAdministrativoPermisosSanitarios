package edu.ues.aps.process.area.base.repository;

import edu.ues.aps.process.area.base.dto.AbstractInfo;
import edu.ues.aps.process.base.model.CasefileSection;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class InfoRepository implements InfoFinderRepository {

    private final SessionFactory sessionFactory;

    public InfoRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractInfo getInfo(Long id_caseFile) throws NoResultException {
        CasefileSection section = getSection(id_caseFile);
        if (section.equals(CasefileSection.ESTABLISHMENT)){
            return sessionFactory.getCurrentSession().createQuery("select new " +
                    "edu.ues.aps.process.area.base.dto.InfoDTO(" +
                    "concat(concat(cf.request_folder,'-'),cf.request_year)," +
                    "concat(concat(concat(concat('06',cf.request_code),cf.request_number),'-'),cf.request_year)," +
                    "cf.section,cf.certification_type,cf.certification_start_on,cf.creation_date," +
                    "cf.status,ucsf.name,sibasi.zone, cf.certification_duration_in_years,cf.apply_payment," +
                    "process.id, process.process,establishment.id,establishment.name,owner.name) " +
                    "from CasefileEstablishment cf " +
                    "left join cf.ucsf ucsf " +
                    "left join ucsf.sibasi sibasi " +
                    "inner join cf.establishment establishment " +
                    "inner join establishment.owner owner " +
                    "inner join cf.processes record " +
                    "inner join record.process process " +
                    "where cf.id = :id " +
                    "and record.active = true", AbstractInfo.class)
                    .setParameter("id", id_caseFile)
                    .getSingleResult();
        }else if (section.equals(CasefileSection.VEHICLE)){
            return sessionFactory.getCurrentSession().createQuery("select new " +
                    "edu.ues.aps.process.area.base.dto.InfoDTO(" +
                    "concat(concat(cf.request_folder,'-'),cf.request_year)," +
                    "concat(concat(concat(concat('06',cf.request_code),cf.request_number),'-'),cf.request_year)," +
                    "cf.section,cf.certification_type,cf.certification_start_on,cf.creation_date," +
                    "cf.status,ucsf.name,sibasi.zone, cf.certification_duration_in_years,cf.apply_payment," +
                    "process.id, process.process, vehicle.id, vehicle.license, owner.name) " +
                    "from CasefileVehicle cf " +
                    "left join cf.ucsf ucsf " +
                    "left join ucsf.sibasi sibasi " +
                    "inner join cf.vehicle vehicle " +
                    "inner join vehicle.owner owner " +
                    "inner join cf.processes record " +
                    "inner join record.process process " +
                    "where cf.id = :id " +
                    "and record.active = true", AbstractInfo.class)
                    .setParameter("id", id_caseFile)
                    .getSingleResult();
        }else {
            throw new NoResultException();
        }
    }

    private CasefileSection getSection(Long id_caseFile) throws NoResultException{
        return sessionFactory.getCurrentSession().createQuery("select section from Casefile where id = :id",CasefileSection.class)
                .setParameter("id",id_caseFile)
                .getSingleResult();
    }
}
