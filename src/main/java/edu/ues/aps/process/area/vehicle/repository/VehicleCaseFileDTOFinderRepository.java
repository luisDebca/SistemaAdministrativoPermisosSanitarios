package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO;

import java.util.List;

public interface VehicleCaseFileDTOFinderRepository {

    List<AbstractVehicleCaseFileDTO> findAll(Long id_vehicle);
}
