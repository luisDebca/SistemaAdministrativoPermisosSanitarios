package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.model.AbstractEstablishment;

public interface EstablishmentPersistenceService {

    void setActive(Long id_establishment);

    void update(AbstractEstablishment establishment);
}
