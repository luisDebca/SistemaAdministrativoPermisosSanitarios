package edu.ues.aps.process.area.establishment.dto;

import java.time.LocalDateTime;

public interface AbstractCasefileEstablishmentInfoDTO {

    Long getId_casefile();

    Long getId_establishment();

    Long getId_owner();

    String getCasefile_request_number();

    String getCasefile_request_code();

    String getCasefile_request_year();

    boolean isCasefile_apply_payment();

    String getCasefile_certification_type();

    Integer getCasefile_certification_duration_in_years();

    LocalDateTime getCasefile_certification_start_on();

    String getUcsf_name();

    String getSibasi_name();

    String getCasefile_request_folder();
}
