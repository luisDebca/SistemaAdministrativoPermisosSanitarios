package edu.ues.aps.process.area.establishment.model;

import java.util.List;

public interface AbstractEstablishmentType {

    Long getId();

    void setId(Long id);

    String getType();

    void setType(String type);

    AbstractSection getSection();

    void setSection(Section section);

    List<Establishment> getEstablishments();

    void setEstablishments(List<Establishment> establishments);
}
