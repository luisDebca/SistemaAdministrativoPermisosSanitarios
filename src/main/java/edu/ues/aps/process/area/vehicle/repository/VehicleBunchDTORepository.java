package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleBunchDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class VehicleBunchDTORepository implements VehicleBunchDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleBunchDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractVehicleBunchDTO findById(Long id_bunch) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleBunchDTO(" +
                "owner.id,bunch.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company,bunch.distribution_destination,bunch.marketing_place," +
                "bunch.vehicle_count,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count(vehicle),concat(concat(cv.request_folder,'-'),cv.request_year) ) " +
                "from VehicleBunch bunch " +
                "inner join bunch.address address " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv. vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where bunch.id = :id " +
                "group by bunch.id", AbstractVehicleBunchDTO.class)
                .setParameter("id", id_bunch)
                .getSingleResult();
    }
}
