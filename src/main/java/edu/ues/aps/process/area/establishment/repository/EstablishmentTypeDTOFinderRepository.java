package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.base.dto.AbstractMapDTO;

import java.util.List;

public interface EstablishmentTypeDTOFinderRepository {

    List<AbstractMapDTO> findAll();

    List<AbstractMapDTO> findAllTypesFromSection(Long id_section);
}
