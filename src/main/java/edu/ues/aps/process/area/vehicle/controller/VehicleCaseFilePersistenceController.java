package edu.ues.aps.process.area.vehicle.controller;

import edu.ues.aps.process.area.vehicle.dto.VehicleBunchFormDTO;
import edu.ues.aps.process.area.vehicle.service.VehicleCaseFilePersistenceService;
import edu.ues.aps.process.area.vehicle.service.VehicleDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/owner/{id_owner}/vehicle/casefile")
public class VehicleCaseFilePersistenceController {

    private static final Logger logger = LogManager.getLogger(VehicleCaseFilePersistenceController.class);
    private static final String CASE_FILE_FORM = "casefile_vehicle";
    private final VehicleDTOFinderService finderService;
    private final VehicleCaseFilePersistenceService persistenceService;

    public VehicleCaseFilePersistenceController(VehicleCaseFilePersistenceService persistenceService, VehicleDTOFinderService finderService) {
        this.persistenceService = persistenceService;
        this.finderService = finderService;
    }

    @GetMapping("/new")
    public String addNewVehicleCaseFileBunch(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show new vehicle casefile bunch");
        model.addAttribute("id_owner", id_owner);
        model.addAttribute("bunch", new VehicleBunchFormDTO());
        model.addAttribute("vehicles", finderService.findAllFromOwner(id_owner));
        return CASE_FILE_FORM;
    }

    @PostMapping("/new")
    public String saveVehicleCaseFileBunch(@PathVariable Long id_owner, @ModelAttribute VehicleBunchFormDTO bunch) {
        logger.info("POST:: Save new vehicle casefile bunch");
        bunch.setId_owner(id_owner);
        persistenceService.saveBunch(bunch);
        return String.format("redirect:/owner/%d/vehicle/casefile/list", id_owner);
    }

    @GetMapping("/outdated")
    public String addOutdatedVehicleCaseFileBunch(@PathVariable Long id_owner) {
        return String.format("redirect:/owner/%d/vehicle/list", id_owner);
    }

}
