package edu.ues.aps.process.area.vehicle.report;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchReportFinderService;
import edu.ues.aps.process.base.report.AbstractReportController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
@RequestMapping("/vehicle/bunch")
public class VehicleBunchReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(VehicleBunchReportController.class);

    private static final String REPORT_NAME = "VehicleListReport";

    private final VehicleBunchReportFinderService reportFinderService;

    public VehicleBunchReportController(VehicleBunchReportFinderService reportFinderService) {
        this.reportFinderService = reportFinderService;
    }

    @GetMapping("/vehicle-list-report.pdf")
    public String generateVehicleBunchReport(@RequestParam Long id_bunch, ModelMap model) {
        logger.info("REPORT:: Generate report for vehicle bunch {}.", id_bunch);
        addReportRequiredSources(reportFinderService.findDatasource(id_bunch));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }
}
