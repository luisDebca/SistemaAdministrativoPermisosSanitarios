package edu.ues.aps.process.area.base.dto;

public interface AbstractInfo {

    Long getId();

    void setId(Long id);

    String getFolder();

    String getRequest();

    String getSection();

    String getSectionType();

    String getType();

    String getStart();

    String getCreated();

    String getStatus();

    String getUcsf();

    String getSibasi();

    Integer getDuration();

    Boolean getPayment();

    Long getProcessId();

    String getProcess();

    Long getTarget();

    void setTarget(Long target);

    int getProcessProgress();

    String getEnd();

    double getCertificationProgress();

    long getCertificationDays();

    String getName();

    void setName(String name);

    String getOwner();

    void setOwner(String owner);
}
