package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleBunchDTO;

import javax.persistence.NoResultException;

public interface VehicleBunchDTOFinderRepository {

    AbstractVehicleBunchDTO findById(Long id_bunch) throws NoResultException;
}
