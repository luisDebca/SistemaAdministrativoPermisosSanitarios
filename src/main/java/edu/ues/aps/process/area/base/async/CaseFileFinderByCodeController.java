package edu.ues.aps.process.area.base.async;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.base.service.CaseFileDTOFinderByIdentifierService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class CaseFileFinderByCodeController {

    private static final Logger logger = LogManager.getLogger(CaseFileFinderByCodeController.class);

    private final CaseFileDTOFinderByIdentifierService finderService;

    public CaseFileFinderByCodeController(CaseFileDTOFinderByIdentifierService finderService) {
        this.finderService = finderService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/find-case-file-by-identifier", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractCaseFileDTO> findAllByIdentifier(@RequestParam String identifier){
        logger.info("GET:: Find all caseFile with identifier like {}",identifier);
        return finderService.findAllByIdentifier(identifier);
    }
}
