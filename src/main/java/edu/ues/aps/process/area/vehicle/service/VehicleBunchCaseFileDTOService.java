package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO;
import edu.ues.aps.process.area.vehicle.dto.EmptyVehicleCaseFileDTO;
import edu.ues.aps.process.area.vehicle.repository.VehicleBunchReportFinderRepository;
import edu.ues.aps.process.area.vehicle.repository.VehicleBunchCaseFileDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleBunchCaseFileDTOService implements VehicleBunchCaseFileDTOFinderService, VehicleBunchReportFinderService {

    private final VehicleBunchReportFinderRepository reportFinderRepository;
    private final VehicleBunchCaseFileDTOFinderRepository finderRepository;

    public VehicleBunchCaseFileDTOService(VehicleBunchCaseFileDTOFinderRepository finderRepository, VehicleBunchReportFinderRepository reportFinderRepository) {
        this.finderRepository = finderRepository;
        this.reportFinderRepository = reportFinderRepository;
    }

    @Override
    public AbstractVehicleCaseFileDTO find(Long id_caseFile) {
        try {
            return finderRepository.find(id_caseFile);
        } catch (NoResultException e) {
            return new EmptyVehicleCaseFileDTO();
        }
    }

    @Override
    public List<AbstractVehicleCaseFileDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public List<AbstractVehicleCaseFileDTO> findAll(Long id_owner) {
        List<AbstractVehicleCaseFileDTO> all = finderRepository.findAll(id_owner);
        for (AbstractVehicleCaseFileDTO dto : all) {
            dto.setId_owner(id_owner);
        }
        return all;
    }

    @Override
    public List<AbstractVehicleCaseFileDTO> findAll(Long id_owner, Long id_bunch) {
        return finderRepository.findAll(id_owner, id_bunch);
    }

    @Override
    public List<AbstractVehicleCaseFileDTO> findDatasource(Long id_bunch) {
        return reportFinderRepository.findDatasource(id_bunch);
    }
}
