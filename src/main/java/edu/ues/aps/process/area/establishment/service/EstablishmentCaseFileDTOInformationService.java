package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.dto.AbstractCasefileInformationDTO;

import java.util.List;

public interface EstablishmentCaseFileDTOInformationService {

    List<AbstractCasefileInformationDTO> findAll(Long id_establishment);
}
