package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.base.dto.AbstractRequestProcessIndexSummaryDTO;
import edu.ues.aps.process.area.base.repository.IndexSummaryRepository;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.List;
import java.util.Locale;

@Repository
public class EstablishmentIndexSummaryRepository implements IndexSummaryRepository {

    private final SessionFactory sessionFactory;

    public EstablishmentIndexSummaryRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractRequestProcessIndexSummaryDTO> findAllFromToday(ProcessIdentifier identifier) {
        return findProcessSummaryFrom(LocalDateTime.now().withHour(0).withMinute(0).withMinute(0), identifier);
    }

    @Override
    public List<AbstractRequestProcessIndexSummaryDTO> findAllFromThisWeek(ProcessIdentifier identifier) {
        LocalDate now = LocalDate.now();
        TemporalField fieldISO = WeekFields.of(Locale.getDefault()).dayOfWeek();
        return findProcessSummaryFrom(now.with(fieldISO, 1).atTime(0, 0, 0), identifier);
    }

    @Override
    public List<AbstractRequestProcessIndexSummaryDTO> findAllFromThisMonth(ProcessIdentifier identifier) {
        return findProcessSummaryFrom(LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0), identifier);
    }

    private List<AbstractRequestProcessIndexSummaryDTO> findProcessSummaryFrom(LocalDateTime date, ProcessIdentifier identifier) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.base.dto.RequestProcessIndexSummaryDTO(" +
                "concat(concat(casefile.request_folder,'-'),casefile.request_year),concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year)," +
                "establishment.name,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "owner.name,casefile.certification_type,casefile.creation_date) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.address address " +
                "inner join establishment.owner owner " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where process.identifier = :id " +
                "and record.active = true " +
                "and record.start_on > :thisDate", AbstractRequestProcessIndexSummaryDTO.class)
                .setParameter("id", identifier)
                .setParameter("thisDate", date)
                .setMaxResults(10)
                .getResultList();
    }

    @Override
    public Long countFromToday(ProcessIdentifier identifier) {
        return findProcessCountFrom(LocalDateTime.now().withHour(0).withMinute(0).withMinute(0), identifier);
    }

    @Override
    public Long countFromThisWeek(ProcessIdentifier identifier) {
        LocalDate now = LocalDate.now();
        TemporalField fieldISO = WeekFields.of(Locale.getDefault()).dayOfWeek();
        return findProcessCountFrom(now.with(fieldISO, 1).atTime(0, 0, 0), identifier);
    }

    @Override
    public Long countFromThisMonth(ProcessIdentifier identifier) {
        return findProcessCountFrom(LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0).withSecond(0), identifier);
    }

    private Long findProcessCountFrom(LocalDateTime date, ProcessIdentifier identifier) {
        return sessionFactory.getCurrentSession().createQuery("select count(casefile.id) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where process.identifier = :id " +
                "and record.active = true " +
                "and record.start_on > :thisDate", Long.class)
                .setParameter("id", identifier)
                .setParameter("thisDate", date)
                .getSingleResult();
    }
}
