package edu.ues.aps.process.area.establishment.model;

import edu.ues.aps.process.base.model.AbstractCasefile;

public interface AbstractCasefileEstablishment extends AbstractCasefile {

    AbstractEstablishment getEstablishment();

    void setEstablishment(Establishment establishment);
}
