package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.model.AbstractEstablishment;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class EstablishmentRepository implements EstablishmentFinderRepository, EstablishmentValidatorRepository {

    private final SessionFactory sessionFactory;

    public EstablishmentRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractEstablishment findById(Long id_establishment) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select establishment " +
                "from Establishment establishment " +
                "join fetch establishment.address address " +
                "join fetch address.municipality municipality " +
                "join fetch municipality.department dep " +
                "join fetch establishment.type etype " +
                "join fetch etype.section " +
                "where establishment.id = :id", AbstractEstablishment.class)
                .setParameter("id", id_establishment)
                .getSingleResult();
    }

    @Override
    public Long isUniqueName(String name) {
        return sessionFactory.getCurrentSession().createQuery("select count(establishment.name) " +
                "from Establishment establishment " +
                "where establishment.name like :name", Long.class)
                .setParameter("name", name)
                .getSingleResult();
    }

    @Override
    public Long isUniqueAddress(String address) {
        return sessionFactory.getCurrentSession().createQuery("select count(address.details) " +
                "from Establishment establishment " +
                "inner join establishment.address address " +
                "where address.details like :address", Long.class)
                .setParameter("address", address)
                .getSingleResult();
    }

    @Override
    public Long isUniqueRC(String rc) {
        return sessionFactory.getCurrentSession().createQuery("select count(establishment.commercial_register) " +
                "from Establishment establishment " +
                "where establishment.commercial_register like :rc", Long.class)
                .setParameter("rc", rc)
                .getSingleResult();
    }

    @Override
    public Long isUniqueNIT(String nit) {
        return sessionFactory.getCurrentSession().createQuery("select count(establishment.NIT) " +
                "from Establishment establishment " +
                "where establishment.NIT like :nit", Long.class)
                .setParameter("nit", nit)
                .getSingleResult();
    }

    @Override
    public Boolean isNameEquals(String name, Long id_establishment) {
        return sessionFactory.getCurrentSession().createQuery("select " +
                "case when establishment.name = :name then true else false end " +
                "from Establishment establishment " +
                "where establishment.id = :id", Boolean.class)
                .setParameter("name", name)
                .setParameter("id", id_establishment)
                .getSingleResult();
    }

    @Override
    public Boolean isAddressEquals(String address, Long id_establishment) {
        return sessionFactory.getCurrentSession().createQuery("select " +
                "case when address.details = :address then true else false end " +
                "from Establishment establishment " +
                "inner join establishment.address address " +
                "where establishment.id = :id", Boolean.class)
                .setParameter("address", address)
                .setParameter("id", id_establishment)
                .getSingleResult();
    }

    @Override
    public Boolean isRCEquals(String rc, Long id_establishment) {
        return sessionFactory.getCurrentSession().createQuery("select " +
                "case when establishment.commercial_register = :rc then true else false end " +
                "from Establishment establishment " +
                "where establishment.id = :id", Boolean.class)
                .setParameter("rc", rc)
                .setParameter("id", id_establishment)
                .getSingleResult();
    }

    @Override
    public Boolean is_NITEquals(String nit, Long id_establishment) {
        return sessionFactory.getCurrentSession().createQuery("select " +
                "case when establishment.NIT = :nit then true else false end " +
                "from Establishment establishment " +
                "where establishment.id = :id", Boolean.class)
                .setParameter("nit", nit)
                .setParameter("id", id_establishment)
                .getSingleResult();
    }
}
