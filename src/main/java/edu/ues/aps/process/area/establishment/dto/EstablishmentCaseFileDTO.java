package edu.ues.aps.process.area.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;

public class EstablishmentCaseFileDTO implements AbstractEstablishmentCaseFileDTO{

    private Long id_casefile;
    private Long id_establishment;
    private Long id_owner;
    private String folder_number;
    private String request_code;
    private CertificationType certification_type;
    private String ucsf;
    private String sibasi;
    private String establishment_name;
    private String establishment_type;
    private String establishment_section;
    private String establishment_address;
    private String owner_name;
    private Boolean request_validate;

    public EstablishmentCaseFileDTO(Long id_casefile, Long id_establishment, Long id_owner, String folder_number, String establishment_name, String establishment_type, String establishment_section, String establishment_address, String owner_name, Boolean request_validate) {
        this.id_casefile = id_casefile;
        this.id_establishment = id_establishment;
        this.id_owner = id_owner;
        this.folder_number = folder_number;
        this.establishment_name = establishment_name;
        this.establishment_type = establishment_type;
        this.establishment_section = establishment_section;
        this.establishment_address = establishment_address;
        this.owner_name = owner_name;
        this.request_validate = request_validate;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public void setId_caseFile(Long id_caseFile) {
        this.id_casefile = id_caseFile;
    }

    public Long getId_establishment() {
        return id_establishment;
    }

    public void setId_establishment(Long id_establishment) {
        this.id_establishment = id_establishment;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public void setId_owner(Long id_owner) {
        this.id_owner = id_owner;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public String getRequest_code() {
        return request_code;
    }

    public String getCertification_type() {
        if(certification_type != null){
            return certification_type.getCertificationType();
        }else {
            return CertificationType.INVALID.getCertificationType();
        }
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getSibasi() {
        return sibasi;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_type() {
        return establishment_type;
    }

    public String getEstablishment_section() {
        return establishment_section;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public Boolean isRequest_validate() {
        if(request_validate == null){
            return false;
        }
        return request_validate;
    }
}
