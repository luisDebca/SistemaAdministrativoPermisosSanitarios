package edu.ues.aps.process.area.base.repository;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;

public interface SummaryCountRepository {

    CasefileSection appliesTo();

    Long getCount(ProcessIdentifier processIdentifier, LocalDateTime start, LocalDateTime end) throws NoResultException;
}
