package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.dto.RecordCoverDTO;
import edu.ues.aps.process.area.establishment.repository.RecordCoverFindDataRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class RecordCoverDTOService implements RecordCoverDataService {

    private RecordCoverFindDataRepository recordCoverRepository;

    RecordCoverDTOService(RecordCoverFindDataRepository repository) {
        this.recordCoverRepository = repository;
    }

    @Override
    public RecordCoverDTO findDataRecordCoverReport(Long id_caseFile) {
        return recordCoverRepository.getDataSource(id_caseFile);
    }
}