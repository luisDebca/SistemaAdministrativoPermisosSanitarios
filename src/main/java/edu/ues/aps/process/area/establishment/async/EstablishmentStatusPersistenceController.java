package edu.ues.aps.process.area.establishment.async;

import edu.ues.aps.process.area.establishment.service.EstablishmentPersistenceService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/establishment")
public class EstablishmentStatusPersistenceController {

    private static final Logger logger = LogManager.getLogger(EstablishmentStatusPersistenceController.class);

    private final EstablishmentPersistenceService persistenceService;

    public EstablishmentStatusPersistenceController(EstablishmentPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping(value = "/set-active-status", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setEstablishmentActiveStatus(@RequestParam Long id_establishment) {
        logger.info("GET:: Set establishment {} active status.", id_establishment);
        try {
            persistenceService.setActive(id_establishment);
            return new SingleStringResponse("OK").asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }
}
