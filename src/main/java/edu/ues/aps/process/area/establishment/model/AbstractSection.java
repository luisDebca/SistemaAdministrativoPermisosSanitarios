package edu.ues.aps.process.area.establishment.model;

import java.util.List;

public interface AbstractSection {

    Long getId();

    void setId(Long id);

    String getType();

    void setType(String type);

    List<EstablishmentType> getEstablishmentTypes();

    void setEstablishmentTypes(List<EstablishmentType> establishmentTypes);
}
