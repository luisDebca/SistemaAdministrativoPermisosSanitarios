package edu.ues.aps.process.area.establishment.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "SECTION")
public class Section implements AbstractSection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "type", nullable = false, unique = true, length = 100)
    private String type;

    @OneToMany(mappedBy = "section", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<EstablishmentType> establishmentTypes = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getType() {
        return type;
    }

    @Override
    public void setType(String type) {
        this.type = type;
    }

    @Override
    public List<EstablishmentType> getEstablishmentTypes() {
        return establishmentTypes;
    }

    @Override
    public void setEstablishmentTypes(List<EstablishmentType> establishmentTypes) {
        this.establishmentTypes = establishmentTypes;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Section section = (Section) o;

        return type.equals(section.type);
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    @Override
    public String toString() {
        return "Section{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
