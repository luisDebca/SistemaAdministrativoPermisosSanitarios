package edu.ues.aps.process.area.vehicle.controller;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchDTOFinderService;
import edu.ues.aps.process.area.vehicle.service.VehicleDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class VehicleBunchInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleBunchInformationProviderController.class);

    private static final String BUNCH_INFORMATION = "vehicle_bunch_information";

    private final VehicleDTOFinderService vehicleFinderService;
    private final VehicleBunchDTOFinderService bunchFinderService;

    public VehicleBunchInformationProviderController(VehicleDTOFinderService vehicleFinderService, VehicleBunchDTOFinderService bunchFinderService) {
        this.vehicleFinderService = vehicleFinderService;
        this.bunchFinderService = bunchFinderService;
    }

    @GetMapping("/vehicle/bunch/{id_bunch}/info")
    public String showInformation(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show vehicle request bunch {} information.", id_bunch);
        model.addAttribute("id_bunch", id_bunch);
        model.addAttribute("bunch", bunchFinderService.findById(id_bunch));
        model.addAttribute("vehicles", vehicleFinderService.findAllVehicles(id_bunch));
        return BUNCH_INFORMATION;
    }

}
