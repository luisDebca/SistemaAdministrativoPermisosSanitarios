package edu.ues.aps.process.area.vehicle.model;

public interface AbstractTransportedProduct {

    Long getId();

    void setId(Long id);

    String getProduct();

    void setProduct(String product);

    String getDescription();

    void setDescription(String description);

    AbstractVehicleBunch getBunch();

    void setBunch(VehicleBunch bunch);
}
