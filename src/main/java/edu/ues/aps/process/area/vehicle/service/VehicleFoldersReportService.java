package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.VehicleFoldersReportDTO;


public interface VehicleFoldersReportService {

    VehicleFoldersReportDTO findDataReport(Long id_bunch);

}
