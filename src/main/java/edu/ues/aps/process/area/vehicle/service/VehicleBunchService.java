package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.model.AbstractVehicleBunch;
import edu.ues.aps.process.area.vehicle.model.EmptyVehicleBunch;
import edu.ues.aps.process.area.vehicle.repository.VehicleBunchFinderRepository;
import edu.ues.aps.process.area.vehicle.repository.VehicleBunchPersistenceRepository;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Municipality;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
public class VehicleBunchService implements VehicleBunchPersistenceService, VehicleBunchFinderService {

    private final VehicleBunchFinderRepository finderRepository;
    private final VehicleBunchPersistenceRepository persistenceRepository;

    public VehicleBunchService(VehicleBunchPersistenceRepository persistenceRepository, VehicleBunchFinderRepository finderRepository) {
        this.persistenceRepository = persistenceRepository;
        this.finderRepository = finderRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractVehicleBunch find(Long id_bunch) {
        try {
            return persistenceRepository.find(id_bunch);
        } catch (NoResultException e) {
            return new EmptyVehicleBunch();
        }
    }

    @Override
    @Transactional
    public void update(AbstractVehicleBunch bunch) {
        AbstractVehicleBunch persistent = persistenceRepository.find(bunch.getId());
        persistent.setVehicle_count(bunch.getVehicle_count());
        persistent.setClient_presented_name(bunch.getClient_presented_name());
        persistent.setClient_presented_telephone(bunch.getClient_presented_telephone());
        persistent.setDistribution_company(bunch.getDistribution_company());
        persistent.setDistribution_destination(bunch.getDistribution_destination());
        persistent.setMarketing_place(bunch.getMarketing_place());
        persistent.getAddress().setDetails(bunch.getAddress().getDetails());
        persistent.getAddress().setMunicipality((Municipality) bunch.getAddress().getMunicipality());
    }

    @Override
    @Transactional(readOnly = true)
    public List<AbstractCasefile> findAllRequest(Long id_bunch) {
        return finderRepository.findAllInBunch(id_bunch);
    }

}
