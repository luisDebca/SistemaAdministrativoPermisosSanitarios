package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO;
import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;

import java.util.List;
import java.util.Map;

public interface VehicleBunchRelationshipService {

    List<AbstractVehicleDTO> findAllVehicles(Long id_bunch);

    List<AbstractClientDTO> findAllClientsWithNaturalOwner(Long id_bunch);

    List<AbstractClientDTO> findAllClients(Long id_bunch);

    List<AbstractCaseFileDTO> findAllCaseFiles(Long id_bunch);

    Map<String,AbstractOwnerEntityDTO> findOwner(Long id_bunch);
}
