package edu.ues.aps.process.area.establishment.controller;

import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.area.establishment.service.EstablishmentFinderService;
import edu.ues.aps.process.area.establishment.service.EstablishmentPersistenceService;
import edu.ues.aps.process.base.service.OwnerEstablishmentAdderService;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;


@Controller
public class EstablishmentPersistenceController {

    private static final Logger logger = LogManager.getLogger(EstablishmentPersistenceController.class);

    private final static String ESTABLISHMENT_NEW = "establishment_new_form";
    private final static String ESTABLISHMENT_EDIT = "establishment_edit_form";

    private final EstablishmentFinderService finderService;
    private final ProcessUserRegisterService registerService;
    private final OwnerEstablishmentAdderService adderService;
    private final EstablishmentPersistenceService persistenceService;

    public EstablishmentPersistenceController(EstablishmentFinderService finderService, OwnerEstablishmentAdderService adderService, EstablishmentPersistenceService persistenceService, ProcessUserRegisterService registerService) {
        this.finderService = finderService;
        this.adderService = adderService;
        this.persistenceService = persistenceService;
        this.registerService = registerService;
    }

    @GetMapping("owner/{id_owner}/establishment/new")
    public String showEstablishmentNewForm(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show new establishment form for owner {}.", id_owner);
        model.addAttribute("establishment", new Establishment());
        model.addAttribute("id_owner", id_owner);
        return ESTABLISHMENT_NEW;
    }

    @PostMapping("owner/{id_owner}/establishment/new")
    public String saveEstablishment(@ModelAttribute Establishment establishment, @PathVariable Long id_owner, Principal principal) {
        logger.info("POST:: Save establishment for owner {}.", id_owner);
        adderService.add(establishment, id_owner);
        registerService.addProcessUser(principal.getName(), "process.establishment.new", new String[]{String.valueOf(id_owner)});
        return String.format("redirect:/owner/%d/establishment", id_owner);
    }

    @GetMapping({"/establishment/{id_establishment}/edit", "/owner/{id_owner}/establishment/{id_establishment}/edit"})
    public String showEstablishmentEditForm(@PathVariable Long id_establishment, @PathVariable(required = false) Long id_owner, ModelMap model) {
        logger.info("GET:: Show establishment {} edit form", id_establishment);
        model.addAttribute("establishment", finderService.findById(id_establishment));
        model.addAttribute("id_owner", id_owner);
        return ESTABLISHMENT_EDIT;
    }

    @PostMapping({"/establishment/{id_establishment}/edit", "/owner/{id_owner}/establishment/{id_establishment}/edit"})
    public String updateEstablishmentFormChanges(@ModelAttribute Establishment establishment, @PathVariable(required = false) Long id_owner,
                                                 @PathVariable Long id_establishment,Principal principal) {
        logger.info("POST:: Update establishment {}.", id_establishment);
        persistenceService.update(establishment);
        registerService.addProcessUser(principal.getName(),"process.establishment.edit",new String[]{
                String.valueOf(id_establishment),establishment.toString()});
        if (id_owner != null) {
            return String.format("redirect:/owner/%d/establishment", id_owner);
        }
        return String.format("redirect:/establishment/%d", establishment.getId());
    }


}
