package edu.ues.aps.process.area.base.repository;

import edu.ues.aps.process.area.base.dto.AbstractInfo;

import javax.persistence.NoResultException;

public interface InfoFinderRepository {

    AbstractInfo getInfo(Long id_caseFile) throws NoResultException;
}
