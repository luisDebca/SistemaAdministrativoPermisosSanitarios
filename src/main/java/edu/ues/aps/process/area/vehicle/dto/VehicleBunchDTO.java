package edu.ues.aps.process.area.vehicle.dto;

public class VehicleBunchDTO implements AbstractVehicleBunchDTO{

    private Long id_owner;
    private Long id_bunch;
    private String owner_name;
    private String client_presented_name;
    private String client_presented_telephone;
    private String distribution_company;
    private String distribution_destination;
    private String marketing_place;
    private Integer vehicle_count;
    private String address_details;
    private Long caseFiles_count;
    private String folder_number;

    public VehicleBunchDTO(Long id_owner, Long id_bunch, String owner_name, String client_presented_name, String client_presented_telephone, String distribution_company, String distribution_destination, String marketing_place, Integer vehicle_count, String address_details, Long caseFiles_count, String folder_number) {
        this.id_owner = id_owner;
        this.id_bunch = id_bunch;
        this.owner_name = owner_name;
        this.client_presented_name = client_presented_name;
        this.client_presented_telephone = client_presented_telephone;
        this.distribution_company = distribution_company;
        this.distribution_destination = distribution_destination;
        this.marketing_place = marketing_place;
        this.vehicle_count = vehicle_count;
        this.address_details = address_details;
        this.caseFiles_count = caseFiles_count;
        this.folder_number = folder_number;
    }

    public Long getId_bunch() {
        return id_bunch;
    }

    public void setId_bunch(Long id_bunch) {
        this.id_bunch = id_bunch;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getClient_presented_name() {
        return client_presented_name;
    }

    public String getClient_presented_telephone() {
        return client_presented_telephone;
    }

    public String getDistribution_company() {
        return distribution_company;
    }

    public String getDistribution_destination() {
        return distribution_destination;
    }

    public String getMarketing_place() {
        return marketing_place;
    }

    public Integer getVehicle_count() {
        return vehicle_count;
    }

    public String getAddress_details() {
        return address_details;
    }

    public Long getCaseFiles_count() {
        return caseFiles_count;
    }

    public String getFolder_number() {
        return folder_number;
    }
}
