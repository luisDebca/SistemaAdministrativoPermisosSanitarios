package edu.ues.aps.process.area.establishment.controller;

import edu.ues.aps.process.area.establishment.model.CasefileEstablishment;
import edu.ues.aps.process.area.establishment.service.EstablishmentCaseFileAdderService;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.record.base.service.CaseFileProcessRegisterService;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
public class EstablishmentCaseFilePersistenceController {

    private static final Logger logger = LogManager.getLogger(EstablishmentCaseFilePersistenceController.class);

    private static final String OUTDATED_REQUEST = "casefile_outdated_form";

    private final ProcessUserRegisterService registerService;
    private final CaseFileProcessRegisterService processRegisterService;
    private final EstablishmentCaseFileAdderService caseFileAdderService;

    public EstablishmentCaseFilePersistenceController(EstablishmentCaseFileAdderService caseFileAdderService, CaseFileProcessRegisterService processRegisterService, ProcessUserRegisterService registerService) {
        this.caseFileAdderService = caseFileAdderService;
        this.processRegisterService = processRegisterService;
        this.registerService = registerService;
    }

    @GetMapping("/establishment/{id_establishment}/caseFile/first/new")
    public String showNewFirstRequestForm(@PathVariable Long id_establishment, Principal principal) {
        logger.info("GET:: Create new first request for establishment {}", id_establishment);
        AbstractCasefile caseFile = caseFileAdderService.add(CertificationType.FIRST, id_establishment);
        registerService.addProcessUser(caseFile.getId(), principal.getName(), "process.record.firstRequestCreation");
        return String.format("redirect:/establishment/%d/caseFile/all", id_establishment);
    }

    @GetMapping("/establishment/{id_establishment}/caseFile/renewal/new")
    public String showNewRenewalRequestForm(@PathVariable Long id_establishment, Principal principal) {
        logger.info("GET:: Create new renewal request for establishment {}", id_establishment);
        AbstractCasefile caseFile = caseFileAdderService.add(CertificationType.RENEWAL, id_establishment);
        registerService.addProcessUser(caseFile.getId(), principal.getName(), "process.record.renewalRequestCreation");
        return String.format("redirect:/establishment/%d/caseFile/all", id_establishment);
    }

    @GetMapping("/establishment/{id_establishment}/caseFile/outdated/new")
    public String showNewOutdatedRequestForm(@PathVariable Long id_establishment, ModelMap model) {
        logger.info("GET:: Create new outdated request for establishment {}", id_establishment);
        model.addAttribute("id_establishment", id_establishment);
        model.addAttribute("casefile", new Casefile());
        return OUTDATED_REQUEST;
    }

    @PostMapping("/establishment/{id_establishment}/caseFile/outdated/new")
    public String saveOutdatedRequest(@PathVariable Long id_establishment, @ModelAttribute CasefileEstablishment casefile, Principal principal) {
        logger.info("POST:: Save outdated request for establishment {}", id_establishment);
        AbstractCasefile persistentCaseFile = caseFileAdderService.add(casefile, id_establishment);
        String customized_message;
        if (persistentCaseFile.getStatus().equals(CasefileStatusType.IN_PROCESS)) {
            processRegisterService.addProcess(persistentCaseFile, ProcessIdentifier.REQUEST_IN_PROCESS);
            customized_message = "process.record.establishment.outdated.inProcess";
        } else if (persistentCaseFile.getStatus().equals(CasefileStatusType.VALID)) {
            processRegisterService.addProcess(persistentCaseFile, ProcessIdentifier.CERTIFICATION_ACTIVE);
            customized_message = "process.record.establishment.outdated.valid";
        } else if (persistentCaseFile.getStatus().equals(CasefileStatusType.EXPIRED)) {
            processRegisterService.addProcess(persistentCaseFile, ProcessIdentifier.CERTIFICATION_EXPIRED);
            customized_message = "process.record.establishment.outdated.expired";
        } else {
            throw new CaseFileOutdatedStatusInvalidException(String.format("CaseFile status %s not available", persistentCaseFile.getStatus().name()));
        }
        registerService.addProcessUser(persistentCaseFile.getId(), principal.getName(), customized_message);
        return String.format("redirect:/establishment/%d/caseFile/all", id_establishment);
    }
}
