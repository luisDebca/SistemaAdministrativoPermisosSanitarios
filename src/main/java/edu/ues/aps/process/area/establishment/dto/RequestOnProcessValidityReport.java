package edu.ues.aps.process.area.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class RequestOnProcessValidityReport implements AbstractRequestOnProcessValidityReport {

    private String client_name;
    private String owner_name;
    private String establishment_name;
    private String establishment_address;
    private LocalDateTime request_process_start;
    private String request_number;
    private CertificationType request_type;
    private String user_name;
    private String user_charge;

    public RequestOnProcessValidityReport(String owner_name, String establishment_name, String establishment_address, LocalDateTime request_process_start, String request_number, CertificationType request_type) {
        this.owner_name = owner_name;
        this.establishment_name = establishment_name;
        this.establishment_address = establishment_address;
        this.request_process_start = request_process_start;
        this.request_number = request_number;
        this.request_type = request_type;
    }

    public String getClient_name() {
        return client_name;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getRequest_process_start() {
        if(request_process_start != null){
            return DatetimeUtil.parseDateToLongDateFormat(request_process_start.toLocalDate());
        }
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getRequest_type() {
        if(request_type != null){
            return request_type.name();
        }
        return CertificationType.INVALID.name();
    }

    public String getCurrent_date() {
        return DatetimeUtil.parseDateToLongDateFormat(LocalDate.now());
    }

    public String getUser_name() {
        return user_name;
    }

    public String getUser_charge() {
        return user_charge;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }


    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public void setUser_charge(String user_charge) {
        this.user_charge = user_charge;
    }
}
