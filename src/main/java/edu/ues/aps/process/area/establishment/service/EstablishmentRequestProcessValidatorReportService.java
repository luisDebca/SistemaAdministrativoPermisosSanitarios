package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.dto.AbstractRequestOnProcessValidityReport;
import edu.ues.aps.process.area.establishment.repository.RequestValidatorReportRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class EstablishmentRequestProcessValidatorReportService implements RequestProcessValidatorReportService {

    private final RequestValidatorReportRepository reportRepository;

    public EstablishmentRequestProcessValidatorReportService(RequestValidatorReportRepository reportRepository) {
        this.reportRepository = reportRepository;
    }

    @Override
    public AbstractRequestOnProcessValidityReport getReportDataSource(Long id_caseFile) {
        return reportRepository.getRequestProcessData(id_caseFile);
    }
}
