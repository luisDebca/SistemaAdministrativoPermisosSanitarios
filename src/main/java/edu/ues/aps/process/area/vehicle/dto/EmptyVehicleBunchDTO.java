package edu.ues.aps.process.area.vehicle.dto;

public class EmptyVehicleBunchDTO implements AbstractVehicleBunchDTO {

    @Override
    public Long getId_bunch() {
        return 0L;
    }

    @Override
    public void setId_bunch(Long id_bunch) {

    }

    @Override
    public Long getId_owner() {
        return 0L;
    }

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public String getClient_presented_name() {
        return "Name Not found";
    }

    @Override
    public String getClient_presented_telephone() {
        return "Phone Not found";
    }

    @Override
    public String getDistribution_company() {
        return "Company Not found";
    }

    @Override
    public String getDistribution_destination() {
        return "Destination Not found";
    }

    @Override
    public String getMarketing_place() {
        return "Marketing Not found";
    }

    @Override
    public Integer getVehicle_count() {
        return 0;
    }

    @Override
    public String getAddress_details() {
        return "Address Not found";
    }

    @Override
    public Long getCaseFiles_count() {
        return 0L;
    }

    @Override
    public String getFolder_number() {
        return "Folder number not found";
    }
}
