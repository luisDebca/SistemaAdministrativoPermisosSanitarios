package edu.ues.aps.process.area.base.repository;

import edu.ues.aps.process.area.base.dto.AbstractRequestDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface RequestDTOFinderRepository {

    AbstractRequestDTO find(Long id_caseFile) throws NoResultException;

    List<AbstractRequestDTO> findAll(Long id_bunch);
}
