package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleBunchDTO;

public interface VehicleBunchDTOFinderService {

    AbstractVehicleBunchDTO findById(Long id_bunch);
}
