package edu.ues.aps.process.area.establishment.controller;

import edu.ues.aps.process.area.establishment.dto.AbstractCasefileEstablishmentInfoDTO;
import edu.ues.aps.process.area.establishment.service.EstablishmentCaseFileDTOFinderService;
import edu.ues.aps.process.area.establishment.service.EstablishmentCaseFileDTOInformationService;
import edu.ues.aps.process.area.establishment.service.EstablishmentDTOFinderService;
import edu.ues.aps.process.base.service.OwnerDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/establishment")
public class EstablishmentCaseFileInformationController {

    private static final Logger logger = LogManager.getLogger(EstablishmentCaseFileInformationController.class);

    private static final String CASE_FILE_INFORMATION = "establishment_casefile_information";
    private static final String CASE_FILE_FROM_ESTABLISHMENT_LIST = "establishment_casefile_list";

    private final EstablishmentCaseFileDTOInformationService informationService;
    private final EstablishmentCaseFileDTOFinderService caseFileFinderService;
    private final EstablishmentDTOFinderService establishmentFinderService;
    private final OwnerDTOFinderService ownerFinderService;

    public EstablishmentCaseFileInformationController(EstablishmentCaseFileDTOInformationService informationService, EstablishmentCaseFileDTOFinderService caseFileFinderService, EstablishmentDTOFinderService establishmentFinderService, OwnerDTOFinderService ownerFinderService) {
        this.informationService = informationService;
        this.caseFileFinderService = caseFileFinderService;
        this.establishmentFinderService = establishmentFinderService;
        this.ownerFinderService = ownerFinderService;
    }

    @GetMapping("/caseFile/{id_caseFile}/info")
    public String showCaseFileFullInformation(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show CaseFile {} Information", id_caseFile);
        AbstractCasefileEstablishmentInfoDTO casefileDTO = caseFileFinderService.find(id_caseFile);
        model.addAttribute("casefile", casefileDTO);
        model.addAttribute("establishment", establishmentFinderService.findById(casefileDTO.getId_establishment()));
        model.addAttribute("owner", ownerFinderService.findById(casefileDTO.getId_owner()));
        return CASE_FILE_INFORMATION;
    }

    @GetMapping("/{id_establishment}/caseFile/all")
    public String showAllCaseFilesFromEstablishment(@PathVariable Long id_establishment, ModelMap model) {
        logger.info("GET:: Show Establishment {} case files", id_establishment);
        model.addAttribute("id_establishment", id_establishment);
        model.addAttribute("case_files", informationService.findAll(id_establishment));
        return CASE_FILE_FROM_ESTABLISHMENT_LIST;
    }
}
