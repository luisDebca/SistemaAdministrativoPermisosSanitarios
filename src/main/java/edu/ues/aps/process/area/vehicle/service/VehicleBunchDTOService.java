package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleBunchDTO;
import edu.ues.aps.process.area.vehicle.dto.EmptyVehicleBunchDTO;
import edu.ues.aps.process.area.vehicle.model.TransportedContentType;
import edu.ues.aps.process.area.vehicle.repository.VehicleBunchDTOFinderRepository;
import edu.ues.aps.process.area.vehicle.repository.VehicleBunchInformationRepository;
import edu.ues.aps.process.base.model.CertificationType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleBunchDTOService implements VehicleBunchDTOFinderService, VehicleBunchInformationService {

    private final VehicleBunchInformationRepository informationRepository;
    private final VehicleBunchDTOFinderRepository finderRepository;

    public VehicleBunchDTOService(VehicleBunchDTOFinderRepository finderRepository, VehicleBunchInformationRepository informationRepository) {
        this.finderRepository = finderRepository;
        this.informationRepository = informationRepository;
    }

    @Override
    public AbstractVehicleBunchDTO findById(Long id_bunch) {
        try {
            AbstractVehicleBunchDTO bunchDTO = finderRepository.findById(id_bunch);
            bunchDTO.setId_bunch(id_bunch);
            return bunchDTO;
        } catch (NoResultException e) {
            return new EmptyVehicleBunchDTO();
        }
    }

    @Override
    @Transactional(readOnly = true)
    public List<String> findAllLicensesList(Long id_bunch) {
        return informationRepository.findAllLicenses(id_bunch);
    }

    @Override
    public String findAllLicenses(Long id_bunch) {
        StringBuilder licenses = new StringBuilder();
        for (String license : findAllLicensesList(id_bunch)) {
            if (licenses.length() == 0) {
                licenses.append(license);
            } else {
                licenses.append(", ").append(license);
            }
        }
        return licenses.toString();
    }

    @Override
    @Transactional(readOnly = true)
    public String findAllContents(Long id_bunch) {
        StringBuilder contents = new StringBuilder();
        for (TransportedContentType type : informationRepository.findAllContents(id_bunch)) {
            if (contents.length() == 0) {
                contents.append(type.getTransportedContentType());
            } else {
                contents.append(" Y ").append(type.getTransportedContentType());
            }
        }
        return contents.toString();
    }

    @Override
    @Transactional(readOnly = true)
    public String findAllTypes(Long id_bunch) {
        StringBuilder types = new StringBuilder();
        for (String type : informationRepository.findAllTypes(id_bunch)) {
            if (types.length() == 0) {
                types.append(type);
            } else {
                types.append(", ").append(type);
            }
        }
        return types.toString();
    }

    @Override
    public String findAllCertificationTypes(Long id_bunch) {
        StringBuilder types = new StringBuilder();
        for (CertificationType type : informationRepository.findAllCertificationTypes(id_bunch)) {
            if (types.length() == 0) {
                types.append(type.getCertificationType());
            } else {
                types.append(" Y ").append(type.getCertificationType());
            }
        }
        return types.toString();
    }

    @Override
    @Transactional(readOnly = true)
    public List<Long> findAllCaseFileId(Long id_bunch) {
        return informationRepository.findAllCaseFileId(id_bunch);
    }
}
