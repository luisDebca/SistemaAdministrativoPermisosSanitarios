package edu.ues.aps.process.area.base.dto;

public interface AbstractRequestDTO {

    String getFolder_number();

    String getRequest_number();

    String getResolution_number();
}
