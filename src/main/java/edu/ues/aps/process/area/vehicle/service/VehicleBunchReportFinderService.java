package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO;

import java.util.List;

public interface VehicleBunchReportFinderService {

    List<AbstractVehicleCaseFileDTO> findDatasource(Long id_bunch);
}
