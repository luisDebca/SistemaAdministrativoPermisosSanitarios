package edu.ues.aps.process.area.establishment.dto;

public interface AbstractEstablishmentEntityDTO {

    public Long getEstablishmentID();

    public String getEstablishmentName();

    public String getEstablishmentNIT();

    public String getEstablishmentAddress();

    public String getEstablishmentCommercialRegister();

    public String getEstablishmentType();

    public String getEstablishmentSection();

    public String getEstablishmentEmail();

    public String getEstablishmentPhone();

    public String getEstablishmentPhone2();

    public String getEstablishmentFAX();

    public String getEstablishmentOperations();

    public Double getEstablishmentCapital();

    public Integer getEstablishmentMaleCount();

    public Integer getEstablishmentFemaleCount();

    public Boolean getEstablishmentActive();
}
