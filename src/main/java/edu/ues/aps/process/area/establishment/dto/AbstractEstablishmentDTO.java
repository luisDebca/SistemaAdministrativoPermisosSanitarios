package edu.ues.aps.process.area.establishment.dto;

public interface AbstractEstablishmentDTO {

    Long getEstablishment_id();

    Long getOwner_id();

    String getEstablishment_name();

    String getEstablishment_owner_name();

    String getEstablishment_type();

    String getEstablishment_type_detail();

    String getCommercial_register();

    Double getEstablishment_capital();

    String getEstablishment_nit();

    String getEstablishment_email();

    String getEstablishment_telephone();

    String getEstablishment_fax();

    Integer getEstablishment_male_employee_count();

    Integer getEstablishment_female_employee_count();

    String getEstablishment_operations();

    String getEstablishment_address();

    String getEstablishment_telephone_extra();

    Boolean isActive();
}
