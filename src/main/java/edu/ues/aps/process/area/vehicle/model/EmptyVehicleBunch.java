package edu.ues.aps.process.area.vehicle.model;

import edu.ues.aps.process.base.model.AbstractAddress;
import edu.ues.aps.process.base.model.Address;
import edu.ues.aps.process.base.model.EmptyAddress;

import java.util.Collections;
import java.util.List;

public class EmptyVehicleBunch implements AbstractVehicleBunch {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getClient_presented_name() {
        return "Client presented name not found";
    }

    @Override
    public void setClient_presented_name(String client_presented_name) {

    }

    @Override
    public String getClient_presented_telephone() {
        return "Client presented telephone not found";
    }

    @Override
    public void setClient_presented_telephone(String client_presented_telephone) {

    }

    @Override
    public String getDistribution_company() {
        return "Distribution company not found";
    }

    @Override
    public void setDistribution_company(String distribution_company) {

    }

    @Override
    public String getDistribution_destination() {
        return "Distribution destination not found";
    }

    @Override
    public void setDistribution_destination(String distribution_destination) {

    }

    @Override
    public String getMarketing_place() {
        return "Marketing place not found";
    }

    @Override
    public void setMarketing_place(String marketing_place) {

    }

    @Override
    public List<CasefileVehicle> getCasefileVehicles() {
        return Collections.emptyList();
    }

    @Override
    public void setCasefileVehicles(List<CasefileVehicle> casefileVehicles) {

    }

    @Override
    public void addCasefile(CasefileVehicle casefile) {

    }

    @Override
    public void removeCasefile(CasefileVehicle casefile) {

    }

    @Override
    public List<TransportedProduct> getProducts() {
        return Collections.emptyList();
    }

    @Override
    public void setProducts(List<TransportedProduct> products) {

    }

    @Override
    public void addProducts(TransportedProduct product) {

    }

    @Override
    public void removeProduct(TransportedProduct product) {

    }

    @Override
    public Integer getVehicle_count() {
        return 0;
    }

    @Override
    public void setVehicle_count(Integer vehicle_count) {

    }

    @Override
    public AbstractAddress getAddress() {
        return new EmptyAddress();
    }

    @Override
    public void setAddress(Address address) {

    }
}
