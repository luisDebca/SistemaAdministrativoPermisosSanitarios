package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.model.AbstractVehicle;
import edu.ues.aps.process.area.vehicle.model.EmptyVehicle;
import edu.ues.aps.process.area.vehicle.repository.VehiclePersistenceRepository;
import edu.ues.aps.process.area.vehicle.repository.VehicleValidatorRepository;
import edu.ues.aps.process.base.service.OwnerVehicleAdderService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
public class VehicleService implements VehiclePersistenceService, VehicleValidatorService{

    private final OwnerVehicleAdderService adderService;
    private final VehicleValidatorRepository validatorRepository;
    private final VehiclePersistenceRepository persistenceRepository;

    public VehicleService(VehiclePersistenceRepository persistenceRepository, OwnerVehicleAdderService adderService, VehicleValidatorRepository validatorRepository) {
        this.persistenceRepository = persistenceRepository;
        this.adderService = adderService;
        this.validatorRepository = validatorRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractVehicle findByID(Long id_vehicle) {
        try {
            return persistenceRepository.findByID(id_vehicle);
        } catch (NoResultException e) {
            return new EmptyVehicle();
        }
    }

    @Override
    @Transactional
    public void save(Long id_owner, AbstractVehicle vehicle) {
        vehicle.setActive(true);
        adderService.add(vehicle,id_owner);
    }

    @Override
    @Transactional
    public void update(AbstractVehicle vehicle) {
        AbstractVehicle persistentVehicle = persistenceRepository.findByID(vehicle.getId());
        persistentVehicle.setEmployees_number(vehicle.getEmployees_number());
        persistentVehicle.setLicense(vehicle.getLicense());
        persistentVehicle.setTransported_content(vehicle.getTransported_content());
        persistentVehicle.setVehicle_type(vehicle.getVehicle_type());
    }

    @Override
    @Transactional
    public void delete(Long id_vehicle) {
        AbstractVehicle vehicle = persistenceRepository.findByID(id_vehicle);
        persistenceRepository.delete(vehicle);
    }

    @Override
    @Transactional
    public void toDisable(Long id_vehicle) {
        persistenceRepository.findByID(id_vehicle).setActive(false);
    }

    @Override
    @Transactional
    public void toEnable(Long id_vehicle) {
        persistenceRepository.findByID(id_vehicle).setActive(true);
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueLicense(String license) {
        return validatorRepository.isLicenseUnique(license) == 0;
    }

    @Override
    @Transactional(readOnly = true)
    public Boolean isUniqueLicense(String license, Long id_vehicle) {
        if(validatorRepository.isLicenseEquals(license,id_vehicle)){
            return true;
        }else {
            return validatorRepository.isLicenseUnique(license) == 0;
        }
    }
}
