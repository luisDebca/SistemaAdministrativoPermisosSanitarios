package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.model.AbstractCasefileVehicle;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class VehicleCaseFileRepository implements VehicleCaseFilePersistenceRepository{

    private final SessionFactory sessionFactory;

    public VehicleCaseFileRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(AbstractCasefileVehicle caseFile) {
        sessionFactory.getCurrentSession().persist(caseFile);
    }
}
