package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.model.AbstractCasefileVehicle;

public interface VehicleCaseFilePersistenceRepository {

    void save(AbstractCasefileVehicle caseFile);
}
