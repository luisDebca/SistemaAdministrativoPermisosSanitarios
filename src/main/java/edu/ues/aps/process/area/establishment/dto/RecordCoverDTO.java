package edu.ues.aps.process.area.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;

public class RecordCoverDTO {

    private String p1;
    private String p2;
    private String p3;
    private String p4;
    private String p5;
    private CertificationType p6;

    public RecordCoverDTO(String p1, String p2, String p3, String p4, String p5, CertificationType p6) {
        this.p1 = p1;
        this.p2 = p2;
        this.p3 = p3;
        this.p4 = p4;
        this.p5 = p5;
        this.p6 = p6;
    }

    public String getP1() {
        return p1;
    }

    public String getP2() {
        return p2;
    }

    public String getP3() {
        return p3;
    }

    public String getP4() {
        return p4;
    }

    public String getP5() {
        return p5;
    }

    public String getP6() {
        return p6.getCertificationType();
    }
}