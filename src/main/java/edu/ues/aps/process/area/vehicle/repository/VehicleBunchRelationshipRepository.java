package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface VehicleBunchRelationshipRepository {

    List<AbstractClientDTO> findAllClientsWithNaturalOwner(Long id_bunch);

    List<AbstractClientDTO> findAllClients(Long id_bunch);

    List<AbstractCaseFileDTO> findAllCaseFiles(Long id_bunch);

    AbstractOwnerEntityDTO findLegalOwner(Long id_bunch) throws NoResultException;

    AbstractOwnerEntityDTO findNaturalOwner(Long id_bunch) throws NoResultException;
}
