package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EstablishmentTypeRepository implements EstablishmentTypeDTOFinderRepository {

    private SessionFactory sessionFactory;

    public EstablishmentTypeRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractMapDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.MapDTO(types.id,types.type) " +
                "from EstablishmentType types", AbstractMapDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractMapDTO> findAllTypesFromSection(Long id_section) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.MapDTO(types.id,types.type) " +
                "from EstablishmentType types " +
                "inner join types.section section " +
                "where section.id = :id", AbstractMapDTO.class)
                .setParameter("id", id_section)
                .getResultList();
    }
}
