package edu.ues.aps.process.area.vehicle.controller;

import edu.ues.aps.process.area.vehicle.service.VehicleDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;

@Controller
@RequestMapping("/vehicle")
public class VehicleInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleInformationProviderController.class);

    private static final String VEHICLE_LIST = "vehicle_list";
    private static final String VEHICLE_INFO = "vehicle_information";
    private static final String INDEX = "vehicle_index";

    private final VehicleDTOFinderService finderService;

    public VehicleInformationProviderController(VehicleDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/index")
    public String showVehicleIndex(){
        logger.info("GET:: Show vehicles index");
        return INDEX;
    }

    @GetMapping("/{id_vehicle}/info")
    public String showVehicleInformation(@PathVariable Long id_vehicle, ModelMap model){
        logger.info("GET:: Show vehicle {} information",id_vehicle);
        model.addAttribute("vehicle",finderService.findByID(id_vehicle));
        return VEHICLE_INFO;
    }

    @GetMapping("/list")
    public String showAllVehicles(ModelMap model){
        logger.info("GET:: Show all vehicles");
        model.addAttribute("vehicles", finderService.findAll());
        return VEHICLE_LIST;
    }
}
