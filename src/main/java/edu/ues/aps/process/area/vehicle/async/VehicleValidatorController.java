package edu.ues.aps.process.area.vehicle.async;

import edu.ues.aps.process.area.vehicle.service.VehicleValidatorService;
import org.springframework.context.MessageSource;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.Locale;

@Controller
@RequestMapping("/vehicle/validator")
public class VehicleValidatorController {

    private final MessageSource messageSource;
    private final VehicleValidatorService validatorService;

    public VehicleValidatorController(VehicleValidatorService validatorService, MessageSource messageSource) {
        this.validatorService = validatorService;
        this.messageSource = messageSource;
    }

    @GetMapping(value = "/license", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateVehicleLicenseUnique(@RequestParam String license){
        return validatorService.isUniqueLicense(license) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("vehicle.license", new String[]{license}, Locale.getDefault()));
    }

    @GetMapping(value = "/license-update", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Object validateVehicleLicenseUnique(@RequestParam String license, @RequestParam Long id_vehicle){
        return validatorService.isUniqueLicense(license,id_vehicle) ?
                Boolean.TRUE : Collections.singleton(messageSource.getMessage("vehicle.license", new String[]{license}, Locale.getDefault()));
    }
}
