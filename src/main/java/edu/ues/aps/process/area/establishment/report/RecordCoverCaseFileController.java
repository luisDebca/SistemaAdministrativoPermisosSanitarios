package edu.ues.aps.process.area.establishment.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import edu.ues.aps.process.area.establishment.service.RecordCoverDataService;

import java.util.Collections;

@Controller
@RequestMapping("/request")
public class RecordCoverCaseFileController extends AbstractReportController {

    private static final String REPORT_NAME = "FolderEReport";
    private static final Logger logger = LogManager.getLogger(RecordCoverCaseFileController.class);

    private RecordCoverDataService recordCoverService;

    RecordCoverCaseFileController(RecordCoverDataService recordCover){
        this.recordCoverService = recordCover;
    }

    @GetMapping("/record-cover-caseFile-report.pdf")
    public String recordCoverCaseFile(@RequestParam Long id_caseFile, ModelMap model){
        logger.info("GET:: Generating record cover caseFile report");
        addReportRequiredSources(Collections.singletonList(recordCoverService.findDataRecordCoverReport(id_caseFile)));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }
}