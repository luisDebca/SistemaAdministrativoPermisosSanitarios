package edu.ues.aps.process.area.base.controller;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.base.service.CaseFileDTOFinderService;
import edu.ues.aps.process.area.base.service.InfoFinderService;
import edu.ues.aps.process.base.model.CasefileSection;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/caseFile")
public class CaseFileInformationProviderController {

    private static final Logger logger = LogManager.getLogger(CaseFileInformationProviderController.class);

    private static final String CASE_FILE_LIST = "casefile_list";
    private static final String CASE_FILE_INFO = "casefile_info";

    private final InfoFinderService infoService;
    private final CaseFileDTOFinderService finderService;

    public CaseFileInformationProviderController(CaseFileDTOFinderService finderService, InfoFinderService infoService) {
        this.finderService = finderService;
        this.infoService = infoService;
    }

    @GetMapping("/{id_caseFile}/info")
    public String showCaseFileInformation(@PathVariable Long id_caseFile, ModelMap model){
        logger.info("GET:: Show caseFile {} information",id_caseFile);
        model.addAttribute("info",infoService.getInfo(id_caseFile));
        return CASE_FILE_INFO;
    }

    @GetMapping("/list")
    public String showCaseFileList(ModelMap model){
        logger.info("GET:: Show caseFile list for all sections");
        model.addAttribute("requests",finderService.findAll());
        return CASE_FILE_LIST;
    }
}
