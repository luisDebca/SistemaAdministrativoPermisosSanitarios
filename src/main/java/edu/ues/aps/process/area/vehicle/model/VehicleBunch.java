package edu.ues.aps.process.area.vehicle.model;

import edu.ues.aps.process.base.model.Address;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "VEHICLE_BUNCH")
public class VehicleBunch implements AbstractVehicleBunch{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "client_presented_name", length = 150)
    private String client_presented_name;

    @Column(name = "client_presented_telephone", length = 15)
    private String client_presented_telephone;

    @Column(name = "distribution_company", length = 100)
    private String distribution_company;

    @Column(name = "distribution_destination", length = 150)
    private String distribution_destination;

    @Column(name = "marketing_place", length = 100)
    private String marketing_place;

    @Column(name = "vehicle_count")
    private Integer vehicle_count;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bunch")
    private List<CasefileVehicle> casefileVehicles = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "bunch", orphanRemoval = true)
    private List<TransportedProduct> products = new ArrayList<>();

    @OneToOne(cascade = CascadeType.ALL)
    @JoinTable(name = "VEHICLE_ADDRESS",
            joinColumns = @JoinColumn(name = "id_vehicle"),
            inverseJoinColumns = @JoinColumn(name = "id_address"))
    private Address address;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getClient_presented_name() {
        return client_presented_name;
    }

    @Override
    public void setClient_presented_name(String client_presented_name) {
        this.client_presented_name = client_presented_name;
    }

    @Override
    public String getClient_presented_telephone() {
        return client_presented_telephone;
    }

    @Override
    public void setClient_presented_telephone(String client_presented_telephone) {
        this.client_presented_telephone = client_presented_telephone;
    }

    @Override
    public String getDistribution_company() {
        return distribution_company;
    }

    @Override
    public void setDistribution_company(String distribution_company) {
        this.distribution_company = distribution_company;
    }

    @Override
    public String getDistribution_destination() {
        return distribution_destination;
    }

    @Override
    public void setDistribution_destination(String distribution_destination) {
        this.distribution_destination = distribution_destination;
    }

    @Override
    public String getMarketing_place() {
        return marketing_place;
    }

    @Override
    public void setMarketing_place(String marketing_place) {
        this.marketing_place = marketing_place;
    }

    @Override
    public Integer getVehicle_count() {
        return vehicle_count;
    }

    @Override
    public void setVehicle_count(Integer vehicle_count) {
        this.vehicle_count = vehicle_count;
    }

    @Override
    public List<CasefileVehicle> getCasefileVehicles() {
        return casefileVehicles;
    }

    @Override
    public void setCasefileVehicles(List<CasefileVehicle> casefileVehicles) {
        this.casefileVehicles = casefileVehicles;
    }

    @Override
    public void addCasefile(CasefileVehicle casefile){
        casefileVehicles.add(casefile);
        casefile.setBunch(this);
    }

    @Override
    public void removeCasefile(CasefileVehicle casefile){
        casefileVehicles.remove(casefile);
        casefile.setBunch(this);
    }

    @Override
    public List<TransportedProduct> getProducts() {
        return products;
    }

    @Override
    public void setProducts(List<TransportedProduct> products) {
        this.products = products;
    }

    @Override
    public void addProducts(TransportedProduct product){
        products.add(product);
        product.setBunch(this);
    }

    @Override
    public void removeProduct(TransportedProduct product){
        products.remove(product);
        product.setBunch(null);
    }

    @Override
    public Address getAddress() {
        return address;
    }

    @Override
    public void setAddress(Address address) {
        this.address = address;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        VehicleBunch that = (VehicleBunch) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "VehicleBunch{" +
                "id=" + id +
                ", client_presented_name='" + client_presented_name + '\'' +
                ", client_presented_telephone='" + client_presented_telephone + '\'' +
                ", distribution_company='" + distribution_company + '\'' +
                ", distribution_destination='" + distribution_destination + '\'' +
                ", marketing_place='" + marketing_place + '\'' +
                ", vehicle_count=" + vehicle_count +
                '}';
    }
}
