package edu.ues.aps.process.area.vehicle.model;

import edu.ues.aps.process.base.model.EmptyCasefile;

public class EmptyCasefileVehicle extends EmptyCasefile implements AbstractCasefileVehicle{

    @Override
    public AbstractVehicle getVehicle() {
        return new EmptyVehicle();
    }

    @Override
    public void setVehicle(Vehicle vehicle) {

    }

    @Override
    public AbstractVehicleBunch getBunch() {
        return new EmptyVehicleBunch();
    }

    @Override
    public void setBunch(VehicleBunch bunch) {

    }

    @Override
    public Integer getCertificationDurationInYears() {
        return 0;
    }

    @Override
    public String toString() {
        return "EmptyCasefileVehicle{}";
    }
}
