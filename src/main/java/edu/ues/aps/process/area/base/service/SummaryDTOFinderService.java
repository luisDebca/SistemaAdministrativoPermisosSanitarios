package edu.ues.aps.process.area.base.service;

import edu.ues.aps.process.area.base.dto.AbstractSummaryDTO;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;

import java.util.List;

public interface SummaryDTOFinderService {

    List<AbstractSummaryDTO> findToday(ProcessIdentifier processIdentifier, CasefileSection section);

    List<AbstractSummaryDTO> findWeek(ProcessIdentifier processIdentifier, CasefileSection section);

    List<AbstractSummaryDTO> findMonth(ProcessIdentifier processIdentifier, CasefileSection section);
}
