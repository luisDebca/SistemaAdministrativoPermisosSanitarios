package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.model.AbstractVehicle;

public interface VehiclePersistenceService {

    AbstractVehicle findByID(Long id_vehicle);

    void save(Long id_owner, AbstractVehicle vehicle);

    void update(AbstractVehicle vehicle);

    void delete(Long id_vehicle);

    void toDisable(Long id_vehicle);

    void toEnable(Long id_vehicle);
}
