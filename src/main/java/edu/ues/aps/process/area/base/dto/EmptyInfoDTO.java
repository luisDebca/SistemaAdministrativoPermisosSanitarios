package edu.ues.aps.process.area.base.dto;

public class EmptyInfoDTO implements AbstractInfo {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getFolder() {
        return "Not found";
    }

    @Override
    public String getRequest() {
        return "Not found";
    }

    @Override
    public String getSection() {
        return "Invalid";
    }

    @Override
    public String getType() {
        return "Invalid";
    }

    @Override
    public String getStart() {
        return "--/--/---- --:--:--";
    }

    @Override
    public String getCreated() {
        return "--/--/---- --:--:--";
    }

    @Override
    public String getStatus() {
        return "Not status";
    }

    @Override
    public String getUcsf() {
        return "Not found";
    }

    @Override
    public String getSibasi() {
        return "Not found";
    }

    @Override
    public Integer getDuration() {
        return 0;
    }

    @Override
    public Boolean getPayment() {
        return false;
    }

    @Override
    public Long getProcessId() {
        return 0L;
    }

    @Override
    public String getProcess() {
        return "Not found";
    }

    @Override
    public Long getTarget() {
        return 0L;
    }

    @Override
    public void setTarget(Long target) {

    }

    @Override
    public String getSectionType() {
        return "invalid";
    }

    @Override
    public int getProcessProgress() {
        return 0;
    }

    @Override
    public String getEnd() {
        return "--/--/---- --:--:--";
    }

    @Override
    public double getCertificationProgress() {
        return 0;
    }

    @Override
    public long getCertificationDays() {
        return 0;
    }

    @Override
    public String getName() {
        return "Not found";
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public String getOwner() {
        return "Not found";
    }

    @Override
    public void setOwner(String owner) {

    }
}
