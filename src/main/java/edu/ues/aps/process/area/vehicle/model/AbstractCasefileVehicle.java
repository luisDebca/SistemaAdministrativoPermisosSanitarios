package edu.ues.aps.process.area.vehicle.model;

import edu.ues.aps.process.base.model.AbstractAddress;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Address;

import java.util.List;

public interface AbstractCasefileVehicle extends AbstractCasefile {

    AbstractVehicle getVehicle();

    void setVehicle(Vehicle vehicle);

    AbstractVehicleBunch getBunch();

    void setBunch(VehicleBunch bunch);


}
