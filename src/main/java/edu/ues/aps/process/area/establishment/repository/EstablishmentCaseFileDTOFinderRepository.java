package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.dto.AbstractCasefileEstablishmentInfoDTO;
import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentCaseFileDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface EstablishmentCaseFileDTOFinderRepository {

    AbstractCasefileEstablishmentInfoDTO find(Long id_caseFile) throws NoResultException;
    
}
