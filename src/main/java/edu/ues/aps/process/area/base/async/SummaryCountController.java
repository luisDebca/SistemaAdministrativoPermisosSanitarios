package edu.ues.aps.process.area.base.async;

import edu.ues.aps.process.area.base.service.SummaryCountService;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/summary")
public class SummaryCountController {

    private final SummaryCountService countService;

    public SummaryCountController(SummaryCountService countService) {
        this.countService = countService;
    }

    @GetMapping(value = "/count/establishment/today", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getTodayEstablishmentCount(@RequestParam ProcessIdentifier process) {
        return countService.getTodayCount(process, CasefileSection.ESTABLISHMENT);
    }

    @GetMapping(value = "/count/establishment/week", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getWeekEstablishmentCount(@RequestParam ProcessIdentifier process) {
        return countService.getWeekCount(process, CasefileSection.ESTABLISHMENT);
    }

    @GetMapping(value = "/count/establishment/month", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getMonthEstablishmentCount(@RequestParam ProcessIdentifier process) {
        return countService.getMonthCount(process, CasefileSection.ESTABLISHMENT);
    }

    @GetMapping(value = "/count/vehicle/today", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getTodayVehicleCount(@RequestParam ProcessIdentifier process) {
        return countService.getTodayCount(process, CasefileSection.VEHICLE);
    }

    @GetMapping(value = "/count/vehicle/week", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getWeekVehicleCount(@RequestParam ProcessIdentifier process) {
        return countService.getWeekCount(process, CasefileSection.VEHICLE);
    }

    @GetMapping(value = "/count/vehicle/month", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getMonthVehicleCount(@RequestParam ProcessIdentifier process) {
        return countService.getMonthCount(process, CasefileSection.VEHICLE);
    }

}
