package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.dto.VehicleFoldersReportDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class VehicleFoldersReportRepository implements VehicleFoldersReportDTORepository {

    private final SessionFactory sessionFactory;

    public VehicleFoldersReportRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public VehicleFoldersReportDTO findDataReport(Long id_bunch) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleFoldersReportDTO(" +
                "concat(concat(cv.request_folder,'-'),cv.request_year), owner.name,vehicle.transported_content," +
                "cv.certification_type,sibasi.zone,ucsf.name )" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.ucsf ucsf " +
                "inner join ucsf.sibasi sibasi " +
                "inner join cv.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where bunch.id = :id " +
                "group by bunch.id", VehicleFoldersReportDTO.class)
                .setParameter("id", id_bunch)
                .getSingleResult();
    }
}
