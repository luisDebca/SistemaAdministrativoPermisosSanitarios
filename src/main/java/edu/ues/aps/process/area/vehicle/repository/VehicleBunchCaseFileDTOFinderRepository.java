package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface VehicleBunchCaseFileDTOFinderRepository {

    AbstractVehicleCaseFileDTO find(Long id_caseFile) throws NoResultException;

    List<AbstractVehicleCaseFileDTO> findAll();

    List<AbstractVehicleCaseFileDTO> findAll(Long id_owner);

    List<AbstractVehicleCaseFileDTO> findAll(Long id_owner, Long id_bunch);
}
