package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.base.repository.CaseFileRequestCountRepository;
import edu.ues.aps.process.base.model.CasefileSection;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;

@Repository
public class CaseFileVehicleStoreProcedureRepository implements CaseFileRequestCountRepository {

    private final SessionFactory sessionFactory;

    public CaseFileVehicleStoreProcedureRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Long getMaxRequestNumberCount() {
        Long requestDefault = executeProcedure("getDefaultRequestNumberForVehicle");
        Long requestMax = executeProcedure("getMaxRequestNumberForVehicle");
        return requestDefault >= requestMax ? requestDefault : requestMax;
    }

    @Override
    public Long getMaxRequestFolderCount() {
        Long folderDefault = executeProcedure("getDefaultRequestFolderForVehicle");
        Long folderMax = executeProcedure("getMaxRequestFolderForVehicle");
        return folderDefault >= folderMax ? folderDefault : folderMax;
    }

    @Override
    public CasefileSection appliesTo() {
        return CasefileSection.VEHICLE;
    }

    private Long executeProcedure(String procedure) {
        StoredProcedureQuery query = sessionFactory.getCurrentSession().createStoredProcedureQuery(procedure).
                registerStoredProcedureParameter("count", Long.class, ParameterMode.OUT);
        query.execute();
        return validateQueryResult(query.getOutputParameterValue("count"));
    }

}
