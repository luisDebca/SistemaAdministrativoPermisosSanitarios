package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO;

import java.util.List;

public interface VehicleBunchReportFinderRepository {

    List<AbstractVehicleCaseFileDTO> findDatasource(Long id_bunch);
}
