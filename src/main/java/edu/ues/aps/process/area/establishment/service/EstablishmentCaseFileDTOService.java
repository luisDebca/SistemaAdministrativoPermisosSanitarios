package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.dto.AbstractCasefileEstablishmentInfoDTO;
import edu.ues.aps.process.area.establishment.dto.AbstractCasefileInformationDTO;
import edu.ues.aps.process.area.establishment.dto.EmptyCasefileEstablishmentInfoDTO;
import edu.ues.aps.process.area.establishment.repository.EstablishmentCaseFileCaseFileDTORepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class EstablishmentCaseFileDTOService implements EstablishmentCaseFileDTOFinderService, EstablishmentCaseFileDTOInformationService {

    private final EstablishmentCaseFileCaseFileDTORepository finderRepository;

    public EstablishmentCaseFileDTOService(EstablishmentCaseFileCaseFileDTORepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractCasefileEstablishmentInfoDTO find(Long id_caseFile) {
        try {
            return finderRepository.find(id_caseFile);
        } catch (NoResultException e) {
            return new EmptyCasefileEstablishmentInfoDTO();
        }
    }

    @Override
    public List<AbstractCasefileInformationDTO> findAll(Long id_establishment) {
        return finderRepository.findAll(id_establishment);
    }

}
