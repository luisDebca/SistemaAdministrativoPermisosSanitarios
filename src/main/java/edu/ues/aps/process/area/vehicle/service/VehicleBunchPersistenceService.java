package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.model.AbstractVehicleBunch;

public interface VehicleBunchPersistenceService {

    AbstractVehicleBunch find(Long id_bunch);

    void update(AbstractVehicleBunch bunch);
}
