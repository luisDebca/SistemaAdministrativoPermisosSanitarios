package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.VehicleBunchFormDTO;

public interface VehicleCaseFilePersistenceService {

    void saveBunch(VehicleBunchFormDTO dto);

}
