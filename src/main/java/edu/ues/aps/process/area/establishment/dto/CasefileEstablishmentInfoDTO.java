package edu.ues.aps.process.area.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;

import java.time.LocalDateTime;

public class CasefileEstablishmentInfoDTO implements AbstractCasefileEstablishmentInfoDTO {

    private Long id_casefile;
    private Long id_establishment;
    private Long id_owner;
    private String casefile_request_number;
    private String casefile_request_code;
    private String casefile_request_year;
    private String casefile_request_folder;
    private boolean casefile_apply_payment;
    private CertificationType casefile_certification_type;
    private Integer casefile_certification_duration_in_years;
    private LocalDateTime casefile_certification_start_on;
    private String ucsf_name;
    private String sibasi_name;

    public CasefileEstablishmentInfoDTO(Long id_casefile, Long id_establishment, Long id_owner, String casefile_request_number, String casefile_request_code, String casefile_request_year, String casefile_request_folder,boolean casefile_apply_payment, CertificationType casefile_certification_type, Integer casefile_certification_duration_in_years, LocalDateTime casefile_certification_start_on, String ucsf_name, String sibasi_name) {
        this.id_casefile = id_casefile;
        this.id_establishment = id_establishment;
        this.id_owner = id_owner;
        this.casefile_request_number = casefile_request_number;
        this.casefile_request_code = casefile_request_code;
        this.casefile_request_year = casefile_request_year;
        this.casefile_request_folder = casefile_request_folder;
        this.casefile_apply_payment = casefile_apply_payment;
        this.casefile_certification_type = casefile_certification_type;
        this.casefile_certification_duration_in_years = casefile_certification_duration_in_years;
        this.casefile_certification_start_on = casefile_certification_start_on;
        this.ucsf_name = ucsf_name;
        this.sibasi_name = sibasi_name;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public Long getId_establishment() {
        return id_establishment;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public String getCasefile_request_number() {
        return casefile_request_number;
    }

    public String getCasefile_request_code() {
        return casefile_request_code;
    }

    public String getCasefile_request_year() {
        return casefile_request_year;
    }

    public String getCasefile_request_folder() {
        return casefile_request_folder;
    }

    public boolean isCasefile_apply_payment() {
        return casefile_apply_payment;
    }

    public String getCasefile_certification_type() {
        if (casefile_certification_type != null) {
            return casefile_certification_type.getCertificationType();
        } else {
            return CertificationType.INVALID.getCertificationType();
        }
    }

    public Integer getCasefile_certification_duration_in_years() {
        return casefile_certification_duration_in_years;
    }

    public LocalDateTime getCasefile_certification_start_on() {
        return casefile_certification_start_on;
    }

    public String getUcsf_name() {
        return ucsf_name;
    }

    public String getSibasi_name() {
        return sibasi_name;
    }

}
