package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.dto.RecordCoverDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class RecordCoverDataRepository implements RecordCoverFindDataRepository {

    private SessionFactory sessionFactory;

    public RecordCoverDataRepository(SessionFactory session) {
        this.sessionFactory = session;
    }

    public RecordCoverDTO getDataSource(Long id_caseFile) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.establishment.dto.RecordCoverDTO(" +
                "concat(casefile.request_folder,concat('-',casefile.request_year)),establishment.name," +
                "owner.name,sibasi.zone,ucsf.name,casefile.certification_type) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "left join establishment.owner owner " +
                "left join casefile.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "where casefile.id = :id", RecordCoverDTO.class).
                setParameter("id", id_caseFile)
                .getSingleResult();
    }
}