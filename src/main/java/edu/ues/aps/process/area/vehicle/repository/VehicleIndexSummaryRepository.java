package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.base.dto.AbstractRequestProcessIndexSummaryDTO;
import edu.ues.aps.process.area.base.repository.IndexSummaryRepository;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleIndexSummaryRepository implements IndexSummaryRepository {

    private final SessionFactory sessionFactory;

    public VehicleIndexSummaryRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractRequestProcessIndexSummaryDTO> findAllFromToday(ProcessIdentifier identifier) {
        return null;
    }

    @Override
    public List<AbstractRequestProcessIndexSummaryDTO> findAllFromThisWeek(ProcessIdentifier identifier) {
        return null;
    }

    @Override
    public List<AbstractRequestProcessIndexSummaryDTO> findAllFromThisMonth(ProcessIdentifier identifier) {
        return null;
    }

    @Override
    public Long countFromToday(ProcessIdentifier identifier) {
        return null;
    }

    @Override
    public Long countFromThisWeek(ProcessIdentifier identifier) {
        return null;
    }

    @Override
    public Long countFromThisMonth(ProcessIdentifier identifier) {
        return null;
    }
}
