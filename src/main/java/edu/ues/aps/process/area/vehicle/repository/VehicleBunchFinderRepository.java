package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.base.model.AbstractCasefile;

import java.util.List;

public interface VehicleBunchFinderRepository {

    List<AbstractCasefile> findAllInBunch(Long id_bunch);

}
