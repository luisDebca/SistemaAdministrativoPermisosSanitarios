package edu.ues.aps.process.area.vehicle.model;

import edu.ues.aps.process.base.model.AbstractAddress;
import edu.ues.aps.process.base.model.AbstractOwner;
import edu.ues.aps.process.base.model.Address;
import edu.ues.aps.process.base.model.OwnerEntity;

import java.util.List;

public interface AbstractVehicle {

    Long getId();

    void setId(Long id);

    String getLicense();

    void setLicense(String license);

    String getVehicle_type();

    void setVehicle_type(String vehicle_type);

    TransportedContentType getTransported_content();

    void setTransported_content(TransportedContentType transported_content);

    Integer getEmployees_number();

    void setEmployees_number(Integer employees_number);

    boolean isActive();

    void setActive(boolean active);

    AbstractOwner getOwner();

    void setOwner(OwnerEntity owner);

    List<CasefileVehicle> getCasefile_list();

    void setCasefile_list(List<CasefileVehicle> casefile_list);

    void addCasefile(CasefileVehicle casefile);

    void removeCasefile(CasefileVehicle casefile);

}
