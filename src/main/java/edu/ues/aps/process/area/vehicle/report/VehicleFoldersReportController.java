package edu.ues.aps.process.area.vehicle.report;

import edu.ues.aps.process.area.vehicle.service.VehicleFoldersReportClaseService;
import edu.ues.aps.process.area.vehicle.service.VehicleFoldersReportService;
import edu.ues.aps.process.base.report.AbstractReportController;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.Collections;

@Controller
@RequestMapping("/vehicle")
public class VehicleFoldersReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(VehicleBunchReportController.class);

    private static final String REPORT_NAME = "FolderVReport";

    private final VehicleFoldersReportService reportVehicleService;

    public VehicleFoldersReportController(VehicleFoldersReportService reportVehicleService) {
        this.reportVehicleService = reportVehicleService;
    }

    @GetMapping("/vehicle-folders-report.pdf")
    public String generateVehicleFoldersReport(@RequestParam Long id_bunch, ModelMap model) {
        logger.info("REPORT:: Generate report for vehicle bunch {}.", id_bunch);
        addReportRequiredSources(Collections.singletonList(reportVehicleService.findDataReport(id_bunch)));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }
}
