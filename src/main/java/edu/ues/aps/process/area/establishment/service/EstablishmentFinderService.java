package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.model.AbstractEstablishment;

public interface EstablishmentFinderService {

    AbstractEstablishment findById(Long id_establishment);

}
