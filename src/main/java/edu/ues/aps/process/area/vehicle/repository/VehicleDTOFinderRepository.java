package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO;
import edu.ues.aps.process.area.vehicle.dto.VehicleDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface VehicleDTOFinderRepository {

    List<AbstractVehicleDTO> findAllFromOwner(Long id_owner);

    List<AbstractVehicleDTO> findAll();

    List<AbstractVehicleDTO> findByPath(String path);

    List<AbstractVehicleDTO> findAllVehicles(Long id_bunch);

    AbstractVehicleDTO findByID(Long id_vehicle) throws NoResultException;

}
