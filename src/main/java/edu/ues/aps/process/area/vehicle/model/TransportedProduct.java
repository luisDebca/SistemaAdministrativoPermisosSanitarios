package edu.ues.aps.process.area.vehicle.model;

import javax.persistence.*;

@Entity
@Table(name = "TRANSPORTED_PRODUCT")
public class TransportedProduct implements AbstractTransportedProduct{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "product_name", nullable = false, length = 100)
    private String product;

    @Column(name = "description", length = 150)
    private String description;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_bunch")
    private VehicleBunch bunch;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getProduct() {
        return product;
    }

    @Override
    public void setProduct(String product) {
        this.product = product;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public VehicleBunch getBunch() {
        return bunch;
    }

    @Override
    public void setBunch(VehicleBunch bunch) {
        this.bunch = bunch;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        TransportedProduct that = (TransportedProduct) o;

        if (!product.equals(that.product)) return false;
        return description != null ? description.equals(that.description) : that.description == null;
    }

    @Override
    public int hashCode() {
        int result = product.hashCode();
        result = 31 * result + (description != null ? description.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "TransportedProduct{" +
                "id=" + id +
                ", product='" + product + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
