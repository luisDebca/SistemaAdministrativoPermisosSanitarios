package edu.ues.aps.process.area.vehicle.dto;

public class EmptyVehicleCaseFileDTO implements AbstractVehicleCaseFileDTO{

    @Override
    public Long getId_bunch() {
        return 0L;
    }

    @Override
    public Long getId_caseFile() {
        return 0L;
    }

    @Override
    public Long getId_vehicle() {
        return 0L;
    }

    @Override
    public Long getId_owner() {
        return 0L;
    }

    @Override
    public void setId_bunch(Long id_bunch) {

    }

    @Override
    public void setId_owner(Long id_owner) {

    }

    @Override
    public String getBunch_client_presented_name() {
        return "Name not found";
    }

    @Override
    public String getBunch_client_presented_telephone() {
        return "Phone not found";
    }

    @Override
    public String getBunch_distribution_company() {
        return "Distribution company not found";
    }

    @Override
    public String getBunch_distribution_destination() {
        return "Distribution destination not found";
    }

    @Override
    public String getBunch_marketing_place() {
        return "Marketing place not found";
    }

    @Override
    public String getBunch_address() {
        return "Address not found";
    }

    @Override
    public Integer getBunch_vehicle_count() {
        return 0;
    }

    @Override
    public Integer getCertification_duration_in_years() {
        return 0;
    }

    @Override
    public Boolean getApply_payment() {
        return false;
    }

    @Override
    public String getCertification_start_on() {
        return "Certification start date not found";
    }

    @Override
    public String getCertification_type() {
        return "Certification type not found";
    }

    @Override
    public String getCreation_date() {
        return "Creation date not found";
    }

    @Override
    public String getRequest_number() {
        return "Request number not found";
    }

    @Override
    public String getFolder_number() {
        return "Folder number not found";
    }

    @Override
    public String getStatus() {
        return "Status not found";
    }

    @Override
    public String getSibasi() {
        return "SIBASI not found";
    }

    @Override
    public String getUcsf() {
        return "UCSF not found";
    }

    @Override
    public Boolean getVehicle_active() {
        return false;
    }

    @Override
    public Integer getVehicle_employees_number() {
        return 0;
    }

    @Override
    public String getVehicle_license() {
        return "Vehicle license not found";
    }

    @Override
    public String getVehicle_transported_content() {
        return "Vehicle transported content not found";
    }

    @Override
    public String getVehicle_type() {
        return "Vehicle type not found";
    }

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public String getCurrent_process() {
        return "Not Found";
    }
}
