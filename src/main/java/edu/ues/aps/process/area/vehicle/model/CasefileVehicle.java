package edu.ues.aps.process.area.vehicle.model;

import edu.ues.aps.process.base.model.Casefile;

import javax.persistence.*;

@Entity
@Table(name = "CASEFILE_VEHICLE")
public class CasefileVehicle extends Casefile implements AbstractCasefileVehicle{

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_vehicle")
    private Vehicle vehicle;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_bunch")
    private VehicleBunch bunch;

    public Vehicle getVehicle() {
        return vehicle;
    }

    public void setVehicle(Vehicle vehicle) {
        this.vehicle = vehicle;
    }

    public VehicleBunch getBunch() {
        return bunch;
    }

    public void setBunch(VehicleBunch bunch) {
        this.bunch = bunch;
    }

    @Override
    public Integer getCertificationDurationInYears() {
        return 2;
    }

    @Override
    public String toString() {
        return "CasefileVehicle{" + super.toString() + "}";
    }
}
