package edu.ues.aps.process.area.establishment.dto;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class CasefileInformationDTO implements AbstractCasefileInformationDTO{

    private Long id_owner;
    private Long id_establishment;
    private Long id_casefile;
    private boolean casefile_apply_payment;
    private int casefile_duration;
    private LocalDateTime casefile_start_on;
    private CertificationType casefile_type;
    private String casefile_code;
    private String folder_number;
    private CasefileSection casefile_section;
    private CasefileStatusType casefile_status;
    private LocalDateTime casefile_date;
    private String sibasi;
    private String ucsf;
    private String establishment_name;
    private String owner_name;
    private String current_process;
    private ProcessIdentifier process;

    public CasefileInformationDTO(Long id_owner, Long id_casefile, LocalDateTime casefile_start_on, CertificationType casefile_type, String casefile_code, String folder_number, CasefileStatusType casefile_status, LocalDateTime casefile_date, String establishment_name, String current_process, ProcessIdentifier process) {
        this.id_owner = id_owner;
        this.id_casefile = id_casefile;
        this.casefile_start_on = casefile_start_on;
        this.casefile_type = casefile_type;
        this.casefile_code = casefile_code;
        this.casefile_status = casefile_status;
        this.casefile_date = casefile_date;
        this.establishment_name = establishment_name;
        this.current_process = current_process;
        this.folder_number = folder_number;
        this.process = process;
    }

    public CasefileInformationDTO(Long id_owner, Long id_casefile, boolean casefile_apply_payment, int casefile_duration, LocalDateTime casefile_start_on, CertificationType casefile_type, String casefile_code, String folder_number, CasefileSection casefile_section, CasefileStatusType casefile_status, LocalDateTime casefile_date, String sibasi, String ucsf, String establishment_name, String owner_name) {
        this.id_owner = id_owner;
        this.id_casefile = id_casefile;
        this.casefile_apply_payment = casefile_apply_payment;
        this.casefile_duration = casefile_duration;
        this.casefile_start_on = casefile_start_on;
        this.casefile_type = casefile_type;
        this.casefile_code = casefile_code;
        this.folder_number = folder_number;
        this.casefile_section = casefile_section;
        this.casefile_status = casefile_status;
        this.casefile_date = casefile_date;
        this.sibasi = sibasi;
        this.ucsf = ucsf;
        this.establishment_name = establishment_name;
        this.owner_name = owner_name;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public Long getId_establishment() {
        return id_establishment;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public boolean getCasefile_apply_payment() {
        return casefile_apply_payment;
    }

    public int getCasefile_duration() {
        return casefile_duration;
    }

    public String getCasefile_start_on() {
        if(casefile_start_on != null){
            return DatetimeUtil.parseDateToLongDateFormat(casefile_start_on.toLocalDate());
        }
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    public String getCasefile_start_on_html(){
        return DatetimeUtil.parseDatetimeToHtmlFormat(casefile_start_on);
    }

    public String getCasefile_type() {
        if(casefile_type != null){
            return casefile_type.getCertificationType();
        }
        return CertificationType.INVALID.getCertificationType();
    }

    public String getCasefile_code() {
        return casefile_code;
    }

    public String getCasefile_section() {
        if(casefile_section != null){
            return casefile_section.getCasefileSection();
        }
        return CasefileSection.INVALID.getCasefileSection();
    }

    public String getCasefile_status() {
        if(casefile_status != null){
            return casefile_status.getStatus_type();
        }
        return CasefileStatusType.INVALID.getStatus_type();
    }

    public String getCasefile_date() {
        if(casefile_date != null){
            return DatetimeUtil.parseDateToLongDateFormat(casefile_date.toLocalDate());
        }
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    public String getSibasi() {
        return sibasi;
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getCurrent_process() {
        return current_process;
    }

    public String getCurrent_process_type() {
        return process.name();
    }

    public String getFolder_number() {
        return folder_number;
    }
}
