package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class EstablishmentDTORepository implements EstablishmentDTOFinderRepository {

    private SessionFactory sessionFactory;

    public EstablishmentDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractEstablishmentDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.establishment.dto.EstablishmentDTO(estbl.id,owner.id," +
                "estbl.name,owner.name,estbl.type_detail,estblType.type,estbl.email,estbl.telephone,estbl.FAX,estbl.active," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name ))" +
                "from Establishment estbl " +
                "inner join estbl.owner owner " +
                "inner join estbl.type estblType " +
                "inner join estbl.address address", AbstractEstablishmentDTO.class)
                .getResultList();
    }

    @Override
    public AbstractEstablishmentDTO findById(Long id_establishment) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.establishment.dto.EstablishmentDTO(estbl.id,owner.id," +
                "estbl.name,owner.name,estblType.type,estbl.type_detail,estbl.commercial_register,estbl.capital," +
                "estbl.NIT,estbl.email,estbl.telephone,estbl.telephone_extra,estbl.FAX,estbl.maleEmployeeCount,estbl.femaleEmployeeCount,estbl.operations," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name),estbl.active)" +
                "from Establishment estbl " +
                "inner join estbl.owner owner " +
                "inner join estbl.type estblType " +
                "inner join estbl.address address " +
                "where estbl.id = :id", AbstractEstablishmentDTO.class)
                .setParameter("id", id_establishment)
                .getSingleResult();
    }

    @Override
    public AbstractEstablishmentDTO findByCaseFile(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.establishment.dto.EstablishmentDTO(" +
                "establishment.id,owner.id,establishment.name,owner.name,type.type,establishment.type_detail,establishment.commercial_register,establishment.capital, " +
                "establishment.NIT,establishment.email,establishment.telephone,establishment.telephone_extra,establishment.FAX,establishment.maleEmployeeCount,establishment.femaleEmployeeCount," +
                "establishment.operations,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name),establishment.active)" +
                "from CasefileEstablishment caseFile " +
                "inner join caseFile.establishment establishment " +
                "inner join establishment.address address " +
                "inner join establishment.type type " +
                "inner join establishment.owner owner " +
                "where caseFile.id = :id", AbstractEstablishmentDTO.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }

    @Override
    public List<AbstractEstablishmentDTO> findAllByOwner(Long id_owner) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.establishment.dto.EstablishmentDTO(estbl.id,owner.id," +
                "estbl.name,owner.name,estbl.type_detail,estblType.type,estbl.email,estbl.telephone,estbl.FAX,estbl.active," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name ))" +
                "from Establishment estbl " +
                "inner join estbl.owner owner " +
                "inner join estbl.type estblType " +
                "inner join estbl.address address " +
                "where owner.id = :id", AbstractEstablishmentDTO.class)
                .setParameter("id", id_owner)
                .getResultList();
    }

    @Override
    public List<AbstractEstablishmentDTO> findByParameter(String parameter) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.establishment.dto.EstablishmentDTO(" +
                "establishment.id,owner.id, establishment.name, establishment.type_detail,establishment.active," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name) ) " +
                "from Establishment establishment " +
                "inner join establishment.owner owner " +
                "inner join establishment.address address " +
                "where establishment.name like concat(concat('%',:param),'%') " +
                "or establishment.NIT like concat(concat('%',:param),'%') " +
                "or address.details like concat(concat('%',:param),'%') ", AbstractEstablishmentDTO.class)
                .setParameter("param", parameter)
                .getResultList();
    }
}
