package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.base.repository.CaseFileRequestCountRepository;
import edu.ues.aps.process.base.model.CasefileSection;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.ParameterMode;
import javax.persistence.StoredProcedureQuery;
import javax.persistence.criteria.CriteriaBuilder;

@Repository
public class CaseFileEstablishmentStoredProcedureRepository implements CaseFileRequestCountRepository {

    private final SessionFactory sessionFactory;

    public CaseFileEstablishmentStoredProcedureRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Long getMaxRequestNumberCount() {
        Long requestDefault = executeProcedure("getDefaultRequestNumberForEstablishment");
        Long requestMax = executeProcedure("getMaxRequestNumberForEstablishment");
        return requestDefault >= requestMax ? requestDefault : requestMax;
    }

    @Override
    public Long getMaxRequestFolderCount() {
        Long folderDefault = executeProcedure("getDefaultRequestFolderForEstablishment");
        Long folderMax = executeProcedure("getMaxRequestFolderForEstablishment");
        return folderDefault >= folderMax ? folderDefault : folderMax;
    }

    @Override
    public CasefileSection appliesTo() {
        return CasefileSection.ESTABLISHMENT;
    }

    private Long executeProcedure(String procedure) {
        StoredProcedureQuery query = sessionFactory.getCurrentSession().createStoredProcedureQuery(procedure).
                registerStoredProcedureParameter("count", Long.class, ParameterMode.OUT);
        query.execute();
        return validateQueryResult(query.getOutputParameterValue("count"));
    }

}
