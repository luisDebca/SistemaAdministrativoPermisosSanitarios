package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.model.AbstractCasefileEstablishment;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.CertificationType;

public interface EstablishmentCaseFileAdderService {

    AbstractCasefile add(CertificationType type, Long id_establishment);

    AbstractCasefile add(AbstractCasefileEstablishment caseFile, Long id_establishment);
}
