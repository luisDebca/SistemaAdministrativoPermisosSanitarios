package edu.ues.aps.process.area.vehicle.dto;

public interface AbstractVehicleCaseFileDTO {

    Long getId_bunch();

    Long getId_caseFile();

    Long getId_vehicle();

    Long getId_owner();

    void setId_bunch(Long id_bunch);

    void setId_owner(Long id_owner);

    String getBunch_client_presented_name();

    String getBunch_client_presented_telephone();

    String getBunch_distribution_company();

    String getBunch_distribution_destination();

    String getBunch_marketing_place();

    String getBunch_address();

    Integer getBunch_vehicle_count();

    Integer getCertification_duration_in_years();

    Boolean getApply_payment();

    String getCertification_start_on();

    String getCertification_type();

    String getCreation_date();

    String getRequest_number();

    String getFolder_number();

    String getStatus();

    String getSibasi();

    String getUcsf();

    Boolean getVehicle_active();

    Integer getVehicle_employees_number();

    String getVehicle_license();

    String getVehicle_transported_content();

    String getVehicle_type();

    String getOwner_name();

    String getCurrent_process();
}
