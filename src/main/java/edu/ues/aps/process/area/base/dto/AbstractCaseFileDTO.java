package edu.ues.aps.process.area.base.dto;

public interface AbstractCaseFileDTO {

    Long getId_casefile();

    void setId_casefile(Long id_casefile);

    String getRequest_identifier();

    String getRequest_folder();

    boolean isApply_payment();

    String getCertification_type();

    String getCasefile_section();

    String getStatus();

    Integer getCertification_duration();

    String getCertification_start();

    String getCreation_date();

    String getUcsf();

    String getSibasi();

    String getCurrent_process();
}
