package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.dto.RecordCoverDTO;

public interface RecordCoverFindDataRepository{

    RecordCoverDTO getDataSource(Long id_caseFile);

}