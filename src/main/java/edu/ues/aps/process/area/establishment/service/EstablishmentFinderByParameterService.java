package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO;

import java.util.List;

public interface EstablishmentFinderByParameterService {

    List<AbstractEstablishmentDTO> findEstablishmentByParameter(String parameter);
}
