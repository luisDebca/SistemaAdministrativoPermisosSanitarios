package edu.ues.aps.process.area.base.dto;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;

public class InfoDTO implements AbstractInfo {

    private Long id;
    private String folder;
    private String request;
    private CasefileSection section;
    private CertificationType type;
    private LocalDateTime start;
    private LocalDateTime created;
    private CasefileStatusType status;
    private String ucsf;
    private String sibasi;
    private Integer duration;
    private Boolean payment;
    private Long processId;
    private String process;
    private Long target;
    private String name;
    private String owner;

    public InfoDTO(String folder, String request, CasefileSection section, CertificationType type, LocalDateTime start, LocalDateTime created, CasefileStatusType status, String ucsf, String sibasi, Integer duration, Boolean payment, Long processId, String process, Long target, String name, String owner) {
        this.folder = folder;
        this.request = request;
        this.section = section;
        this.type = type;
        this.start = start;
        this.created = created;
        this.status = status;
        this.ucsf = ucsf;
        this.sibasi = sibasi;
        this.duration = duration;
        this.payment = payment;
        this.processId = processId;
        this.process = process;
        this.target = target;
        this.name = name;
        this.owner = owner;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getFolder() {
        return folder;
    }

    public String getRequest() {
        return request;
    }

    public String getSection() {
        if (section == null) return CasefileSection.INVALID.getCasefileSection();
        return section.getCasefileSection();
    }

    public String getSectionType() {
        if (section == null) return CasefileSection.INVALID.name().toLowerCase();
        return section.name().toLowerCase();
    }

    public String getType() {
        if (type == null) return CertificationType.INVALID.getCertificationType();
        return type.getCertificationType();
    }

    public String getStart() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(start);
    }

    public String getEnd() {
        if (start == null) return DatetimeUtil.DATETIME_NOT_DEFINED;
        return DatetimeUtil.parseDatetimeToLongDateFormat(start.plusYears(duration).withHour(0).withMinute(0).withSecond(0));
    }

    public String getCreated() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(created);
    }

    public String getStatus() {
        if (status == null) return CasefileStatusType.INVALID.getStatus_type();
        return status.getStatus_type();
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getSibasi() {
        return sibasi;
    }

    public Integer getDuration() {
        return duration;
    }

    public Boolean getPayment() {
        return payment;
    }

    public Long getProcessId() {
        return processId;
    }

    public String getProcess() {
        return process;
    }

    public Long getTarget() {
        return target;
    }

    public void setTarget(Long target) {
        this.target = target;
    }

    public int getProcessProgress() {
        return Math.round((processId * 10) / 14);
    }

    public double getCertificationProgress() {
        if (start == null) return 0;
        return Math.round(100.0 * (getCertificationDays() * 1.0 / (duration * 365.0)));
    }

    public long getCertificationDays() {
        if (start == null) return 0;
        return start.toLocalDate().until(LocalDate.now(), ChronoUnit.DAYS);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }
}
