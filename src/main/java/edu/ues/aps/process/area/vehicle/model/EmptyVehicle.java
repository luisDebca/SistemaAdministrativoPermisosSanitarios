package edu.ues.aps.process.area.vehicle.model;

import edu.ues.aps.process.base.model.*;

import java.util.List;

public class EmptyVehicle implements AbstractVehicle{

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getLicense() {
        return "Vehicle licence not found";
    }

    @Override
    public String getVehicle_type() {
        return "Vehicle type not found";
    }

    @Override
    public TransportedContentType getTransported_content() {
        return TransportedContentType.INVALID;
    }

    @Override
    public Integer getEmployees_number() {
        return 0;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setLicense(String license) {

    }

    @Override
    public void setVehicle_type(String vehicle_type) {

    }

    @Override
    public void setTransported_content(TransportedContentType transported_content) {

    }

    @Override
    public void setEmployees_number(Integer employees_number) {

    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void setActive(boolean active) {

    }

    @Override
    public AbstractOwner getOwner() {
        return new EmptyOwner();
    }

    @Override
    public void setOwner(OwnerEntity owner) {

    }

    @Override
    public List<CasefileVehicle> getCasefile_list() {
        return null;
    }

    @Override
    public void setCasefile_list(List<CasefileVehicle> casefile_list) {

    }

    @Override
    public void addCasefile(CasefileVehicle casefile) {

    }

    @Override
    public void removeCasefile(CasefileVehicle casefile) {

    }

    @Override
    public String toString() {
        return "EmptyVehicle{}";
    }
}
