package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.model.AbstractVehicle;

import javax.persistence.NoResultException;

public interface VehiclePersistenceRepository {

    AbstractVehicle findByID(Long id_vehicle) throws NoResultException;

    void save(AbstractVehicle vehicle);

    void delete(AbstractVehicle vehicle);

}
