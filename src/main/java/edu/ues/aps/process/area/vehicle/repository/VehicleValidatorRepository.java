package edu.ues.aps.process.area.vehicle.repository;

public interface VehicleValidatorRepository {

    Long isLicenseUnique(String license);

    Boolean isLicenseEquals(String license, Long id_vehicle);
}
