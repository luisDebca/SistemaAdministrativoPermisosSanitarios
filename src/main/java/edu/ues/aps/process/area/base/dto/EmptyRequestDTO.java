package edu.ues.aps.process.area.base.dto;

public class EmptyRequestDTO implements AbstractRequestDTO{

    @Override
    public String getFolder_number() {
        return "Not found";
    }

    @Override
    public String getRequest_number() {
        return "Not found";
    }

    @Override
    public String getResolution_number() {
        return "Not found";
    }
}
