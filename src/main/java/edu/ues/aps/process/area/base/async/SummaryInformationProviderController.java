package edu.ues.aps.process.area.base.async;

import edu.ues.aps.process.area.base.dto.AbstractSummaryDTO;
import edu.ues.aps.process.area.base.service.SummaryDTOFinderService;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/summary")
public class SummaryInformationProviderController {

    private final SummaryDTOFinderService finderService;

    public SummaryInformationProviderController(SummaryDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "request/establishment/today", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractSummaryDTO> findTodayEstablishmentSummary(@RequestParam ProcessIdentifier process) {
        return finderService.findToday(process, CasefileSection.ESTABLISHMENT);
    }

    @GetMapping(value = "request/establishment/week", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractSummaryDTO> findWeekEstablishmentSummary(@RequestParam ProcessIdentifier process) {
        return finderService.findWeek(process, CasefileSection.ESTABLISHMENT);
    }

    @GetMapping(value = "request/establishment/month", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractSummaryDTO> findMonthEstablishmentSummary(@RequestParam ProcessIdentifier process) {
        return finderService.findMonth(process, CasefileSection.ESTABLISHMENT);
    }

    @GetMapping(value = "/request/vehicle/today", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractSummaryDTO> findTodayVehicleSummary(@RequestParam ProcessIdentifier process) {
        return finderService.findToday(process, CasefileSection.VEHICLE);
    }

    @GetMapping(value = "/request/vehicle/week", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractSummaryDTO> findWeekVehicleSummary(@RequestParam ProcessIdentifier process) {
        return finderService.findWeek(process, CasefileSection.VEHICLE);
    }

    @GetMapping(value = "/request/vehicle/month", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractSummaryDTO> findMonthVehicleSummary(@RequestParam ProcessIdentifier process) {
        return finderService.findMonth(process, CasefileSection.VEHICLE);
    }
}
