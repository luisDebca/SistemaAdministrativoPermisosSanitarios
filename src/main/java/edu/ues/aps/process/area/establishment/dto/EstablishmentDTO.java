package edu.ues.aps.process.area.establishment.dto;

public class EstablishmentDTO implements AbstractEstablishmentDTO {

    private Long establishment_id;
    private Long owner_id;
    private String establishment_name;
    private String establishment_owner_name;
    private String establishment_type;
    private String establishment_type_detail;
    private String commercial_register;
    private Double establishment_capital;
    private String establishment_nit;
    private String establishment_email;
    private String establishment_telephone;
    private String establishment_telephone_extra;
    private String establishment_fax;
    private Integer establishment_male_employee_count;
    private Integer establishment_female_employee_count;
    private String establishment_operations;
    private String establishment_address;
    private Boolean active;

    //For owner establishment list
    public EstablishmentDTO(Long establishment_id, Long owner_id, String establishment_name, String establishment_type_detail, Boolean active, String establishment_address) {
        this.establishment_id = establishment_id;
        this.owner_id = owner_id;
        this.establishment_name = establishment_name;
        this.establishment_type_detail = establishment_type_detail;
        this.establishment_address = establishment_address;
        this.active = active;
    }

    //For establishment list and owner establishment list
    public EstablishmentDTO(Long establishment_id, Long owner_id, String establishment_name, String establishment_owner_name, String establishment_type_detail, String establishment_type, String establishment_email, String establishment_telephone, String establishment_fax, Boolean active, String establishment_address) {
        this.establishment_id = establishment_id;
        this.owner_id = owner_id;
        this.establishment_name = establishment_name;
        this.establishment_owner_name = establishment_owner_name;
        this.establishment_type_detail = establishment_type_detail;
        this.establishment_type = establishment_type;
        this.establishment_email = establishment_email;
        this.establishment_telephone = establishment_telephone;
        this.establishment_fax = establishment_fax;
        this.active = active;
        this.establishment_address = establishment_address;
    }

    //For establishment information
    public EstablishmentDTO(Long establishment_id, Long owner_id, String establishment_name, String establishment_owner_name, String establishment_type, String establishment_type_detail, String commercial_register, Double establishment_capital, String establishment_nit, String establishment_email, String establishment_telephone, String establishment_telephone_extra, String establishment_fax, Integer establishment_male_employee_count, Integer establishment_female_employee_count, String establishment_operations, String establishment_address, Boolean active) {
        this.establishment_id = establishment_id;
        this.owner_id = owner_id;
        this.establishment_name = establishment_name;
        this.establishment_owner_name = establishment_owner_name;
        this.establishment_type = establishment_type;
        this.establishment_type_detail = establishment_type_detail;
        this.commercial_register = commercial_register;
        this.establishment_capital = establishment_capital;
        this.establishment_nit = establishment_nit;
        this.establishment_email = establishment_email;
        this.establishment_telephone = establishment_telephone;
        this.establishment_telephone_extra = establishment_telephone_extra;
        this.establishment_fax = establishment_fax;
        this.establishment_male_employee_count = establishment_male_employee_count;
        this.establishment_female_employee_count = establishment_female_employee_count;
        this.establishment_operations = establishment_operations;
        this.establishment_address = establishment_address;
        this.active = active;
    }

    public Long getEstablishment_id() {
        return establishment_id;
    }

    public Long getOwner_id() {
        return owner_id;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_owner_name() {
        return establishment_owner_name;
    }

    public String getEstablishment_type() {
        return establishment_type;
    }

    public String getEstablishment_type_detail() {
        return establishment_type_detail;
    }

    public String getCommercial_register() {
        return commercial_register;
    }

    public Double getEstablishment_capital() {
        return establishment_capital;
    }

    public String getEstablishment_nit() {
        return establishment_nit;
    }

    public String getEstablishment_email() {
        return establishment_email;
    }

    public String getEstablishment_telephone() {
        return establishment_telephone;
    }

    public String getEstablishment_fax() {
        return establishment_fax;
    }

    public Integer getEstablishment_male_employee_count() {
        return establishment_male_employee_count;
    }

    public Integer getEstablishment_female_employee_count() {
        return establishment_female_employee_count;
    }

    public String getEstablishment_operations() {
        return establishment_operations;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getEstablishment_telephone_extra() {
        return establishment_telephone_extra;
    }

    public Boolean isActive() {
        return active;
    }
}
