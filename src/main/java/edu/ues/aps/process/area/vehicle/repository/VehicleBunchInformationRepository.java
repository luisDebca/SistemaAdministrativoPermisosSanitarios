package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.model.TransportedContentType;
import edu.ues.aps.process.base.model.CertificationType;

import java.util.List;

public interface VehicleBunchInformationRepository {

    List<String> findAllLicenses(Long id_bunch);

    List<TransportedContentType> findAllContents(Long id_bunch);

    List<String> findAllTypes(Long id_bunch);

    List<CertificationType> findAllCertificationTypes(Long id_bunch);


    List<Long> findAllCaseFileId(Long id_bunch);
}
