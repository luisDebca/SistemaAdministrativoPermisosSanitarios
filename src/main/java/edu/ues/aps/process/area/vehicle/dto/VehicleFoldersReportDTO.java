package edu.ues.aps.process.area.vehicle.dto;


import edu.ues.aps.process.area.vehicle.model.TransportedContentType;
import edu.ues.aps.process.base.model.CertificationType;

public class VehicleFoldersReportDTO {

    private String numExpediente;
    private String nomPropietario;
    private TransportedContentType tipoVehiculo;
    private CertificationType ingreso;
    private String sibasi;
    private String ucsf;

    public VehicleFoldersReportDTO(String numExpediente, String nomPropietario, TransportedContentType tipoVehiculo, CertificationType ingreso, String sibasi, String ucsf) {
        this.numExpediente = numExpediente;
        this.nomPropietario = nomPropietario;
        this.tipoVehiculo = tipoVehiculo;
        this.ingreso = ingreso;
        this.sibasi = sibasi;
        this.ucsf = ucsf;
    }

    public String getNumExpediente() {
        return numExpediente;
    }

    public void setNumExpediente(String numExpediente) {
        this.numExpediente = numExpediente;
    }

    public String getNomPropietario() {
        return nomPropietario;
    }

    public void setNomPropietario(String nomPropietario) {
        this.nomPropietario = nomPropietario;
    }

    public String getTipoVehiculo() {
        return tipoVehiculo.getTransportedContentType();
    }

    public void setTipoVehiculo(TransportedContentType tipoVehiculo) {
        this.tipoVehiculo = tipoVehiculo;
    }

    public String getIngreso() {
        return ingreso.getCertificationType();
    }

    public void setIngreso(CertificationType ingreso) {
        this.ingreso = ingreso;
    }

    public String getSibasi() {
        return sibasi;
    }

    public void setSibasi(String sibasi) {
        this.sibasi = sibasi;
    }

    public String getUcsf() {
        return ucsf;
    }

    public void setUcsf(String ucsf) {
        this.ucsf = ucsf;
    }
}
