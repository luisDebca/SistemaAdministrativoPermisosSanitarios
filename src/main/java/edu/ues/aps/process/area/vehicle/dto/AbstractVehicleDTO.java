package edu.ues.aps.process.area.vehicle.dto;

public interface AbstractVehicleDTO {

    Long getId_vehicle();

    Long getId_owner();

    String getVehicle_license();

    Integer getVehicle_employees_number();

    String getVehicle_transported_content();

    String getVehicle_type();

    String getOwner_name();

    Boolean getActive();
}
