package edu.ues.aps.process.area.vehicle.async;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO;
import edu.ues.aps.process.area.vehicle.service.VehicleBunchCaseFileDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class VehicleCaseFileInformationProviderAsyncController {

    private static final Logger logger = LogManager.getLogger(VehicleCaseFileInformationProviderAsyncController.class);

    private final VehicleBunchCaseFileDTOFinderService finderService;

    public VehicleCaseFileInformationProviderAsyncController(VehicleBunchCaseFileDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/vehicle/case-file/bunch/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractVehicleCaseFileDTO> findAllCaseFilesFromBunch(@RequestParam Long id_owner, @RequestParam Long id_bunch) {
        logger.info("GET:: Find all request for owner {} in bunch {}", id_owner, id_bunch);
        return finderService.findAll(id_owner, id_bunch);
    }
}
