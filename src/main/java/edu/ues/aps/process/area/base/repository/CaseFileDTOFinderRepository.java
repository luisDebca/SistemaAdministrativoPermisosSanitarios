package edu.ues.aps.process.area.base.repository;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface CaseFileDTOFinderRepository {

    AbstractCaseFileDTO findById(Long id_caseFile) throws NoResultException;

    List<AbstractCaseFileDTO> findByIdentifier(String identifier) throws NoResultException;

    List<AbstractCaseFileDTO> findAll();
}
