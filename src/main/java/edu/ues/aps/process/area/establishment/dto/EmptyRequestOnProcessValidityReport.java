package edu.ues.aps.process.area.establishment.dto;

import java.time.LocalDate;

public class EmptyRequestOnProcessValidityReport implements AbstractRequestOnProcessValidityReport {

    @Override
    public String getClient_name() {
        return "Client name not found";
    }

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public String getEstablishment_name() {
        return "Establishment name not found";
    }

    @Override
    public String getEstablishment_address() {
        return "Establishment address not found";
    }

    @Override
    public String getRequest_process_start() {
        return "Date not found";
    }

    @Override
    public String getRequest_number() {
        return "00000-00";
    }

    @Override
    public String getRequest_type() {
        return "Invalid";
    }

    @Override
    public String getCurrent_date() {
        return LocalDate.now().toString();
    }

    @Override
    public String getUser_name() {
        return "User name not found";
    }

    @Override
    public String getUser_charge() {
        return "User charge not found";
    }

    @Override
    public void setClient_name(String client_name) {

    }

    @Override
    public void setUser_name(String user_name) {

    }

    @Override
    public void setUser_charge(String user_charge) {

    }
}
