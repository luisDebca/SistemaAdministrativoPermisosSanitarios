package edu.ues.aps.process.area.vehicle.controller;

import edu.ues.aps.process.area.vehicle.model.VehicleBunch;
import edu.ues.aps.process.area.vehicle.service.VehicleBunchPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class VehicleBunchPersistenceController {

    private static final Logger logger = LogManager.getLogger(VehicleBunchPersistenceController.class);

    private static final String BUNCH_FORM = "vehicle_bunch_form";

    private final VehicleBunchPersistenceService persistenceService;

    public VehicleBunchPersistenceController(VehicleBunchPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping({"/owner/{id_owner}/vehicle/casefile/bunch/{id_bunch}/edit","/vehicle/case-file/bunch/{id_bunch}/edit"})
    public String showVehicleBunchForm(@PathVariable Long id_bunch, @PathVariable(required = false) Long id_owner, ModelMap model) {
        logger.info("GET:: Show vehicle bunch {} edit form.");
        model.addAttribute("bunch", persistenceService.find(id_bunch));
        model.addAttribute("id_owner", id_owner);
        return BUNCH_FORM;
    }

    @PostMapping({"/owner/{id_owner}/vehicle/casefile/bunch/{id_bunch}/edit","/vehicle/case-file/bunch/{id_bunch}/edit"})
    public String updateVehicleBunch(@PathVariable(required = false) Long id_owner, @PathVariable Long id_bunch, @ModelAttribute VehicleBunch bunch) {
        logger.info("POST:: Update vehicle bunch {}.", id_bunch);
        persistenceService.update(bunch);
        if(id_owner == null){
            return "redirect:/vehicle/casefile/list";
        }else{
            return String.format("redirect:/owner/%d/vehicle/casefile/list", id_owner);
        }
    }
}
