package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.model.AbstractVehicle;
import edu.ues.aps.process.area.vehicle.model.AbstractVehicleBunch;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.AbstractClient;
import edu.ues.aps.process.base.model.AbstractOwner;
import edu.ues.aps.process.base.model.Client;

import java.util.List;

public interface VehicleBunchFinderService {

    AbstractVehicleBunch find(Long id_bunch);

    List<AbstractCasefile> findAllRequest(Long id_bunch);

}
