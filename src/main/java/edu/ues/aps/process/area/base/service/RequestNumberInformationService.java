package edu.ues.aps.process.area.base.service;

import edu.ues.aps.process.area.base.dto.AbstractRequestDTO;

import java.util.List;

public interface RequestNumberInformationService {

    AbstractRequestDTO find(Long id_caseFile);

    AbstractRequestDTO getFirst(Long id_bunch);

    List<AbstractRequestDTO> findAll(Long id_bunch);
}
