package edu.ues.aps.process.area.establishment.dto;

public interface AbstractEstablishmentCaseFileDTO {

    Long getId_casefile();

    void setId_caseFile(Long id_caseFile);

    Long getId_establishment();

    void setId_establishment(Long id_establishment);

    Long getId_owner();

    void setId_owner(Long id_owner);

    String getFolder_number();

    String getRequest_code();

    String getCertification_type();

    String getUcsf();

    String getSibasi();

    String getEstablishment_name();

    String getEstablishment_type();

    String getEstablishment_section();

    String getEstablishment_address();

    String getOwner_name();

    Boolean isRequest_validate();
}
