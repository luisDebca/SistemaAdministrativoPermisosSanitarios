package edu.ues.aps.process.area.vehicle.dto;

public interface AbstractVehicleBunchDTO {

    Long getId_bunch();

    void setId_bunch(Long id_bunch);

    Long getId_owner();

    String getOwner_name();

    String getClient_presented_name();

    String getClient_presented_telephone();

    String getDistribution_company();

    String getDistribution_destination();

    String getMarketing_place();

    Integer getVehicle_count();

    String getAddress_details();

    Long getCaseFiles_count();

    String getFolder_number();

}
