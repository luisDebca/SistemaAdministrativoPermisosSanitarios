package edu.ues.aps.process.area.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;

import java.time.LocalDateTime;

public class EmptyCasefileEstablishmentInfoDTO implements AbstractCasefileEstablishmentInfoDTO {

    @Override
    public Long getId_casefile() {
        return 0L;
    }

    @Override
    public Long getId_establishment() {
        return 0L;
    }

    @Override
    public Long getId_owner() {
        return 0L;
    }

    @Override
    public String getCasefile_request_number() {
        return "0000";
    }

    @Override
    public String getCasefile_request_code() {
        return "0";
    }

    @Override
    public String getCasefile_request_year() {
        return "00";
    }

    @Override
    public boolean isCasefile_apply_payment() {
        return false;
    }

    @Override
    public String getCasefile_certification_type() {
        return CertificationType.INVALID.getCertificationType();
    }

    @Override
    public Integer getCasefile_certification_duration_in_years() {
        return 0;
    }

    @Override
    public LocalDateTime getCasefile_certification_start_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00, 00);
    }

    @Override
    public String getUcsf_name() {
        return "UCSF name not found";
    }

    @Override
    public String getSibasi_name() {
        return "SIBASI name not found";
    }

    @Override
    public String getCasefile_request_folder() {
        return "Case file folder number not found";
    }
}
