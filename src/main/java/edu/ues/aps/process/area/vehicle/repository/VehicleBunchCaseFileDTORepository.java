package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class VehicleBunchCaseFileDTORepository implements VehicleBunchCaseFileDTOFinderRepository, VehicleBunchReportFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleBunchCaseFileDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractVehicleCaseFileDTO find(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleCaseFileDTO(" +
                "bunch.id,vehicle.id,owner.id,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company,bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name),bunch.vehicle_count," +
                "casefile.certification_duration_in_years,casefile.apply_payment,casefile.certification_start_on,casefile.certification_type,casefile.creation_date," +
                "concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year),concat(concat(casefile.request_folder,'-'),casefile.request_year)," +
                "casefile.status,sibasi.zone,ucsf.name,vehicle.active,vehicle.employees_number,vehicle.license,vehicle.transported_content,vehicle.vehicle_type,owner.name)" +
                "from VehicleBunch bunch " +
                "inner join bunch.address address " +
                "inner join bunch.casefileVehicles casefile " +
                "left join casefile.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join casefile.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where casefile.id = :id",AbstractVehicleCaseFileDTO.class)
                .setParameter("id",id_caseFile)
                .getSingleResult();
    }

    @Override
    public List<AbstractVehicleCaseFileDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleCaseFileDTO(" +
                "bunch.id,casefile.id,vehicle.id,owner.id," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "casefile.certification_type,concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year),concat(concat(casefile.request_folder,'-'),casefile.request_year)," +
                "casefile.status,vehicle.active,vehicle.employees_number,vehicle.license,vehicle.transported_content,vehicle.vehicle_type,owner.name) " +
                "from VehicleBunch bunch " +
                "inner join bunch.address address " +
                "inner join bunch.casefileVehicles casefile " +
                "inner join casefile.vehicle vehicle " +
                "inner join vehicle.owner owner",AbstractVehicleCaseFileDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractVehicleCaseFileDTO> findAll(Long id_owner) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleCaseFileDTO(" +
                "bunch.id,casefile.id,vehicle.id," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name),bunch.vehicle_count," +
                "casefile.certification_type,concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year),concat(concat(casefile.request_folder,'-'),casefile.request_year)," +
                "casefile.status,process.process) " +
                "from VehicleBunch bunch " +
                "inner join bunch.address address " +
                "inner join bunch.casefileVehicles casefile " +
                "inner join casefile.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where owner.id = :id " +
                "and record.active = true " +
                "group by owner.id",AbstractVehicleCaseFileDTO.class)
                .setParameter("id",id_owner)
                .getResultList();
    }

    @Override
    public List<AbstractVehicleCaseFileDTO> findAll(Long id_owner, Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleCaseFileDTO(" +
                "bunch.id,casefile.id,vehicle.id,owner.id," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "casefile.certification_type,concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year),concat(concat(casefile.request_folder,'-'),casefile.request_year)," +
                "casefile.status,vehicle.active,vehicle.employees_number,vehicle.license,vehicle.transported_content,vehicle.vehicle_type,owner.name) " +
                "from VehicleBunch bunch " +
                "inner join bunch.address address " +
                "inner join bunch.casefileVehicles casefile " +
                "inner join casefile.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where owner.id = :id_owner " +
                "and bunch.id = :id_bunch",AbstractVehicleCaseFileDTO.class)
                .setParameter("id_owner",id_owner)
                .setParameter("id_bunch",id_bunch)
                .getResultList();
    }

    @Override
    public List<AbstractVehicleCaseFileDTO> findDatasource(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleCaseFileDTO(" +
                "concat(concat(concat(concat('06',cv.request_code),cv.request_number),'-'),cv.request_year),cv.certification_type," +
                "vehicle.employees_number,vehicle.license,vehicle.transported_content,vehicle.vehicle_type,owner.name ) " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where bunch.id = :id " +
                "group by vehicle.id " +
                "order by vehicle.license", AbstractVehicleCaseFileDTO.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }
}
