package edu.ues.aps.process.area.vehicle.dto;

import edu.ues.aps.process.area.vehicle.model.TransportedContentType;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class VehicleCaseFileDTO implements AbstractVehicleCaseFileDTO {

    private Long id_bunch;
    private Long id_caseFile;
    private Long id_vehicle;
    private Long id_owner;
    private String bunch_client_presented_name;
    private String bunch_client_presented_telephone;
    private String bunch_distribution_company;
    private String bunch_distribution_destination;
    private String bunch_marketing_place;
    private String bunch_address;
    private Integer bunch_vehicle_count;
    private Integer certification_duration_in_years;
    private Boolean apply_payment;
    private LocalDateTime certification_start_on;
    private CertificationType certification_type;
    private LocalDateTime creation_date;
    private String request_number;
    private String folder_number;
    private CasefileStatusType status;
    private String sibasi;
    private String ucsf;
    private Boolean vehicle_active;
    private Integer vehicle_employees_number;
    private String vehicle_license;
    private TransportedContentType vehicle_transported_content;
    private String vehicle_type;
    private String owner_name;
    private String current_process;

    public VehicleCaseFileDTO() {
    }

    //Find by caseFile ID
    public VehicleCaseFileDTO(Long id_bunch, Long id_vehicle, Long id_owner, String bunch_client_presented_name, String bunch_client_presented_telephone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Integer bunch_vehicle_count, Integer certification_duration_in_years, Boolean apply_payment, LocalDateTime certification_start_on, CertificationType certification_type, LocalDateTime creation_date, String request_number, String folder_number, CasefileStatusType status, String sibasi, String ucsf, Boolean vehicle_active, Integer vehicle_employees_number, String vehicle_license, TransportedContentType vehicle_transported_content, String vehicle_type, String owner_name) {
        this.id_bunch = id_bunch;
        this.id_vehicle = id_vehicle;
        this.id_owner = id_owner;
        this.bunch_client_presented_name = bunch_client_presented_name;
        this.bunch_client_presented_telephone = bunch_client_presented_telephone;
        this.bunch_distribution_company = bunch_distribution_company;
        this.bunch_distribution_destination = bunch_distribution_destination;
        this.bunch_marketing_place = bunch_marketing_place;
        this.bunch_address = bunch_address;
        this.bunch_vehicle_count = bunch_vehicle_count;
        this.certification_duration_in_years = certification_duration_in_years;
        this.apply_payment = apply_payment;
        this.certification_start_on = certification_start_on;
        this.certification_type = certification_type;
        this.creation_date = creation_date;
        this.request_number = request_number;
        this.folder_number = folder_number;
        this.status = status;
        this.sibasi = sibasi;
        this.ucsf = ucsf;
        this.vehicle_active = vehicle_active;
        this.vehicle_employees_number = vehicle_employees_number;
        this.vehicle_license = vehicle_license;
        this.vehicle_transported_content = vehicle_transported_content;
        this.vehicle_type = vehicle_type;
        this.owner_name = owner_name;
    }

    //Find all
    public VehicleCaseFileDTO(Long id_bunch, Long id_caseFile, Long id_vehicle, Long id_owner, String bunch_address, CertificationType certification_type, String request_number, String folder_number, CasefileStatusType status, Boolean vehicle_active, Integer vehicle_employees_number, String vehicle_license, TransportedContentType vehicle_transported_content, String vehicle_type, String owner_name) {
        this.id_bunch = id_bunch;
        this.id_caseFile = id_caseFile;
        this.id_vehicle = id_vehicle;
        this.id_owner = id_owner;
        this.bunch_address = bunch_address;
        this.certification_type = certification_type;
        this.request_number = request_number;
        this.folder_number = folder_number;
        this.status = status;
        this.vehicle_active = vehicle_active;
        this.vehicle_employees_number = vehicle_employees_number;
        this.vehicle_license = vehicle_license;
        this.vehicle_transported_content = vehicle_transported_content;
        this.vehicle_type = vehicle_type;
        this.owner_name = owner_name;
    }

    //Find all for owner
    public VehicleCaseFileDTO(Long id_bunch, Long id_caseFile, Long id_vehicle, String bunch_address, Integer bunch_vehicle_count, CertificationType certification_type, String request_number, String folder_number, CasefileStatusType status, String current_process) {
        this.id_bunch = id_bunch;
        this.id_caseFile = id_caseFile;
        this.id_vehicle = id_vehicle;
        this.bunch_address = bunch_address;
        this.bunch_vehicle_count = bunch_vehicle_count;
        this.certification_type = certification_type;
        this.request_number = request_number;
        this.folder_number = folder_number;
        this.status = status;
        this.current_process = current_process;
    }

    //Find all for owner in bunch
    public VehicleCaseFileDTO(Long id_caseFile, Long id_vehicle, String bunch_address, CertificationType certification_type, String request_number, String folder_number, CasefileStatusType status, Boolean vehicle_active, Integer vehicle_employees_number, String vehicle_license, TransportedContentType vehicle_transported_content, String vehicle_type, String owner_name) {
        this.id_caseFile = id_caseFile;
        this.id_vehicle = id_vehicle;
        this.bunch_address = bunch_address;
        this.certification_type = certification_type;
        this.request_number = request_number;
        this.folder_number = folder_number;
        this.status = status;
        this.vehicle_active = vehicle_active;
        this.vehicle_employees_number = vehicle_employees_number;
        this.vehicle_license = vehicle_license;
        this.vehicle_transported_content = vehicle_transported_content;
        this.vehicle_type = vehicle_type;
        this.owner_name = owner_name;
    }

    public VehicleCaseFileDTO(String request_number, CertificationType certification_type, Integer vehicle_employees_number, String vehicle_license, TransportedContentType vehicle_transported_content, String vehicle_type, String owner_name) {
        this.request_number = request_number;
        this.vehicle_employees_number = vehicle_employees_number;
        this.vehicle_license = vehicle_license;
        this.vehicle_transported_content = vehicle_transported_content;
        this.vehicle_type = vehicle_type;
        this.owner_name = owner_name;
    }

    public VehicleCaseFileDTO(Long id_caseFile, Long id_owner, LocalDateTime certification_start_on, CertificationType certification_type, LocalDateTime creation_date, String request_number, String folder_number, CasefileStatusType status, String sibasi, String ucsf, String owner_name) {
        this.id_caseFile = id_caseFile;
        this.id_owner = id_owner;
        this.certification_start_on = certification_start_on;
        this.certification_type = certification_type;
        this.creation_date = creation_date;
        this.request_number = request_number;
        this.folder_number = folder_number;
        this.status = status;
        this.sibasi = sibasi;
        this.ucsf = ucsf;
        this.owner_name = owner_name;
    }

    public Long getId_bunch() {
        return id_bunch;
    }

    public void setId_bunch(Long id_bunch) {
        this.id_bunch = id_bunch;
    }

    public Long getId_caseFile() {
        return id_caseFile;
    }

    public Long getId_vehicle() {
        return id_vehicle;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public void setId_owner(Long id_owner) {
        this.id_owner = id_owner;
    }

    public String getBunch_client_presented_name() {
        return bunch_client_presented_name;
    }

    public String getBunch_client_presented_telephone() {
        return bunch_client_presented_telephone;
    }

    public String getBunch_distribution_company() {
        return bunch_distribution_company;
    }

    public String getBunch_distribution_destination() {
        return bunch_distribution_destination;
    }

    public String getBunch_marketing_place() {
        return bunch_marketing_place;
    }

    public String getBunch_address() {
        return bunch_address;
    }

    public Integer getBunch_vehicle_count() {
        return bunch_vehicle_count;
    }

    public Integer getCertification_duration_in_years() {
        return certification_duration_in_years;
    }

    public Boolean getApply_payment() {
        return apply_payment;
    }

    public String getCertification_start_on() {
        if (certification_start_on == null) {
            return DatetimeUtil.DATE_NOT_DEFINED;
        }
        return DatetimeUtil.parseDateToLongDateFormat(certification_start_on.toLocalDate());
    }

    public String getCertification_type() {
        if (certification_type == null) {
            return CertificationType.INVALID.getCertificationType();
        }
        return certification_type.getCertificationType();
    }

    public String getCreation_date() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(creation_date);
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public String getStatus() {
        if (status == null) {
            return CasefileStatusType.INVALID.getStatus_type();
        }
        return status.getStatus_type();
    }

    public String getSibasi() {
        return sibasi;
    }

    public String getUcsf() {
        return ucsf;
    }

    public Boolean getVehicle_active() {
        return vehicle_active;
    }

    public Integer getVehicle_employees_number() {
        return vehicle_employees_number;
    }

    public String getVehicle_license() {
        return vehicle_license;
    }

    public String getVehicle_transported_content() {
        if (vehicle_transported_content == null) {
            return TransportedContentType.INVALID.getTransportedContentType();
        }
        return vehicle_transported_content.getTransportedContentType();
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getCurrent_process() {
        return current_process;
    }
}
