package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.dto.AbstractRequestOnProcessValidityReport;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class EstablishmentRequestProcessValidatorReportRepository implements RequestValidatorReportRepository {

    private final SessionFactory sessionFactory;

    public EstablishmentRequestProcessValidatorReportRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractRequestOnProcessValidityReport getRequestProcessData(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.establishment.dto.RequestOnProcessValidityReport(" +
                "owner.name,establishment.name,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "casefile.creation_date,concat(concat(concat(concat('06',casefile.request_number),casefile.request_code),'-'),casefile.request_year) ,casefile.certification_type) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.address address " +
                "inner join establishment.owner owner " +
                "where casefile.id = :id", AbstractRequestOnProcessValidityReport.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }
}
