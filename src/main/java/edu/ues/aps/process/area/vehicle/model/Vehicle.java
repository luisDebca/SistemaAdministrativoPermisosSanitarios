package edu.ues.aps.process.area.vehicle.model;

import edu.ues.aps.process.base.model.OwnerEntity;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "VEHICLE")
@DynamicUpdate
public class Vehicle implements AbstractVehicle{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "license", nullable = false, unique = true, length = 10)
    private String license;

    @Column(name = "vehicle_type", nullable = false, length = 25)
    private String vehicle_type;

    @Enumerated(EnumType.STRING)
    @Column(name = "transported_content",nullable = false,length = 25)
    private TransportedContentType transported_content;

    @Column(name = "employees_number")
    private Integer employees_number;

    @Column(name = "is_active", nullable = false)
    private boolean active;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_owner")
    private OwnerEntity owner;

    @OneToMany(mappedBy = "vehicle", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CasefileVehicle> casefile_list = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getLicense() {
        return license;
    }

    @Override
    public void setLicense(String license) {
        this.license = license;
    }

    @Override
    public String getVehicle_type() {
        return vehicle_type;
    }

    @Override
    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }

    @Override
    public TransportedContentType getTransported_content() {
        return transported_content;
    }

    @Override
    public void setTransported_content(TransportedContentType transported_content) {
        this.transported_content = transported_content;
    }

    @Override
    public Integer getEmployees_number() {
        return employees_number;
    }

    @Override
    public void setEmployees_number(Integer employees_number) {
        this.employees_number = employees_number;
    }

    @Override
    public boolean isActive() {
        return active;
    }

    @Override
    public void setActive(boolean active) {
        this.active = active;
    }

    @Override
    public OwnerEntity getOwner() {
        return owner;
    }

    @Override
    public void setOwner(OwnerEntity owner) {
        this.owner = owner;
    }

    @Override
    public List<CasefileVehicle> getCasefile_list() {
        return casefile_list;
    }

    @Override
    public void setCasefile_list(List<CasefileVehicle> casefile_list) {
        this.casefile_list = casefile_list;
    }

    @Override
    public void addCasefile(CasefileVehicle casefile){
        casefile_list.add(casefile);
        casefile.setVehicle(this);
    }

    @Override
    public void removeCasefile(CasefileVehicle casefile){
        casefile_list.remove(casefile);
        casefile.setVehicle(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Vehicle vehicle = (Vehicle) o;

        return license.equals(vehicle.license) && vehicle_type.equals(vehicle.vehicle_type);
    }

    @Override
    public int hashCode() {
        int result = license.hashCode();
        result = 31 * result + vehicle_type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Vehicle{" +
                "id=" + id +
                ", license='" + license + '\'' +
                ", vehicle_type='" + vehicle_type + '\'' +
                ", transported_content=" + transported_content +
                ", employees_number=" + employees_number +
                ", active=" + active +
                '}';
    }
}
