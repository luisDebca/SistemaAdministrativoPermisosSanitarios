package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.repository.EstablishmentTypeDTOFinderRepository;
import edu.ues.aps.process.base.dto.AbstractMapDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class EstablishmentTypeDTOService implements EstablishmentTypeDTOFinderService {

    private EstablishmentTypeDTOFinderRepository finderRepository;

    public EstablishmentTypeDTOService(EstablishmentTypeDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractMapDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public List<AbstractMapDTO> findAllTypesFromSection(Long id_section) {
        return finderRepository.findAllTypesFromSection(id_section);
    }
}
