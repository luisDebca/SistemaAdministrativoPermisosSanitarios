package edu.ues.aps.process.area.vehicle.model;

import java.util.HashMap;
import java.util.Map;

public enum TransportedContentType {

    PERISHABLE("PERECEDERO"),
    NON_PERISHABLE("NO PERECEDERO"),
    INVALID("INVALIDO");

    private String transportedContentType;

    TransportedContentType(String transportedContentType) {
        this.transportedContentType = transportedContentType;
    }

    public String getTransportedContentType() {
        return transportedContentType;
    }

    public static Map<String, String> asMap() {
        Map<String, String> map = new HashMap<>();
        for (TransportedContentType type : TransportedContentType.values()) {
            map.put(type.name(), type.transportedContentType);
        }
        return map;
    }
}
