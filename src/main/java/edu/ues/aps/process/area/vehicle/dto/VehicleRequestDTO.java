package edu.ues.aps.process.area.vehicle.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class VehicleRequestDTO implements AbstractVehicleRequestDTO{

    private Long id_bunch;
    private Long id_owner;
    private String owner_name;
    private String bunch_presented_name;
    private String bunch_presented_phone;
    private String bunch_distribution_company;
    private String bunch_distribution_destination;
    private String bunch_marketing_place;
    private String bunch_address;
    private Long request_count;
    private String request_folder;
    private LocalDateTime request_create_on;

    public VehicleRequestDTO() {
    }

    public VehicleRequestDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on) {
        this.id_bunch = id_bunch;
        this.id_owner = id_owner;
        this.owner_name = owner_name;
        this.bunch_presented_name = bunch_presented_name;
        this.bunch_presented_phone = bunch_presented_phone;
        this.bunch_distribution_company = bunch_distribution_company;
        this.bunch_distribution_destination = bunch_distribution_destination;
        this.bunch_marketing_place = bunch_marketing_place;
        this.bunch_address = bunch_address;
        this.request_count = request_count;
        this.request_folder = request_folder;
        this.request_create_on = request_create_on;
    }

    public Long getId_bunch() {
        return id_bunch;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getBunch_presented_name() {
        return bunch_presented_name;
    }

    public String getBunch_presented_phone() {
        return bunch_presented_phone;
    }

    public String getBunch_distribution_company() {
        return bunch_distribution_company;
    }

    public String getBunch_distribution_destination() {
        return bunch_distribution_destination;
    }

    public String getBunch_marketing_place() {
        return bunch_marketing_place;
    }

    public String getBunch_address() {
        return bunch_address;
    }

    public Long getRequest_count() {
        return request_count;
    }

    public String getRequest_folder() {
        return request_folder;
    }

    public String getRequest_create_on() {
        if(request_create_on == null){
            return DatetimeUtil.DATE_NOT_DEFINED;
        }
        return DatetimeUtil.parseDateToLongDateFormat(request_create_on.toLocalDate());
    }
}
