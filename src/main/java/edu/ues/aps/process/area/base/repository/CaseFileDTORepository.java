package edu.ues.aps.process.area.base.repository;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.base.repository.CaseFileDTOFinderRepository;
import edu.ues.aps.process.base.model.CasefileStatusType;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class CaseFileDTORepository implements CaseFileDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public CaseFileDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractCaseFileDTO findById(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.base.dto.CaseFileDTO(" +
                "concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year)," +
                "concat(concat(casefile.request_folder,'-'),casefile.request_year),casefile.apply_payment,casefile.certification_type," +
                "casefile.section,casefile.status,casefile.certification_duration_in_years,casefile.certification_start_on,casefile.creation_date," +
                "ucsf.name,sibasi.zone )" +
                "from Casefile casefile " +
                "left join casefile.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "where casefile.id = :id",AbstractCaseFileDTO.class)
                .setParameter("id",id_caseFile)
                .getSingleResult();
    }

    @Override
    public List<AbstractCaseFileDTO> findByIdentifier(String identifier) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.base.dto.CaseFileDTO(casefile.id," +
                "concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year)," +
                "concat(concat(casefile.request_folder,'-'),casefile.request_year),casefile.certification_type,casefile.section," +
                "casefile.creation_date,process.process,casefile.status) " +
                "from Casefile casefile " +
                "left join casefile.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year) like concat(concat('%',:identifier),'%' ) " +
                "and concat(concat(casefile.request_folder,'-'),casefile.request_year) like concat(concat('%',:identifier),'%') ",AbstractCaseFileDTO.class)
                .setParameter("identifier",identifier)
                .getResultList();
    }

    @Override
    public List<AbstractCaseFileDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.base.dto.CaseFileDTO(casefile.id," +
                "concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year)," +
                "concat(concat(casefile.request_folder,'-'),casefile.request_year),casefile.certification_type,casefile.section," +
                "casefile.creation_date,process.process,casefile.status) " +
                "from Casefile casefile " +
                "left join casefile.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and not casefile.status = :invalid ",AbstractCaseFileDTO.class)
                .setParameter("invalid", CasefileStatusType.INVALID)
                .getResultList();
    }
}
