package edu.ues.aps.process.area.establishment.model;

import java.util.Collections;
import java.util.List;

public class EmptySection implements AbstractSection {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getType() {
        return "Section type not found.";
    }

    @Override
    public List<EstablishmentType> getEstablishmentTypes() {
        return Collections.emptyList();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setType(String type) {

    }

    @Override
    public void setEstablishmentTypes(List<EstablishmentType> establishmentTypes) {

    }

    @Override
    public String toString() {
        return "EmptySection{}";
    }
}
