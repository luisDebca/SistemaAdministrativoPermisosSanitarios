package edu.ues.aps.process.area.base.dto;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class CaseFileDTO implements AbstractCaseFileDTO {

    private Long id_casefile;
    private String request_identifier;
    private String request_folder;
    private boolean apply_payment;
    private CertificationType certification_type;
    private CasefileSection casefile_section;
    private CasefileStatusType status;
    private Integer certification_duration;
    private LocalDateTime certification_start;
    private LocalDateTime creation_date;
    private String ucsf;
    private String sibasi;
    private String current_process;

    public CaseFileDTO(Long id_casefile, String request_folder, boolean apply_payment, CertificationType certification_type, CasefileSection casefile_section, CasefileStatusType status, Integer certification_duration, LocalDateTime creation_date, String ucsf, String sibasi) {
        this.id_casefile = id_casefile;
        this.request_folder = request_folder;
        this.apply_payment = apply_payment;
        this.certification_type = certification_type;
        this.casefile_section = casefile_section;
        this.status = status;
        this.certification_duration = certification_duration;
        this.creation_date = creation_date;
        this.ucsf = ucsf;
        this.sibasi = sibasi;
    }

    public CaseFileDTO(String request_identifier, String request_folder, boolean apply_payment, CertificationType certification_type, CasefileSection casefile_section, CasefileStatusType status, Integer certification_duration, LocalDateTime certification_start, LocalDateTime creation_date, String ucsf, String sibasi) {
        this.request_identifier = request_identifier;
        this.request_folder = request_folder;
        this.apply_payment = apply_payment;
        this.certification_type = certification_type;
        this.casefile_section = casefile_section;
        this.status = status;
        this.certification_duration = certification_duration;
        this.certification_start = certification_start;
        this.creation_date = creation_date;
        this.ucsf = ucsf;
        this.sibasi = sibasi;
    }

    public CaseFileDTO(Long id_casefile, String request_identifier, String request_folder, CertificationType certification_type, CasefileSection casefile_section, LocalDateTime creation_date, String current_process, CasefileStatusType status) {
        this.id_casefile = id_casefile;
        this.request_identifier = request_identifier;
        this.request_folder = request_folder;
        this.certification_type = certification_type;
        this.casefile_section = casefile_section;
        this.creation_date = creation_date;
        this.current_process = current_process;
        this.status = status;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public void setId_casefile(Long id_casefile) {
     this.id_casefile = id_casefile;
    }

    public String getRequest_identifier() {
        return request_identifier;
    }

    public String getRequest_folder() {
        return request_folder;
    }

    public boolean isApply_payment() {
        return apply_payment;
    }

    public String getCertification_type() {
        if(certification_type != null){
            return certification_type.getCertificationType();
        }else {
            return CertificationType.INVALID.getCertificationType();
        }
    }

    public String getCasefile_section() {
        if(casefile_section != null){
            return casefile_section.getCasefileSection();
        }else{
            return CasefileSection.INVALID.getCasefileSection();
        }
    }

    public String getStatus() {
        if(status != null){
            return status.getStatus_type();
        }else{
            return CasefileStatusType.INVALID.getStatus_type();
        }
    }

    public Integer getCertification_duration() {
        return certification_duration;
    }

    public String getCertification_start() {
        if(certification_start != null){
            return DatetimeUtil.parseDateToLongDateFormat(certification_start.toLocalDate());
        }else{
            return DatetimeUtil.DATE_NOT_DEFINED;
        }
    }

    public String getCreation_date() {
        if(creation_date != null){
            return DatetimeUtil.parseDatetimeToLongDateFormat(creation_date);
        }else{
            return DatetimeUtil.DATETIME_NOT_DEFINED;
        }
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getSibasi() {
        return sibasi;
    }

    public String getCurrent_process() {
        return current_process;
    }
}
