package edu.ues.aps.process.area.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class EstablishmentRequestDTO implements AbstractEstablishmentRequestDTO {

    private Long id_caseFile;
    private Long id_owner;
    private Long id_establishment;
    private String folder_number;
    private String request_number;
    private String establishment_name;
    private String establishment_type_detail;
    private String establishment_type;
    private String establishment_section;
    private String establishment_address;
    private String establishment_nit;
    private String UCSF;
    private String SIBASI;
    private CertificationType certification_type;
    private LocalDateTime caseFile_createOn;
    private String owner_name;

    public EstablishmentRequestDTO() {
    }

    public EstablishmentRequestDTO(Long id_caseFile, Long id_owner, Long id_establishment, String folder_number, String request_number, String establishment_name, String establishment_type_detail, String establishment_type, String establishment_section, String establishment_address, String establishment_nit, String UCSF, String SIBASI, CertificationType certification_type, LocalDateTime caseFile_createOn, String owner_name) {
        this.id_caseFile = id_caseFile;
        this.id_owner = id_owner;
        this.id_establishment = id_establishment;
        this.folder_number = folder_number;
        this.request_number = request_number;
        this.establishment_name = establishment_name;
        this.establishment_type_detail = establishment_type_detail;
        this.establishment_type = establishment_type;
        this.establishment_section = establishment_section;
        this.establishment_address = establishment_address;
        this.establishment_nit = establishment_nit;
        this.UCSF = UCSF;
        this.SIBASI = SIBASI;
        this.certification_type = certification_type;
        this.caseFile_createOn = caseFile_createOn;
        this.owner_name = owner_name;
    }

    public Long getId_caseFile() {
        return id_caseFile;
    }

    public void setId_caseFile(Long id_caseFile) {
        this.id_caseFile = id_caseFile;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public void setId_owner(Long id_owner) {
        this.id_owner = id_owner;
    }

    public Long getId_establishment() {
        return id_establishment;
    }

    public void setId_establishment(Long id_establishment) {
        this.id_establishment = id_establishment;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_type_detail() {
        return establishment_type_detail;
    }

    public String getEstablishment_type() {
        return establishment_type;
    }

    public String getEstablishment_section() {
        return establishment_section;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getEstablishment_nit() {
        return establishment_nit;
    }

    public String getUCSF() {
        return UCSF;
    }

    public String getSIBASI() {
        return SIBASI;
    }

    public String getCertification_type() {
        if (certification_type == null) return CertificationType.INVALID.getCertificationType();
        return certification_type.getCertificationType();
    }

    public String getCaseFile_createOn() {
        if (caseFile_createOn == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(caseFile_createOn.toLocalDate());
    }

    public String getOwner_name() {
        return owner_name;
    }
}
