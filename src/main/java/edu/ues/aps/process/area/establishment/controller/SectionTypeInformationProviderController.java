package edu.ues.aps.process.area.establishment.controller;

import edu.ues.aps.process.area.establishment.service.EstablishmentTypeDTOFinderService;
import edu.ues.aps.process.area.establishment.service.SectionDTOFinderService;
import edu.ues.aps.process.base.dto.AbstractMapDTO;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.List;

@Controller
@ControllerAdvice
@RequestMapping({"/requirement-control/establishment/template-*/new-control-document-for-*", "/casefile/establishment/required-data-completion/data-completion-*"})
public class SectionTypeInformationProviderController implements InitializingBean {

    private List<AbstractMapDTO> sections;
    private List<AbstractMapDTO> establishment_types;
    private final SectionDTOFinderService sectionDTOFinderService;
    private final EstablishmentTypeDTOFinderService establishmentTypeDTOFinderService;

    public SectionTypeInformationProviderController(SectionDTOFinderService sectionDTOFinderService, EstablishmentTypeDTOFinderService establishmentTypeDTOFinderService) {
        this.sectionDTOFinderService = sectionDTOFinderService;
        this.establishmentTypeDTOFinderService = establishmentTypeDTOFinderService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        sections = sectionDTOFinderService.findAll();
        establishment_types = establishmentTypeDTOFinderService.findAll();
    }

    @ModelAttribute("establishment_types")
    public List<AbstractMapDTO> getEstablishment_types() {
        return establishment_types;
    }

    @ModelAttribute("sections")
    public List<AbstractMapDTO> getSections() {
        return sections;
    }
}
