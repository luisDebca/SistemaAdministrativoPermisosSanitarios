package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.dto.AbstractCasefileEstablishmentInfoDTO;

public interface EstablishmentCaseFileDTOFinderService {

    AbstractCasefileEstablishmentInfoDTO find(Long id_caseFile);

}
