package edu.ues.aps.process.area.establishment.dto;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

public class EmptyCasefileInformationDTO implements AbstractCasefileInformationDTO{

    @Override
    public Long getId_owner() {
        return 0L;
    }

    @Override
    public Long getId_establishment() {
        return 0L;
    }

    @Override
    public Long getId_casefile() {
        return 0L;
    }

    @Override
    public boolean getCasefile_apply_payment() {
        return false;
    }

    @Override
    public int getCasefile_duration() {
        return 0;
    }

    @Override
    public String getCasefile_start_on() {
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    @Override
    public String getCasefile_start_on_html() {
        return DatetimeUtil.DATETIME_NOT_DEFINED;
    }

    @Override
    public String getCasefile_type() {
        return CertificationType.INVALID.getCertificationType();
    }

    @Override
    public String getCasefile_code() {
        return "Not found";
    }

    @Override
    public String getCasefile_section() {
        return CasefileSection.INVALID.getCasefileSection();
    }

    @Override
    public String getCasefile_status() {
        return CasefileStatusType.INVALID.getStatus_type();
    }

    @Override
    public String getCasefile_date() {
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    @Override
    public String getSibasi() {
        return "Not found";
    }

    @Override
    public String getUcsf() {
        return "Not found";
    }

    @Override
    public String getEstablishment_name() {
        return "Not found";
    }

    @Override
    public String getOwner_name() {
        return "Not found";
    }

    @Override
    public String getCurrent_process() {
        return "Not found";
    }

    @Override
    public String getCurrent_process_type() {
        return "Not found";
    }

    @Override
    public String getFolder_number() {
        return "Case file folder number not found";
    }
}
