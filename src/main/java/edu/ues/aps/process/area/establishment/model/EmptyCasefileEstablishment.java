package edu.ues.aps.process.area.establishment.model;

import edu.ues.aps.process.base.model.EmptyCasefile;

public class EmptyCasefileEstablishment extends EmptyCasefile implements AbstractCasefileEstablishment {

    @Override
    public AbstractEstablishment getEstablishment() {
        return new EmptyEstablishment();
    }

    @Override
    public void setEstablishment(Establishment establishment) {

    }

    @Override
    public Integer getCertificationDurationInYears() {
        return 0;
    }
}
