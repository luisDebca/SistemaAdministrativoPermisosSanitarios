package edu.ues.aps.process.area.base.dto;

public class EmptyCaseFileDTO implements AbstractCaseFileDTO {

    @Override
    public Long getId_casefile() {
        return 0L;
    }

    @Override
    public void setId_casefile(Long id_casefile) {

    }

    @Override
    public String getRequest_identifier() {
        return "00000-00";
    }

    @Override
    public String getRequest_folder() {
        return "00-00";
    }

    @Override
    public boolean isApply_payment() {
        return false;
    }

    @Override
    public String getCertification_type() {
        return "Certification type not found";
    }

    @Override
    public String getCasefile_section() {
        return "Section not found";
    }

    @Override
    public String getStatus() {
        return "Status not found";
    }

    @Override
    public Integer getCertification_duration() {
        return 0;
    }

    @Override
    public String getCertification_start() {
        return "Certification date not found";
    }

    @Override
    public String getCreation_date() {
        return "Creation date not found";
    }

    @Override
    public String getUcsf() {
        return "UCSF name not found";
    }

    @Override
    public String getSibasi() {
        return "SIBASI name not found";
    }

    @Override
    public String getCurrent_process() {
        return "Current process not found";
    }
}
