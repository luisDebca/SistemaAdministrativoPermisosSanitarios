package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.base.dto.AbstractRequestProcessIndexSummaryDTO;
import edu.ues.aps.process.area.base.repository.IndexSummaryRepository;
import edu.ues.aps.process.area.base.service.IndexSummaryService;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class EstablishmentIndexSummaryService implements IndexSummaryService {

    private final IndexSummaryRepository summaryRepository;

    public EstablishmentIndexSummaryService(@Qualifier("establishmentIndexSummaryRepository") IndexSummaryRepository summaryRepository) {
        this.summaryRepository = summaryRepository;
    }

    @Override
    public List<AbstractRequestProcessIndexSummaryDTO> findAllFromToday(ProcessIdentifier identifier) {
        return summaryRepository.findAllFromToday(identifier);
    }

    @Override
    public List<AbstractRequestProcessIndexSummaryDTO> findAllFromThisWeek(ProcessIdentifier identifier) {
        return summaryRepository.findAllFromThisWeek(identifier);
    }

    @Override
    public List<AbstractRequestProcessIndexSummaryDTO> findAllFromThisMonth(ProcessIdentifier identifier) {
        return summaryRepository.findAllFromThisMonth(identifier);
    }

    @Override
    public Long countFromToday(ProcessIdentifier identifier) {
        return summaryRepository.countFromToday(identifier);
    }

    @Override
    public Long countFromThisWeek(ProcessIdentifier identifier) {
        return summaryRepository.countFromThisWeek(identifier);
    }

    @Override
    public Long countFromThisMonth(ProcessIdentifier identifier) {
        return summaryRepository.countFromThisMonth(identifier);
    }
}
