package edu.ues.aps.process.area.establishment.dto;

public class EstablishmentEntityDTO implements AbstractEstablishmentEntityDTO{

    private Long establishmentID;
    private String establishmentName;
    private String establishmentNIT;
    private String establishmentAddress;
    private String establishmentCommercialRegister;
    private String establishmentType;
    private String establishmentSection;
    private String establishmentEmail;
    private String establishmentPhone;
    private String establishmentPhone2;
    private String establishmentFAX;
    private String establishmentOperations;
    private Double establishmentCapital;
    private Integer establishmentMaleCount;
    private Integer establishmentFemaleCount;
    private Boolean establishmentActive;

    @Override
    public Long getEstablishmentID() {
        return establishmentID;
    }

    @Override
    public String getEstablishmentName() {
        return establishmentName;
    }

    @Override
    public String getEstablishmentNIT() {
        return establishmentNIT;
    }

    @Override
    public String getEstablishmentAddress() {
        return establishmentAddress;
    }

    @Override
    public String getEstablishmentCommercialRegister() {
        return establishmentCommercialRegister;
    }

    @Override
    public String getEstablishmentType() {
        return establishmentType;
    }

    @Override
    public String getEstablishmentSection() {
        return establishmentSection;
    }

    @Override
    public String getEstablishmentEmail() {
        return establishmentEmail;
    }

    @Override
    public String getEstablishmentPhone() {
        return establishmentPhone;
    }

    @Override
    public String getEstablishmentPhone2() {
        return establishmentPhone2;
    }

    @Override
    public String getEstablishmentFAX() {
        return establishmentFAX;
    }

    @Override
    public String getEstablishmentOperations() {
        return establishmentOperations;
    }

    @Override
    public Double getEstablishmentCapital() {
        return establishmentCapital;
    }

    @Override
    public Integer getEstablishmentMaleCount() {
        return establishmentMaleCount;
    }

    @Override
    public Integer getEstablishmentFemaleCount() {
        return establishmentFemaleCount;
    }

    @Override
    public Boolean getEstablishmentActive() {
        return establishmentActive;
    }
}
