package edu.ues.aps.process.area.base.dto;

public class RequestDTO implements AbstractRequestDTO{

    private String folder_number;
    private String request_number;
    private String resolution_number;

    public RequestDTO(String folder_number, String request_number, String resolution_number) {
        this.folder_number = folder_number;
        this.request_number = request_number;
        this.resolution_number = resolution_number;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getResolution_number() {
        return resolution_number;
    }
}
