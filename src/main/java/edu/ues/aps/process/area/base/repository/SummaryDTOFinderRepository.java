package edu.ues.aps.process.area.base.repository;

import edu.ues.aps.process.area.base.dto.AbstractSummaryDTO;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;

import java.time.LocalDateTime;
import java.util.List;

public interface SummaryDTOFinderRepository {

    CasefileSection appliesTo();

    List<AbstractSummaryDTO> find(ProcessIdentifier processIdentifier, LocalDateTime start, LocalDateTime end);
}
