package edu.ues.aps.process.area.vehicle.model;

import edu.ues.aps.process.base.model.AbstractAddress;
import edu.ues.aps.process.base.model.Address;

import java.util.List;

public interface AbstractVehicleBunch {

    Long getId();

    void setId(Long id);

    String getClient_presented_name();

    void setClient_presented_name(String client_presented_name);

    String getClient_presented_telephone();

    void setClient_presented_telephone(String client_presented_telephone);

    String getDistribution_company();

    void setDistribution_company(String distribution_company);

    String getDistribution_destination();

    void setDistribution_destination(String distribution_destination);

    String getMarketing_place();

    void setMarketing_place(String marketing_place);

    Integer getVehicle_count();

    void setVehicle_count(Integer vehicle_count);

    List<CasefileVehicle> getCasefileVehicles();

    void setCasefileVehicles(List<CasefileVehicle> casefileVehicles);

    void addCasefile(CasefileVehicle casefile);

    void removeCasefile(CasefileVehicle casefile);

    List<TransportedProduct> getProducts();

    void setProducts(List<TransportedProduct> products);

    void addProducts(TransportedProduct product);

    void removeProduct(TransportedProduct product);

    AbstractAddress getAddress();

    void setAddress(Address address);

}
