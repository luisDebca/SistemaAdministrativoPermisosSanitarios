package edu.ues.aps.process.area.vehicle.controller;

import edu.ues.aps.process.area.vehicle.model.Vehicle;
import edu.ues.aps.process.area.vehicle.service.VehiclePersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class VehiclePersistenceController {

    private static final Logger logger = LogManager.getLogger(VehiclePersistenceController.class);

    private static final String VEHICLE_NEW = "vehicle_new";
    private static final String VEHICLE_EDIT = "vehicle_edit";

    private final VehiclePersistenceService persistenceService;

    public VehiclePersistenceController(VehiclePersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping("/owner/{id_owner}/vehicle/new")
    public String showNewVehicleForm(@PathVariable Long id_owner, ModelMap model) {
        logger.info("GET:: Show new vehicle form for owner {}", id_owner);
        model.addAttribute("vehicle", new Vehicle());
        model.addAttribute("id_owner", id_owner);
        return VEHICLE_NEW;
    }

    @PostMapping("/owner/{id_owner}/vehicle/new")
    public String saveVehicle(@ModelAttribute Vehicle vehicle, @PathVariable Long id_owner) {
        logger.info("POST:: Save new vehicle for owner {}", id_owner);
        persistenceService.save(id_owner, vehicle);
        return String.format("redirect:/owner/%d/vehicle/list", id_owner);
    }


    @GetMapping({"/owner/{id_owner}/vehicle/{id_vehicle}/edit", "/vehicle/{id_vehicle}/edit"})
    public String showVehicleEditForm(@PathVariable Long id_vehicle, @PathVariable(required = false) Long id_owner, ModelMap model) {
        logger.info("GET:: Show vehicle {} edit form", id_vehicle);
        model.addAttribute("vehicle", persistenceService.findByID(id_vehicle));
        model.addAttribute("id_owner", id_owner);
        return VEHICLE_EDIT;
    }

    @PostMapping({"/owner/{id_owner}/vehicle/{id_vehicle}/edit", "/vehicle/{id_vehicle}/edit"})
    public String updateVehicle(@ModelAttribute Vehicle vehicle, @PathVariable Long id_vehicle, @PathVariable(required = false) Long id_owner) {
        logger.info("POST:: Update vehicle {}", id_vehicle);
        persistenceService.update(vehicle);
        if (id_owner == null) {
            return "redirect:/vehicle/list";
        }
        return String.format("redirect:/owner/%d/vehicle/list", id_owner);
    }

    @GetMapping({"/owner/{id_owner}/vehicle/{id_vehicle}/delete", "/vehicle/{id_vehicle}/delete"})
    public String deleteVehicle(@PathVariable Long id_vehicle, @PathVariable(required = false) Long id_owner) {
        persistenceService.delete(id_vehicle);
        if (id_owner == null) {
            return "redirect:/vehicle/list";
        }
        return String.format("redirect:/owner/%d/vehicle/list", id_owner);
    }

    @GetMapping({"/owner/{id_owner}/vehicle/{id_vehicle}/to-disable", "/vehicle/{id_vehicle}/to-disable"})
    public String toDisableVehicle(@PathVariable(required = false) Long id_owner, @PathVariable Long id_vehicle) {
        persistenceService.toDisable(id_vehicle);
        if (id_owner == null) {
            return "redirect:/vehicle/list";
        }
        return String.format("redirect:/owner/%d/vehicle/list", id_owner);
    }

    @GetMapping({"/owner/{id_owner}/vehicle/{id_vehicle}/to-enable", "/vehicle/{id_vehicle}/to-enable"})
    public String toEnableVehicle(@PathVariable(required = false) Long id_owner, @PathVariable Long id_vehicle) {
        persistenceService.toEnable(id_vehicle);
        if (id_owner == null) {
            return "redirect:/vehicle/list";
        }
        return String.format("redirect:/owner/%d/vehicle/list", id_owner);
    }
}
