package edu.ues.aps.process.area.base.service;

import edu.ues.aps.process.area.base.dto.AbstractSummaryDTO;
import edu.ues.aps.process.area.base.repository.SummaryCountRepository;
import edu.ues.aps.process.area.base.repository.SummaryDTOFinderRepository;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDate;
import java.time.temporal.TemporalField;
import java.time.temporal.WeekFields;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;
import java.util.Map;

@Service
@Transactional(readOnly = true)
public class SummaryDTOService implements InitializingBean, SummaryDTOFinderService, SummaryCountService {

    private final List<SummaryDTOFinderRepository> finderRepositories;
    private final List<SummaryCountRepository> countRepositories;

    private Map<CasefileSection, SummaryDTOFinderRepository> finderMap = new HashMap<>();
    private Map<CasefileSection, SummaryCountRepository> countMap = new HashMap<>();

    public SummaryDTOService(List<SummaryDTOFinderRepository> finderRepositories, List<SummaryCountRepository> countRepositories) {
        this.finderRepositories = finderRepositories;
        this.countRepositories = countRepositories;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        initFinders();
        initCounters();
    }

    private void initCounters() {
        for (SummaryCountRepository repository : countRepositories) {
            countMap.put(repository.appliesTo(), repository);
        }
    }

    private void initFinders() {
        for (SummaryDTOFinderRepository repository : finderRepositories) {
            finderMap.put(repository.appliesTo(), repository);
        }
    }

    @Override
    public List<AbstractSummaryDTO> findToday(ProcessIdentifier processIdentifier, CasefileSection section) {
        return finderMap.get(section).find(
                processIdentifier,
                LocalDate.now().atStartOfDay(),
                LocalDate.now().atTime(23, 59));
    }

    @Override
    public List<AbstractSummaryDTO> findWeek(ProcessIdentifier processIdentifier, CasefileSection section) {
        LocalDate now = LocalDate.now();
        TemporalField fieldISO = WeekFields.of(Locale.US).dayOfWeek();
        return finderMap.get(section).find(
                processIdentifier,
                now.with(fieldISO, 1).atStartOfDay(),
                now.with(fieldISO, 7).atTime(23,59));
    }

    @Override
    public List<AbstractSummaryDTO> findMonth(ProcessIdentifier processIdentifier, CasefileSection section) {
        LocalDate firstDayOfMonth = LocalDate.now().withDayOfMonth(1);
        LocalDate lastDayOfMonth = LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth());
        return finderMap.get(section).find(
                processIdentifier,
                firstDayOfMonth.atStartOfDay(),
                lastDayOfMonth.atTime(23,59));
    }

    @Override
    public Long getTodayCount(ProcessIdentifier identifier, CasefileSection section) {
        try {
            return countMap.get(section).getCount(
                    identifier,
                    LocalDate.now().atStartOfDay(),
                    LocalDate.now().atTime(23, 59));
        } catch (NoResultException e) {
            return 0L;
        }
    }

    @Override
    public Long getWeekCount(ProcessIdentifier identifier, CasefileSection section) {
        try {
            LocalDate now = LocalDate.now();
            TemporalField fieldISO = WeekFields.of(Locale.US).dayOfWeek();
            return countMap.get(section).getCount(
                    identifier,
                    now.with(fieldISO, 1).atStartOfDay(),
                    now.with(fieldISO, 7).atTime(23,59));
        } catch (NoResultException e) {
            return 0L;
        }
    }

    @Override
    public Long getMonthCount(ProcessIdentifier identifier, CasefileSection section) {
        try {
            LocalDate firstDayOfMonth = LocalDate.now().withDayOfMonth(1);
            LocalDate lastDayOfMonth = LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth());
            return countMap.get(section).getCount(
                    identifier,
                    firstDayOfMonth.atStartOfDay(),
                    lastDayOfMonth.atTime(23,59));
        } catch (NoResultException e) {
            return 0L;
        }
    }
}
