package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO;

import java.util.List;

public interface VehicleBunchCaseFileDTOFinderService {

    AbstractVehicleCaseFileDTO find(Long id_caseFile);

    List<AbstractVehicleCaseFileDTO> findAll();

    List<AbstractVehicleCaseFileDTO> findAll(Long id_owner);

    List<AbstractVehicleCaseFileDTO> findAll(Long id_owner, Long id_bunch);
    
}
