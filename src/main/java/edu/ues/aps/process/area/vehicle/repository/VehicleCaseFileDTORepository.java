package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleCaseFileDTORepository implements VehicleCaseFileDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleCaseFileDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleCaseFileDTO> findAll(Long id_vehicle) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleCaseFileDTO(" +
                "cf.id,owner.id,cf.certification_start_on,cf.certification_type,cf.creation_date," +
                "concat(concat(concat(concat('06',cf.request_code),cf.request_number),'-'),cf.request_year)," +
                "concat(concat(cf.request_folder,'-'),cf.request_year),cf.status,sibasi.zone,ucsf.name,owner.name) " +
                "from Vehicle vehicle " +
                "inner join vehicle.casefile_list cf " +
                "left join cf.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join vehicle.owner owner " +
                "where vehicle.id = :id", AbstractVehicleCaseFileDTO.class)
                .setParameter("id",id_vehicle)
                .getResultList();
    }
}
