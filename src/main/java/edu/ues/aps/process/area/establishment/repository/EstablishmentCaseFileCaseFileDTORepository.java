package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.dto.AbstractCasefileEstablishmentInfoDTO;
import edu.ues.aps.process.area.establishment.dto.AbstractCasefileInformationDTO;
import edu.ues.aps.process.area.establishment.dto.CasefileEstablishmentInfoDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class EstablishmentCaseFileCaseFileDTORepository implements EstablishmentCaseFileDTOFinderRepository, EstablishmentCaseFileInformationDTORepository {

    private SessionFactory sessionFactory;

    public EstablishmentCaseFileCaseFileDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractCasefileEstablishmentInfoDTO find(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.establishment.dto.CasefileEstablishmentInfoDTO(" +
                "casefile.id,casefile.establishment.id,casefile.establishment.owner.id," +
                "casefile.request_number,casefile.request_code,casefile.request_year,concat(concat(casefile.request_folder,'-'),casefile.request_year)," +
                "casefile.apply_payment,casefile.certification_type,casefile.certification_duration_in_years," +
                "casefile.certification_start_on,ucsf.name,sibasi.zone) " +
                "from CasefileEstablishment casefile " +
                "left join casefile.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "where casefile.id = :id", CasefileEstablishmentInfoDTO.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }

    @Override
    public List<AbstractCasefileInformationDTO> findAll(Long id_establishment) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.establishment.dto.CasefileInformationDTO(" +
                "owner.id,casefile.id,casefile.certification_start_on,casefile.certification_type," +
                "case when casefile.request_number is not null then concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year) else '' end , " +
                "concat(concat(casefile.request_folder,'-'),casefile.request_year) ," +
                "casefile.status,casefile.creation_date,establishment.name,process.process,process.identifier) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.owner owner " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where establishment.id = :id " +
                "and record.active = true", AbstractCasefileInformationDTO.class)
                .setParameter("id", id_establishment)
                .getResultList();
    }

}
