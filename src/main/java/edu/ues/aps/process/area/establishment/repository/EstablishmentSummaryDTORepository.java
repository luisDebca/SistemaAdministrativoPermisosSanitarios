package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.base.dto.AbstractSummaryDTO;
import edu.ues.aps.process.area.base.repository.SummaryCountRepository;
import edu.ues.aps.process.area.base.repository.SummaryDTOFinderRepository;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;
import java.util.List;

@Repository
public class EstablishmentSummaryDTORepository implements SummaryDTOFinderRepository, SummaryCountRepository {

    private final SessionFactory sessionFactory;

    public EstablishmentSummaryDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public CasefileSection appliesTo() {
        return CasefileSection.ESTABLISHMENT;
    }

    @Override
    public Long getCount(ProcessIdentifier processIdentifier, LocalDateTime start, LocalDateTime end) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select count(ce) " +
                "from CasefileEstablishment ce " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "and record.start_on > :start " +
                "and record.start_on < :end",Long.class)
                .setParameter("id",processIdentifier)
                .setParameter("start",start)
                .setParameter("end",end)
                .getSingleResult();
    }

    @Override
    public List<AbstractSummaryDTO> find(ProcessIdentifier processIdentifier, LocalDateTime start, LocalDateTime end) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.base.dto.SummaryDTO(" +
                "ce.id,concat(concat(ce.request_folder,'-'),ce.request_year)," +
                "case when ce.request_number is null then '' else concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year) end," +
                "case when resolution is null then '' else concat(concat(concat(concat('06',resolution.resolution_code),resolution.resolution_number),'-'),resolution.resolution_year) end," +
                "owner.name,establishment.name,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)) " +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "inner join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.resolution resolution " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "and record.start_on > :start " +
                "and record.start_on < :end " +
                "order by ce.id desc ", AbstractSummaryDTO.class)
                .setParameter("id", processIdentifier)
                .setParameter("start",start)
                .setParameter("end",end)
                .setMaxResults(5)
                .getResultList();
    }
}
