package edu.ues.aps.process.area.vehicle.service;

public interface VehicleValidatorService {

    Boolean isUniqueLicense(String license);

    Boolean isUniqueLicense(String license, Long id_vehicle);
}
