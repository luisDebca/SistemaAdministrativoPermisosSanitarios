package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO;

import java.util.List;

public interface EstablishmentDTOFinderService {

    AbstractEstablishmentDTO findById(Long id_establishment);

    AbstractEstablishmentDTO findByCaseFile(Long id_caseFile);

    List<AbstractEstablishmentDTO> findAll();

    List<AbstractEstablishmentDTO> findAllByOwner(Long id_owner);
}
