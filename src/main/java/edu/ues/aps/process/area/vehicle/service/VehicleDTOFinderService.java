package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO;
import edu.ues.aps.process.area.vehicle.dto.VehicleDTO;
import edu.ues.aps.process.area.vehicle.model.Vehicle;

import java.util.List;

public interface VehicleDTOFinderService {

    List<AbstractVehicleDTO> findAllFromOwner(Long id_owner);

    List<AbstractVehicleDTO> findAll();

    List<AbstractVehicleDTO> findByPath(String path);

    List<AbstractVehicleDTO> findAllVehicles(Long id_bunch);

    AbstractVehicleDTO findByID(Long id_vehicle);

}
