package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO;
import edu.ues.aps.process.base.dto.*;
import edu.ues.aps.process.base.model.ClientType;
import edu.ues.aps.process.base.model.NaturalEntity;
import org.hibernate.HibernateException;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Repository
public class VehicleDTORepository implements VehicleDTOFinderRepository, VehicleBunchRelationshipRepository {

    private final SessionFactory sessionFactory;

    public VehicleDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleDTO> findAllFromOwner(Long id_owner) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleDTO(vehicle.id,owner.id,vehicle.license," +
                "vehicle.employees_number,vehicle.transported_content,vehicle.vehicle_type," +
                "owner.name,vehicle.active)" +
                "from Vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where owner.id = :id", AbstractVehicleDTO.class)
                .setParameter("id", id_owner)
                .getResultList();
    }

    @Override
    public List<AbstractVehicleDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleDTO(vehicle.id,owner.id,vehicle.license," +
                "vehicle.employees_number,vehicle.transported_content,vehicle.vehicle_type," +
                "owner.name,vehicle.active)" +
                "from Vehicle vehicle " +
                "inner join vehicle.owner owner", AbstractVehicleDTO.class)
                .getResultList();
    }

    @Override
    public AbstractVehicleDTO findByID(Long id_vehicle) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleDTO(vehicle.id,owner.id,vehicle.license," +
                "vehicle.employees_number,vehicle.transported_content,vehicle.vehicle_type," +
                "owner.name,vehicle.active)" +
                "from Vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where vehicle.id = :id", AbstractVehicleDTO.class)
                .setParameter("id", id_vehicle)
                .getSingleResult();
    }

    @Override
    public List<AbstractVehicleDTO> findByPath(String path) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleDTO(" +
                "vehicle.id,vehicle.license,vehicle.transported_content,vehicle.vehicle_type,owner.name,vehicle.active) " +
                "from Vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where vehicle.license like :path " +
                "or owner.name like :path", AbstractVehicleDTO.class)
                .setParameter("path", "%" + path + "%")
                .getResultList();
    }

    @Override
    public List<AbstractVehicleDTO> findAllVehicles(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.vehicle.dto.VehicleDTO(" +
                "vehicle.id,owner.id,vehicle.license,vehicle.employees_number,vehicle.transported_content," +
                "vehicle.vehicle_type,owner.name,vehicle.active) " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles casefile " +
                "inner join casefile.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where bunch.id = :id", AbstractVehicleDTO.class)
                .setParameter("id", id_bunch)
                .getResultList();

    }

    @Override
    public List<AbstractClientDTO> findAllClients(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.ClientDTO(client.id,oc.id.type,client.name,client.nationally," +
                "client.DUI,client.passport,client.resident_card,client.email,client.telephone) " +
                "from Client client " +
                "inner join client.owners oc " +
                "inner join oc.owner owner " +
                "inner join owner.vehicles vehicle " +
                "inner join vehicle.casefile_list cv " +
                "inner join cv.bunch bunch " +
                "where bunch.id = :id " +
                "group by client.id", AbstractClientDTO.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }

    @Override
    public List<AbstractClientDTO> findAllClientsWithNaturalOwner(Long id_bunch) throws NoResultException {
        List<AbstractClientDTO> client_list = new ArrayList<>();
        AbstractOwnerEntityDTO owner = sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.OwnerEntityDTO(owner.id, owner.name, owner.nit, owner.email, owner.telephone, owner.FAX, type(owner)) " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicles " +
                "inner join vehicles.owner owner " +
                "where bunch.id = :id " +
                "group by bunch.id", AbstractOwnerEntityDTO.class)
                .setParameter("id", id_bunch)
                .getSingleResult();
        if (owner.getClass_type().equals(NaturalEntity.class.getSimpleName())) {
            client_list.add(new ClientDTO(owner.getId_owner(), ClientType.PROPIETARIO, owner.getOwner_name(), owner.getOwner_email(), owner.getOwner_telephone()));
        }
        client_list.addAll(sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.base.dto.ClientDTO(client.id,oc.id.type,client.name,client.nationally,client.DUI,client.passport,client.resident_card,client.email,client.telephone) " +
                "from Client client " +
                "inner join client.owners oc " +
                "where oc.id.owner_id = :id", AbstractClientDTO.class).setParameter("id", owner.getId_owner()).getResultList());
        return client_list;
    }

    @Override
    public List<AbstractCaseFileDTO> findAllCaseFiles(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.area.base.dto.CaseFileDTO(" +
                "cv.id,concat(concat(cv.request_folder,'-'),cv.request_year),cv.apply_payment," +
                "cv.certification_type,cv.section,cv.status,cv.certification_duration_in_years," +
                "cv.creation_date,ucsf.name,sibasi.zone) " +
                "from CasefileVehicle cv " +
                "inner join cv.bunch bunch " +
                "left join cv.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "where bunch.id = :id", AbstractCaseFileDTO.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }

    @Override
    public AbstractOwnerEntityDTO findLegalOwner(Long id_bunch) throws NoResultException {
        try {
            return sessionFactory.getCurrentSession().createQuery("select new " +
                    "edu.ues.aps.process.base.dto.LegalEntityDTO(" +
                    "owner.id,owner.name,owner.nit,owner.email,owner.telephone,owner.FAX," +
                    "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)) " +
                    "from LegalEntity owner " +
                    "left join owner.address address " +
                    "inner join owner.vehicles vehicles " +
                    "inner join vehicles.casefile_list cv " +
                    "inner join cv.bunch bunch " +
                    "where bunch.id = :id group by bunch.id", AbstractLegalEntityDTO.class)
                    .setParameter("id", id_bunch)
                    .getSingleResult();
        } catch (HibernateException e) {
            return new EmptyLegalEntityDTO();
        }
    }

    @Override
    public AbstractOwnerEntityDTO findNaturalOwner(Long id_bunch) throws NoResultException {
        try {
            return sessionFactory.getCurrentSession().createQuery("select new " +
                    "edu.ues.aps.process.base.dto.NaturalEntityDTO(" +
                    "owner.id,owner.name,owner.nit,owner.email,owner.telephone,owner.FAX," +
                    "owner.DUI,owner.nationally,owner.passport,owner.residentCard)" +
                    "from NaturalEntity owner " +
                    "inner join owner.vehicles vehicles " +
                    "inner join vehicles.casefile_list cv " +
                    "inner join cv.bunch bunch " +
                    "where bunch.id = :id group by bunch.id", AbstractNaturalEntityDTO.class)
                    .setParameter("id", id_bunch)
                    .getSingleResult();
        } catch (HibernateException e) {
            return new EmptyNaturalEntityDTO();
        }
    }

}
