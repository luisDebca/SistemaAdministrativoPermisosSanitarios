package edu.ues.aps.process.area.establishment.model;

import edu.ues.aps.process.base.model.*;

import java.util.List;

public interface AbstractEstablishment {

    Long getId();

    void setId(Long id);

    String getName();

    void setName(String name);

    String getCommercial_register();

    void setCommercial_register(String commercial_register);

    Double getCapital();

    void setCapital(Double capital);

    String getNIT();

    void setNIT(String NIT);

    String getTelephone();

    void setTelephone(String telephone);

    String getFAX();

    void setFAX(String FAX);

    String getEmail();

    void setEmail(String email);

    Integer getMaleEmployeeCount();

    void setMaleEmployeeCount(Integer maleEmployeeCount);

    Integer getFemaleEmployeeCount();

    void setFemaleEmployeeCount(Integer femaleEmployeeCount);

    String getOperations();

    void setOperations(String operations);

    String getType_detail();

    void setType_detail(String type_detail);

    AbstractEstablishmentType getType();

    void setType(EstablishmentType type);

    AbstractAddress getAddress();

    void setAddress(Address address);

    AbstractOwner getOwner();

    void setOwner(OwnerEntity owner);

    List<CasefileEstablishment> getCasefiles();

    void setCasefiles(List<CasefileEstablishment> casefiles);

    void addCasefile(CasefileEstablishment casefile);

    void removeCasefile(CasefileEstablishment casefile);

    boolean isActive();

    void setActive(boolean active);

    String getTelephone_extra();

    void setTelephone_extra(String telephone_extra);

}
