package edu.ues.aps.process.area.establishment.controller;

import edu.ues.aps.process.area.establishment.service.EstablishmentDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class EstablishmentFromOwnerInformationProviderController {

    private static final Logger logger = LogManager.getLogger(EstablishmentFromOwnerInformationProviderController.class);

    private final static String OWNER_ESTABLISHMENT_LIST = "owner_establishment_list";

    private final EstablishmentDTOFinderService finderService;

    @Autowired
    public EstablishmentFromOwnerInformationProviderController(EstablishmentDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/owner/{id_owner}/establishment")
    public String showAllEstablishmentFromOwner(@PathVariable Long id_owner, ModelMap model){
        logger.info("GET:: Show all establishment from owner {}",id_owner);
        model.addAttribute("establishments",finderService.findAllByOwner(id_owner));
        model.addAttribute("id_owner",id_owner);
        return OWNER_ESTABLISHMENT_LIST;
    }
}
