package edu.ues.aps.process.area.establishment.service;

public interface EstablishmentValidatorService {

    Boolean isUniqueName(String name);

    Boolean isUniqueName(String name, Long id_establishment);

    Boolean isUniqueAddress(String address);

    Boolean isUniqueAddress(String address, Long id_establishment);

    Boolean isUniqueRC(String rc);

    Boolean isUniqueRC(String rc, Long id_establishment);

    Boolean isUniqueNIT(String nit);

    Boolean isUniqueNIT(String nit, Long id_establishment);
}
