package edu.ues.aps.process.area.base.service;

import edu.ues.aps.process.area.base.dto.AbstractRequestDTO;
import edu.ues.aps.process.area.base.dto.EmptyRequestDTO;
import edu.ues.aps.process.area.base.repository.RequestDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class RequestDTOService implements RequestNumberInformationService {

    private final RequestDTOFinderRepository finderRepository;

    public RequestDTOService(RequestDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractRequestDTO find(Long id_caseFile) {
        try {
            return finderRepository.find(id_caseFile);
        } catch (NoResultException e) {
            return new EmptyRequestDTO();
        }
    }

    @Override
    public AbstractRequestDTO getFirst(Long id_bunch) {
        List<AbstractRequestDTO> all = finderRepository.findAll(id_bunch);
        if (all.isEmpty())
            return new EmptyRequestDTO();
        return all.get(0);
    }

    @Override
    public List<AbstractRequestDTO> findAll(Long id_bunch) {
        return finderRepository.findAll(id_bunch);
    }
}
