package edu.ues.aps.process.area.base.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class CaseFileIndexController {

    private static final Logger logger = LogManager.getLogger(CaseFileIndexController.class);

    private static final String CASEFILE_INDEX = "casefile_index";

    @GetMapping({"/caseFile/index", "/caseFile/"})
    public String showCaseFileIndex() {
        logger.info("GET:: Show caseFile index");
        return CASEFILE_INDEX;
    }
}
