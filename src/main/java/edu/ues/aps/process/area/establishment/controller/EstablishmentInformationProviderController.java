package edu.ues.aps.process.area.establishment.controller;

import edu.ues.aps.process.area.establishment.service.EstablishmentDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/establishment")
public class EstablishmentInformationProviderController {

    private static final Logger logger = LogManager.getLogger(EstablishmentInformationProviderController.class);

    private final static String ESTABLISHMENT_INDEX = "establishment_index";
    private final static String ESTABLISHMENT_INFO = "establishment_info";
    private final static String ESTABLISHMENT_LIST = "establishment_list";

    private EstablishmentDTOFinderService finderService;

    public EstablishmentInformationProviderController(EstablishmentDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/index")
    public String showIndex() {
        logger.info("GET:: Show establishment index");
        return ESTABLISHMENT_INDEX;
    }

    @GetMapping({"/{id_establishment}","/{id_establishment}/info"})
    public String showEstablishmentInformation(@PathVariable Long id_establishment, ModelMap model) {
        logger.info("GET:: Show establishment {} information", id_establishment);
        model.addAttribute("establishment", finderService.findById(id_establishment));
        return ESTABLISHMENT_INFO;
    }

    @GetMapping("/list")
    public String showEstablishmentList(ModelMap model) {
        logger.info("GET:: Show establishment list");
        model.addAttribute("establishments", finderService.findAll());
        return ESTABLISHMENT_LIST;
    }

}
