package edu.ues.aps.process.area.vehicle.service;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleBunchDTO;

import java.util.List;

public interface VehicleBunchInformationService {

    AbstractVehicleBunchDTO findById(Long id_bunch);

    List<String> findAllLicensesList(Long id_bunch);

    String findAllLicenses(Long id_bunch);

    String findAllContents(Long id_bunch);

    String findAllTypes(Long id_bunch);

    String findAllCertificationTypes(Long id_bunch);

    List<Long> findAllCaseFileId(Long id_bunch);

}
