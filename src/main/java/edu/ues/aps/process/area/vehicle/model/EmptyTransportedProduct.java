package edu.ues.aps.process.area.vehicle.model;

public class EmptyTransportedProduct implements AbstractTransportedProduct{

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getProduct() {
        return "Product name not found";
    }

    @Override
    public String getDescription() {
        return "Product description not found";
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setProduct(String product) {

    }

    @Override
    public void setDescription(String description) {

    }

    @Override
    public AbstractVehicleBunch getBunch() {
        return new EmptyVehicleBunch();
    }

    @Override
    public void setBunch(VehicleBunch bunch) {

    }

    @Override
    public String toString() {
        return "EmptyTransportedProduct{}";
    }
}
