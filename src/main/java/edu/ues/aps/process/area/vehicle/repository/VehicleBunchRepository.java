package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO;
import edu.ues.aps.process.area.vehicle.model.AbstractVehicleBunch;
import edu.ues.aps.process.area.vehicle.model.TransportedContentType;
import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.CertificationType;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class VehicleBunchRepository implements VehicleBunchPersistenceRepository, VehicleBunchFinderRepository, VehicleBunchInformationRepository {

    private final SessionFactory sessionFactory;

    public VehicleBunchRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractVehicleBunch find(Long id_bunch) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select bunch " +
                "from VehicleBunch bunch " +
                "join fetch bunch.address address " +
                "join fetch address.municipality municipality " +
                "join fetch municipality.department dep " +
                "where bunch.id = :id", AbstractVehicleBunch.class)
                .setParameter("id", id_bunch)
                .getSingleResult();
    }

    @Override
    public List<AbstractCasefile> findAllInBunch(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select casefile " +
                "from CasefileVehicle casefile " +
                "inner join casefile.bunch bunch " +
                "where bunch.id = :id", AbstractCasefile.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }

    @Override
    public void save(AbstractVehicleBunch bunch) {
        sessionFactory.getCurrentSession().persist(bunch);
    }

    @Override
    public void merge(AbstractVehicleBunch bunch) {
        sessionFactory.getCurrentSession().merge(bunch);
    }

    @Override
    public List<String> findAllLicenses(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select vehicle.license " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "where bunch.id = :id", String.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }

    @Override
    public List<TransportedContentType> findAllContents(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select distinct vehicle.transported_content " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "where bunch.id = :id", TransportedContentType.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }

    @Override
    public List<String> findAllTypes(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select distinct vehicle.vehicle_type " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "where bunch.id = :id", String.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }

    @Override
    public List<CertificationType> findAllCertificationTypes(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select distinct cv.certification_type " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "where bunch.id = :id", CertificationType.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }

    @Override
    public List<Long> findAllCaseFileId(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select cv.id " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "where bunch.id = :id", Long.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }
}
