package edu.ues.aps.process.area.base.dto;

import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class RequestProcessIndexSummaryDTO implements AbstractRequestProcessIndexSummaryDTO{

    private String folder_number;
    private String request_number;
    private String target_name;
    private String target_address;
    private String owner_name;
    private CertificationType certification_type;
    private Long target_count;
    private LocalDateTime start_date;

    //For establishment
    public RequestProcessIndexSummaryDTO(String folder_number, String request_number, String target_name, String target_address, String owner_name, CertificationType certification_type, LocalDateTime start_date) {
        this.folder_number = folder_number;
        this.request_number = request_number;
        this.target_name = target_name;
        this.target_address = target_address;
        this.owner_name = owner_name;
        this.certification_type = certification_type;
        this.start_date = start_date;
        this.target_count = 1L;
    }

    //For vehicles
    public RequestProcessIndexSummaryDTO(String folder_number, String request_number, String target_name, String target_address, String owner_name, CertificationType certification_type, Long target_count, LocalDateTime start_date) {
        this.folder_number = folder_number;
        this.request_number = request_number;
        this.target_name = target_name;
        this.target_address = target_address;
        this.owner_name = owner_name;
        this.certification_type = certification_type;
        this.target_count = target_count;
        this.start_date = start_date;
    }

    @Override
    public String getFolder_number() {
        return folder_number;
    }

    @Override
    public String getRequest_number() {
        return request_number;
    }

    @Override
    public String getTarget_name() {
        return target_name;
    }

    @Override
    public String getTarget_address() {
        return target_address;
    }

    @Override
    public String getOwner_name() {
        return owner_name;
    }

    @Override
    public String getCertification_type() {
        if(certification_type == null){
            return CertificationType.INVALID.getCertificationType();
        }
        return certification_type.getCertificationType();
    }

    @Override
    public Long getTarget_count() {
        return target_count;
    }

    @Override
    public String getStart_date() {
        return DatetimeUtil.parseDatetimeToShortFormat(start_date);
    }
}
