package edu.ues.aps.process.area.vehicle.controller;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchCaseFilePersistenceService;
import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.service.SingleCaseFileFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import java.util.List;

@Controller
public class VehicleBunchCaseFilePersistenceController {

    private static final Logger logger = LogManager.getLogger(VehicleBunchCaseFilePersistenceController.class);

    private static final String BUNCH_CASE_FILE_EDIT = "bunch_casefile_edit";

    private final SingleCaseFileFinderService caseFileFinderService;
    private final VehicleBunchInformationService informationService;
    private final VehicleBunchCaseFilePersistenceService persistenceService;

    public VehicleBunchCaseFilePersistenceController(VehicleBunchCaseFilePersistenceService persistenceService, VehicleBunchInformationService informationService, SingleCaseFileFinderService caseFileFinderService) {
        this.persistenceService = persistenceService;
        this.informationService = informationService;
        this.caseFileFinderService = caseFileFinderService;
    }

    @GetMapping({"/vehicle/bunch/{id_bunch}/casefile/edit",
            "/owner/{id_owner}/vehicle/bunch/{id_bunch}/casefile/edit"})
    public String showBunchCaseFileEditForm(@PathVariable Long id_bunch, @PathVariable(required = false) Long id_owner,
                                            ModelMap model) {
        logger.info("GET:: Show case file bunch {} edit form.", id_bunch);
        List<String> licenses_list = informationService.findAllLicensesList(id_bunch);
        List<Long> caseFile_ids = informationService.findAllCaseFileId(id_bunch);
        if (caseFile_ids.isEmpty()) {
            model.addAttribute("caseFile", new Casefile());
        } else {
            model.addAttribute("caseFile", caseFileFinderService.find(caseFile_ids.get(0)));

        }
        model.addAttribute("request_count", licenses_list.size());
        model.addAttribute("licenses", licenses_list);
        model.addAttribute("id_owner", id_owner);
        return BUNCH_CASE_FILE_EDIT;
    }

    @PostMapping("/vehicle/bunch/{id_bunch}/casefile/edit")
    public String updateBunchCaseFile(@PathVariable Long id_bunch, @ModelAttribute Casefile caseFile) {
        logger.info("GET:: Update all case files in bunch {}.", id_bunch);
        persistenceService.updateAllCaseFiles(id_bunch, caseFile);
        return String.format("redirect:/case-file/vehicle/request/bunch/%d/review", id_bunch);
    }

    @PostMapping("/owner/{id_owner}/vehicle/bunch/{id_bunch}/casefile/edit")
    public String updateBunchCaseFile(@PathVariable Long id_bunch, @PathVariable Long id_owner, @ModelAttribute Casefile caseFile) {
        logger.info("GET:: Update all case files in bunch {}.", id_bunch);
        persistenceService.updateAllCaseFiles(id_bunch, caseFile);
        return String.format("redirect:/owner/%d/vehicle/casefile/list", id_owner);
    }
}
