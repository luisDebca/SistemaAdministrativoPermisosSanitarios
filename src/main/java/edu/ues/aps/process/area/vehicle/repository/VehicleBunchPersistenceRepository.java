package edu.ues.aps.process.area.vehicle.repository;

import edu.ues.aps.process.area.vehicle.model.AbstractVehicleBunch;

import javax.persistence.NoResultException;

public interface VehicleBunchPersistenceRepository {

    AbstractVehicleBunch find(Long id_bunch) throws NoResultException;

    void save(AbstractVehicleBunch bunch);

    void merge(AbstractVehicleBunch bunch);

}
