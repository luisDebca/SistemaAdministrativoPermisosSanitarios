package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.dto.RecordCoverDTO;

public interface RecordCoverDataService {

    RecordCoverDTO findDataRecordCoverReport(Long id_caseFile);

}  