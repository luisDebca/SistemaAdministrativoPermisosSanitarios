package edu.ues.aps.process.area.establishment.repository;

public interface EstablishmentValidatorRepository {

    Long isUniqueName(String name);

    Boolean isNameEquals(String name, Long id_establishment);

    Long isUniqueAddress(String address);

    Boolean isAddressEquals(String address, Long id_establishment);

    Long isUniqueRC(String rc);

    Boolean isRCEquals(String rc, Long id_establishment);

    Long isUniqueNIT(String nit);

    Boolean is_NITEquals(String nit, Long id_establishment);
}
