package edu.ues.aps.process.area.establishment.model;

import edu.ues.aps.process.base.model.Casefile;

import javax.persistence.*;

@Entity
@Table(name = "CASEFILE_ESTABLISHMENT")
public class CasefileEstablishment extends Casefile implements AbstractCasefileEstablishment {

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_establishment")
    private Establishment establishment;

    public Establishment getEstablishment() {
        return establishment;
    }

    public void setEstablishment(Establishment establishment) {
        this.establishment = establishment;
    }

    @Override
    public Integer getCertificationDurationInYears() {
        return 3;
    }

    @Override
    public String toString() {
        return "Case file Establishment{ } "
                + super.toString();
    }
}
