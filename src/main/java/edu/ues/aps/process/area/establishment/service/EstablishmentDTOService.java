package edu.ues.aps.process.area.establishment.service;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO;
import edu.ues.aps.process.area.establishment.dto.EmptyEstablishmentDTO;
import edu.ues.aps.process.area.establishment.repository.EstablishmentDTOFinderRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
@Transactional(readOnly = true)
public class EstablishmentDTOService implements EstablishmentDTOFinderService, EstablishmentFinderByParameterService {

    private EstablishmentDTOFinderRepository finderRepository;

    public EstablishmentDTOService(EstablishmentDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractEstablishmentDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public AbstractEstablishmentDTO findById(Long id_establishment) {
        try {
            return finderRepository.findById(id_establishment);
        } catch (NoResultException e) {
            return new EmptyEstablishmentDTO();
        }
    }

    @Override
    public AbstractEstablishmentDTO findByCaseFile(Long id_caseFile) {
        try {
            return finderRepository.findByCaseFile(id_caseFile);
        } catch (NoResultException e) {
            return new EmptyEstablishmentDTO();
        }
    }

    @Override
    public List<AbstractEstablishmentDTO> findAllByOwner(Long id_owner) {
        return finderRepository.findAllByOwner(id_owner);
    }

    @Override
    public List<AbstractEstablishmentDTO> findEstablishmentByParameter(String parameter) {
        return finderRepository.findByParameter(parameter);
    }
}
