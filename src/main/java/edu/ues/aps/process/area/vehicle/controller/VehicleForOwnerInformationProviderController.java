package edu.ues.aps.process.area.vehicle.controller;

import edu.ues.aps.process.area.vehicle.service.VehicleDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@Controller
public class VehicleForOwnerInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleForOwnerInformationProviderController.class);

    private static final String OWNER_VEHICLE_LIST = "owner_vehicle_list";

    private final VehicleDTOFinderService finderService;

    public VehicleForOwnerInformationProviderController(VehicleDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/owner/{id_owner}/vehicle/list")
    public String showAllVehiclesFromOwner(@PathVariable Long id_owner, ModelMap model){
        logger.info("GET:: Show all vehicles for owner {}",id_owner);
        model.addAttribute("vehicles",finderService.findAllFromOwner(id_owner));
        model.addAttribute("id_owner",id_owner);
        return OWNER_VEHICLE_LIST;
    }
}
