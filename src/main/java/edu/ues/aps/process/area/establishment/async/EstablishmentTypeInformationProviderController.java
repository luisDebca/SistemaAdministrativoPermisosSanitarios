package edu.ues.aps.process.area.establishment.async;

import edu.ues.aps.process.area.establishment.service.EstablishmentTypeDTOFinderService;
import edu.ues.aps.process.base.dto.AbstractMapDTO;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class EstablishmentTypeInformationProviderController {

    private static final Logger logger = LogManager.getLogger(EstablishmentTypeInformationProviderController.class);

    private final EstablishmentTypeDTOFinderService finderService;

    public EstablishmentTypeInformationProviderController(EstablishmentTypeDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/find-all-types-from-section", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractMapDTO> findAllTypesFromSection(@RequestParam("id") Long id_section) {
        logger.info("GET:: Find all types from section {}", id_section);
        return finderService.findAllTypesFromSection(id_section);
    }
}
