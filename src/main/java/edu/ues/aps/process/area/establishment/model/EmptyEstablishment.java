package edu.ues.aps.process.area.establishment.model;

import edu.ues.aps.process.base.model.*;

import java.util.Collections;
import java.util.List;

public class EmptyEstablishment implements AbstractEstablishment {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getName() {
        return "Establishment name not found";
    }

    @Override
    public String getCommercial_register() {
        return "Establishment commercial register not found";
    }

    @Override
    public Double getCapital() {
        return 0.0;
    }

    @Override
    public String getNIT() {
        return "Establishment NIT not found";
    }

    @Override
    public String getTelephone() {
        return "Establishment telephone not found";
    }

    @Override
    public String getFAX() {
        return "Establishment FAX not found";
    }

    @Override
    public String getEmail() {
        return "Establishment email not found";
    }

    @Override
    public Integer getMaleEmployeeCount() {
        return 0;
    }

    @Override
    public Integer getFemaleEmployeeCount() {
        return 0;
    }

    @Override
    public String getOperations() {
        return "Establishment operations not found";
    }

    @Override
    public String getType_detail() {
        return "Establishment type details not found";
    }

    @Override
    public AbstractEstablishmentType getType() {
        return new EmptyEstablishmentType();
    }

    @Override
    public AbstractAddress getAddress() {
        return new EmptyAddress();
    }

    @Override
    public AbstractOwner getOwner() {
        return new EmptyOwner();
    }

    @Override
    public List<CasefileEstablishment> getCasefiles() {
        return Collections.emptyList();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setName(String name) {

    }

    @Override
    public void setCommercial_register(String commercial_register) {

    }

    @Override
    public void setCapital(Double capital) {

    }

    @Override
    public void setNIT(String NIT) {

    }

    @Override
    public void setTelephone(String telephone) {

    }

    @Override
    public void setFAX(String FAX) {

    }

    @Override
    public void setEmail(String email) {

    }

    @Override
    public void setMaleEmployeeCount(Integer maleEmployeeCount) {

    }

    @Override
    public void setFemaleEmployeeCount(Integer femaleEmployeeCount) {

    }

    @Override
    public void setOperations(String operations) {

    }

    @Override
    public void setType_detail(String type_detail) {

    }

    @Override
    public void setType(EstablishmentType type) {

    }

    @Override
    public void setAddress(Address address) {

    }

    @Override
    public void setOwner(OwnerEntity owner) {

    }

    @Override
    public void setCasefiles(List<CasefileEstablishment> casefiles) {

    }

    @Override
    public void addCasefile(CasefileEstablishment casefile) {

    }

    @Override
    public void removeCasefile(CasefileEstablishment casefile) {

    }

    @Override
    public boolean isActive() {
        return false;
    }

    @Override
    public void setActive(boolean active) {

    }

    @Override
    public String getTelephone_extra() {
        return "Establishment telephone not found";
    }

    @Override
    public void setTelephone_extra(String telephone_extra) {

    }

    @Override
    public String toString() {
        return "EmptyEstablishment{} " + super.toString();
    }
}
