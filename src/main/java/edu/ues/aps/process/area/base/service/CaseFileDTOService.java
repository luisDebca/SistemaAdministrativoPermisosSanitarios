package edu.ues.aps.process.area.base.service;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.base.repository.CaseFileDTOFinderRepository;
import edu.ues.aps.process.area.base.service.CaseFileDTOFinderService;
import edu.ues.aps.process.base.model.AbstractCasefile;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class CaseFileDTOService implements CaseFileDTOFinderService, CaseFileDTOFinderByIdentifierService {

    private final CaseFileDTOFinderRepository finderRepository;

    public CaseFileDTOService(CaseFileDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractCaseFileDTO find(Long id_caseFile) {
        AbstractCaseFileDTO dto = finderRepository.findById(id_caseFile);
        dto.setId_casefile(id_caseFile);
        return dto;
    }

    @Override
    public List<AbstractCaseFileDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public List<AbstractCaseFileDTO> findAllByIdentifier(String identifier) {
        return finderRepository.findByIdentifier(identifier);
    }

}
