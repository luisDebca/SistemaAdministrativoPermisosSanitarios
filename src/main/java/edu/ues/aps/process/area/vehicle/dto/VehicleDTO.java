package edu.ues.aps.process.area.vehicle.dto;

import edu.ues.aps.process.area.vehicle.model.TransportedContentType;

public class VehicleDTO implements AbstractVehicleDTO{

    private Long id_vehicle;
    private Long id_owner;
    private String vehicle_license;
    private Integer vehicle_employees_number;
    private TransportedContentType vehicle_transported_content;
    private String vehicle_type;
    private String owner_name;
    private Boolean active;

    //For vehicle index
    public VehicleDTO(Long id_vehicle, String vehicle_license, TransportedContentType vehicle_transported_content, String vehicle_type, String owner_name, Boolean active) {
        this.id_vehicle = id_vehicle;
        this.vehicle_license = vehicle_license;
        this.vehicle_transported_content = vehicle_transported_content;
        this.vehicle_type = vehicle_type;
        this.owner_name = owner_name;
        this.active = active;
    }

    //For owner vehicles list and vehicle list
    public VehicleDTO(Long id_vehicle, Long id_owner, String vehicle_license, Integer vehicle_employees_number, TransportedContentType vehicle_transported_content, String vehicle_type, String owner_name, Boolean active) {
        this.id_vehicle = id_vehicle;
        this.id_owner = id_owner;
        this.vehicle_license = vehicle_license;
        this.vehicle_employees_number = vehicle_employees_number;
        this.vehicle_transported_content = vehicle_transported_content;
        this.vehicle_type = vehicle_type;
        this.owner_name = owner_name;
        this.active = active;
    }

    public Long getId_vehicle() {
        return id_vehicle;
    }

    public Long getId_owner() {
        return id_owner;
    }

    public String getVehicle_license() {
        return vehicle_license;
    }

    public Integer getVehicle_employees_number() {
        return vehicle_employees_number;
    }

    public String getVehicle_transported_content() {
        if(vehicle_transported_content == null){
            return TransportedContentType.INVALID.getTransportedContentType();
        }
        return vehicle_transported_content.getTransportedContentType();
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public Boolean getActive() {
        return active;
    }

}
