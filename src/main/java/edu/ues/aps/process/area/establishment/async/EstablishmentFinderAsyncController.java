package edu.ues.aps.process.area.establishment.async;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO;
import edu.ues.aps.process.area.establishment.service.EstablishmentFinderByParameterService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class EstablishmentFinderAsyncController {

    private static final Logger logger = LogManager.getLogger(EstablishmentFinderAsyncController.class);

    private final EstablishmentFinderByParameterService finderByParameterService;

    public EstablishmentFinderAsyncController(EstablishmentFinderByParameterService finderByParameterService) {
        this.finderByParameterService = finderByParameterService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/find-establishment-by-parameter", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractEstablishmentDTO> findEstablishmentByParameter(@RequestParam String param) {
        logger.info("GET:: Find establishment by '{}'", param);
        return finderByParameterService.findEstablishmentByParameter(param);
    }
}
