package edu.ues.aps.process.area.vehicle.dto;

import edu.ues.aps.process.base.model.Address;

import java.util.Collections;
import java.util.List;

public class VehicleBunchFormDTO {

    private Long id_owner;
    private Long id_bunch;
    private String client_presented_name;
    private String client_presented_telephone;
    private String distribution_company;
    private String distribution_destination;
    private String marketing_place;
    private Integer vehicle_count;
    private Address address;
    private List<Long> selected_vehicles;

    public Long getId_owner() {
        return id_owner;
    }

    public void setId_owner(Long id_owner) {
        this.id_owner = id_owner;
    }

    public Long getId_bunch() {
        return id_bunch;
    }

    public void setId_bunch(Long id_bunch) {
        this.id_bunch = id_bunch;
    }

    public String getClient_presented_name() {
        return client_presented_name;
    }

    public void setClient_presented_name(String client_presented_name) {
        this.client_presented_name = client_presented_name;
    }

    public String getClient_presented_telephone() {
        return client_presented_telephone;
    }

    public void setClient_presented_telephone(String client_presented_telephone) {
        this.client_presented_telephone = client_presented_telephone;
    }

    public String getDistribution_company() {
        return distribution_company;
    }

    public void setDistribution_company(String distribution_company) {
        this.distribution_company = distribution_company;
    }

    public String getDistribution_destination() {
        return distribution_destination;
    }

    public void setDistribution_destination(String distribution_destination) {
        this.distribution_destination = distribution_destination;
    }

    public String getMarketing_place() {
        return marketing_place;
    }

    public void setMarketing_place(String marketing_place) {
        this.marketing_place = marketing_place;
    }

    public Integer getVehicle_count() {
        return vehicle_count;
    }

    public void setVehicle_count(Integer vehicle_count) {
        this.vehicle_count = vehicle_count;
    }

    public Address getAddress() {
        return address;
    }

    public void setAddress(Address address) {
        this.address = address;
    }

    public List<Long> getSelected_vehicles() {
        if(selected_vehicles == null)
            return Collections.emptyList();
        return selected_vehicles;
    }

    public void setSelected_vehicles(List<Long> selected_vehicles) {
        this.selected_vehicles = selected_vehicles;
    }
}
