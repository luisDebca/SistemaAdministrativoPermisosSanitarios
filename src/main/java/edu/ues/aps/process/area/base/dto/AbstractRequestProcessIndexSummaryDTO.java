package edu.ues.aps.process.area.base.dto;

public interface AbstractRequestProcessIndexSummaryDTO {

    String getFolder_number();

    String getRequest_number();

    String getTarget_name();

    String getTarget_address();

    String getOwner_name();

    String getCertification_type();

    Long getTarget_count();

    String getStart_date();
}
