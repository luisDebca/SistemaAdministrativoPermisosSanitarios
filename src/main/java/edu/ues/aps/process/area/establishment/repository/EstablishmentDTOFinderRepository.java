package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface EstablishmentDTOFinderRepository {

    AbstractEstablishmentDTO findById(Long id_establishment) throws NoResultException;

    AbstractEstablishmentDTO findByCaseFile(Long id_caseFile) throws NoResultException;

    List<AbstractEstablishmentDTO> findAll();

    List<AbstractEstablishmentDTO> findAllByOwner(Long id_owner);

    List<AbstractEstablishmentDTO> findByParameter(String parameter);
}
