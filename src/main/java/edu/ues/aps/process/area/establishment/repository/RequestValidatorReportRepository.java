package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.process.area.establishment.dto.AbstractRequestOnProcessValidityReport;

import javax.persistence.NoResultException;

public interface RequestValidatorReportRepository {

    AbstractRequestOnProcessValidityReport getRequestProcessData(Long id_caseFile) throws NoResultException;
}
