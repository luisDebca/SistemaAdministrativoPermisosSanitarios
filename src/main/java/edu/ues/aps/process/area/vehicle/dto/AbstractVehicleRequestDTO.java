package edu.ues.aps.process.area.vehicle.dto;

public interface AbstractVehicleRequestDTO {

    Long getId_bunch();

    Long getId_owner();

    String getOwner_name();

    String getBunch_presented_name();

    String getBunch_presented_phone();

    String getBunch_distribution_company();

    String getBunch_distribution_destination();

    String getBunch_marketing_place();

    String getBunch_address();

    Long getRequest_count();

    String getRequest_folder();

    String getRequest_create_on();

}
