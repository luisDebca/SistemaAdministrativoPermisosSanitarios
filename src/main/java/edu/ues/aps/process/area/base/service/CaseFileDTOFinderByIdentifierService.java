package edu.ues.aps.process.area.base.service;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;

import java.util.List;

public interface CaseFileDTOFinderByIdentifierService {

    List<AbstractCaseFileDTO> findAllByIdentifier(String identifier);
}
