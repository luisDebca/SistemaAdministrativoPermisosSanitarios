package edu.ues.aps.process.tracing.establishment.repository;

import edu.ues.aps.process.tracing.establishment.dto.AbstractTracingEstablishmentDTO;

import java.util.List;

public interface TracingProcessFinderRepository {

    List<AbstractTracingEstablishmentDTO> findAllTracingProcessEntities();
}
