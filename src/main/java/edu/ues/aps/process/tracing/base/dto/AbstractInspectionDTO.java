package edu.ues.aps.process.tracing.base.dto;

public interface AbstractInspectionDTO {

    Long getId_tracing();

    void setId_tracing(Long id_tracing);

    Long getId_inspection();

    String getInspection_result_type();

    Integer getInspection_extension_received();

    String getInspection_inspected_on();

    String getInspection_sibasi_received();

    String getInspection_memorandum_number();

    String getInspection_observation();

    String getInspection_technical_name();

    String getInspection_technical_charge();

    Integer getInspection_score();
}
