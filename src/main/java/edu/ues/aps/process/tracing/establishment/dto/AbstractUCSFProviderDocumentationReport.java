package edu.ues.aps.process.tracing.establishment.dto;

import edu.ues.aps.process.tracing.base.model.InspectionResultType;

import java.time.LocalDateTime;
import java.util.List;

public interface AbstractUCSFProviderDocumentationReport {

    String getReturn_date();

    String getReview_date();

    String getDelivery_date();

    String getEstablishment_name();

    String getEstablishment_address();

    Boolean getEstablishment_report();

    String getOwner_name();

    String getUcsf();

    String getSibasi();

    String getInspection1();

    String getInspection2();

    String getInspection3();

    String getInspection4();

    Boolean getInspection_result();

    String getObservation();

    String getToday();

    String getFor_name();

    String getFrom_name();

    void setReturn_date(LocalDateTime return_date);

    void setFor_name(String for_name);

    void setFrom_name(String from_name);

    void setInspection_result(Boolean inspection_result);

    List<InspectionResultType> getInspection_result_list();

    void setInspection_result_list(List<InspectionResultType> inspection_result_list);
}
