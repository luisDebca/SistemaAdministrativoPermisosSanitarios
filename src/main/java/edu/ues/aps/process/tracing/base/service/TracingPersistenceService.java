package edu.ues.aps.process.tracing.base.service;

import edu.ues.aps.process.tracing.base.model.AbstractTracing;

public interface TracingPersistenceService {

    AbstractTracing find(Long id_caseFile);

    void saveTracing(AbstractTracing tracing, Long id_caseFile);

    void updateTracing(AbstractTracing tracing);
}
