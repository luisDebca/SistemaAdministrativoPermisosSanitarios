package edu.ues.aps.process.tracing.base.repository;

import edu.ues.aps.process.tracing.base.model.AbstractTracing;
import edu.ues.aps.process.tracing.base.model.Tracing;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class TracingRepository implements TracingFinderRepository {

    private final SessionFactory sessionFactory;

    public TracingRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractTracing find(Long id_caseFile) {
        return sessionFactory.getCurrentSession().get(Tracing.class, id_caseFile);
    }
}
