package edu.ues.aps.process.tracing.establishment.service;

import edu.ues.aps.process.tracing.base.repository.UCSFDocumentationProviderRepository;
import edu.ues.aps.process.tracing.base.service.AbstractUCSFDocumentationProviderService;
import edu.ues.aps.process.tracing.base.service.UCSFDocumentationProviderService;
import edu.ues.aps.process.tracing.establishment.dto.AbstractUCSFProviderDocumentationReport;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional(readOnly = true)
public class EstablishmentUCSFDocumentationProviderService extends AbstractUCSFDocumentationProviderService implements UCSFDocumentationProviderService {

    private final UCSFDocumentationProviderRepository providerRepository;

    public EstablishmentUCSFDocumentationProviderService(@Qualifier("establishmentUCSFDocumentationProviderRepository") UCSFDocumentationProviderRepository providerRepository) {
        this.providerRepository = providerRepository;
    }

    @Override
    public AbstractUCSFProviderDocumentationReport find(Long id) {
        setReport(providerRepository.find(id));
        setInspectionResult();
        return getReport();
    }
}
