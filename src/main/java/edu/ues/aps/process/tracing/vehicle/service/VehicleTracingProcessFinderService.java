package edu.ues.aps.process.tracing.vehicle.service;

import edu.ues.aps.process.tracing.vehicle.dto.AbstractVehicleTracingProcessDTO;

import java.util.List;

public interface VehicleTracingProcessFinderService {

    List<AbstractVehicleTracingProcessDTO> findAllRequest();
}
