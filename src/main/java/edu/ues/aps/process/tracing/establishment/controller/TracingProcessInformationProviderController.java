package edu.ues.aps.process.tracing.establishment.controller;

import edu.ues.aps.process.tracing.base.service.TracingFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tracing/establishment")
public class TracingProcessInformationProviderController {

    private static final String TRACING_PROCESS_REQUEST_LIST = "request_tracing_list";

    private static final Logger logger = LogManager.getLogger(TracingProcessInformationProviderController.class);

    private final TracingFinderService finderService;

    public TracingProcessInformationProviderController(TracingFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showAllRequestForTracingProcessReview(ModelMap model) {
        logger.info("GET:: Show all request for tracing review process.");
        model.addAttribute("requests", finderService.findAllRequestForTracingProcess());
        return TRACING_PROCESS_REQUEST_LIST;
    }
}
