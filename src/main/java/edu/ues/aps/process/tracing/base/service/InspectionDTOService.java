package edu.ues.aps.process.tracing.base.service;

import edu.ues.aps.process.tracing.base.dto.AbstractInspectionDTO;
import edu.ues.aps.process.tracing.base.dto.EmptyInspectionDTO;
import edu.ues.aps.process.tracing.base.repository.InspectionDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class InspectionDTOService implements InspectionDTOFinderService {

    private final InspectionDTOFinderRepository finderRepository;

    public InspectionDTOService(InspectionDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractInspectionDTO find(Long id_inspection) {
        try {
            return finderRepository.findById(id_inspection);
        } catch (NoResultException e) {
            return new EmptyInspectionDTO();
        }
    }

    @Override
    public List<AbstractInspectionDTO> findAll(Long id_tracing) {
        return finderRepository.findAll(id_tracing);
    }

    @Override
    public List<AbstractInspectionDTO> findBunchAll(Long id_bunch) {
        return finderRepository.findBunchAll(id_bunch);
    }
}
