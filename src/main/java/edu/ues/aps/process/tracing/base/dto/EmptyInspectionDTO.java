package edu.ues.aps.process.tracing.base.dto;

public class EmptyInspectionDTO implements AbstractInspectionDTO {

    @Override
    public Long getId_tracing() {
        return 0L;
    }

    @Override
    public void setId_tracing(Long id_tracing) {

    }

    @Override
    public Long getId_inspection() {
        return 0L;
    }

    @Override
    public String getInspection_result_type() {
        return "Inspection result not found";
    }

    @Override
    public Integer getInspection_extension_received() {
        return 0;
    }

    @Override
    public String getInspection_inspected_on() {
        return "----/--/-- --:--:--";
    }

    @Override
    public String getInspection_sibasi_received() {
        return "----/--/-- --:--:--";
    }

    @Override
    public String getInspection_memorandum_number() {
        return "####-####-####-##-###";
    }

    @Override
    public String getInspection_observation() {
        return "Inspection observation not found";
    }

    @Override
    public String getInspection_technical_name() {
        return "Inspection technical name not found";
    }

    @Override
    public String getInspection_technical_charge() {
        return "Inspection technical charge not found";
    }

    @Override
    public Integer getInspection_score() {
        return 0;
    }
}
