package edu.ues.aps.process.tracing.base.repository;

import edu.ues.aps.process.tracing.base.model.AbstractInspection;

import java.util.List;

public interface InspectionPersistenceRepository {

    AbstractInspection find(Long id_inspection);

    List<AbstractInspection> findAll(Long id_tracing);

}
