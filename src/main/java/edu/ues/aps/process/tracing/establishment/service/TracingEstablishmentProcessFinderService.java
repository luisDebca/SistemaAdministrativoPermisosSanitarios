package edu.ues.aps.process.tracing.establishment.service;

import edu.ues.aps.process.tracing.establishment.repository.TracingProcessFinderRepository;
import edu.ues.aps.process.tracing.base.service.TracingFinderService;
import edu.ues.aps.process.tracing.establishment.dto.AbstractTracingEstablishmentDTO;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class TracingEstablishmentProcessFinderService implements TracingFinderService {

    private final TracingProcessFinderRepository finderRepository;

    public TracingEstablishmentProcessFinderService(TracingProcessFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractTracingEstablishmentDTO> findAllRequestForTracingProcess() {
        return finderRepository.findAllTracingProcessEntities();
    }
}
