package edu.ues.aps.process.tracing.establishment.repository;

import edu.ues.aps.process.tracing.base.model.InspectionResultType;
import edu.ues.aps.process.tracing.base.repository.UCSFDocumentationProviderRepository;
import edu.ues.aps.process.tracing.establishment.dto.AbstractUCSFProviderDocumentationReport;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class EstablishmentUCSFDocumentationProviderRepository implements UCSFDocumentationProviderRepository {

    private final SessionFactory sessionFactory;

    public EstablishmentUCSFDocumentationProviderRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractUCSFProviderDocumentationReport find(Long id_caseFile) throws NoResultException {
        AbstractUCSFProviderDocumentationReport result = sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.tracing.establishment.dto.UCSFProviderDocumentationDTOReport(" +
                "tracing.return_date,tracing.ucsf_received_on,tracing.review_date,establishment.name," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "casefile.ucsf.name,casefile.ucsf.sibasi.zone," +
                "tracing.observation,owner.name,true) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.owner owner " +
                "inner join establishment.address address " +
                "inner join casefile.tracing tracing " +
                "where casefile.id = :id", AbstractUCSFProviderDocumentationReport.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
        result.setInspection_result_list(getInspectionResultTypes(id_caseFile));
        return result;
    }

    private List<InspectionResultType> getInspectionResultTypes(Long id_caseFile) {
        return sessionFactory.getCurrentSession().createQuery("select inspection.result_type " +
                "from Inspection inspection " +
                "where inspection.tracing.id = :id", InspectionResultType.class)
                .setParameter("id", id_caseFile)
                .getResultList();
    }

}
