package edu.ues.aps.process.tracing.base.repository;

import edu.ues.aps.process.tracing.base.dto.AbstractInspectionDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class InspectionDTORepository implements InspectionDTOFinderRepository {

    protected final SessionFactory sessionFactory;

    public InspectionDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractInspectionDTO> findAll(Long id_tracing) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.tracing.base.dto.InspectionDTO(" +
                "inspection.id,inspection.result_type,inspection.extension_received,inspection.inspected_on,inspection.sibasi_received) " +
                "from Tracing tracing " +
                "inner join tracing.inspections inspection " +
                "where tracing.id = :id",AbstractInspectionDTO.class)
                .setParameter("id",id_tracing)
                .getResultList();
    }

    @Override
    public List<AbstractInspectionDTO> findBunchAll(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.tracing.base.dto.InspectionDTO(" +
                "inspection.id,inspection.result_type,inspection.extension_received,inspection.inspected_on,inspection.sibasi_received) " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.tracing tracing " +
                "inner join tracing.inspections inspection " +
                "where bunch.id = :id " +
                "group by inspection.memorandum_number",AbstractInspectionDTO.class)
                .setParameter("id",id_bunch)
                .getResultList();
    }

    @Override
    public AbstractInspectionDTO findById(Long id_inspection) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.tracing.base.dto.InspectionDTO(" +
                "inspection.memorandum_number,inspection.observation,inspection.technical_name,inspection.technical_charge," +
                "inspection.result_type,inspection.extension_received,inspection.score,inspection.inspected_on,inspection.sibasi_received) " +
                "from Inspection inspection " +
                "where inspection.id = :id",AbstractInspectionDTO.class)
                .setParameter("id",id_inspection)
                .getSingleResult();
    }

}
