package edu.ues.aps.process.tracing.base.service;

import edu.ues.aps.process.tracing.base.model.InspectionResultType;
import edu.ues.aps.process.tracing.establishment.dto.AbstractUCSFProviderDocumentationReport;

public abstract class AbstractUCSFDocumentationProviderService {

    private AbstractUCSFProviderDocumentationReport report;

    public AbstractUCSFProviderDocumentationReport getReport() {
        return report;
    }

    public void setReport(AbstractUCSFProviderDocumentationReport report) {
        this.report = report;
    }

    public void setInspectionResult() {
        if (isLastInspectionFavorable())
            report.setInspection_result(true);
    }

    public Boolean isLastInspectionFavorable() {
        return report.getInspection_result_list().get(getInspectionSize() - 1).equals(InspectionResultType.FAVORABLE);
    }

    public Integer getInspectionSize() {
        return report.getInspection_result_list().size();
    }
}
