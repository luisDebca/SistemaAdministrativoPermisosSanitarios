package edu.ues.aps.process.tracing.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.users.management.model.MailRegister;

import java.time.LocalDateTime;
import java.util.List;

public interface AbstractTracing {

    Long getId();

    void setId(Long id);

    LocalDateTime getUcsf_received_on();

    void setUcsf_received_on(LocalDateTime ucsf_received_on);

    LocalDateTime getCreated_on();

    void setCreated_on(LocalDateTime created_on);

    String getObservation();

    void setObservation(String observation);

    List<Inspection> getInspections();

    void setInspections(List<Inspection> inspections);

    AbstractCasefile getCasefile();

    void setCasefile(Casefile casefile);

    void addInspection(Inspection inspection);

    void removeInspection(Inspection inspection);

    List<MailRegister> getMailRegisters();

    void setMailRegisters(List<MailRegister> mailRegisters);

    void addMailRegister(MailRegister register);

    LocalDateTime getShipping_date();

    void setShipping_date(LocalDateTime shipping_date);

    LocalDateTime getReturn_date();

    void setReturn_date(LocalDateTime return_date);

    LocalDateTime getReview_date();

    void setReview_date(LocalDateTime review_date);
}
