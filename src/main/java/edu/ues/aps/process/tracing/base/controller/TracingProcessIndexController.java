package edu.ues.aps.process.tracing.base.controller;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class TracingProcessIndexController {

    private static final Logger logger = LogManager.getLogger(TracingProcessIndexController.class);

    private static final String TRACING_INDEX = "request_tracing_index";

    @GetMapping({"/tracing/","/tracing/index"})
    public String showTracingProcessIndex(ModelMap model){
        logger.info("GET:: Show Tracing process index");
        model.addAttribute("process", ProcessIdentifier.REQUEST_TRACING);
        return TRACING_INDEX;
    }

}
