package edu.ues.aps.process.tracing.base.controller;

import edu.ues.aps.process.tracing.base.model.AbstractTracing;
import edu.ues.aps.process.tracing.base.model.Tracing;
import edu.ues.aps.process.tracing.base.service.TracingPersistenceService;
import org.springframework.ui.ModelMap;

public abstract class TracingPersistenceController {

    private final TracingPersistenceService persistenceService;

    public TracingPersistenceController(TracingPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public abstract String showNewTracingForm(Long id, ModelMap model);

    public abstract String saveTracing(Long id, Tracing tracing);

    public abstract String showTracingEditForm(Long id, ModelMap model);

    public abstract String updateTracing(Long id, Tracing tracing);

    public void save(Tracing tracing, Long id_caseFile) {
        persistenceService.saveTracing(tracing, id_caseFile);
    }

    public void update(Tracing tracing) {
        persistenceService.updateTracing(tracing);
    }

    public AbstractTracing getTracing(Long id_tracing) {
        return persistenceService.find(id_tracing);
    }
}
