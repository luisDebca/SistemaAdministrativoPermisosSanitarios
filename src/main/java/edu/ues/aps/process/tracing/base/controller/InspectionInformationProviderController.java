package edu.ues.aps.process.tracing.base.controller;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.tracing.base.service.InspectionDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Collections;
import java.util.List;

@Controller
@RequestMapping("/tracing")
public class InspectionInformationProviderController {

    private static final Logger logger = LogManager.getLogger(InspectionInformationProviderController.class);

    private static final String ESTABLISHMENT_INSPECTION = "establishment_inspection_list";
    private static final String VEHICLE_INSPECTION = "vehicle_inspection_list";

    private final VehicleBunchInformationService informationService;
    private final InspectionDTOFinderService finderService;

    public InspectionInformationProviderController(InspectionDTOFinderService finderService, VehicleBunchInformationService informationService) {
        this.finderService = finderService;
        this.informationService = informationService;
    }

    @GetMapping("/{id_tracing}/inspection/all")
    public String showAllInspectionsFromTracing(@PathVariable Long id_tracing, ModelMap model) {
        logger.info("GET:: Show all inspections from tracing {}", id_tracing);
        model.addAttribute("id_tracing", id_tracing);
        model.addAttribute("inspections", finderService.findAll(id_tracing));
        return ESTABLISHMENT_INSPECTION;
    }

    @GetMapping("/bunch/{id_bunch}/inspection/all")
    public String showAllInspectionForTracingBunch(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show all inspection for bunch {} tracing.", id_bunch);
        List<Long> allCaseFileId = informationService.findAllCaseFileId(id_bunch);
        if (!allCaseFileId.isEmpty()) {
            model.addAttribute("inspections", finderService.findAll(allCaseFileId.get(0)));
        } else {
            model.addAttribute("inspections", Collections.emptyList());
        }
        model.addAttribute("id_bunch", id_bunch);
        return VEHICLE_INSPECTION;
    }
}
