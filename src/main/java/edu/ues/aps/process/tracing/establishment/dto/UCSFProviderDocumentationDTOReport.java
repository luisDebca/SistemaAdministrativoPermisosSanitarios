package edu.ues.aps.process.tracing.establishment.dto;

import edu.ues.aps.process.tracing.base.model.InspectionResultType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

public class UCSFProviderDocumentationDTOReport implements AbstractUCSFProviderDocumentationReport {

    private LocalDateTime return_date;
    private LocalDateTime review_date;
    private LocalDateTime delivery_date;
    private String establishment_name;
    private String establishment_address;
    private Boolean establishment_report;
    private String ucsf;
    private String sibasi;
    private List<InspectionResultType> inspection_result_list;
    private Boolean inspection_result = false;
    private String observation;
    private String owner_name;
    private String for_name;
    private String from_name;

    public UCSFProviderDocumentationDTOReport(LocalDateTime return_date, LocalDateTime review_date, LocalDateTime delivery_date ,String establishment_name, String establishment_address, String ucsf, String sibasi, String observation, String owner_name, Boolean establishment_report) {
        this.return_date = return_date;
        this.review_date = review_date;
        this.delivery_date = delivery_date;
        this.establishment_name = establishment_name;
        this.establishment_address = establishment_address;
        this.ucsf = ucsf;
        this.sibasi = sibasi;
        this.observation = observation;
        this.owner_name = owner_name;
        this.establishment_report = establishment_report;
    }

    public String getReturn_date() {
        if(return_date == null){
            return DatetimeUtil.DATE_NOT_DEFINED;
        }
        return DatetimeUtil.parseDateToShortFormat(return_date.toLocalDate());
    }

    public String getReview_date() {
        if(review_date == null){
            return DatetimeUtil.DATE_NOT_DEFINED;
        }
        return DatetimeUtil.parseDateToShortFormat(review_date.toLocalDate());
    }

    public String getDelivery_date() {
        if(delivery_date == null){
            return DatetimeUtil.DATE_NOT_DEFINED;
        }
        return DatetimeUtil.parseDateToShortFormat(delivery_date.toLocalDate());
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public Boolean getEstablishment_report() {
        return establishment_report;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getSibasi() {
        return sibasi;
    }

    public String getInspection1() {
        if(inspection_result_list.size() >= 1){
            return inspection_result_list.get(0).getInspectionResultType();
        }
        return InspectionResultType.INVALID.getInspectionResultType();
    }

    public String getInspection2() {
        if(inspection_result_list.size() >= 2){
            return inspection_result_list.get(1).getInspectionResultType();
        }
        return InspectionResultType.INVALID.getInspectionResultType();
    }

    public String getInspection3() {
        if(inspection_result_list.size() >= 3){
            return inspection_result_list.get(2).getInspectionResultType();
        }
        return InspectionResultType.INVALID.getInspectionResultType();
    }

    public String getInspection4() {
        if(inspection_result_list.size() >= 4){
            return inspection_result_list.get(3).getInspectionResultType();
        }
        return InspectionResultType.INVALID.getInspectionResultType();
    }

    public Boolean getInspection_result() {
        return inspection_result;
    }

    public String getObservation() {
        return observation;
    }

    public String getToday() {
        return DatetimeUtil.parseDateToShortFormat(LocalDate.now());
    }

    public String getFor_name() {
        return for_name;
    }

    public String getFrom_name() {
        return from_name;
    }

    public void setReturn_date(LocalDateTime return_date) {
        this.return_date = return_date;
    }

    public void setFor_name(String for_name) {
        this.for_name = for_name;
    }

    public void setFrom_name(String from_name) {
        this.from_name = from_name;
    }

    public void setInspection_result(Boolean inspection_result) {
        this.inspection_result = inspection_result;
    }

    public List<InspectionResultType> getInspection_result_list() {
        return inspection_result_list;
    }

    public void setInspection_result_list(List<InspectionResultType> inspection_result_list) {
        this.inspection_result_list = inspection_result_list;
    }
}
