package edu.ues.aps.process.tracing.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentRequestDTO;

public interface AbstractTracingEstablishmentDTO extends AbstractEstablishmentRequestDTO {

    Boolean getTracing();

    String getSend_date();

    String getReceived_date();

    String getReturn_date();

    Long getInspection_count();
}
