package edu.ues.aps.process.tracing.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleRequestDTO;

public interface AbstractVehicleTracingProcessDTO extends AbstractVehicleRequestDTO {

    String getUcsf();

    String getSibasi();

    Boolean getIs_tracing();

    String getDelivery_date();

    String getReceived_date();

    String getReturn_date();

    Long getInspection_count();
}
