package edu.ues.aps.process.tracing.base.model;

import java.time.LocalDateTime;

public class EmptyInspection implements AbstractInspection {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getMemorandum_number() {
        return "Inspection memorandum number not found";
    }

    @Override
    public String getTechnical_name() {
        return "Inspection technical name not found";
    }

    @Override
    public String getTechnical_charge() {
        return "Inspection technical charge not found";
    }

    @Override
    public String getObservation() {
        return "Inspection observation not found";
    }

    @Override
    public InspectionResultType getResult_type() {
        return InspectionResultType.UNDELIVERED;
    }

    @Override
    public Integer getExtension_received() {
        return 0;
    }

    @Override
    public LocalDateTime getInspected_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public LocalDateTime getSibasi_received() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public AbstractTracing getTracing() {
        return new EmptyTracing();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setMemorandum_number(String memorandum_number) {

    }

    @Override
    public void setTechnical_name(String technical_name) {

    }

    @Override
    public void setTechnical_charge(String technical_charge) {

    }

    @Override
    public void setObservation(String observation) {

    }

    @Override
    public void setResult_type(InspectionResultType result_type) {

    }

    @Override
    public void setExtension_received(Integer extension_received) {

    }

    @Override
    public void setInspected_on(LocalDateTime inspected_on) {

    }

    @Override
    public void setSibasi_received(LocalDateTime sibasi_received) {

    }

    @Override
    public void setTracing(Tracing tracing) {

    }

    @Override
    public Double getScore() {
        return 0.0;
    }

    @Override
    public void setScore(Double score) {

    }


    @Override
    public String toString() {
        return "EmptyInspection{}";
    }
}
