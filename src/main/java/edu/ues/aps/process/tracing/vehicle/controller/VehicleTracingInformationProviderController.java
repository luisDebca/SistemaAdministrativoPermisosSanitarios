package edu.ues.aps.process.tracing.vehicle.controller;

import edu.ues.aps.process.tracing.vehicle.service.VehicleTracingProcessFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/tracing/vehicle")
public class VehicleTracingInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleTracingInformationProviderController.class);

    private static final String TRACING_LIST = "vehicle_request_tracing_list";

    private final VehicleTracingProcessFinderService finderService;

    public VehicleTracingInformationProviderController(VehicleTracingProcessFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showVehicleTracingProcess(ModelMap model) {
        logger.info("GET:: Show all request for tracing process.");
        model.addAttribute("requests", finderService.findAllRequest());
        return TRACING_LIST;
    }
}
