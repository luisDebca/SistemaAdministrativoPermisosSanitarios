package edu.ues.aps.process.tracing.vehicle.service;

import edu.ues.aps.process.tracing.vehicle.dto.AbstractVehicleTracingProcessDTO;
import edu.ues.aps.process.tracing.vehicle.repository.VehicleTracingProcessFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleTracingProcessServer implements VehicleTracingProcessFinderService {

    private final VehicleTracingProcessFinderRepository finderRepository;

    public VehicleTracingProcessServer(VehicleTracingProcessFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractVehicleTracingProcessDTO> findAllRequest() {
        return finderRepository.findAllRequest();
    }
}
