package edu.ues.aps.process.tracing.base.service;

import edu.ues.aps.process.tracing.establishment.dto.AbstractTracingEstablishmentDTO;

import java.util.List;

public interface TracingFinderService {

    List<AbstractTracingEstablishmentDTO> findAllRequestForTracingProcess();
}
