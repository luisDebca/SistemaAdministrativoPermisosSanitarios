package edu.ues.aps.process.tracing.base.model;

public enum InspectionResultType {

    FAVORABLE("FAVORABLE"),
    RECOMMENDATIONS("RECOMENDACIONES"),
    UNDELIVERED("SIN INFORME"),
    INVALID("INVALIDO");

    private String inspectionResultType;

    InspectionResultType(String type) {
        this.inspectionResultType = type;
    }

    public String getInspectionResultType() {
        return inspectionResultType;
    }
}
