package edu.ues.aps.process.tracing.vehicle.controller;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.tracing.base.controller.InspectionPersistenceController;
import edu.ues.aps.process.tracing.base.model.Inspection;
import edu.ues.aps.process.tracing.base.service.InspectionPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/tracing/bunch")
public class VehicleInspectionPersistenceController extends InspectionPersistenceController {

    private static final Logger logger = LogManager.getLogger(VehicleInspectionPersistenceController.class);

    private static final String INSPECTION = "vehicle_inspection_form";

    private final VehicleBunchInformationService informationService;

    public VehicleInspectionPersistenceController(InspectionPersistenceService persistenceService, VehicleBunchInformationService informationService) {
        super(persistenceService);
        this.informationService = informationService;
    }

    @Override
    @GetMapping("/{id_bunch}/inspection/new")
    public String showNewInspectionForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show new inspection for bunch {}.", id_bunch);
        model.addAttribute("id_bunch",id_bunch);
        model.addAttribute("inspection", new Inspection());
        return INSPECTION;
    }

    @Override
    @PostMapping("/{id_bunch}/inspection/new")
    public String saveInspection(@PathVariable Long id_bunch, @ModelAttribute Inspection inspection) {
        logger.info("POST:: Save new inspection for all request in bunch {}.", id_bunch);
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            save(id_caseFile, inspection);
        }
        return String.format("redirect:/tracing/bunch/%d/inspection/all", id_bunch);
    }

    @Override
    @GetMapping("/{id_bunch}/inspection/{inspection_number}/edit")
    public String showInspectionEditForm(@PathVariable Long id_bunch, @PathVariable Long inspection_number, ModelMap modelMap) {
        logger.info("GET:: Show inspection number {} edit for bunch {}.", inspection_number + 1, id_bunch);
        modelMap.addAttribute("id_bunch",id_bunch);
        List<Long> allCaseFileId = informationService.findAllCaseFileId(id_bunch);
        if (!allCaseFileId.isEmpty()) {
            modelMap.addAttribute("inspection", getInspection(allCaseFileId.get(0), Math.toIntExact(inspection_number)));
        }
        return INSPECTION;
    }

    @Override
    @PostMapping("/{id_bunch}/inspection/{inspection_number}/edit")
    public String updateInspection(@PathVariable Long id_bunch, @PathVariable Long inspection_number, @ModelAttribute Inspection inspection) {
        logger.info("POST:: Update inspection {} for all request in bunch {}.", inspection_number, id_bunch);
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            inspection.setId(getInspection(id_caseFile, Math.toIntExact(inspection_number)).getId());
            update(inspection);
        }
        return String.format("redirect:/tracing/bunch/%d/inspection/all", id_bunch);
    }

    @Override
    @GetMapping("/{id_bunch}/inspection/{inspection_number}/delete")
    public String deleteInspection(@PathVariable Long id_bunch, @PathVariable Long inspection_number) {
        logger.info("GET:: Delete inspection {} for all request in bunch {}", inspection_number, id_bunch);
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            delete(id_caseFile, getInspection(id_caseFile, Math.toIntExact(inspection_number)).getId());
        }
        return String.format("redirect:/tracing/bunch/%d/inspection/all", id_bunch);
    }

}
