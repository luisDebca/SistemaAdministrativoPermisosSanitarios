package edu.ues.aps.process.tracing.base.model;

import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.users.management.model.MailRegister;
import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "TRACING")
public class Tracing implements AbstractTracing {

    @Id
    private Long id;

    @Column(name = "created_on", nullable = false)
    private LocalDateTime created_on;

    @Column(name = "ucsf_received_on")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime ucsf_received_on;

    @Column(name = "shipping_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime shipping_date;

    @Column(name = "return_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime return_date;

    @Column(name = "review_date")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime review_date;

    @Column(name = "observation", length = 1500)
    private String observation;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    private Casefile casefile;

    @OneToMany(mappedBy = "tracing", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<Inspection> inspections = new ArrayList<>();

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MailRegister> mailRegisters = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public LocalDateTime getUcsf_received_on() {
        return ucsf_received_on;
    }

    @Override
    public void setUcsf_received_on(LocalDateTime ucsf_received_on) {
        this.ucsf_received_on = ucsf_received_on;
    }

    @Override
    public LocalDateTime getCreated_on() {
        return created_on;
    }

    @Override
    public void setCreated_on(LocalDateTime created_on) {
        this.created_on = created_on;
    }

    @Override
    public String getObservation() {
        return observation;
    }

    @Override
    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public List<Inspection> getInspections() {
        return inspections;
    }

    @Override
    public void setInspections(List<Inspection> inspections) {
        this.inspections = inspections;
    }

    @Override
    public Casefile getCasefile() {
        return casefile;
    }

    @Override
    public void setCasefile(Casefile casefile) {
        this.casefile = casefile;
    }

    @Override
    public void addInspection(Inspection inspection){
        inspections.add(inspection);
        inspection.setTracing(this);
    }

    @Override
    public void removeInspection(Inspection inspection){
        inspections.remove(inspection);
        inspection.setTracing(null);
    }

    @Override
    public List<MailRegister> getMailRegisters() {
        return mailRegisters;
    }

    @Override
    public void setMailRegisters(List<MailRegister> mailRegisters) {
        this.mailRegisters = mailRegisters;
    }

    @Override
    public void addMailRegister(MailRegister register){
        mailRegisters.add(register);
    }

    @Override
    public LocalDateTime getShipping_date() {
        return shipping_date;
    }

    @Override
    public void setShipping_date(LocalDateTime shipping_date) {
        this.shipping_date = shipping_date;
    }

    @Override
    public LocalDateTime getReturn_date() {
        return return_date;
    }

    @Override
    public void setReturn_date(LocalDateTime return_date) {
        this.return_date = return_date;
    }

    @Override
    public LocalDateTime getReview_date() {
        return review_date;
    }

    @Override
    public void setReview_date(LocalDateTime review_date) {
        this.review_date = review_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Tracing tracing = (Tracing) o;

        return id.equals(tracing.id) && (created_on != null ? created_on.equals(tracing.created_on) : tracing.created_on == null);
    }

    @Override
    public int hashCode() {
        int result = id.hashCode();
        result = 31 * result + (created_on != null ? created_on.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "Tracing{" +
                "id=" + id +
                ", created_on=" + created_on +
                ", ucsf_received_on=" + ucsf_received_on +
                ", shipping_date=" + shipping_date +
                ", return_date=" + return_date +
                ", review_date=" + review_date +
                '}';
    }
}
