package edu.ues.aps.process.tracing.base.service;

import edu.ues.aps.process.tracing.base.dto.AbstractInspectionDTO;

import java.util.List;

public interface InspectionDTOFinderService {

    AbstractInspectionDTO find(Long id_inspection);

    List<AbstractInspectionDTO> findAll(Long id_tracing);

    List<AbstractInspectionDTO> findBunchAll(Long id_bunch);

}
