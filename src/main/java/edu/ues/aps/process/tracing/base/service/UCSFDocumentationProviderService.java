package edu.ues.aps.process.tracing.base.service;

import edu.ues.aps.process.tracing.establishment.dto.AbstractUCSFProviderDocumentationReport;

public interface UCSFDocumentationProviderService {

    AbstractUCSFProviderDocumentationReport find(Long id);
}
