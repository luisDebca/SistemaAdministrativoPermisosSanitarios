package edu.ues.aps.process.tracing.base.dto;

import edu.ues.aps.process.tracing.base.model.InspectionResultType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class InspectionDTO implements AbstractInspectionDTO {

    private Long id_tracing;
    private Long id_inspection;
    private String inspection_memorandum_number;
    private String inspection_observation;
    private String inspection_technical_name;
    private String inspection_technical_charge;
    private InspectionResultType inspection_result_type;
    private Integer inspection_extension_received;
    private Integer inspection_score;
    private LocalDateTime inspection_inspected_on;
    private LocalDateTime inspection_sibasi_received;

    public InspectionDTO(Long id_inspection, InspectionResultType inspection_result_type, Integer inspection_extension_received, LocalDateTime inspection_inspected_on, LocalDateTime inspection_sibasi_received) {
        this.id_inspection = id_inspection;
        this.inspection_result_type = inspection_result_type;
        this.inspection_extension_received = inspection_extension_received;
        this.inspection_inspected_on = inspection_inspected_on;
        this.inspection_sibasi_received = inspection_sibasi_received;
    }

    public InspectionDTO(String inspection_memorandum_number, String inspection_observation, String inspection_technical_name, String inspection_technical_charge, InspectionResultType inspection_result_type, Integer inspection_extension_received, Integer inspection_score, LocalDateTime inspection_inspected_on, LocalDateTime inspection_sibasi_received) {
        this.inspection_memorandum_number = inspection_memorandum_number;
        this.inspection_observation = inspection_observation;
        this.inspection_technical_name = inspection_technical_name;
        this.inspection_technical_charge = inspection_technical_charge;
        this.inspection_result_type = inspection_result_type;
        this.inspection_extension_received = inspection_extension_received;
        this.inspection_score = inspection_score;
        this.inspection_inspected_on = inspection_inspected_on;
        this.inspection_sibasi_received = inspection_sibasi_received;
    }

    public Long getId_tracing() {
        return id_tracing;
    }

    public void setId_tracing(Long id_tracing) {
        this.id_tracing = id_tracing;
    }

    public Long getId_inspection() {
        return id_inspection;
    }

    public String getInspection_result_type() {
        if(inspection_result_type != null){
            return inspection_result_type.getInspectionResultType();
        }
        return InspectionResultType.UNDELIVERED.getInspectionResultType();
    }

    public Integer getInspection_extension_received() {
        return inspection_extension_received;
    }

    public String getInspection_inspected_on() {
        if(inspection_inspected_on != null){
            return DatetimeUtil.parseDateToLongDateFormat(inspection_inspected_on.toLocalDate());
        }
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    public String getInspection_sibasi_received() {
        if(inspection_sibasi_received != null){
            return DatetimeUtil.parseDateToLongDateFormat(inspection_sibasi_received.toLocalDate());
        }
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    public String getInspection_memorandum_number() {
        return inspection_memorandum_number;
    }

    public String getInspection_observation() {
        return inspection_observation;
    }

    public String getInspection_technical_name() {
        return inspection_technical_name;
    }

    public String getInspection_technical_charge() {
        return inspection_technical_charge;
    }

    public Integer getInspection_score() {
        return inspection_score;
    }
}
