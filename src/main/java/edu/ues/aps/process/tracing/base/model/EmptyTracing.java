package edu.ues.aps.process.tracing.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.EmptyCasefile;
import edu.ues.aps.users.management.model.MailRegister;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class EmptyTracing implements AbstractTracing {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public LocalDateTime getUcsf_received_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public LocalDateTime getCreated_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public String getObservation() {
        return "Tracing observation not found";
    }

    @Override
    public List<Inspection> getInspections() {
        return Collections.emptyList();
    }

    @Override
    public AbstractCasefile getCasefile() {
        return new EmptyCasefile();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setUcsf_received_on(LocalDateTime ucsf_received_on) {

    }

    @Override
    public void setCreated_on(LocalDateTime created_on) {

    }

    @Override
    public void setObservation(String observation) {

    }

    @Override
    public void setInspections(List<Inspection> inspections) {

    }

    @Override
    public void setCasefile(Casefile casefile) {

    }

    @Override
    public void addInspection(Inspection inspection) {

    }

    @Override
    public void removeInspection(Inspection inspection) {

    }

    @Override
    public List<MailRegister> getMailRegisters() {
        return Collections.emptyList();
    }

    @Override
    public void setMailRegisters(List<MailRegister> mailRegisters) {

    }

    @Override
    public void addMailRegister(MailRegister register) {

    }

    @Override
    public LocalDateTime getShipping_date() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public void setShipping_date(LocalDateTime shipping_date) {

    }

    @Override
    public LocalDateTime getReturn_date() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public void setReturn_date(LocalDateTime return_date) {

    }

    @Override
    public LocalDateTime getReview_date() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public void setReview_date(LocalDateTime review_date) {

    }

    @Override
    public String toString() {
        return "EmptyTracing{}";
    }
}
