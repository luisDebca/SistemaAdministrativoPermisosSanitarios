package edu.ues.aps.process.tracing.vehicle.repository;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.tracing.vehicle.dto.AbstractVehicleTracingProcessDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleTracingProcessRepository implements VehicleTracingProcessFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleTracingProcessRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleTracingProcessDTO> findAllRequest() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.tracing.vehicle.dto.VehicleTracingProcessDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date,ucsf.name,sibasi.zone," +
                "case when tracing is null then false else true end, tracing.shipping_date,tracing.ucsf_received_on,tracing.return_date," +
                "(select count(inspection) from Inspection inspection where inspection.tracing.id = cv.id))" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "left join cv.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "left join cv.tracing tracing " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehicleTracingProcessDTO.class)
                .setParameter("id", ProcessIdentifier.REQUEST_TRACING)
                .getResultList();
    }
}
