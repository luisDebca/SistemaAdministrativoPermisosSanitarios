package edu.ues.aps.process.tracing.base.repository;

import edu.ues.aps.process.tracing.base.dto.AbstractInspectionDTO;
import edu.ues.aps.process.tracing.base.model.AbstractInspection;

import javax.persistence.NoResultException;
import java.util.List;

public interface TracingInspectionFinderRepository {

    AbstractInspection findInspectionById(Long id_inspection) throws NoResultException;

    List<AbstractInspectionDTO> findAllInspectionsFromTracing(Long id_tracing);

    AbstractInspectionDTO findById(Long id_inspection) throws NoResultException;
}
