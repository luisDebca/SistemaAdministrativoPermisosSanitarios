package edu.ues.aps.process.tracing.base.repository;

import edu.ues.aps.process.tracing.base.model.AbstractTracing;

public interface TracingFinderRepository {

    AbstractTracing find(Long id_caseFile);
}
