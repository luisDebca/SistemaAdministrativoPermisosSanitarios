package edu.ues.aps.process.tracing.base.model;

import org.springframework.format.annotation.DateTimeFormat;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Table(name = "INSPECTION")
public class Inspection implements AbstractInspection {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "memorandum_number", length = 25)
    private String memorandum_number;

    @Column(name = "technical_name", length = 100)
    private String technical_name;

    @Column(name = "technical_charge", length = 100)
    private String technical_charge;

    @Column(name = "observation", length = 1000)
    private String observation;

    @Column(name = "score")
    private Double score;

    @Enumerated(EnumType.STRING)
    @Column(name = "inspection_result_type", nullable = false, length = 25)
    private InspectionResultType result_type;

    @Column(name = "extension_received")
    private Integer extension_received;

    @Column(name = "inspected_on")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime inspected_on;

    @Column(name = "sibasi_received")
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm")
    private LocalDateTime sibasi_received;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_tracing")
    private Tracing tracing;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getMemorandum_number() {
        return memorandum_number;
    }

    @Override
    public void setMemorandum_number(String memorandum_number) {
        this.memorandum_number = memorandum_number;
    }

    @Override
    public String getTechnical_name() {
        return technical_name;
    }

    @Override
    public void setTechnical_name(String technical_name) {
        this.technical_name = technical_name;
    }

    @Override
    public String getTechnical_charge() {
        return technical_charge;
    }

    @Override
    public void setTechnical_charge(String technical_charge) {
        this.technical_charge = technical_charge;
    }

    @Override
    public Double getScore() {
        return score;
    }

    @Override
    public void setScore(Double score) {
        this.score = score;
    }

    @Override
    public String getObservation() {
        return observation;
    }

    @Override
    public void setObservation(String observation) {
        this.observation = observation;
    }

    @Override
    public InspectionResultType getResult_type() {
        return result_type;
    }

    @Override
    public void setResult_type(InspectionResultType result_type) {
        this.result_type = result_type;
    }

    @Override
    public Integer getExtension_received() {
        return extension_received;
    }

    @Override
    public void setExtension_received(Integer extension_received) {
        this.extension_received = extension_received;
    }

    @Override
    public LocalDateTime getInspected_on() {
        return inspected_on;
    }

    @Override
    public void setInspected_on(LocalDateTime inspected_on) {
        this.inspected_on = inspected_on;
    }

    @Override
    public LocalDateTime getSibasi_received() {
        return sibasi_received;
    }

    @Override
    public void setSibasi_received(LocalDateTime sibasi_received) {
        this.sibasi_received = sibasi_received;
    }

    @Override
    public Tracing getTracing() {
        return tracing;
    }

    @Override
    public void setTracing(Tracing tracing) {
        this.tracing = tracing;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Inspection that = (Inspection) o;

        return (memorandum_number != null ? memorandum_number.equals(that.memorandum_number) : that.memorandum_number == null) && (technical_name != null ? technical_name.equals(that.technical_name) : that.technical_name == null) && (technical_charge != null ? technical_charge.equals(that.technical_charge) : that.technical_charge == null) && result_type == that.result_type;
    }

    @Override
    public int hashCode() {
        int result = memorandum_number != null ? memorandum_number.hashCode() : 0;
        result = 31 * result + (technical_name != null ? technical_name.hashCode() : 0);
        result = 31 * result + (technical_charge != null ? technical_charge.hashCode() : 0);
        result = 31 * result + result_type.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Inspection{" +
                "id=" + id +
                ", memorandum_number='" + memorandum_number + '\'' +
                ", technical_name='" + technical_name + '\'' +
                ", technical_charge='" + technical_charge + '\'' +
                ", observation='" + observation + '\'' +
                ", result_type=" + result_type +
                ", extension_received='" + extension_received + '\'' +
                ", inspected_on=" + inspected_on +
                ", sibasi_received=" + sibasi_received +
                '}';
    }
}
