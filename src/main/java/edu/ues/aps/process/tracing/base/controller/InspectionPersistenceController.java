package edu.ues.aps.process.tracing.base.controller;

import edu.ues.aps.process.tracing.base.model.AbstractInspection;
import edu.ues.aps.process.tracing.base.model.Inspection;
import edu.ues.aps.process.tracing.base.service.InspectionPersistenceService;
import org.springframework.ui.ModelMap;

public abstract class InspectionPersistenceController {

    private final InspectionPersistenceService persistenceService;

    public InspectionPersistenceController(InspectionPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    public abstract String showNewInspectionForm(Long id, ModelMap model);

    public abstract String saveInspection(Long id, Inspection inspection);

    public abstract String showInspectionEditForm(Long id, Long id_inspection, ModelMap modelMap);

    public abstract String updateInspection(Long id, Long id_inspection, Inspection inspection);

    public abstract String deleteInspection(Long id, Long id_inspection);

    public AbstractInspection getInspection(Long id_inspection) {
        return persistenceService.find(id_inspection);
    }

    public AbstractInspection getInspection(Long id_tracing, Integer inspection_number) {
        return persistenceService.find(id_tracing, inspection_number);
    }

    public void save(Long id_tracing, AbstractInspection inspection) {
        persistenceService.saveInspection(inspection, id_tracing);
    }

    public void update(AbstractInspection inspection) {
        persistenceService.updateInspection(inspection);
    }

    public void delete(Long id_tracing, Long id_inspection) {
        persistenceService.deleteInspection(id_tracing, id_inspection);
    }

}
