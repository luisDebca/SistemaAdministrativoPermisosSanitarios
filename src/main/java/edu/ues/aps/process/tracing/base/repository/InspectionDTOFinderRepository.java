package edu.ues.aps.process.tracing.base.repository;

import edu.ues.aps.process.tracing.base.dto.AbstractInspectionDTO;
import edu.ues.aps.process.tracing.base.model.AbstractInspection;

import javax.persistence.NoResultException;
import java.util.List;

public interface InspectionDTOFinderRepository {

    List<AbstractInspectionDTO> findAll(Long id_tracing);

    List<AbstractInspectionDTO> findBunchAll(Long id_bunch);

    AbstractInspectionDTO findById(Long id_inspection) throws NoResultException;
}
