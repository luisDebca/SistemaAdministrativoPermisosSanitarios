package edu.ues.aps.process.tracing.base.service;

import edu.ues.aps.process.tracing.base.model.AbstractInspection;

public interface InspectionPersistenceService {

    AbstractInspection find(Long id_inspection);

    AbstractInspection find(Long id_tracing, Integer inspection_number);

    void saveInspection(AbstractInspection inspection, Long id_tracing);

    void updateInspection(AbstractInspection inspection);

    void deleteInspection(Long id_tracing, Long id_inspection);

}
