package edu.ues.aps.process.tracing.base.repository;

import edu.ues.aps.process.tracing.establishment.dto.AbstractUCSFProviderDocumentationReport;

import javax.persistence.NoResultException;

public interface UCSFDocumentationProviderRepository {

    AbstractUCSFProviderDocumentationReport find(Long id) throws NoResultException;
}
