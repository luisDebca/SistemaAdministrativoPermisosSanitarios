package edu.ues.aps.process.tracing.base.async;

import edu.ues.aps.process.tracing.base.dto.AbstractInspectionDTO;
import edu.ues.aps.process.tracing.base.service.InspectionDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class InspectionInformationProviderAsyncController {

    private static final Logger logger = LogManager.getLogger(InspectionInformationProviderAsyncController.class);


    private InspectionDTOFinderService finderService;

    public InspectionInformationProviderAsyncController(InspectionDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @RequestMapping(value = "/get-inspection-information", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AbstractInspectionDTO findInspectionInformation(@RequestParam("id_inspection") Long id_inspection) {
        logger.info("GET:: Get inspection {} information", id_inspection);
        return finderService.find(id_inspection);
    }

}
