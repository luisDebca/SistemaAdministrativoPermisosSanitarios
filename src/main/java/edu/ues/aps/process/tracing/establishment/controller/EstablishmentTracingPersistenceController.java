package edu.ues.aps.process.tracing.establishment.controller;

import edu.ues.aps.process.tracing.base.controller.TracingPersistenceController;
import edu.ues.aps.process.tracing.base.model.Tracing;
import edu.ues.aps.process.tracing.base.service.TracingPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/tracing")
public class EstablishmentTracingPersistenceController extends TracingPersistenceController {

    private static final Logger logger = LogManager.getLogger(TracingPersistenceController.class);

    private static final String TRACING_FORM = "tracing_form";

    public EstablishmentTracingPersistenceController(TracingPersistenceService persistenceService) {
        super(persistenceService);
    }

    @Override
    @GetMapping("/{id_caseFile}/new")
    public String showNewTracingForm(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show new request {} tracing form", id_caseFile);
        model.addAttribute("tracing", new Tracing());
        return TRACING_FORM;
    }

    @Override
    @PostMapping("/{id_caseFile}/new")
    public String saveTracing(@PathVariable Long id_caseFile, @ModelAttribute Tracing tracing) {
        logger.info("POST:: Save case file {} tracing", id_caseFile);
        save(tracing, id_caseFile);
        return "redirect:/tracing/establishment/all";
    }

    @Override
    @GetMapping("/{id_caseFile}/edit")
    public String showTracingEditForm(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show tracing {} edit form", id_caseFile);
        model.addAttribute("tracing", getTracing(id_caseFile));
        return TRACING_FORM;
    }

    @Override
    @PostMapping("/{id_caseFile}/edit")
    public String updateTracing(@PathVariable Long id_caseFile, @ModelAttribute Tracing tracing) {
        logger.info("POST:: Update tracing {}.", id_caseFile);
        update(tracing);
        return "redirect:/tracing/establishment/all";
    }

}
