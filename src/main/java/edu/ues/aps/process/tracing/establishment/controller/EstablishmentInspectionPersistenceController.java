package edu.ues.aps.process.tracing.establishment.controller;

import edu.ues.aps.process.tracing.base.controller.InspectionPersistenceController;
import edu.ues.aps.process.tracing.base.model.Inspection;
import edu.ues.aps.process.tracing.base.service.InspectionPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.time.LocalDate;

@Controller
@RequestMapping("/tracing")
public class EstablishmentInspectionPersistenceController extends InspectionPersistenceController {

    private static final Logger logger = LogManager.getLogger(EstablishmentInspectionPersistenceController.class);

    private static final String INSPECTION = "inspection_form";
    private static final String MEMO_FORMAT = "-3000UCSF-STO";

    public EstablishmentInspectionPersistenceController(InspectionPersistenceService persistenceService) {
        super(persistenceService);
    }

    @GetMapping("/{id_tracing}/inspection/new")
    public String showNewInspectionForm(@PathVariable Long id_tracing, ModelMap model) {
        logger.info("GET:: Show new inspection form tracing {}", id_tracing);
        Inspection newInspection = new Inspection();
        newInspection.setMemorandum_number(LocalDate.now().getYear() + MEMO_FORMAT);
        model.addAttribute("id_tracing", id_tracing);
        model.addAttribute("inspection", newInspection);
        return INSPECTION;
    }

    @PostMapping("/{id_tracing}/inspection/new")
    public String saveInspection(@PathVariable Long id_tracing, @ModelAttribute Inspection inspection) {
        logger.info("POST:: Save new inspection for tracing {}", id_tracing);
        save(id_tracing, inspection);
        return String.format("redirect:/tracing/%d/inspection/all", id_tracing);
    }

    @GetMapping("/{id_tracing}/inspection/{id_inspection}/edit")
    public String showInspectionEditForm(@PathVariable("id_tracing") Long id_tracing, @PathVariable("id_inspection") Long id_inspection, ModelMap modelMap) {
        logger.info("GET:: Show tracing {} inspection {} edit form", id_tracing, id_inspection);
        modelMap.addAttribute("id_tracing", id_tracing);
        modelMap.addAttribute("inspection", getInspection(id_inspection));
        return INSPECTION;
    }

    @PostMapping("/{id_tracing}/inspection/{id_inspection}/edit")
    public String updateInspection(@PathVariable("id_tracing") Long id_tracing, @PathVariable("id_inspection") Long id_inspection, @ModelAttribute Inspection inspection) {
        logger.info("POST:: Update inspection {} from tracing {}", id_inspection, id_tracing);
        update(inspection);
        return String.format("redirect:/tracing/%d/inspection/all", id_tracing);
    }

    @GetMapping("/${id_tracing}/inspection/{id_inspection}/delete")
    public String deleteInspection(@PathVariable("id_tracing") Long id_tracing, @PathVariable("id_inspection") Long id_inspection) {
        logger.info("GET:: Delete inspection {} from tracing {}", id_inspection, id_tracing);
        delete(id_tracing, id_inspection);
        return String.format("redirect:/tracing/%d/inspection/all", id_tracing);
    }

}
