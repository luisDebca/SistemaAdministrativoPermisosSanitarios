package edu.ues.aps.process.tracing.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.VehicleRequestDTO;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class VehicleTracingProcessDTO extends VehicleRequestDTO implements AbstractVehicleTracingProcessDTO {

    private String ucsf;
    private String sibasi;
    private Boolean is_tracing;
    private LocalDateTime delivery_date;
    private LocalDateTime received_date;
    private LocalDateTime return_date;
    private Long inspection_count;

    public VehicleTracingProcessDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on, String ucsf, String sibasi, Boolean is_tracing, LocalDateTime delivery_date, LocalDateTime received_date, LocalDateTime return_date, Long inspection_count) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
        this.ucsf = ucsf;
        this.sibasi = sibasi;
        this.is_tracing = is_tracing;
        this.delivery_date = delivery_date;
        this.received_date = received_date;
        this.return_date = return_date;
        this.inspection_count = inspection_count;
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getSibasi() {
        return sibasi;
    }

    public Boolean getIs_tracing() {
        return is_tracing;
    }

    public String getDelivery_date() {
        if (delivery_date == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(delivery_date.toLocalDate());
    }

    public String getReceived_date() {
        if (received_date == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(received_date.toLocalDate());
    }

    public String getReturn_date() {
        if (return_date == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(return_date.toLocalDate());
    }

    public Long getInspection_count() {
        return inspection_count;
    }
}
