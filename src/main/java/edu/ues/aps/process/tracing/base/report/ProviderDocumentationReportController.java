package edu.ues.aps.process.tracing.base.report;

import edu.ues.aps.process.tracing.base.service.UCSFDocumentationProviderService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;
import java.util.Collections;

@Controller
public class ProviderDocumentationReportController extends UCSFProviderDocumentationReportController {

    private static final Logger logger = LogManager.getLogger(ProviderDocumentationReportController.class);

    private static final String LARGE_REPORT = "TracingProcessLargeReport";
    private static final String NORMAL_REPORT = "TracingProcessReport";

    private final UCSFDocumentationProviderService vehicleFinderService;
    private UCSFDocumentationProviderService establishmentFinderService;

    public ProviderDocumentationReportController(SinglePersonalInformationService personalInformationService,
                                                 @Qualifier("establishmentUCSFDocumentationProviderService") UCSFDocumentationProviderService establishmentFinderService,
                                                 @Qualifier("vehicleUCSFDocumentationProviderService") UCSFDocumentationProviderService vehicleFinderService) {
        super(personalInformationService);
        this.establishmentFinderService = establishmentFinderService;
        this.vehicleFinderService = vehicleFinderService;
    }

    @GetMapping("/tracing/establishment/request/{id_caseFile}/ucsf-provider-documentation-report.pdf")
    public String generateEstablishmentReport(@PathVariable Long id_caseFile, String name, ModelMap model, Principal principal) {
        logger.info("REPORT:: Generate UCSF provider documentation report for case file {}.", id_caseFile);
        setDatasource(establishmentFinderService.find(id_caseFile));
        buildReportContent(name, principal.getName());
        model.addAllAttributes(getModel());
        return getReportName();
    }

    @GetMapping("/tracing/vehicle/bunch/{id_bunch}/ucsf-provider-documentation-report.pdf")
    public String generateVehicleReport(@PathVariable Long id_bunch, String name, ModelMap model, Principal principal) {
        logger.info("REPORT:: Generate UCSF provider documentation report for bunch {}.", id_bunch);
        setDatasource(vehicleFinderService.find(id_bunch));
        buildReportContent(name, principal.getName());
        model.addAllAttributes(getModel());
        return getReportName();
    }

    private void buildReportContent(String for_name, String user_name) {
        setForName(for_name);
        setFromName(user_name);
        addReportRequiredSources(Collections.singletonList(getDatasource()));
    }

    private String getReportName() {
        if (getDatasource().getObservation() != null) {
            if (getDatasource().getObservation().length() == 0) {
                return LARGE_REPORT;
            }
        }
        return NORMAL_REPORT;
    }
}
