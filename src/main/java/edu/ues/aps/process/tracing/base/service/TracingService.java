package edu.ues.aps.process.tracing.base.service;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.service.SingleCaseFileFinderService;
import edu.ues.aps.process.tracing.base.model.AbstractTracing;
import edu.ues.aps.process.tracing.base.model.Tracing;
import edu.ues.aps.process.tracing.base.repository.TracingFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class TracingService implements TracingPersistenceService {

    private AbstractTracing tracing;

    private final SingleCaseFileFinderService caseFileFinderRepository;
    private final TracingFinderRepository finderRepository;

    public TracingService(TracingFinderRepository finderRepository, SingleCaseFileFinderService caseFileFinderRepository) {
        this.finderRepository = finderRepository;
        this.caseFileFinderRepository = caseFileFinderRepository;
    }

    @Override
    public AbstractTracing find(Long id_caseFile) {
        return finderRepository.find(id_caseFile);
    }

    @Override
    public void saveTracing(AbstractTracing draft, Long id_caseFile) {
        tracing = new Tracing();
        tracing.setCreated_on(LocalDateTime.now());
        buildTracing(draft);
        AbstractCasefile caseFile = caseFileFinderRepository.find(id_caseFile);
        caseFile.addTracing((Tracing) tracing);
    }

    @Override
    public void updateTracing(AbstractTracing draft) {
        tracing = finderRepository.find(draft.getId());
        buildTracing(draft);
    }

    private void buildTracing(AbstractTracing draft) {
        tracing.setUcsf_received_on(draft.getUcsf_received_on());
        tracing.setShipping_date(draft.getShipping_date());
        tracing.setReturn_date(draft.getReturn_date());
        tracing.setReview_date(draft.getReview_date());
        if(draft.getObservation().length() < 1500)
            tracing.setObservation(draft.getObservation());
    }

}
