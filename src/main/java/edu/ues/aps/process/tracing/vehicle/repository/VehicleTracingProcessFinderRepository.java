package edu.ues.aps.process.tracing.vehicle.repository;

import edu.ues.aps.process.tracing.vehicle.dto.AbstractVehicleTracingProcessDTO;

import java.util.List;

public interface VehicleTracingProcessFinderRepository {

    List<AbstractVehicleTracingProcessDTO> findAllRequest();
}
