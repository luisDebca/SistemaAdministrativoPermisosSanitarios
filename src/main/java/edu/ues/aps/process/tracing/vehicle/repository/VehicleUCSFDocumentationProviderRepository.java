package edu.ues.aps.process.tracing.vehicle.repository;

import edu.ues.aps.process.tracing.base.model.InspectionResultType;
import edu.ues.aps.process.tracing.base.repository.UCSFDocumentationProviderRepository;
import edu.ues.aps.process.tracing.establishment.dto.AbstractUCSFProviderDocumentationReport;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class VehicleUCSFDocumentationProviderRepository implements UCSFDocumentationProviderRepository {

    private final SessionFactory sessionFactory;

    public VehicleUCSFDocumentationProviderRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractUCSFProviderDocumentationReport find(Long id_bunch) throws NoResultException {
        AbstractUCSFProviderDocumentationReport report = sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.tracing.establishment.dto.UCSFProviderDocumentationDTOReport(" +
                "tracing.return_date,tracing.ucsf_received_on,tracing.review_date,bunch.distribution_company," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "ucsf.name,sibasi.zone,tracing.observation,owner.name,false) " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "inner join cv.ucsf ucsf " +
                "inner join bunch.address address " +
                "inner join ucsf.sibasi sibasi " +
                "inner join cv.tracing tracing " +
                "where bunch.id = :id " +
                "group by bunch.id ", AbstractUCSFProviderDocumentationReport.class)
                .setParameter("id", id_bunch)
                .getSingleResult();
        report.setInspection_result_list(getInspectionResultTypes(id_bunch));
        return report;
    }

    private List<InspectionResultType> getInspectionResultTypes(Long id_bunch) {
        Long id_caseFile = sessionFactory.getCurrentSession().createQuery("select cv.id " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "where bunch.id = :id " +
                "group by bunch.id", Long.class)
                .setParameter("id", id_bunch)
                .getSingleResult();
        return sessionFactory.getCurrentSession().createQuery("select inspections.result_type " +
                "from Tracing tracing " +
                "inner join tracing.inspections inspections " +
                "where tracing.id = :id", InspectionResultType.class)
                .setParameter("id", id_caseFile)
                .getResultList();
    }
}
