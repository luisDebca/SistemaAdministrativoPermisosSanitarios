package edu.ues.aps.process.tracing.vehicle.controller;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.tracing.base.controller.TracingPersistenceController;
import edu.ues.aps.process.tracing.base.model.Tracing;
import edu.ues.aps.process.tracing.base.service.TracingPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/tracing/bunch")
public class VehicleTracingPersistenceController extends TracingPersistenceController {

    private static final Logger logger = LogManager.getLogger(VehicleTracingPersistenceController.class);

    private static final String TRACING = "vehicle_tracing_form";

    private final VehicleBunchInformationService informationService;

    public VehicleTracingPersistenceController(TracingPersistenceService persistenceService, VehicleBunchInformationService informationService) {
        super(persistenceService);
        this.informationService = informationService;
    }

    @Override
    @GetMapping("/{id_bunch}/new")
    public String showNewTracingForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show new tracing for bunch {}.", id_bunch);
        model.addAttribute("tracing", new Tracing());
        return TRACING;
    }

    @Override
    @PostMapping("/{id_bunch}/new")
    public String saveTracing(@PathVariable Long id_bunch, @ModelAttribute Tracing tracing) {
        logger.info("POST:: Save new tracing for all request in bunch {}.", id_bunch);
        saveAll(id_bunch, tracing);
        return "redirect:/tracing/vehicle/all";
    }

    @Override
    @GetMapping("/{id_bunch}/edit")
    public String showTracingEditForm(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show tracing edit for bunch {}.", id_bunch);
        List<Long> allId = informationService.findAllCaseFileId(id_bunch);
        if (!allId.isEmpty()) {
            model.addAttribute("tracing", getTracing(allId.get(0)));
        }
        return TRACING;
    }

    @Override
    @PostMapping("/{id_bunch}/edit")
    public String updateTracing(@PathVariable Long id_bunch, @ModelAttribute Tracing tracing) {
        logger.info("POST:: Update all tracing request in bunch {}.", id_bunch);
        updateAll(id_bunch, tracing);
        return "redirect:/tracing/vehicle/all";
    }

    private void saveAll(Long id_bunch, Tracing tracing) {
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            save(tracing, id_caseFile);
        }
    }

    private void updateAll(Long id_bunch, Tracing tracing) {
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            tracing.setId(id_caseFile);
            update(tracing);
        }
    }
}
