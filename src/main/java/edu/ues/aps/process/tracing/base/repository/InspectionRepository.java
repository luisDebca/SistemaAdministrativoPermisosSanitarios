package edu.ues.aps.process.tracing.base.repository;

import edu.ues.aps.process.tracing.base.model.AbstractInspection;
import edu.ues.aps.process.tracing.base.model.Inspection;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class InspectionRepository implements InspectionPersistenceRepository{

    private final SessionFactory sessionFactory;

    public InspectionRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractInspection find(Long id_inspection) {
        return sessionFactory.getCurrentSession().get(Inspection.class, id_inspection);
    }

    @Override
    public List<AbstractInspection> findAll(Long id_tracing) {
        return sessionFactory.getCurrentSession().createQuery("select inspection " +
                "from Inspection inspection " +
                "where inspection.tracing.id = :id", AbstractInspection.class)
                .setParameter("id", id_tracing)
                .getResultList();
    }

}
