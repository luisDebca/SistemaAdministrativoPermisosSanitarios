package edu.ues.aps.process.tracing.base.dto;

public interface TracingEntityDTO {

    Long getId_casefile();

    Long getId_entity();

    Long getId_owner();

    String getCasefile_code();

    String getCasefile_certification_type();

    String getUcsf();

    String getSibasi();

    String getEntity_name();

    String getEntity_address();

    String getEntity_type();

    String getOwner_name();

    String getRequest_entry_date();

    String getUcsf_received_date();

    String getObservations();

    Boolean isTracing_created();

}
