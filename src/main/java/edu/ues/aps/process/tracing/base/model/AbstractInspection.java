package edu.ues.aps.process.tracing.base.model;

import java.time.LocalDateTime;

public interface AbstractInspection {

    Long getId();

    void setId(Long id);

    String getMemorandum_number();

    void setMemorandum_number(String memorandum_number);

    String getTechnical_name();

    void setTechnical_name(String technical_name);

    String getTechnical_charge();

    void setTechnical_charge(String technical_charge);

    String getObservation();

    void setObservation(String observation);

    InspectionResultType getResult_type();

    void setResult_type(InspectionResultType result_type);

    Integer getExtension_received();

    void setExtension_received(Integer extension_received);

    LocalDateTime getInspected_on();

    void setInspected_on(LocalDateTime inspected_on);

    LocalDateTime getSibasi_received();

    void setSibasi_received(LocalDateTime sibasi_received);

    AbstractTracing getTracing();

    void setTracing(Tracing tracing);

    Double getScore();

    void setScore(Double score);

}
