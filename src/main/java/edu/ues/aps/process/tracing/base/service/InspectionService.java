package edu.ues.aps.process.tracing.base.service;

import edu.ues.aps.process.tracing.base.model.AbstractInspection;
import edu.ues.aps.process.tracing.base.model.AbstractTracing;
import edu.ues.aps.process.tracing.base.model.EmptyInspection;
import edu.ues.aps.process.tracing.base.model.Inspection;
import edu.ues.aps.process.tracing.base.repository.InspectionPersistenceRepository;
import edu.ues.aps.process.tracing.base.repository.TracingFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class InspectionService implements InspectionPersistenceService{

    private AbstractInspection inspection;

    private final InspectionPersistenceRepository inspectionRepository;
    private final TracingFinderRepository tracingFinderRepository;

    public InspectionService(InspectionPersistenceRepository inspectionRepository, TracingFinderRepository tracingFinderRepository) {
        this.inspectionRepository = inspectionRepository;
        this.tracingFinderRepository = tracingFinderRepository;
    }

    @Override
    public AbstractInspection find(Long id_inspection) {
        return inspectionRepository.find(id_inspection);
    }

    @Override
    public AbstractInspection find(Long id_tracing, Integer inspection_number) {
        List<AbstractInspection> all = inspectionRepository.findAll(id_tracing);
        if (all.size() > inspection_number) {
            return all.get(inspection_number);
        } else {
            return new EmptyInspection();
        }
    }

    @Override
    public void saveInspection(AbstractInspection template, Long id_tracing) {
        AbstractTracing tracing = tracingFinderRepository.find(id_tracing);
        inspection = new Inspection();
        buildInspection(template);
        tracing.addInspection((Inspection) inspection);
    }

    @Override
    public void updateInspection(AbstractInspection template) {
        inspection = inspectionRepository.find(template.getId());
        buildInspection(template);
    }

    @Override
    public void deleteInspection(Long id_tracing, Long id_inspection) {
        tracingFinderRepository.find(id_tracing).removeInspection((Inspection) inspectionRepository.find(id_inspection));
    }

    private void buildInspection(AbstractInspection template) {
        inspection.setScore(template.getScore());
        inspection.setResult_type(template.getResult_type());
        inspection.setInspected_on(template.getInspected_on());
        inspection.setTechnical_name(template.getTechnical_name());
        inspection.setSibasi_received(template.getSibasi_received());
        inspection.setTechnical_charge(template.getTechnical_charge());
        inspection.setMemorandum_number(template.getMemorandum_number());
        inspection.setExtension_received(template.getExtension_received());
        if(template.getObservation().length() < 1000)
            inspection.setObservation(template.getObservation());
    }
}
