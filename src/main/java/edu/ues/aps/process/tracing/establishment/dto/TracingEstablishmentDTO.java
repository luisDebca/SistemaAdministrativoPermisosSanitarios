package edu.ues.aps.process.tracing.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.EstablishmentRequestDTO;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class TracingEstablishmentDTO extends EstablishmentRequestDTO implements AbstractTracingEstablishmentDTO {

    private Boolean tracing;
    private LocalDateTime send_date;
    private LocalDateTime received_date;
    private LocalDateTime return_date;
    private Long inspection_count;

    public TracingEstablishmentDTO(Long id_caseFile, Long id_owner, Long id_establishment, String folder_number, String request_number, String establishment_name, String establishment_type_detail, String establishment_type, String establishment_section, String establishment_address, String establishment_nit, String UCSF, String SIBASI, CertificationType certification_type, LocalDateTime caseFile_createOn, String owner_name, Boolean tracing, LocalDateTime send_date, LocalDateTime received_date, LocalDateTime return_date, Long inspection_count) {
        super(id_caseFile, id_owner, id_establishment, folder_number, request_number, establishment_name, establishment_type_detail, establishment_type, establishment_section, establishment_address, establishment_nit, UCSF, SIBASI, certification_type, caseFile_createOn, owner_name);
        this.tracing = tracing;
        this.send_date = send_date;
        this.received_date = received_date;
        this.return_date = return_date;
        this.inspection_count = inspection_count;
    }

    public Boolean getTracing() {
        return tracing;
    }

    public String getSend_date() {
        if(send_date == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(send_date.toLocalDate());
    }

    public String getReceived_date() {
        if(received_date == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(received_date.toLocalDate());
    }

    public String getReturn_date() {
        if(return_date == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(return_date.toLocalDate());
    }

    public Long getInspection_count() {
        return inspection_count;
    }
}
