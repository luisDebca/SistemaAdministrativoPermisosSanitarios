package edu.ues.aps.process.tracing.base.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.tracing.establishment.dto.AbstractUCSFProviderDocumentationReport;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;

public abstract class UCSFProviderDocumentationReportController extends AbstractReportController {

    private final SinglePersonalInformationService personalInformationService;
    private AbstractUCSFProviderDocumentationReport datasource;

    public UCSFProviderDocumentationReportController(SinglePersonalInformationService personalInformationService) {
        this.personalInformationService = personalInformationService;
    }

    public AbstractUCSFProviderDocumentationReport getDatasource() {
        return datasource;
    }

    public void setDatasource(AbstractUCSFProviderDocumentationReport datasource) {
        this.datasource = datasource;
    }

    public void setForName(String name) {
        datasource.setFor_name(name);
    }

    public void setFromName(String name) {
        datasource.setFrom_name(personalInformationService.getFullName(name));
    }
}
