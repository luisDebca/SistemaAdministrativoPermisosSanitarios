package edu.ues.aps.process.dispatch.vehicle.controller;

import edu.ues.aps.process.dispatch.vehicle.service.VehicleRequestDispatchFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dispatch/vehicle")
public class VehicleRequestDispatchInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleRequestDispatchInformationProviderController.class);

    private static final String DISPATCH_LIST = "vehicle_request_entry_list";

    private final VehicleRequestDispatchFinderService finderService;

    public VehicleRequestDispatchInformationProviderController(VehicleRequestDispatchFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showAllRequestForDispatchProcess(ModelMap model) {
        logger.info("GET:: Show all request for dispatch process.");
        model.addAttribute("requests", finderService.findAllRequest());
        return DISPATCH_LIST;
    }
}
