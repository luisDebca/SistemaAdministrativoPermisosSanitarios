package edu.ues.aps.process.dispatch.establishment.service;

import edu.ues.aps.process.dispatch.establishment.dto.AbstractRequestEntryReportDTO;

public interface RequestEntryReportFinderService {

    AbstractRequestEntryReportDTO findRequestInformationForReport(Long id_casefile);
}
