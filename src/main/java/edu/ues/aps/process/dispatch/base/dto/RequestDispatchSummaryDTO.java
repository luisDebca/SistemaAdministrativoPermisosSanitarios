package edu.ues.aps.process.dispatch.base.dto;

public class RequestDispatchSummaryDTO implements RequestDispatchSummary{

    private String sibasi;
    private String ucsf;
    private Long request_delivery_count;
    private Long request_entry_count;
    private Long request_in_process_count;

    public RequestDispatchSummaryDTO(String sibasi, String ucsf, Long request_delivery_count, Long request_entry_count, Long request_in_process_count) {
        this.sibasi = sibasi;
        this.ucsf = ucsf;
        this.request_delivery_count = request_delivery_count;
        this.request_entry_count = request_entry_count;
        this.request_in_process_count = request_in_process_count;
    }

    public String getSibasi() {
        return sibasi;
    }

    public String getUcsf() {
        return ucsf;
    }

    public Long getRequest_delivery_count() {
        return request_delivery_count;
    }

    public Long getRequest_entry_count() {
        return request_entry_count;
    }

    public Long getRequest_in_process_count() {
        return request_in_process_count;
    }
}
