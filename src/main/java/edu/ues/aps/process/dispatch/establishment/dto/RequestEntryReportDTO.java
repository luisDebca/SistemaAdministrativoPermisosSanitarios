package edu.ues.aps.process.dispatch.establishment.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class RequestEntryReportDTO implements AbstractRequestEntryReportDTO{

    private String establishment_name;
    private String establishment_type;
    private String owner_name;
    private String establishment_address;
    private String request_number;
    private LocalDateTime reception_on;
    private String user_name;

    public RequestEntryReportDTO(String establishment_name, String establishment_type, String owner_name, String establishment_address, String request_number, LocalDateTime reception_on) {
        this.establishment_name = establishment_name;
        this.establishment_type = establishment_type;
        this.owner_name = owner_name;
        this.establishment_address = establishment_address;
        this.request_number = request_number;
        this.reception_on = reception_on;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_type() {
        return establishment_type;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getReception_on() {
        if (reception_on != null){
            return DatetimeUtil.parseDateToLongDateFormat(reception_on.toLocalDate());
        }
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
