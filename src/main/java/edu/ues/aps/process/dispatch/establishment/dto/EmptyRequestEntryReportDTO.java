package edu.ues.aps.process.dispatch.establishment.dto;

public class EmptyRequestEntryReportDTO implements AbstractRequestEntryReportDTO{

    @Override
    public String getEstablishment_name() {
        return "Establishment name not found";
    }

    @Override
    public String getEstablishment_type() {
        return "Establishment type not found";
    }

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public String getEstablishment_address() {
        return "Establishment address not found";
    }

    @Override
    public String getRequest_number() {
        return "Request number not found";
    }

    @Override
    public String getReception_on() {
        return "--/--/----";
    }

    @Override
    public String getUser_name() {
        return "User name not found";
    }

    @Override
    public void setUser_name(String user_name) {

    }
}
