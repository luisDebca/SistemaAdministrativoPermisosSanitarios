package edu.ues.aps.process.dispatch.vehicle.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

public class EmptyVehicleProcessValidationReportDTO implements AbstractVehicleProcessValidationReportDTO {

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public String getVehicle_licenses() {
        return "Licenses not found";
    }

    @Override
    public void setVehicle_licenses(String vehicle_licenses) {

    }

    @Override
    public String getVehicle_address() {
        return "Address not found";
    }

    @Override
    public String getRequest_process_start() {
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    @Override
    public String getRequest_type() {
        return "Request type not found";
    }

    @Override
    public void setRequest_type(String request_type) {

    }

    @Override
    public String getCurrent_date() {
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    @Override
    public String getClient_name() {
        return "Client not found";
    }

    @Override
    public void setClient_name(String client_name) {

    }

    @Override
    public String getUser_name() {
        return "User not found";
    }

    @Override
    public void setUser_name(String user_name) {

    }

    @Override
    public String getUser_charge() {
        return "User not found";
    }

    @Override
    public void setUser_charge(String user_charge) {

    }

    @Override
    public Long getVehicle_count() {
        return 0L;
    }
}
