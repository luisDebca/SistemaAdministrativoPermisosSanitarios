package edu.ues.aps.process.dispatch.vehicle.repository;

import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleRequestDispatchDTO;

import java.util.List;

public interface VehicleRequestDispatchFinderRepository {

    List<AbstractVehicleRequestDispatchDTO> findAllRequest();
}
