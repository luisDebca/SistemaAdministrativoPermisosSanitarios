package edu.ues.aps.process.dispatch.base.service;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.service.CaseFilePersistenceService;
import edu.ues.aps.process.base.service.SingleCaseFileFinderService;
import edu.ues.aps.process.dispatch.establishment.service.RequestTracingStartProcessService;
import edu.ues.aps.process.tracing.base.model.Tracing;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class RequestEntryService implements RequestTracingStartProcessService {

    private final SingleCaseFileFinderService finderService;
    private final CaseFilePersistenceService persistenceService;

    public RequestEntryService(SingleCaseFileFinderService finderService, CaseFilePersistenceService persistenceService) {
        this.finderService = finderService;
        this.persistenceService = persistenceService;
    }

    @Override
    public void startTracing(Long id_caseFile) {
        AbstractCasefile caseFile = finderService.find(id_caseFile);
        Tracing tracing = new Tracing();
        tracing.setCreated_on(LocalDateTime.now());
        caseFile.addTracing(tracing);
        persistenceService.update(caseFile);
    }
}
