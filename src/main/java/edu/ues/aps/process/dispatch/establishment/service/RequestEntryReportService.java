package edu.ues.aps.process.dispatch.establishment.service;

import edu.ues.aps.process.dispatch.establishment.dto.AbstractRequestEntryReportDTO;
import edu.ues.aps.process.dispatch.establishment.dto.EmptyRequestEntryReportDTO;
import edu.ues.aps.process.dispatch.establishment.repository.RequestEntryReportFinderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional(readOnly = true)
public class RequestEntryReportService implements RequestEntryReportFinderService {

    private final RequestEntryReportFinderRepository finderRepository;

    @Autowired
    public RequestEntryReportService(RequestEntryReportFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractRequestEntryReportDTO findRequestInformationForReport(Long id_casefile) {
        try {
            return finderRepository.findReportInformation(id_casefile);
        } catch (NoResultException e) {
            return new EmptyRequestEntryReportDTO();
        }
    }
}
