package edu.ues.aps.process.dispatch.vehicle.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class VehicleRequestEntryReportDTO implements AbstractVehicleRequestEntryReportDTO {

    private String owner_name;
    private String vehicle_address;
    private LocalDateTime start_date;
    private String ucsf;
    private String request_number;
    private String vehicle_license;
    private String user_name;

    public VehicleRequestEntryReportDTO(String owner_name, String vehicle_address, LocalDateTime start_date, String ucsf, String request_number, String vehicle_license) {
        this.owner_name = owner_name;
        this.vehicle_address = vehicle_address;
        this.start_date = start_date;
        this.ucsf = ucsf;
        this.request_number = request_number;
        this.vehicle_license = vehicle_license;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getVehicle_address() {
        return vehicle_address;
    }

    public String getStart_date() {
        if (start_date == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(start_date.toLocalDate());
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getVehicle_license() {
        return vehicle_license;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

}
