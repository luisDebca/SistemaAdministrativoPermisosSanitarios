package edu.ues.aps.process.dispatch.base.controller;

import edu.ues.aps.utilities.mail.service.EmailDTO;
import edu.ues.aps.utilities.mail.service.EmailService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;

@Controller
@RequestMapping("/dispatch")
public class UCSFMailSenderController {

    private static final Logger logger = LogManager.getLogger(UCSFMailSenderController.class);

    private static final String REQUEST_EMAIL_FORM = "request_email_form";

    private final EmailService emailService;

    public UCSFMailSenderController(EmailService emailService) {
        this.emailService = emailService;
    }

    @GetMapping("/email-sender")
    public String showUCSFEmailSenderForm() {
        logger.info("GET:: new ucsf case file delivery email form.");
        return REQUEST_EMAIL_FORM;
    }

    @PostMapping("/email-sender")
    public String sendEmailToUCSF(@RequestParam("files") MultipartFile[] files,
                                  @RequestParam("email.to") String[] email_to,
                                  @RequestParam("email.subject") String email_subject,
                                  @RequestParam("email.cc") String[] email_cc,
                                  @RequestParam("email.text") String email_text) {
        logger.info("POST:: new UCSF email.");
        EmailDTO dto = new EmailDTO(email_to, email_subject, email_text, email_cc);
        if (containsEmptyMultipartFiles(files)) {
            emailService.sendSimpleMessage(dto);
        } else {
            emailService.sendMessageWithAttachment(dto, files);
        }
        return "redirect:/dispatch/index";
    }

    private boolean containsEmptyMultipartFiles(MultipartFile[] files) {
        for (MultipartFile file : files) {
            if (file.isEmpty()) {
                return true;
            }
        }
        return false;
    }
}
