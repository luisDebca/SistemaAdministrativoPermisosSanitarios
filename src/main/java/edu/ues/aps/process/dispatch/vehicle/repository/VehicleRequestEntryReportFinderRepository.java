package edu.ues.aps.process.dispatch.vehicle.repository;

import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleRequestEntryReportDTO;

import java.util.List;

public interface VehicleRequestEntryReportFinderRepository {

    List<AbstractVehicleRequestEntryReportDTO> getDatasource(Long id_bunch);
}
