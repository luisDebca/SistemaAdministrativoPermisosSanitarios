package edu.ues.aps.process.dispatch.vehicle.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleRequestEntryReportDTO;
import edu.ues.aps.process.dispatch.vehicle.service.VehicleRequestEntryReportFinderService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/dispatch/vehicle")
public class VehicleRequestEntryReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(VehicleRequestEntryReportController.class);

    private static final String REPORT_NAME = "RequestEntryVReport";

    private final SinglePersonalInformationService personalInformationService;
    private final VehicleRequestEntryReportFinderService finderService;

    public VehicleRequestEntryReportController(VehicleRequestEntryReportFinderService finderService, SinglePersonalInformationService personalInformationService) {
        this.finderService = finderService;
        this.personalInformationService = personalInformationService;
    }

    @GetMapping("/request-entry-report.pdf")
    public String generateReport(@RequestParam Long id_bunch, ModelMap model, Principal principal) {
        logger.info("REPORT:: Generate bunch {} request entry report", id_bunch);
        addReportRequiredSources(getReport(id_bunch, personalInformationService.getFullName(principal.getName())));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }

    private List<AbstractVehicleRequestEntryReportDTO> getReport(Long id_bunch, String user_name) {
        List<AbstractVehicleRequestEntryReportDTO> reportDTOS = finderService.getDatasource(id_bunch);
        for (AbstractVehicleRequestEntryReportDTO dto : reportDTOS) {
            dto.setUser_name(user_name);
        }
        return reportDTOS;
    }


}
