package edu.ues.aps.process.dispatch.base.service;

import edu.ues.aps.process.dispatch.base.dto.RequestDispatchSummary;
import edu.ues.aps.process.dispatch.base.repository.UCSFDispatchFinder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class UCSFDispatch implements GetAllUCSFDispatchSummary {

    private final UCSFDispatchFinder finder;

    @Autowired
    public UCSFDispatch(UCSFDispatchFinder finder) {
        this.finder = finder;
    }

    @Override
    public List<RequestDispatchSummary> getAllUcsfDispatchSummary() {
        return finder.getRequestSummary();
    }
}
