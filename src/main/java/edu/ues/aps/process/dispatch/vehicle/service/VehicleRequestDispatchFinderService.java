package edu.ues.aps.process.dispatch.vehicle.service;

import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleRequestDispatchDTO;

import java.util.List;

public interface VehicleRequestDispatchFinderService {

    List<AbstractVehicleRequestDispatchDTO> findAllRequest();
}
