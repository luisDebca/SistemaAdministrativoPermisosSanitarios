package edu.ues.aps.process.dispatch.establishment.async;

import edu.ues.aps.process.base.service.CaseFileRequiredReviewValidatorService;
import edu.ues.aps.process.dispatch.base.async.RequestUCSFDeliveryController;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
public class EstablishmentRequestUCSFDeliveryController extends RequestUCSFDeliveryController {

    private static final Logger logger = LogManager.getLogger(EstablishmentRequestUCSFDeliveryController.class);

    private final ProcessUserRegisterService registerService;

    public EstablishmentRequestUCSFDeliveryController(CaseFileRequiredReviewValidatorService reviewValidatorService, ProcessUserRegisterService registerService) {
        super(reviewValidatorService);
        this.registerService = registerService;
    }

    @Override
    @GetMapping(value = "/delivery/set-request-ucsf-delivery", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse updateRequestDelivery(@RequestParam Long id_caseFile, Principal principal) {
        logger.info("GET:: Update case file {} ucsf delivery status", id_caseFile);
        validateDelivery(id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.ucsf.delivery");
        return new SingleStringResponse("OK").asSuccessResponse();
    }
}
