package edu.ues.aps.process.dispatch.base.dto;

public interface RequestDispatchSummary {

    String getSibasi();

    String getUcsf();

    Long getRequest_delivery_count();

    Long getRequest_entry_count();

    Long getRequest_in_process_count();
}
