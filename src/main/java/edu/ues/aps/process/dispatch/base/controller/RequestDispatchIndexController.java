package edu.ues.aps.process.dispatch.base.controller;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dispatch")
public class RequestDispatchIndexController {

    private static final Logger logger = LogManager.getLogger(RequestDispatchIndexController.class);

    private static final String INDEX_TEMPLATE_NAME = "request_entry_index";

    @GetMapping({"/", "/index"})
    public String index(ModelMap model) {
        logger.info("GET:: Show request entry index");
        model.addAttribute("process", ProcessIdentifier.REQUEST_ACCEPTATION);
        return INDEX_TEMPLATE_NAME;
    }
}
