package edu.ues.aps.process.dispatch.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentRequestDTO;

public interface AbstractEstablishmentRequestDispatchDTO extends AbstractEstablishmentRequestDTO {

    Boolean getUcsf_delivery();
}
