package edu.ues.aps.process.dispatch.establishment.dto;

public interface AbstractRequestEntryReportDTO {

    String getEstablishment_name();

    String getEstablishment_type();

    String getOwner_name();

    String getEstablishment_address();

    String getRequest_number();

    String getReception_on();

    String getUser_name();

    void setUser_name(String user_name);
}
