package edu.ues.aps.process.dispatch.vehicle.dto;

public interface AbstractVehicleProcessValidationReportDTO {

    String getOwner_name();

    String getVehicle_licenses();

    void setVehicle_licenses(String vehicle_licenses);

    String getVehicle_address();

    String getRequest_process_start();

    String getRequest_type();

    void setRequest_type(String request_type);

    String getCurrent_date();

    String getClient_name();

    void setClient_name(String client_name);

    String getUser_name();

    void setUser_name(String user_name);

    String getUser_charge();

    void setUser_charge(String user_charge);

    Long getVehicle_count();
}
