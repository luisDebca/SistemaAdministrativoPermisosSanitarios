package edu.ues.aps.process.dispatch.base.async;

import edu.ues.aps.process.base.service.CaseFileRequiredReviewValidatorService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;

import java.security.Principal;

public abstract class RequestUCSFDeliveryController {

    private CaseFileRequiredReviewValidatorService reviewValidatorService;

    public RequestUCSFDeliveryController(CaseFileRequiredReviewValidatorService reviewValidatorService) {
        this.reviewValidatorService = reviewValidatorService;
    }

    public abstract SingleStringResponse updateRequestDelivery(Long id, Principal principal);

    public void validateDelivery(Long id_caseFile) {
        reviewValidatorService.validateUCSFDelivery(id_caseFile);
    }
}
