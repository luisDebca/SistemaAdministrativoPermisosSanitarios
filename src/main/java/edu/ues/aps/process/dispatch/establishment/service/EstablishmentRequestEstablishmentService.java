package edu.ues.aps.process.dispatch.establishment.service;

import edu.ues.aps.process.dispatch.base.service.EstablishmentRequestDispatchFinderService;
import edu.ues.aps.process.dispatch.establishment.dto.AbstractEstablishmentRequestDispatchDTO;
import edu.ues.aps.process.dispatch.establishment.repository.EstablishmentRequestDispatchFinderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class EstablishmentRequestEstablishmentService implements EstablishmentRequestDispatchFinderService {

    private final EstablishmentRequestDispatchFinderRepository finderRepository;

    @Autowired
    public EstablishmentRequestEstablishmentService(EstablishmentRequestDispatchFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractEstablishmentRequestDispatchDTO> findAllRequestForDispatchProcess() {
        return finderRepository.findAllRequestForDispatchProcess();
    }
}
