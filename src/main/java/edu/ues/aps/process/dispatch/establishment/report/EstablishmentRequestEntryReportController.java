package edu.ues.aps.process.dispatch.establishment.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.dispatch.establishment.dto.AbstractRequestEntryReportDTO;
import edu.ues.aps.process.dispatch.establishment.service.RequestEntryReportFinderService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Collections;

@Controller
@RequestMapping("/dispatch/establishment")
public class EstablishmentRequestEntryReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(EstablishmentRequestEntryReportController.class);

    private static final String REPORT_NAME = "RequestEntryEReport";

    private final SinglePersonalInformationService personalInformationService;
    private final RequestEntryReportFinderService finderService;

    public EstablishmentRequestEntryReportController(RequestEntryReportFinderService finderService, SinglePersonalInformationService personalInformationService) {
        this.finderService = finderService;
        this.personalInformationService = personalInformationService;
    }

    @GetMapping("/request-entry-report.pdf")
    public String generateRequestEntryVoucherReport(@RequestParam("id_caseFile") Long id_caseFile, Principal principal, ModelMap model) {
        logger.info("REPORT:: Generate case file {} request entry report", id_caseFile);
        AbstractRequestEntryReportDTO datasource = finderService.findRequestInformationForReport(id_caseFile);
        datasource.setUser_name(personalInformationService.getFullName(principal.getName()));
        addReportRequiredSources(Collections.singletonList(datasource));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }
}
