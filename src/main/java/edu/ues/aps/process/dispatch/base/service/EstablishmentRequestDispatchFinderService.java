package edu.ues.aps.process.dispatch.base.service;

import edu.ues.aps.process.dispatch.establishment.dto.AbstractEstablishmentRequestDispatchDTO;

import java.util.List;

public interface EstablishmentRequestDispatchFinderService {

    List<AbstractEstablishmentRequestDispatchDTO> findAllRequestForDispatchProcess();
}
