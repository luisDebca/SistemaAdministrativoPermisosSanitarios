package edu.ues.aps.process.dispatch.vehicle.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class VehicleProcessValidationReportDTO implements AbstractVehicleProcessValidationReportDTO {

    private String owner_name;
    private String vehicle_licenses;
    private String vehicle_address;
    private LocalDateTime request_process_start;
    private String request_type;
    private String current_date;
    private String client_name;
    private String user_name;
    private String user_charge;
    private Long vehicle_count;

    public VehicleProcessValidationReportDTO(String owner_name, String vehicle_address, LocalDateTime request_process_start, Long vehicle_count) {
        this.owner_name = owner_name;
        this.vehicle_address = vehicle_address;
        this.request_process_start = request_process_start;
        this.vehicle_count = vehicle_count;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getVehicle_licenses() {
        return vehicle_licenses;
    }

    public void setVehicle_licenses(String vehicle_licenses) {
        this.vehicle_licenses = vehicle_licenses;
    }

    public String getVehicle_address() {
        return vehicle_address;
    }

    public String getRequest_process_start() {
        if (request_process_start == null) return DatetimeUtil.DATE_NOT_DEFINED;
        return DatetimeUtil.parseDateToLongDateFormat(request_process_start.toLocalDate());
    }

    public String getRequest_type() {
        return request_type;
    }

    public void setRequest_type(String request_type) {
        this.request_type = request_type;
    }

    public String getCurrent_date() {
        return DatetimeUtil.parseDateToVerboseDate(LocalDate.now());
    }

    public String getClient_name() {
        return client_name;
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }

    public String getUser_charge() {
        return user_charge;
    }

    public void setUser_charge(String user_charge) {
        this.user_charge = user_charge;
    }

    public Long getVehicle_count() {
        return vehicle_count;
    }
}
