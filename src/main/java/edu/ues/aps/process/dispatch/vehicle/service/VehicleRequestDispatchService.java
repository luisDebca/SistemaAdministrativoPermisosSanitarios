package edu.ues.aps.process.dispatch.vehicle.service;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleProcessValidationReportDTO;
import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleRequestDispatchDTO;
import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleRequestEntryReportDTO;
import edu.ues.aps.process.dispatch.vehicle.repository.VehicleRequestDispatchFinderRepository;
import edu.ues.aps.process.dispatch.vehicle.repository.VehicleRequestEntryReportFinderRepository;
import edu.ues.aps.process.dispatch.vehicle.repository.VehicleRequestProcessValidationReportFinderRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleRequestDispatchService implements VehicleRequestDispatchFinderService, VehicleRequestEntryReportFinderService, VehicleProcessValidationReportFinderService {

    private final VehicleBunchInformationService informationService;
    private final VehicleRequestDispatchFinderRepository finderRepository;
    private final VehicleRequestEntryReportFinderRepository reportFinderRepository;
    private final VehicleRequestProcessValidationReportFinderRepository validationReportFinderRepository;

    @Autowired
    public VehicleRequestDispatchService(VehicleRequestDispatchFinderRepository finderRepository, VehicleBunchInformationService informationService, VehicleRequestEntryReportFinderRepository reportFinderRepository, VehicleRequestProcessValidationReportFinderRepository validationReportFinderRepository) {
        this.finderRepository = finderRepository;
        this.informationService = informationService;
        this.reportFinderRepository = reportFinderRepository;
        this.validationReportFinderRepository = validationReportFinderRepository;
    }

    @Override
    public List<AbstractVehicleRequestDispatchDTO> findAllRequest() {
        List<AbstractVehicleRequestDispatchDTO> allRequest = finderRepository.findAllRequest();
        for (AbstractVehicleRequestDispatchDTO dto : allRequest) {
            dto.setCertification_type(informationService.findAllCertificationTypes(dto.getId_bunch()));
        }
        return allRequest;
    }

    @Override
    public List<AbstractVehicleRequestEntryReportDTO> getDatasource(Long id_bunch) {
        return reportFinderRepository.getDatasource(id_bunch);
    }

    @Override
    public AbstractVehicleProcessValidationReportDTO getReport(Long id_bunch) {
        AbstractVehicleProcessValidationReportDTO dto = validationReportFinderRepository.getReport(id_bunch);
        dto.setRequest_type(informationService.findAllCertificationTypes(id_bunch));
        dto.setVehicle_licenses(informationService.findAllLicenses(id_bunch));
        return dto;
    }
}
