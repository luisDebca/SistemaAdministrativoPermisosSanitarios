package edu.ues.aps.process.dispatch.vehicle.service;

import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleProcessValidationReportDTO;

public interface VehicleProcessValidationReportFinderService {

    AbstractVehicleProcessValidationReportDTO getReport(Long id_bunch);
}
