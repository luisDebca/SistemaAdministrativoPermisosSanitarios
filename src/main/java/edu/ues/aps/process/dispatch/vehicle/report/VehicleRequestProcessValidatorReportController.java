package edu.ues.aps.process.dispatch.vehicle.report;

import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleProcessValidationReportDTO;
import edu.ues.aps.process.dispatch.vehicle.service.VehicleProcessValidationReportFinderService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Collections;

@Controller
@RequestMapping("/dispatch/vehicle")
public class VehicleRequestProcessValidatorReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(VehicleRequestProcessValidatorReportController.class);

    private static final String REPORT_NAME = "ProcessInformationVReport";

    private final SinglePersonalInformationService personalInformationService;
    private final VehicleProcessValidationReportFinderService finderService;

    public VehicleRequestProcessValidatorReportController(VehicleProcessValidationReportFinderService finderService, SinglePersonalInformationService personalInformationService) {
        this.finderService = finderService;
        this.personalInformationService = personalInformationService;
    }

    @GetMapping("/request-process-validation.pdf")
    public String generateReport(@RequestParam Long id_bunch, @RequestParam String client_name,
                                 ModelMap model, Principal principal) {
        logger.info("REPORT:: Generate bunch {} request process validator.", id_bunch);
        AbstractVehicleProcessValidationReportDTO dto = finderService.getReport(id_bunch);
        dto.setClient_name(client_name);
        dto.setUser_name(personalInformationService.getFullName(principal.getName()));
        dto.setUser_charge(personalInformationService.getCharge(principal.getName()));
        addReportRequiredSources(Collections.singletonList(dto));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }


}
