package edu.ues.aps.process.dispatch.base.repository;

import edu.ues.aps.process.dispatch.base.dto.RequestDispatchSummary;

import java.util.List;

public interface UCSFDispatchFinder {

    List<RequestDispatchSummary> getRequestSummary();
}
