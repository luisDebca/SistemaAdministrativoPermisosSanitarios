package edu.ues.aps.process.dispatch.base.service;

import edu.ues.aps.process.dispatch.base.dto.RequestDispatchSummary;

import java.util.List;

public interface GetAllUCSFDispatchSummary {

    List<RequestDispatchSummary> getAllUcsfDispatchSummary();
}
