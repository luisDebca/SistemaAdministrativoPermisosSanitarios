package edu.ues.aps.process.dispatch.base.repository;

import edu.ues.aps.process.dispatch.base.dto.RequestDispatchSummary;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.time.LocalDateTime;
import java.util.List;

@Repository
public class UCSFDispatchDatabaseFinder implements UCSFDispatchFinder{

    private final SessionFactory sessionFactory;

    @Autowired
    public UCSFDispatchDatabaseFinder(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<RequestDispatchSummary> getRequestSummary() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.dispatch.base.dto.RequestDispatchSummaryDTO(" +
                "sibasi.zone,ucsf.name,count(casefile),count(casefile),count(casefile)) " +
                "from Ucsf ucsf " +
                "inner join ucsf.sibasi sibasi " +
                "inner join ucsf.casefiles casefile " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "inner join casefile.tracing tracing " +
                "where (process.identifier = :initial_process or process.identifier = :end_process) " +
                "and record.active = true " +
                "and record.start_on >= :this_month " +
                "group by ucsf",RequestDispatchSummary.class)
                .setParameter("initial_process",ProcessIdentifier.REQUEST_ACCEPTATION)
                .setParameter("end_process",ProcessIdentifier.REQUEST_TRACING)
                .setParameter("this_month",LocalDateTime.now().withDayOfMonth(1).withHour(0).withMinute(0))
                .getResultList();
    }
}
