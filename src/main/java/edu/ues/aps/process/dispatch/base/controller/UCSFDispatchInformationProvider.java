package edu.ues.aps.process.dispatch.base.controller;

import edu.ues.aps.process.dispatch.base.dto.RequestDispatchSummary;
import edu.ues.aps.process.dispatch.base.service.GetAllUCSFDispatchSummary;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/dispatch")
public class UCSFDispatchInformationProvider {

    private static final Logger logger = LogManager.getLogger(UCSFDispatchInformationProvider.class);

    private final GetAllUCSFDispatchSummary dispatchSummary;

    public UCSFDispatchInformationProvider(GetAllUCSFDispatchSummary dispatchSummary) {
        this.dispatchSummary = dispatchSummary;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/ucsf-dispatch-summary", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<RequestDispatchSummary> getUCSFRequestDispatchSummary() {
        logger.info("GET:: Find all request entry summary");
        return dispatchSummary.getAllUcsfDispatchSummary();
    }
}
