package edu.ues.aps.process.dispatch.vehicle.repository;

import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleProcessValidationReportDTO;

import javax.persistence.NoResultException;

public interface VehicleRequestProcessValidationReportFinderRepository {

    AbstractVehicleProcessValidationReportDTO getReport(Long id_bunch) throws NoResultException;
}
