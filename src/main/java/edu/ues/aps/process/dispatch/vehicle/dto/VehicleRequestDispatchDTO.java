package edu.ues.aps.process.dispatch.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.VehicleRequestDTO;

import java.time.LocalDateTime;

public class VehicleRequestDispatchDTO extends VehicleRequestDTO implements AbstractVehicleRequestDispatchDTO {

    private String ucsf;
    private String sibasi;
    private Boolean ucsf_delivery;
    private String certification_type;

    public VehicleRequestDispatchDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on, String ucsf, String sibasi, Boolean ucsf_delivery) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
        this.ucsf = ucsf;
        this.sibasi = sibasi;
        this.ucsf_delivery = ucsf_delivery;
    }

    public String getCertification_type() {
        return certification_type;
    }

    public void setCertification_type(String certification_type) {
        this.certification_type = certification_type;
    }

    public String getUcsf() {
        return ucsf;
    }

    public String getSibasi() {
        return sibasi;
    }

    public Boolean getUcsf_delivery() {
        if (ucsf_delivery == null) return false;
        return ucsf_delivery;
    }
}
