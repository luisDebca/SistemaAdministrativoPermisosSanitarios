package edu.ues.aps.process.dispatch.vehicle.async;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.base.service.CaseFileRequiredReviewValidatorService;
import edu.ues.aps.process.dispatch.base.async.RequestUCSFDeliveryController;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
public class VehicleRequestUCSFDeliveryController extends RequestUCSFDeliveryController {

    private static final Logger logger = LogManager.getLogger(VehicleRequestUCSFDeliveryController.class);

    private final ProcessUserRegisterService registerService;
    private final VehicleBunchInformationService informationService;

    public VehicleRequestUCSFDeliveryController(CaseFileRequiredReviewValidatorService reviewValidatorService, ProcessUserRegisterService registerService, VehicleBunchInformationService informationService) {
        super(reviewValidatorService);
        this.registerService = registerService;
        this.informationService = informationService;
    }

    @Override
    @GetMapping(value = "/delivery/bunch/set-request-ucsf-delivery", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse updateRequestDelivery(Long id_bunch, Principal principal) {
        logger.info("GET:: Update all case file ucsf delivery status in bunch {}.", id_bunch);
        for (Long id_caseFile : informationService.findAllCaseFileId(id_bunch)) {
            validateDelivery(id_caseFile);
            registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.ucsf.delivery");
        }
        return new SingleStringResponse("OK").asSuccessResponse();
    }
}
