package edu.ues.aps.process.dispatch.establishment.repository;

import edu.ues.aps.process.dispatch.establishment.dto.AbstractEstablishmentRequestDispatchDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EstablishmentRequestDispatchService implements EstablishmentRequestDispatchFinderRepository {

    private final SessionFactory sessionFactory;

    @Autowired
    public EstablishmentRequestDispatchService(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractEstablishmentRequestDispatchDTO> findAllRequestForDispatchProcess() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.dispatch.establishment.dto.EstablishmentRequestDispatchDTO(ce.id ,owner.id,establishment.id,concat(concat(ce.request_folder,'-'),ce.request_year),concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year)," +
                "establishment.name,establishment.type_detail,etype.type,section.type,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,ucsf.name,sibasi.zone,ce.certification_type,ce.creation_date,owner.name,ce.ucsf_delivery)" +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "left join establishment.type etype " +
                "left join etype.section section " +
                "left join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id", AbstractEstablishmentRequestDispatchDTO.class)
                .setParameter("id", ProcessIdentifier.REQUEST_ACCEPTATION)
                .getResultList();
    }
}
