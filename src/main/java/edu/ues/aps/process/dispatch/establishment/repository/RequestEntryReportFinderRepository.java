package edu.ues.aps.process.dispatch.establishment.repository;

import edu.ues.aps.process.dispatch.establishment.dto.AbstractRequestEntryReportDTO;

import javax.persistence.NoResultException;

public interface RequestEntryReportFinderRepository {

    AbstractRequestEntryReportDTO findReportInformation(Long id_casefile) throws NoResultException;
}
