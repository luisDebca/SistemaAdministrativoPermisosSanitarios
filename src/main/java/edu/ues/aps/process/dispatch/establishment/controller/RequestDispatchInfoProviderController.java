package edu.ues.aps.process.dispatch.establishment.controller;

import edu.ues.aps.process.dispatch.base.service.EstablishmentRequestDispatchFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/dispatch/establishment")
public class RequestDispatchInfoProviderController {

    private static final Logger logger = LogManager.getLogger(RequestDispatchInfoProviderController.class);

    private static final String REQUEST_ENTRY_LIST = "request_entry_list";

    private final EstablishmentRequestDispatchFinderService service;

    @Autowired
    public RequestDispatchInfoProviderController(EstablishmentRequestDispatchFinderService service) {
        this.service = service;
    }

    @GetMapping("/list")
    public String showAllRequestForUCSFCaseFileInformationDelivery(ModelMap model) {
        logger.info("GET:: request entry for UCSF delivery list.");
        model.addAttribute("requests", service.findAllRequestForDispatchProcess());
        return REQUEST_ENTRY_LIST;
    }

}
