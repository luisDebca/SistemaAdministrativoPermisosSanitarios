package edu.ues.aps.process.dispatch.establishment.report;

import edu.ues.aps.process.area.establishment.dto.AbstractRequestOnProcessValidityReport;
import edu.ues.aps.process.area.establishment.service.RequestProcessValidatorReportService;
import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;
import java.util.Collections;

@Controller
@RequestMapping("/dispatch/establishment")
public class RequestProcessValidatorReportController extends AbstractReportController {

    private static final Logger logger = LogManager.getLogger(RequestProcessValidatorReportController.class);

    private static final String REPORT_NAME = "ProcessInformationEReport";

    private final SinglePersonalInformationService personalInformationService;
    private final RequestProcessValidatorReportService reportService;

    public RequestProcessValidatorReportController(RequestProcessValidatorReportService reportService, SinglePersonalInformationService personalInformationService) {
        this.reportService = reportService;
        this.personalInformationService = personalInformationService;
    }

    @GetMapping("/request-process-validation.pdf")
    public String generateRequestProcessValidatorReport(@RequestParam Long id_caseFile, @RequestParam String client_name,
                                                        ModelMap model, Principal principal) {
        logger.info("GET:: Generating request {} process validation report.", id_caseFile);
        AbstractRequestOnProcessValidityReport dto = reportService.getReportDataSource(id_caseFile);
        dto.setClient_name(client_name);
        dto.setUser_name(personalInformationService.getFullName(principal.getName()));
        dto.setUser_charge(personalInformationService.getCharge(principal.getName()));
        addReportRequiredSources(Collections.singletonList(dto));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }

}
