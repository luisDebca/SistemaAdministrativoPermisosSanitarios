package edu.ues.aps.process.dispatch.vehicle.service;

import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleRequestEntryReportDTO;

import java.util.List;

public interface VehicleRequestEntryReportFinderService {

    List<AbstractVehicleRequestEntryReportDTO> getDatasource(Long id_bunch);
}
