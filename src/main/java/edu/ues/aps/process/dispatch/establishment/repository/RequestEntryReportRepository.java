package edu.ues.aps.process.dispatch.establishment.repository;

import edu.ues.aps.process.dispatch.establishment.dto.AbstractRequestEntryReportDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class RequestEntryReportRepository implements RequestEntryReportFinderRepository{

    private final SessionFactory sessionFactory;

    @Autowired
    public RequestEntryReportRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractRequestEntryReportDTO findReportInformation(Long id_casefile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.dispatch.establishment.dto.RequestEntryReportDTO(" +
                "establishment.name,establishment.type_detail,owner.name," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "concat(concat(concat(concat('06',casefile.request_code),casefile.request_number),'-'),casefile.request_year)," +
                "record.start_on) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.address address " +
                "inner join establishment.owner owner " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where casefile.id = :id " +
                "and process.identifier = :identifier",AbstractRequestEntryReportDTO.class)
                .setParameter("id",id_casefile)
                .setParameter("identifier", ProcessIdentifier.REQUEST_ACCEPTATION)
                .getSingleResult();
    }
}
