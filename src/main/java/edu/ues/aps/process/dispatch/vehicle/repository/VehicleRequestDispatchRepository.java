package edu.ues.aps.process.dispatch.vehicle.repository;

import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleProcessValidationReportDTO;
import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleRequestDispatchDTO;
import edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleRequestEntryReportDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class VehicleRequestDispatchRepository implements VehicleRequestDispatchFinderRepository, VehicleRequestEntryReportFinderRepository, VehicleRequestProcessValidationReportFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleRequestDispatchRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleRequestDispatchDTO> findAllRequest() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.dispatch.vehicle.dto.VehicleRequestDispatchDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date,ucsf.name,sibasi.zone,cv.ucsf_delivery)" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "left join cv.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehicleRequestDispatchDTO.class)
                .setParameter("id", ProcessIdentifier.REQUEST_ACCEPTATION)
                .getResultList();
    }

    @Override
    public List<AbstractVehicleRequestEntryReportDTO> getDatasource(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.dispatch.vehicle.dto.VehicleRequestEntryReportDTO(" +
                "owner.name,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "cv.creation_date,ucsf.name,concat(concat(concat(concat('06',cv.request_code),cv.request_number),'-'),cv.request_year)," +
                "vehicle.license) " +
                "from VehicleBunch bunch " +
                "left join bunch.address address " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "left join cv.ucsf ucsf " +
                "where bunch.id = :id", AbstractVehicleRequestEntryReportDTO.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }

    @Override
    public AbstractVehicleProcessValidationReportDTO getReport(Long id_bunch) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.dispatch.vehicle.dto.VehicleProcessValidationReportDTO(" +
                "owner.name,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "cv.creation_date, count (cv)) " +
                "from VehicleBunch bunch " +
                "inner join bunch.address address " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "where bunch.id = :id", AbstractVehicleProcessValidationReportDTO.class)
                .setParameter("id", id_bunch)
                .getSingleResult();
    }
}
