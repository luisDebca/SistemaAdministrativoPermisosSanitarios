package edu.ues.aps.process.dispatch.vehicle.dto;

public interface AbstractVehicleRequestEntryReportDTO {

    String getOwner_name();

    String getVehicle_address();

    String getStart_date();

    String getUcsf();

    String getRequest_number();

    String getVehicle_license();

    String getUser_name();

    void setUser_name(String user_name);
}
