package edu.ues.aps.process.dispatch.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleRequestDTO;

public interface AbstractVehicleRequestDispatchDTO extends AbstractVehicleRequestDTO {

    String getCertification_type();

    void setCertification_type(String certification_type);

    String getUcsf();

    String getSibasi();

    Boolean getUcsf_delivery();
}
