package edu.ues.aps.process.dispatch.establishment.repository;

import edu.ues.aps.process.dispatch.establishment.dto.AbstractEstablishmentRequestDispatchDTO;

import java.util.List;

public interface EstablishmentRequestDispatchFinderRepository {

    List<AbstractEstablishmentRequestDispatchDTO> findAllRequestForDispatchProcess();

}
