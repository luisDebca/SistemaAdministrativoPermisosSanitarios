package edu.ues.aps.process.archive.vehicle.service;

import edu.ues.aps.process.archive.vehicle.dto.AbstractVehicleArchiveDTO;
import edu.ues.aps.process.archive.vehicle.repository.VehicleArchiveDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleArchiveDTOService implements VehicleArchiveDTOFinderService {

    private final VehicleArchiveDTOFinderRepository finderRepository;

    public VehicleArchiveDTOService(VehicleArchiveDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractVehicleArchiveDTO> findAll() {
        return finderRepository.findAll();
    }
}
