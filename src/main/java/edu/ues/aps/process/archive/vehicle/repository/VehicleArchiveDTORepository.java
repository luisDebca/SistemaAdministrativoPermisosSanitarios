package edu.ues.aps.process.archive.vehicle.repository;

import edu.ues.aps.process.archive.vehicle.dto.AbstractVehicleArchiveDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.review.vehicle.dto.AbstractVehicleRequestPreReviewDTO;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleArchiveDTORepository implements VehicleArchiveDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleArchiveDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleArchiveDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.archive.vehicle.dto.VehicleArchiveDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date)" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehicleArchiveDTO.class)
                .setParameter("id", ProcessIdentifier.REQUEST_STORE)
                .getResultList();
    }
}
