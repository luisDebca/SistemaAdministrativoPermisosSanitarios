package edu.ues.aps.process.archive.vehicle.repository;

import edu.ues.aps.process.archive.vehicle.dto.AbstractVehicleArchiveDTO;

import java.util.List;

public interface VehicleArchiveDTOFinderRepository {

    List<AbstractVehicleArchiveDTO> findAll();
}
