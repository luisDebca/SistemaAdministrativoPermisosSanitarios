package edu.ues.aps.process.archive.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentRequestDTO;

public interface AbstractEstablishmentRequestArchiveDTO extends AbstractEstablishmentRequestDTO {

    String getResolution_number();
}
