package edu.ues.aps.process.archive.establishment.repository;

import edu.ues.aps.process.archive.establishment.dto.AbstractEstablishmentRequestArchiveDTO;

import java.util.List;

public interface EstablishmentRequestArchiveFinderRepository {

    List<AbstractEstablishmentRequestArchiveDTO> findAll();
}
