package edu.ues.aps.process.archive.vehicle.controller;

import edu.ues.aps.process.archive.vehicle.service.VehicleArchiveDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/archive/vehicle")
public class VehicleDocumentArchiveInformationProvider {

    private static final Logger logger = LogManager.getLogger(VehicleDocumentArchiveInformationProvider.class);

    private static final String REQUEST_LIST = "vehicle_process_file_store_list";

    private final VehicleArchiveDTOFinderService finderService;

    public VehicleDocumentArchiveInformationProvider(VehicleArchiveDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showAllRequest(ModelMap model){
        logger.info("GET:: Show all vehicles request for documentation archive.");
        model.addAttribute("requests", finderService.findAll());
        return REQUEST_LIST;
    }
}
