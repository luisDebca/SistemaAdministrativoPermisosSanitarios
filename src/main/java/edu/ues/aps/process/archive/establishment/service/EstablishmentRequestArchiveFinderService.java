package edu.ues.aps.process.archive.establishment.service;

import edu.ues.aps.process.archive.establishment.dto.AbstractEstablishmentRequestArchiveDTO;

import java.util.List;

public interface EstablishmentRequestArchiveFinderService {

    List<AbstractEstablishmentRequestArchiveDTO> findAll();
}
