package edu.ues.aps.process.archive.base.controller;

import edu.ues.aps.process.approval.base.controller.RequestApprovalIndexController;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RequestDocumentationArchiveIndexController {

    private static final Logger logger = LogManager.getLogger(RequestApprovalIndexController.class);

    private static final String FILE_STORE_INDEX = "process_file_store_index";

    @GetMapping({"/archive/", "/archive/index"})
    public String showCaseFileProcessFileStoreIndex(ModelMap model) {
        logger.info("GET:: Show caseFile file store index");
        model.addAttribute("process", ProcessIdentifier.REQUEST_STORE);
        return FILE_STORE_INDEX;
    }
}
