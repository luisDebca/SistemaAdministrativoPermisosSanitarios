package edu.ues.aps.process.archive.establishment.controller;

import edu.ues.aps.process.archive.establishment.service.EstablishmentRequestArchiveFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/archive/establishment")
public class EstablishmentDocumentArchiveInformationProviderController {

    private static final Logger logger = LogManager.getLogger(EstablishmentDocumentArchiveInformationProviderController.class);

    private static final String REQUEST_LIST = "establishment_process_file_store_list";

    private final EstablishmentRequestArchiveFinderService finderService;

    public EstablishmentDocumentArchiveInformationProviderController(EstablishmentRequestArchiveFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/list")
    public String showBeforeFinishingProcessReviewRequestList(ModelMap model){
        logger.info("GET:: Show all establishment request for documentation archive.");
        model.addAttribute("requests",finderService.findAll());
        return REQUEST_LIST;
    }
}
