package edu.ues.aps.process.archive.vehicle.service;

import edu.ues.aps.process.archive.vehicle.dto.AbstractVehicleArchiveDTO;

import java.util.List;

public interface VehicleArchiveDTOFinderService {

    List<AbstractVehicleArchiveDTO> findAll();
}
