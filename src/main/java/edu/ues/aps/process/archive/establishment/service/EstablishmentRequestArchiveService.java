package edu.ues.aps.process.archive.establishment.service;

import edu.ues.aps.process.archive.establishment.dto.AbstractEstablishmentRequestArchiveDTO;
import edu.ues.aps.process.archive.establishment.repository.EstablishmentRequestArchiveFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class EstablishmentRequestArchiveService implements EstablishmentRequestArchiveFinderService {

    private final EstablishmentRequestArchiveFinderRepository finderRepository;

    public EstablishmentRequestArchiveService(EstablishmentRequestArchiveFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractEstablishmentRequestArchiveDTO> findAll() {
        return finderRepository.findAll();
    }
}
