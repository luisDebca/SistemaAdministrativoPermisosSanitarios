package edu.ues.aps.process.requirement.base.model;

public enum DocumentTemplateType {
    ESTABLISHMENT("Establecimientos"),
    VEHICLE("VehÝculos"),
    TOBACCO("Tabaco"),
    INVALID("Invalido");

    private String documentTemplateType;

    DocumentTemplateType(String type) {
        this.documentTemplateType = type;
    }

    public String getDocumentTemplateType() {
        return documentTemplateType;
    }
}
