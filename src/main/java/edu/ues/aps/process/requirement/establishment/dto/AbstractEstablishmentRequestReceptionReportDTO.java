package edu.ues.aps.process.requirement.establishment.dto;

import edu.ues.aps.process.requirement.base.dto.AbstractRequestReceptionReportDTO;

public interface AbstractEstablishmentRequestReceptionReportDTO extends AbstractRequestReceptionReportDTO {

    String getEstablishment_name();

    String getEstablishment_address();
}
