package edu.ues.aps.process.requirement.establishment.dto;

import edu.ues.aps.process.requirement.base.dto.AbstractRequirementControlDocumentReportDTO;

public interface AbstractRequirementControlEstablishmentReportDTO extends AbstractRequirementControlDocumentReportDTO {

    String getEstablishment_name();

    String getEstablishment_nit();

    String getEstablishment_type_detail();

    String getEstablishment_address();
}
