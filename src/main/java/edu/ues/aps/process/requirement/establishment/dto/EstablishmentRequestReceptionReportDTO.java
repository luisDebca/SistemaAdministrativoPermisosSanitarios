package edu.ues.aps.process.requirement.establishment.dto;

import edu.ues.aps.process.requirement.base.dto.RequestReceptionReportDTO;

import java.time.LocalDateTime;

public class EstablishmentRequestReceptionReportDTO extends RequestReceptionReportDTO implements AbstractEstablishmentRequestReceptionReportDTO{

    private String establishment_name;
    private String establishment_address;

    public EstablishmentRequestReceptionReportDTO(LocalDateTime acceptance_created_on, String establishment_name, String establishment_address) {
        super(acceptance_created_on);
        this.establishment_name = establishment_name;
        this.establishment_address = establishment_address;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }
}
