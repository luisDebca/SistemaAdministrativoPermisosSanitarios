package edu.ues.aps.process.requirement.vehicle.report;

import edu.ues.aps.process.base.dto.ClientDTO;
import edu.ues.aps.process.requirement.base.report.RequestReceptionReportController;
import edu.ues.aps.process.requirement.vehicle.service.VehicleRequestReceptionReportService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;
import java.util.Collections;

@Controller
public class VehicleRequestReceptionReportController extends RequestReceptionReportController {

    private static final Logger logger = LogManager.getLogger(VehicleRequestReceptionReportController.class);

    private static final String SMALL_REPORT = "RequestReceptionVSmallReport";
    private static final String MEDIUM_REPORT = "RequestReceptionVMediumReport";
    private static final String LARGE_REPORT = "RequestReceptionVLargeReport";

    private final SinglePersonalInformationService personalInformationService;
    private final VehicleRequestReceptionReportService receptionReportService;

    public VehicleRequestReceptionReportController(SinglePersonalInformationService personalInformationService, VehicleRequestReceptionReportService receptionReportService) {
        this.personalInformationService = personalInformationService;
        this.receptionReportService = receptionReportService;
    }

    @Override
    @GetMapping("/requirement-control/vehicle/bunch/{id_bunch}/case-file/request-reception-report.pdf")
    public String generateReport(@PathVariable Long id_bunch, ClientDTO selected_client, ModelMap model, Principal principal) {
        logger.info("REPORT:: Generate request bunch {} reception report", id_bunch);
        setDatasource(receptionReportService.findReportResource(id_bunch));
        addClientDataToDatasource(selected_client);
        addCurrentUserNameToDatasource(personalInformationService.getFullName(principal.getName()));
        addReportRequiredSources(Collections.singletonList(getDatasource()));
        model.addAllAttributes(getModel());
        return getReportByVehicleSize(receptionReportService.bunchVehicleCount(id_bunch));
    }

    private String getReportByVehicleSize(int size){
        if (size <= 25){
            return SMALL_REPORT;
        }else if (size <= 50){
            return MEDIUM_REPORT;
        }else {
            return LARGE_REPORT;
        }
    }
}
