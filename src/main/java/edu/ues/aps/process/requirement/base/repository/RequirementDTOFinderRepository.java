package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.dto.AbstractRequirementDTO;
import edu.ues.aps.process.requirement.base.dto.RequirementDTO;

import java.util.List;

public interface RequirementDTOFinderRepository {

    List<AbstractRequirementDTO> findAll();

    List<AbstractRequirementDTO> findAllRequirementsFromTemplate(Long id_template);
}
