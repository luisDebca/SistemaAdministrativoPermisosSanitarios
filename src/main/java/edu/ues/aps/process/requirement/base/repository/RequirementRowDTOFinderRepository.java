package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;

import java.util.List;

public interface RequirementRowDTOFinderRepository {

    List<RequirementRowDTO> findAllRowsFromDocument(Long id_document);
}
