package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RequirementControlTemplateRepository implements RequirementControlTemplateCrudRepository {

    private SessionFactory sessionFactory;

    public RequirementControlTemplateRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void delete(Long id_template, Long id_requirement) {
        sessionFactory.getCurrentSession().createQuery("delete from RequirementControlTemplate control " +
                "where control.id.requirement_id = :id_requirement " +
                "and control.id.template_id = :id_template")
                .setParameter("id_requirement", id_requirement)
                .setParameter("id_template", id_template)
                .executeUpdate();
    }

    @Override
    public List<RequirementControlTemplate> findAllFromTemplate(Long id_template) {
        return sessionFactory.getCurrentSession().createQuery("select control " +
                "from RequirementControlTemplate  control " +
                "inner join control.requirement requirement " +
                "inner join control.template template " +
                "where template.id = :id", RequirementControlTemplate.class)
                .setParameter("id", id_template)
                .getResultList();
    }
}
