package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.model.AbstractRequirementControlDocument;

import javax.persistence.NoResultException;

public interface SingleRequirementControlDocumentFinderRepository {

    AbstractRequirementControlDocument find(Long id_document) throws NoResultException;
}
