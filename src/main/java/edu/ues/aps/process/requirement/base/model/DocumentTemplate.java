package edu.ues.aps.process.requirement.base.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity
@Table(name = "DOCUMENT_TEMPLATE")
public class DocumentTemplate implements AbstractDocumentTemplate {

    public static final Integer DEFAULT_VERSION = 1;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, length = 20)
    private DocumentTemplateType type;

    @Column(name = "title", nullable = false, length = 250)
    private String title;

    @Column(name = "subtitle", length = 250)
    private String subtitle;

    @Column(name = "version")
    private Integer version;

    @OneToMany(mappedBy = "template", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RequirementControlTemplate> requirements = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public DocumentTemplateType getType() {
        return type;
    }

    @Override
    public String getType_text() {
        return type.getDocumentTemplateType();
    }

    @Override
    public void setType(DocumentTemplateType type) {
        this.type = type;
    }

    @Override
    public String getTitle() {
        return title;
    }

    @Override
    public void setTitle(String title) {
        this.title = title;
    }

    @Override
    public String getSubtitle() {
        return subtitle;
    }

    @Override
    public void setSubtitle(String subtitle) {
        this.subtitle = subtitle;
    }

    @Override
    public Integer getVersion() {
        return version;
    }

    @Override
    public void setVersion(Integer version) {
        this.version = version;
    }

    @Override
    public List<RequirementControlTemplate> getRequirements() {
        return requirements;
    }

    @Override
    public void setRequirements(List<RequirementControlTemplate> requirements) {
        this.requirements = requirements;
    }

    @Override
    public void addRequirement(Requirement requirement){
        RequirementControlTemplate template = new RequirementControlTemplate(this,requirement);
        requirements.add(template);
        requirement.getTemplates().add(template);
    }

    @Override
    public void removeRequirement(Requirement requirement){
        for(Iterator<RequirementControlTemplate> iterator = requirements.iterator();
        iterator.hasNext();){
            RequirementControlTemplate template = iterator.next();
            if(template.getTemplate().equals(this) && template.getRequirement().equals(requirement)){
                iterator.remove();
                template.getRequirement().getTemplates().remove(template);
                template.setRequirement(null);
                template.setTemplate(null);
            }
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        DocumentTemplate that = (DocumentTemplate) o;

        return type == that.type && title.equals(that.title) && version.equals(that.version);
    }

    @Override
    public int hashCode() {
        int result = type.hashCode();
        result = 31 * result + title.hashCode();
        result = 31 * result + version.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "DocumentTemplate{" +
                "id=" + id +
                ", type=" + type +
                ", title='" + title + '\'' +
                ", subtitle='" + subtitle + '\'' +
                ", version=" + version +
                '}';
    }
}
