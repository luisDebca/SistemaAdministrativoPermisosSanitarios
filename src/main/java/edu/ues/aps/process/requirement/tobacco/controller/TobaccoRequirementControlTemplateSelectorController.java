package edu.ues.aps.process.requirement.tobacco.controller;

import edu.ues.aps.process.requirement.base.service.DocumentTemplateDTOTemplateKindFinderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/requirement-control/tobacco")
public class TobaccoRequirementControlTemplateSelectorController {

    private static final String CONTROL_DOCUMENT_TEMPLATE_FROM_TOBACCO = "templates_from_tobacco";

    private final DocumentTemplateDTOTemplateKindFinderService finderService;

    @Autowired
    public TobaccoRequirementControlTemplateSelectorController(DocumentTemplateDTOTemplateKindFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/template-selector-for-first-request")
    public String showAllDocumentTemplatesFromTobacco(ModelMap model){
        model.addAttribute("templates", finderService.findAllTemplatesFromTobacco());
        return CONTROL_DOCUMENT_TEMPLATE_FROM_TOBACCO;
    }
}
