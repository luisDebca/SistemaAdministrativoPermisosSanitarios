package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.dto.AbstractRequirementDTO;
import edu.ues.aps.process.requirement.base.model.AbstractRequirement;
import edu.ues.aps.process.requirement.base.model.Requirement;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class RequirementRepository implements RequirementDTOFinderRepository, RequirementPersistenceRepository {

    private SessionFactory sessionFactory;

    public RequirementRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractRequirementDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.base.dto.RequirementDTO(" +
                "req.id,req.requirement) " +
                "from Requirement req",AbstractRequirementDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractRequirementDTO> findAllRequirementsFromTemplate(Long id_template) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.base.dto.RequirementDTO(requirement.id,requirement.requirement) " +
                "from Requirement requirement " +
                "inner join requirement.templates document " +
                "inner join document.template template " +
                "where template.id = :id", AbstractRequirementDTO.class)
                .setParameter("id", id_template)
                .getResultList();
    }

    @Override
    public void save(AbstractRequirement requirement) {
        sessionFactory.getCurrentSession().persist(requirement);
    }

    @Override
    public void update(AbstractRequirement requirement) {
        sessionFactory.getCurrentSession().saveOrUpdate(requirement);
    }

    @Override
    public void delete(AbstractRequirement requirement) {
        sessionFactory.getCurrentSession().delete(requirement);
    }

    @Override
    public AbstractRequirement findById(Long id_requirement) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select requirement " +
                "from Requirement requirement " +
                "where requirement.id = :id", Requirement.class)
                .setParameter("id", id_requirement)
                .getSingleResult();
    }
}
