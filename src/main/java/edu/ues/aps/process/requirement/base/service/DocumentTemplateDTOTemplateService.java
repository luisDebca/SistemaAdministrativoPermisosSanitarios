package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.dto.AbstractDocumentTemplateDTO;
import edu.ues.aps.process.requirement.base.dto.EmptyDocumentTemplateDTO;
import edu.ues.aps.process.requirement.base.model.DocumentTemplateType;
import edu.ues.aps.process.requirement.base.repository.DocumentTemplateDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class DocumentTemplateDTOTemplateService implements DocumentTemplateDTOFinderService, DocumentTemplateDTOTemplateKindFinderService {

    private DocumentTemplateDTOFinderRepository finderRepository;

    public DocumentTemplateDTOTemplateService(DocumentTemplateDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractDocumentTemplateDTO find(Long id_template) {
        try {
            return finderRepository.find(id_template);
        } catch (NoResultException e) {
            return new EmptyDocumentTemplateDTO();
        }
    }

    @Override
    public List<AbstractDocumentTemplateDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public List<AbstractDocumentTemplateDTO> findAllTemplatesFromEstablishment() {
        return finderRepository.findAllByType(DocumentTemplateType.ESTABLISHMENT);
    }

    @Override
    public List<AbstractDocumentTemplateDTO> findAllTemplatesFromVehicle() {
        return finderRepository.findAllByType(DocumentTemplateType.VEHICLE);
    }

    @Override
    public List<AbstractDocumentTemplateDTO> findAllTemplatesFromTobacco() {
        return finderRepository.findAllByType(DocumentTemplateType.TOBACCO);
    }
}
