package edu.ues.aps.process.requirement.base.dto;

public class EmptyRequirementDTO implements AbstractRequirementDTO {

    @Override
    public Long getId_requirement() {
        return 0L;
    }

    @Override
    public String getRequirement_requirement() {
        return "Requirement requirement not found";
    }
}
