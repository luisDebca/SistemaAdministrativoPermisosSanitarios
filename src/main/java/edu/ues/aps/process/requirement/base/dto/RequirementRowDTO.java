package edu.ues.aps.process.requirement.base.dto;

import edu.ues.aps.process.requirement.base.model.RequirementStatusType;

public class RequirementRowDTO {

    private Long id_requirement;
    private Long id_template;
    private Long id_document;
    private String row_requirement;
    private String row_observation;
    private RequirementStatusType row_status;

    public RequirementRowDTO() {
        super();
    }

    public RequirementRowDTO(Long id_requirement, Long id_template, String row_requirement, String row_observation, RequirementStatusType row_status) {
        this.id_requirement = id_requirement;
        this.id_template = id_template;
        this.row_requirement = row_requirement;
        this.row_observation = row_observation;
        this.row_status = row_status;
    }

    public RequirementRowDTO(Long id_requirement, Long id_template, Long id_document, String row_requirement, String row_observation, RequirementStatusType row_status) {
        this.id_requirement = id_requirement;
        this.id_template = id_template;
        this.id_document = id_document;
        this.row_requirement = row_requirement;
        this.row_observation = row_observation;
        this.row_status = row_status;
    }

    public Long getId_requirement() {
        return id_requirement;
    }

    public Long getId_template() {
        return id_template;
    }

    public Long getId_document() {
        return id_document;
    }

    public void setId_document(Long id_document) {
        this.id_document = id_document;
    }

    public String getRow_requirement() {
        return row_requirement;
    }

    public String getRow_observation() {
        return row_observation;
    }

    public String getRow_status() {
        return row_status.getRequirementStatus();
    }

    public RequirementStatusType getRow_status_type() {
        return row_status;
    }
}
