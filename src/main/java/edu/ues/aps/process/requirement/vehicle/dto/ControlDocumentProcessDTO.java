package edu.ues.aps.process.requirement.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.VehicleRequestDTO;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class ControlDocumentProcessDTO extends VehicleRequestDTO implements AbstractControlDocumentProcessDTO{

    private LocalDateTime document_create_on;

    public ControlDocumentProcessDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on, LocalDateTime document_create_on) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
        this.document_create_on = document_create_on;
    }

    public String getDocument_create_on() {
        if(document_create_on == null){
            return DatetimeUtil.DATE_NOT_DEFINED;
        }
        return DatetimeUtil.parseDateToLongDateFormat(document_create_on.toLocalDate());
    }
}
