package edu.ues.aps.process.requirement.establishment.service;

import edu.ues.aps.process.requirement.establishment.dto.AbstractEstablishmentControlDocumentDTO;
import edu.ues.aps.process.requirement.establishment.repository.ControlDocumentDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class ControlDocumentFinderService implements ControlDocumentDTOFinderService {

    private ControlDocumentDTOFinderRepository finderRepository;

    public ControlDocumentFinderService(ControlDocumentDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractEstablishmentControlDocumentDTO> findAllRequestWithCompleteRequirements() {
        return finderRepository.findAllRequest(true);
    }

    @Override
    public List<AbstractEstablishmentControlDocumentDTO> findAllRequestWithIncompleteRequirements() {
        return finderRepository.findAllRequest(false);
    }

    @Override
    public List<AbstractEstablishmentControlDocumentDTO> findAllRequestWithAnalysisProcess() {
        return null;
    }
}
