package edu.ues.aps.process.requirement.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleRequestDTO;

public interface AbstractControlDocumentProcessDTO extends AbstractVehicleRequestDTO {

    String getDocument_create_on();
}
