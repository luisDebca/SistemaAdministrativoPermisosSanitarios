package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.repository.RequirementControlTemplateCrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class RequirementControlTemplateService implements RequirementControlTemplatePersistenceService {

    private RequirementControlTemplateCrudRepository crudRepository;

    public RequirementControlTemplateService(RequirementControlTemplateCrudRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @Override
    public void removeRequirementFromTemplate(Long id_template, Long id_requirement) {
        crudRepository.delete(id_template, id_requirement);
    }
}
