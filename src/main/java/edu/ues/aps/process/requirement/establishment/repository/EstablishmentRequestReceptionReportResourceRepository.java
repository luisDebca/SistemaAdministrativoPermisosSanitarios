package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.process.requirement.base.dto.AbstractRequestReceptionReportDTO;

import javax.persistence.NoResultException;

public interface EstablishmentRequestReceptionReportResourceRepository {

    AbstractRequestReceptionReportDTO findReceptionReportResources(Long id_caseFile) throws NoResultException;
}
