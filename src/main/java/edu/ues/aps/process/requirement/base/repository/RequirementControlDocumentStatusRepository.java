package edu.ues.aps.process.requirement.base.repository;

public interface RequirementControlDocumentStatusRepository {

    boolean isCompleted(Long id_document);

    void updateStatus(Long id_document);
}
