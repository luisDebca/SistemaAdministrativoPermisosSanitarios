package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.process.requirement.establishment.dto.AbstractEstablishmentControlDocumentDTO;

import java.util.List;

public interface ControlDocumentDTOFinderRepository {

    List<AbstractEstablishmentControlDocumentDTO> findAllRequest(boolean document_status);
}
