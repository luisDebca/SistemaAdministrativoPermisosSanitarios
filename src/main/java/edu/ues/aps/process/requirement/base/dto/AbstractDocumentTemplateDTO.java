package edu.ues.aps.process.requirement.base.dto;

public interface AbstractDocumentTemplateDTO {

    Long getId_template();

    String getType();

    String getTitle();

    String getSubtitle();

    Integer getVersion();
}
