package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.model.RequirementRow;

import java.util.List;

public interface RequirementRowBuilderService {

    List<RequirementRow> buildEmptyRequirementRowsFromTemplate(Long id_template);
}
