package edu.ues.aps.process.requirement.base.dto;

public class RequirementDTO implements AbstractRequirementDTO {

    private Long id_requirement;
    private String requirement_requirement;

    public RequirementDTO(Long id_requirement, String requirement_requirement) {
        this.id_requirement = id_requirement;
        this.requirement_requirement = requirement_requirement;
    }

    public Long getId_requirement() {
        return id_requirement;
    }

    public String getRequirement_requirement() {
        return requirement_requirement;
    }
}
