package edu.ues.aps.process.requirement.base.controller;

import edu.ues.aps.process.requirement.base.model.AbstractDocumentTemplate;
import edu.ues.aps.process.requirement.base.model.DocumentTemplate;
import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;
import edu.ues.aps.process.requirement.base.service.DocumentTemplateFinderService;
import edu.ues.aps.process.requirement.base.service.DocumentTemplatePersistenceService;
import edu.ues.aps.process.requirement.base.service.RequirementDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/document-template")
public class DocumentTemplatePersistenceController {

    private static final Logger logger = LogManager.getLogger(DocumentTemplatePersistenceController.class);

    private static final String DOCUMENT_TEMPLATE_NEW = "new_template";
    private static final String DOCUMENT_TEMPLATE_EDIT = "edit_template";
    private static final String DOCUMENT_TEMPLATE_NEW_VERSION = "edit_template";

    private final RequirementDTOFinderService requirementFinderService;
    private final DocumentTemplatePersistenceService persistenceService;
    private final DocumentTemplateFinderService finderService;

    public DocumentTemplatePersistenceController(DocumentTemplatePersistenceService persistenceService, DocumentTemplateFinderService finderService, RequirementDTOFinderService requirementFinderService) {
        this.persistenceService = persistenceService;
        this.finderService = finderService;
        this.requirementFinderService = requirementFinderService;
    }

    @GetMapping("/new")
    public String createNewDocumentTemplate(ModelMap model) {
        logger.info("GET:: Show new document template form");
        model.addAttribute("template", new DocumentTemplate());
        model.addAttribute("requirements", requirementFinderService.findAll());
        return DOCUMENT_TEMPLATE_NEW;
    }

    @PostMapping("/new")
    public String saveNewDocumentTemplate(@ModelAttribute DocumentTemplate template) {
        logger.info("POST:: Save new document template");
        persistenceService.save(template);
        return "redirect:/document-template/all";
    }

    @GetMapping("/{id_template}/edit")
    public String editDocumentTemplate(@PathVariable Long id_template, ModelMap model) {
        logger.info("GET:: Show document template {} edit form", id_template);
        AbstractDocumentTemplate document = finderService.findById(id_template);
        List<Long> id_requirements = new ArrayList<>();
        for(RequirementControlTemplate template : document.getRequirements()){
            id_requirements.add(template.getRequirement().getId());
        }
        model.addAttribute("template", document);
        model.addAttribute("requirements", requirementFinderService.findAll());
        model.addAttribute("ids",id_requirements);
        return DOCUMENT_TEMPLATE_EDIT;
    }

    @PostMapping("/{id_template}/edit")
    public String updateDocumentTemplate(@ModelAttribute DocumentTemplate template, @PathVariable Long id_template) {
        logger.info("POST:: Update document template: {}", id_template);
        persistenceService.update(template);
        return "redirect:/document-template/all";
    }

    @GetMapping("/{id_template}/new-version")
    public String newVersionControlTemplateFromExistentTemplate(@PathVariable Long id_template, ModelMap model) {
        logger.info("GET:: Show new document template {} version form", id_template);
        AbstractDocumentTemplate template = finderService.findById(id_template);
        template.setId(0L);
        template.setVersion(template.getVersion() + 1);
        model.addAttribute("template", template);
        return DOCUMENT_TEMPLATE_NEW_VERSION;
    }

    @PostMapping("/{id_template}/new-version")
    public String saveNewControlTemplateVersion(@ModelAttribute DocumentTemplate template, @PathVariable Long id_template) {
        logger.info("POST:: Save new document template version: {}", id_template);
        persistenceService.save(template);
        return "redirect:/document-template/all";
    }

    @GetMapping("/{id_document}/delete")
    public String deleteDocumentTemplate(@PathVariable Long id_document){
        logger.info("GET:: Delete document template {}",id_document);
        persistenceService.delete(id_document);
        return "redirect:/document-template/all";
    }

}
