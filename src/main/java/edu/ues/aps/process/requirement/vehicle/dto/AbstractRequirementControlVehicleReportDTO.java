package edu.ues.aps.process.requirement.vehicle.dto;

import edu.ues.aps.process.requirement.base.dto.AbstractRequirementControlDocumentReportDTO;

public interface AbstractRequirementControlVehicleReportDTO extends AbstractRequirementControlDocumentReportDTO {

    Long getVehicle_count();

    String getVehicle_license();

    String getVehicle_type();

    void setVehicle_type(String vehicle_type);
}
