package edu.ues.aps.process.requirement.base.async;

import edu.ues.aps.process.requirement.base.dto.AbstractRequirementDTO;
import edu.ues.aps.process.requirement.base.service.RequirementDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class RequirementFinderAsyncController {

    private static final Logger logger = LogManager.getLogger(RequirementFinderAsyncController.class);

    private RequirementDTOFinderService finderService;

    public RequirementFinderAsyncController(RequirementDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/document-template/requirement/all", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractRequirementDTO> findAllRequirementsFromTemplate(@RequestParam("id") long id_template) {
        logger.info("GET:: Find document template {} requirements", id_template);
        return finderService.findAllRequirementsFromTemplate(id_template);
    }
}
