package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;

import java.util.List;

public interface RequirementRowDTOFinderService {

    List<RequirementRowDTO> findAllFromDocument(Long id_document);

    List<RequirementRowDTO> findAllFromBunch(Long id_bunch);
}
