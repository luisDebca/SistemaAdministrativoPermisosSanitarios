package edu.ues.aps.process.requirement.establishment.service;

import edu.ues.aps.process.requirement.establishment.dto.AbstractEstablishmentControlDocumentDTO;

import java.util.List;

public interface ControlDocumentDTOFinderService {

    List<AbstractEstablishmentControlDocumentDTO> findAllRequestWithCompleteRequirements();

    List<AbstractEstablishmentControlDocumentDTO> findAllRequestWithIncompleteRequirements();

    List<AbstractEstablishmentControlDocumentDTO> findAllRequestWithAnalysisProcess();
}
