package edu.ues.aps.process.requirement.base.dto;

import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.process.requirement.base.model.RequirementStatusType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class RequirementControlDocumentReportDTO implements AbstractRequirementControlDocumentReportDTO {

    private String owner_name;
    private Integer owner_class;
    private CertificationType request_certification_type;
    private String request_certification;
    private LocalDateTime document_created_on;
    private String document_template_title;
    private String document_template_subtitle;
    private String requirement_row_requirement;
    private RequirementStatusType requirement_row_status;
    private String requirement_row_observation;
    private boolean requirements_completed;

    private String approved_name_by;
    private String approved_type_by;
    private String signed_name_by;
    private String user_name;

    public RequirementControlDocumentReportDTO(String owner_name, Integer owner_class, CertificationType request_certification_type, boolean requirements_completed, LocalDateTime document_created_on, String document_template_title, String document_template_subtitle, String requirement_row_requirement, RequirementStatusType requirement_row_status, String requirement_row_observation) {
        this.owner_name = owner_name;
        this.owner_class = owner_class;
        this.request_certification_type = request_certification_type;
        this.requirements_completed = requirements_completed;
        this.document_created_on = document_created_on;
        this.document_template_title = document_template_title;
        this.document_template_subtitle = document_template_subtitle;
        this.requirement_row_requirement = requirement_row_requirement;
        this.requirement_row_status = requirement_row_status;
        this.requirement_row_observation = requirement_row_observation;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public Integer getOwner_class() {
        return owner_class;
    }

    public String getRequest_certification_type() {
        if (request_certification_type == null) {
            return CertificationType.INVALID.getCertificationType();
        }
        if(request_certification != null){
            return request_certification;
        }
        return request_certification_type.getCertificationType();
    }

    public void setRequest_certification(String request_certification) {
        this.request_certification = request_certification;
    }

    public boolean isRequirements_completed() {
        return requirements_completed;
    }

    public String getDocument_created_on() {
        if (document_created_on != null) {
            return DatetimeUtil.parseDateToShortFormat(document_created_on.toLocalDate());
        } else {
            return DatetimeUtil.DATE_NOT_DEFINED;
        }
    }

    public String getDocument_template_title() {
        return document_template_title;
    }

    public String getDocument_template_subtitle() {
        return document_template_subtitle;
    }

    public String getRequirement_row_requirement() {
        return requirement_row_requirement;
    }

    public String getRequirement_row_status() {
        if (requirement_row_status == null) {
            return RequirementStatusType.INVALID.getRequirementStatus();
        }
        return requirement_row_status.getRequirementStatus();
    }

    public String getRequirement_row_observation() {
        return requirement_row_observation;
    }

    public String getApproved_name_by() {
        return approved_name_by;
    }

    public String getApproved_type_by() {
        return approved_type_by;
    }

    public String getSigned_name_by() {
        return signed_name_by;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setApproved_name_by(String approved_name_by) {
        this.approved_name_by = approved_name_by;
    }

    public void setApproved_type_by(String approved_type_by) {
        this.approved_type_by = approved_type_by;
    }

    public void setSigned_name_by(String signed_name_by) {
        this.signed_name_by = signed_name_by;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
