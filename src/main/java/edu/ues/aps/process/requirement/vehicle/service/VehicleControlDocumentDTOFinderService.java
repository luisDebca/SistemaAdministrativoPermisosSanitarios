package edu.ues.aps.process.requirement.vehicle.service;

import edu.ues.aps.process.requirement.vehicle.dto.AbstractControlDocumentProcessDTO;

import java.util.List;

public interface VehicleControlDocumentDTOFinderService {

    List<AbstractControlDocumentProcessDTO> findAll(boolean document_status);
}
