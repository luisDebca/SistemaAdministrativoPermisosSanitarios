package edu.ues.aps.process.requirement.establishment.controller;

public class UnknownOwnerPathVariableException extends RuntimeException {

    public UnknownOwnerPathVariableException(String message) {
        super(message);
    }
}
