package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.process.requirement.establishment.dto.RequirementDocumentInfoDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class RequirementDocumentInfoDTORepository implements RequirementDocumentInfoDTOFinderRepository {

    private SessionFactory sessionFactory;

    public RequirementDocumentInfoDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public RequirementDocumentInfoDTO findDocumentInfoById(Long id_casefile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.establishment.dto.RequirementDocumentInfoDTO(" +
                "owner.name,owner.nit,establishment.name," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,establishment.type.type,casefile.certification_type,document.acceptance_document_created_on,document.control_document_created_on," +
                "document.status," +
                "(select count(row) from RequirementControlDocument document " +
                "inner join document.requirementRows row where document.id = :id and (row.status = 'APPROVED' or row.status = 'NOT_APPLY'))," +
                "(select count(row) from RequirementControlDocument document " +
                "inner join document.requirementRows row where document.id = :id and row.status = 'FAULT')) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.controlDocument document " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.address address " +
                "inner join establishment.owner owner " +
                "where casefile.id = :id", RequirementDocumentInfoDTO.class)
                .setParameter("id", id_casefile)
                .getSingleResult();
    }
}
