package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.requirement.base.dto.AbstractRequestReceptionReportDTO;
import edu.ues.aps.process.requirement.establishment.dto.AbstractEstablishmentControlDocumentDTO;
import edu.ues.aps.process.requirement.establishment.dto.AbstractRequirementControlEstablishmentReportDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class ControlDocumentDTORepository implements ControlDocumentDTOFinderRepository, EstablishmentControlDocumentReportRepository, EstablishmentRequestReceptionReportResourceRepository {

    private SessionFactory sessionFactory;

    public ControlDocumentDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractEstablishmentControlDocumentDTO> findAllRequest(boolean document_status) {
        return sessionFactory.getCurrentSession().createQuery("select new edu.ues.aps.process.requirement.establishment.dto.EstablishmentControlDocumentDTO(ce.id ,owner.id,establishment.id,concat(concat(ce.request_folder,'-'),ce.request_year),concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year)," +
                "establishment.name,establishment.type_detail,etype.type,section.type,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,ucsf.name,sibasi.zone,ce.certification_type,ce.creation_date,owner.name)" +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "left join establishment.type etype " +
                "left join etype.section section " +
                "left join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join ce.controlDocument document " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "and document.status = :doc", AbstractEstablishmentControlDocumentDTO.class)
                .setParameter("id", ProcessIdentifier.REQUIREMENT_REVIEW)
                .setParameter("doc", document_status)
                .getResultList();
    }

    @Override
    public List<AbstractRequirementControlEstablishmentReportDTO> findDocumentReportResources(Long id_caseFile) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.establishment.dto.RequirementControlEstablishmentReportDTO(" +
                "owner.name,owner.class,casefile.certification_type,document.status,document.control_document_created_on," +
                "template.title,template.subtitle,requirement.requirement,rows.status,rows.observation,establishment.name,establishment.NIT,establishment.type_detail," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)) " +
                "from CasefileEstablishment casefile " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.owner owner " +
                "inner join establishment.address address " +
                "inner join casefile.controlDocument document " +
                "inner join document.requirementRows rows " +
                "inner join rows.template.requirement requirement " +
                "inner join rows.template.template template " +
                "where casefile.id = :id", AbstractRequirementControlEstablishmentReportDTO.class)
                .setParameter("id", id_caseFile)
                .getResultList();
    }

    @Override
    public AbstractRequestReceptionReportDTO findReceptionReportResources(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.establishment.dto.EstablishmentRequestReceptionReportDTO(" +
                "document.acceptance_document_created_on,establishment.name,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name))" +
                "from CasefileEstablishment casefile " +
                "inner join casefile.controlDocument document " +
                "inner join casefile.establishment establishment " +
                "inner join establishment.address address " +
                "where casefile.id = :id", AbstractRequestReceptionReportDTO.class)
                .setParameter("id", id_caseFile)
                .getSingleResult();
    }
}
