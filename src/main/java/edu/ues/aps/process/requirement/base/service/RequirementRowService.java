package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;
import edu.ues.aps.process.requirement.base.model.RequirementControlDocument;
import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;
import edu.ues.aps.process.requirement.base.model.RequirementRow;
import edu.ues.aps.process.requirement.base.repository.RequirementControlTemplateCrudRepository;
import edu.ues.aps.process.requirement.base.repository.RequirementRowPersistenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

@Service
@Transactional
public class RequirementRowService implements RequirementRowBuilderService, RequirementRowPersistenceService {

    private RequirementControlTemplateCrudRepository crudRepository;
    private RequirementRowPersistenceRepository persistenceRepository;

    public RequirementRowService(RequirementControlTemplateCrudRepository crudRepository, RequirementRowPersistenceRepository persistenceRepository) {
        this.crudRepository = crudRepository;
        this.persistenceRepository = persistenceRepository;
    }

    @Override
    public List<RequirementRow> buildEmptyRequirementRowsFromTemplate(Long id_template) {
        List<RequirementRow> rows = new ArrayList<>();
        for (RequirementControlTemplate controlTemplate : crudRepository.findAllFromTemplate(id_template)) {
            rows.add(new RequirementRow(new RequirementControlDocument(), controlTemplate));
        }
        return rows;
    }

    @Override
    public void updateRequirementRows(List<RequirementRowDTO> rows) {
        for (RequirementRowDTO row : rows) {
            persistenceRepository.updateRequirementRow(row);
        }
    }
}
