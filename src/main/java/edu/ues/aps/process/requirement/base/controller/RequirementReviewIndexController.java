package edu.ues.aps.process.requirement.base.controller;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/requirement-control")
public class RequirementReviewIndexController {

    private static final Logger logger = LogManager.getLogger(RequirementReviewIndexController.class);

    private static final String REQUIREMENT_REVIEW_INDEX = "requirement_review_index";

    @GetMapping({"/", "/index"})
    public String showIndex(ModelMap model) {
        logger.info("GET:: Show requirement control process index");
        model.addAttribute("process", ProcessIdentifier.REQUIREMENT_REVIEW);
        return REQUIREMENT_REVIEW_INDEX;
    }
}
