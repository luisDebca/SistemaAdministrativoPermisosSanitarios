package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.model.AbstractRequirement;
import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;

import java.util.List;

public interface RequirementFinderService {

    List<RequirementControlTemplate> findAll();

    AbstractRequirement findById(Long id_requirement);
}
