package edu.ues.aps.process.requirement.vehicle.dto;

import edu.ues.aps.process.requirement.base.dto.RequestReceptionReportDTO;

import java.time.LocalDateTime;

public class VehicleRequestReceptionReportDTO extends RequestReceptionReportDTO implements AbstractVehicleRequestReceptionReportDTO {

    private Long vehicle_count;
    private String vehicle_licenses;
    private String vehicle_address;

    public VehicleRequestReceptionReportDTO(LocalDateTime acceptance_created_on, Long vehicle_count, String vehicle_licenses, String vehicle_address) {
        super(acceptance_created_on);
        this.vehicle_count = vehicle_count;
        this.vehicle_licenses = vehicle_licenses;
        this.vehicle_address = vehicle_address;
    }

    public Long getVehicle_count() {
        return vehicle_count;
    }

    public String getVehicle_licenses() {
        return vehicle_licenses;
    }

    public void setVehicle_licenses(String vehicle_licenses) {
        this.vehicle_licenses = vehicle_licenses;
    }

    public String getVehicle_address() {
        return vehicle_address;
    }
}
