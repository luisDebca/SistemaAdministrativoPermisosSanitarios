package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.model.AbstractDocumentTemplate;
import edu.ues.aps.process.requirement.base.model.DocumentTemplate;
import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;
import edu.ues.aps.process.requirement.base.repository.DocumentTemplateCrudRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class DocumentTemplateService implements DocumentTemplatePersistenceService, DocumentTemplateFinderService {

    private DocumentTemplateCrudRepository crudRepository;

    public DocumentTemplateService(DocumentTemplateCrudRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @Override
    public AbstractDocumentTemplate findById(Long id_template) {
        return crudRepository.findById(id_template);
    }

    @Override
    public void save(AbstractDocumentTemplate template) {
        DocumentTemplate document = new DocumentTemplate();
        document.setTitle(template.getTitle());
        document.setSubtitle(template.getSubtitle());
        document.setType(template.getType());
        document.setVersion(DocumentTemplate.DEFAULT_VERSION);
        crudRepository.save(document);
        for (RequirementControlTemplate requirement : template.getRequirements()) {
            document.addRequirement(crudRepository.findRequirement(requirement.getRequirement().getId()));
        }
    }

    @Override
    public void update(AbstractDocumentTemplate updated) {
        AbstractDocumentTemplate document = crudRepository.findById(updated.getId());
        document.setTitle(updated.getTitle());
        document.setSubtitle(updated.getSubtitle());
        document.setType(updated.getType());
        for (RequirementControlTemplate fromUpdate : updated.getRequirements()) {
            boolean is_included = false;
            for (RequirementControlTemplate fromPersistent : document.getRequirements()) {
                if (fromUpdate.getRequirement().equals(fromPersistent.getRequirement()))
                    is_included = true;
            }
            if (!is_included) {
                document.addRequirement(crudRepository.findRequirement(fromUpdate.getRequirement().getId()));
            }
        }
        for (RequirementControlTemplate fromPersistent : document.getRequirements()) {
            boolean is_included = false;
            for (RequirementControlTemplate fromUpdate : updated.getRequirements()) {
                if (fromPersistent.getRequirement().equals(fromUpdate.getRequirement()))
                    is_included = true;
            }
            if (!is_included) {
                document.removeRequirement(crudRepository.findRequirement(fromPersistent.getRequirement().getId()));
            }
        }
    }

    @Override
    public void delete(Long id_document) {
        AbstractDocumentTemplate document = crudRepository.findById(id_document);
        crudRepository.delete(document);
    }

}
