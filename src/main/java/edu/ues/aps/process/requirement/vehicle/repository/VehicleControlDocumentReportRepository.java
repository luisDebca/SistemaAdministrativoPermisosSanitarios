package edu.ues.aps.process.requirement.vehicle.repository;

import edu.ues.aps.process.requirement.vehicle.dto.AbstractRequirementControlVehicleReportDTO;

import java.util.List;

public interface VehicleControlDocumentReportRepository {

    List<AbstractRequirementControlVehicleReportDTO> findVehicleDocumentReportResources(Long id_bunch);
}
