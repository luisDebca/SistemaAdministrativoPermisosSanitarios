package edu.ues.aps.process.requirement.establishment.service;

import edu.ues.aps.process.requirement.establishment.dto.AbstractRequirementDocumentInfoDTO;
import edu.ues.aps.process.requirement.establishment.dto.EmptyRequirementDocumentInfoDTO;
import edu.ues.aps.process.requirement.establishment.repository.RequirementDocumentInfoDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional(readOnly = true)
public class RequirementDocumentDTOService implements RequirementDocumentInfoDTOFinderService {

    private RequirementDocumentInfoDTOFinderRepository finderRepository;

    public RequirementDocumentDTOService(RequirementDocumentInfoDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractRequirementDocumentInfoDTO findDocumentInfoById(Long id_casefile) {
        try {
            return finderRepository.findDocumentInfoById(id_casefile);
        } catch (NoResultException e) {
            return new EmptyRequirementDocumentInfoDTO();
        }
    }
}
