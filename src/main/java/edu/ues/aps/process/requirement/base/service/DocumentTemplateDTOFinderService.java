package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.dto.AbstractDocumentTemplateDTO;
import edu.ues.aps.process.requirement.base.model.AbstractDocumentTemplate;

import java.util.List;

public interface DocumentTemplateDTOFinderService {

    AbstractDocumentTemplateDTO find(Long id_template);

    List<AbstractDocumentTemplateDTO> findAll();
}
