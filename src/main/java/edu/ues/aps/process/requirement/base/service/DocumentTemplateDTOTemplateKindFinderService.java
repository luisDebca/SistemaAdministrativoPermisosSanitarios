package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.dto.AbstractDocumentTemplateDTO;

import java.util.List;

public interface DocumentTemplateDTOTemplateKindFinderService {

    List<AbstractDocumentTemplateDTO> findAllTemplatesFromEstablishment();

    List<AbstractDocumentTemplateDTO> findAllTemplatesFromVehicle();

    List<AbstractDocumentTemplateDTO> findAllTemplatesFromTobacco();
}
