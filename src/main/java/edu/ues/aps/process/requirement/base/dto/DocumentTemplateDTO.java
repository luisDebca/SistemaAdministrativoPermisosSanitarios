package edu.ues.aps.process.requirement.base.dto;

import edu.ues.aps.process.requirement.base.model.DocumentTemplateType;

public class DocumentTemplateDTO implements AbstractDocumentTemplateDTO {

    private Long id_template;
    private DocumentTemplateType type;
    private String title;
    private String subtitle;
    private Integer version;

    public DocumentTemplateDTO(Long id_template, DocumentTemplateType type, String title, String subtitle, Integer version) {
        this.id_template = id_template;
        this.type = type;
        this.title = title;
        this.subtitle = subtitle;
        this.version = version;
    }

    public Long getId_template() {
        return id_template;
    }

    public String getType() {
        return type.getDocumentTemplateType();
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public Integer getVersion() {
        return version;
    }
}
