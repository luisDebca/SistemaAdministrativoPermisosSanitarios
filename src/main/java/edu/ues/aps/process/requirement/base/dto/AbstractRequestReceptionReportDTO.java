package edu.ues.aps.process.requirement.base.dto;

import edu.ues.aps.process.base.model.ClientType;

public interface AbstractRequestReceptionReportDTO {

    String getAcceptance_created_on();

    String getClient_name();

    String getClient_dui();

    String getClient_resident_card();

    String getClient_passport();

    String getClient_type();

    String getUser_name();

    void setUser_name(String user_name);

    void setClient_name(String client_name);

    void setClient_dui(String client_dui);

    void setClient_resident_card(String client_resident_card);

    void setClient_passport(String client_passport);

    void setClient_type(ClientType client_type);
}
