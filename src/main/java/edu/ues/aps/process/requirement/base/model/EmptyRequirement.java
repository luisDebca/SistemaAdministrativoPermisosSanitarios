package edu.ues.aps.process.requirement.base.model;

import java.util.Collections;
import java.util.List;

public class EmptyRequirement implements AbstractRequirement {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getRequirement() {
        return "Requirement not found";
    }

    @Override
    public List<RequirementControlTemplate> getTemplates() {
        return Collections.emptyList();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setRequirement(String requirement) {

    }

    @Override
    public void setTemplates(List<RequirementControlTemplate> templates) {

    }

    @Override
    public String toString() {
        return "EmptyRequirement{}";
    }
}
