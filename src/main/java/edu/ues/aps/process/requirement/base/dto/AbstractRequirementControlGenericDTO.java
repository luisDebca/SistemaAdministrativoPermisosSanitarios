package edu.ues.aps.process.requirement.base.dto;

public interface AbstractRequirementControlGenericDTO {

    Long getId_casefile();

    String getRequest_folder();

    String getSubject();

    String getOwner_name();

    String getSubject_address();

    boolean isDocument_status();

    void setSubject(String subject);

    void setOwner_name(String owner_name);

    void setSubject_address(String subject_address);
}
