package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.model.AbstractRequirement;
import edu.ues.aps.process.requirement.base.model.EmptyRequirement;
import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;
import edu.ues.aps.process.requirement.base.repository.RequirementPersistenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional
public class RequirementService implements RequirementPersistenceService, RequirementFinderService {

    private RequirementPersistenceRepository crudRepository;

    public RequirementService(RequirementPersistenceRepository crudRepository) {
        this.crudRepository = crudRepository;
    }

    @Override
    public List<RequirementControlTemplate> findAll() {
        return null;
    }

    @Override
    public AbstractRequirement findById(Long id_requirement) {
        try {
            return crudRepository.findById(id_requirement);
        } catch (NoResultException e) {
            return new EmptyRequirement();
        }
    }

    @Override
    public void save(AbstractRequirement requirement) {
        crudRepository.save(requirement);
    }

    @Override
    public void update(AbstractRequirement requirement) {
        crudRepository.update(requirement);
    }

    @Override
    public void delete(Long id_requirement) {
        AbstractRequirement requirement = crudRepository.findById(id_requirement);
        crudRepository.delete(requirement);
    }
}

