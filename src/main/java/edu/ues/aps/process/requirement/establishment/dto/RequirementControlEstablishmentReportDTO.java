package edu.ues.aps.process.requirement.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.process.requirement.base.dto.RequirementControlDocumentReportDTO;
import edu.ues.aps.process.requirement.base.model.RequirementStatusType;

import java.time.LocalDateTime;

public class RequirementControlEstablishmentReportDTO extends RequirementControlDocumentReportDTO implements AbstractRequirementControlEstablishmentReportDTO {

    private String establishment_name;
    private String establishment_nit;
    private String establishment_type_detail;
    private String establishment_address;

    public RequirementControlEstablishmentReportDTO(String owner_name, Integer owner_class, CertificationType request_certification_type, boolean requirements_completed, LocalDateTime document_created_on, String document_template_title, String document_template_subtitle, String requirement_row_requirement, RequirementStatusType requirement_row_status, String requirement_row_observation, String establishment_name, String establishment_nit, String establishment_type_detail, String establishment_address) {
        super(owner_name, owner_class, request_certification_type, requirements_completed, document_created_on, document_template_title, document_template_subtitle, requirement_row_requirement, requirement_row_status, requirement_row_observation);
        this.establishment_name = establishment_name;
        this.establishment_nit = establishment_nit;
        this.establishment_type_detail = establishment_type_detail;
        this.establishment_address = establishment_address;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_nit() {
        return establishment_nit;
    }

    public String getEstablishment_type_detail() {
        return establishment_type_detail;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }
}
