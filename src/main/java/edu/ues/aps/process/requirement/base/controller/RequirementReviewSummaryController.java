package edu.ues.aps.process.requirement.base.controller;

import edu.ues.aps.process.base.service.BaseFinderService;
import edu.ues.aps.process.requirement.base.dto.EmptyDocumentTemplateDTO;
import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;
import edu.ues.aps.process.requirement.base.model.RequirementStatusType;
import edu.ues.aps.process.requirement.base.service.DocumentTemplateDTOFinderService;
import edu.ues.aps.process.requirement.base.service.RequirementRowDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.util.List;

@Controller
public class RequirementReviewSummaryController {

    private static final Logger logger = LogManager.getLogger(RequirementReviewSummaryController.class);

    private static final String SUMMARY = "requirement_review_summary";

    private final BaseFinderService baseFinderService;
    private final RequirementRowDTOFinderService rowFinderService;
    private final DocumentTemplateDTOFinderService templateFinderService;

    public RequirementReviewSummaryController(RequirementRowDTOFinderService rowFinderService, DocumentTemplateDTOFinderService templateFinderService, BaseFinderService baseFinderService) {
        this.rowFinderService = rowFinderService;
        this.templateFinderService = templateFinderService;
        this.baseFinderService = baseFinderService;
    }

    @GetMapping("/requirement-control/request/{id_request}/summary")
    public String showProcessSummary(@PathVariable Long id_request, ModelMap model){
        logger.info("GET:: Show request {} requirement review summary.");
        List<RequirementRowDTO> rows = rowFinderService.findAllFromDocument(id_request);
        if(rows.isEmpty()){
            model.addAttribute("template",new EmptyDocumentTemplateDTO());
        }else {
            model.addAttribute("template", templateFinderService.find(rows.get(0).getId_template()));
        }
        model.addAttribute("base",baseFinderService.findById(id_request));
        model.addAttribute("accepted", isValidRequirementDocument(rows));
        model.addAttribute("rows", rows);
        return SUMMARY;
    }

    private boolean isValidRequirementDocument(List<RequirementRowDTO> rows){
        for (RequirementRowDTO dto:rows) {
            if(dto.getRow_status_type().equals(RequirementStatusType.FAULT))
                return false;
        }
        return true;
    }
}
