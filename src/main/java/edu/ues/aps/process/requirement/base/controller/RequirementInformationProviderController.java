package edu.ues.aps.process.requirement.base.controller;

import edu.ues.aps.process.requirement.base.service.RequirementDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/requirement")
public class RequirementInformationProviderController {

    private static final Logger logger = LogManager.getLogger(RequirementInformationProviderController.class);

    private static final String REQUIREMENT_LIST = "requirement_list";

    private final RequirementDTOFinderService finderService;

    public RequirementInformationProviderController(RequirementDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showAllRequirements(ModelMap model){
        logger.info("GET:: Show all requirements");
        model.addAttribute("requirements",finderService.findAll());
        return REQUIREMENT_LIST;
    }
}
