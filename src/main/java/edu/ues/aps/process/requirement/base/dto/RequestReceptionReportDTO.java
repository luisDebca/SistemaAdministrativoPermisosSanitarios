package edu.ues.aps.process.requirement.base.dto;

import edu.ues.aps.process.base.model.ClientType;
import edu.ues.aps.process.requirement.base.dto.AbstractRequestReceptionReportDTO;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class RequestReceptionReportDTO implements AbstractRequestReceptionReportDTO {

    private LocalDateTime acceptance_created_on;
    private String client_name;
    private String client_dui;
    private String client_resident_card;
    private String client_passport;
    private ClientType client_type;
    private String user_name;

    public RequestReceptionReportDTO() {
    }

    public RequestReceptionReportDTO(LocalDateTime acceptance_created_on) {
        this.acceptance_created_on = acceptance_created_on;
    }

    public String getAcceptance_created_on() {
        return DatetimeUtil.parseDatetimeToVerboseDate(acceptance_created_on);
    }

    public String getClient_name() {
        return client_name;
    }

    public String getClient_dui() {
        return client_dui;
    }

    public String getClient_resident_card() {
        return client_resident_card;
    }

    public String getClient_passport() {
        return client_passport;
    }

    public String getClient_type() {
        if (client_type != null) {
            return client_type.getClientType();
        } else {
            return ClientType.INVALID.getClientType();
        }
    }

    public void setClient_name(String client_name) {
        this.client_name = client_name;
    }

    public void setClient_dui(String client_dui) {
        this.client_dui = client_dui;
    }

    public void setClient_resident_card(String client_resident_card) {
        this.client_resident_card = client_resident_card;
    }

    public void setClient_passport(String client_passport) {
        this.client_passport = client_passport;
    }

    public void setClient_type(ClientType client_type) {
        this.client_type = client_type;
    }

    public String getUser_name() {
        return user_name;
    }

    public void setUser_name(String user_name) {
        this.user_name = user_name;
    }
}
