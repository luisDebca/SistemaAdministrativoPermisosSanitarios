package edu.ues.aps.process.requirement.base.model;

import edu.ues.aps.process.base.model.Casefile;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "REQUIREMENT_CONTROL_DOCUMENT")
public class RequirementControlDocument implements AbstractRequirementControlDocument {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "status")
    private Boolean status;

    @Column(name = "control_document_created_on")
    private LocalDateTime control_document_created_on;

    @Column(name = "acceptance_created_on")
    private LocalDateTime acceptance_document_created_on;

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    private Casefile casefile;

    @OneToMany(mappedBy = "document", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RequirementRow> requirementRows = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public Boolean getStatus() {
        return status;
    }

    @Override
    public void setStatus(Boolean status) {
        this.status = status;
    }

    @Override
    public LocalDateTime getControl_document_created_on() {
        return control_document_created_on;
    }

    @Override
    public void setControl_document_created_on(LocalDateTime control_document_created_on) {
        this.control_document_created_on = control_document_created_on;
    }

    @Override
    public LocalDateTime getAcceptance_document_created_on() {
        return acceptance_document_created_on;
    }

    @Override
    public void setAcceptance_document_created_on(LocalDateTime acceptance_document_created_on) {
        this.acceptance_document_created_on = acceptance_document_created_on;
    }

    @Override
    public List<RequirementRow> getRequirementRows() {
        return requirementRows;
    }

    @Override
    public void setRequirementRows(List<RequirementRow> requirementRows) {
        this.requirementRows = requirementRows;
    }

    @Override
    public Casefile getCasefile() {
        return casefile;
    }

    @Override
    public void setCasefile(Casefile casefile) {
        this.casefile = casefile;
    }

    @Override
    public void addRequirementRow(RequirementRow row) {
        requirementRows.add(row);
        row.setDocument(this);
    }

    public void addRequirementRow(RequirementControlTemplate template, RequirementStatusType status, String observation) {
        RequirementRow row = new RequirementRow(this, template);
        row.setStatus(status);
        row.setObservation(observation);
        requirementRows.add(row);
        template.getRequirementRows().add(row);
    }

    @Override
    public void removeRequirementRow(RequirementRow row) {
        requirementRows.remove(row);
        row.setDocument(null);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequirementControlDocument that = (RequirementControlDocument) o;

        return status.equals(that.status) && control_document_created_on.equals(that.control_document_created_on);
    }

    @Override
    public int hashCode() {
        int result = status.hashCode();
        result = 31 * result + control_document_created_on.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "RequirementControlDocument{" +
                "id=" + id +
                ", status=" + status +
                ", control_document_created_on=" + control_document_created_on +
                ", acceptance_document_created_on=" + acceptance_document_created_on +
                '}';
    }
}
