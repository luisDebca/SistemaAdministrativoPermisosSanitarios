package edu.ues.aps.process.requirement.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;

import java.time.LocalDateTime;
import java.util.List;

public interface AbstractRequirementControlDocument {

    Long getId();

    void setId(Long id);

    Boolean getStatus();

    void setStatus(Boolean status);

    LocalDateTime getControl_document_created_on();

    void setControl_document_created_on(LocalDateTime control_document_created_on);

    LocalDateTime getAcceptance_document_created_on();

    void setAcceptance_document_created_on(LocalDateTime acceptance_document_created_on);

    List<RequirementRow> getRequirementRows();

    void setRequirementRows(List<RequirementRow> requirementRows);

    AbstractCasefile getCasefile();

    void setCasefile(Casefile casefile);

    void addRequirementRow(RequirementRow row);

    void addRequirementRow(RequirementControlTemplate template, RequirementStatusType status, String observation);

    void removeRequirementRow(RequirementRow row);
}
