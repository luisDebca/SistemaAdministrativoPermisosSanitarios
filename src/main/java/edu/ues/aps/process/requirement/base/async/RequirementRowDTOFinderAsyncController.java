package edu.ues.aps.process.requirement.base.async;

import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;
import edu.ues.aps.process.requirement.base.service.RequirementRowDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class RequirementRowDTOFinderAsyncController {

    private static final Logger logger = LogManager.getLogger(RequirementRowDTOFinderAsyncController.class);

    private RequirementRowDTOFinderService finderService;

    public RequirementRowDTOFinderAsyncController(RequirementRowDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/requirement-control/find-all-requirement-rows-from-document", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<RequirementRowDTO> findAllRequirementRowsFromDocument(@RequestParam("id") Long id_document) {
        logger.info("GET:: Find document control {} requirement rows.", id_document);
        return finderService.findAllFromDocument(id_document);
    }

    @GetMapping(value = "/requirement-control/find-all-requirement-rows-from-bunch", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<RequirementRowDTO> findAllRequirementRowsFromBunch(@RequestParam("id") Long id_document) {
        logger.info("GET:: Find bunch {} document control draft.",id_document);
        return finderService.findAllFromBunch(id_document);
    }

}
