package edu.ues.aps.process.requirement.base.dto;

public class RequirementControlGenericDTO  implements  AbstractRequirementControlGenericDTO{

    private Long id_casefile;
    private String request_folder;
    private String subject;
    private String owner_name;
    private String subject_address;
    private boolean document_status;

    public RequirementControlGenericDTO(Long id_casefile, String subject_address, boolean document_status) {
        this.id_casefile = id_casefile;
        this.subject_address = subject_address;
        this.document_status = document_status;
    }

    public RequirementControlGenericDTO(Long id_casefile, String subject, String owner_name, String subject_address, boolean document_status) {
        this.id_casefile = id_casefile;
        this.subject = subject;
        this.owner_name = owner_name;
        this.subject_address = subject_address;
        this.document_status = document_status;
    }

    public RequirementControlGenericDTO(Long id_casefile, String request_folder, String subject, String owner_name, String subject_address, boolean document_status) {
        this.id_casefile = id_casefile;
        this.request_folder = request_folder;
        this.subject = subject;
        this.owner_name = owner_name;
        this.subject_address = subject_address;
        this.document_status = document_status;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public String getRequest_folder() {
        return request_folder;
    }

    public String getSubject() {
        return subject;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getSubject_address() {
        return subject_address;
    }

    public boolean isDocument_status() {
        return document_status;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public void setOwner_name(String owner_name) {
        this.owner_name = owner_name;
    }

    public void setSubject_address(String subject_address) {
        this.subject_address = subject_address;
    }
}
