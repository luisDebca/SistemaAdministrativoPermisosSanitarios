package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;

import java.util.List;

public interface RequirementRowPersistenceService {

    void updateRequirementRows(List<RequirementRowDTO> rows);
}
