package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.model.AbstractRequirementControlDocument;

public interface RequirementControlDocumentPersistenceRepository {

    void save(AbstractRequirementControlDocument document);

    void saveOrUpdate(AbstractRequirementControlDocument document);

    void merge(AbstractRequirementControlDocument document);
}
