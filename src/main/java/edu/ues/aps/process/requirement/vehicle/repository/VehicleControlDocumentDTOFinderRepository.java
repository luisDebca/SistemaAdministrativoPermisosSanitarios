package edu.ues.aps.process.requirement.vehicle.repository;

import edu.ues.aps.process.requirement.vehicle.dto.AbstractControlDocumentProcessDTO;

import java.util.List;

public interface VehicleControlDocumentDTOFinderRepository {

    List<AbstractControlDocumentProcessDTO> findAll(boolean document_status);
}
