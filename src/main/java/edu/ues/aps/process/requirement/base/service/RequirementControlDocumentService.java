package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.repository.CaseFilePersistenceRepository;
import edu.ues.aps.process.base.repository.SingleCaseFileFinderRepository;
import edu.ues.aps.process.requirement.base.model.RequirementControlDocument;
import edu.ues.aps.process.requirement.base.model.RequirementRow;
import edu.ues.aps.process.requirement.base.repository.RequirementControlDocumentStatusRepository;
import edu.ues.aps.process.requirement.base.repository.RequirementControlDocumentPersistenceRepository;
import edu.ues.aps.process.requirement.establishment.repository.EstablishmentControlDocumentFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class RequirementControlDocumentService implements RequirementControlDocumentPersistenceService {

    private final CaseFilePersistenceRepository caseFilePersistenceRepository;
    private final SingleCaseFileFinderRepository caseFileFinderRepository;
    private final RequirementControlDocumentStatusRepository crudRepository;
    private final EstablishmentControlDocumentFinderRepository documentFinderRepository;
    private final RequirementControlDocumentPersistenceRepository documentPersistenceRepository;

    public RequirementControlDocumentService(RequirementControlDocumentStatusRepository crudRepository, SingleCaseFileFinderRepository caseFileFinderRepository, CaseFilePersistenceRepository caseFilePersistenceRepository, EstablishmentControlDocumentFinderRepository documentFinderRepository, RequirementControlDocumentPersistenceRepository documentPersistenceRepository) {
        this.crudRepository = crudRepository;
        this.caseFileFinderRepository = caseFileFinderRepository;
        this.caseFilePersistenceRepository = caseFilePersistenceRepository;
        this.documentFinderRepository = documentFinderRepository;
        this.documentPersistenceRepository = documentPersistenceRepository;
    }

    @Override
    public void updateRequirementDocumentStatus(Long id_document) {
        if (crudRepository.isCompleted(id_document)) {
            crudRepository.updateStatus(id_document);
        }
    }

    @Override
    public void save(Long id_caseFile, RequirementControlDocument document) {
        AbstractCasefile caseFile = caseFileFinderRepository.find(id_caseFile);
        RequirementControlDocument controlDocument = new RequirementControlDocument();
        controlDocument.setControl_document_created_on(LocalDateTime.now());
        controlDocument.setStatus(document.getStatus());
        if(document.getStatus()){
            controlDocument.setAcceptance_document_created_on(LocalDateTime.now());
        }
        caseFile.addControlDocument(controlDocument);
        caseFilePersistenceRepository.saveOrUpdate(caseFile);
        for(RequirementRow row: document.getRequirementRows()){
            controlDocument.addRequirementRow(
                    documentFinderRepository.findTemplateById(row.getTemplate().getTemplate().getId(),row.getTemplate().getRequirement().getId()),
                    row.getStatus(),row.getObservation());
        }
        documentPersistenceRepository.saveOrUpdate(controlDocument);
    }
}
