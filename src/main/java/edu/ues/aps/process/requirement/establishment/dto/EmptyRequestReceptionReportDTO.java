package edu.ues.aps.process.requirement.establishment.dto;

import edu.ues.aps.process.base.model.ClientType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class EmptyRequestReceptionReportDTO implements AbstractEstablishmentRequestReceptionReportDTO {

    @Override
    public String getEstablishment_name() {
        return "Establishment name not found";
    }

    @Override
    public String getEstablishment_address() {
        return "Establishment address not found";
    }

    @Override
    public String getAcceptance_created_on() {
        return DatetimeUtil.parseDatetimeToVerboseDate(LocalDateTime.of(2000, 01, 01, 00, 00));
    }

    @Override
    public String getClient_name() {
        return "Client name not found";
    }

    @Override
    public void setClient_name(String client_name) {

    }

    @Override
    public String getClient_dui() {
        return "Client DUI not found";
    }

    @Override
    public void setClient_dui(String client_dui) {

    }

    @Override
    public String getClient_resident_card() {
        return "Client resident card not found";
    }

    @Override
    public void setClient_resident_card(String client_resident_card) {

    }

    @Override
    public String getClient_passport() {
        return "Client passport not found";
    }

    @Override
    public void setClient_passport(String client_passport) {

    }

    @Override
    public String getClient_type() {
        return "Client type not found";
    }

    @Override
    public void setClient_type(ClientType client_type) {

    }

    @Override
    public String getUser_name() {
        return "User name not found";
    }

    @Override
    public void setUser_name(String user_name) {

    }
}
