package edu.ues.aps.process.requirement.base.model;

import java.util.List;

public interface AbstractRequirement {

    Long getId();

    void setId(Long id);

    String getRequirement();

    void setRequirement(String requirement);

    List<RequirementControlTemplate> getTemplates();

    void setTemplates(List<RequirementControlTemplate> templates);
}
