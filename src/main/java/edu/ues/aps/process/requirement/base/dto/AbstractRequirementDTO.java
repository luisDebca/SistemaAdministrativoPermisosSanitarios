package edu.ues.aps.process.requirement.base.dto;

public interface AbstractRequirementDTO {

    Long getId_requirement();

    String getRequirement_requirement();
}
