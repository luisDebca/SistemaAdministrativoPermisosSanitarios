package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.process.area.establishment.model.AbstractEstablishment;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.AbstractClient;
import edu.ues.aps.process.base.model.AbstractOwner;
import edu.ues.aps.process.record.base.model.Process;
import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;
import edu.ues.aps.users.management.model.User;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class EstablishmentControlDocumentRepository implements EstablishmentControlDocumentPersistenceRepository, EstablishmentControlDocumentFinderRepository {

    private SessionFactory sessionFactory;

    public EstablishmentControlDocumentRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public RequirementControlTemplate findTemplateById(Long id_template, Long id_requirement) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select reqTemp " +
                "from RequirementControlTemplate reqTemp " +
                "where reqTemp.id.template_id = :id_template " +
                "and reqTemp.id.requirement_id = :id_requirement", RequirementControlTemplate.class)
                .setParameter("id_template", id_template)
                .setParameter("id_requirement", id_requirement)
                .getSingleResult();
    }

    @Override
    public Process findStartProcess() throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select process " +
                "from Process process " +
                "where process.id = 1", Process.class)
                .getSingleResult();
    }

    @Override
    public User findUserByUsername(String username) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select user " +
                "from User user " +
                "where user.username = :username", User.class)
                .setParameter("username", username)
                .getSingleResult();
    }

    @Override
    public void updateEstablishmentClientList(AbstractEstablishment establishment) {
        sessionFactory.getCurrentSession().saveOrUpdate(establishment);
        sessionFactory.getCurrentSession().flush();
    }

    @Override
    public void updateCasefileProcess(AbstractCasefile casefile) {
        sessionFactory.getCurrentSession().saveOrUpdate(casefile);
    }

    @Override
    public void saveOwnerEstablishmentCasefileDocumentByCascade(AbstractOwner ownerEntity) {
        sessionFactory.getCurrentSession().persist(ownerEntity);
    }

    @Override
    public void saveClient(AbstractClient client) {
        sessionFactory.getCurrentSession().persist(client);
    }
}
