package edu.ues.aps.process.requirement.vehicle.report;

import edu.ues.aps.process.requirement.base.report.RequirementDocumentReportController;
import edu.ues.aps.process.requirement.vehicle.dto.AbstractRequirementControlVehicleReportDTO;
import edu.ues.aps.process.requirement.vehicle.service.VehicleControlDocumentReportService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;

@Controller
public class VehicleRequirementDocumentReportController extends RequirementDocumentReportController {

    private static final Logger logger = LogManager.getLogger(VehicleRequirementDocumentReportController.class);

    private static final String REPORT_NAME = "RequirementControlDocumentVReport";

    private final SinglePersonalInformationService personalInformationService;
    private final VehicleControlDocumentReportService reportService;

    public VehicleRequirementDocumentReportController(SinglePersonalInformationService personalInformationService, VehicleControlDocumentReportService reportService) {
        this.personalInformationService = personalInformationService;
        this.reportService = reportService;
    }

    @Override
    @GetMapping("/requirement-control/vehicle/bunch/{id_bunch}/case-file/requirement-control-report.pdf")
    public String generateReport(@PathVariable Long id_bunch, String selected_clients_json,
                                 ModelMap model, Principal principal) {
        logger.info("REPORT:: Generate requirement control document for bunch {}.", id_bunch);
        clearDataSource();
        setSelectedClientsFromJSON(selected_clients_json);
        setPrincipal_name(personalInformationService.getFullName(principal.getName()));
        for (AbstractRequirementControlVehicleReportDTO dto : reportService.findVehicleDocumentReportResources(id_bunch)) {
            addReportSource(dto);
        }
        addReportRequiredSources(getDatasource());
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }
}
