package edu.ues.aps.process.requirement.establishment.async;

import edu.ues.aps.process.requirement.establishment.dto.AbstractRequirementDocumentInfoDTO;
import edu.ues.aps.process.requirement.establishment.service.RequirementDocumentInfoDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
public class RequirementDocumentInformationProviderAsyncController {

    private static final Logger logger = LogManager.getLogger(RequirementDocumentInformationProviderAsyncController.class);

    private RequirementDocumentInfoDTOFinderService finderService;

    public RequirementDocumentInformationProviderAsyncController(RequirementDocumentInfoDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/requirement-control/establishment/requirement-process-information", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public AbstractRequirementDocumentInfoDTO showRequestInformationForRequirementReview(@RequestParam("id") Long id_caseFile) {
        logger.info("GET:: Get document {} information", id_caseFile);
        return finderService.findDocumentInfoById(id_caseFile);
    }
}
