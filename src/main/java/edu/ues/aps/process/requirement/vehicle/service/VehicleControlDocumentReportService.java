package edu.ues.aps.process.requirement.vehicle.service;

import edu.ues.aps.process.requirement.vehicle.dto.AbstractRequirementControlVehicleReportDTO;

import java.util.List;

public interface VehicleControlDocumentReportService {

    List<AbstractRequirementControlVehicleReportDTO> findVehicleDocumentReportResources(Long id_bunch);
}
