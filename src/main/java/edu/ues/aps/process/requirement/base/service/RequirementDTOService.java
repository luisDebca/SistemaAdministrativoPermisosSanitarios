package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.dto.AbstractRequirementDTO;
import edu.ues.aps.process.requirement.base.repository.RequirementDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class RequirementDTOService implements RequirementDTOFinderService {

    private RequirementDTOFinderRepository finderRepository;

    public RequirementDTOService(RequirementDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractRequirementDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public List<AbstractRequirementDTO> findAllRequirementsFromTemplate(Long id_template) {
        return finderRepository.findAllRequirementsFromTemplate(id_template);
    }
}
