package edu.ues.aps.process.requirement.base.model;

public enum RequirementStatusType {
    APPROVED("SI"),
    FAULT("NO"),
    NOT_APPLY("N/A"),
    INVALID("INVALIDO");

    private String requirementStatus;

    RequirementStatusType(String status) {
        this.requirementStatus = status;
    }

    public String getRequirementStatus() {
        return requirementStatus;
    }
}
