package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.model.AbstractDocumentTemplate;

public interface DocumentTemplateFinderService {

    AbstractDocumentTemplate findById(Long id_template);
}
