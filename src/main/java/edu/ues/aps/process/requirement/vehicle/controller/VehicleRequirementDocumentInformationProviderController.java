package edu.ues.aps.process.requirement.vehicle.controller;

import edu.ues.aps.process.requirement.vehicle.service.VehicleControlDocumentDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/requirement-control/vehicle")
public class VehicleRequirementDocumentInformationProviderController {

    private static final Logger logger = LogManager.getLogger(VehicleRequirementDocumentInformationProviderController.class);

    private static final String COMPLETED = "vehicle_complete_control_document_list";
    private static final String INCOMPLETE = "vehicle_incomplete_control_document_list";

    private final VehicleControlDocumentDTOFinderService finderService;

    public VehicleRequirementDocumentInformationProviderController(VehicleControlDocumentDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/completed/all")
    public String showAllRequestCompleted(ModelMap model) {
        logger.info("GET:: Show all request completed for vehicles.");
        model.addAttribute("requests", finderService.findAll(true));
        return COMPLETED;
    }

    @GetMapping("/incomplete/all")
    public String showAllRequestIncomplete(ModelMap model) {
        logger.info("GET:: Show all request incomplete for vehicles.");
        model.addAttribute("requests", finderService.findAll(false));
        return INCOMPLETE;
    }
}
