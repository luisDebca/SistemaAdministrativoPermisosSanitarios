package edu.ues.aps.process.requirement.base.dto;

public interface AbstractRequirementControlDocumentReportDTO {

    String getOwner_name();

    Integer getOwner_class();

    String getRequest_certification_type();

    String getDocument_created_on();

    String getDocument_template_title();

    String getDocument_template_subtitle();

    String getRequirement_row_requirement();

    String getRequirement_row_status();

    String getRequirement_row_observation();

    String getApproved_name_by();

    String getApproved_type_by();

    String getSigned_name_by();

    String getUser_name();

    boolean isRequirements_completed();

    void setApproved_name_by(String approved_name_by);

    void setApproved_type_by(String approved_type_by);

    void setSigned_name_by(String signed_name_by);

    void setUser_name(String user_name);

    void setRequest_certification(String request_certification);
}
