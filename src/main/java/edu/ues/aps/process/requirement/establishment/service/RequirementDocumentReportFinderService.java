package edu.ues.aps.process.requirement.establishment.service;

import edu.ues.aps.process.requirement.establishment.dto.AbstractRequirementControlEstablishmentReportDTO;

import java.util.List;

public interface RequirementDocumentReportFinderService {

    List<AbstractRequirementControlEstablishmentReportDTO> findDocumentReportResources(Long id_caseFile);

}
