package edu.ues.aps.process.requirement.vehicle.repository;

import edu.ues.aps.process.requirement.vehicle.dto.AbstractVehicleRequestReceptionReportDTO;

public interface VehicleRequestReceptionReportRepository {

    AbstractVehicleRequestReceptionReportDTO findReportResource(Long id_bunch);
}
