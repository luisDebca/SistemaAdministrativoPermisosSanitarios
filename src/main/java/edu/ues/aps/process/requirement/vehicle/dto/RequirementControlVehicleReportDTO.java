package edu.ues.aps.process.requirement.vehicle.dto;

import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.process.requirement.base.model.RequirementStatusType;
import edu.ues.aps.process.requirement.base.dto.RequirementControlDocumentReportDTO;

import java.time.LocalDateTime;

public class RequirementControlVehicleReportDTO extends RequirementControlDocumentReportDTO implements AbstractRequirementControlVehicleReportDTO {

    private Long vehicle_count;
    private String vehicle_license;
    private String vehicle_type;

    public RequirementControlVehicleReportDTO(String owner_name, Integer owner_class, CertificationType request_certification_type, boolean requirements_completed, LocalDateTime document_created_on, String document_template_title, String document_template_subtitle, String requirement_row_requirement, RequirementStatusType requirement_row_status, String requirement_row_observation, Long vehicle_count, String vehicle_license) {
        super(owner_name, owner_class, request_certification_type, requirements_completed, document_created_on, document_template_title, document_template_subtitle, requirement_row_requirement, requirement_row_status, requirement_row_observation);
        this.vehicle_count = vehicle_count;
        this.vehicle_license = vehicle_license;
    }

    public Long getVehicle_count() {
        return vehicle_count;
    }

    public String getVehicle_license() {
        return vehicle_license;
    }

    public String getVehicle_type() {
        return vehicle_type;
    }

    public void setVehicle_type(String vehicle_type) {
        this.vehicle_type = vehicle_type;
    }
}
