package edu.ues.aps.process.requirement.establishment.async;

import edu.ues.aps.process.area.base.dto.AbstractRequestProcessIndexSummaryDTO;
import edu.ues.aps.process.area.base.service.IndexSummaryService;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/requirement-control/establishment")
public class EstablishmentRequirementControlIndexController {

    private static final Logger logger = LogManager.getLogger(EstablishmentRequirementControlIndexController.class);

    private final IndexSummaryService summaryService;

    public EstablishmentRequirementControlIndexController(@Qualifier("establishmentIndexSummaryService") IndexSummaryService summaryService) {
        this.summaryService = summaryService;
    }

    @GetMapping(value = "/request-summary-for-today", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractRequestProcessIndexSummaryDTO> getTodaySummaryForRequirementControlProcess(){
        logger.info("GET:: Get requirement control summary for today");
        return summaryService.findAllFromToday(ProcessIdentifier.REQUIREMENT_REVIEW);
    }

    @GetMapping(value = "/request-summary-for-week", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractRequestProcessIndexSummaryDTO> getWeekSummaryForRequirementControlProcess(){
        logger.info("GET:: Get requirement control summary for week");
        return summaryService.findAllFromThisWeek(ProcessIdentifier.REQUIREMENT_REVIEW);
    }

    @GetMapping(value = "/request-summary-for-month", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractRequestProcessIndexSummaryDTO> getMonthSummaryForRequirementControlProcess(){
        logger.info("GET:: Get requirement control summary for month");
        return summaryService.findAllFromThisMonth(ProcessIdentifier.REQUIREMENT_REVIEW);
    }

    @GetMapping(value = "/request-count-for-today", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getTodayRequestCount(){
        logger.info("GET:: Get requirement control count summary for today");
        return summaryService.countFromToday(ProcessIdentifier.REQUIREMENT_REVIEW);
    }

    @GetMapping(value = "/request-count-for-week", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getWeekRequestCount(){
        logger.info("GET:: Get requirement control count summary for week");
        return summaryService.countFromThisWeek(ProcessIdentifier.REQUIREMENT_REVIEW);
    }

    @GetMapping(value = "/request-count-for-month", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getMonthRequestCount(){
        logger.info("GET:: Get requirement control count summary for month");
        return summaryService.countFromThisMonth(ProcessIdentifier.REQUIREMENT_REVIEW);
    }

}
