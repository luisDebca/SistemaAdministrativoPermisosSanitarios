package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;
import edu.ues.aps.process.requirement.base.model.RequirementRow;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RequirementRowDTORepository implements RequirementRowDTOFinderRepository, RequirementRowPersistenceRepository {

    private SessionFactory sessionFactory;

    public RequirementRowDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<RequirementRowDTO> findAllRowsFromDocument(Long id_document) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.base.dto.RequirementRowDTO(" +
                "rows.template.requirement.id,rows.template.template.id,rows.template.requirement.requirement,rows.observation,rows.status) " +
                "from RequirementControlDocument document " +
                "inner join document.requirementRows rows " +
                "where document.id = :id", RequirementRowDTO.class)
                .setParameter("id", id_document)
                .getResultList();
    }

    @Override
    public void updateRequirementRow(RequirementRowDTO row) {
        sessionFactory.getCurrentSession().createQuery("update RequirementRow row " +
                "set row.status = :status, row.observation = :observation " +
                "where row.id =:id")
                .setParameter("observation", row.getRow_observation())
                .setParameter("status", row.getRow_status_type())
                .setParameter("id", new RequirementRow.RequirementRowId(row.getId_document(), row.getId_template(), row.getId_requirement()))
                .executeUpdate();
    }
}
