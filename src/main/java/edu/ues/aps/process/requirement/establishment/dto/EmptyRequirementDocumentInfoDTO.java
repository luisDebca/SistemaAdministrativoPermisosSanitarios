package edu.ues.aps.process.requirement.establishment.dto;

public class EmptyRequirementDocumentInfoDTO implements AbstractRequirementDocumentInfoDTO {

    @Override
    public String getOwner_name() {
        return "Owner name not found";
    }

    @Override
    public String getOwner_nit() {
        return "Owner nit not found";
    }

    @Override
    public String getEstablishment_name() {
        return "Establishment name not found";
    }

    @Override
    public String getEstablishment_address() {
        return "Establishment address not found";
    }

    @Override
    public String getEstablishment_nit() {
        return "Establishment NIT not found";
    }

    @Override
    public String getEstablishment_type() {
        return "Establishment type not found";
    }

    @Override
    public String getDocument_acceptance_on() {
        return "--/--/-- --:--:--";
    }

    @Override
    public String getDocument_created_on() {
        return "--/--/-- --:--:--";
    }

    @Override
    public boolean isDocument_status() {
        return false;
    }

    @Override
    public long getDocument_approved_requirement_count() {
        return 0;
    }

    @Override
    public long getDocument_fault_requirement_count() {
        return 0;
    }

    @Override
    public String getCasefile_certification_type() {
        return "Casefile certification type not found";
    }
}
