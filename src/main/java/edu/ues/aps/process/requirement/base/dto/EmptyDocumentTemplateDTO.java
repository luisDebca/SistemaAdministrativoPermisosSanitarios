package edu.ues.aps.process.requirement.base.dto;

public class EmptyDocumentTemplateDTO implements AbstractDocumentTemplateDTO {

    @Override
    public Long getId_template() {
        return 0L;
    }

    @Override
    public String getType() {
        return "Document template type not found";
    }

    @Override
    public String getTitle() {
        return "Document template title not found";
    }

    @Override
    public String getSubtitle() {
        return "Document template subtitle not found";
    }

    @Override
    public Integer getVersion() {
        return 0;
    }
}
