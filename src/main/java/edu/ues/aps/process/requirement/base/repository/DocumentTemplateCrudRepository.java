package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.model.AbstractDocumentTemplate;
import edu.ues.aps.process.requirement.base.model.Requirement;

public interface DocumentTemplateCrudRepository {

    AbstractDocumentTemplate findById(Long id_template);

    Requirement findRequirement(Long id_requirement);

    void save(AbstractDocumentTemplate template);

    void merge(AbstractDocumentTemplate template);

    void delete(AbstractDocumentTemplate template);
}
