package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.model.AbstractRequirement;
import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;

import javax.persistence.NoResultException;
import java.util.List;

public interface RequirementPersistenceRepository {

    AbstractRequirement findById(Long id_requirement) throws NoResultException;

    void save(AbstractRequirement requirement);

    void update(AbstractRequirement requirement);

    void delete(AbstractRequirement requirement);
}
