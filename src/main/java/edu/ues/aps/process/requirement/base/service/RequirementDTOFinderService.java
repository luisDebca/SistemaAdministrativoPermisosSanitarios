package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.dto.AbstractRequirementDTO;

import java.util.List;

public interface RequirementDTOFinderService {

    List<AbstractRequirementDTO> findAll();

    List<AbstractRequirementDTO> findAllRequirementsFromTemplate(Long id_template);
}
