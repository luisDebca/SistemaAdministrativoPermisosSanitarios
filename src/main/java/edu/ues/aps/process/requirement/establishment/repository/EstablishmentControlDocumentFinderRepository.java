package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.process.record.base.model.Process;
import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;
import edu.ues.aps.users.management.model.User;

import javax.persistence.NoResultException;

public interface EstablishmentControlDocumentFinderRepository {

    RequirementControlTemplate findTemplateById(Long id_template, Long id_requirement) throws NoResultException;

    Process findStartProcess() throws NoResultException;

    User findUserByUsername(String username) throws NoResultException;
}
