package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;

public interface RequirementRowPersistenceRepository {

    void updateRequirementRow(RequirementRowDTO row);
}
