package edu.ues.aps.process.requirement.base.controller;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchFinderService;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.service.SingleCaseFileFinderService;
import edu.ues.aps.process.record.base.service.CaseFileProcessRegisterService;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import edu.ues.aps.process.requirement.base.model.RequirementControlDocument;
import edu.ues.aps.process.requirement.base.model.RequirementRow;
import edu.ues.aps.process.requirement.base.model.RequirementStatusType;
import edu.ues.aps.process.requirement.base.service.RequirementControlDocumentPersistenceService;
import edu.ues.aps.process.requirement.base.service.RequirementRowBuilderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/requirement-control")
public class RequirementDocumentPersistenceController {

    private static final Logger logger = LogManager.getLogger(RequirementDocumentPersistenceController.class);

    private static final String CONTROL_DOCUMENT = "control_document_form";

    private final ProcessUserRegisterService registerService;
    private final RequirementRowBuilderService builderService;
    private final VehicleBunchFinderService bunchFinderService;
    private final SingleCaseFileFinderService caseFileFinderService;
    private final CaseFileProcessRegisterService processRegisterService;
    private final RequirementControlDocumentPersistenceService documentPersistenceService;
    private RequirementControlDocument emptyDocument;
    private Boolean document_row_status;
    private List<AbstractCasefile> bunch;
    private CasefileSection section;

    public RequirementDocumentPersistenceController(RequirementRowBuilderService builderService, ProcessUserRegisterService registerService, SingleCaseFileFinderService caseFileFinderService, CaseFileProcessRegisterService processRegisterService, RequirementControlDocumentPersistenceService documentPersistenceService, VehicleBunchFinderService bunchFinderService) {
        this.builderService = builderService;
        this.registerService = registerService;
        this.caseFileFinderService = caseFileFinderService;
        this.processRegisterService = processRegisterService;
        this.documentPersistenceService = documentPersistenceService;
        this.bunchFinderService = bunchFinderService;
    }

    @GetMapping("/template/{id_template}/case-file/{id_caseFile}/new")
    public String showRequirementControlForm(@PathVariable Long id_template, @PathVariable Long id_caseFile, ModelMap model, Principal principal) {
        logger.info("GET:: Show new requirement control document.");
        buildNewDocument(id_template);
        model.addAttribute("document", emptyDocument);
        model.addAttribute("id_casefile", id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.establishment.requirement.review.new");
        return CONTROL_DOCUMENT;
    }

    @PostMapping("/template/*/case-file/{id_caseFile}/new")
    public String saveRequirementControlDocument(@PathVariable Long id_caseFile, @ModelAttribute RequirementControlDocument document, Principal principal) {
        logger.info("POST:: Save new control document for case-file {}", id_caseFile);
        section = CasefileSection.ESTABLISHMENT;
        setDocumentStatusValid();
        moveCaseFileToNexProcess(id_caseFile);
        checkDocumentStatus(document.getRequirementRows());
        document.setStatus(document_row_status);
        documentPersistenceService.save(id_caseFile, document);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.establishment.requirement.review.save");
        return properListViewForDocumentAcceptation();
    }

    @GetMapping("/template/{id_template}/bunch/{id_bunch}/new")
    public String showDocumentControlBunchForm(@PathVariable Long id_template, @PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show new requirement control document.");
        buildNewDocument(id_template);
        model.addAttribute("document", emptyDocument);
        model.addAttribute("id_bunch", id_bunch);
        return CONTROL_DOCUMENT;
    }

    @PostMapping("/template/*/bunch/{id_bunch}/new")
    public String saveRequirementControlDocumentBunch(@PathVariable Long id_bunch, @ModelAttribute RequirementControlDocument document, Principal principal) {
        logger.info("POST:: Save new control document for vehicle bunch {}", id_bunch);
        section = CasefileSection.VEHICLE;
        bunch = bunchFinderService.findAllRequest(id_bunch);
        setDocumentStatusValid();
        moveAllCaseFilesToNextProcess();
        checkDocumentStatus(document.getRequirementRows());
        document.setStatus(document_row_status);
        saveAllDocuments(document);
        registerAllCaseFileChanges(principal.getName());
        return properListViewForDocumentAcceptation();
    }

    private void checkDocumentStatus(List<RequirementRow> rows) {
        for (RequirementRow row : rows) {
            if (row.getStatus().equals(RequirementStatusType.FAULT)) {
                document_row_status = false;
                break;
            }
        }
    }

    private void moveCaseFileToNexProcess(Long id_caseFile) {
        AbstractCasefile caseFile = caseFileFinderService.find(id_caseFile);
        processRegisterService.moveToNext(caseFile);
    }

    private void setDocumentStatusValid() {
        document_row_status = true;
    }

    private void buildNewDocument(Long id_template) {
        emptyDocument = new RequirementControlDocument();
        List<RequirementRow> rows = builderService.buildEmptyRequirementRowsFromTemplate(id_template);
        for (RequirementRow row : rows) {
            emptyDocument.addRequirementRow(row);
        }
    }

    private void moveAllCaseFilesToNextProcess() {
        for (AbstractCasefile caseFile : bunch) {
            processRegisterService.moveToNext(caseFile);
        }
    }

    private void saveAllDocuments(RequirementControlDocument document) {
        for (AbstractCasefile caseFile : bunch) {
            documentPersistenceService.save(caseFile.getId(), copyDocument(document));
        }
    }

    private RequirementControlDocument copyDocument(RequirementControlDocument document) {
        RequirementControlDocument copy = new RequirementControlDocument();
        copy.setRequirementRows(document.getRequirementRows());
        copy.setStatus(document_row_status);
        return copy;
    }

    private void registerAllCaseFileChanges(String username) {
        for (AbstractCasefile caseFile : bunch) {
            registerService.addProcessUser(caseFile.getId(), username, "process.record.vehicle.requirement.review.new");
        }
    }

    private String properListViewForDocumentAcceptation() {
        if (section.equals(CasefileSection.ESTABLISHMENT)) {
            if (document_row_status) {
                return "redirect:/requirement-control/establishment/completed/all";
            } else {
                return "redirect:/requirement-control/establishment/incomplete/all";
            }
        } else if (section.equals(CasefileSection.VEHICLE)) {
            if (document_row_status) {
                return "redirect:/requirement-control/vehicle/completed/all";
            } else {
                return "redirect:/requirement-control/vehicle/incomplete/all";
            }
        } else {
            return "redirect:/requirement-control/index";
        }
    }
}
