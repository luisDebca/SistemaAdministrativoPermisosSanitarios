package edu.ues.aps.process.requirement.vehicle.dto;

import edu.ues.aps.process.requirement.base.dto.AbstractRequestReceptionReportDTO;

public interface AbstractVehicleRequestReceptionReportDTO extends AbstractRequestReceptionReportDTO {

    Long getVehicle_count();

    String getVehicle_licenses();

    void setVehicle_licenses(String vehicle_licenses);

    String getVehicle_address();
}
