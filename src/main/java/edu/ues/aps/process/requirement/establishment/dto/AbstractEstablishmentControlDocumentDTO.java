package edu.ues.aps.process.requirement.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentRequestDTO;

public interface AbstractEstablishmentControlDocumentDTO extends AbstractEstablishmentRequestDTO {

}
