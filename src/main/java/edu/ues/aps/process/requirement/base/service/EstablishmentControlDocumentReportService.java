package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.establishment.dto.AbstractRequirementControlEstablishmentReportDTO;

import java.util.List;

public interface EstablishmentControlDocumentReportService {

    List<AbstractRequirementControlEstablishmentReportDTO> findEstablishmentDocumentReportResources(Long id_caseFile);
}
