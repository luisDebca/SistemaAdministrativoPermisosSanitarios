package edu.ues.aps.process.requirement.base.model;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.EmptyCasefile;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class EmptyRequirementControlDocument implements AbstractRequirementControlDocument {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public Boolean getStatus() {
        return false;
    }

    @Override
    public LocalDateTime getControl_document_created_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public LocalDateTime getAcceptance_document_created_on() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public List<RequirementRow> getRequirementRows() {
        return Collections.emptyList();
    }

    @Override
    public AbstractCasefile getCasefile() {
        return new EmptyCasefile();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setStatus(Boolean status) {

    }

    @Override
    public void setControl_document_created_on(LocalDateTime control_document_created_on) {

    }

    @Override
    public void setAcceptance_document_created_on(LocalDateTime acceptance_document_created_on) {

    }

    @Override
    public void setRequirementRows(List<RequirementRow> requirementRows) {

    }

    @Override
    public void setCasefile(Casefile casefile) {

    }

    @Override
    public void addRequirementRow(RequirementRow row) {

    }

    @Override
    public void removeRequirementRow(RequirementRow row) {

    }

    @Override
    public void addRequirementRow(RequirementControlTemplate template, RequirementStatusType status, String observation) {

    }

    @Override
    public String toString() {
        return "EmptyRequirementControlDocument{}";
    }
}
