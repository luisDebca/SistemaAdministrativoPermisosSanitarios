package edu.ues.aps.process.requirement.establishment.service;

import edu.ues.aps.process.requirement.base.dto.AbstractRequestReceptionReportDTO;
import edu.ues.aps.process.requirement.establishment.dto.AbstractRequirementControlEstablishmentReportDTO;
import edu.ues.aps.process.requirement.establishment.dto.EmptyRequestReceptionReportDTO;
import edu.ues.aps.process.requirement.establishment.repository.EstablishmentControlDocumentReportRepository;
import edu.ues.aps.process.requirement.establishment.repository.EstablishmentRequestReceptionReportResourceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class RequirementDocumentReportService implements RequirementDocumentReportFinderService, RequestReceptionReportFinderService {

    private final EstablishmentControlDocumentReportRepository documentReportRepository;
    private EstablishmentRequestReceptionReportResourceRepository receptionFinderRepository;

    public RequirementDocumentReportService(EstablishmentRequestReceptionReportResourceRepository receptionFinderRepository, EstablishmentControlDocumentReportRepository documentReportRepository) {
        this.receptionFinderRepository = receptionFinderRepository;
        this.documentReportRepository = documentReportRepository;
    }

    @Override
    public List<AbstractRequirementControlEstablishmentReportDTO> findDocumentReportResources(Long id_caseFile) {
        return documentReportRepository.findDocumentReportResources(id_caseFile);
    }

    @Override
    public AbstractRequestReceptionReportDTO findRequestReceptionReportResources(Long id_caseFile) {
        try {
            return receptionFinderRepository.findReceptionReportResources(id_caseFile);
        } catch (NoResultException e) {
            return new EmptyRequestReceptionReportDTO();
        }
    }
}
