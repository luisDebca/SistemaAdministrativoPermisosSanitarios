package edu.ues.aps.process.requirement.establishment.service;

import edu.ues.aps.process.requirement.establishment.dto.AbstractRequirementDocumentInfoDTO;

public interface RequirementDocumentInfoDTOFinderService {

    AbstractRequirementDocumentInfoDTO findDocumentInfoById(Long id_casefile);
}
