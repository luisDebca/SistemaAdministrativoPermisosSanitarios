package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.process.base.model.AbstractClient;
import edu.ues.aps.process.area.establishment.model.AbstractEstablishment;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.AbstractOwner;

public interface EstablishmentControlDocumentPersistenceRepository {

    void updateEstablishmentClientList(AbstractEstablishment establishment);

    void saveOwnerEstablishmentCasefileDocumentByCascade(AbstractOwner ownerEntity);

    void updateCasefileProcess(AbstractCasefile casefile);

    void saveClient(AbstractClient client);

}
