package edu.ues.aps.process.requirement.base.report;

import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.ClientDTO;
import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.requirement.base.dto.AbstractRequestReceptionReportDTO;
import org.springframework.ui.ModelMap;

import java.security.Principal;

public abstract class RequestReceptionReportController extends AbstractReportController {

    private AbstractRequestReceptionReportDTO datasource;

    public abstract String generateReport(Long id, ClientDTO selected_client, ModelMap model, Principal principal);

    public void addClientDataToDatasource(AbstractClientDTO clientDTO) {
        datasource.setClient_name(clientDTO.getClient_name());
        datasource.setClient_dui(clientDTO.getClient_DUI());
        datasource.setClient_resident_card(clientDTO.getClient_resident_card());
        datasource.setClient_passport(clientDTO.getClient_passport());
        datasource.setClient_type(clientDTO.getClientType());
    }

    public void addCurrentUserNameToDatasource(String user_name) {
        datasource.setUser_name(user_name);
    }

    public AbstractRequestReceptionReportDTO getDatasource() {
        return datasource;
    }

    public void setDatasource(AbstractRequestReceptionReportDTO datasource) {
        this.datasource = datasource;
    }
}
