package edu.ues.aps.process.requirement.base.converter;

import edu.ues.aps.process.requirement.base.model.DocumentTemplate;
import edu.ues.aps.process.requirement.base.model.Requirement;
import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;
import edu.ues.aps.process.requirement.base.service.RequirementFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.convert.TypeDescriptor;
import org.springframework.core.convert.converter.ConditionalConverter;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

import java.lang.annotation.Annotation;

@Component
public class LongToRequirementTemplateConverter implements Converter<String, RequirementControlTemplate>, ConditionalConverter {

    private static final Logger logger = LogManager.getLogger(LongToRequirementTemplateConverter.class);

    private final RequirementFinderService finderService;

    public LongToRequirementTemplateConverter(RequirementFinderService finderService) {
        this.finderService = finderService;
    }

    @Override
    public RequirementControlTemplate convert(String from) {
        Long id_requirement = Long.parseLong(from);
        Requirement requirement = (Requirement) finderService.findById(id_requirement);
        logger.info("CONVERTER:: Long {} to requirement {}", from, requirement.toString());
        return new RequirementControlTemplate(new DocumentTemplate(), requirement);
    }

    @Override
    public boolean matches(TypeDescriptor sourceType, TypeDescriptor targetType) {
        return !targetType.isCollection();
    }
}
