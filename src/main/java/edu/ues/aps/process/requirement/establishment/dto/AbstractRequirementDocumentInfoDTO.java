package edu.ues.aps.process.requirement.establishment.dto;

public interface AbstractRequirementDocumentInfoDTO {

    String getOwner_name();

    String getOwner_nit();

    String getEstablishment_name();

    String getEstablishment_address();

    String getEstablishment_nit();

    String getEstablishment_type();

    String getDocument_acceptance_on();

    String getDocument_created_on();

    boolean isDocument_status();

    long getDocument_approved_requirement_count();

    long getDocument_fault_requirement_count();

    String getCasefile_certification_type();
}
