package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.model.AbstractRequirementControlDocument;
import edu.ues.aps.process.requirement.base.model.RequirementStatusType;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class RequirementControlDocumentRepository implements RequirementControlDocumentStatusRepository, RequirementControlDocumentPersistenceRepository, SingleRequirementControlDocumentFinderRepository {

    private SessionFactory sessionFactory;

    public RequirementControlDocumentRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public boolean isCompleted(Long id_document) {
        return !(sessionFactory.getCurrentSession().createQuery("select sum(case when (rows.status = :fault_status) then true else false end) " +
                "from RequirementRow rows " +
                "where rows.document.id = :id", Boolean.class)
                .setParameter("fault_status", RequirementStatusType.FAULT)
                .setParameter("id", id_document)
                .getSingleResult());
    }

    @Override
    public void updateStatus(Long id_document) {
        sessionFactory.getCurrentSession().createQuery("update RequirementControlDocument document " +
                "set document.status = true, document.acceptance_document_created_on = current_timestamp " +
                "where document.id = :id")
                .setParameter("id", id_document)
                .executeUpdate();
    }

    @Override
    public void save(AbstractRequirementControlDocument document) {
        sessionFactory.getCurrentSession().persist(document);
    }

    @Override
    public void saveOrUpdate(AbstractRequirementControlDocument document) {
        sessionFactory.getCurrentSession().saveOrUpdate(document);
    }

    @Override
    public void merge(AbstractRequirementControlDocument document) {
        sessionFactory.getCurrentSession().merge(document);
    }

    @Override
    public AbstractRequirementControlDocument find(Long id_document) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select document " +
                "from RequirementControlDocument document " +
                "where document.id = :id",AbstractRequirementControlDocument.class)
                .setParameter("id",id_document)
                .getSingleResult();
    }
}
