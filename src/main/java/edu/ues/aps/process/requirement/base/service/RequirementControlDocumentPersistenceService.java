package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.model.RequirementControlDocument;

public interface RequirementControlDocumentPersistenceService {

    void updateRequirementDocumentStatus(Long id_document);

    void save(Long id_caseFile, RequirementControlDocument document);
}
