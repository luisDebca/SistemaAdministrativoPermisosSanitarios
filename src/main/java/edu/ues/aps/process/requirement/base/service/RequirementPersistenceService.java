package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.model.AbstractRequirement;

public interface RequirementPersistenceService {

    void save(AbstractRequirement requirement);

    void update(AbstractRequirement requirement);

    void delete(Long id_requirement);
}
