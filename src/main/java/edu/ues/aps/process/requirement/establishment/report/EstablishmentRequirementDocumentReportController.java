package edu.ues.aps.process.requirement.establishment.report;

import edu.ues.aps.process.requirement.base.report.RequirementDocumentReportController;
import edu.ues.aps.process.requirement.establishment.dto.AbstractRequirementControlEstablishmentReportDTO;
import edu.ues.aps.process.requirement.establishment.service.RequirementDocumentReportFinderService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.security.Principal;

@Controller
public class EstablishmentRequirementDocumentReportController extends RequirementDocumentReportController {

    private static final Logger logger = LogManager.getLogger(EstablishmentRequirementDocumentReportController.class);

    private static final String SMALL_REPORT = "RequirementControlDocumentESmallReport";
    private static final String LARGE_REPORT = "RequirementControlDocumentELargeReport";

    private final SinglePersonalInformationService personalInformationService;
    private RequirementDocumentReportFinderService reportFinderService;

    public EstablishmentRequirementDocumentReportController(RequirementDocumentReportFinderService reportFinderService, SinglePersonalInformationService personalInformationService) {
        this.reportFinderService = reportFinderService;
        this.personalInformationService = personalInformationService;
    }

    @Override
    @GetMapping(value = "/requirement-control/establishment/case-file/{id_casefile}/requirement-control-report.pdf")
    public String generateReport(@PathVariable Long id_casefile,@RequestParam String selected_clients_json,
                                 ModelMap model, Principal principal) {
        logger.info("REPORT:: Generate requirement control document for case file {}.", id_casefile);
        clearDataSource();
        setSelectedClientsFromJSON(selected_clients_json);
        setPrincipal_name(personalInformationService.getFullName(principal.getName()));
        for(AbstractRequirementControlEstablishmentReportDTO dto: reportFinderService.findDocumentReportResources(id_casefile)){
            addReportSource(dto);
        }
        addReportRequiredSources(getDatasource());
        model.addAllAttributes(getModel());
        return reportByContentSize(getDatasource().size());
    }

    private String reportByContentSize(int size) {
        if (size < 11) {
            return SMALL_REPORT;
        } else {
            return LARGE_REPORT;
        }
    }

}
