package edu.ues.aps.process.requirement.vehicle.service;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.requirement.vehicle.dto.AbstractControlDocumentProcessDTO;
import edu.ues.aps.process.requirement.vehicle.dto.AbstractRequirementControlVehicleReportDTO;
import edu.ues.aps.process.requirement.vehicle.dto.AbstractVehicleRequestReceptionReportDTO;
import edu.ues.aps.process.requirement.vehicle.repository.VehicleControlDocumentDTOFinderRepository;
import edu.ues.aps.process.requirement.vehicle.repository.VehicleControlDocumentReportRepository;
import edu.ues.aps.process.requirement.vehicle.repository.VehicleRequestReceptionReportRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class VehicleControlDocumentService implements VehicleControlDocumentDTOFinderService, VehicleControlDocumentReportService, VehicleRequestReceptionReportService {

    private final VehicleRequestReceptionReportRepository receptionReportRepository;
    private final VehicleControlDocumentReportRepository documentReportRepository;
    private final VehicleControlDocumentDTOFinderRepository finderRepository;
    private final VehicleBunchInformationService bunchInformationService;

    public VehicleControlDocumentService(VehicleControlDocumentDTOFinderRepository finderRepository, VehicleControlDocumentReportRepository documentReportRepository, VehicleBunchInformationService bunchInformationService, VehicleRequestReceptionReportRepository receptionReportRepository) {
        this.finderRepository = finderRepository;
        this.documentReportRepository = documentReportRepository;
        this.bunchInformationService = bunchInformationService;
        this.receptionReportRepository = receptionReportRepository;
    }

    @Override
    public List<AbstractControlDocumentProcessDTO> findAll(boolean document_status) {
        return finderRepository.findAll(document_status);
    }

    @Override
    public List<AbstractRequirementControlVehicleReportDTO> findVehicleDocumentReportResources(Long id_bunch) {
        List<AbstractRequirementControlVehicleReportDTO> resources = documentReportRepository.findVehicleDocumentReportResources(id_bunch);
        String content_types = bunchInformationService.findAllContents(id_bunch);
        String certification_types = bunchInformationService.findAllCertificationTypes(id_bunch);
        for(AbstractRequirementControlVehicleReportDTO dto: resources){
            dto.setVehicle_type(content_types);
            dto.setRequest_certification(certification_types);
        }
        return resources;
    }

    @Override
    public AbstractVehicleRequestReceptionReportDTO findReportResource(Long id_bunch) {
        AbstractVehicleRequestReceptionReportDTO reportResource = receptionReportRepository.findReportResource(id_bunch);
        if(reportResource.getVehicle_count() > 1){
            reportResource.setVehicle_licenses(bunchInformationService.findAllLicenses(id_bunch));
        }
        return reportResource;
    }

    @Override
    public int bunchVehicleCount(Long id_bunch) {
        return bunchInformationService.findAllLicensesList(id_bunch).size();
    }
}
