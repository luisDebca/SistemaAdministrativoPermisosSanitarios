package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.process.requirement.establishment.dto.AbstractRequirementControlEstablishmentReportDTO;

import java.util.List;

public interface EstablishmentControlDocumentReportRepository {

    List<AbstractRequirementControlEstablishmentReportDTO> findDocumentReportResources(Long id_caseFile);

}
