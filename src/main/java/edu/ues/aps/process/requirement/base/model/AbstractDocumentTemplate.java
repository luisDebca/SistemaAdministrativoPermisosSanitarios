package edu.ues.aps.process.requirement.base.model;

import java.util.List;

public interface AbstractDocumentTemplate {

    Long getId();

    void setId(Long id);

    DocumentTemplateType getType();

    String getType_text();

    void setType(DocumentTemplateType type);

    String getTitle();

    void setTitle(String title);

    String getSubtitle();

    void setSubtitle(String subtitle);

    Integer getVersion();

    void setVersion(Integer version);

    List<RequirementControlTemplate> getRequirements();

    void setRequirements(List<RequirementControlTemplate> requirements);

    public void addRequirement(Requirement requirement);

    public void removeRequirement(Requirement requirement);

}
