package edu.ues.aps.process.requirement.base.controller;

import edu.ues.aps.process.requirement.base.model.Requirement;
import edu.ues.aps.process.requirement.base.service.RequirementFinderService;
import edu.ues.aps.process.requirement.base.service.RequirementPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/requirement")
public class RequirementPersistenceController {

    private static final Logger logger = LogManager.getLogger(RequirementPersistenceController.class);

    private static final String REQUIREMENT_NEW_FORM = "new_requirement";
    private static final String REQUIREMENT_EDIT_FORM = "edit_requirement";

    private RequirementPersistenceService persistenceService;
    private RequirementFinderService finderService;

    public RequirementPersistenceController(RequirementPersistenceService persistenceService, RequirementFinderService finderService) {
        this.persistenceService = persistenceService;
        this.finderService = finderService;
    }

    @GetMapping("/new")
    public String showNewRequirementForm(ModelMap model) {
        logger.info("GET:: Show new requirement form");
        model.addAttribute("requirement", new Requirement());
        return REQUIREMENT_NEW_FORM;
    }

    @PostMapping("/new")
    public String saveNewRequirement(@ModelAttribute Requirement requirement) {
        logger.info("POST: Save requirement: {}", requirement.toString());
        persistenceService.save(requirement);
        return "redirect:/requirement/all";
    }

    @GetMapping("/{id_requirement}/edit")
    public String showUpdateRequirementForm(@PathVariable Long id_requirement, ModelMap model) {
        logger.info("GET:: Show requirement {} edit form", id_requirement);
        model.addAttribute("requirement", finderService.findById(id_requirement));
        return REQUIREMENT_EDIT_FORM;
    }

    @PostMapping("/{id_requirement}/edit")
    public String updateRequirement(@ModelAttribute Requirement requirement, @PathVariable Long id_requirement) {
        logger.info("POST:: Update requirement: {}", id_requirement);
        persistenceService.update(requirement);
        return "redirect:/requirement/all";
    }

    @GetMapping("/{id_requirement}/delete")
    public String deleteRequirement(@PathVariable Long id_requirement){
        logger.info("GET:: Delete requirement {}",id_requirement);
        persistenceService.delete(id_requirement);
        return "redirect:/requirement/all";
    }
}
