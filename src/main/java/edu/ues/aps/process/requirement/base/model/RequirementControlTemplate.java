package edu.ues.aps.process.requirement.base.model;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "REQUIREMENT_CONTROL_TEMPLATE")
public class RequirementControlTemplate {

    @EmbeddedId
    private RequirementControlTemplateId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("template_id")
    private DocumentTemplate template;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("requirement_id")
    private Requirement requirement;

    @OneToMany(mappedBy = "template", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RequirementRow> requirementRows = new ArrayList<>();

    public RequirementControlTemplate() {
    }

    public RequirementControlTemplate(DocumentTemplate template, Requirement requirement) {
        this.template = template;
        this.requirement = requirement;
        this.id = new RequirementControlTemplateId(template.getId(), requirement.getId());
    }

    public RequirementControlTemplateId getId() {
        return id;
    }

    public void setId(RequirementControlTemplateId id) {
        this.id = id;
    }

    public DocumentTemplate getTemplate() {
        return template;
    }

    public void setTemplate(DocumentTemplate template) {
        this.template = template;
    }

    public Requirement getRequirement() {
        return requirement;
    }

    public void setRequirement(Requirement requirement) {
        this.requirement = requirement;
    }

    public List<RequirementRow> getRequirementRows() {
        return requirementRows;
    }

    public void setRequirementRows(List<RequirementRow> requirementRows) {
        this.requirementRows = requirementRows;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequirementControlTemplate that = (RequirementControlTemplate) o;

        return template.equals(that.template) && requirement.equals(that.requirement);
    }

    @Override
    public int hashCode() {
        int result = template.hashCode();
        result = 31 * result + requirement.hashCode();
        return result;
    }

    @Embeddable
    public static class RequirementControlTemplateId implements Serializable {

        @Column(name = "id_template")
        private Long template_id;

        @Column(name = "id_requirement")
        private Long requirement_id;

        public RequirementControlTemplateId() {
        }

        public RequirementControlTemplateId(Long template_id, Long requirement_id) {
            this.template_id = template_id;
            this.requirement_id = requirement_id;
        }

        public Long getTemplate_id() {
            return template_id;
        }

        public Long getRequirement_id() {
            return requirement_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RequirementControlTemplateId that = (RequirementControlTemplateId) o;

            return template_id.equals(that.template_id) && requirement_id.equals(that.requirement_id);
        }

        @Override
        public int hashCode() {
            int result = template_id.hashCode();
            result = 31 * result + requirement_id.hashCode();
            return result;
        }
    }


}
