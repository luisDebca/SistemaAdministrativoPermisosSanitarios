package edu.ues.aps.process.requirement.establishment.controller;

import edu.ues.aps.process.requirement.establishment.service.ControlDocumentDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/requirement-control/establishment")
public class EstablishmentRequirementDocumentInformationProviderController {

    private static final Logger logger = LogManager.getLogger(EstablishmentRequirementDocumentInformationProviderController.class);

    private static final String COMPLETED_DOCUMENT_LIST = "establishment_complete_control_document_list";
    private static final String INCOMPLETE_DOCUMENT_LIST = "establishment_incomplete_control_document_list";
    private static final String ANALYSIS_DOCUMENT_LIST = "";

    private ControlDocumentDTOFinderService finderService;

    public EstablishmentRequirementDocumentInformationProviderController(ControlDocumentDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/completed/all")
    public String showCompletedControlDocumentList(ModelMap model) {
        logger.info("GET:: Show completed request for requirement control process");
        model.addAttribute("documents", finderService.findAllRequestWithCompleteRequirements());
        return COMPLETED_DOCUMENT_LIST;
    }

    @GetMapping("/incomplete/all")
    public String showIncompleteControlDocumentList(ModelMap model) {
        logger.info("GET:: Show incomplete request for requirement control process");
        model.addAttribute("documents", finderService.findAllRequestWithIncompleteRequirements());
        return INCOMPLETE_DOCUMENT_LIST;
    }

    @GetMapping("/analysis/all")
    public String showControlDocumentWithAnalysisProcessList(ModelMap model) {
        logger.info("GET:: Show request in requirement analysis process");
        model.addAttribute("documents", finderService.findAllRequestWithAnalysisProcess());
        return ANALYSIS_DOCUMENT_LIST;
    }
}
