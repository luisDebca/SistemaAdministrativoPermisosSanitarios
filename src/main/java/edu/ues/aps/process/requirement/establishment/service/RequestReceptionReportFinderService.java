package edu.ues.aps.process.requirement.establishment.service;

import edu.ues.aps.process.requirement.base.dto.AbstractRequestReceptionReportDTO;

public interface RequestReceptionReportFinderService {

    AbstractRequestReceptionReportDTO findRequestReceptionReportResources(Long id_casefile);
}
