package edu.ues.aps.process.requirement.base.model;

import java.util.Collections;
import java.util.List;

public class EmptyDocumentTemplate implements AbstractDocumentTemplate {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public DocumentTemplateType getType() {
        return DocumentTemplateType.INVALID;
    }

    @Override
    public String getTitle() {
        return "Document Template title not found";
    }

    @Override
    public String getSubtitle() {
        return "Document Template subtitle not found";
    }

    @Override
    public Integer getVersion() {
        return 0;
    }

    @Override
    public List<RequirementControlTemplate> getRequirements() {
        return Collections.emptyList();
    }

    @Override
    public String getType_text() {
        return "Document template type not found";
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setVersion(Integer version) {

    }

    @Override
    public void setType(DocumentTemplateType type) {

    }

    @Override
    public void setTitle(String title) {

    }

    @Override
    public void setSubtitle(String subtitle) {

    }

    @Override
    public void setRequirements(List<RequirementControlTemplate> requirements) {

    }

    @Override
    public void addRequirement(Requirement requirement) {

    }

    @Override
    public void removeRequirement(Requirement requirement) {

    }

    @Override
    public String toString() {
        return "EmptyDocumentTemplate{}";
    }
}
