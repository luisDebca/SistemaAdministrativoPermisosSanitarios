package edu.ues.aps.process.requirement.vehicle.repository;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.requirement.vehicle.dto.AbstractControlDocumentProcessDTO;
import edu.ues.aps.process.requirement.vehicle.dto.AbstractRequirementControlVehicleReportDTO;
import edu.ues.aps.process.requirement.vehicle.dto.AbstractVehicleRequestReceptionReportDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleControlDocumentDTORepository implements VehicleControlDocumentDTOFinderRepository, VehicleControlDocumentReportRepository, VehicleRequestReceptionReportRepository {

    private final SessionFactory sessionFactory;

    public VehicleControlDocumentDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractControlDocumentProcessDTO> findAll(boolean document_status) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.vehicle.dto.ControlDocumentProcessDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date,document.control_document_created_on) " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "inner join cv.controlDocument document " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "and document.status = :status " +
                "group by bunch.id " +
                "order by bunch.id", AbstractControlDocumentProcessDTO.class)
                .setParameter("id", ProcessIdentifier.REQUIREMENT_REVIEW)
                .setParameter("status", document_status)
                .getResultList();
    }

    @Override
    public List<AbstractRequirementControlVehicleReportDTO> findVehicleDocumentReportResources(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.vehicle.dto.RequirementControlVehicleReportDTO(" +
                "owner.name,owner.class,casefile.certification_type,document.status,document.control_document_created_on," +
                "template.title,template.subtitle,requirement.requirement,rows.status,rows.observation," +
                "count(vehicle),vehicle.license)" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles casefile " +
                "inner join casefile.vehicle vehicle " +
                "inner join vehicle.owner owner " +
                "inner join casefile.controlDocument document " +
                "inner join document.requirementRows rows " +
                "inner join rows.template.requirement requirement " +
                "inner join rows.template.template template " +
                "where bunch.id = :id " +
                "group by requirement.id", AbstractRequirementControlVehicleReportDTO.class)
                .setParameter("id", id_bunch)
                .getResultList();
    }

    @Override
    public AbstractVehicleRequestReceptionReportDTO findReportResource(Long id_bunch) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.vehicle.dto.VehicleRequestReceptionReportDTO(" +
                "document.acceptance_document_created_on, count(vehicle), vehicle.license," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name) ) " +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join cv.controlDocument document " +
                "where bunch.id = :id " +
                "group by bunch.id",AbstractVehicleRequestReceptionReportDTO.class)
                .setParameter("id",id_bunch)
                .getSingleResult();
    }
}
