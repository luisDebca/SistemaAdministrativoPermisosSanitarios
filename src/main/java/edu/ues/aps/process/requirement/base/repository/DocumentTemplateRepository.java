package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.dto.AbstractDocumentTemplateDTO;
import edu.ues.aps.process.requirement.base.model.AbstractDocumentTemplate;
import edu.ues.aps.process.requirement.base.model.DocumentTemplate;
import edu.ues.aps.process.requirement.base.model.DocumentTemplateType;
import edu.ues.aps.process.requirement.base.model.Requirement;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class DocumentTemplateRepository implements DocumentTemplateCrudRepository, DocumentTemplateDTOFinderRepository {

    private SessionFactory sessionFactory;

    public DocumentTemplateRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public DocumentTemplate findById(Long id_template) {
        return sessionFactory.getCurrentSession().createQuery("select doc " +
                "from DocumentTemplate doc " +
                "left join fetch doc.requirements tem " +
                "left join fetch tem.requirement req " +
                "where doc.id = :id", DocumentTemplate.class)
                .setParameter("id", id_template)
                .getSingleResult();
    }

    @Override
    public Requirement findRequirement(Long id_requirement) {
        return sessionFactory.getCurrentSession().createQuery("select req " +
                "from Requirement req " +
                "left join fetch req.templates temp " +
                "where req.id = :id", Requirement.class)
                .setParameter("id", id_requirement)
                .getSingleResult();
    }

    @Override
    public void save(AbstractDocumentTemplate template) {
        sessionFactory.getCurrentSession().persist(template);
    }

    @Override
    public void merge(AbstractDocumentTemplate template) {
        sessionFactory.getCurrentSession().merge(template);
    }

    @Override
    public void delete(AbstractDocumentTemplate template) {
        sessionFactory.getCurrentSession().delete(template);
    }

    @Override
    public AbstractDocumentTemplateDTO find(Long id_template) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.base.dto.DocumentTemplateDTO(" +
                "template.id, template.type, template.title, template.subtitle, template.version) " +
                "from DocumentTemplate template " +
                "where template.id = :id ", AbstractDocumentTemplateDTO.class)
                .setParameter("id",id_template)
                .getSingleResult();
    }

    @Override
    public List<AbstractDocumentTemplateDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.base.dto.DocumentTemplateDTO(" +
                "template.id,template.type,template.title,template.subtitle,template.version)" +
                "from DocumentTemplate template", AbstractDocumentTemplateDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractDocumentTemplateDTO> findAllByType(DocumentTemplateType type) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.requirement.base.dto.DocumentTemplateDTO(template.id,template.type,template.title,template.subtitle,template.version) " +
                "from DocumentTemplate template " +
                "where template.type = :type", AbstractDocumentTemplateDTO.class)
                .setParameter("type", type)
                .getResultList();
    }
}
