package edu.ues.aps.process.requirement.establishment.dto;

import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class RequirementDocumentInfoDTO implements AbstractRequirementDocumentInfoDTO {

    private String owner_name;
    private String owner_nit;
    private String establishment_name;
    private String establishment_address;
    private String establishment_nit;
    private String establishment_type;
    private CertificationType casefile_certification_type;
    private LocalDateTime document_acceptance_on;
    private LocalDateTime document_created_on;
    private boolean document_status;
    private long document_approved_requirement_count;
    private long document_fault_requirement_count;

    public RequirementDocumentInfoDTO(String owner_name, String owner_nit, String establishment_name, String establishment_address, String establishment_nit, String establishment_type, CertificationType casefile_certification_type, LocalDateTime document_acceptance_on, LocalDateTime document_created_on, boolean document_status, long document_approved_requirement_count, long document_fault_requirement_count) {
        this.owner_name = owner_name;
        this.owner_nit = owner_nit;
        this.establishment_name = establishment_name;
        this.establishment_address = establishment_address;
        this.establishment_nit = establishment_nit;
        this.establishment_type = establishment_type;
        this.casefile_certification_type = casefile_certification_type;
        this.document_acceptance_on = document_acceptance_on;
        this.document_created_on = document_created_on;
        this.document_status = document_status;
        this.document_approved_requirement_count = document_approved_requirement_count;
        this.document_fault_requirement_count = document_fault_requirement_count;
    }

    public String getOwner_name() {
        return owner_name;
    }

    public String getOwner_nit() {
        return owner_nit;
    }

    public String getEstablishment_name() {
        return establishment_name;
    }

    public String getEstablishment_address() {
        return establishment_address;
    }

    public String getEstablishment_nit() {
        return establishment_nit;
    }

    public String getEstablishment_type() {
        return establishment_type;
    }

    public String getDocument_acceptance_on() {
        if (document_acceptance_on != null) {
            return DatetimeUtil.parseDateToLongDateFormat(document_acceptance_on.toLocalDate());
        } else {
            return DatetimeUtil.DATE_NOT_DEFINED;
        }
    }

    public String getDocument_created_on() {
        return DatetimeUtil.parseDateToLongDateFormat(document_created_on.toLocalDate());
    }

    public boolean isDocument_status() {
        return document_status;
    }

    public long getDocument_approved_requirement_count() {
        return document_approved_requirement_count;
    }

    public long getDocument_fault_requirement_count() {
        return document_fault_requirement_count;
    }

    public String getCasefile_certification_type() {
        return casefile_certification_type.getCertificationType();
    }
}
