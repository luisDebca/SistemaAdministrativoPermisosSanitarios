package edu.ues.aps.process.requirement.base.model;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "REQUIREMENT_ROW")
public class RequirementRow {

    @EmbeddedId
    private RequirementRowId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("document_id")
    private RequirementControlDocument document;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("template_id")
    private RequirementControlTemplate template;

    @Enumerated(EnumType.STRING)
    @Column(name = "status", nullable = false, length = 15)
    private RequirementStatusType status;

    @Column(name = "observation", length = 250)
    private String observation;

    public RequirementRow() {
    }

    public RequirementRow(RequirementControlDocument document, RequirementControlTemplate template) {
        this.document = document;
        this.template = template;
        this.id = new RequirementRowId(document.getId(), template.getTemplate().getId(), template.getRequirement().getId());
    }

    public RequirementRow(RequirementControlDocument document, RequirementControlTemplate template, RequirementStatusType status, String observation) {
        this.document = document;
        this.template = template;
        this.status = status;
        this.observation = observation;
        this.id = new RequirementRowId(document.getId(), template.getTemplate().getId(), template.getRequirement().getId());
    }

    public RequirementRowId getId() {
        return id;
    }

    public void setId(RequirementRowId id) {
        this.id = id;
    }

    public RequirementControlDocument getDocument() {
        return document;
    }

    public void setDocument(RequirementControlDocument document) {
        this.document = document;
    }

    public RequirementControlTemplate getTemplate() {
        return template;
    }

    public void setTemplate(RequirementControlTemplate template) {
        this.template = template;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public RequirementStatusType getStatus() {
        return status;
    }

    public void setStatus(RequirementStatusType status) {
        this.status = status;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        RequirementRow that = (RequirementRow) o;

        return document.equals(that.document) && template.equals(that.template) && (status != null ? status.equals(that.status) : that.status == null);
    }

    @Override
    public int hashCode() {
        int result = document.hashCode();
        result = 31 * result + template.hashCode();
        result = 31 * result + (status != null ? status.hashCode() : 0);
        return result;
    }

    @Override
    public String toString() {
        return "RequirementRow{" +
                "status=" + status +
                ", observation='" + observation + '\'' +
                '}';
    }

    @Embeddable
    public static class RequirementRowId implements Serializable {

        @Column(name = "id_document")
        private Long document_id;

        @Column(name = "template")
        private RequirementControlTemplate.RequirementControlTemplateId template_id;

        public RequirementRowId() {
        }

        public RequirementRowId(Long document_id, Long template_id, Long requirement_id) {
            this.document_id = document_id;
            this.template_id = new RequirementControlTemplate.RequirementControlTemplateId(template_id, requirement_id);
        }

        public Long getDocument_id() {
            return document_id;
        }

        public RequirementControlTemplate.RequirementControlTemplateId getTemplate_id() {
            return template_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            RequirementRowId that = (RequirementRowId) o;

            return document_id.equals(that.document_id) && template_id.equals(that.template_id);
        }

        @Override
        public int hashCode() {
            int result = document_id.hashCode();
            result = 31 * result + template_id.hashCode();
            return result;
        }
    }
}
