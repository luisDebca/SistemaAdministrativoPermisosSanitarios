package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.process.requirement.establishment.dto.AbstractRequirementDocumentInfoDTO;

import javax.persistence.NoResultException;

public interface RequirementDocumentInfoDTOFinderRepository {

    AbstractRequirementDocumentInfoDTO findDocumentInfoById(Long id_casefile) throws NoResultException;
}
