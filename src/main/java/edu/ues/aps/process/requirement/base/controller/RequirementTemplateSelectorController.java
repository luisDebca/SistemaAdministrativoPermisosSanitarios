package edu.ues.aps.process.requirement.base.controller;

import edu.ues.aps.process.requirement.base.service.DocumentTemplateDTOTemplateKindFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/requirement-control")
public class RequirementTemplateSelectorController {

    private static final Logger logger = LogManager.getLogger(RequirementTemplateSelectorController.class);

    private static final String FOR_ESTABLISHMENT = "establishment_requirement_template_list";
    private static final String FOR_VEHICLES = "vehicle_requirement_template_list";

    private final DocumentTemplateDTOTemplateKindFinderService templateKindFinderService;

    public RequirementTemplateSelectorController(DocumentTemplateDTOTemplateKindFinderService templateKindFinderService) {
        this.templateKindFinderService = templateKindFinderService;
    }

    @GetMapping("/establishment/case-file/{id_caseFile}/template/selector")
    public String showForEstablishment(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show establishment document template selector");
        model.addAttribute("templates", templateKindFinderService.findAllTemplatesFromEstablishment());
        model.addAttribute("id_casefile", id_caseFile);
        return FOR_ESTABLISHMENT;
    }

    @GetMapping("/vehicle/bunch/{id_bunch}/template/selector")
    public String showForVehicles(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show vehicle document template selector");
        model.addAttribute("templates", templateKindFinderService.findAllTemplatesFromVehicle());
        model.addAttribute("id_bunch", id_bunch);
        return FOR_VEHICLES;
    }
}
