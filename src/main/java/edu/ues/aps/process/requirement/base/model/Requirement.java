package edu.ues.aps.process.requirement.base.model;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "REQUIREMENT")
public class Requirement implements AbstractRequirement {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "requirement", nullable = false, length = 250)
    private String requirement;

    @OneToMany(mappedBy = "requirement", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<RequirementControlTemplate> templates = new ArrayList<>();

    public Requirement() {
    }

    public Requirement(Long id, String requirement) {
        this.id = id;
        this.requirement = requirement;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getRequirement() {
        return requirement;
    }

    @Override
    public void setRequirement(String requirement) {
        this.requirement = requirement;
    }

    @Override
    public List<RequirementControlTemplate> getTemplates() {
        return templates;
    }

    @Override
    public void setTemplates(List<RequirementControlTemplate> templates) {
        this.templates = templates;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Requirement that = (Requirement) o;

        return requirement.equals(that.requirement);
    }

    @Override
    public int hashCode() {
        return requirement.hashCode();
    }

    @Override
    public String toString() {
        return "Requirement{" +
                "id=" + id +
                ", requirement='" + requirement + '\'' +
                '}';
    }
}
