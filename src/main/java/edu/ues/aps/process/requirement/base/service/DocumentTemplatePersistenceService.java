package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.requirement.base.model.AbstractDocumentTemplate;

public interface DocumentTemplatePersistenceService {

    void save(AbstractDocumentTemplate template);

    void update(AbstractDocumentTemplate template);

    void delete(Long id_document );
}
