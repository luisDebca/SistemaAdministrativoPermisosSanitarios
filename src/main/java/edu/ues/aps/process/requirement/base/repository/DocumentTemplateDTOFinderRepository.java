package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.dto.AbstractDocumentTemplateDTO;
import edu.ues.aps.process.requirement.base.model.DocumentTemplateType;

import javax.persistence.NoResultException;
import java.util.List;

public interface DocumentTemplateDTOFinderRepository {

    AbstractDocumentTemplateDTO find(Long id_template) throws NoResultException;

    List<AbstractDocumentTemplateDTO> findAll();

    List<AbstractDocumentTemplateDTO> findAllByType(DocumentTemplateType type);
}
