package edu.ues.aps.process.requirement.base.async;

import edu.ues.aps.process.requirement.base.service.RequirementControlTemplatePersistenceService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import javax.persistence.NoResultException;

@Controller
public class RequirementPersistenceAsyncController {

    private static final Logger logger = LogManager.getLogger(RequirementPersistenceAsyncController.class);

    private RequirementControlTemplatePersistenceService persistenceService;

    public RequirementPersistenceAsyncController(RequirementControlTemplatePersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping(value = "/document-template/requirement/remove", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse removeRequirementFromTemplate(@RequestParam("id_template") Long id_template, @RequestParam("id_requirement") Long id_requirement) {
        try {
            logger.info("GET:: Trying remove requirement from template: {} ,requirement: {}", id_template, id_requirement);
            persistenceService.removeRequirementFromTemplate(id_template, id_requirement);
            return new SingleStringResponse().asSuccessResponse();
        } catch (NoResultException e) {
            logger.error("ERROR:: Exception thrown trying remove requirement from template!, {}", e.getMessage());
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

}
