package edu.ues.aps.process.requirement.base.controller;

import edu.ues.aps.process.requirement.base.service.DocumentTemplateDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/document-template")
public class DocumentTemplateInformationProviderController {

    private static final Logger logger = LogManager.getLogger(DocumentTemplateInformationProviderController.class);

    private static final String DOCUMENT_TEMPLATE_LIST = "template_list";

    private DocumentTemplateDTOFinderService finderService;

    public DocumentTemplateInformationProviderController(DocumentTemplateDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/all")
    public String showDocumentTemplateList(ModelMap model) {
        logger.info("GET:: Show document template list");
        model.addAttribute("templates", finderService.findAll());
        return DOCUMENT_TEMPLATE_LIST;
    }

}
