package edu.ues.aps.process.requirement.base.service;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;
import edu.ues.aps.process.requirement.base.repository.RequirementRowDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Collections;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class RequirementRowDTOService implements RequirementRowDTOFinderService {

    private final VehicleBunchInformationService bunchSummaryService;
    private final RequirementRowDTOFinderRepository finderRepository;

    public RequirementRowDTOService(RequirementRowDTOFinderRepository finderRepository, VehicleBunchInformationService bunchSummaryService) {
        this.finderRepository = finderRepository;
        this.bunchSummaryService = bunchSummaryService;
    }

    @Override
    public List<RequirementRowDTO> findAllFromDocument(Long id_document) {
        List<RequirementRowDTO> rows = finderRepository.findAllRowsFromDocument(id_document);
        for (RequirementRowDTO row : rows) {
            row.setId_document(id_document);
        }
        return rows;
    }

    @Override
    public List<RequirementRowDTO> findAllFromBunch(Long id_bunch) {
        List<Long> ids = bunchSummaryService.findAllCaseFileId(id_bunch);
        if(ids.isEmpty()){
            return Collections.emptyList();
        }
        return findAllFromDocument(ids.get(0));
    }
}
