package edu.ues.aps.process.requirement.base.service;

public interface RequirementControlTemplatePersistenceService {

    void removeRequirementFromTemplate(Long id_template, Long id_requirement);
}
