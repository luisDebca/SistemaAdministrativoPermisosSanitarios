package edu.ues.aps.process.requirement.vehicle.service;

import edu.ues.aps.process.requirement.vehicle.dto.AbstractVehicleRequestReceptionReportDTO;

public interface VehicleRequestReceptionReportService {

    AbstractVehicleRequestReceptionReportDTO findReportResource(Long id_bunch);

    int bunchVehicleCount(Long id_bunch);
}
