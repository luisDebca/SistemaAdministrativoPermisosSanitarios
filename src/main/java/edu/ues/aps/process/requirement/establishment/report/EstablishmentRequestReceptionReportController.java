package edu.ues.aps.process.requirement.establishment.report;

import edu.ues.aps.process.base.dto.ClientDTO;
import edu.ues.aps.process.requirement.base.report.RequestReceptionReportController;
import edu.ues.aps.process.requirement.establishment.service.RequestReceptionReportFinderService;
import edu.ues.aps.users.management.service.SinglePersonalInformationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.security.Principal;
import java.util.Collections;

@Controller
public class EstablishmentRequestReceptionReportController extends RequestReceptionReportController {

    private static final Logger logger = LogManager.getLogger(EstablishmentRequestReceptionReportController.class);

    private static final String REPORT_NAME = "RequestReceptionEReport";

    private final SinglePersonalInformationService personalInformationService;
    private final RequestReceptionReportFinderService receptionFinderService;

    public EstablishmentRequestReceptionReportController(RequestReceptionReportFinderService receptionFinderService, SinglePersonalInformationService personalInformationService) {
        this.receptionFinderService = receptionFinderService;
        this.personalInformationService = personalInformationService;
    }

    @Override
    @GetMapping("/requirement-control/establishment/case-file/{id_casefile}/request-reception-report.pdf")
    public String generateReport(@PathVariable Long id_casefile, ClientDTO selected_client,
                                 ModelMap model, Principal principal) {
        logger.info("REPORT:: Generate request {} reception report", id_casefile);
        setDatasource(receptionFinderService.findRequestReceptionReportResources(id_casefile));
        addClientDataToDatasource(selected_client);
        addCurrentUserNameToDatasource(personalInformationService.getFullName(principal.getName()));
        addReportRequiredSources(Collections.singletonList(getDatasource()));
        model.addAllAttributes(getModel());
        return REPORT_NAME;
    }
}
