package edu.ues.aps.process.requirement.base.report;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import edu.ues.aps.process.base.dto.ClientDTO;
import edu.ues.aps.process.base.report.AbstractReportController;
import edu.ues.aps.process.requirement.base.dto.AbstractRequirementControlDocumentReportDTO;
import org.springframework.ui.ModelMap;

import java.io.IOException;
import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

public abstract class RequirementDocumentReportController extends AbstractReportController {

    private ClientDTO[] selected_clients = new ClientDTO[0];
    private List<AbstractRequirementControlDocumentReportDTO> datasource = new ArrayList<>();
    private String principal_name;

    public abstract String generateReport(Long id, String selected_clients_json, ModelMap model, Principal principal);

    public void setSelectedClientsFromJSON(String json) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            objectMapper.configure(DeserializationFeature.USE_JAVA_ARRAY_FOR_JSON_ARRAY, true);
            selected_clients = objectMapper.readValue(json, ClientDTO[].class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void addReportSource(AbstractRequirementControlDocumentReportDTO dto){
        if(selected_clients.length == 2){
            dto.setApproved_name_by(selected_clients[0].getClient_name());
            dto.setApproved_type_by(selected_clients[0].getType());
            dto.setSigned_name_by(selected_clients[1].getClient_name());
        }
        dto.setUser_name(principal_name);
        datasource.add(dto);
    }

    public List<AbstractRequirementControlDocumentReportDTO> getDatasource() {
        return datasource;
    }

    public void setDatasource(List<AbstractRequirementControlDocumentReportDTO> datasource) {
        this.datasource = datasource;
    }

    public void clearDataSource(){
        this.datasource.clear();
    }

    public String getPrincipal_name() {
        return principal_name;
    }

    public void setPrincipal_name(String principal_name) {
        this.principal_name = principal_name;
    }
}
