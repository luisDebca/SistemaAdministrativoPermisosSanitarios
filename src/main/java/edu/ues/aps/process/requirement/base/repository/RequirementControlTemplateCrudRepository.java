package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;

import java.util.List;

public interface RequirementControlTemplateCrudRepository {

    List<RequirementControlTemplate> findAllFromTemplate(Long id_template);

    void delete(Long id_template, Long id_requirement);
}
