package edu.ues.aps.process.requirement.base.async;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;
import edu.ues.aps.process.requirement.base.service.RequirementControlDocumentPersistenceService;
import edu.ues.aps.process.requirement.base.service.RequirementRowPersistenceService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;
import java.util.List;

@Controller
public class RequirementRowPersistenceAsyncController {

    private static final Logger logger = LogManager.getLogger(RequirementRowPersistenceAsyncController.class);

    private final ProcessUserRegisterService registerService;
    private final RequirementRowPersistenceService persistenceService;
    private final VehicleBunchInformationService bunchInformationService;
    private final RequirementControlDocumentPersistenceService updateService;

    public RequirementRowPersistenceAsyncController(RequirementRowPersistenceService persistenceService, RequirementControlDocumentPersistenceService updateService, ProcessUserRegisterService registerService, VehicleBunchInformationService bunchInformationService) {
        this.persistenceService = persistenceService;
        this.updateService = updateService;
        this.registerService = registerService;
        this.bunchInformationService = bunchInformationService;
    }

    @PostMapping(value = "/requirement-control/update-modified-requirement-rows", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse updateModifiedRequirementRows(@RequestBody List<RequirementRowDTO> rows, Principal principal) {
        try {
            if (isRowListNotEmpty(rows)) {
                logger.info("POST:: Update case file {} control document requirement rows", rows.get(0).getId_document());
                persistenceService.updateRequirementRows(rows);
                updateService.updateRequirementDocumentStatus(rows.get(0).getId_document());
                registerService.addProcessUser(rows.get(0).getId_document(), principal.getName(), "process.record.requirement.review.edit");
            } else {
                logger.error("ERROR:: Trying update requirement row but its empty list!");
            }
            return new SingleStringResponse().asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

    @PostMapping(value = "/requirement-control/bunch/{id_bunch}/update-modified-requirement-rows", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse updateModifiedRequirementRows(@RequestBody List<RequirementRowDTO> rows, @PathVariable Long id_bunch, Principal principal) {
        try {
            if (isRowListNotEmpty(rows)) {
                List<Long> document_ids = bunchInformationService.findAllCaseFileId(id_bunch);
                for (Long id_document : document_ids) {
                    logger.info("POST:: Update case file {} control document requirement rows", id_document);
                    for (RequirementRowDTO dto : rows) {
                        dto.setId_document(id_document);
                    }
                    persistenceService.updateRequirementRows(rows);
                    updateService.updateRequirementDocumentStatus(id_document);
                    registerService.addProcessUser(id_document, principal.getName(), "process.record.requirement.review.edit");
                }
            } else {
                logger.error("ERROR:: Trying update requirement row but its empty list!");
            }
            return new SingleStringResponse().asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

    private boolean isRowListNotEmpty(List<RequirementRowDTO> rows) {
        return !rows.isEmpty();
    }

}
