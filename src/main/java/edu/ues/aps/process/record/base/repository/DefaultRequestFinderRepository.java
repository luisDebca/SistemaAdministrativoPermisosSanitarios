package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.base.model.Casefile;

import javax.persistence.NoResultException;

public interface DefaultRequestFinderRepository {

    Casefile getDefault() throws NoResultException;

    void deleteDefault();

    void createDefault();

    Boolean isDefault();
}
