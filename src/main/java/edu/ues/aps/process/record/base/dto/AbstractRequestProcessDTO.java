package edu.ues.aps.process.record.base.dto;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;

public interface AbstractRequestProcessDTO {

    Long getId_caseFile();

    void setId_caseFile(Long id_caseFile);

    Long getId_process();

    Boolean getActive();

    String getStart();

    String getEnd();

    ProcessIdentifier getIdentifier();

    String getProcess();

    String getObservation();

    Long getModifications();
}
