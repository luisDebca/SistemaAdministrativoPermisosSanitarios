package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.model.Process;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;

@Repository
public class DefaultRequestRepository implements DefaultRequestFinderRepository {

    private final SessionFactory sessionFactory;

    public DefaultRequestRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Casefile getDefault() throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select cf " +
                "from Casefile cf " +
                "where cf.request_number = :def " +
                "and cf.request_folder = :def", Casefile.class)
                .setParameter("def", "0")
                .getSingleResult();
    }

    @Override
    public void deleteDefault() {
        sessionFactory.getCurrentSession().createQuery("delete from Casefile cf " +
                "where cf.request_folder = :def " +
                "and cf.request_number = :def")
                .setParameter("def", "0")
                .executeUpdate();
    }

    @Override
    public void createDefault() {
        sessionFactory.getCurrentSession().save(getDefaultCaseFile());
    }

    @Override
    public Boolean isDefault() {
        try {
            Casefile def = getDefault();
            return def.getRequest_number().equals("0") && def.getRequest_folder().equals("0");
        } catch (NoResultException e) {
            return false;
        }
    }

    private Casefile getDefaultCaseFile() {
        Casefile cf = new Casefile();
        cf.setRequest_folder("0");
        cf.setRequest_number("0");
        cf.setStatus(CasefileStatusType.INVALID);
        cf.setApply_payment(false);
        cf.setCertification_type(CertificationType.INVALID);
        cf.setSection(CasefileSection.INVALID);
        cf.setCreation_date(LocalDateTime.now());
        cf.setCertification_duration_in_years(0);
        cf.addProcessRecord(sessionFactory.getCurrentSession().get(Process.class,160L));
        return cf;
    }
}
