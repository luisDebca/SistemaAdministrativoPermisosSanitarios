package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.model.CasefileProcess;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class CaseFileProcessRepository implements CaseFileProcessFinderRepository, CaseFileProcessPersistenceRepository {

    private SessionFactory sessionFactory;

    public CaseFileProcessRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public CasefileProcess getRecord(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select record " +
                "from CasefileProcess record " +
                "where record.casefile.id = :id " +
                "and record.active = true",CasefileProcess.class)
                .setParameter("id",id_caseFile)
                .getSingleResult();
    }

    @Override
    public CasefileProcess getGeneralProcessRegister() throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select record " +
                "from CasefileProcess record " +
                "where record.casefile.request_folder = '0' " +
                "and record.active = true",CasefileProcess.class)
                .getSingleResult();
    }

    @Override
    public void merge(CasefileProcess record) {
        sessionFactory.getCurrentSession().merge(record);
    }

    @Override
    public void update(CasefileProcess record) {
        sessionFactory.getCurrentSession().update(record);
    }
}
