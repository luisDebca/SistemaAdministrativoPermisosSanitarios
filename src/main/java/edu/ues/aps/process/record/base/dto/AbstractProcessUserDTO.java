package edu.ues.aps.process.record.base.dto;

public interface AbstractProcessUserDTO {

    Long getId_user();

    Long getId_casefile();

    Long getId_process();

    void setId_casefile(Long id_casefile);

    void setId_process(Long id_process);

    String getModified_on();

    String getDescription();

    String getUser_username();

    String getUser_full_name();
}
