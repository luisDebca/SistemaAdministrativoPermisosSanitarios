package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.repository.CaseFileProcessFinderRepository;
import edu.ues.aps.process.record.base.repository.CaseFileProcessPersistenceRepository;
import edu.ues.aps.users.management.model.User;
import edu.ues.aps.users.management.service.SingleUserFinderService;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.Locale;

@Service
@Transactional
public class ProcessUserService implements ProcessUserRegisterService {

    private final MessageSource messageSource;
    private final DefaultUserFinderService defaultUser;
    private final SingleUserFinderService userFinderService;
    private final DefaultRequestFinderService defaultRequest;
    private final CaseFileProcessFinderRepository finderRepository;
    private final CaseFileProcessPersistenceRepository persistenceRepository;

    public ProcessUserService(CaseFileProcessFinderRepository finderRepository, SingleUserFinderService userFinderService, CaseFileProcessPersistenceRepository persistenceRepository, MessageSource messageSource, DefaultRequestFinderService defaultRequest, DefaultUserFinderService defaultUser) {
        this.finderRepository = finderRepository;
        this.userFinderService = userFinderService;
        this.persistenceRepository = persistenceRepository;
        this.messageSource = messageSource;
        this.defaultRequest = defaultRequest;
        this.defaultUser = defaultUser;
    }

    @Override
    public void addProcessUser(Long id_caseFile, String username, String message) {
        recordChanges(id_caseFile, username, message, new String[]{});
    }

    @Override
    public void addProcessUser(String username, String message) {
        Casefile def = defaultRequest.getDefault();
        recordChanges(def.getId(), username, message, new String[]{});
    }

    @Override
    public void addProcessUser(Long id_caseFile, String message) {
        User def = defaultUser.getDefault();
        recordChanges(id_caseFile, def.getUsername(), message, new String[]{});
    }

    @Override
    public void addProcessUser(String username, String message, String[] extra) {
        Casefile def = defaultRequest.getDefault();
        recordChanges(def.getId(), username, message,extra);
    }

    @Override
    public void addProcessUser(Long id_caseFile, String message, String[] extra) {
        User def = defaultUser.getDefault();
        recordChanges(id_caseFile, def.getUsername(), message,extra);
    }

    private void recordChanges(Long id_caseFile, String username, String rawMessage, String[] extra) {
        CasefileProcess record = finderRepository.getRecord(id_caseFile);
        record.addParticipant(userFinderService.find(username), getValidateMessage(rawMessage, extra));
        persistenceRepository.update(record);
    }

    private String getValidateMessage(String message, String[] extra) {
        String validateMessage = getMessageDescription(message, extra);
        if (validateMessage.length() > 500) {
            validateMessage = validateMessage.substring(0, 500);
        }
        return validateMessage;
    }

    private String getMessageDescription(String message, String[] extra) {
        return messageSource.getMessage(message, extra, Locale.getDefault());
    }

}
