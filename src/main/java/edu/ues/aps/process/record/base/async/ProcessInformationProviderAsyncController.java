package edu.ues.aps.process.record.base.async;

import edu.ues.aps.process.record.base.dto.AbstractCasefileProcessDTO;
import edu.ues.aps.process.record.base.dto.AbstractProcessUserDTO;
import edu.ues.aps.process.record.base.service.CaseFileProcessDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
public class ProcessInformationProviderAsyncController {

    private static final Logger logger = LogManager.getLogger(ProcessInformationProviderAsyncController.class);

    private CaseFileProcessDTOFinderService finderService;

    public ProcessInformationProviderAsyncController(CaseFileProcessDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/find-casefile-process-record", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractCasefileProcessDTO> findCaseFileProcessRecord(@RequestParam Long id_caseFile) {
        logger.info("GET:: Find all caseFile {} process record.", id_caseFile);
        return finderService.findCaseFileRecord(id_caseFile);
    }

    @GetMapping(value = "/find-process-user-record-from-casefile-modifications", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractProcessUserDTO> findProcessUserRecordFromCaseFileModifications(
            @RequestParam("id_casefile") Long id_caseFile, @RequestParam("id_process") Long id_process) {
        logger.info("GET:: Find all user record from caseFile {} in process {}", id_caseFile, id_process);
        return finderService.findUserRecord(id_caseFile, id_process);
    }

}
