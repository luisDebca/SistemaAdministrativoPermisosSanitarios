package edu.ues.aps.process.record.base.dto;

import edu.ues.aps.process.base.model.CasefileSection;

import java.time.LocalDateTime;

public interface AbstractHistoryDTO {

    Long getId_user();

    Long getId_caseFile();

    Long getId_process();

    String getUser_full_name();

    String getCaseFile_folder_number();

    String getCaseFile_section();

    String getProcess();

    String getDescription();

    String getModification_date();
}
