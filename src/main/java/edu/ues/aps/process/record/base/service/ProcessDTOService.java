package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.record.base.dto.AbstractProcessSummaryDTO;
import edu.ues.aps.process.record.base.repository.ProcessDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional
public class ProcessDTOService implements ProcessDTOFinderService {

    private final ProcessDTOFinderRepository finderRepository;

    public ProcessDTOService(ProcessDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractProcessSummaryDTO> findAll() {
        return finderRepository.findAll();
    }
}
