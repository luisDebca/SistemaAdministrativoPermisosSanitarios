package edu.ues.aps.process.record.base.dto;

public class ProcessSummaryDTO implements AbstractProcessSummaryDTO {

    private Long id;
    private String process;
    private String description;
    private Long requestCounter;

    public ProcessSummaryDTO(Long id, String process, String description, Long requestCounter) {
        this.id = id;
        this.process = process;
        this.description = description;
        this.requestCounter = requestCounter;
    }

    public Long getId() {
        return id;
    }

    public String getProcess() {
        return process;
    }

    public String getDescription() {
        return description;
    }

    public Long getRequestCounter() {
        return requestCounter;
    }
}
