package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;

public interface CaseFileProcessRegisterService {

    void addProcess(AbstractCasefile caseFile, ProcessIdentifier process);

    void addProcess(Long id_caseFile, ProcessIdentifier process);

    void moveTo(AbstractCasefile caseFile, ProcessIdentifier processIdentifier);

    void moveTo(Long id_caseFile, ProcessIdentifier processIdentifier);

    void moveToNext(AbstractCasefile caseFile);

    void moveToNext(Long id_caseFile);

    void moveToPrevious(AbstractCasefile caseFile);

    void moveToPrevious(Long id_caseFile);

}
