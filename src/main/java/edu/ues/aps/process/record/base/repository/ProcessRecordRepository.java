package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.model.Process;
import edu.ues.aps.users.management.model.User;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class ProcessRecordRepository implements ProcessRecordPersistenceRepository {

    private SessionFactory sessionFactory;

    public ProcessRecordRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void addNewProcessToCasefile(Long id_casefile, Long id_process) throws NoResultException{
        AbstractCasefile casefile = sessionFactory.getCurrentSession().createQuery("select casefile " +
                "from Casefile casefile " +
                "where casefile.id = :id",AbstractCasefile.class)
                .setParameter("id",id_casefile)
                .getSingleResult();

        Process process = sessionFactory.getCurrentSession().createQuery("select process " +
                "from Process process " +
                "where process.id = :id",Process.class)
                .setParameter("id",id_process)
                .getSingleResult();

        casefile.addProcessRecord(process);
        sessionFactory.getCurrentSession().saveOrUpdate(casefile);
    }

    @Override
    public void addNewProcessToCasefile(Long id_casefile) throws NoResultException {
        AbstractCasefile casefile = sessionFactory.getCurrentSession().createQuery("select casefile " +
                "from Casefile casefile " +
                "left join fetch casefile.processes record " +
                "left join fetch record.process process " +
                "join fetch process.next " +
                "where casefile.id = :id " +
                "and record.active = true ",AbstractCasefile.class)
                .setParameter("id",id_casefile)
                .getSingleResult();
        if(!casefile.getProcesses().isEmpty()){
            Process next_process = casefile.getProcesses().get(casefile.getProcesses().size() - 1).getProcess().getNext();
            casefile.addProcessRecord(next_process);
        }
        sessionFactory.getCurrentSession().saveOrUpdate(casefile);
    }

    @Override
    public void addNewUserModificationRecordToCasefileProcess(Long id_casefile, String username, String message) throws NoResultException {
        CasefileProcess record = sessionFactory.getCurrentSession().createQuery("select record " +
                "from CasefileProcess record " +
                "join fetch record.participants " +
                "where record.casefile.id = :id " +
                "and record.active = true",CasefileProcess.class)
                .setParameter("id",id_casefile)
                .getSingleResult();

        User user = sessionFactory.getCurrentSession().createQuery("select user " +
                "from User user " +
                "join fetch user.process " +
                "where user.username = :username",User.class)
                .setParameter("username",username)
                .getSingleResult();

        record.addParticipant(user, message);
        sessionFactory.getCurrentSession().saveOrUpdate(record);
    }

    @Override
    public void closeCurrentProcess(Long id_casefile) {
        sessionFactory.getCurrentSession().createQuery("update CasefileProcess record " +
                "set record.active = false , record.finished_on = current_timestamp " +
                "where record.active = true " +
                "and record.casefile.id = :id")
                .setParameter("id",id_casefile)
                .executeUpdate();
    }
}
