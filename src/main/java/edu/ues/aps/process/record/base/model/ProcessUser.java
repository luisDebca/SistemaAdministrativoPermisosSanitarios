package edu.ues.aps.process.record.base.model;

import edu.ues.aps.users.management.model.User;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "PROCESS_USER")
public class ProcessUser {

    @EmbeddedId
    private ProcessUserId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("process_id")
    private CasefileProcess casefile_process;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("user_id")
    private User user;

    @Column(name = "description", length = 500)
    private String description;

    public ProcessUser() {
    }

    public ProcessUser(CasefileProcess casefile_process, User user) {
        this.casefile_process = casefile_process;
        this.user = user;
        this.id = new ProcessUserId(user.getId(), casefile_process.getCasefile().getId(), casefile_process.getProcess().getId(),LocalDateTime.now());
    }

    public ProcessUserId getId() {
        return id;
    }

    public void setId(ProcessUserId id) {
        this.id = id;
    }

    public CasefileProcess getCasefile_process() {
        return casefile_process;
    }

    public void setCasefile_process(CasefileProcess casefileProcess) {
        this.casefile_process = casefileProcess;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ProcessUser that = (ProcessUser) o;
        return Objects.equals(casefile_process, that.casefile_process) &&
                Objects.equals(user, that.user);
    }

    @Override
    public int hashCode() {
        return Objects.hash(casefile_process, user);
    }

    @Override
    public String toString() {
        return "ProcessUser{" +
                "description='" + description + '\'' +
                '}';
    }

    @Embeddable
    public static class ProcessUserId implements Serializable {

        @Column(name = "id_casefile_process")
        private CasefileProcess.CasefileProcessId process_id;

        @Column(name = "id_user")
        private Long user_id;

        @Column(name = "modified_on")
        private LocalDateTime modified_on;

        public ProcessUserId() {
        }

        public ProcessUserId(Long user_id, Long casefile_id, Long process_id) {
            this.user_id = user_id;
            this.process_id = new CasefileProcess.CasefileProcessId(casefile_id, process_id);
        }

        public ProcessUserId(Long user_id, Long casefile_id, Long process_id, LocalDateTime modified_on) {
            this.user_id = user_id;
            this.process_id = new CasefileProcess.CasefileProcessId(casefile_id,process_id);
            this.modified_on = modified_on;
        }

        public CasefileProcess.CasefileProcessId getProcess_id() {
            return process_id;
        }

        public Long getUser_id() {
            return user_id;
        }

        public LocalDateTime getModified_on() {
            return modified_on;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            ProcessUserId that = (ProcessUserId) o;
            return Objects.equals(process_id, that.process_id) &&
                    Objects.equals(user_id, that.user_id) &&
                    Objects.equals(modified_on, that.modified_on);
        }

        @Override
        public int hashCode() {
            return Objects.hash(process_id, user_id, modified_on);
        }
    }
}
