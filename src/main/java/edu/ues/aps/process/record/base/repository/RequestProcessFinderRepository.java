package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.dto.AbstractRequestProcessDTO;

import java.util.List;

public interface RequestProcessFinderRepository {

    List<AbstractRequestProcessDTO> findAll(Long id_caseFile);
}
