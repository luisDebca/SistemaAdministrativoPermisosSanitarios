package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.record.base.repository.DefaultUserFinderRepository;
import edu.ues.aps.users.management.model.User;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional
public class DefaultUserService implements DefaultUserFinderService {

    private static final Logger logger = LogManager.getLogger(DefaultUserService.class);

    private User defaultUser;
    private Boolean isEnabled;

    private final DefaultUserFinderRepository repository;

    public DefaultUserService(DefaultUserFinderRepository repository) {
        this.repository = repository;
    }

    @Override
    public void init() {
        try {
            if (!repository.isDefault()){
                repository.deleteDefault();
                repository.createDefault();
                logger.info("...Default user created!");
            }
            defaultUser = repository.getDefault();
            isEnabled = true;
            logger.info("...Default user is OK!");
        } catch (Exception e) {
            isEnabled = false;
            logger.warn("...Default user not enable. Try restart the system. ");
            logger.warn("..." + e.getMessage());
        }
    }

    @Override
    public User getDefault() {
        return defaultUser;
    }

    @Override
    public void reloadUser() {
        try {
            logger.info("Reload default user...");
            defaultUser = repository.getDefault();
        } catch (NoResultException e) {
            isEnabled = false;
            logger.warn("...Default user not enable. Try restart the system.");
        }
    }

    @Override
    public Boolean defaultEnable() {
        return isEnabled;
    }
}
