package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.model.ProcessUser;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProcessUserRepository implements ProcessUserFinderRepository {

    private final SessionFactory sessionFactory;

    public ProcessUserRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<ProcessUser> findAll(CasefileProcess.CasefileProcessId record_id) {
        return sessionFactory.getCurrentSession().createQuery("select user_record " +
                "from ProcessUser user_record " +
                "inner join user_record.casefile_process casefile_record " +
                "where casefile_record.id = :id", ProcessUser.class)
                .setParameter("id", record_id)
                .getResultList();
    }

    @Override
    public List<ProcessUser> findAll(String username) {
        return sessionFactory.getCurrentSession().createQuery("select user_record " +
                "from ProcessUser user_record " +
                "inner join user_record.user user " +
                "where user.username = :username", ProcessUser.class)
                .setParameter("username", username)
                .getResultList();
    }
}
