package edu.ues.aps.process.record.base.dto;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class RequestProcessDTO implements AbstractRequestProcessDTO {

    private Long id_caseFile;
    private Long id_process;
    private Boolean active;
    private LocalDateTime start;
    private LocalDateTime end;
    private ProcessIdentifier identifier;
    private String process;
    private String observation;
    private Long modifications;

    public RequestProcessDTO(Long id_process, Boolean active, LocalDateTime start, LocalDateTime end, ProcessIdentifier identifier, String process, String observation, Long modifications) {
        this.id_process = id_process;
        this.active = active;
        this.start = start;
        this.end = end;
        this.identifier = identifier;
        this.process = process;
        this.observation = observation;
        this.modifications = modifications;
    }

    public Long getId_caseFile() {
        return id_caseFile;
    }

    public void setId_caseFile(Long id_caseFile) {
        this.id_caseFile = id_caseFile;
    }

    public Long getId_process() {
        return id_process;
    }

    public Boolean getActive() {
        return active;
    }

    public String getStart() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(start);
    }

    public String getEnd() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(end);
    }

    public ProcessIdentifier getIdentifier() {
        return identifier;
    }

    public String getProcess() {
        return process;
    }

    public String getObservation() {
        if(observation == null)
            return "N/A";
        return observation;
    }

    public Long getModifications() {
        return modifications;
    }
}
