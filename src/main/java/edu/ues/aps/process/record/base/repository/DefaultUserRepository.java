package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.users.management.model.Personal;
import edu.ues.aps.users.management.model.User;
import org.hibernate.SessionFactory;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;

@Repository
public class DefaultUserRepository implements DefaultUserFinderRepository {

    private static final String DEFAULT_NAME = "SYSTEM";
    private static final String DEFAULT_KEY = "4h@N*v";

    private final SessionFactory sessionFactory;
    private final PasswordEncoder passwordEncoder;

    public DefaultUserRepository(SessionFactory sessionFactory, PasswordEncoder passwordEncoder) {
        this.sessionFactory = sessionFactory;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public User getDefault() throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select user from User user where user.username=:def",User.class)
                .setParameter("def",DEFAULT_NAME)
                .getSingleResult();
    }

    @Override
    public Boolean isDefault() {
        try {
            User def = getDefault();
            return passwordEncoder.matches(DEFAULT_KEY,def.getPassword());
        } catch (NoResultException e) {
            return false;
        }
    }

    @Override
    public void deleteDefault() {
        sessionFactory.getCurrentSession().createQuery("delete from User user where user.username = :def")
                .setParameter("def",DEFAULT_NAME)
                .executeUpdate();
    }

    @Override
    public void createDefault() {
        sessionFactory.getCurrentSession().save(getDefaultUser());
    }

    private Personal getDefaultUser(){
        Personal syst = new Personal();
        syst.setFirst_name("SYSTEM");
        syst.setLast_name("OPERATION");
        User def = new User();
        def.setIs_active(false);
        def.setUsername(DEFAULT_NAME);
        def.setPassword(passwordEncoder.encode(DEFAULT_KEY));
        def.setJoining_date(LocalDateTime.now());
        def.setLast_login(LocalDateTime.now());
        syst.addUser(def);
        return syst;
    }
}
