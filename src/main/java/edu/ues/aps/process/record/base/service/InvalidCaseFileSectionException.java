package edu.ues.aps.process.record.base.service;

public class InvalidCaseFileSectionException extends RuntimeException {

    public InvalidCaseFileSectionException() {
    }

    public InvalidCaseFileSectionException(String message) {
        super(message);
    }
}
