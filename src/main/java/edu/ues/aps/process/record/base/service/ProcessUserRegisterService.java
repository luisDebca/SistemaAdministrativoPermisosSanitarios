package edu.ues.aps.process.record.base.service;

public interface ProcessUserRegisterService {

    void addProcessUser(Long id_caseFile, String username,String message);

    void addProcessUser(String username, String message);

    void addProcessUser(String username, String message, String[] extra);

    void addProcessUser(Long id_caseFile, String message);

    void addProcessUser(Long id_caseFile, String message, String[] extra);

}
