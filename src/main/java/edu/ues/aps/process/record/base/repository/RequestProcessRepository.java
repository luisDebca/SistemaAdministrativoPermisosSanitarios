package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.dto.AbstractRequestProcessDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class RequestProcessRepository implements RequestProcessFinderRepository{

    private final SessionFactory sessionFactory;

    public RequestProcessRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractRequestProcessDTO> findAll(Long id_caseFile) {
        return  sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.record.base.dto.RequestProcessDTO(" +
                "process.id, cp.active, cp.start_on, cp.finished_on, process.identifier," +
                "process.process,cp.observation," +
                "(select count(participant) from ProcessUser participant where participant.casefile_process.id.casefile_id = :id and participant.casefile_process.id.process_id = process.id)) " +
                "from CasefileProcess cp " +
                "inner join cp.process process " +
                "where cp.id.casefile_id = :id",AbstractRequestProcessDTO.class)
                .setParameter("id",id_caseFile)
                .getResultList();
    }
}
