package edu.ues.aps.process.record.base.dto;

public interface AbstractCasefileProcessDTO {

    Long getId_casefile();

    void setId_casefile(Long id_casefile);

    Long getId_process();

    String getProcess_process();

    String getProcess_description();

    String getStart_on();

    String getFinished_on();

    String getObservations();

    boolean isActive();
}
