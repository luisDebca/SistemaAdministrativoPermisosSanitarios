package edu.ues.aps.process.record.base.initializer;

import edu.ues.aps.process.record.base.service.DefaultRequestFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class DefaultRequestInitializer implements InitializingBean {

    private static final Logger logger = LogManager.getLogger(DefaultRequestInitializer.class);

    private final DefaultRequestFinderService service;
    
    public DefaultRequestInitializer(DefaultRequestFinderService service) {
        this.service = service;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Starting default request...");
        service.init();
    }
}
