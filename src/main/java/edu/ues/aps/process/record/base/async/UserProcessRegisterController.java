package edu.ues.aps.process.record.base.async;

import edu.ues.aps.process.record.base.service.CaseFileProcessObservationService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.security.Principal;

@Controller
public class UserProcessRegisterController {

    private static final Logger logger = LogManager.getLogger(UserProcessRegisterController.class);

    private final CaseFileProcessObservationService observationService;

    public UserProcessRegisterController(CaseFileProcessObservationService observationService) {
        this.observationService = observationService;
    }

    @RequestMapping(method = RequestMethod.GET, value = "/get-casefile-process-observation", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse getCaseFileProcessObservation(@RequestParam("id_caseFile") Long id_caseFile, Principal principal) {
        if (principal != null) {
            logger.info("GET:: Get caseFile current process observation");
            return new SingleStringResponse(observationService.getCurrentProcessObservation(id_caseFile)).asSuccessResponse();
        } else {
            logger.error("ERROR:: User principal is null");
            throw new CaseFileProcessHandlerException("User principal is null");
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/add-casefile-process-observation", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse addCaseFileProcessObservation(@RequestParam("id_caseFile") Long id_caseFile, @RequestParam("commentary") String observation, Principal principal) {
        if (principal != null) {
            logger.info("POST:: Add observation to caseFile {} current process by {}", id_caseFile, principal.getName());
            observationService.updateCurrentProcessObservation(id_caseFile, observation);
            return new SingleStringResponse("OK").asSuccessResponse();
        } else {
            logger.error("ERROR:: User principal is null");
            throw new CaseFileProcessHandlerException("User principal is null");
        }
    }
}
