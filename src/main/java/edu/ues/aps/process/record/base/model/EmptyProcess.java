package edu.ues.aps.process.record.base.model;

import java.util.Collections;
import java.util.List;

public class EmptyProcess implements AbstractProcess {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getProcess() {
        return "Process not found";
    }

    @Override
    public List<CasefileProcess> getCasefiles() {
        return Collections.emptyList();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setProcess(String process) {

    }

    @Override
    public void setCasefiles(List<CasefileProcess> casefiles) {

    }

    @Override
    public Process getNext() {
        return null;
    }

    @Override
    public void setNext(Process next) {

    }

    @Override
    public Process getPrevious() {
        return null;
    }

    @Override
    public void setPrevious(Process previous) {

    }

    @Override
    public String getDescription() {
        return "Process description not found";
    }

    @Override
    public void setDescription(String description) {

    }

    @Override
    public ProcessIdentifier getIdentifier() {
        return ProcessIdentifier.INVALID;
    }

    @Override
    public void setIdentifier(ProcessIdentifier identifier) {

    }

    @Override
    public String toString() {
        return "EmptyProcess{} ";
    }
}
