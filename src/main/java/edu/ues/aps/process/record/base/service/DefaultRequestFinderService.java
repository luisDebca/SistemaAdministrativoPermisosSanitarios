package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.base.model.Casefile;

public interface DefaultRequestFinderService {

    void init();

    Casefile getDefault();

    void reloadDefault();

    Boolean isEnabled();
}
