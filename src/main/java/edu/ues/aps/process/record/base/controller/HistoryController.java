package edu.ues.aps.process.record.base.controller;

import edu.ues.aps.process.record.base.service.HistoryFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/user-record")
public class HistoryController {

    private static final Logger logger = LogManager.getLogger(HistoryController.class);

    private static final String INDEX = "history-index";
    private static final String USER_HISTORY = "";
    private static final String CASEFILE_HISTORY = "";

    private final HistoryFinderService finderService;

    public HistoryController(HistoryFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping({"/index", "/"})
    public String showRegisterIndex(ModelMap model) {
        logger.info("GET:: Show history.");
        model.addAttribute("items", finderService.findAll());
        return INDEX;
    }

    @GetMapping("/user/{id_user}/history")
    public String showUserHistory(@PathVariable Long id_user, ModelMap model) {
        logger.info("GET:: Show user {} history.", id_user);
        return USER_HISTORY;
    }

    @GetMapping("/case-file/{id_caseFile}/history")
    public String showCaseFileHistory(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show case-file {} history", id_caseFile);
        return CASEFILE_HISTORY;
    }

}
