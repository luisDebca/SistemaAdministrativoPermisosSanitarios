package edu.ues.aps.process.record.base.model;

import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.users.management.model.User;

import javax.persistence.*;
import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

@Entity
@Table(name = "CASEFILE_PROCESS")
public class CasefileProcess {

    @EmbeddedId
    private CasefileProcessId id;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("casefile_id")
    private Casefile casefile;

    @ManyToOne(fetch = FetchType.LAZY)
    @MapsId("process_id")
    private Process process;

    @Column(name = "start_on")
    private LocalDateTime start_on;

    @Column(name = "finished_on")
    private LocalDateTime finished_on;

    @Column(name = "observation", length = 1000)
    private String observation;

    @Column(name = "is_active", nullable = false)
    private boolean active;

    @OneToMany(mappedBy = "casefile_process", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProcessUser> participants = new ArrayList<>();

    public CasefileProcess() {
    }

    public CasefileProcess(Casefile casefile, Process process) {
        this.casefile = casefile;
        this.process = process;
        this.id = new CasefileProcessId(casefile.getId(), process.getId());
    }

    public CasefileProcessId getId() {
        return id;
    }

    public void setId(CasefileProcessId id) {
        this.id = id;
    }

    public Casefile getCasefile() {
        return casefile;
    }

    public void setCasefile(Casefile casefile) {
        this.casefile = casefile;
    }

    public Process getProcess() {
        return process;
    }

    public void setProcess(Process process) {
        this.process = process;
    }

    public LocalDateTime getStart_on() {
        return start_on;
    }

    public void setStart_on(LocalDateTime start_on) {
        this.start_on = start_on;
    }

    public LocalDateTime getFinished_on() {
        return finished_on;
    }

    public void setFinished_on(LocalDateTime finished_on) {
        this.finished_on = finished_on;
    }

    public String getObservation() {
        return observation;
    }

    public void setObservation(String observation) {
        this.observation = observation;
    }

    public List<ProcessUser> getParticipants() {
        return participants;
    }

    public void setParticipants(List<ProcessUser> participants) {
        this.participants = participants;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public void addParticipant(User user, String message) {
        ProcessUser process_modification_record = new ProcessUser(this, user);
        process_modification_record.setDescription(message);
        participants.add(process_modification_record);
        user.getProcess().add(process_modification_record);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        CasefileProcess that = (CasefileProcess) o;

        return casefile.equals(that.casefile) && process.equals(that.process);
    }

    @Override
    public int hashCode() {
        int result = casefile.hashCode();
        result = 31 * result + process.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "CasefileProcess{" +
                "start_on=" + start_on +
                ", finished_on=" + finished_on +
                ", observation='" + observation + '\'' +
                ", active=" + active +
                '}';
    }

    public void close() {
        this.active = false;
        this.finished_on = LocalDateTime.now();
    }

    @Embeddable
    public static class CasefileProcessId implements Serializable {

        @Column(name = "id_casefile")
        private Long casefile_id;

        @Column(name = "id_process")
        private Long process_id;

        public CasefileProcessId() {
        }

        public CasefileProcessId(Long casefile_id, Long process_id) {
            this.casefile_id = casefile_id;
            this.process_id = process_id;
        }

        public Long getCasefile_id() {
            return casefile_id;
        }

        public Long getProcess_id() {
            return process_id;
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;

            CasefileProcessId that = (CasefileProcessId) o;

            return casefile_id.equals(that.casefile_id) && process_id.equals(that.process_id);
        }

        @Override
        public int hashCode() {
            int result = casefile_id.hashCode();
            result = 31 * result + process_id.hashCode();
            return result;
        }

    }
}
