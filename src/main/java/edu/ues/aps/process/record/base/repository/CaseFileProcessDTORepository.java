package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.dto.AbstractCasefileProcessDTO;
import edu.ues.aps.process.record.base.dto.AbstractProcessUserDTO;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class CaseFileProcessDTORepository implements CaseFileProcessDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public CaseFileProcessDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractCasefileProcessDTO> findAllProcessFromCaseFile(Long id_caseFile) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.record.base.dto.CasefileProcessDTO(" +
                "process.id,process.process,process.description," +
                "record.start_on,record.finished_on,record.observation,record.active) " +
                "from Casefile casefile " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where casefile.id = :id", AbstractCasefileProcessDTO.class)
                .setParameter("id", id_caseFile)
                .getResultList();
    }

    @Override
    public List<AbstractProcessUserDTO> findAllProcessUserFromCaseFileProcess(CasefileProcess.CasefileProcessId caseFileProcessId) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.record.base.dto.ProcessUserDTO(" +
                "user.id,participants.id.modified_on,participants.description," +
                "user.username,concat(concat(concat(personal.designation,' '),personal.first_name),personal.last_name)) " +
                "from CasefileProcess casefileProcess " +
                "inner join casefileProcess.participants participants " +
                "inner join participants.user user " +
                "inner join user.personal personal " +
                "where casefileProcess.id.casefile_id = :id_casefile " +
                "and casefileProcess.id.process_id = :id_process", AbstractProcessUserDTO.class)
                .setParameter("id_casefile", caseFileProcessId.getCasefile_id())
                .setParameter("id_process", caseFileProcessId.getProcess_id())
                .getResultList();
    }
}
