package edu.ues.aps.process.record.base.repository;

import javax.persistence.NoResultException;

public interface ProcessRecordPersistenceRepository {

    void addNewProcessToCasefile(Long id_casefile, Long id_process) throws NoResultException;

    void addNewProcessToCasefile(Long id_casefile) throws NoResultException;

    void addNewUserModificationRecordToCasefileProcess(Long id_casefile, String username, String message) throws NoResultException;

    void closeCurrentProcess(Long id_casefile);

}
