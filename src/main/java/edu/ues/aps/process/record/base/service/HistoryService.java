package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.record.base.dto.AbstractHistoryDTO;
import edu.ues.aps.process.record.base.repository.HistoryFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class HistoryService implements HistoryFinderService{

    private final HistoryFinderRepository finderRepository;

    public HistoryService(HistoryFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractHistoryDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public List<AbstractHistoryDTO> findUserAll(Long id_user) {
        return finderRepository.findUserAll(id_user);
    }

    @Override
    public List<AbstractHistoryDTO> findCaseFileAll(Long id_caseFile) {
        return finderRepository.findCaseFileAll(id_caseFile);
    }
}
