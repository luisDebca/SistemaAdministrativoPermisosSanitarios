package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.record.base.dto.AbstractCasefileProcessDTO;
import edu.ues.aps.process.record.base.dto.AbstractProcessUserDTO;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.repository.CaseFileProcessDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class CaseFileProcessDTOService implements CaseFileProcessDTOFinderService {

    private final CaseFileProcessDTOFinderRepository finderRepository;

    public CaseFileProcessDTOService(CaseFileProcessDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractCasefileProcessDTO> findCaseFileRecord(Long id_caseFile) {
        List<AbstractCasefileProcessDTO> caseFile_record = finderRepository.findAllProcessFromCaseFile(id_caseFile);
        for (AbstractCasefileProcessDTO record : caseFile_record) {
            record.setId_casefile(id_caseFile);
        }
        return caseFile_record;
    }

    @Override
    public List<AbstractProcessUserDTO> findUserRecord(Long id_caseFile, Long id_process) {
        CasefileProcess.CasefileProcessId id = new CasefileProcess.CasefileProcessId(id_caseFile, id_process);
        List<AbstractProcessUserDTO> process_users = finderRepository.findAllProcessUserFromCaseFileProcess(id);
        for (AbstractProcessUserDTO processUserDTO : process_users) {
            processUserDTO.setId_casefile(id_caseFile);
            processUserDTO.setId_process(id_process);
        }
        return process_users;
    }
}
