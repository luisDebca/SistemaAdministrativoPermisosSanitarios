package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.model.AbstractProcess;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;

import javax.persistence.NoResultException;

public interface ProcessFinderRepository {

    AbstractProcess find(Long id_process) throws NoResultException;

    AbstractProcess find(ProcessIdentifier identifier) throws NoResultException;

    AbstractProcess getNext(ProcessIdentifier identifier) throws NoResultException;

    AbstractProcess getNext(Long id_caseFile) throws NoResultException;

    AbstractProcess getPrevious(ProcessIdentifier identifier) throws NoResultException;

    AbstractProcess getPrevious(Long id_caseFile) throws NoResultException;
}
