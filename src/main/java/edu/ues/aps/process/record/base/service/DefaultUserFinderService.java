package edu.ues.aps.process.record.base.service;

import edu.ues.aps.users.management.model.User;

public interface DefaultUserFinderService {

    void init();

    User getDefault();

    void reloadUser();

    Boolean defaultEnable();
}
