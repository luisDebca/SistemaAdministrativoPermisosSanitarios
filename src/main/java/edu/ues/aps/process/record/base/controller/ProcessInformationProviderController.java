package edu.ues.aps.process.record.base.controller;

import edu.ues.aps.process.record.base.service.ProcessDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/process")
public class ProcessInformationProviderController {

    private static final Logger logger = LogManager.getLogger(ProcessInformationProviderController.class);

    private static final String INDEX = "process_index";

    private final ProcessDTOFinderService finderService;

    public ProcessInformationProviderController(ProcessDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping({"/index", "/"})
    public String showProcessIndex(ModelMap model) {
        logger.info("Show process index.");
        model.addAttribute("process", finderService.findAll());
        return INDEX;
    }
}
