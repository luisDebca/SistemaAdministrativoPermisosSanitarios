package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.users.management.model.User;

import javax.persistence.NoResultException;

public interface DefaultUserFinderRepository {

    User getDefault() throws NoResultException;

    Boolean isDefault();

    void deleteDefault();

    void createDefault();
}
