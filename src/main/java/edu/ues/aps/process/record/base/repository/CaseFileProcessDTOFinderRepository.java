package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.dto.AbstractCasefileProcessDTO;
import edu.ues.aps.process.record.base.dto.AbstractProcessUserDTO;
import edu.ues.aps.process.record.base.model.CasefileProcess;

import java.util.List;

public interface CaseFileProcessDTOFinderRepository {

    List<AbstractCasefileProcessDTO> findAllProcessFromCaseFile(Long id_caseFile);

    List<AbstractProcessUserDTO> findAllProcessUserFromCaseFileProcess(CasefileProcess.CasefileProcessId caseFileProcessId);
}
