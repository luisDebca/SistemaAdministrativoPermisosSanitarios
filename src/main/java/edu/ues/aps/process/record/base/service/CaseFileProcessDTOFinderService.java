package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.record.base.dto.AbstractCasefileProcessDTO;
import edu.ues.aps.process.record.base.dto.AbstractProcessUserDTO;

import java.util.List;

public interface CaseFileProcessDTOFinderService {

    List<AbstractCasefileProcessDTO> findCaseFileRecord(Long id_caseFile);

    List<AbstractProcessUserDTO> findUserRecord(Long id_caseFile, Long id_process);
}
