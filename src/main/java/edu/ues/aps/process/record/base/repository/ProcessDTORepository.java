package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.dto.AbstractProcessSummaryDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ProcessDTORepository implements ProcessDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public ProcessDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractProcessSummaryDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.record.base.dto.ProcessSummaryDTO(" +
                "process.id,process.process,process.description,count(cp) ) " +
                "from CasefileProcess cp " +
                "inner join cp.process process " +
                "where cp.active = true " +
                "group by process.id ",AbstractProcessSummaryDTO.class)
                .getResultList();
    }
}
