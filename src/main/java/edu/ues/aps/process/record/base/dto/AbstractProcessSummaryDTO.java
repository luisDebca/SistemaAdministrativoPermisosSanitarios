package edu.ues.aps.process.record.base.dto;

public interface AbstractProcessSummaryDTO {

    Long getId();

    String getProcess();

    String getDescription();

    Long getRequestCounter();
}
