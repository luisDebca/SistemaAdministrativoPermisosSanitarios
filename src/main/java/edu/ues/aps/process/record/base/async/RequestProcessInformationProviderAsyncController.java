package edu.ues.aps.process.record.base.async;

import edu.ues.aps.process.record.base.dto.AbstractRequestProcessDTO;
import edu.ues.aps.process.record.base.service.RequestProcessFinderService;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.List;

@Controller
@RequestMapping("/record/process")
public class RequestProcessInformationProviderAsyncController {

    private final RequestProcessFinderService finderService;

    public RequestProcessInformationProviderAsyncController(RequestProcessFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping(value = "/summary", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractRequestProcessDTO> getProcessSummary(@RequestParam Long id_caseFile) {
        return finderService.findAll(id_caseFile);
    }
}
