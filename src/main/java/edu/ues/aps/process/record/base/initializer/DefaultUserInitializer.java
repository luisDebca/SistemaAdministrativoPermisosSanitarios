package edu.ues.aps.process.record.base.initializer;

import edu.ues.aps.process.record.base.service.DefaultUserFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

@Component
public class DefaultUserInitializer implements InitializingBean {

    private static final Logger logger = LogManager.getLogger(DefaultUserInitializer.class);

    private final DefaultUserFinderService service;

    public DefaultUserInitializer(DefaultUserFinderService service) {
        this.service = service;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        logger.info("Starting default user SYSTEM...");
        service.init();
    }
}
