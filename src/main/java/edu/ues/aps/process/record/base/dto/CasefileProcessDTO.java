package edu.ues.aps.process.record.base.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class CasefileProcessDTO implements AbstractCasefileProcessDTO {

    private Long id_casefile;
    private Long id_process;
    private String process_process;
    private String process_description;
    private LocalDateTime start_on;
    private LocalDateTime finished_on;
    private String observations;
    private boolean active;

    public CasefileProcessDTO(Long id_process, String process_process, String process_description, LocalDateTime start_on, LocalDateTime finished_on, String observations, boolean active) {
        this.id_process = id_process;
        this.process_process = process_process;
        this.process_description = process_description;
        this.start_on = start_on;
        this.finished_on = finished_on;
        this.observations = observations;
        this.active = active;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public void setId_casefile(Long id_casefile) {
        this.id_casefile = id_casefile;
    }

    public Long getId_process() {
        return id_process;
    }

    public String getProcess_process() {
        return process_process;
    }

    public String getProcess_description() {
        return process_description;
    }

    public String getStart_on() {
        if (start_on != null) {
            return DatetimeUtil.parseDatetimeToLongDateFormat(start_on);
        } else {
            return DatetimeUtil.DATETIME_NOT_DEFINED;
        }
    }

    public String getFinished_on() {
        if (finished_on != null) {
            return DatetimeUtil.parseDatetimeToLongDateFormat(finished_on);
        } else {
            return DatetimeUtil.DATETIME_NOT_DEFINED;
        }
    }

    public String getObservations() {
        return observations;
    }

    public boolean isActive() {
        return active;
    }
}
