package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.dto.AbstractProcessSummaryDTO;

import java.util.List;

public interface ProcessDTOFinderRepository {

    List<AbstractProcessSummaryDTO> findAll();
}
