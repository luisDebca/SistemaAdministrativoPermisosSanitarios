package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.dto.AbstractHistoryDTO;

import java.util.List;

public interface HistoryFinderRepository {

    List<AbstractHistoryDTO> findAll();

    List<AbstractHistoryDTO> findUserAll(Long id_user);

    List<AbstractHistoryDTO> findCaseFileAll(Long id_caseFile);
}
