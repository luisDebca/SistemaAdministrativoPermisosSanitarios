package edu.ues.aps.process.record.base.dto;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class HistoryDTO implements AbstractHistoryDTO{

    private Long id_user;
    private Long id_caseFile;
    private Long id_process;
    private String user_full_name;
    private String caseFile_folder_number;
    private CasefileSection caseFile_section;
    private String process;
    private String description;
    private LocalDateTime modification_date;

    public HistoryDTO(Long id_user, Long id_caseFile, Long id_process, String user_full_name, String caseFile_folder_number, CasefileSection caseFile_section, String process, String description, LocalDateTime modification_date) {
        this.id_user = id_user;
        this.id_caseFile = id_caseFile;
        this.id_process = id_process;
        this.user_full_name = user_full_name;
        this.caseFile_folder_number = caseFile_folder_number;
        this.caseFile_section = caseFile_section;
        this.process = process;
        this.description = description;
        this.modification_date = modification_date;
    }

    public Long getId_user() {
        return id_user;
    }

    public Long getId_caseFile() {
        return id_caseFile;
    }

    public Long getId_process() {
        return id_process;
    }

    public String getUser_full_name() {
        return user_full_name;
    }

    public String getCaseFile_folder_number() {
        return caseFile_folder_number;
    }

    public String getCaseFile_section() {
        if (caseFile_section.getCasefileSection() == null){
            return CasefileSection.INVALID.getCasefileSection();
        }
        return caseFile_section.getCasefileSection();
    }

    public String getProcess() {
        return process;
    }

    public String getDescription() {
        return description;
    }

    @Override
    public String getModification_date() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(modification_date);
    }
}
