package edu.ues.aps.process.record.base.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class ProcessUserDTO implements AbstractProcessUserDTO {

    private Long id_user;
    private Long id_casefile;
    private Long id_process;
    private LocalDateTime modified_on;
    private String description;
    private String user_username;
    private String user_full_name;

    public ProcessUserDTO(Long id_user, LocalDateTime modified_on, String description, String user_username, String user_full_name) {
        this.id_user = id_user;
        this.modified_on = modified_on;
        this.description = description;
        this.user_username = user_username;
        this.user_full_name = user_full_name;
    }

    public Long getId_user() {
        return id_user;
    }

    public Long getId_casefile() {
        return id_casefile;
    }

    public Long getId_process() {
        return id_process;
    }

    public void setId_casefile(Long id_casefile) {
        this.id_casefile = id_casefile;
    }

    public void setId_process(Long id_process) {
        this.id_process = id_process;
    }

    public String getModified_on() {
        if (modified_on != null) {
            return DatetimeUtil.parseDatetimeToLongDateFormat(modified_on);
        } else {
            return DatetimeUtil.DATETIME_NOT_DEFINED;
        }
    }

    public String getDescription() {
        return description;
    }

    public String getUser_username() {
        return user_username;
    }

    public String getUser_full_name() {
        return user_full_name;
    }
}
