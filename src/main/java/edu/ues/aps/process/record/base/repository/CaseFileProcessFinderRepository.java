package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;

import javax.persistence.NoResultException;

public interface CaseFileProcessFinderRepository {

    CasefileProcess getRecord(Long id_caseFile) throws NoResultException;

    CasefileProcess getGeneralProcessRegister() throws NoResultException;
}
