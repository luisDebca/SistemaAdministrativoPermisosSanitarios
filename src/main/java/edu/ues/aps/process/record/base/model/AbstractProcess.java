package edu.ues.aps.process.record.base.model;

import java.util.List;

public interface AbstractProcess {

    Long getId();

    void setId(Long id);

    String getProcess();

    void setProcess(String process);

    List<CasefileProcess> getCasefiles();

    void setCasefiles(List<CasefileProcess> casefiles);

    Process getNext();

    void setNext(Process next);

    Process getPrevious();

    void setPrevious(Process previous);

    String getDescription();

    void setDescription(String description);

    ProcessIdentifier getIdentifier();

    void setIdentifier(ProcessIdentifier identifier);

}
