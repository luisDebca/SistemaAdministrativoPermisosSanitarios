package edu.ues.aps.process.record.base.service;

public interface CaseFileProcessObservationService {

    String getCurrentProcessObservation(Long id_caseFile);

    void updateCurrentProcessObservation(Long id_caseFile, String observation);
}
