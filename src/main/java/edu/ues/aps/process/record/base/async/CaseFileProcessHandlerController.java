package edu.ues.aps.process.record.base.async;

import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.record.base.dto.AbstractCasefileProcessDTO;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.record.base.service.CaseFileProcessDTOFinderService;
import edu.ues.aps.process.record.base.service.CaseFileProcessRegisterService;
import edu.ues.aps.process.record.base.service.ProcessUserRegisterService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.List;

@Controller
@RequestMapping("/process/record")
public class CaseFileProcessHandlerController {

    private static final Logger logger = LogManager.getLogger(CaseFileProcessHandlerController.class);

    private final ProcessUserRegisterService registerService;
    private final CaseFileProcessRegisterService processRegisterService;
    private final VehicleBunchInformationService bunchInformationService;
    private final CaseFileProcessDTOFinderService processDTOFinderService;

    public CaseFileProcessHandlerController(CaseFileProcessDTOFinderService processDTOFinderService, CaseFileProcessRegisterService processRegisterService, ProcessUserRegisterService registerService, VehicleBunchInformationService bunchInformationService) {
        this.processDTOFinderService = processDTOFinderService;
        this.processRegisterService = processRegisterService;
        this.registerService = registerService;
        this.bunchInformationService = bunchInformationService;
    }

    @GetMapping(value = "/move-case-file-to-next-process", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse moveCaseFileToNexProcess(@RequestParam Long id_caseFile, Principal principal) {
        logger.info("GET:: Move caseFile {} to next process.", id_caseFile);
        processRegisterService.moveToNext(id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.move.next");
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @GetMapping(value = "/move-case-file-to-canceled-request", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse moveCaseFileToCanceledRequest(@RequestParam Long id_caseFile, Principal principal) {
        logger.info("GET:: Move caseFile {} to canceled request.", id_caseFile);
        processRegisterService.moveTo(id_caseFile, ProcessIdentifier.REQUEST_REJECTED);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.move.rejected");
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @GetMapping(value = "/move-case-file-to-forgotten-request", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse moveCaseFileToForgottenRequest(@RequestParam Long id_caseFile, Principal principal) {
        logger.info("GET:: Move caseFile {} to forgotten request.", id_caseFile);
        processRegisterService.moveTo(id_caseFile, ProcessIdentifier.REQUEST_FORGOTTEN);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.move.forgotten");
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @GetMapping(value = "/move-case-file-to-previous-process", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse moveCaseFileToPreviousProcess(@RequestParam Long id_caseFile, Principal principal) {
        logger.info("GET:: Move caseFile {} to previous process.", id_caseFile);
        processRegisterService.moveToPrevious(id_caseFile);
        registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.move.previous");
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @GetMapping(value = "/move-bunch-to-next-process", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse moveBunchToNexProcess(@RequestParam Long id_bunch, Principal principal) {
        logger.info("GET:: Move bunch {} to next process.", id_bunch);
        for (Long id_caseFile : bunchInformationService.findAllCaseFileId(id_bunch)) {
            logger.info("...Move caseFile {} to next process.", id_caseFile);
            processRegisterService.moveToNext(id_caseFile);
            registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.move.next");
        }
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @GetMapping(value = "/move-bunch-to-canceled-request", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse moveBunchToCanceledRequest(@RequestParam Long id_bunch, Principal principal) {
        logger.info("GET:: Move bunch {} to canceled request.", id_bunch);
        for (Long id_caseFile : bunchInformationService.findAllCaseFileId(id_bunch)) {
            logger.info("...Move caseFile {} to canceled request.", id_caseFile);
            processRegisterService.moveTo(id_caseFile, ProcessIdentifier.REQUEST_REJECTED);
            registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.move.rejected");
        }
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @GetMapping(value = "/move-bunch-to-forgotten-request", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse moveBunchToForgottenRequest(@RequestParam Long id_bunch, Principal principal) {
        logger.info("GET:: Move bunch {} to forgotten request.", id_bunch);
        for (Long id_caseFile : bunchInformationService.findAllCaseFileId(id_bunch)) {
            logger.info("...Move caseFile {} to forgotten request.", id_caseFile);
            processRegisterService.moveTo(id_caseFile, ProcessIdentifier.REQUEST_FORGOTTEN);
            registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.move.forgotten");
        }
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @GetMapping(value = "/move-bunch-to-previous-process", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse moveBunchToPreviousProcess(@RequestParam Long id_bunch, Principal principal) {
        logger.info("GET:: Move bunch {} to previos process.", id_bunch);
        for (Long id_caseFile : bunchInformationService.findAllCaseFileId(id_bunch)) {
            logger.info("...Move caseFile {} to previous process.", id_caseFile);
            processRegisterService.moveToPrevious(id_caseFile);
            registerService.addProcessUser(id_caseFile, principal.getName(), "process.record.move.previous");
        }
        return new SingleStringResponse("OK").asSuccessResponse();
    }

    @GetMapping(value = "/find-case-file-process-for-modification-record", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractCasefileProcessDTO> findCaseFileProcessForModificationRecord(@RequestParam Long id_caseFile) {
        logger.info("GET:: Find current caseFile {} process record.", id_caseFile);
        return processDTOFinderService.findCaseFileRecord(id_caseFile);
    }

    @PostMapping(value = "/save-case-file-process-record-modification", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse saveCaseFileProcessRecordModifications() {
        logger.info("POST:: Save process modifications.");
        return null;
    }
}
