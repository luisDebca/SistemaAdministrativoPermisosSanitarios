package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.model.AbstractProcess;
import edu.ues.aps.process.record.base.model.Process;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class ProcessRepository implements ProcessFinderRepository {

    private final SessionFactory sessionFactory;

    public ProcessRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractProcess find(Long id_process) {
        return sessionFactory.getCurrentSession().get(Process.class, id_process);
    }

    @Override
    public AbstractProcess getNext(ProcessIdentifier identifier) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select process.next " +
                "from Process process " +
                "where process.identifier = :id", Process.class)
                .setParameter("id", identifier)
                .getSingleResult();
    }

    @Override
    public AbstractProcess getPrevious(ProcessIdentifier identifier) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select process.previous " +
                "from Process  process " +
                "where process.identifier = :id", Process.class)
                .setParameter("id", identifier)
                .getSingleResult();
    }

    @Override
    public Process find(ProcessIdentifier identifier) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select process " +
                "from Process process " +
                "where process.identifier = :idf", Process.class)
                .setParameter("idf", identifier)
                .getSingleResult();
    }

    @Override
    public AbstractProcess getNext(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select process.next " +
                "from Casefile casefile " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where casefile.id = :id " +
                "and record.active = true",AbstractProcess.class)
                .setParameter("id",id_caseFile)
                .getSingleResult();
    }

    @Override
    public AbstractProcess getPrevious(Long id_caseFile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select process.previous " +
                "from Casefile casefile " +
                "inner join casefile.processes record " +
                "inner join record.process process " +
                "where casefile.id = :id " +
                "and record.active = true",AbstractProcess.class)
                .setParameter("id",id_caseFile)
                .getSingleResult();
    }
}
