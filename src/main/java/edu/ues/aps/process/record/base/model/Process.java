package edu.ues.aps.process.record.base.model;

import org.springframework.stereotype.Controller;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "PROCESS")
public class Process implements AbstractProcess {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "process", unique = true, nullable = false, length = 50)
    private String process;

    @Column(name = "description", length = 150)
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "identifier", length = 50, nullable = false, unique = true)
    private ProcessIdentifier identifier;

    @OneToMany(mappedBy = "process", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<CasefileProcess> casefiles = new ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "next_process")
    private Process next;

    @OneToOne(mappedBy = "next", cascade = CascadeType.ALL)
    private Process previous;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getProcess() {
        return process;
    }

    @Override
    public void setProcess(String process) {
        this.process = process;
    }

    @Override
    public String getDescription() {
        return description;
    }

    @Override
    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public List<CasefileProcess> getCasefiles() {
        return casefiles;
    }

    @Override
    public void setCasefiles(List<CasefileProcess> casefiles) {
        this.casefiles = casefiles;
    }

    @Override
    public Process getNext() {
        return next;
    }

    @Override
    public void setNext(Process next) {
        this.next = next;
    }

    @Override
    public Process getPrevious() {
        return previous;
    }

    @Override
    public void setPrevious(Process previous) {
        this.previous = previous;
    }

    @Override
    public ProcessIdentifier getIdentifier() {
        return identifier;
    }

    @Override
    public void setIdentifier(ProcessIdentifier identifier) {
        this.identifier = identifier;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Process process1 = (Process) o;
        return Objects.equals(process, process1.process);
    }

    @Override
    public int hashCode() {
        return Objects.hash(process);
    }

    @Override
    public String toString() {
        return "Process{" +
                "id=" + id +
                ", process='" + process + '\'' +
                ", description='" + description + '\'' +
                '}';
    }
}
