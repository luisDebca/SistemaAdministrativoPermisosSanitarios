package edu.ues.aps.process.record.base.async;

public class CaseFileProcessHandlerException extends RuntimeException {

    public CaseFileProcessHandlerException() {
    }

    public CaseFileProcessHandlerException(String s) {
        super(s);
    }
}
