package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.model.CasefileProcess;

public interface CaseFileProcessPersistenceRepository {

    void merge(CasefileProcess record);

    void update(CasefileProcess record);
}
