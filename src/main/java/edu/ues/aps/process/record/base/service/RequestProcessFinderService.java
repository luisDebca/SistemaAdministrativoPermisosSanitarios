package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.record.base.dto.AbstractRequestProcessDTO;

import java.util.List;

public interface RequestProcessFinderService {

    List<AbstractRequestProcessDTO> findAll(Long id_caseFile);
}
