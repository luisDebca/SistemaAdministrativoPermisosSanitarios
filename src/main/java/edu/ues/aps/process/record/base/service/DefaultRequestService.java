package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.record.base.repository.DefaultRequestFinderRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional
public class DefaultRequestService implements DefaultRequestFinderService {

    private static final Logger logger = LogManager.getLogger(DefaultRequestService.class);

    private final DefaultRequestFinderRepository repository;

    private Casefile defaultRequest;
    private Boolean isEnabled;

    public DefaultRequestService(DefaultRequestFinderRepository repository) {
        this.repository = repository;
    }

    @Override
    public void init() {
        try {
            if (!repository.isDefault()){
                repository.deleteDefault();
                repository.createDefault();
                logger.info("...Default request created!");
            }
            defaultRequest = repository.getDefault();
            isEnabled = true;
            logger.info("...Default request is OK!");
        } catch (Exception e) {
            isEnabled = false;
            logger.warn("...Default request not enable. Try restart the system. ");
            logger.warn("..." + e.getMessage());
        }
    }

    @Override
    public Casefile getDefault() {
        return defaultRequest;
    }

    @Override
    public void reloadDefault() {
        try {
            defaultRequest = repository.getDefault();
        } catch (NoResultException e) {
            isEnabled = false;
        }
    }

    @Override
    public Boolean isEnabled() {
        return isEnabled;
    }

}
