package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.record.base.dto.AbstractRequestProcessDTO;
import edu.ues.aps.process.record.base.repository.RequestProcessFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class RequestProcessService implements RequestProcessFinderService {

    private final RequestProcessFinderRepository finderRepository;

    public RequestProcessService(RequestProcessFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractRequestProcessDTO> findAll(Long id_caseFile) {
        return finderRepository.findAll(id_caseFile);
    }
}
