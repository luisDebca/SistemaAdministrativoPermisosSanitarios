package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.area.base.repository.CaseFileRequestCountRepository;
import edu.ues.aps.process.base.model.AbstractCasefile;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.repository.CaseFilePersistenceRepository;
import edu.ues.aps.process.base.service.SingleCaseFileFinderService;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.model.Process;
import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.record.base.repository.CaseFileProcessFinderRepository;
import edu.ues.aps.process.record.base.repository.ProcessFinderRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional
public class CaseFileProcessService implements InitializingBean, CaseFileProcessRegisterService, CaseFileProcessObservationService {

    private static final Logger logger = LogManager.getLogger(CaseFileProcessService.class);

    private final ProcessFinderRepository processFinderRepository;
    private final SingleCaseFileFinderService caseFileFinderService;
    private final CaseFileProcessFinderRepository recordFinderRepository;
    private final CaseFilePersistenceRepository caseFilePersistenceRepository;
    private final List<CaseFileRequestCountRepository> requestCountRepositories;

    private Map<CasefileSection, CaseFileRequestCountRepository> requestCountMap = new HashMap<>();

    public CaseFileProcessService(CaseFilePersistenceRepository caseFilePersistenceRepository, ProcessFinderRepository processFinderRepository, CaseFileProcessFinderRepository recordFinderRepository, List<CaseFileRequestCountRepository> requestCountRepositories, SingleCaseFileFinderService caseFileFinderService) {
        this.caseFilePersistenceRepository = caseFilePersistenceRepository;
        this.processFinderRepository = processFinderRepository;
        this.recordFinderRepository = recordFinderRepository;
        this.requestCountRepositories = requestCountRepositories;
        this.caseFileFinderService = caseFileFinderService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (CaseFileRequestCountRepository repository : requestCountRepositories) {
            requestCountMap.put(repository.appliesTo(), repository);
        }
    }

    @Override
    public void moveTo(AbstractCasefile caseFile, ProcessIdentifier processIdentifier) {
        try {
            Process process = (Process) processFinderRepository.find(processIdentifier);
            logger.info("Moving case file {} to process {}", caseFile.getId(), process.getProcess());
            move(caseFile, process);
        } catch (NoResultException e) {
            e.printStackTrace();
        }
    }


    @Override
    public void moveToNext(AbstractCasefile caseFile) {
        try {
            Process next = findNextProcess(caseFile);
            logger.info("Moving case file {} to process {}", caseFile.getId(), next.getProcess());
            move(caseFile, next);
        } catch (NoResultException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void moveToPrevious(AbstractCasefile caseFile) {
        try {
            Process previous = (Process) processFinderRepository.getPrevious(caseFile.getId());
            logger.info("Moving case file {} to process {}", caseFile.getId(), previous.getProcess());
            move(caseFile, previous);
        } catch (NoResultException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void addProcess(AbstractCasefile caseFile, ProcessIdentifier identifier) {
        try {
            Process process = (Process) processFinderRepository.find(identifier);
            logger.info("Moving case file {} to process {}", caseFile.getId(), process.getProcess());
            CasefileProcess record = new CasefileProcess((Casefile) caseFile, process);
            record.setActive(true);
            record.setStart_on(LocalDateTime.now());
            caseFile.addProcessRecord(record);
            caseFilePersistenceRepository.merge(caseFile);
        } catch (NoResultException e) {
            e.printStackTrace();
        }
    }

    @Override
    public String getCurrentProcessObservation(Long id_caseFile) {
        return recordFinderRepository.getRecord(id_caseFile).getObservation();
    }

    @Override
    public void updateCurrentProcessObservation(Long id_caseFile, String observation) {
        CasefileProcess currentProcess = recordFinderRepository.getRecord(id_caseFile);
        if (observation.length() <= 1000 && !currentProcess.getObservation().equals(observation)) {
            currentProcess.setObservation(observation);
        }
    }

    @Override
    public void addProcess(Long id_caseFile, ProcessIdentifier process) {
        addProcess(caseFileFinderService.find(id_caseFile), process);
    }

    @Override
    public void moveTo(Long id_caseFile, ProcessIdentifier processIdentifier) {
        try {
            Process process = (Process) processFinderRepository.find(processIdentifier);
            logger.info("Moving case file {} to process {}", id_caseFile, process.getProcess());
            move(caseFileFinderService.find(id_caseFile), process);
        } catch (NoResultException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void moveToNext(Long id_caseFile) {
        moveToNext(caseFileFinderService.find(id_caseFile));
    }

    @Override
    public void moveToPrevious(Long id_caseFile) {
        moveToPrevious(caseFileFinderService.find(id_caseFile));
    }

    private void move(AbstractCasefile caseFile, Process process) {
        closeCurrentProcess(caseFile.getId());
        caseFilePersistenceRepository.saveOrUpdate(caseFile);
        CasefileProcess new_record = buildProcessRecord(caseFile, process);
        caseFile.addProcessRecord(new_record);
        caseFilePersistenceRepository.merge(caseFile);
    }

    private void closeCurrentProcess(Long id_caseFile) {
        recordFinderRepository.getRecord(id_caseFile).close();
    }

    private Process findNextProcess(AbstractCasefile caseFile) {
        Process next = (Process) processFinderRepository.getNext(caseFile.getId());
        setCertificationValidIfCertificationActive(caseFile, next);
        setCertificationInvalidIfCertificationExpired(caseFile, next);
        if (isNotApplyPaymentProcessRequest(caseFile, next) || isNextProcessCertificationAccepted(next)) {
            caseFile.setRequest_number(getNewRequestNumber(caseFile.getSection()));
            return (Process) processFinderRepository.find(ProcessIdentifier.REQUEST_ACCEPTATION);
        }
        return next;
    }

    private boolean isNextProcessCertificationAccepted(Process next) {
        return next.getIdentifier().equals(ProcessIdentifier.REQUEST_ACCEPTATION);
    }

    private boolean isNotApplyPaymentProcessRequest(AbstractCasefile caseFile, Process next) {
        return next.getIdentifier().equals(ProcessIdentifier.PAYMENT_PROCEDURE_START) && !caseFile.getApply_payment();
    }

    private void setCertificationInvalidIfCertificationExpired(AbstractCasefile caseFile, Process next) {
        if (next.getIdentifier().equals(ProcessIdentifier.CERTIFICATION_EXPIRED)) {
            caseFile.setStatus(CasefileStatusType.INVALID);
        }
    }

    private void setCertificationValidIfCertificationActive(AbstractCasefile caseFile, Process next) {
        if (next.getIdentifier().equals(ProcessIdentifier.CERTIFICATION_ACTIVE)) {
            caseFile.setStatus(CasefileStatusType.VALID);
        }
    }

    private String getNewRequestNumber(CasefileSection section) {
        if (section.equals(CasefileSection.ESTABLISHMENT) || section.equals(CasefileSection.VEHICLE)) {
            return String.valueOf(requestCountMap.get(section).getMaxRequestNumberCount() + 1);
        } else {
            throw new InvalidCaseFileSectionException();
        }
    }

    private CasefileProcess buildProcessRecord(AbstractCasefile caseFile, Process process) {
        CasefileProcess record = new CasefileProcess((Casefile) caseFile, process);
        record.setActive(true);
        record.setStart_on(LocalDateTime.now());
        return record;
    }

}
