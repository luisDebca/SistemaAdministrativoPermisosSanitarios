package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.dto.AbstractHistoryDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class HistoryRepository implements HistoryFinderRepository {

    private final SessionFactory sessionFactory;

    public HistoryRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractHistoryDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.record.base.dto.HistoryDTO(" +
                "user.id,caseFile.id,process.id,user.username," +
                "concat(concat(caseFile.request_folder,'-'),caseFile.request_year),caseFile.section,process.process," +
                "record.description,record.id.modified_on) " +
                "from ProcessUser record " +
                "inner join record.user user " +
                "inner join user.personal personal " +
                "inner join record.casefile_process.process process " +
                "inner join record.casefile_process.casefile caseFile", AbstractHistoryDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractHistoryDTO> findUserAll(Long id_user) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.record.base.dto.HistoryDTO(" +
                "user.id,caseFile.id,process.id,user.username," +
                "concat(concat(caseFile.request_folder,'-'),caseFile.request_year),caseFile.section,process.process," +
                "record.description,record.id.modified_on) " +
                "from ProcessUser record " +
                "inner join record.user user " +
                "inner join user.personal personal " +
                "inner join record.casefile_process.process process " +
                "inner join record.casefile_process.casefile caseFile", AbstractHistoryDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractHistoryDTO> findCaseFileAll(Long id_caseFile) {
        return null;
    }
}
