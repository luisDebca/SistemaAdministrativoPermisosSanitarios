package edu.ues.aps.process.record.base.service;

import edu.ues.aps.process.record.base.dto.AbstractProcessSummaryDTO;

import java.util.List;

public interface ProcessDTOFinderService {

    List<AbstractProcessSummaryDTO> findAll();
}
