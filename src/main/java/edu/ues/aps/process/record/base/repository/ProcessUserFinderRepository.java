package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.process.record.base.model.CasefileProcess;
import edu.ues.aps.process.record.base.model.ProcessUser;

import java.util.List;

public interface ProcessUserFinderRepository {

    List<ProcessUser> findAll(CasefileProcess.CasefileProcessId record_id);

    List<ProcessUser> findAll(String username);
}
