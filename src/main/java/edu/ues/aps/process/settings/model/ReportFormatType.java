package edu.ues.aps.process.settings.model;

public enum ReportFormatType {

    DICTUM,
    COORDINATOR,
    RESOLUTION,
    OPINION,
    MEMO,
    SIMPLE,
    INVALID

}
