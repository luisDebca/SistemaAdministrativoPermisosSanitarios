package edu.ues.aps.process.settings.service;

import edu.ues.aps.process.settings.model.AbstractReportFormat;

public interface ReportFormatPersistenceService {

    AbstractReportFormat find(Long id_report);

    void save(AbstractReportFormat format);

    void update(AbstractReportFormat format);

    void setProtect(Long id_format);

    void delete(Long id_format);
}
