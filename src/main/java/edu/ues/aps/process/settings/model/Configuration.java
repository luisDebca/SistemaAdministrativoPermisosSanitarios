package edu.ues.aps.process.settings.model;

import javax.persistence.*;
import java.time.LocalDate;

@Entity
@Table(name = "CONFIGURATION")
public class Configuration implements AbstractConfiguration {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 8)
    private String establishment_start_folder_number;

    @Column(length = 8)
    private String establishment_start_request_number;

    @Column(length = 8)
    private String establishment_start_resolution_number;

    @Column(length = 8)
    private String vehicle_start_folder_number;

    @Column(length = 8)
    private String vehicle_start_request_number;

    @Column(length = 8)
    private String vehicle_start_resolution_number;

    @Column(length = 8)
    private String start_payment_number;

    @Column(length = 8)
    private String start_memorandum_number;

    @Column(length = 8)
    private String start_legal_memorandum_number;

    @Column
    private LocalDate date;

    public Configuration() {
        establishment_start_folder_number = "0";
        establishment_start_request_number = "0";
        establishment_start_resolution_number = "0";
        vehicle_start_folder_number = "0";
        vehicle_start_request_number = "0";
        vehicle_start_resolution_number = "0";
        start_payment_number = "0";
        start_memorandum_number = "0";
        start_legal_memorandum_number = "0";
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getEstablishment_start_folder_number() {
        return establishment_start_folder_number;
    }

    @Override
    public void setEstablishment_start_folder_number(String establishment_start_folder_number) {
        this.establishment_start_folder_number = establishment_start_folder_number;
    }

    @Override
    public String getEstablishment_start_request_number() {
        return establishment_start_request_number;
    }

    @Override
    public void setEstablishment_start_request_number(String establishment_start_request_number) {
        this.establishment_start_request_number = establishment_start_request_number;
    }

    @Override
    public String getEstablishment_start_resolution_number() {
        return establishment_start_resolution_number;
    }

    @Override
    public void setEstablishment_start_resolution_number(String establishment_start_resolution_number) {
        this.establishment_start_resolution_number = establishment_start_resolution_number;
    }

    @Override
    public String getVehicle_start_folder_number() {
        return vehicle_start_folder_number;
    }

    @Override
    public void setVehicle_start_folder_number(String vehicle_start_folder_number) {
        this.vehicle_start_folder_number = vehicle_start_folder_number;
    }

    @Override
    public String getVehicle_start_request_number() {
        return vehicle_start_request_number;
    }

    @Override
    public void setVehicle_start_request_number(String vehicle_start_request_number) {
        this.vehicle_start_request_number = vehicle_start_request_number;
    }

    @Override
    public String getVehicle_start_resolution_number() {
        return vehicle_start_resolution_number;
    }

    @Override
    public void setVehicle_start_resolution_number(String vehicle_start_resolution_number) {
        this.vehicle_start_resolution_number = vehicle_start_resolution_number;
    }

    @Override
    public String getStart_payment_number() {
        return start_payment_number;
    }

    @Override
    public void setStart_payment_number(String start_payment_number) {
        this.start_payment_number = start_payment_number;
    }

    @Override
    public String getStart_memorandum_number() {
        return start_memorandum_number;
    }

    @Override
    public void setStart_memorandum_number(String start_memorandum_number) {
        this.start_memorandum_number = start_memorandum_number;
    }

    @Override
    public String getStart_legal_memorandum_number() {
        return start_legal_memorandum_number;
    }

    @Override
    public void setStart_legal_memorandum_number(String start_legal_memorandum_number) {
        this.start_legal_memorandum_number = start_legal_memorandum_number;
    }

    @Override
    public LocalDate getDate() {
        return date;
    }

    @Override
    public void setDate(LocalDate date) {
        this.date = date;
    }

    @Override
    public String toString() {
        return "Configuration{" +
                "id=" + id +
                ", establishment_start_folder_number='" + establishment_start_folder_number + '\'' +
                ", establishment_start_request_number='" + establishment_start_request_number + '\'' +
                ", establishment_start_resolution_number='" + establishment_start_resolution_number + '\'' +
                ", vehicle_start_folder_number='" + vehicle_start_folder_number + '\'' +
                ", vehicle_start_request_number='" + vehicle_start_request_number + '\'' +
                ", vehicle_start_resolution_number='" + vehicle_start_resolution_number + '\'' +
                ", start_payment_number='" + start_payment_number + '\'' +
                ", start_memorandum_number='" + start_memorandum_number + '\'' +
                ", start_legal_memorandum_number='" + start_legal_memorandum_number + '\'' +
                '}';
    }
}
