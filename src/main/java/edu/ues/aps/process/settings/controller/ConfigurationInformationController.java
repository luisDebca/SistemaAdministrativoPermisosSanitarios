package edu.ues.aps.process.settings.controller;

import edu.ues.aps.process.settings.service.ConfigurationDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/configuration/default-values")
public class ConfigurationInformationController {

    private static final Logger logger = LogManager.getLogger(ConfigurationInformationController.class);

    private static final String CONFIGURATION_INDEX = "config-default";

    private final ConfigurationDTOFinderService finderService;

    public ConfigurationInformationController(ConfigurationDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/index")
    public String showIndex(ModelMap model){
        logger.info("GET:: Show system default values.");
        model.addAttribute("config",finderService.getConfig());
        return CONFIGURATION_INDEX;
    }

}
