package edu.ues.aps.process.settings.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "REPORT_FORMAT")
public class ReportFormat implements AbstractReportFormat {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 25)
    @Enumerated(EnumType.STRING)
    private ReportFormatType reportType;

    @Column(nullable = false, length = 100)
    private String reportName;

    @Column(nullable = false, length = 5000)
    private String format;

    @Column
    private Boolean isProtected;

    @Column
    private LocalDateTime lastModified;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public ReportFormatType getReportType() {
        return reportType;
    }

    @Override
    public void setReportType(ReportFormatType reportType) {
        this.reportType = reportType;
    }

    @Override
    public String getReportName() {
        return reportName;
    }

    @Override
    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    @Override
    public String getFormat() {
        return format;
    }

    @Override
    public void setFormat(String format) {
        this.format = format;
    }

    @Override
    public Boolean getProtected() {
        return isProtected;
    }

    @Override
    public void setProtected(Boolean aProtected) {
        isProtected = aProtected;
    }

    @Override
    public LocalDateTime getLastModified() {
        return lastModified;
    }

    @Override
    public void setLastModified(LocalDateTime lastModified) {
        this.lastModified = lastModified;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ReportFormat that = (ReportFormat) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(reportType, that.reportType) &&
                Objects.equals(reportName, that.reportName);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, reportType, reportName);
    }

    @Override
    public String toString() {
        return "ReportFormat{" +
                "id=" + id +
                ", reportType='" + reportType + '\'' +
                ", reportName='" + reportName + '\'' +
                ", format='" + format + '\'' +
                ", isProtected=" + isProtected +
                ", lastModified=" + lastModified +
                '}';
    }
}
