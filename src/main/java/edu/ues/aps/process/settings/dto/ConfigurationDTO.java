package edu.ues.aps.process.settings.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDate;

public class ConfigurationDTO implements AbstractConfigurationDTO {

    private Long id;
    private String establishment_folder_number;
    private String establishment_request_number;
    private String establishment_resolution_number;
    private String vehicle_folder_number;
    private String vehicle_request_number;
    private String vehicle_resolution_number;
    private String payment_number;
    private String memorandum_number;
    private String legal_memorandum_number;
    private LocalDate date;

    public ConfigurationDTO(Long id, String establishment_folder_number, String establishment_request_number, String establishment_resolution_number, String vehicle_folder_number, String vehicle_request_number, String vehicle_resolution_number, String payment_number, String memorandum_number, String legal_memorandum_number, LocalDate date) {
        this.id = id;
        this.establishment_folder_number = establishment_folder_number;
        this.establishment_request_number = establishment_request_number;
        this.establishment_resolution_number = establishment_resolution_number;
        this.vehicle_folder_number = vehicle_folder_number;
        this.vehicle_request_number = vehicle_request_number;
        this.vehicle_resolution_number = vehicle_resolution_number;
        this.payment_number = payment_number;
        this.memorandum_number = memorandum_number;
        this.legal_memorandum_number = legal_memorandum_number;
        this.date = date;
    }

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getEstablishment_folder_number() {
        return establishment_folder_number;
    }

    @Override
    public String getEstablishment_request_number() {
        return establishment_request_number;
    }

    @Override
    public String getEstablishment_resolution_number() {
        return establishment_resolution_number;
    }

    @Override
    public String getVehicle_folder_number() {
        return vehicle_folder_number;
    }

    @Override
    public String getVehicle_request_number() {
        return vehicle_request_number;
    }

    @Override
    public String getVehicle_resolution_number() {
        return vehicle_resolution_number;
    }

    @Override
    public String getPayment_number() {
        return payment_number;
    }

    @Override
    public String getMemorandum_number() {
        return memorandum_number;
    }

    @Override
    public String getLegal_memorandum_number() {
        return legal_memorandum_number;
    }

    @Override
    public String getDate() {
        return DatetimeUtil.parseDateToLongDateFormat(date);
    }
}

