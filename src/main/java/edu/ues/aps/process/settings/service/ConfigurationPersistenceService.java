package edu.ues.aps.process.settings.service;

public interface ConfigurationPersistenceService {

    void setEstablishmentFolderNumber(String number, Long id);

    void setEstablishmentRequestNumber(String number, Long id);

    void setEstablishmentResolutionNumber(String number, Long id);

    void setVehicleFolderNumber(String number, Long id);

    void setVehicleRequestNumber(String number, Long id);

    void setVehicleResolutionNumber(String number, Long id);

    void setPaymentNumber(String number, Long id);

    void setMemorandum(String number, Long id);

    void setLegalMemorandum(String number, Long id);
}
