package edu.ues.aps.process.settings.service;

import edu.ues.aps.process.settings.dto.AbstractReportFormatDTO;
import edu.ues.aps.process.settings.model.AbstractReportFormat;
import edu.ues.aps.process.settings.model.ReportFormatType;
import edu.ues.aps.process.settings.repository.ReportFormatDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true)
public class ReportFormatDTOService implements ReportFormatDTOFinderService, ReportFormatContentService {

    private final ReportFormatDTOFinderRepository finderRepository;

    public ReportFormatDTOService(ReportFormatDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractReportFormatDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public Map<String, String> getFormats(ReportFormatType type) {
        Map<String,String> formats = new HashMap<>();
        for (AbstractReportFormat format: finderRepository.findAll(type)){
            formats.put(format.getReportName(),format.getFormat());
        }
        return formats;
    }
}
