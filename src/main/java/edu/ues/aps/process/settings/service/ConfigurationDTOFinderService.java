package edu.ues.aps.process.settings.service;

import edu.ues.aps.process.settings.dto.AbstractConfigurationDTO;

public interface ConfigurationDTOFinderService {

    AbstractConfigurationDTO getConfig();
}
