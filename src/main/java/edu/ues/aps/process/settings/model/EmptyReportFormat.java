package edu.ues.aps.process.settings.model;

import java.time.LocalDateTime;

public class EmptyReportFormat implements AbstractReportFormat {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public ReportFormatType getReportType() {
        return ReportFormatType.INVALID;
    }

    @Override
    public void setReportType(ReportFormatType reportType) {

    }

    @Override
    public String getReportName() {
        return "Format name not found";
    }

    @Override
    public void setReportName(String reportName) {

    }

    @Override
    public String getFormat() {
        return "Format not found";
    }

    @Override
    public void setFormat(String format) {

    }

    @Override
    public Boolean getProtected() {
        return false;
    }

    @Override
    public void setProtected(Boolean aProtected) {

    }

    @Override
    public LocalDateTime getLastModified() {
        return LocalDateTime.now();
    }

    @Override
    public void setLastModified(LocalDateTime lastModified) {

    }
}
