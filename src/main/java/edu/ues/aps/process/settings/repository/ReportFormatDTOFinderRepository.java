package edu.ues.aps.process.settings.repository;

import edu.ues.aps.process.settings.dto.AbstractReportFormatDTO;
import edu.ues.aps.process.settings.model.AbstractReportFormat;
import edu.ues.aps.process.settings.model.ReportFormatType;

import java.util.List;

public interface ReportFormatDTOFinderRepository {

    List<AbstractReportFormatDTO> findAll();

    List<AbstractReportFormat> findAll(ReportFormatType type);
}
