package edu.ues.aps.process.settings.service;

import edu.ues.aps.process.base.model.Place;
import edu.ues.aps.process.settings.repository.PlacePersistenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class PlaceService implements PlacePersistenceService {

    private final PlacePersistenceRepository persistenceRepository;

    public PlaceService(PlacePersistenceRepository persistenceRepository) {
        this.persistenceRepository = persistenceRepository;
    }

    @Override
    public Place get() {
        return persistenceRepository.get();
    }

    @Override
    public void save(Place place) {
        persistenceRepository.save(place);
    }

    @Override
    public void update(Place draft) {
        Place place = persistenceRepository.get();
        place.setZone(draft.getZone());
        place.setAddress(draft.getAddress());
        place.setDepartment(draft.getDepartment());
        place.setDepartment_code(draft.getDepartment_code());
        place.setMunicipality(draft.getMunicipality());
        place.setEmail(draft.getEmail());
        place.setFax(draft.getFax());
        place.setTelephone(draft.getTelephone());
        place.setAvailability(draft.getAvailability());
        place.setOrganization_name(draft.getOrganization_name());
        place.setBusiness_description(draft.getBusiness_description());
        place.setSection_name(draft.getSection_name());
    }
}
