package edu.ues.aps.process.settings.service;

import edu.ues.aps.process.settings.model.AbstractReportFormat;
import edu.ues.aps.process.settings.repository.ReportFormatPersistenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
public class ReportFormatService implements ReportFormatPersistenceService {

    private final ReportFormatPersistenceRepository persistenceRepository;

    public ReportFormatService(ReportFormatPersistenceRepository persistenceRepository) {
        this.persistenceRepository = persistenceRepository;
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractReportFormat find(Long id_report) {
        return persistenceRepository.find(id_report);
    }

    @Override
    @Transactional
    public void save(AbstractReportFormat format) {
        format.setProtected(false);
        format.setLastModified(LocalDateTime.now());
        persistenceRepository.save(format);
    }

    @Override
    @Transactional
    public void update(AbstractReportFormat format) {
        AbstractReportFormat persistence = persistenceRepository.find(format.getId());
        persistence.setReportName(format.getReportName());
        persistence.setFormat(format.getFormat());
        persistence.setReportType(format.getReportType());
        persistence.setLastModified(LocalDateTime.now());
    }

    @Override
    @Transactional
    public void setProtect(Long id_format) {
        AbstractReportFormat format = persistenceRepository.find(id_format);
        format.setProtected(!format.getProtected());
        format.setLastModified(LocalDateTime.now());
    }

    @Override
    @Transactional
    public void delete(Long id_format) {
        persistenceRepository.delete(persistenceRepository.find(id_format));
    }
}
