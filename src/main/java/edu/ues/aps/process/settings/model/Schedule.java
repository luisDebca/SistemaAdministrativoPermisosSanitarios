package edu.ues.aps.process.settings.model;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalTime;

public class Schedule {

    private LocalDate date;
    private LocalTime time;
    private DayOfWeek dayOfWeek;
    private Integer month;
    private Integer year;
    private Boolean allDays;
    private Boolean allWeeks;
    private Boolean allMonths;
    private Boolean allYears;
}
