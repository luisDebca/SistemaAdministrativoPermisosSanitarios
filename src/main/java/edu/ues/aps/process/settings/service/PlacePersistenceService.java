package edu.ues.aps.process.settings.service;

import edu.ues.aps.process.base.model.Place;

public interface PlacePersistenceService {

    Place get();

    void save(Place place);

    void update(Place place);
}
