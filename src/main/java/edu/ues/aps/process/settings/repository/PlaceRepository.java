package edu.ues.aps.process.settings.repository;

import edu.ues.aps.process.base.model.Place;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class PlaceRepository implements PlacePersistenceRepository {

    private final SessionFactory sessionFactory;

    public PlaceRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public Place get() {
        return sessionFactory.getCurrentSession().get(Place.class,1L);
    }

    @Override
    public void save(Place place) {
        sessionFactory.getCurrentSession().save(place);
    }
}
