package edu.ues.aps.process.settings.model;

import java.time.LocalDate;

public class EmptyConfiguration implements AbstractConfiguration {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getEstablishment_start_folder_number() {
        return "0";
    }

    @Override
    public void setEstablishment_start_folder_number(String establishment_start_folder_number) {

    }

    @Override
    public String getEstablishment_start_request_number() {
        return "0";
    }

    @Override
    public void setEstablishment_start_request_number(String establishment_start_request_number) {

    }

    @Override
    public String getEstablishment_start_resolution_number() {
        return "0";
    }

    @Override
    public void setEstablishment_start_resolution_number(String establishment_start_resolution_number) {

    }

    @Override
    public String getVehicle_start_folder_number() {
        return "0";
    }

    @Override
    public void setVehicle_start_folder_number(String vehicle_start_folder_number) {

    }

    @Override
    public String getVehicle_start_request_number() {
        return "0";
    }

    @Override
    public void setVehicle_start_request_number(String vehicle_start_request_number) {

    }

    @Override
    public String getVehicle_start_resolution_number() {
        return "0";
    }

    @Override
    public void setVehicle_start_resolution_number(String vehicle_start_resolution_number) {

    }

    @Override
    public String getStart_payment_number() {
        return "0";
    }

    @Override
    public void setStart_payment_number(String start_payment_number) {

    }

    @Override
    public String getStart_memorandum_number() {
        return "0";
    }

    @Override
    public void setStart_memorandum_number(String start_memorandum_number) {

    }

    @Override
    public String getStart_legal_memorandum_number() {
        return "0";
    }

    @Override
    public void setStart_legal_memorandum_number(String start_legal_memorandum_number) {

    }

    @Override
    public LocalDate getDate() {
        return LocalDate.now();
    }

    @Override
    public void setDate(LocalDate date) {

    }

    @Override
    public String toString() {
        return "EmptyConfiguration{}";
    }
}
