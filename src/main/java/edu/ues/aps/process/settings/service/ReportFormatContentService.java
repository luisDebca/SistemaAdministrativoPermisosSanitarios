package edu.ues.aps.process.settings.service;

import edu.ues.aps.process.settings.model.ReportFormatType;

import java.util.Map;

public interface ReportFormatContentService {

    Map<String, String> getFormats(ReportFormatType type);
}
