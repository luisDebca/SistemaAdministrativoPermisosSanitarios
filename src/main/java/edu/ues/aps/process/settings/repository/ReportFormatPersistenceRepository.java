package edu.ues.aps.process.settings.repository;

import edu.ues.aps.process.settings.model.AbstractReportFormat;

import javax.persistence.NoResultException;

public interface ReportFormatPersistenceRepository {

    AbstractReportFormat find(Long id_format) throws NoResultException;

    void save(AbstractReportFormat format);

    void delete(AbstractReportFormat format);
}
