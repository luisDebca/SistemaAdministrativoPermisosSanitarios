package edu.ues.aps.process.settings.model;

import java.time.LocalDate;

public interface AbstractConfiguration {

    Long getId();

    void setId(Long id);

    String getEstablishment_start_folder_number();

    void setEstablishment_start_folder_number(String establishment_start_folder_number);

    String getEstablishment_start_request_number();

    void setEstablishment_start_request_number(String establishment_start_request_number);

    String getEstablishment_start_resolution_number();

    void setEstablishment_start_resolution_number(String establishment_start_resolution_number);

    String getVehicle_start_folder_number();

    void setVehicle_start_folder_number(String vehicle_start_folder_number);

    String getVehicle_start_request_number();

    void setVehicle_start_request_number(String vehicle_start_request_number);

    String getVehicle_start_resolution_number();

    void setVehicle_start_resolution_number(String vehicle_start_resolution_number);

    String getStart_payment_number();

    void setStart_payment_number(String start_payment_number);

    String getStart_memorandum_number();

    void setStart_memorandum_number(String start_memorandum_number);

    String getStart_legal_memorandum_number();

    void setStart_legal_memorandum_number(String start_legal_memorandum_number);

    LocalDate getDate();

    void setDate(LocalDate date);
}
