package edu.ues.aps.process.settings.service;

import edu.ues.aps.process.settings.dto.AbstractReportFormatDTO;

import java.util.List;

public interface ReportFormatDTOFinderService {

    List<AbstractReportFormatDTO> findAll();
}
