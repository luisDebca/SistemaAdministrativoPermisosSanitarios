package edu.ues.aps.process.settings.dto;

public class EmptyConfigurationDTO implements AbstractConfigurationDTO {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getEstablishment_folder_number() {
        return "0";
    }

    @Override
    public String getEstablishment_request_number() {
        return "0";
    }

    @Override
    public String getEstablishment_resolution_number() {
        return "0";
    }

    @Override
    public String getVehicle_folder_number() {
        return "0";
    }

    @Override
    public String getVehicle_request_number() {
        return "0";
    }

    @Override
    public String getVehicle_resolution_number() {
        return "0";
    }

    @Override
    public String getPayment_number() {
        return "0";
    }

    @Override
    public String getMemorandum_number() {
        return "0";
    }

    @Override
    public String getLegal_memorandum_number() {
        return "0";
    }

    @Override
    public String getDate() {
        return "--/--/--";
    }
}
