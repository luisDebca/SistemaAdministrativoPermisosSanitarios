package edu.ues.aps.process.settings.repository;

import edu.ues.aps.process.base.model.Place;

public interface PlacePersistenceRepository {

    Place get();

    void save(Place place);
}
