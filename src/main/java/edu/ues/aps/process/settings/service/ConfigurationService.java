package edu.ues.aps.process.settings.service;

import edu.ues.aps.process.settings.model.AbstractConfiguration;
import edu.ues.aps.process.settings.model.Configuration;
import edu.ues.aps.process.settings.repository.ConfigurationPersistenceRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDate;

@Service
@Transactional
public class ConfigurationService implements ConfigurationPersistenceService {

    private final ConfigurationPersistenceRepository persistenceRepository;
    private AbstractConfiguration currentConfiguration;

    public ConfigurationService(ConfigurationPersistenceRepository persistenceRepository) {
        this.persistenceRepository = persistenceRepository;
    }

    @Override
    public void setEstablishmentFolderNumber(String number, Long id) {
        validateConfiguration();
        currentConfiguration.setEstablishment_start_folder_number(number);
    }

    @Override
    public void setEstablishmentRequestNumber(String number, Long id) {
        validateConfiguration();
        currentConfiguration.setEstablishment_start_request_number(number);
    }

    @Override
    public void setEstablishmentResolutionNumber(String number, Long id) {
        validateConfiguration();
        currentConfiguration.setEstablishment_start_resolution_number(number);
    }

    @Override
    public void setVehicleFolderNumber(String number, Long id) {
        validateConfiguration();
        currentConfiguration.setVehicle_start_folder_number(number);
    }

    @Override
    public void setVehicleRequestNumber(String number, Long id) {
        validateConfiguration();
        currentConfiguration.setVehicle_start_request_number(number);
    }

    @Override
    public void setVehicleResolutionNumber(String number, Long id) {
        validateConfiguration();
        currentConfiguration.setVehicle_start_resolution_number(number);
    }

    @Override
    public void setPaymentNumber(String number, Long id) {
        validateConfiguration();
        currentConfiguration.setStart_payment_number(number);
    }

    @Override
    public void setMemorandum(String number, Long id) {
        validateConfiguration();
        currentConfiguration.setStart_memorandum_number(number);
    }

    @Override
    public void setLegalMemorandum(String number, Long id) {
        validateConfiguration();
        currentConfiguration.setStart_legal_memorandum_number(number);
    }

    private void validateConfiguration(){
        if (currentConfiguration == null) {
            setCurrentConfiguration();
        }else {
            persistenceRepository.refresh(currentConfiguration);
        }
    }

    private void setCurrentConfiguration(){
        try {
            currentConfiguration = persistenceRepository.getConfig();
        } catch (NoResultException e) {
            createNewConfiguration();
        }
    }

    private void createNewConfiguration(){
        currentConfiguration = new Configuration();
        currentConfiguration.setDate(LocalDate.now());
        persistenceRepository.save(currentConfiguration);
    }
}
