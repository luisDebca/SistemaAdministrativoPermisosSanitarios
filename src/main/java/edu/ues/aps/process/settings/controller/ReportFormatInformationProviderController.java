package edu.ues.aps.process.settings.controller;

import edu.ues.aps.process.settings.service.ReportFormatDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/configuration/report-format")
public class ReportFormatInformationProviderController {

    private static final Logger logger = LogManager.getLogger(ReportFormatInformationProviderController.class);

    private static final String INDEX = "format-index";

    private final ReportFormatDTOFinderService finderService;

    public ReportFormatInformationProviderController(ReportFormatDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping("/index")
    public String showReportFormats(ModelMap model){
        logger.info("GET:: Show report format index.");
        model.addAttribute("formats", finderService.findAll());
        return INDEX;
    }

}
