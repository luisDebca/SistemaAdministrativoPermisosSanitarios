package edu.ues.aps.process.settings.dto;

import edu.ues.aps.process.settings.model.ReportFormatType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class ReportFormatDTO implements AbstractReportFormatDTO {

    private Long id;
    private ReportFormatType type;
    private String name;
    private String format;
    private Boolean isProtected;
    private LocalDateTime modifiedOn;

    public ReportFormatDTO(Long id, ReportFormatType type, String name, String format, Boolean isProtected, LocalDateTime modifiedOn) {
        this.id = id;
        this.type = type;
        this.name = name;
        this.format = format;
        this.isProtected = isProtected;
        this.modifiedOn = modifiedOn;
    }

    public Long getId() {
        return id;
    }

    public String getType() {
        if (type == null)
            return ReportFormatType.INVALID.name();
        return type.name();
    }

    public String getName() {
        return name;
    }

    public String getFormat() {
        String simpleText = format.replaceAll("\\<.*?>","");
        if (simpleText.length() > 500)
            return simpleText.substring(0,499).concat("...");
        return simpleText;
    }

    public Boolean getProtection() {
        return isProtected;
    }

    public String getModifiedOn() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(modifiedOn);
    }
}
