package edu.ues.aps.process.settings.repository;

import edu.ues.aps.process.settings.model.AbstractConfiguration;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.time.LocalDate;

@Repository
public class ConfigurationRepository implements ConfigurationPersistenceRepository {

    private final SessionFactory sessionFactory;

    public ConfigurationRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractConfiguration getConfig() throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select conf " +
                "from Configuration conf " +
                "where conf.date >= :yearFirstDay", AbstractConfiguration.class)
                .setParameter("yearFirstDay", LocalDate.now().withMonth(1).withDayOfMonth(1))
                .getSingleResult();
    }

    @Override
    public void save(AbstractConfiguration configuration) {
        sessionFactory.getCurrentSession().save(configuration);
    }

    @Override
    public void refresh(AbstractConfiguration configuration) {
        sessionFactory.getCurrentSession().refresh(configuration);
    }
}
