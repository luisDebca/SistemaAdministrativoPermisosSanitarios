package edu.ues.aps.process.settings.model;

import java.time.LocalDateTime;

public interface AbstractReportFormat {

    Long getId();

    void setId(Long id);

    ReportFormatType getReportType();

    void setReportType(ReportFormatType reportType);

    String getReportName();

    void setReportName(String reportName);

    String getFormat();

    void setFormat(String format);

    Boolean getProtected();

    void setProtected(Boolean aProtected);

    LocalDateTime getLastModified();

    void setLastModified(LocalDateTime lastModified);
}
