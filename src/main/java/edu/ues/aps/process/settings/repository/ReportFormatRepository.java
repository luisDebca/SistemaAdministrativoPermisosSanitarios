package edu.ues.aps.process.settings.repository;

import edu.ues.aps.process.settings.model.AbstractReportFormat;
import edu.ues.aps.process.settings.model.ReportFormat;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class ReportFormatRepository implements ReportFormatPersistenceRepository {

    private final SessionFactory sessionFactory;

    public ReportFormatRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractReportFormat find(Long id_format) throws NoResultException {
        return sessionFactory.getCurrentSession().get(ReportFormat.class,id_format);
    }

    @Override
    public void save(AbstractReportFormat format) {
        sessionFactory.getCurrentSession().save(format);
    }

    @Override
    public void delete(AbstractReportFormat format) {
        sessionFactory.getCurrentSession().delete(format);
    }
}
