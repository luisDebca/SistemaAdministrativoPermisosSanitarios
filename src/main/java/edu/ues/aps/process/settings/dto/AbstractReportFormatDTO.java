package edu.ues.aps.process.settings.dto;

public interface AbstractReportFormatDTO {

    Long getId();

    String getType();

    String getName();

    String getFormat();

    Boolean getProtection();

    String getModifiedOn();
}
