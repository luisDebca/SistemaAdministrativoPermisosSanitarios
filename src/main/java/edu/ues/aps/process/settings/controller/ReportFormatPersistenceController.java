package edu.ues.aps.process.settings.controller;

import edu.ues.aps.process.settings.model.ReportFormat;
import edu.ues.aps.process.settings.service.ReportFormatPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/configuration/report-format")
public class ReportFormatPersistenceController {

    private static final Logger logger = LogManager.getLogger(ReportFormatPersistenceController.class);

    private static final String FORM = "format-form";

    private final ReportFormatPersistenceService persistenceService;

    public ReportFormatPersistenceController(ReportFormatPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping("/new")
    public String showNewFormatForm(ModelMap model){
        logger.info("GET:: New format form.");
        model.addAttribute("format", new ReportFormat());
        return FORM;
    }

    @PostMapping("/new")
    public String saveNewFormat(@ModelAttribute ReportFormat format){
        logger.info("POST:: Save new format.");
        persistenceService.save(format);
        return "redirect:/configuration/report-format/index";
    }

    @GetMapping("/{id_format}/edit")
    public String showEditFormatForm(@PathVariable Long id_format, ModelMap model){
        logger.info("GET:: Edit {} format form.",id_format);
        model.addAttribute("format", persistenceService.find(id_format));
        return FORM;
    }

    @PostMapping("/{id_format}/edit")
    public String updateFormat(@PathVariable Long id_format, @ModelAttribute ReportFormat format){
        logger.info("POST:: Update {} format.", id_format);
        persistenceService.update(format);
        return "redirect:/configuration/report-format/index";
    }

    @GetMapping({"/{id_format}/lock","/{id_format}/unlock"})
    public String changeProtectStatus(@PathVariable Long id_format){
        logger.info("GET:: Update {} format protect status.",id_format);
        persistenceService.setProtect(id_format);
        return "redirect:/configuration/report-format/index";
    }

    @GetMapping("/{id_format}/delete")
    public String deleteFormat(@PathVariable Long id_format){
        logger.info("GET:: Delete {} format.",id_format);
        persistenceService.delete(id_format);
        return "redirect:/configuration/report-format/index";
    }

}
