package edu.ues.aps.process.settings.repository;

import edu.ues.aps.process.settings.dto.AbstractReportFormatDTO;
import edu.ues.aps.process.settings.model.AbstractReportFormat;
import edu.ues.aps.process.settings.model.ReportFormatType;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ReportFormatDTORepository implements ReportFormatDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public ReportFormatDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractReportFormatDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.settings.dto.ReportFormatDTO(" +
                "report.id,report.reportType,report.reportName,report.format,report.isProtected,report.lastModified) " +
                "from ReportFormat report", AbstractReportFormatDTO.class)
                .getResultList();
    }

    @Override
    public List<AbstractReportFormat> findAll(ReportFormatType type) {
        return sessionFactory.getCurrentSession().createQuery("select format " +
                "from ReportFormat format " +
                "where format.reportType = :rtype",AbstractReportFormat.class)
                .setParameter("rtype",type)
                .getResultList();
    }
}
