package edu.ues.aps.process.settings.service;

import edu.ues.aps.process.settings.dto.AbstractConfigurationDTO;
import edu.ues.aps.process.settings.dto.EmptyConfigurationDTO;
import edu.ues.aps.process.settings.repository.ConfigurationDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional(readOnly = true)
public class ConfigurationDTOService implements ConfigurationDTOFinderService {

    private final ConfigurationDTOFinderRepository finderRepository;

    public ConfigurationDTOService(ConfigurationDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractConfigurationDTO getConfig() {
        try {
            return finderRepository.getCustomConfig();
        } catch (NoResultException e) {
            return new EmptyConfigurationDTO();
        }
    }
}
