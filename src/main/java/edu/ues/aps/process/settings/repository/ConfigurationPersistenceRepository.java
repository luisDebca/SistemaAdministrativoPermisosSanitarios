package edu.ues.aps.process.settings.repository;

import edu.ues.aps.process.settings.model.AbstractConfiguration;

import javax.persistence.NoResultException;

public interface ConfigurationPersistenceRepository {

    AbstractConfiguration getConfig() throws NoResultException;

    void save(AbstractConfiguration configuration);

    void refresh(AbstractConfiguration configuration);
}
