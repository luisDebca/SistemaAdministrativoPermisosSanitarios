package edu.ues.aps.process.settings.repository;

import edu.ues.aps.process.settings.dto.AbstractConfigurationDTO;

import javax.persistence.NoResultException;

public interface ConfigurationDTOFinderRepository {

    AbstractConfigurationDTO getCustomConfig() throws NoResultException;
}
