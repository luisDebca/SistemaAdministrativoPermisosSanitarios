package edu.ues.aps.process.settings.dto;

public interface AbstractConfigurationDTO {

    Long getId();

    void setId(Long id);

    String getEstablishment_folder_number();

    String getEstablishment_request_number();

    String getEstablishment_resolution_number();

    String getVehicle_folder_number();

    String getVehicle_request_number();

    String getVehicle_resolution_number();

    String getPayment_number();

    String getMemorandum_number();

    String getLegal_memorandum_number();

    String getDate();
}
