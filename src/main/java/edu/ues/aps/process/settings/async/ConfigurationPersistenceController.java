package edu.ues.aps.process.settings.async;

import edu.ues.aps.process.settings.service.ConfigurationPersistenceService;
import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/configuration/default-value")
public class ConfigurationPersistenceController {

    private static final Logger logger = LogManager.getLogger(ConfigurationPersistenceController.class);

    private final ConfigurationPersistenceService persistenceService;

    public ConfigurationPersistenceController(ConfigurationPersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping(value = "/establishment-folder-number", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setEstablishmentDefaultFolderNumber(@RequestParam Long id_conf, @RequestParam String value){
        logger.info("GET:: Save default value {}",value);
        try {
            persistenceService.setEstablishmentFolderNumber(value,id_conf);
            return new SingleStringResponse("OK").asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

    @GetMapping(value = "/establishment-request-number", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setEstablishmentDefaultRequestNumber(@RequestParam Long id_conf, @RequestParam String value){
        logger.info("GET:: Save default value {}",value);
        try {
            persistenceService.setEstablishmentRequestNumber(value,id_conf);
            return new SingleStringResponse("OK").asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

    @GetMapping(value = "/establishment-resolution-number", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setEstablishmentDefaultResolutionNumber(@RequestParam Long id_conf, @RequestParam String value){
        logger.info("GET:: Save default value {}",value);
        try {
            persistenceService.setEstablishmentResolutionNumber(value,id_conf);
            return new SingleStringResponse("OK").asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

    @GetMapping(value = "/vehicle-folder-number", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setVehicleFolderNumber(@RequestParam Long id_conf, @RequestParam String value){
        logger.info("GET:: Save default value {}",value);
        try {
            persistenceService.setVehicleFolderNumber(value,id_conf);
            return new SingleStringResponse("OK").asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

    @GetMapping(value = "/vehicle-request-number", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setVehicleRequestNumber(@RequestParam Long id_conf, @RequestParam String value){
        logger.info("GET:: Save default value {}",value);
        try {
            persistenceService.setVehicleRequestNumber(value,id_conf);
            return new SingleStringResponse("OK").asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

    @GetMapping(value = "/vehicle-resolution-number", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setVehicleResolutionNumber(@RequestParam Long id_conf, @RequestParam String value){
        logger.info("GET:: Save default value {}",value);
        try {
            persistenceService.setVehicleResolutionNumber(value,id_conf);
            return new SingleStringResponse("OK").asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

    @GetMapping(value = "/payment-procedure", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setPaymentProcedure(@RequestParam Long id_conf, @RequestParam String value){
        logger.info("GET:: Save default value {}",value);
        try {
            persistenceService.setPaymentNumber(value,id_conf);
            return new SingleStringResponse("OK").asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

    @GetMapping(value = "/legal-memorandum", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setLegalMemorandum(@RequestParam Long id_conf, @RequestParam String value){
        logger.info("GET:: Save default value {}",value);
        try {
            persistenceService.setLegalMemorandum(value,id_conf);
            return new SingleStringResponse("OK").asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }

    @GetMapping(value = "/memorandum", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse setMemorandum(@RequestParam Long id_conf, @RequestParam String value){
        logger.info("GET:: Save default value {}",value);
        try {
            persistenceService.setMemorandum(value,id_conf);
            return new SingleStringResponse("OK").asSuccessResponse();
        } catch (Exception e) {
            return new SingleStringResponse(e.getMessage()).asErrorResponse();
        }
    }
}
