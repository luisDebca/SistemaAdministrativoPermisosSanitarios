package edu.ues.aps.process.settings.repository;

import edu.ues.aps.process.settings.dto.AbstractConfigurationDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public class ConfigurationDTORepository implements ConfigurationDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public ConfigurationDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractConfigurationDTO getCustomConfig() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.settings.dto.ConfigurationDTO( " +
                "conf.id, conf.establishment_start_folder_number,conf.establishment_start_request_number,conf.establishment_start_resolution_number," +
                "conf.vehicle_start_folder_number,conf.vehicle_start_request_number,conf.vehicle_start_resolution_number," +
                "conf.start_payment_number,conf.start_memorandum_number,conf.start_legal_memorandum_number,conf.date)" +
                "from Configuration conf " +
                "where conf.date >= :yearFirstDay",AbstractConfigurationDTO.class)
                .setParameter("yearFirstDay", LocalDate.now().withMonth(1).withDayOfMonth(1))
                .getSingleResult();
    }
}
