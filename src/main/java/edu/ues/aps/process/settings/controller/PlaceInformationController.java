package edu.ues.aps.process.settings.controller;

import edu.ues.aps.process.base.model.Place;
import edu.ues.aps.process.settings.service.PlacePersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/app/information")
public class PlaceInformationController {

    private static final Logger logger = LogManager.getLogger(PlaceInformationController.class);

    private static final String PLACE_EDIT = "app-edit";

    private final PlacePersistenceService persistenceService;

    public PlaceInformationController(PlacePersistenceService persistenceService) {
        this.persistenceService = persistenceService;
    }

    @GetMapping("/init")
    public String initInformation(ModelMap model) {
        logger.info("GET:: Set app info.");
        model.addAttribute("info", new Place());
        return PLACE_EDIT;
    }

    @PostMapping("/init")
    public String saveInformation(@ModelAttribute Place place) {
        logger.info("POST:: Save app info.");;
        persistenceService.save(place);
        return "redirect:/app/information/index";
    }

    @GetMapping("/edit")
    public String showEditForm(ModelMap model) {
        logger.info("GET:: Show information edit form.");
        model.addAttribute("info",persistenceService.get());
        return PLACE_EDIT;
    }

    @PostMapping("/edit")
    public String updatePlaceInfo(@ModelAttribute Place place) {
        logger.info("POST:: Update app information.");
        persistenceService.update(place);
        return "redirect:/app/information/index";
    }
}
