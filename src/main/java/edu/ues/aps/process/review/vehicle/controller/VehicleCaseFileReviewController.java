package edu.ues.aps.process.review.vehicle.controller;

import edu.ues.aps.process.review.vehicle.service.VehicleRequestPreReviewFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/case-file/vehicle/request")
public class VehicleCaseFileReviewController {

    private static final Logger logger = LogManager.getLogger(VehicleCaseFileReviewController.class);

    private static final String ALL_REVIEW = "vehicle_request_review_list";
    private static final String REVIEW = "vehicle_request_review";

    private final VehicleRequestPreReviewFinderService preReviewFinderService;

    public VehicleCaseFileReviewController(VehicleRequestPreReviewFinderService preReviewFinderService) {
        this.preReviewFinderService = preReviewFinderService;
    }

    @GetMapping("/review/list")
    public String showAllRequest(ModelMap model) {
        logger.info("GET:: Show all request for pre-review");
        model.addAttribute("requests", preReviewFinderService.findAll());
        return ALL_REVIEW;
    }

    @GetMapping("/bunch/{id_bunch}/review")
    public String showRequestRequiredFieldsValidator(@PathVariable Long id_bunch, ModelMap model) {
        logger.info("GET:: Show bunch {} request field validator.", id_bunch);
        preReviewFinderService.setToFindBunch(id_bunch);
        model.addAttribute("bunch", preReviewFinderService.getBunch());
        model.addAttribute("caseFile", preReviewFinderService.getAllCaseFiles());
        model.addAttribute("vehicles", preReviewFinderService.findAllVehicles());
        model.addAttribute("legal_owner", preReviewFinderService.getLegalOwner());
        model.addAttribute("natural_owner", preReviewFinderService.getNaturalOwner());
        model.addAttribute("clients", preReviewFinderService.getClientList());
        return REVIEW;
    }

    @GetMapping("/bunch/{id_bunch}/validated")
    public String setAllCaseFilesAsValidated(@PathVariable Long id_bunch) {
        logger.info("POST:: Update all case file in bunch {} validate status to true.");
        preReviewFinderService.updateAllValidateStatus(id_bunch);
        return "redirect:/case-file/vehicle/request/review/list";
    }
}
