package edu.ues.aps.process.review.vehicle.service;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleBunchDTO;
import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO;
import edu.ues.aps.process.area.vehicle.service.VehicleBunchDTOFinderService;
import edu.ues.aps.process.area.vehicle.service.VehicleBunchInformationService;
import edu.ues.aps.process.area.vehicle.service.VehicleBunchRelationshipService;
import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.AbstractLegalEntityDTO;
import edu.ues.aps.process.base.dto.AbstractNaturalEntityDTO;
import edu.ues.aps.process.base.service.CaseFileRequiredReviewValidatorService;
import edu.ues.aps.process.base.service.OwnerDTOFinderService;
import edu.ues.aps.process.review.vehicle.dto.AbstractVehicleRequestPreReviewDTO;
import edu.ues.aps.process.review.vehicle.repository.VehicleRequestPreReviewFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
public class VehicleRequestReviewService implements VehicleRequestPreReviewFinderService {

    private final OwnerDTOFinderService ownerFinderService;
    private final VehicleBunchDTOFinderService bunchFinderService;
    private final VehicleBunchRelationshipService relationshipService;
    private final VehicleBunchInformationService bunchInformationService;
    private final CaseFileRequiredReviewValidatorService reviewValidatorService;
    private final VehicleRequestPreReviewFinderRepository preReviewFinderRepository;

    private AbstractVehicleBunchDTO bunch;

    public VehicleRequestReviewService(VehicleRequestPreReviewFinderRepository preReviewFinderRepository, VehicleBunchInformationService bunchInformationService, OwnerDTOFinderService ownerFinderService, VehicleBunchDTOFinderService bunchFinderService, VehicleBunchRelationshipService relationshipService, CaseFileRequiredReviewValidatorService reviewValidatorService) {
        this.preReviewFinderRepository = preReviewFinderRepository;
        this.bunchInformationService = bunchInformationService;
        this.ownerFinderService = ownerFinderService;
        this.bunchFinderService = bunchFinderService;
        this.relationshipService = relationshipService;
        this.reviewValidatorService = reviewValidatorService;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AbstractVehicleRequestPreReviewDTO> findAll() {
        return preReviewFinderRepository.findAll();
    }

    @Override
    @Transactional(readOnly = true)
    public void setToFindBunch(Long id_bunch) {
        bunch = bunchFinderService.findById(id_bunch);
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractVehicleBunchDTO getBunch() {
        return bunch;
    }

    @Override
    @Transactional(readOnly = true)
    public List<AbstractCaseFileDTO> getAllCaseFiles() {
        return relationshipService.findAllCaseFiles(bunch.getId_bunch());
    }

    @Override
    @Transactional(readOnly = true)
    public List<AbstractVehicleDTO> findAllVehicles() {
        return relationshipService.findAllVehicles(bunch.getId_bunch());
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractNaturalEntityDTO getNaturalOwner() {
        return ownerFinderService.findNaturalEntity(bunch.getId_owner());
    }

    @Override
    @Transactional(readOnly = true)
    public AbstractLegalEntityDTO getLegalOwner() {
        return ownerFinderService.findLegalEntity(bunch.getId_owner());
    }

    @Override
    @Transactional(readOnly = true)
    public List<AbstractClientDTO> getClientList() {
        return relationshipService.findAllClients(bunch.getId_bunch());
    }

    @Override
    @Transactional
    public void updateAllValidateStatus(Long id_bunch) {
        for (Long id_caseFile : bunchInformationService.findAllCaseFileId(id_bunch)) {
            reviewValidatorService.validateCaseFile(id_caseFile);
        }
    }
}
