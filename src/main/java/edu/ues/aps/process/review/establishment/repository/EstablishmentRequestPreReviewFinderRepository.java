package edu.ues.aps.process.review.establishment.repository;

import edu.ues.aps.process.review.establishment.dto.AbstractEstablishmentRequestPreReviewDTO;

import java.util.List;

public interface EstablishmentRequestPreReviewFinderRepository {

    List<AbstractEstablishmentRequestPreReviewDTO> findAll();
}
