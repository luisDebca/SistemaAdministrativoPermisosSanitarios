package edu.ues.aps.process.review.vehicle.repository;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.review.vehicle.dto.AbstractVehicleRequestPreReviewDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class VehicleRequestReviewRepository implements VehicleRequestPreReviewFinderRepository {

    private final SessionFactory sessionFactory;

    public VehicleRequestReviewRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractVehicleRequestPreReviewDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.review.vehicle.dto.VehicleRequestPreReviewDTO(" +
                "bunch.id,owner.id,owner.name,bunch.client_presented_name,bunch.client_presented_telephone,bunch.distribution_company," +
                "bunch.distribution_destination,bunch.marketing_place," +
                "concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "count (vehicle), concat(concat(cv.request_folder,'-'),cv.request_year),cv.creation_date,cv.validate)" +
                "from VehicleBunch bunch " +
                "inner join bunch.casefileVehicles cv " +
                "inner join cv.vehicle vehicle " +
                "left join bunch.address address " +
                "inner join vehicle.owner owner " +
                "inner join cv.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id " +
                "group by bunch.id " +
                "order by bunch.id", AbstractVehicleRequestPreReviewDTO.class)
                .setParameter("id", ProcessIdentifier.DOCUMENTATION_REVIEW)
                .getResultList();
    }
}
