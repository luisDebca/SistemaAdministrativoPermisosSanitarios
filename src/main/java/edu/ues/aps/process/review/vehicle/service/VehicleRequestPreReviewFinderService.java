package edu.ues.aps.process.review.vehicle.service;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleBunchDTO;
import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO;
import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.AbstractLegalEntityDTO;
import edu.ues.aps.process.base.dto.AbstractNaturalEntityDTO;
import edu.ues.aps.process.review.vehicle.dto.AbstractVehicleRequestPreReviewDTO;

import java.util.List;

public interface VehicleRequestPreReviewFinderService {

    List<AbstractVehicleRequestPreReviewDTO> findAll();

    void setToFindBunch(Long id_bunch);

    AbstractVehicleBunchDTO getBunch();

    List<AbstractCaseFileDTO> getAllCaseFiles();

    List<AbstractVehicleDTO> findAllVehicles();

    AbstractNaturalEntityDTO getNaturalOwner();

    AbstractLegalEntityDTO getLegalOwner();

    List<AbstractClientDTO> getClientList();

    void updateAllValidateStatus(Long id_bunch);
}
