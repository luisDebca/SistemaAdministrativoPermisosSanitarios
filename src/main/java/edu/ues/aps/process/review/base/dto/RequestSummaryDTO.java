package edu.ues.aps.process.review.base.dto;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

import static java.time.temporal.ChronoUnit.DAYS;
import static java.time.temporal.ChronoUnit.MONTHS;

public class RequestSummaryDTO implements AbstractRequestSummaryDTO {

    private LocalDateTime today = LocalDateTime.now();

    private Long id_caseFile;
    private String folder_number;
    private String request_number;
    private CertificationType certification_type;
    private CasefileSection section;
    private LocalDateTime start_date;

    public RequestSummaryDTO(Long id_caseFile, String folder_number, String request_number, CertificationType certification_type, CasefileSection section, LocalDateTime start_date) {
        this.id_caseFile = id_caseFile;
        this.folder_number = folder_number;
        this.request_number = request_number;
        this.certification_type = certification_type;
        this.section = section;
        this.start_date = start_date;
    }

    public Long getId_caseFile() {
        return id_caseFile;
    }

    public String getFolder_number() {
        return folder_number;
    }

    public String getRequest_number() {
        return request_number;
    }

    public String getCertification_type() {
        if(certification_type != null){
            return certification_type.getCertificationType();
        }
        return CertificationType.INVALID.getCertificationType();
    }

    public String getSection() {
        if(section != null){
            return section.getCasefileSection();
        }
        return CasefileSection.INVALID.getCasefileSection();
    }

    public String getStart_date() {
        if(start_date != null){
            return DatetimeUtil.parseDateToLongDateFormat(start_date.toLocalDate());
        }
        return DatetimeUtil.DATE_NOT_DEFINED;
    }

    @Override
    public Long days_since_start() {
        if(start_date != null){
            return DAYS.between(today,start_date);
        }
        return 0L;
    }

    @Override
    public Long month_since_start() {
        if(start_date != null){
            return MONTHS.between(today,start_date);
        }
        return 0L;
    }
}
