package edu.ues.aps.process.review.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.AbstractVehicleRequestDTO;

public interface AbstractVehicleRequestPreReviewDTO extends AbstractVehicleRequestDTO {

    Boolean getRequest_valid();

}
