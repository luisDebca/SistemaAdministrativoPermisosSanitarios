package edu.ues.aps.process.review.establishment.repository;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import edu.ues.aps.process.review.establishment.dto.AbstractEstablishmentRequestPreReviewDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class EstablishmentRequestPreReviewRepository implements EstablishmentRequestPreReviewFinderRepository{

    private final SessionFactory sessionFactory;

    public EstablishmentRequestPreReviewRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractEstablishmentRequestPreReviewDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.process.review.establishment.dto.EstablishmentRequestPreReviewDTO(ce.id ,owner.id,establishment.id,concat(concat(ce.request_folder,'-'),ce.request_year),concat(concat(concat(concat('06',ce.request_code),ce.request_number),'-'),ce.request_year)," +
                "establishment.name,establishment.type_detail,etype.type,section.type,concat(concat(concat(concat(address.details,', '),address.municipality.name),', '),address.municipality.department.name)," +
                "establishment.NIT,ucsf.name,sibasi.zone,ce.certification_type,ce.creation_date,owner.name,ce.validate)" +
                "from CasefileEstablishment ce " +
                "inner join ce.establishment establishment " +
                "left join establishment.type etype " +
                "left join etype.section section " +
                "left join establishment.address address " +
                "inner join establishment.owner owner " +
                "left join ce.ucsf ucsf " +
                "left join ucsf.sibasi sibasi " +
                "inner join ce.processes record " +
                "inner join record.process process " +
                "where record.active = true " +
                "and process.identifier = :id", AbstractEstablishmentRequestPreReviewDTO.class)
                .setParameter("id", ProcessIdentifier.DOCUMENTATION_REVIEW)
                .getResultList();
    }
}
