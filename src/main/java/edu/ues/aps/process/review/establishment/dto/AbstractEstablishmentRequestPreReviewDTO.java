package edu.ues.aps.process.review.establishment.dto;

import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentRequestDTO;

public interface AbstractEstablishmentRequestPreReviewDTO extends AbstractEstablishmentRequestDTO {

    Boolean getValidated();
}
