package edu.ues.aps.process.review.base.controller;

import edu.ues.aps.process.record.base.model.ProcessIdentifier;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class RequestReviewIndexController {

    private static final Logger logger = LogManager.getLogger(RequestReviewIndexController.class);

    private static final String DATA_COMPLETION_INDEX = "request_review_index";

    @GetMapping({"/case-file/request/review/", "/case-file/request/review/index"})
    public String showCaseFileDataCompletionIndex(ModelMap model){
        logger.info("GET:: Show caseFile data completion index");
        model.addAttribute("process", ProcessIdentifier.DOCUMENTATION_REVIEW);
        return DATA_COMPLETION_INDEX;
    }

}
