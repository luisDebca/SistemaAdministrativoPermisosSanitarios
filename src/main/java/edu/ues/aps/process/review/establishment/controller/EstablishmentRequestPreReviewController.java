package edu.ues.aps.process.review.establishment.controller;

import edu.ues.aps.process.base.service.CaseFileRequiredReviewValidatorService;
import edu.ues.aps.process.review.establishment.service.EstablishmentRequestPreReviewFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/case-file/establishment/request")
public class EstablishmentRequestPreReviewController {

    private static final Logger logger = LogManager.getLogger(EstablishmentRequestPreReviewController.class);

    private static final String ESTABLISHMENT_REQUEST_REVIEW_LIST = "establishment_request_review_list";
    private static final String ESTABLISHMENT_REQUEST_REVIEW = "establishment_request_review";

    private final EstablishmentRequestPreReviewFinderService finderService;
    private final CaseFileRequiredReviewValidatorService reviewValidatorService;

    public EstablishmentRequestPreReviewController(CaseFileRequiredReviewValidatorService reviewValidatorService, EstablishmentRequestPreReviewFinderService finderService) {
        this.reviewValidatorService = reviewValidatorService;
        this.finderService = finderService;
    }

    @GetMapping("/review/list")
    public String showEstablishmentRequestReviewProcessList(ModelMap model) {
        logger.info("GET:: Show all establishment request for data review");
        model.addAttribute("requests", finderService.findAll());
        return ESTABLISHMENT_REQUEST_REVIEW_LIST;
    }

    @GetMapping("/{id_caseFile}/review")
    public String showRequiredFiledReview(@PathVariable Long id_caseFile, ModelMap model) {
        logger.info("GET:: Show required field review");
        finderService.setReviewCaseFile(id_caseFile);
        model.addAttribute("caseFile", finderService.getCaseFile());
        model.addAttribute("establishment", finderService.getEstablishment());
        model.addAttribute("natural", finderService.getNaturalEntity());
        model.addAttribute("legal", finderService.getLegalEntity());
        model.addAttribute("clients", finderService.findClients());
        return ESTABLISHMENT_REQUEST_REVIEW;
    }

    @GetMapping("/{id_caseFile}/validated")
    public String updateCaseFileAsValidated(@PathVariable Long id_caseFile) {
        logger.info("GET:: Update case file {} validate status to true.", id_caseFile);
        reviewValidatorService.validateCaseFile(id_caseFile);
        return "redirect:/case-file/establishment/request/review/list";
    }

}
