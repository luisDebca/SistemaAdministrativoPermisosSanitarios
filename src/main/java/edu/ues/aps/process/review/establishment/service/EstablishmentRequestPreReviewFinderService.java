package edu.ues.aps.process.review.establishment.service;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO;
import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.AbstractLegalEntityDTO;
import edu.ues.aps.process.base.dto.AbstractNaturalEntityDTO;
import edu.ues.aps.process.review.establishment.dto.AbstractEstablishmentRequestPreReviewDTO;

import java.util.List;

public interface EstablishmentRequestPreReviewFinderService {

    List<AbstractEstablishmentRequestPreReviewDTO> findAll();

    void setReviewCaseFile(Long id_caseFile);

    AbstractEstablishmentDTO getEstablishment();

    AbstractCaseFileDTO getCaseFile();

    AbstractLegalEntityDTO getLegalEntity();

    AbstractNaturalEntityDTO getNaturalEntity();

    List<AbstractClientDTO> findClients();
}
