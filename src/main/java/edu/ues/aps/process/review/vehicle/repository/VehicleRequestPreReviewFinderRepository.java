package edu.ues.aps.process.review.vehicle.repository;

import edu.ues.aps.process.review.vehicle.dto.AbstractVehicleRequestPreReviewDTO;

import java.util.List;

public interface VehicleRequestPreReviewFinderRepository {

    List<AbstractVehicleRequestPreReviewDTO> findAll();
}
