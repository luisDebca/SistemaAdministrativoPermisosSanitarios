package edu.ues.aps.process.review.establishment.service;

import edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO;
import edu.ues.aps.process.area.base.service.CaseFileDTOFinderService;
import edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO;
import edu.ues.aps.process.area.establishment.service.EstablishmentDTOFinderService;
import edu.ues.aps.process.base.dto.AbstractClientDTO;
import edu.ues.aps.process.base.dto.AbstractLegalEntityDTO;
import edu.ues.aps.process.base.dto.AbstractNaturalEntityDTO;
import edu.ues.aps.process.base.service.ClientDTOFinderService;
import edu.ues.aps.process.base.service.OwnerDTOFinderService;
import edu.ues.aps.process.review.establishment.dto.AbstractEstablishmentRequestPreReviewDTO;
import edu.ues.aps.process.review.establishment.repository.EstablishmentRequestPreReviewFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Transactional(readOnly = true)
public class EstablishmentRequestPreReviewService implements EstablishmentRequestPreReviewFinderService{

    private AbstractCaseFileDTO caseFile;
    private AbstractEstablishmentDTO establishment;

    private final OwnerDTOFinderService ownerFinderService;
    private final ClientDTOFinderService clientFinderService;
    private final CaseFileDTOFinderService caseFileFinderService;
    private final EstablishmentDTOFinderService establishmentFinderService;
    private final EstablishmentRequestPreReviewFinderRepository finderRepository;

    public EstablishmentRequestPreReviewService(EstablishmentRequestPreReviewFinderRepository finderRepository, CaseFileDTOFinderService caseFileFinderService, EstablishmentDTOFinderService establishmentFinderService, OwnerDTOFinderService ownerFinderService, ClientDTOFinderService clientFinderService) {
        this.finderRepository = finderRepository;
        this.caseFileFinderService = caseFileFinderService;
        this.establishmentFinderService = establishmentFinderService;
        this.ownerFinderService = ownerFinderService;
        this.clientFinderService = clientFinderService;
    }

    @Override
    public List<AbstractEstablishmentRequestPreReviewDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public void setReviewCaseFile(Long id_caseFile) {
        caseFile = caseFileFinderService.find(id_caseFile);
        establishment = establishmentFinderService.findByCaseFile(caseFile.getId_casefile());
    }

    @Override
    public AbstractEstablishmentDTO getEstablishment() {
        return establishment;
    }

    @Override
    public AbstractCaseFileDTO getCaseFile() {
        return caseFile;
    }

    @Override
    public AbstractLegalEntityDTO getLegalEntity() {
        return ownerFinderService.findLegalEntity(establishment.getOwner_id());
    }

    @Override
    public AbstractNaturalEntityDTO getNaturalEntity() {
        return ownerFinderService.findNaturalEntity(establishment.getOwner_id());
    }

    @Override
    public List<AbstractClientDTO> findClients() {
        return clientFinderService.findAll(establishment.getOwner_id());
    }
}
