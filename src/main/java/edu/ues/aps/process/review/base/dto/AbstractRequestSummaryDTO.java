package edu.ues.aps.process.review.base.dto;

public interface AbstractRequestSummaryDTO {

    Long getId_caseFile();

    String getFolder_number();

    String getRequest_number();

    String getCertification_type();

    String getSection();

    String getStart_date();

    Long days_since_start();

    Long month_since_start();

}
