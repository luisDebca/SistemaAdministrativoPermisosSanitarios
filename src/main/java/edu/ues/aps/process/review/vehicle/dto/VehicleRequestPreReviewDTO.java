package edu.ues.aps.process.review.vehicle.dto;

import edu.ues.aps.process.area.vehicle.dto.VehicleRequestDTO;

import java.time.LocalDateTime;

public class VehicleRequestPreReviewDTO extends VehicleRequestDTO implements AbstractVehicleRequestPreReviewDTO{

    private Boolean request_valid;

    public VehicleRequestPreReviewDTO(Long id_bunch, Long id_owner, String owner_name, String bunch_presented_name, String bunch_presented_phone, String bunch_distribution_company, String bunch_distribution_destination, String bunch_marketing_place, String bunch_address, Long request_count, String request_folder, LocalDateTime request_create_on, Boolean request_valid) {
        super(id_bunch, id_owner, owner_name, bunch_presented_name, bunch_presented_phone, bunch_distribution_company, bunch_distribution_destination, bunch_marketing_place, bunch_address, request_count, request_folder, request_create_on);
        this.request_valid = request_valid;
    }

    public Boolean getRequest_valid() {
        return request_valid;
    }
}
