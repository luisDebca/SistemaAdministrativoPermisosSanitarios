package edu.ues.aps.system.db.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/db")
public class DataBaseInformationProviderController {

    private static final Logger logger = LogManager.getLogger(DataBaseInformationProviderController.class);

    private static final String INDEX = "db-index";

    @GetMapping("/index")
    public String showIndex(){
        logger.info("GET:: Show bd index.");
        return INDEX;
    }
}
