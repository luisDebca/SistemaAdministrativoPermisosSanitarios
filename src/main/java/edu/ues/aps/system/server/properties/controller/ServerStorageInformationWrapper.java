package edu.ues.aps.system.server.properties.controller;

import java.io.File;

public class ServerStorageInformationWrapper {

    private File storage_information;

    public ServerStorageInformationWrapper(File storage_information) {
        this.storage_information = storage_information;
    }

    public Long getTotal_space_bytes() {
        return storage_information.getTotalSpace();
    }

    public Long getUsable_space_bytes() {
        return storage_information.getUsableSpace();
    }

    public Long getFree_space_bytes() {
        return storage_information.getFreeSpace();
    }

    public Long getTotal_space_megabytes() {
        return storage_information.getTotalSpace()/1024/1024;
    }

    public Long getUsable_space_megabytes() {
        return storage_information.getUsableSpace()/1024/1024;
    }

    public Long getFree_space_megabytes() {
        return storage_information.getFreeSpace()/1024/1024;
    }
}
