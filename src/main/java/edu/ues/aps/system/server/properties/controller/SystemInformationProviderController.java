package edu.ues.aps.system.server.properties.controller;

import edu.ues.aps.utilities.file.directory.ApplicationDefaultDirectoryInformationProvider;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Enumeration;
import java.util.Properties;

@Controller
@RequestMapping("/system")
public class SystemInformationProviderController {

    private static final Logger logger = LogManager.getLogger(SystemInformationProviderController.class);

    private static final String SERVER_INFORMATION = "system_information";

    private final ApplicationDefaultDirectoryInformationProvider directoryInformationProvider;

    @Autowired
    public SystemInformationProviderController(ApplicationDefaultDirectoryInformationProvider directoryInformationProvider) {
        this.directoryInformationProvider = directoryInformationProvider;
    }

    @GetMapping("/info")
    public String showServerInformation(ModelMap model){
        logger.info("GET:: Show server information.");
        ServerStorageInformationWrapper storage = new ServerStorageInformationWrapper(directoryInformationProvider.getSources_directory());
        Properties properties = System.getProperties();
        Enumeration keys = properties.keys();
        model.addAttribute(properties);
        model.addAttribute(keys);
        model.addAttribute("storage",storage);
        return SERVER_INFORMATION;
    }

}
