package edu.ues.aps.system.server.properties.async;

import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/system")
public class SystemMonitorInformationAsyncController {

    private static final Runtime runtime = Runtime.getRuntime();

    @RequestMapping(method = RequestMethod.GET, value = "/free-memory", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getSystemFreeMemory(){
        return runtime.freeMemory();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/total-memory", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getSystemTotalMemory(){
        return runtime.totalMemory();
    }

    @RequestMapping(method = RequestMethod.GET, value = "/max-memory", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public Long getMaxMemory(){
        return runtime.maxMemory();
    }

}
