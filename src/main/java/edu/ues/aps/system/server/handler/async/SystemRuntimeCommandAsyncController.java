package edu.ues.aps.system.server.handler.async;

import edu.ues.aps.utilities.http.response.SingleStringResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.io.IOException;

@Controller
public class SystemRuntimeCommandAsyncController {

    private static final Logger logger = LogManager.getLogger(SystemRuntimeCommandAsyncController.class);
    private static final String APS_VERY_SIMPLE_PASSWORD = "$2a$12$xg2g3dfR/UaMg3S8oN8E8u11GR92zGW91xrjBLN45a1sqSt.of3nS";
    private PasswordEncoder passwordEncoder;

    public SystemRuntimeCommandAsyncController(PasswordEncoder passwordEncoder) {
        this.passwordEncoder = passwordEncoder;
    }

    @PostMapping(value = "/system-shutdown", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public SingleStringResponse shutdownServer(@ModelAttribute String password) {
        if (passwordEncoder.matches(password, APS_VERY_SIMPLE_PASSWORD)) {
            logger.info("Trying shutdown server...");
            shutdownSystem();
            return new SingleStringResponse("OK").asSuccessResponse();
        } else {
            return new SingleStringResponse("The password is not matches").asErrorResponse();
        }
    }

    private void shutdownSystem() {
        try {
            Thread.sleep(10000);
            logger.info("Shutdown the system...");
            Runtime.getRuntime().exec("systemctl poweroff");
        } catch (InterruptedException | IOException e) {
            logger.error("Error trying shutdown server, {} caused by {}", e.getMessage(), e.getCause().toString());
        }
    }
}
