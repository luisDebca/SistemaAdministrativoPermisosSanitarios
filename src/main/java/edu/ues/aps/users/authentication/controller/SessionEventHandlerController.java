package edu.ues.aps.users.authentication.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class SessionEventHandlerController {

    private static final String SESSION_EXPIRED = "session_expired";
    private static final String INVALID_SESSION = "invalid_session";


    @GetMapping("/sessionExpired")
    public String onSessionExpiredExceptionHandler(){
        return SESSION_EXPIRED;
    }

    @GetMapping("/invalidSession")
    public String onInvalidSessionExceptionHandler(){
        return INVALID_SESSION;
    }


}
