package edu.ues.aps.users.authentication.controller;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.authentication.AuthenticationTrustResolver;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.web.authentication.rememberme.PersistentTokenBasedRememberMeServices;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.security.Principal;

@Controller
public class UserAuthenticationController {

    private static final Logger logger = LogManager.getLogger(UserAuthenticationController.class);

    private static final String HOME_VIEW_REDIRECT = "redirect:/home";
    private static final String LOGIN_VIEW = "login";
    private static final String LOGOUT_REDIRECT_PATH = "redirect:/login?logout";
    private static final String ACCESS_DENIED_PATH = "accessDenied";

    private PersistentTokenBasedRememberMeServices rememberMeServices;
    private AuthenticationTrustResolver authenticationTrustResolver;

    public UserAuthenticationController(PersistentTokenBasedRememberMeServices rememberMeServices, AuthenticationTrustResolver authenticationTrustResolver) {
        this.rememberMeServices = rememberMeServices;
        this.authenticationTrustResolver = authenticationTrustResolver;
    }

    @GetMapping("/login")
    public String showLoginPage() {
        if (isCurrentAuthenticationAnonymous()) {
            return LOGIN_VIEW;
        } else {
            logger.info("GET:: Show index");
            return HOME_VIEW_REDIRECT;
        }
    }

    @GetMapping("/logout")
    public String logoutPage(HttpServletRequest request, HttpServletResponse response) {
        Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        if (authentication != null) {
            logger.info("GET:: User {} logout from APS",authentication.getPrincipal());
            rememberMeServices.logout(request, response, authentication);
            SecurityContextHolder.getContext().setAuthentication(null);
        }
        return LOGOUT_REDIRECT_PATH;
    }

    @GetMapping("/Access_Denied")
    public String showAccessDeniedPage(ModelMap model, Principal principal) {
        logger.warn("GET:: User {} access denied",principal.getName());
        model.addAttribute("loggedinuser", principal.getName());
        return ACCESS_DENIED_PATH;
    }

    private boolean isCurrentAuthenticationAnonymous() {
        final Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
        return authenticationTrustResolver.isAnonymous(authentication);
    }
}
