package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.dto.AbstractProfileDTO;

import java.util.List;

public interface UserProfileRelationsRepository {

    List<AbstractProfileDTO> findAllProfiles(Long id_user);
}
