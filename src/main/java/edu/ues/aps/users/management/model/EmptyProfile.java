package edu.ues.aps.users.management.model;


public class EmptyProfile implements AbstractProfile {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public UserProfileType getType() {
        return UserProfileType.INVALID;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setType(UserProfileType type) {

    }

    @Override
    public String toString() {
        return "EmptyProfile{}";
    }

}
