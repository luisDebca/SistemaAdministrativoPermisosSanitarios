package edu.ues.aps.users.management.model;

public interface AbstractPersonal {

    Long getId();

    void setId(Long id);

    String getFirst_name();

    void setFirst_name(String first_name);

    String getLast_name();

    void setLast_name(String last_name);

    String getCharge();

    void setCharge(String charge);

    String getEmail();

    void setEmail(String email);

    String getDesignation();

    void setDesignation(String designation);

    AbstractUser getUser();

    void setUser(User user);

    void addUser(User user);

    void removeUser();

    String getFull_name();

}
