package edu.ues.aps.users.management.repository;

public interface UserValidatorRepository {

    Long isUsernameUnique(String username);

    Long isUsernameUnique(Long id_user, String username);
}
