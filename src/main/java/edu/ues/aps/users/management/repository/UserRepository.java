package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.model.User;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class UserRepository implements UserCrudRepository, CustomUserRepository, UserValidatorRepository {

    private SessionFactory sessionFactory;

    public UserRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public User findByUsername(String username) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select user from User user " +
                "where user.username = :username", User.class)
                .setParameter("username", username)
                .getSingleResult();
    }

    @Override
    public void updateLastLoggingToCurrentDate(Long id_user) {
        sessionFactory.getCurrentSession().createQuery("update User user " +
                "set user.last_login = current_timestamp " +
                "where user.id = :id")
                .setParameter("id", id_user)
                .executeUpdate();
    }

    @Override
    public void deleteUser(String username) throws NoResultException {
        User user = sessionFactory.getCurrentSession().createQuery("select user " +
                "from User user " +
                "left join user.process process " +
                "where user.username = :username", User.class)
                .setParameter("username", username)
                .getSingleResult();
        user.removeProcess();
        sessionFactory.getCurrentSession().delete(user);
    }

    @Override
    public Long isUsernameUnique(String username) {
        return sessionFactory.getCurrentSession().createQuery("select count(user) " +
                "from User user " +
                "where user.username = :name", Long.class)
                .setParameter("name", username)
                .getSingleResult();

    }

    @Override
    public Long isUsernameUnique(Long id_user, String username) {
        return sessionFactory.getCurrentSession().createQuery("select count(user) " +
                "from User user " +
                "where user.username = :name " +
                "and not user.id = :id", Long.class)
                .setParameter("name", username)
                .setParameter("id", id_user)
                .getSingleResult();
    }
}
