package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.model.AbstractProfile;

import java.util.Map;

public interface ProfileCrudService {

    AbstractProfile findById(Long id_profile);

    Map<Long, String> findAll();
}
