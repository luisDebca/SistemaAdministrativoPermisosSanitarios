package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.dto.AbstractPersonalUserDTO;
import edu.ues.aps.users.management.model.UserProfileType;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class PersonalUserDTORepository implements PersonalUserDTOFinderRepository {

    private SessionFactory sessionFactory;

    public PersonalUserDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public List<AbstractPersonalUserDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.users.management.dto.PersonalUserDTO(personal.id,personal.first_name,personal.last_name,personal.charge," +
                "personal.email,personal.designation,user.username,user.joining_date,user.last_login,user.is_active) " +
                "from Personal personal " +
                "inner join personal.user user", AbstractPersonalUserDTO.class)
                .getResultList();
    }

    @Override
    public AbstractPersonalUserDTO findById(Long id_personal) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.users.management.dto.PersonalUserDTO(personal.id,personal.first_name,personal.last_name,personal.charge," +
                "personal.email,personal.designation,user.username,user.joining_date,user.last_login,user.is_active) " +
                "from Personal personal " +
                "inner join personal.user user " +
                "where personal.id = :id", AbstractPersonalUserDTO.class)
                .setParameter("id", id_personal)
                .getSingleResult();
    }

    @Override
    public List<String> findAllPersonalFullName() {
        return sessionFactory.getCurrentSession().createQuery("select case when personal.designation is null " +
                "then concat(concat(personal.first_name,' '),personal.last_name) " +
                "else concat(concat(concat(concat(personal.designation,' '),personal.first_name),' '),personal.last_name) end " +
                "from Personal personal " +
                "inner join personal.user user " +
                "where user.is_active = true ",String.class)
                .getResultList();
    }

    @Override
    public List<String> findAllPersonalFullNameAndCharge() {
        return sessionFactory.getCurrentSession().createQuery("select case when personal.designation is null " +
                "then concat(concat(concat(concat(personal.first_name,' '),personal.last_name),' - '),personal.charge) " +
                "else concat(concat(concat(concat(concat(concat(personal.designation,' '),personal.first_name),' '),personal.last_name),' - '),personal.charge) end " +
                "from Personal personal " +
                "inner join personal.user user " +
                "where user.is_active = true ",String.class)
                .getResultList();
    }

    @Override
    public String getPersonalFullName(String username) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select case when personal.designation is null " +
                "then concat(concat(personal.first_name,' '),personal.last_name) " +
                "else concat(concat(concat(concat(personal.designation,' '),personal.first_name),' '),personal.last_name) end " +
                "from Personal personal " +
                "inner join personal.user user " +
                "where user.username = :username",String.class)
                .setParameter("username",username)
                .getSingleResult();
    }

    @Override
    public String getPersonalCharge(String username) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select personal.charge " +
                "from Personal personal " +
                "inner join personal.user user " +
                "where user.username = :username",String.class)
                .setParameter("username",username)
                .getSingleResult();
    }
}
