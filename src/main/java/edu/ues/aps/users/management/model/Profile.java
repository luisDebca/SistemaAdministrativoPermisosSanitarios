package edu.ues.aps.users.management.model;

import javax.persistence.*;

@Entity
@Table(name = "PROFILE")
public class Profile implements AbstractProfile {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Enumerated(EnumType.STRING)
    @Column(name = "type", nullable = false, unique = true, length = 50)
    private UserProfileType type;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public UserProfileType getType() {
        return type;
    }

    @Override
    public void setType(UserProfileType type) {
        this.type = type;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Profile profile = (Profile) o;

        return type == profile.type;
    }

    @Override
    public int hashCode() {
        return type.hashCode();
    }

    @Override
    public String toString() {
        return "Profile{" +
                "id=" + id +
                ", type=" + type +
                '}';
    }
}
