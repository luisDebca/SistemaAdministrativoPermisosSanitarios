package edu.ues.aps.users.management.model;

import edu.ues.aps.storage.model.File;

import java.time.LocalDateTime;
import java.util.List;

public interface AbstractMailRegister {

    Long getId();

    void setId(Long id);

    AbstractUser getFrom();

    void setFrom(User from);

    String getTo();

    void setTo(String to);

    String getCc();

    void setCc(String cc);

    String getBcc();

    void setBcc(String bcc);

    String getSubject();

    void setSubject(String subject);

    String getText();

    void setText(String text);

    List<File> getAttached_files();

    void setAttached_files(List<File> attached_files);

    LocalDateTime getShipping_date();

    void setShipping_date(LocalDateTime shipping_date);

}
