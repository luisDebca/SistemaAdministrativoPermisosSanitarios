package edu.ues.aps.users.management.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class PersonalUserDTO implements AbstractPersonalUserDTO {

    private Long id_personal;
    private String personal_first_name;
    private String personal_last_name;
    private String personal_charge;
    private String personal_email;
    private String personal_designation;
    private String user_username;
    private LocalDateTime user_joining_date;
    private LocalDateTime user_last_login;
    private Boolean user_is_active;

    public PersonalUserDTO(Long id_personal, String personal_first_name, String personal_last_name, String personal_charge, String personal_email, String personal_designation, String user_username, LocalDateTime user_joining_date, LocalDateTime user_last_login, Boolean user_is_active) {
        this.id_personal = id_personal;
        this.personal_first_name = personal_first_name;
        this.personal_last_name = personal_last_name;
        this.personal_charge = personal_charge;
        this.personal_email = personal_email;
        this.personal_designation = personal_designation;
        this.user_username = user_username;
        this.user_joining_date = user_joining_date;
        this.user_last_login = user_last_login;
        this.user_is_active = user_is_active;
    }

    public Long getId_personal() {
        return id_personal;
    }

    public String getPersonal_first_name() {
        return personal_first_name;
    }

    public String getPersonal_last_name() {
        return personal_last_name;
    }

    public String getPersonal_charge() {
        return personal_charge;
    }

    public String getPersonal_email() {
        return personal_email;
    }

    public String getPersonal_designation() {
        return personal_designation;
    }

    public String getUser_username() {
        return user_username;
    }

    public LocalDateTime getUser_joining_date() {
        return user_joining_date;
    }

    public LocalDateTime getUser_last_login() {
        return user_last_login;
    }

    public Boolean getUser_is_active() {
        return user_is_active;
    }

    public String getUser_joining_date_formatted() {
        return DatetimeUtil.parseDatetimeToShortFormat(user_joining_date);
    }

    public String getUser_last_login_formatted() {
        return DatetimeUtil.parseDatetimeToShortFormat(user_last_login);
    }

}
