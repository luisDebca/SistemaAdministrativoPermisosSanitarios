package edu.ues.aps.users.management.model;

public class EmptyPersonal implements AbstractPersonal {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getFirst_name() {
        return "Personal first name not found";
    }

    @Override
    public String getLast_name() {
        return "Personal last name not found";
    }

    @Override
    public String getCharge() {
        return "Personal charge not found";
    }

    @Override
    public String getEmail() {
        return "Personal email not found";
    }

    @Override
    public String getDesignation() {
        return "Personal designation not found";
    }

    @Override
    public AbstractUser getUser() {
        return new EmptyUser();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setFirst_name(String first_name) {

    }

    @Override
    public void setLast_name(String last_name) {

    }

    @Override
    public void setCharge(String charge) {

    }

    @Override
    public void setEmail(String email) {

    }

    @Override
    public void setDesignation(String designation) {

    }

    @Override
    public void setUser(User user) {

    }

    @Override
    public void addUser(User user) {

    }

    @Override
    public void removeUser() {

    }

    @Override
    public String getFull_name() {
        return "Personal full name not found";
    }

    @Override
    public String toString() {
        return "EmptyPersonal{}";
    }
}
