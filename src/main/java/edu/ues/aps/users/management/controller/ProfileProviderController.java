package edu.ues.aps.users.management.controller;

import edu.ues.aps.users.management.service.ProfileCrudService;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import java.util.Map;

@ControllerAdvice
@RequestMapping("/user")
public class ProfileProviderController implements InitializingBean {

    private ProfileCrudService profileCrudService;

    private Map<Long, String> profiles;

    public ProfileProviderController(ProfileCrudService profileCrudService) {
        this.profileCrudService = profileCrudService;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        profiles = profileCrudService.findAll();
    }

    @ModelAttribute
    public void initializeProfileList(Model model) {
        model.addAttribute("profiles", profiles);
    }
}
