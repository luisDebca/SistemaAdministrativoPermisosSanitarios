package edu.ues.aps.users.management.model;

import edu.ues.aps.process.record.base.model.ProcessUser;

import java.time.LocalDateTime;
import java.util.List;

public interface AbstractUser {

    Long getId();

    void setId(Long id);

    String getUsername();

    void setUsername(String username);

    String getPassword();

    void setPassword(String password);

    Boolean getIs_active();

    void setIs_active(Boolean is_active);

    LocalDateTime getJoining_date();

    void setJoining_date(LocalDateTime joining_date);

    LocalDateTime getLast_login();

    void setLast_login(LocalDateTime last_login);

    List<Profile> getProfiles();

    void setProfiles(List<Profile> profiles);

    AbstractPersonal getPersonal();

    void setPersonal(Personal personal);

    List<ProcessUser> getProcess();

    void setProcess(List<ProcessUser> process);

    void removeProcess();

    List<MailRegister> getMails();

    void setMails(List<MailRegister> mails);

    void addMailRegister(MailRegister register);

}
