package edu.ues.aps.users.management.controller;

import edu.ues.aps.users.management.model.Personal;
import edu.ues.aps.users.management.service.PersonalUserFinderService;
import edu.ues.aps.users.management.service.PersonalUserPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/user")
public class PersonalUserPersistenceController {

    private static final Logger logger = LogManager.getLogger(PersonalUserPersistenceController.class);

    private static final String PERSONAL_EDITION_VIEW = "user_edition";
    private static final String USER_FROM_VIEW = "user_registration";

    private PersonalUserPersistenceService persistenceService;
    private PersonalUserFinderService finderService;

    public PersonalUserPersistenceController(PersonalUserPersistenceService persistenceService, PersonalUserFinderService finderService) {
        this.persistenceService = persistenceService;
        this.finderService = finderService;
    }

    @GetMapping("/new")
    public String showUserRegistrationForm(ModelMap model) {
        logger.info("GET:: Show user registration form.");
        model.addAttribute("personal", new Personal());
        return USER_FROM_VIEW;
    }

    @PostMapping("/new")
    public String saveNewUser(@ModelAttribute Personal personal) {
        logger.info("POST:: Save new user with personal.");
        persistenceService.save(personal);
        return "redirect:/user";
    }

    @GetMapping("/{id_personal}/edit")
    public String showPersonalEditionForm(@PathVariable Long id_personal, ModelMap model) {
        logger.info("GET:: Show personal edit by identifier {}.", id_personal);
        model.addAttribute("personal", finderService.findById(id_personal));
        return PERSONAL_EDITION_VIEW;
    }

    @PostMapping("/{id_personal}/edit")
    public String updatePersonalEntity(@PathVariable Long id_personal, @ModelAttribute Personal personal) {
        logger.info("POST:: Update personal {}.", id_personal);
        persistenceService.update(personal);
        return "redirect:/user";
    }

    @GetMapping("/{username}/delete")
    public String deleteUser(@PathVariable String username) {
        logger.info("GET:: Delete user by username: {}", username);
        persistenceService.deletePersonalUser(username);
        return "redirect:/user";
    }
}
