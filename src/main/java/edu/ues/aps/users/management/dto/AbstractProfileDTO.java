package edu.ues.aps.users.management.dto;

import edu.ues.aps.users.management.model.UserProfileType;

public interface AbstractProfileDTO {

    Long getId_profile();

    UserProfileType getProfile_type();

}
