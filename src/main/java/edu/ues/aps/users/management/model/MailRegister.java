package edu.ues.aps.users.management.model;

import edu.ues.aps.storage.model.File;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Entity
@Table(name = "MAIL_REGISTER")
public class MailRegister implements AbstractMailRegister{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "addressee",length = 500, nullable = false)
    private String to;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "FK_user")
    private User from;

    @Column(length = 500)
    private String cc;

    @Column(length = 500)
    private String bcc;

    @Column(nullable = false, length = 100)
    private String subject;

    @Column(nullable = false, length = 5000)
    private String text;

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(name = "ATTACHED_MAIL_FILES", joinColumns = {@JoinColumn(name = "file_id")}, inverseJoinColumns = {@JoinColumn(name = "mail_id")})
    private List<File> attached_files = new ArrayList<>();

    @Column(nullable = false)
    private LocalDateTime shipping_date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public User getFrom() {
        return from;
    }

    public void setFrom(User from) {
        this.from = from;
    }

    public String getTo() {
        return to;
    }

    public void setTo(String to) {
        this.to = to;
    }

    public String getCc() {
        return cc;
    }

    public void setCc(String cc) {
        this.cc = cc;
    }

    public String getBcc() {
        return bcc;
    }

    public void setBcc(String bcc) {
        this.bcc = bcc;
    }

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<File> getAttached_files() {
        return attached_files;
    }

    public void setAttached_files(List<File> attached_files) {
        this.attached_files = attached_files;
    }

    public LocalDateTime getShipping_date() {
        return shipping_date;
    }

    public void setShipping_date(LocalDateTime shipping_date) {
        this.shipping_date = shipping_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        MailRegister that = (MailRegister) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(subject, that.subject);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, subject);
    }

    @Override
    public String toString() {
        return "MailRegister{" +
                "id=" + id +
                ", to='" + to + '\'' +
                ", cc='" + cc + '\'' +
                ", bcc='" + bcc + '\'' +
                ", subject='" + subject + '\'' +
                ", text='" + text + '\'' +
                ", shipping_date=" + shipping_date +
                '}';
    }
}
