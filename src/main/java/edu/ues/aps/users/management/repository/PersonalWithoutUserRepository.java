package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.model.Personal;

import java.util.List;

public interface PersonalWithoutUserRepository {

    List<Personal> findAll();
}
