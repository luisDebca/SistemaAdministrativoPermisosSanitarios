package edu.ues.aps.users.management.model;

public enum UserProfileType {
    USER("Usuario"),
    USER_ADMIN("Administrador de usuarios"),
    BD_ADMIN("Administrador de base de datos"),
    APP_ADMIN("Administrador de aplicaci�n"),
    COORDINATOR("Cordinador"),
    JURIDICAL("Jur�dico"),
    ESTABLISHMENT_TECHNICAL("T�cnico de establecimientos"),
    VEHICLE_TECHNICAL("T�cnico de veh�culos"),
    TOBACCO_TECHNICAL("T�cnico de tabaco"),
    VISITOR("Visitante"),
    CENSUS("Censos"),
    INVALID("Usuario invalido");

    private String userProfileType;

    UserProfileType(String type) {
        this.userProfileType = type;
    }

    public String getUserProfileType() {
        return userProfileType;
    }
}
