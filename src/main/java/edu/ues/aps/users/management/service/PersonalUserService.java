package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.model.AbstractPersonal;
import edu.ues.aps.users.management.model.EmptyPersonal;
import edu.ues.aps.users.management.model.User;
import edu.ues.aps.users.management.repository.PersonalCrudRepository;
import edu.ues.aps.users.management.repository.UserCrudRepository;
import edu.ues.aps.users.management.repository.UserValidatorRepository;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;

@Service
@Transactional
public class PersonalUserService implements PersonalUserPersistenceService, PersonalUserFinderService, UserProcessFinderService, SingleUserFinderService, UserValidatorService {

    private final UserValidatorRepository validatorRepository;
    private PersonalCrudRepository personalCrudRepository;
    private UserCrudRepository userCrudRepository;
    private PasswordEncoder passwordEncoder;

    public PersonalUserService(PersonalCrudRepository personalCrudRepository, UserCrudRepository userCrudRepository, PasswordEncoder passwordEncoder, UserValidatorRepository validatorRepository) {
        this.personalCrudRepository = personalCrudRepository;
        this.userCrudRepository = userCrudRepository;
        this.passwordEncoder = passwordEncoder;
        this.validatorRepository = validatorRepository;
    }

    @Override
    public void save(AbstractPersonal personal) {
        User user = new User();
        user.setUsername(personal.getUser().getUsername());
        user.setJoining_date(LocalDateTime.now());
        user.setLast_login(LocalDateTime.now());
        user.setPassword(passwordEncoder.encode(personal.getUser().getPassword()));
        user.setProfiles(personal.getUser().getProfiles());
        user.setIs_active(personal.getUser().getIs_active());
        personal.addUser(user);
        personalCrudRepository.save(personal);
    }

    @Override
    public Boolean update(AbstractPersonal personal) {
        boolean userNameUpdated = false;
        AbstractPersonal persistent = personalCrudRepository.findById(personal.getId());
        persistent.setFirst_name(personal.getFirst_name());
        persistent.setLast_name(personal.getLast_name());
        persistent.setCharge(personal.getCharge());
        persistent.setDesignation(personal.getDesignation());
        persistent.setEmail(personal.getEmail());
        persistent.getUser().setIs_active(personal.getUser().getIs_active());
        if(!persistent.getUser().getUsername().equals(personal.getUser().getUsername())){
            userNameUpdated = true;
        }
        persistent.getUser().setUsername(personal.getUser().getUsername());
        if (!personal.getUser().getProfiles().isEmpty()) {
            persistent.getUser().setProfiles(personal.getUser().getProfiles());
        }
        return userNameUpdated;
    }

    @Override
    public void updatePassword(String username, String pass, String new_pass) {
        User user = userCrudRepository.findByUsername(username);
        if (passwordEncoder.matches(pass, user.getPassword())) {
            user.setPassword(passwordEncoder.encode(new_pass));
        }
    }

    @Override
    public void deletePersonalUser(String username) {
        userCrudRepository.deleteUser(username);
    }

    @Override
    public AbstractPersonal findById(Long id_personal) {
        try {
            return personalCrudRepository.findById(id_personal);
        } catch (NoResultException e) {
            return new EmptyPersonal();
        }
    }

    @Override
    public AbstractPersonal findByUsername(String username) {
        try {
            return personalCrudRepository.findByUsername(username);
        } catch (NoResultException e) {
            return new EmptyPersonal();
        }
    }

    @Override
    public User findUserByUsername(String username) throws NoResultException {
        return userCrudRepository.findByUsername(username);
    }

    @Override
    public User find(String username) {
        return userCrudRepository.findByUsername(username);
    }

    @Override
    public Boolean isUsernameUnique(Long id_user, String username) {
        if (id_user == null) {
            return validatorRepository.isUsernameUnique(username) == 0L;
        } else {
            return validatorRepository.isUsernameUnique(id_user, username) == 0L;
        }
    }

    @Override
    public Boolean isValidPassword(String username, String pass) {
        return passwordEncoder.matches(pass, userCrudRepository.findByUsername(username).getPassword());
    }
}
