package edu.ues.aps.users.management.repository;

public interface CustomUserRepository {

    void updateLastLoggingToCurrentDate(Long id_user);
}
