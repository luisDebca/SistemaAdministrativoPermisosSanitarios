package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.model.AbstractPersonal;

public interface PersonalUserFinderService {

    AbstractPersonal findById(Long id_personal);

    AbstractPersonal findByUsername(String username);
}
