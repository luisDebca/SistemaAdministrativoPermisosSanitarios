package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.model.User;

import javax.persistence.NoResultException;

public interface UserProcessFinderService {

    User findUserByUsername(String username) throws NoResultException
            ;
}
