package edu.ues.aps.users.management.model;

import edu.ues.aps.process.record.base.model.ProcessUser;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

@Entity
@Table(name = "USER")
public class User implements AbstractUser {

    @Id
    private Long id;

    @Column(name = "username", nullable = false, unique = true, length = 50)
    private String username;

    @Column(name = "password", nullable = false, length = 125)
    private String password;

    @Column(name = "joining_date")
    private LocalDateTime joining_date;

    @Column(name = "last_login")
    private LocalDateTime last_login;

    @Column(name = "is_active", nullable = false)
    private Boolean is_active;

    @OneToMany(mappedBy = "from", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<MailRegister> mails = new ArrayList<>();

    @OneToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "id")
    @MapsId
    private Personal personal;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL, orphanRemoval = true)
    private List<ProcessUser> process = new ArrayList<>();

    @ManyToMany(cascade = CascadeType.MERGE, fetch = FetchType.LAZY)
    @JoinTable(name = "USER_PROFILE", joinColumns = {@JoinColumn(name = "id_user")}, inverseJoinColumns = {@JoinColumn(name = "id_profile")})
    private List<Profile> profiles = new ArrayList<>();

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getUsername() {
        return username;
    }

    @Override
    public void setUsername(String username) {
        this.username = username;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public void setPassword(String password) {
        this.password = password;
    }

    @Override
    public Boolean getIs_active() {
        return is_active;
    }

    @Override
    public void setIs_active(Boolean is_active) {
        this.is_active = is_active;
    }

    @Override
    public LocalDateTime getJoining_date() {
        return joining_date;
    }

    @Override
    public void setJoining_date(LocalDateTime joining_date) {
        this.joining_date = joining_date;
    }

    @Override
    public LocalDateTime getLast_login() {
        return last_login;
    }

    @Override
    public void setLast_login(LocalDateTime last_login) {
        this.last_login = last_login;
    }

    @Override
    public List<Profile> getProfiles() {
        return profiles;
    }

    @Override
    public void setProfiles(List<Profile> profiles) {
        this.profiles = profiles;
    }

    @Override
    public Personal getPersonal() {
        return personal;
    }

    @Override
    public void setPersonal(Personal personal) {
        this.personal = personal;
    }

    @Override
    public List<ProcessUser> getProcess() {
        return process;
    }

    @Override
    public void setProcess(List<ProcessUser> process) {
        this.process = process;
    }

    @Override
    public void removeProcess() {
        for (Iterator<ProcessUser> iterator = process.iterator(); iterator.hasNext(); ) {
            ProcessUser processUser = iterator.next();
            iterator.remove();
            processUser.getCasefile_process().getParticipants().remove(processUser);
            processUser.setUser(null);
            processUser.setCasefile_process(null);
        }
    }

    public List<MailRegister> getMails() {
        return mails;
    }

    public void setMails(List<MailRegister> mails) {
        this.mails = mails;
    }

    public void addMailRegister(MailRegister register){
        mails.add(register);
        register.setFrom(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        User user = (User) o;

        return username.equals(user.username) && password.equals(user.password);
    }

    @Override
    public int hashCode() {
        int result = username.hashCode();
        result = 31 * result + password.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", joining_date=" + joining_date +
                ", last_login=" + last_login +
                ", is_active=" + is_active +
                '}';
    }
}
