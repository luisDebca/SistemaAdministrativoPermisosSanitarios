package edu.ues.aps.users.management.async;

import edu.ues.aps.users.management.service.PersonalUserDTOFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.Collections;
import java.util.List;

@Controller
public class CurrentRegisterUserInformationProviderController {

    private static final Logger logger = LogManager.getLogger(CurrentRegisterUserInformationProviderController.class);

    private final PersonalUserDTOFinderService finderService;

    public CurrentRegisterUserInformationProviderController(PersonalUserDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @RequestMapping(value = "/get-all-users-full-name",method = RequestMethod.GET,produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getAllUserFullNameList(){
        logger.info("GET:: Get all users full name.");
        return finderService.findAllPersonalFullName();
    }

    @RequestMapping(value = "/get-all-users-full-name-and-charge", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<String> getAllUserFullNameAndChargeList(){
        logger.info("GET:: Get all users full name and charge");
        return finderService.findAllPersonalFullNameAndCharge();
    }
}
