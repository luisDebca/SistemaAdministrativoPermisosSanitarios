package edu.ues.aps.users.management.model;

import edu.ues.aps.storage.model.File;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class EmptyMailRegister implements AbstractMailRegister {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public EmptyUser getFrom() {
        return new EmptyUser();
    }

    @Override
    public void setFrom(User from) {

    }

    @Override
    public String getTo() {
        return "email to not found";
    }

    @Override
    public void setTo(String to) {

    }

    @Override
    public String getCc() {
        return "email cc not found";
    }

    @Override
    public void setCc(String cc) {

    }

    @Override
    public String getBcc() {
        return "email bcc not found";
    }

    @Override
    public void setBcc(String bcc) {

    }

    @Override
    public String getSubject() {
        return "Email register subject not found";
    }

    @Override
    public void setSubject(String subject) {

    }

    @Override
    public String getText() {
        return "Email register text not found";
    }

    @Override
    public void setText(String text) {

    }

    @Override
    public List<File> getAttached_files() {
        return Collections.emptyList();
    }

    @Override
    public void setAttached_files(List<File> attached_files) {

    }

    @Override
    public LocalDateTime getShipping_date() {
        return LocalDateTime.of(2000,1,1,0,0,0);
    }

    @Override
    public void setShipping_date(LocalDateTime shipping_date) {

    }

}
