package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.model.User;

public interface UserCrudRepository {

    User findByUsername(String username);

    void deleteUser(String username);
}
