package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.model.AbstractUser;
import edu.ues.aps.users.management.model.Profile;
import edu.ues.aps.users.management.repository.CustomUserRepository;
import edu.ues.aps.users.management.repository.UserCrudRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.ArrayList;
import java.util.List;

@Service("userDetailsService")
@Transactional
public class CustomUserDetailsService implements UserDetailsService {

    private static final boolean IS_USER_ENABLED = true;
    private static final boolean ACCOUNT_HAS_NOT_EXPIRED = true;
    private static final boolean CREDENTIALS_HAVE_NOT_EXPIRED = true;
    private static final boolean IS_ACCOUNT_NOT_LOCKED = true;

    private static final Logger logger = LogManager.getLogger(CustomUserDetailsService.class);

    private UserCrudRepository userCrudRepository;
    private CustomUserRepository customUserRepository;

    public CustomUserDetailsService(UserCrudRepository userCrudRepository, CustomUserRepository customUserRepository) {
        this.userCrudRepository = userCrudRepository;
        this.customUserRepository = customUserRepository;
    }

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        try {
            AbstractUser user = userCrudRepository.findByUsername(username);
            logger.info("Custom User Details: {}", user);
            return new User(user.getUsername(), user.getPassword(),
                    user.getIs_active(), ACCOUNT_HAS_NOT_EXPIRED, CREDENTIALS_HAVE_NOT_EXPIRED, IS_ACCOUNT_NOT_LOCKED, getGrantedAuthorities(user));
        } catch (NoResultException e) {
            logger.info("Custom User Not Found...");
            throw new UsernameNotFoundException("User name not found");
        }
    }

    private List<GrantedAuthority> getGrantedAuthorities(AbstractUser user) {
        List<GrantedAuthority> authorities = new ArrayList<>();
        for (Profile profile : user.getProfiles()) {
            authorities.add(new SimpleGrantedAuthority("ROLE_" + profile.getType()));
        }
        logger.info("Authorities: {}", authorities);
        updateUserLastLogging(user.getId());
        return authorities;
    }

    private void updateUserLastLogging(Long id_user) {
        customUserRepository.updateLastLoggingToCurrentDate(id_user);
        logger.info("Updating user last logging date");
    }
}
