package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.dto.AbstractPersonalUserDTO;

import java.util.List;

public interface PersonalUserDTOFinderService {

    List<AbstractPersonalUserDTO> findAll();

    AbstractPersonalUserDTO findById(Long id_personal);

    List<String> findAllPersonalFullName();

    List<String> findAllPersonalFullNameAndCharge();
}
