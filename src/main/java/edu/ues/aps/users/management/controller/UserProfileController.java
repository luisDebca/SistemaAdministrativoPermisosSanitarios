package edu.ues.aps.users.management.controller;

import edu.ues.aps.users.management.model.Personal;
import edu.ues.aps.users.management.service.PersonalUserFinderService;
import edu.ues.aps.users.management.service.PersonalUserPersistenceService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

import java.security.Principal;

@Controller
public class UserProfileController {

    private static final Logger logger = LogManager.getLogger(UserProfileController.class);

    private static final String MY_PROFILE = "user_profile";
    private static final String EDIT_PROFILE = "user_profile_edit";
    private static final String EDIT_PASSWORD = "password_edit";

    private final PersonalUserFinderService finderService;
    private final PersonalUserPersistenceService persistenceService;

    public UserProfileController(PersonalUserFinderService finderService, PersonalUserPersistenceService persistenceService) {
        this.finderService = finderService;
        this.persistenceService = persistenceService;
    }

    @GetMapping("my-profile")
    public String showMyProfile(Principal principal, ModelMap model) {
        logger.info("GET:: Show {} profile", principal.getName());
        model.addAttribute("user", finderService.findByUsername(principal.getName()));
        return MY_PROFILE;
    }

    @GetMapping("edit-profile")
    public String editMyProfile(Principal principal, ModelMap model) {
        logger.info("GET:: Show edit profile for user {}.", principal.getName());
        model.addAttribute("personal", finderService.findByUsername(principal.getName()));
        return EDIT_PROFILE;
    }

    @PostMapping("edit-profile")
    public String updateMyProfile(@ModelAttribute Personal personal, Principal principal) {
        logger.info("POST:: Update user {} profile.", principal.getName());
        Boolean updated = persistenceService.update(personal);
        if(updated) return "redirect:/logout";
        return "redirect:/my-profile";
    }

    @GetMapping("edit-password")
    public String editPassword(Principal principal, ModelMap model) {
        logger.info("GET:: Show user {} password change form.", principal.getName());
        model.addAttribute("password", new PasswordDTO());
        return EDIT_PASSWORD;
    }

    @PostMapping("edit-password")
    public String updatePassword(@ModelAttribute PasswordDTO dto, Principal principal) {
        logger.info("POST:: Update user {} password", principal.getName());
        persistenceService.updatePassword(principal.getName(), dto.getCurrent_password(), dto.getNew_password());
        return "redirect:/logout";
    }
}
