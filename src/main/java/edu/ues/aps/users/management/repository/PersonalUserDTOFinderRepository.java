package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.dto.AbstractPersonalUserDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface PersonalUserDTOFinderRepository {

    List<AbstractPersonalUserDTO> findAll();

    AbstractPersonalUserDTO findById(Long id_personal);

    List<String> findAllPersonalFullName();

    List<String> findAllPersonalFullNameAndCharge();

    String getPersonalFullName(String username) throws NoResultException;

    String getPersonalCharge(String username) throws NoResultException;
}

