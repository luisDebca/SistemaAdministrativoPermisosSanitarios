package edu.ues.aps.users.management.controller;

import edu.ues.aps.users.management.service.PersonalUserDTOFinderService;
import edu.ues.aps.users.management.service.UserProfileRelationService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.security.Principal;

@Controller
@RequestMapping("/user")
public class UserInformationController {

    private static final Logger logger = LogManager.getLogger(UserInformationController.class);

    private static final String USER_LIST_VIEW = "user_list";
    private static final String USER_INFO_VIEW = "user_information";

    private PersonalUserDTOFinderService personalUserDTOFinderService;
    private UserProfileRelationService userProfileRelationService;

    public UserInformationController(PersonalUserDTOFinderService personalUserDTOFinderService, UserProfileRelationService userProfileRelationService) {
        this.personalUserDTOFinderService = personalUserDTOFinderService;
        this.userProfileRelationService = userProfileRelationService;
    }

    @GetMapping
    public String showUserList(ModelMap model, Principal principal) {
        logger.info("GET:: Show registered user list.");
        model.addAttribute("users", personalUserDTOFinderService.findAll());
        model.addAttribute("loggedinuser", principal.getName());
        return USER_LIST_VIEW;
    }

    @GetMapping("/{id_personal}/info")
    public String showPersonalInformation(@PathVariable Long id_personal, ModelMap model) {
        logger.info("GET:: Show Personal information");
        model.addAttribute("personal", personalUserDTOFinderService.findById(id_personal));
        model.addAttribute("profiles", userProfileRelationService.findAllProfilesFromUser(id_personal));
        return USER_INFO_VIEW;
    }

}
