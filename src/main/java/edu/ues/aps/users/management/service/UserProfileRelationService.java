package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.dto.AbstractProfileDTO;

import java.util.List;

public interface UserProfileRelationService {

    List<AbstractProfileDTO> findAllProfilesFromUser(Long id_user);
}
