package edu.ues.aps.users.management.model;

public interface AbstractProfile {

    Long getId();

    void setId(Long id);

    UserProfileType getType();

    void setType(UserProfileType type);
}
