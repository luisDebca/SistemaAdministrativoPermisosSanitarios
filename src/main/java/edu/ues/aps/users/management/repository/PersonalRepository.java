package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.model.AbstractPersonal;
import edu.ues.aps.users.management.model.Personal;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class PersonalRepository implements PersonalCrudRepository, PersonalWithoutUserRepository {

    private SessionFactory sessionFactory;

    public PersonalRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractPersonal findById(Long id_personal) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select personal " +
                "from Personal personal " +
                "join fetch personal.user user " +
                "left join fetch user.profiles profiles " +
                "where personal.id = :id",AbstractPersonal.class)
                .setParameter("id",id_personal)
                .getSingleResult();
    }

    @Override
    public void save(AbstractPersonal personal) {
        sessionFactory.getCurrentSession().saveOrUpdate(personal);
    }

    @Override
    public void update(AbstractPersonal personal) {
        sessionFactory.getCurrentSession().saveOrUpdate(personal);
    }

    @Override
    public List<Personal> findAll() throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select personal " +
                "from Personal personal " +
                "left join personal.user user " +
                "where user is null", Personal.class)
                .getResultList();
    }

    @Override
    public AbstractPersonal findByUsername(String username) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select personal " +
                "from Personal personal " +
                "join fetch personal.user user " +
                "join fetch user.profiles profile " +
                "where user.username = :username", AbstractPersonal.class)
                .setParameter("username", username)
                .getSingleResult();
    }
}
