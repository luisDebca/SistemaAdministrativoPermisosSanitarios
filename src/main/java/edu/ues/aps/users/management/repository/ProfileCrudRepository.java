package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.model.AbstractProfile;
import edu.ues.aps.users.management.model.Profile;

import java.util.List;

public interface ProfileCrudRepository {

    AbstractProfile findById(Long id_profile);

    List<Profile> findAll();

}
