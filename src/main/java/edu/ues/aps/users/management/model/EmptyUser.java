package edu.ues.aps.users.management.model;

import edu.ues.aps.process.record.base.model.ProcessUser;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;

public class EmptyUser implements AbstractUser {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getUsername() {
        return "Username Not found";
    }

    @Override
    public String getPassword() {
        return "User password Not found";
    }

    @Override
    public Boolean getIs_active() {
        return false;
    }

    @Override
    public LocalDateTime getJoining_date() {
        return LocalDateTime.of(2000, 1, 1, 00, 00);
    }

    @Override
    public LocalDateTime getLast_login() {
        return LocalDateTime.of(2000, 1, 1, 00, 00);
    }

    @Override
    public List<Profile> getProfiles() {
        return Collections.emptyList();
    }

    @Override
    public AbstractPersonal getPersonal() {
        return new EmptyPersonal();
    }

    @Override
    public List<ProcessUser> getProcess() {
        return Collections.emptyList();
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public void setUsername(String username) {

    }

    @Override
    public void setPassword(String password) {

    }

    @Override
    public void setIs_active(Boolean is_active) {

    }

    @Override
    public void setJoining_date(LocalDateTime joining_date) {

    }

    @Override
    public void setLast_login(LocalDateTime last_login) {

    }

    @Override
    public void setProfiles(List<Profile> profiles) {

    }

    @Override
    public void setPersonal(Personal personal) {

    }

    @Override
    public void setProcess(List<ProcessUser> process) {

    }

    @Override
    public void removeProcess() {

    }

    @Override
    public List<MailRegister> getMails() {
        return Collections.emptyList();
    }

    @Override
    public void setMails(List<MailRegister> mails) {

    }

    @Override
    public void addMailRegister(MailRegister register) {

    }

    @Override
    public String toString() {
        return "EmptyUser{}";
    }
}
