package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.model.AbstractPersonal;

import javax.persistence.NoResultException;

public interface PersonalCrudRepository {

    AbstractPersonal findById(Long id_personal) throws NoResultException;

    AbstractPersonal findByUsername(String username) throws NoResultException;

    void save(AbstractPersonal personal);

    void update(AbstractPersonal personal);
}
