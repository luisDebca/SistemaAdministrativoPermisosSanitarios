package edu.ues.aps.users.management.async;

import edu.ues.aps.users.management.service.UserValidatorService;
import org.springframework.context.MessageSource;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.security.Principal;
import java.util.Collections;
import java.util.Locale;

@Controller
@RequestMapping("/user/validator")
public class UserValidatorController {

    private final MessageSource messageSource;
    private final UserValidatorService validatorService;

    public UserValidatorController(UserValidatorService validatorService, MessageSource messageSource) {
        this.validatorService = validatorService;
        this.messageSource = messageSource;
    }

    @GetMapping("/username")
    @ResponseBody
    public Object validateUserNameUnique(@RequestParam String username, @RequestParam Long id_user) {
        return validatorService.isUsernameUnique(id_user, username) ? Boolean.TRUE :
                Collections.singleton(messageSource.getMessage("user.username", new String[]{username}, Locale.getDefault()));
    }

    @PostMapping("/password")
    @ResponseBody
    public Object validateUserPassword(@RequestParam String password, Principal principal) {
        return validatorService.isValidPassword(principal.getName(), password) ? Boolean.TRUE :
                Collections.singleton(messageSource.getMessage("user.password", new String[]{}, Locale.getDefault()));
    }

}
