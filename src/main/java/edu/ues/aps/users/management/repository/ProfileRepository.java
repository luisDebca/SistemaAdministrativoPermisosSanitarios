package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.dto.AbstractProfileDTO;
import edu.ues.aps.users.management.model.AbstractProfile;
import edu.ues.aps.users.management.model.Profile;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class ProfileRepository implements ProfileCrudRepository, UserProfileRelationsRepository {

    private SessionFactory sessionFactory;

    public ProfileRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractProfile findById(Long id_profile) throws NoResultException {
        return sessionFactory.getCurrentSession().get(Profile.class, id_profile);
    }

    @Override
    public List<Profile> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select profile from Profile profile",
                Profile.class)
                .getResultList();
    }

    @Override
    public List<AbstractProfileDTO> findAllProfiles(Long id_user) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.users.management.dto.ProfileDTO(profiles.id,profiles.type) " +
                "from User user " +
                "inner join user.profiles profiles " +
                "where user.id = :id", AbstractProfileDTO.class)
                .setParameter("id", id_user)
                .getResultList();
    }
}
