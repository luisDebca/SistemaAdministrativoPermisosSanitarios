package edu.ues.aps.users.management.repository;

import edu.ues.aps.users.management.model.PersistentLogin;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.hibernate.SessionFactory;
import org.springframework.security.web.authentication.rememberme.PersistentRememberMeToken;
import org.springframework.security.web.authentication.rememberme.PersistentTokenRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;

@Repository
@Transactional
public class TokenRepository implements PersistentTokenRepository {

    private static final Logger logger = LogManager.getLogger(TokenRepository.class);

    private SessionFactory sessionFactory;

    public TokenRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void createNewToken(PersistentRememberMeToken token) {
        logger.info("Creating token for user: {}", token.getUsername());
        PersistentLogin persistentLogin = new PersistentLogin();
        persistentLogin.setUsername(token.getUsername());
        persistentLogin.setSeries(token.getSeries());
        persistentLogin.setToken(token.getTokenValue());
        persistentLogin.setLast_used(LocalDateTime.ofInstant(token.getDate().toInstant(), ZoneId.systemDefault()));
        sessionFactory.getCurrentSession().persist(persistentLogin);
    }

    @Override
    public void updateToken(String seriesId, String tokenValue, Date lastUsed) {
        logger.info("Updating token for seriesId: {}", seriesId);
        PersistentLogin persistentLogin = sessionFactory.getCurrentSession().get(PersistentLogin.class, seriesId);
        persistentLogin.setToken(tokenValue);
        persistentLogin.setLast_used(LocalDateTime.ofInstant(lastUsed.toInstant(), ZoneId.systemDefault()));
        sessionFactory.getCurrentSession().saveOrUpdate(persistentLogin);
    }

    @Override
    public PersistentRememberMeToken getTokenForSeries(String seriesId) {
        logger.info("Fetch token if any for seriesId: {}", seriesId);
        try {
            PersistentLogin persistentLogin = sessionFactory.getCurrentSession().get(PersistentLogin.class, seriesId);
            return new PersistentRememberMeToken(
                    persistentLogin.getUsername(),
                    persistentLogin.getSeries(),
                    persistentLogin.getToken(),
                    Date.from(persistentLogin.getLast_used().atZone(ZoneId.systemDefault()).toInstant()));
        } catch (Exception e) {
            logger.info("Token no found...");
            return null;
        }
    }

    @Override
    public void removeUserTokens(String username) {
        logger.info("Removing token if any for user: {}", username);
        try {
            PersistentLogin persistentLogin = sessionFactory.getCurrentSession().createQuery("select persisten from PersistentLogin persisten " +
                    "where persisten.username = :username", PersistentLogin.class).setParameter("username", username).getSingleResult();
            logger.info("Remember me was selected");
            sessionFactory.getCurrentSession().delete(persistentLogin);
        } catch (NoResultException e) {
            logger.info("Persistent token with username: {} not found", username);
        }

    }
}
