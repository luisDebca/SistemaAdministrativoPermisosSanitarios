package edu.ues.aps.users.management.converter;

import edu.ues.aps.users.management.model.Profile;
import edu.ues.aps.users.management.service.ProfileCrudService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class RoleToProfileConverter implements Converter<Object,Profile>{

    private static final Logger logger = LogManager.getLogger(RoleToProfileConverter.class);

    private ProfileCrudService profileCrudService;

    public RoleToProfileConverter(ProfileCrudService profileCrudService) {
        this.profileCrudService = profileCrudService;
    }

    @Override
    public Profile convert(Object source) {
        Long id = Long.parseLong((String) source);
        Profile profile = (Profile) profileCrudService.findById(id);
        logger.info("CONVERTER:: Role to Profile Converter result: {}", profile);
        return profile;
    }
}
