package edu.ues.aps.users.management.service;

public interface SinglePersonalInformationService {

    String getFullName(String username);

    String getCharge(String username);
}
