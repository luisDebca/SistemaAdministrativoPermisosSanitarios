package edu.ues.aps.users.management.dto;

import java.time.LocalDateTime;

public interface AbstractPersonalUserDTO {

    Long getId_personal();

    String getPersonal_first_name();

    String getPersonal_last_name();

    String getPersonal_charge();

    String getPersonal_email();

    String getPersonal_designation();

    String getUser_username();

    LocalDateTime getUser_joining_date();

    LocalDateTime getUser_last_login();

    Boolean getUser_is_active();

    String getUser_joining_date_formatted();

    String getUser_last_login_formatted();
}
