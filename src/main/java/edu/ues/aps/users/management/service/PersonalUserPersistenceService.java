package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.model.AbstractPersonal;

public interface PersonalUserPersistenceService {

    void save(AbstractPersonal personal);

    Boolean update(AbstractPersonal personal);

    void updatePassword(String username, String current_pass, String new_pass);

    void deletePersonalUser(String username);
}
