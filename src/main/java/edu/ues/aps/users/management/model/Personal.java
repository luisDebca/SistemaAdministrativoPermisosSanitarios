package edu.ues.aps.users.management.model;

import javax.persistence.*;

@Entity
@Table(name = "PERSONAL")
public class Personal implements AbstractPersonal {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "first_name", nullable = false, length = 100)
    private String first_name;

    @Column(name = "last_name", nullable = false, length = 100)
    private String last_name;

    @Column(name = "charge", length = 150)
    private String charge;

    @Column(name = "email", length = 150)
    private String email;

    @Column(name = "designation", length = 10)
    private String designation;

    @OneToOne(mappedBy = "personal", cascade = CascadeType.ALL, orphanRemoval = true)
    private User user;

    @Override
    public Long getId() {
        return id;
    }

    @Override
    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getFirst_name() {
        return first_name;
    }

    @Override
    public void setFirst_name(String first_name) {
        this.first_name = first_name;
    }

    @Override
    public String getLast_name() {
        return last_name;
    }

    @Override
    public void setLast_name(String last_name) {
        this.last_name = last_name;
    }

    @Override
    public String getCharge() {
        return charge;
    }

    @Override
    public void setCharge(String charge) {
        this.charge = charge;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public String getDesignation() {
        return designation;
    }

    @Override
    public void setDesignation(String designation) {
        this.designation = designation;
    }

    @Override
    public User getUser() {
        return user;
    }

    @Override
    public void setUser(User user) {
        this.user = user;
    }

    @Override
    public void addUser(User user) {
        this.user = user;
        user.setPersonal(this);
    }

    public String getFull_name() {
        if (designation != null) {
            return designation + " " + first_name + " " + last_name;
        } else {
            return first_name + " " + last_name;
        }
    }

    public void removeUser() {
        if (user != null) {
            this.user.setPersonal(null);
        }
        this.user = null;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Personal personal = (Personal) o;

        return first_name.equals(personal.first_name) && last_name.equals(personal.last_name) && email.equals(personal.email);
    }

    @Override
    public int hashCode() {
        int result = first_name.hashCode();
        result = 31 * result + last_name.hashCode();
        result = 31 * result + email.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Personal{" +
                "id=" + id +
                ", first_name='" + first_name + '\'' +
                ", last_name='" + last_name + '\'' +
                ", charge='" + charge + '\'' +
                ", email='" + email + '\'' +
                ", designation='" + designation + '\'' +
                '}';
    }
}
