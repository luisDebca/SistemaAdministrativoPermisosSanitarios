package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.model.User;

public interface SingleUserFinderService {

    User find(String username);
}
