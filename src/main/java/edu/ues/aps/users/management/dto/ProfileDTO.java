package edu.ues.aps.users.management.dto;

import edu.ues.aps.users.management.model.UserProfileType;

public class ProfileDTO implements AbstractProfileDTO {

    private Long id_profile;
    private UserProfileType profile_type;

    public ProfileDTO(Long id_profile, UserProfileType profile_type) {
        this.id_profile = id_profile;
        this.profile_type = profile_type;
    }

    public Long getId_profile() {
        return id_profile;
    }

    public UserProfileType getProfile_type() {
        return profile_type;
    }
}
