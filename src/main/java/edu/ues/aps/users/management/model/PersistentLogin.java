package edu.ues.aps.users.management.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Table(name = "PERSISTEN_LOGINS")
public class PersistentLogin {

    @Id
    @Column(name = "series", nullable = false, unique = true, length = 100)
    private String series;

    @Column(name = "username", unique = true, nullable = false, length = 25)
    private String username;

    @Column(name = "token", unique = true, nullable = false, length = 100)
    private String token;

    @Column(name = "last_used")
    private LocalDateTime last_used;

    public String getSeries() {
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public LocalDateTime getLast_used() {
        return last_used;
    }

    public void setLast_used(LocalDateTime last_used) {
        this.last_used = last_used;
    }
}
