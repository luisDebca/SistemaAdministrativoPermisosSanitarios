package edu.ues.aps.users.management.dto;

import java.time.LocalDateTime;

public class EmptyPersonalUserDTO implements AbstractPersonalUserDTO {

    @Override
    public Long getId_personal() {
        return 0L;
    }

    @Override
    public String getPersonal_first_name() {
        return "Personal first name not found";
    }

    @Override
    public String getPersonal_last_name() {
        return "Personal last name not found";
    }

    @Override
    public String getPersonal_charge() {
        return "Personal charge not found";
    }

    @Override
    public String getPersonal_email() {
        return "Personal email not found";
    }

    @Override
    public String getPersonal_designation() {
        return "Personal designation not found";
    }

    @Override
    public String getUser_username() {
        return "User username not found";
    }

    @Override
    public LocalDateTime getUser_joining_date() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public LocalDateTime getUser_last_login() {
        return LocalDateTime.of(2000, 01, 01, 00, 00);
    }

    @Override
    public Boolean getUser_is_active() {
        return false;
    }

    @Override
    public String getUser_joining_date_formatted() {
        return "User joining date is not defined";
    }

    @Override
    public String getUser_last_login_formatted() {
        return "User last login is not defined";
    }
}
