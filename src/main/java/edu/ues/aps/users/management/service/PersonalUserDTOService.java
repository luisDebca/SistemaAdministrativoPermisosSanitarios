package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.dto.AbstractPersonalUserDTO;
import edu.ues.aps.users.management.dto.EmptyPersonalUserDTO;
import edu.ues.aps.users.management.repository.PersonalUserDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class PersonalUserDTOService implements PersonalUserDTOFinderService, SinglePersonalInformationService {

    private PersonalUserDTOFinderRepository finderRepository;

    public PersonalUserDTOService(PersonalUserDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public List<AbstractPersonalUserDTO> findAll() {
        return finderRepository.findAll();
    }

    @Override
    public AbstractPersonalUserDTO findById(Long id_personal) {
        try {
            return finderRepository.findById(id_personal);
        } catch (NoResultException e) {
            return new EmptyPersonalUserDTO();
        }
    }

    @Override
    public List<String> findAllPersonalFullName() {
        return finderRepository.findAllPersonalFullName();
    }

    @Override
    public List<String> findAllPersonalFullNameAndCharge() {
        return finderRepository.findAllPersonalFullNameAndCharge();
    }

    @Override
    public String getFullName(String username) {
        try {
            return finderRepository.getPersonalFullName(username);
        } catch (NoResultException e) {
            return " ";
        }
    }

    @Override
    public String getCharge(String username) {
        try {
            return finderRepository.getPersonalCharge(username);
        } catch (NoResultException e) {
            return " ";
        }
    }

}
