package edu.ues.aps.users.management.service;

public interface UserValidatorService {

    Boolean isUsernameUnique(Long id_user, String username);

    Boolean isValidPassword(String username, String pass);
}
