package edu.ues.aps.users.management.service;

import edu.ues.aps.users.management.dto.AbstractProfileDTO;
import edu.ues.aps.users.management.model.AbstractProfile;
import edu.ues.aps.users.management.model.EmptyProfile;
import edu.ues.aps.users.management.model.Profile;
import edu.ues.aps.users.management.repository.ProfileCrudRepository;
import edu.ues.aps.users.management.repository.UserProfileRelationsRepository;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Transactional(readOnly = true)
public class ProfileService implements ProfileCrudService, UserProfileRelationService {

    private static final Logger logger = LogManager.getLogger(ProfileService.class);

    private ProfileCrudRepository profileCrudRepository;
    private UserProfileRelationsRepository userProfileRelationsRepository;

    public ProfileService(ProfileCrudRepository profileCrudRepository, UserProfileRelationsRepository userProfileRelationsRepository) {
        this.profileCrudRepository = profileCrudRepository;
        this.userProfileRelationsRepository = userProfileRelationsRepository;
    }

    @Override
    public Map<Long, String> findAll() {
        logger.info("Find all profiles.");
        List<Profile> profiles = profileCrudRepository.findAll();
        Map<Long, String> map = new HashMap<>();
        for (Profile profile : profiles) {
            map.put(profile.getId(), profile.getType().getUserProfileType());
        }
        return map;
    }

    @Override
    public AbstractProfile findById(Long id_profile) {
        try {
            logger.info("Find profile with identifier {}", id_profile);
            return profileCrudRepository.findById(id_profile);
        } catch (NoResultException e) {
            logger.info("Profile with identifier {} not found.", id_profile);
            return new EmptyProfile();
        }
    }

    @Override
    public List<AbstractProfileDTO> findAllProfilesFromUser(Long id_user) {
        logger.info("Finding user profiles for user with identifier: {}", id_user);
        return userProfileRelationsRepository.findAllProfiles(id_user);
    }
}
