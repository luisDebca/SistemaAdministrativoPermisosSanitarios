package edu.ues.aps.users.message.model;

import java.time.LocalDateTime;

public class Message extends AbstractMessage{

    private Long id;
    private String content;
    private LocalDateTime create_on;

    @Override
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    @Override
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public LocalDateTime getCreate_on() {
        return create_on;
    }

    public void setCreate_on(LocalDateTime create_on) {
        this.create_on = create_on;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Message message = (Message) o;

        if (!content.equals(message.content)) return false;
        return create_on.equals(message.create_on);
    }

    @Override
    public int hashCode() {
        int result = content.hashCode();
        result = 31 * result + create_on.hashCode();
        return result;
    }

    @Override
    public String toString() {
        return "Message{" +
                "id=" + id +
                ", content='" + content + '\'' +
                ", create_on=" + create_on +
                '}';
    }
}
