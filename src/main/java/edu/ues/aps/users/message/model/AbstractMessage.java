package edu.ues.aps.users.message.model;

import java.time.LocalDateTime;

public abstract class AbstractMessage {

    public abstract Long getId();

    public abstract String getContent();

    public abstract LocalDateTime getCreate_on();
}
