package edu.ues.aps.sigaps.dto;

import java.time.LocalDateTime;

import org.joda.time.DateTime;



public class InfoUtilEstablishmentDTO {
	private String ucsfName;
	private String sibaseName;
	private String establishmentType;
	private String establishmentDetailType;
	private String establishmentName;
	private String ownerName;
	private String addressDetail;
	private LocalDateTime certificationStartOn;
	String certificatIonCode;
	
	public InfoUtilEstablishmentDTO(String ucsfName, String sibaseName, String establishmentType, String establishmentDetailType,
			String establishmentName, String ownerName, String addressDetail, LocalDateTime certificationStartOn, String certificatIonCode) {
		super();
		this.ucsfName = ucsfName;
		this.sibaseName = sibaseName;
		this.establishmentType = establishmentType;
		this.establishmentDetailType = establishmentDetailType;
		this.establishmentName = establishmentName;
		this.ownerName = ownerName;
		this.addressDetail = addressDetail;
		this.certificationStartOn = certificationStartOn;
		this.certificatIonCode = certificatIonCode;
	}
	public String getUcsfName() {
		return ucsfName;
	}
	
	public void setUcsfName(String ucsfName) {
		this.ucsfName = ucsfName;
	}
	
	public String getSibaseName() {
		return sibaseName;
	}
	public void setSibaseName(String sibaseName) {
		this.sibaseName = sibaseName;
	}
	public String getEstablishmentType() {
		return establishmentType;
	}
	
	public String getEstablishmentDetailType() {
		return establishmentDetailType;
	}
	public String getEstablishmentName() {
		return establishmentName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public String getAddressDetail() {
		return addressDetail;
	}
	public LocalDateTime getCertificationStartOn() {
		return certificationStartOn;
	}
	
	public void setCertificationStartOn(LocalDateTime certificationStartOn) {
		this.certificationStartOn = certificationStartOn;
	}
	public String getCertificatIonCode() {
		return certificatIonCode;
	}
	
}
