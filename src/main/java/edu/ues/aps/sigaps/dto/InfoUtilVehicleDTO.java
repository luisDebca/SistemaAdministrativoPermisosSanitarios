package edu.ues.aps.sigaps.dto;

import java.time.LocalDateTime;

public class InfoUtilVehicleDTO {
	
	private String sibasi;
	private String ucsf;
	private String request_code;
	private String license;
	private String name;
	private String owner;
	private String stand_address;
	private LocalDateTime certificationStartOn;
	private String codeVehicle;
	
	public InfoUtilVehicleDTO(String sibasi, String ucsf, String request_code, String license, String name, String owner,
			String stand_address, LocalDateTime certificationStartOn, String codeVehicle) {
		super();
		this.sibasi = sibasi;
		this.ucsf = ucsf;
		this.request_code = request_code;
		this.license = license;
		this.name = name;
		this.owner = owner;
		this.stand_address = stand_address;
		this.certificationStartOn = certificationStartOn;
		this.codeVehicle = codeVehicle;
	}
	public String getSibasi() {
		return sibasi;
	}
	public String getUcsf() {
		return ucsf;
	}
	public String getRequest_code() {
		return request_code;
	}
	public String getLicense() {
		return license;
	}
	public String getName() {
		return name;
	}
	public String getOwner() {
		return owner;
	}
	public String getStand_address() {
		return stand_address;
	}
	public LocalDateTime getCertificationStartOn() {
		return certificationStartOn;
	}
	public String getCodeVehicle() {
		return codeVehicle;
	}
	public void setCertificationStartOn(LocalDateTime certificationStartOn) {
		this.certificationStartOn = certificationStartOn;
	}
	public void setCodeVehicle(String codeVehicle) {
		this.codeVehicle = codeVehicle;
	}
	public void setSibasi(String sibasi) {
		this.sibasi = sibasi;
	}
	public void setUcsf(String ucsf) {
		this.ucsf = ucsf;
	}
	
	
	
}
