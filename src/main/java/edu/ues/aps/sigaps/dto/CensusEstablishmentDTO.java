package edu.ues.aps.sigaps.dto;

import java.time.LocalDateTime;

import edu.ues.aps.process.base.model.CasefileStatusType;

public class CensusEstablishmentDTO {
	
	private String establishmentType;
	private String establishmentName;
	private String ownerName;
	private String addressDetail;
	private Integer totalMaleEmployee;
	private Integer totalFemaleEmployee;
	private CasefileStatusType status;
	private LocalDateTime certificationStartOn;
	private String ucsfName;
	private String sibaseName;
	
	public CensusEstablishmentDTO(String establishmentType, String establishmentName, String ownerName,
			String addressDetail, Integer totalMaleEmployee, Integer totalFemaleEmployee, CasefileStatusType status,
			LocalDateTime certificationStartOn, String ucsfName, String sibaseName) {
		super();
		this.establishmentType = establishmentType;
		this.establishmentName = establishmentName;
		this.ownerName = ownerName;
		this.addressDetail = addressDetail;
		this.totalMaleEmployee = totalMaleEmployee;
		this.totalFemaleEmployee = totalFemaleEmployee;
		this.status = status;
		this.certificationStartOn = certificationStartOn;
		this.ucsfName = ucsfName;
		this.sibaseName = sibaseName;
	}
	public String getEstablishmentType() {
		return establishmentType;
	}
	public String getEstablishmentName() {
		return establishmentName;
	}
	public String getOwnerName() {
		return ownerName;
	}
	public String getAddressDetail() {
		return addressDetail;
	}
	public Integer getTotalMaleEmployee() {
		return totalMaleEmployee;
	}
	public Integer getTotalFemaleEmployee() {
		return totalFemaleEmployee;
	}
	public String getStatus() {
		return status.toString();
	}
	public LocalDateTime getCertificationStartOn() {
		return certificationStartOn;
	}
	public String getUcsfName() {
		return ucsfName;
	}
	public String getSibaseName() {
		return sibaseName;
	}
	public void setTotalMaleEmployee(Integer totalMaleEmployee) {
		this.totalMaleEmployee = totalMaleEmployee;
	}
	public void setTotalFemaleEmployee(Integer totalFemaleEmployee) {
		this.totalFemaleEmployee = totalFemaleEmployee;
	}
	
	
	
	
    

}
