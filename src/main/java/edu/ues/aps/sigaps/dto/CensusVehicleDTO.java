package edu.ues.aps.sigaps.dto;

import java.time.LocalDateTime;

import edu.ues.aps.process.area.vehicle.model.TransportedContentType;

public class CensusVehicleDTO {
	
	private TransportedContentType transported_content;
	private String license;
	private String name;
	private String distribution_company;
	private String vehicle_type;
	private LocalDateTime certificationStartOn;
	private String codVehicle;
	



	public CensusVehicleDTO(TransportedContentType transported_content, String license, String name,
			String distribution_company, String vehicle_type, LocalDateTime certificationStartOn, String codVehicle) {
		super();
		this.transported_content = transported_content;
		this.license = license;
		this.name = name;
		this.distribution_company = distribution_company;
		this.vehicle_type = vehicle_type;
		this.certificationStartOn = certificationStartOn;
		this.codVehicle = codVehicle;
	}


	public TransportedContentType getTransported_content() {
		return transported_content;
	}


	public String getLicense() {
		return license;
	}


	public String getName() {
		return name;
	}


	public String getDistribution_company() {
		return distribution_company;
	}
	

	public String getVehicle_type() {
		return vehicle_type;
	}
	

	public LocalDateTime getCertificationStartOn() {
		return certificationStartOn;
	}
	
	

	public void setCertificationStartOn(LocalDateTime certificationStartOn) {
		this.certificationStartOn = certificationStartOn;
	}


	public String getCodVehicle() {
		return codVehicle;
	}


	public void setCodVehicle(String codVehicle) {
		this.codVehicle = codVehicle;
	}

	
	

}
