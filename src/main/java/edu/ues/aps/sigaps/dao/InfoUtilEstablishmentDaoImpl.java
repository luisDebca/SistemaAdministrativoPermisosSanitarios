package edu.ues.aps.sigaps.dao;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.SessionFactory;
import org.joda.time.DateTime;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.sigaps.dto.InfoUtilEstablishmentDTO;
@Repository
public class InfoUtilEstablishmentDaoImpl implements InfoUtilEstablishmentDao {
	@Autowired
	private SessionFactory sessionFactory;
	

	@Override
	public List<InfoUtilEstablishmentDTO> listEstablishment(LocalDateTime starDate, LocalDateTime finishDate,
			CasefileStatusType stateCasefile, Long type) {
		if(type==0) {
			return sessionFactory.getCurrentSession().createQuery("select new "
					+ "edu.ues.aps.sigaps.dto.InfoUtilEstablishmentDTO(ucs.name, sib.zone, estt.type, est.type_detail, est.name, "
					+ "owe.name, adr.details|| ' '||adr.municipality.name||' '||adr.municipality.department.name, " 
					+ "casf.certification_start_on, casf.request_code||casf.request_number||casf.request_year)"
					+ "from CasefileEstablishment casf "
					+ "left join casf.ucsf ucs "
					+ "left join ucs.sibasi sib "
					+ "left join casf.establishment est "
					+ "left join est.owner owe "
					+ "left join est.type estt "
					+ "left join estt.section sct "
					+ "left join est.address adr "
					+ "where casf.creation_date between :startdate and :finishdate "
					+ "and casf.status =:statecasefile "			
					,InfoUtilEstablishmentDTO.class)
					.setParameter("startdate", starDate)
					.setParameter("finishdate", finishDate)
					.setParameter("statecasefile", stateCasefile).getResultList();			
		}
		else 
			return sessionFactory.getCurrentSession().createQuery("select new "
				+ "edu.ues.aps.sigaps.dto.InfoUtilEstablishmentDTO(ucs.name, sib.zone, estt.type, est.type_detail, est.name, "
				+ "owe.name, adr.details|| ' '||adr.municipality.name||' '||adr.municipality.department.name, " 
				+ "casf.certification_start_on, casf.request_code||casf.request_number||casf.request_year)"
				+ "from CasefileEstablishment casf "
				+ "left join casf.ucsf ucs "
				+ "left join ucs.sibasi sib "
				+ "left join casf.establishment est "
				+ "left join est.owner owe "
				+ "left join est.type estt "
				+ "left join estt.section sct "
				+ "left join est.address adr "
				+ "where casf.creation_date between :startdate and :finishdate "
				+ "and casf.status =:statecasefile "
				+ "and estt.id=:type "			
				,InfoUtilEstablishmentDTO.class)
				.setParameter("startdate", starDate)
				.setParameter("finishdate", finishDate)
				.setParameter("statecasefile", stateCasefile)
				.setParameter("type", type).getResultList();
	}
	

}
