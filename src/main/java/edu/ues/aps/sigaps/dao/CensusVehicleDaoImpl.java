package edu.ues.aps.sigaps.dao;

import java.time.LocalDateTime;
import java.util.List;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dto.CensusVehicleDTO;

@Repository
public class CensusVehicleDaoImpl implements CensusVehicleDao  {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<CensusVehicleDTO> listVehicle(LocalDateTime fromDate, LocalDateTime finishDate) {
		return (List<CensusVehicleDTO>) sessionFactory.getCurrentSession().createQuery("select new "
				+ "edu.ues.aps.sigaps.dto.CensusVehicleDTO(vehi.transported_content, vehi.license, owne.name, "
				+ "vebunch.distribution_company, vehi.vehicle_type, casfvehi.certification_start_on, casfvehi.request_code||casfvehi.request_number||casfvehi.request_year) "
				+ "from CasefileVehicle casfvehi "
				+ "left join casfvehi.vehicle vehi "
				+ "left join vehi.owner owne "
				+ "left join casfvehi.bunch vebunch "
				+ "where casfvehi.creation_date between :startdate and :finishdate "
				+ "and casfvehi.status =:statecasefile "
				,CensusVehicleDTO.class)
				.setParameter("startdate", fromDate)
				.setParameter("finishdate", finishDate)
		        .setParameter("statecasefile",CasefileStatusType.IN_PROCESS).getResultList();
			
		        
	}


}
