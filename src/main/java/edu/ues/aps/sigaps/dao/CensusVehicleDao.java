package edu.ues.aps.sigaps.dao;

import java.time.LocalDateTime;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dto.CensusVehicleDTO;
import java.util.List;

public interface CensusVehicleDao {
	List<CensusVehicleDTO>listVehicle(LocalDateTime fromDate, LocalDateTime finishDate);

}
