package edu.ues.aps.sigaps.dao;

import java.time.LocalDateTime;
import java.util.List;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dto.CensusEstablishmentDTO;

public interface CensusEstablishmentDao {
	List<CensusEstablishmentDTO> listEstablishment(Long ucsfName, Long sibaseName,LocalDateTime fromDate, LocalDateTime finishDate, CasefileStatusType stateCasefile);

}
