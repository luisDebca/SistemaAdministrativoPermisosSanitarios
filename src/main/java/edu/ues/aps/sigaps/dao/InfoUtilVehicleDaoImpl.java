package edu.ues.aps.sigaps.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.model.CasefileStatusType;

import edu.ues.aps.sigaps.dto.InfoUtilVehicleDTO;

@Repository
public class InfoUtilVehicleDaoImpl implements InfoUtilVehicleDao{
	
	@Autowired
	private SessionFactory sessionFactory;
	@Override
	public List<InfoUtilVehicleDTO> infoUtilListVehicle(LocalDateTime fromDate, LocalDateTime untilDate,Long iducsf, CasefileStatusType stateCasefile) {
		
		if(iducsf==0) {
		return  sessionFactory.getCurrentSession().createQuery("select new "
				+ "edu.ues.aps.sigaps.dto.InfoUtilVehicleDTO(sib.zone, ucs.name, casfvehi.request_code, vehi.license, "
				+ "vebunch.distribution_company, ow.name, adr.details|| ' '||adr.municipality.name||' '||adr.municipality.department.name, casfvehi.certification_start_on, casfvehi.request_code||casfvehi.request_number||casfvehi.request_year) "
				+ "from CasefileVehicle casfvehi "
				+ "left join casfvehi.ucsf ucs "
				+ "left join ucs.sibasi sib "
				+ "left join casfvehi.vehicle vehi "
				+ "left join vehi.owner ow "	
				+ "left join casfvehi.bunch vebunch "
				+ "left join vebunch.address adr "
				+ "where casfvehi.creation_date between :startdate and :finishdate "
				+ "and casfvehi.status = :statecasefile "
				+ "and casfvehi.section =:section "
				,InfoUtilVehicleDTO.class)
				.setParameter("startdate", fromDate)
				.setParameter("finishdate", untilDate)
		        .setParameter("statecasefile",stateCasefile)
		        .setParameter("section",  CasefileSection.VEHICLE ).getResultList();
		}
		else
			return  sessionFactory.getCurrentSession().createQuery("select new "
					+ "edu.ues.aps.sigaps.dto.InfoUtilVehicleDTO(sib.zone, ucs.name, casfvehi.request_code, vehi.license, "
					+ "vebunch.distribution_company, ow.name, adr.details|| ' '||adr.municipality.name||' '||adr.municipality.department.name, casfvehi.certification_start_on, casfvehi.request_code||casfvehi.request_number||casfvehi.request_year) "
					+ "from CasefileVehicle casfvehi "
					+ "left join casfvehi.ucsf ucs "
					+ "left join ucs.sibasi sib "
					+ "left join casfvehi.vehicle vehi "
					+ "left join vehi.owner ow "
					+ "left join casfvehi.bunch vebunch "
					+ "left join vebunch.address adr "
					+ "where casfvehi.creation_date between :startdate and :finishdate "
					+ "and casfvehi.status = :statecasefile "
					+ "and casfvehi.section =:section "
					+ "and ucs.id = :iducsf "
					,InfoUtilVehicleDTO.class)
					.setParameter("startdate", fromDate)
					.setParameter("finishdate", untilDate)
			        .setParameter("statecasefile",stateCasefile)
			        .setParameter("section",  CasefileSection.VEHICLE )
			        .setParameter("iducsf", iducsf).getResultList();
	}

}
