package edu.ues.aps.sigaps.dao;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.joda.time.DateTime;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.sigaps.dto.InfoUtilEstablishmentDTO;;

public interface InfoUtilEstablishmentDao {
	List<InfoUtilEstablishmentDTO> listEstablishment(LocalDateTime starDate, LocalDateTime finishDate, CasefileStatusType stateCasefile, Long type);

}
