package edu.ues.aps.sigaps.dao;

import java.time.LocalDateTime;
import java.util.List;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dto.CensusEstablishmentDTO;
import edu.ues.aps.sigaps.dto.InfoUtilEstablishmentDTO;

@Repository
public class CensusEstablishmentDaoImpl implements CensusEstablishmentDao {
	
	@Autowired
	private SessionFactory sessionFactory;

	@Override
	public List<CensusEstablishmentDTO> listEstablishment(Long ucsfName, Long sibaseName,
			LocalDateTime fromDate, LocalDateTime finishDate,CasefileStatusType stateCasefile) {
		
		if(ucsfName==0) {
			return sessionFactory.getCurrentSession().createQuery("select new "
					+ "edu.ues.aps.sigaps.dto.CensusEstablishmentDTO(estt.type, est.name, owe.name, "
					+ "adr.details||' '||adr.municipality.name||' '||adr.municipality.department.name, " 
					+ "est.maleEmployeeCount, est.femaleEmployeeCount, "
					+ "casf.status, casf.certification_start_on, ucs.name, sib.zone) "
					+ "from CasefileEstablishment casf "
					+ "left join casf.ucsf ucs "
					+ "left join ucs.sibasi sib "
					+ "left join casf.establishment est "
					+ "left join est.owner owe "
					+ "left join est.type estt "
					+ "left join est.address adr "
					+ "where casf.creation_date between :startdate and :finishdate "
					+ "and ucs.sibasi.id = :idsibasi "
					+ "and casf.status =:statecasefile "
					,CensusEstablishmentDTO.class)
					.setParameter("startdate", fromDate)
					.setParameter("finishdate", finishDate)
					.setParameter("idsibasi", sibaseName)
					.setParameter("statecasefile", stateCasefile).getResultList();
	
		}
		else
				return sessionFactory.getCurrentSession().createQuery("select new "
						+ "edu.ues.aps.sigaps.dto.CensusEstablishmentDTO(estt.type, est.name, owe.name, "
						+ "adr.details||' '||adr.municipality.name||' '||adr.municipality.department.name, " 
						+ "est.maleEmployeeCount, est.femaleEmployeeCount,  "
						+ "casf.status, casf.certification_start_on, ucs.name, sib.zone) "
						+ "from CasefileEstablishment casf "
						+ "left join casf.ucsf ucs "
						+ "left join ucs.sibasi sib "
						+ "left join casf.establishment est "
						+ "left join est.owner owe "
						+ "left join est.type estt "
						+ "left join est.address adr "
						+ "where casf.creation_date between :startdate and :finishdate "
						+ "and casf.status =:statecasefile "
						+ "and ucs.id= :iducsf "
						,CensusEstablishmentDTO.class)
						.setParameter("startdate", fromDate)
						.setParameter("finishdate", finishDate)
						.setParameter("statecasefile", stateCasefile)
						.setParameter("iducsf", ucsfName).getResultList();
			
	}
}