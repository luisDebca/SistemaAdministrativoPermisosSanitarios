package edu.ues.aps.sigaps.service;

import java.time.LocalDateTime;
import java.util.List;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dto.CensusEstablishmentDTO;

public interface CensusEstablishmentService {
	List<CensusEstablishmentDTO> listEstablishment(Long ucsfName, Long sibaseName,LocalDateTime fromDate, LocalDateTime finishDate, CasefileStatusType stateCasefile);

}
