package edu.ues.aps.sigaps.service;

import java.time.LocalDateTime;
import java.util.List;
import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dao.CensusEstablishmentDao;
import edu.ues.aps.sigaps.dto.CensusEstablishmentDTO;

@Service
@Transactional
public class CensusEstablishmentServiceImpl  implements CensusEstablishmentService{

	@Autowired
	CensusEstablishmentDao dao;
	
	@Override
	public List<CensusEstablishmentDTO> listEstablishment(Long ucsfName, Long sibaseName,
			LocalDateTime fromDate,LocalDateTime finishDate, CasefileStatusType stateCasefile) {
		
		return dao.listEstablishment(ucsfName, sibaseName,fromDate,  finishDate, stateCasefile);
	}

}
