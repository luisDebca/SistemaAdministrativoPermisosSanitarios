package edu.ues.aps.sigaps.service;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dao.InfoUtilVehicleDao;
import edu.ues.aps.sigaps.dto.InfoUtilVehicleDTO;


@Service
@Transactional
public class InfoUtilVehicleServiceImpl  implements InfoUtilVehicleService{
	
	@Autowired
	InfoUtilVehicleDao dao;

	@Override
	public List<InfoUtilVehicleDTO> infoUtilListVehicle(LocalDateTime fromDate, LocalDateTime untilDate, Long iducsf,
			CasefileStatusType stateCasefile) {
		return dao.infoUtilListVehicle(fromDate, untilDate, iducsf, stateCasefile);
	}

}
