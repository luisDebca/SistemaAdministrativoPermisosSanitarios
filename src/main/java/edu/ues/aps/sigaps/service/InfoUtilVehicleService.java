package edu.ues.aps.sigaps.service;

import java.time.LocalDateTime;
import java.util.List;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dto.InfoUtilVehicleDTO;

public interface InfoUtilVehicleService {
	List<InfoUtilVehicleDTO>infoUtilListVehicle(LocalDateTime fromDate, LocalDateTime untilDate ,Long iducsf,CasefileStatusType stateCasefile);
}
