package edu.ues.aps.sigaps.service;

import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dao.CensusVehicleDao;
import edu.ues.aps.sigaps.dto.CensusVehicleDTO;

@Service
@Transactional
public class CensusVehicleServiceImpl implements CensusVehicleService {
	
	@Autowired
	CensusVehicleDao dao;

	@Override
	public List<CensusVehicleDTO> listVehicle(LocalDateTime fromDate, LocalDateTime finishDate) {
		return dao.listVehicle(fromDate, finishDate);
	}

}
