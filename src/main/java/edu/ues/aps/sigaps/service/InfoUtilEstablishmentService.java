package edu.ues.aps.sigaps.service;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.joda.time.DateTime;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.sigaps.dto.InfoUtilEstablishmentDTO;

public interface InfoUtilEstablishmentService {
	List<InfoUtilEstablishmentDTO> listEstablishment(LocalDateTime starDate, LocalDateTime finishDate, CasefileStatusType stateCasefile, Long type);
}
