package edu.ues.aps.sigaps.service;


import java.time.LocalDateTime;
import java.util.List;

import javax.transaction.Transactional;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dao.InfoUtilEstablishmentDao;
import edu.ues.aps.sigaps.dto.InfoUtilEstablishmentDTO;

@Service
@Transactional
public class InfoUtilEstablishmentServiceImpl implements InfoUtilEstablishmentService {

	@Autowired
	InfoUtilEstablishmentDao dao;
	
	@Override
	public List<InfoUtilEstablishmentDTO> listEstablishment(LocalDateTime starDate, LocalDateTime finishDate,
			CasefileStatusType stateCasefile, Long type) {
		return dao.listEstablishment(starDate, finishDate, stateCasefile, type);
	}

}
