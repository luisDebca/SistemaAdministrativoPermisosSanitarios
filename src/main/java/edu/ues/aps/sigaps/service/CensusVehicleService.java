package edu.ues.aps.sigaps.service;

import java.time.LocalDateTime;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.dto.CensusVehicleDTO;
import java.util.List;


public interface CensusVehicleService {
	List<CensusVehicleDTO>listVehicle(LocalDateTime fromDate, LocalDateTime finishDate);

}
