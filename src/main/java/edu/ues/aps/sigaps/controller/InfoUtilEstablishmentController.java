package edu.ues.aps.sigaps.controller;


import java.time.LocalDate;
import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.exportexcel.InfoUtilEstablishmentExcel;
import edu.ues.aps.sigaps.service.InfoUtilEstablishmentService;

@Controller
@RequestMapping("/infoutil")
public class InfoUtilEstablishmentController {
	private static final Logger logger = LogManager.getLogger(InfoUtilEstablishmentController.class);
	private static final String INFO_UTIL_ESTABLISHMENT_LIST_VIEW = "infoUtilEstablishment";
	
	@Autowired
	InfoUtilEstablishmentService infoUtilEst;
	
	@GetMapping({ "/", "/infoutilestablishment" })
	public String getInfoUtilEstablishment() {
		return INFO_UTIL_ESTABLISHMENT_LIST_VIEW;
	}
	
	@GetMapping( "/infoutilestablishmentexcel" )
	public ModelAndView exportInfoUtilEstablishment(
			@RequestParam("until")  String  until,
			@RequestParam("state")  String state,
			@RequestParam("category")  Long category) {
		CasefileStatusType  stateCertification =CasefileStatusType.valueOf(state);
		logger.info(until);
		logger.info(state);
		logger.info(category);
		
		LocalDate parseUntilDate=LocalDate.parse(until);
		LocalDate parseFromDate=parseUntilDate.minusYears(3);
		
		LocalDateTime fromDate = parseFromDate.atTime(0,0,0);
		LocalDateTime untilDate = parseUntilDate.atTime(0,0,0);
		
		return new ModelAndView(new InfoUtilEstablishmentExcel(),"infoUtilEstablishment",infoUtilEst.listEstablishment(fromDate, untilDate, stateCertification, category)) ;
	}

}
