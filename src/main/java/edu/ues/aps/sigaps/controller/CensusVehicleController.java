package edu.ues.aps.sigaps.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.sigaps.exportexcel.CensusVehicleExcel;
import edu.ues.aps.sigaps.exportexcel.InfoUtilEstablishmentExcel;
import edu.ues.aps.sigaps.service.CensusVehicleService;
import edu.ues.aps.sigaps.service.InfoUtilEstablishmentService;

@Controller
@RequestMapping("/census")
public class CensusVehicleController {
	private static final Logger logger = LogManager.getLogger(CensusVehicleController.class);
	private static final String CENSUS_VEHICLE_LIST_VIEW = "censusVehicle";
	
	
	@Autowired
	CensusVehicleService censusVehicle;
	
	
	@GetMapping({ "/", "/censusvehicle" })
	public String getCensusVehicle() {
		return CENSUS_VEHICLE_LIST_VIEW;
	}
	
	@GetMapping( "/censusvehicleexcel" )
	public ModelAndView exportInfoUtilEstablishment(
			@RequestParam("until")  String  until) {
			logger.info(until);
		
		
		LocalDate parseUntilDate=LocalDate.parse(until);
		LocalDate parseFromDate=parseUntilDate.minusYears(1);
		
		LocalDateTime fromDate = parseFromDate.atTime(0,0,0);
		LocalDateTime untilDate = parseUntilDate.atTime(0,0,0);
	
		return new ModelAndView(new CensusVehicleExcel(),"censusVehicle",censusVehicle.listVehicle(fromDate,untilDate)) ;
	}

	

}
