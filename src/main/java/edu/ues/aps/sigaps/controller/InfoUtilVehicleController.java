package edu.ues.aps.sigaps.controller;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

import edu.ues.aps.process.base.dto.AbstractMapDTO;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.service.SibasiDTOFinderService;
import edu.ues.aps.process.base.service.UcsfDTOFinderService;
import edu.ues.aps.sigaps.exportexcel.InfoUtilVechicleExcel;
import edu.ues.aps.sigaps.service.InfoUtilVehicleService;

@Controller
@RequestMapping("/infoutil")
public class InfoUtilVehicleController {

	private static final Logger logger = LogManager.getLogger(InfoUtilVehicleController.class);
	
	
	@Autowired
	private InfoUtilVehicleService infoVehicle;
	
	@Autowired
	private UcsfDTOFinderService finderService;
	
	@Autowired
	private SibasiDTOFinderService sibasiDTOFinderService;
    
	
	private class SibaUcsf{
		private Long iducsf;
		private Long idsibasi;
		public Long getIducsf() {
			return iducsf;
		}
		public void setIducsf(Long iducsf) {
			this.iducsf = iducsf;
		}
		public Long getIdsibasi() {
			return idsibasi;
		}
		public void setIdsibasi(Long idsibasi) {
			this.idsibasi = idsibasi;
		}
		
	}
	@GetMapping({ "/", "/infoUtilVehicle" })
	public String getCensusEstablishment(ModelMap model) {
		model.addAttribute("sibasi", new SibaUcsf());
		return "infoUtilVehicle";
	}
	
	@GetMapping( "/infoUtilVehicleExcel" )
	public ModelAndView exportInfoUtilVehicle(
			@RequestParam("iducsf")   Long ucsf,
			@RequestParam("until")  String  until,
			@RequestParam("state")  String state) {
	
		CasefileStatusType  stateCertification =CasefileStatusType.valueOf(state);
		LocalDate parseUntilDate=LocalDate.parse(until);
		LocalDate parseFromDate= parseUntilDate.minusYears(1);
		LocalDateTime fromDate = parseFromDate.atTime(0,0,0);
		LocalDateTime untilDate = parseUntilDate.atTime(0,0,0);
		
		return new ModelAndView(new InfoUtilVechicleExcel(),"infoUtilVehicle",infoVehicle.infoUtilListVehicle(fromDate, untilDate, ucsf, stateCertification)) ;

     }
	
	@GetMapping(value = "/find-all-ucsf-from-sibasi", produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseBody
    public List<AbstractMapDTO> findAllUcsfFromSibasi(@RequestParam("id") Long id_sibasi) {
        return finderService.findAllUcsfFromSibasi(id_sibasi);
    }
	
	@ModelAttribute("sibasi_list")
    public List<AbstractMapDTO> getSibasi_list() {
        return sibasiDTOFinderService.findAll();
    }

    @ModelAttribute("ucsf_list")
    public List<AbstractMapDTO> getUcsf_list() {
        return finderService.findAll();
    }
}
