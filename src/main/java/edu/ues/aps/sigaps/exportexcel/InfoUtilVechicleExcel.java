package edu.ues.aps.sigaps.exportexcel;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import edu.ues.aps.sigaps.dto.InfoUtilVehicleDTO;

public class InfoUtilVechicleExcel  extends AbstractXlsView{

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.setHeader("Content-Disposition", "attachmen;filename=\"infoUtilVehiculos.xls\"");
		@SuppressWarnings("unchecked")
		List<InfoUtilVehicleDTO> infoUtilVehicleList= (List<InfoUtilVehicleDTO>)model.get("infoUtilVehicle");
		Sheet sheet = workbook.createSheet("InfoUtilVehiculos");
		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("Region");
	    header.createCell(1).setCellValue("Sibasi");
		header.createCell(2).setCellValue("UCSF");
	    header.createCell(3).setCellValue("Tipo Establecimiento Autorizados");
	    header.createCell(4).setCellValue("Numero de placa");
	    header.createCell(5).setCellValue("Nombre del Establecimiento");
	    header.createCell(6).setCellValue("Propetario");
		header.createCell(7).setCellValue("Direccion de Permanencia");
		header.createCell(8).setCellValue("Fecha de Permiso");
		header.createCell(9).setCellValue("Fecha de Vencimiento");
		header.createCell(10).setCellValue("Codigo de Permiso");	
		int rowNum = 1;
		for(InfoUtilVehicleDTO infoVehicle: infoUtilVehicleList) {
			if(infoVehicle.getSibasi()==null) {
				infoVehicle.setSibasi("no definido");
			}
			if(infoVehicle.getUcsf()==null) {
				infoVehicle.setUcsf("no definido");
			}
			if(infoVehicle.getCodeVehicle()==null) {
				infoVehicle.setCodeVehicle("-no defino");
			}
			if(infoVehicle.getCertificationStartOn()==null) {
				infoVehicle.setCertificationStartOn(LocalDateTime.of(2000, 1, 1, 0, 0));
			}
		}
		
		for(InfoUtilVehicleDTO infoVehicle: infoUtilVehicleList) {
			Row row =sheet.createRow(rowNum++);
			row.createCell(0).setCellValue("Metropolitana");
			row.createCell(1).setCellValue(infoVehicle.getSibasi());
			row.createCell(2).setCellValue(infoVehicle.getUcsf());
			row.createCell(3).setCellValue(infoVehicle.getRequest_code());
			row.createCell(4).setCellValue(infoVehicle.getLicense());
			row.createCell(5).setCellValue(infoVehicle.getName());
			row.createCell(6).setCellValue(infoVehicle.getOwner());
			row.createCell(7).setCellValue(infoVehicle.getStand_address());
			row.createCell(8).setCellValue(infoVehicle.getCertificationStartOn().toLocalDate().toString());
			row.createCell(9).setCellValue(infoVehicle.getCertificationStartOn().plusYears(1).toLocalDate().toString());
			row.createCell(10).setCellValue("06".concat(infoVehicle.getCodeVehicle()));
		} 
		
	}

}
