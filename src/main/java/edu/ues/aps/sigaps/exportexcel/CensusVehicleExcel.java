package edu.ues.aps.sigaps.exportexcel;

import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;
import edu.ues.aps.sigaps.dto.CensusVehicleDTO;
import edu.ues.aps.sigaps.dto.InfoUtilEstablishmentDTO;

public class CensusVehicleExcel extends AbstractXlsView {
	private static final Logger logger = LogManager.getLogger(CensusVehicleExcel.class);
 
	
	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.setHeader("Content-Disposition", "attachmen;filename=\"censusVehicleExcel.xls\"");
		
		@SuppressWarnings("unchecked")
		List<CensusVehicleDTO> listVehicle = (List<CensusVehicleDTO> )model.get("censusVehicle");
		Sheet sheet = workbook.createSheet("Vehiculos");
		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("TIPO DE VEHICULO");
	    header.createCell(1).setCellValue("NUMERO DE PLACA");
		header.createCell(2).setCellValue("NOMBRE DEL PROPIETARIO");
	    header.createCell(3).setCellValue("EMPRESA A LA QUE PRESTA SERVCIO DE TRANSPORTE");
	    header.createCell(4).setCellValue("DESCRIPCIN DEL VEHICULO");
		header.createCell(5).setCellValue("FECHA DE PERMISO");
		header.createCell(6).setCellValue("FECHA DE VENCIMIENTO");
		header.createCell(7).setCellValue("CODIGO DE VEHICULO");
		
		int rowNum = 1;
		
		for(CensusVehicleDTO censusVehi:  listVehicle ) {
			if( censusVehi.getCertificationStartOn() == null) {
				 censusVehi.setCertificationStartOn(LocalDateTime.of(2000, 1, 1, 0, 0));
			}
			if( censusVehi.getCodVehicle() == null) {
				censusVehi.setCodVehicle("No definido");
			}
			else {
				censusVehi.setCodVehicle("06-".concat(censusVehi.getCodVehicle()));
				
			}
		}
		for(CensusVehicleDTO censusVehicle: listVehicle ) {
			Row row =sheet.createRow(rowNum++);
			 row.createCell(0).setCellValue(translateIntoType(censusVehicle.getTransported_content().toString()));
			 row.createCell(1).setCellValue(censusVehicle.getLicense());
			 row.createCell(2).setCellValue(censusVehicle.getName());
			 row.createCell(3).setCellValue(censusVehicle.getDistribution_company());
			 row.createCell(4).setCellValue(censusVehicle.getVehicle_type());
			 row.createCell(5).setCellValue(censusVehicle.getCertificationStartOn().toLocalDate().toString());
			 row.createCell(6).setCellValue(censusVehicle.getCertificationStartOn().plusYears(1).toLocalDate().toString());
		     row.createCell(7).setCellValue(censusVehicle.getCodVehicle());
			
		
	     }
	   }

	private String translateIntoType(String type)
	{
		switch(type)
		{
		case "PERISHABLE":
			return "Perecedero";
			
		case "NON_PERISHABLE":
			return "No perecedero";
			
		default:
			return "NO_ESPECIFICADO";
		}
	}



}
