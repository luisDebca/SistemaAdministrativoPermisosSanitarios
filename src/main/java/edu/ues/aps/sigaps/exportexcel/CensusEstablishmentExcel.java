package edu.ues.aps.sigaps.exportexcel;

import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import edu.ues.aps.sigaps.dto.CensusEstablishmentDTO;
import edu.ues.aps.sigaps.dto.InfoUtilEstablishmentDTO;

public class CensusEstablishmentExcel extends AbstractXlsView{
	private static final Logger logger = LogManager.getLogger(InfoUtilEstablishmentExcel.class);

@Override
protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
		HttpServletResponse response) throws Exception {
	response.setHeader("Content-Disposition", "attachmen;filename=\"censusEstablecimientos.xls\"");
	@SuppressWarnings("unchecked")
	List<CensusEstablishmentDTO> listCensusEstablishment = (List<CensusEstablishmentDTO> )model.get("censusEstablishment");
	Sheet sheet = workbook.createSheet("Establecmientos");
	Row header = sheet.createRow(0);
	header.createCell(0).setCellValue("ucsf");	
	header.createCell(1).setCellValue("sibasi");
	header.createCell(2).setCellValue("Tipo De Establecimiento");
    header.createCell(3).setCellValue("Nombre del Establecimiento");
	header.createCell(4).setCellValue("Propetario");
    header.createCell(5).setCellValue("Direccion del Establecimiento");
	header.createCell(6).setCellValue("Total de empleados");
	header.createCell(7).setCellValue("Estado del permiso");
	header.createCell(8).setCellValue("Fecha de Permiso");
	
	int rowNum = 1;
	
	for(CensusEstablishmentDTO censusEstablishment: listCensusEstablishment) {
		if(censusEstablishment.getTotalMaleEmployee()==null) {
			censusEstablishment.setTotalMaleEmployee(0);
			
		}
		if(censusEstablishment.getTotalFemaleEmployee()==null) {
			censusEstablishment.setTotalFemaleEmployee(0);
		}
	}
	
	for(CensusEstablishmentDTO censusEstablishment: listCensusEstablishment) {
		Row row =sheet.createRow(rowNum++);
		 row.createCell(0).setCellValue(censusEstablishment.getUcsfName());
		 row.createCell(1).setCellValue(censusEstablishment.getSibaseName());
		 row.createCell(2).setCellValue(censusEstablishment.getEstablishmentType());
		 row.createCell(3).setCellValue(censusEstablishment.getEstablishmentName());
		 row.createCell(4).setCellValue(censusEstablishment.getOwnerName());
		 row.createCell(5).setCellValue(censusEstablishment.getAddressDetail());
		 row.createCell(6).setCellValue(censusEstablishment.getTotalMaleEmployee()+censusEstablishment.getTotalFemaleEmployee());
		 row.createCell(7).setCellValue(translateIntoES(censusEstablishment.getStatus()));
	     row.createCell(8).setCellValue("no iniciado");
		
	
     }
   }
private String translateIntoES(String state)
{
	switch(state)
	{
	case "VALID":
		return "Valido";
		
	case "INVALID":
		return "Invalido";
		
	case "IN_PROCESS":
		return "En proceso";
		
	case "FIRST":
		return "Primera vez";
		
	default:
		return "NO_ESPECIFICADO";
	}
}

  }
	
