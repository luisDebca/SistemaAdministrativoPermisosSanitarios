package edu.ues.aps.sigaps.exportexcel;


import java.time.LocalDateTime;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.springframework.web.servlet.view.document.AbstractXlsView;

import edu.ues.aps.sigaps.dto.InfoUtilEstablishmentDTO;




public class InfoUtilEstablishmentExcel extends AbstractXlsView{
	private static final Logger logger = LogManager.getLogger(InfoUtilEstablishmentExcel.class);

	@Override
	protected void buildExcelDocument(Map<String, Object> model, Workbook workbook, HttpServletRequest request,
			HttpServletResponse response) throws Exception {
		response.setHeader("Content-Disposition", "attachmen;filename=\"infoUtilEstablecimientos.xls\"");
		@SuppressWarnings("unchecked")
		List<InfoUtilEstablishmentDTO> listInfoUtilEstablishment = (List<InfoUtilEstablishmentDTO> )model.get("infoUtilEstablishment");
		Sheet sheet = workbook.createSheet("Establecmientos");
		Row header = sheet.createRow(0);
		header.createCell(0).setCellValue("Sibasi");
	    header.createCell(1).setCellValue("UCSF");
		header.createCell(2).setCellValue("Clasificacion");
		header.createCell(3).setCellValue("Tipo");
	    header.createCell(4).setCellValue("Nombre del Establecimiento");
		header.createCell(5).setCellValue("Propetario");
		header.createCell(6).setCellValue("Direccion del Establecimiento");
		header.createCell(7).setCellValue("Fecha de Permiso");
		header.createCell(8).setCellValue("Codigo de permiso");	
		int rowNum = 1;
		for(InfoUtilEstablishmentDTO InfoUtilEsta: listInfoUtilEstablishment) {
			if(InfoUtilEsta.getUcsfName() == null) {
				InfoUtilEsta.setUcsfName("NO DEFINIDO");
				InfoUtilEsta.setSibaseName("NO DEFINIDO");
			}
			if(InfoUtilEsta.getCertificationStartOn() == null) {
				InfoUtilEsta.setCertificationStartOn(LocalDateTime.of(2000, 1, 1, 0, 0));
			}
			
		}
		for(InfoUtilEstablishmentDTO InfoUtilEstablishment: listInfoUtilEstablishment) {
			Row row =sheet.createRow(rowNum++);
			 row.createCell(0).setCellValue(InfoUtilEstablishment.getSibaseName());
			 row.createCell(1).setCellValue(InfoUtilEstablishment.getUcsfName());
			 row.createCell(2).setCellValue(InfoUtilEstablishment.getEstablishmentType());
			 row.createCell(3).setCellValue(InfoUtilEstablishment.getEstablishmentDetailType());
			 row.createCell(4).setCellValue(InfoUtilEstablishment.getEstablishmentName());
			 row.createCell(5).setCellValue(InfoUtilEstablishment.getOwnerName());
			 row.createCell(6).setCellValue(InfoUtilEstablishment.getAddressDetail());
		     row.createCell(7).setCellValue(InfoUtilEstablishment.getCertificationStartOn().toLocalDate().toString());
			 row.createCell(8).setCellValue(InfoUtilEstablishment.getCertificatIonCode());
		}
		
	}
	

}
