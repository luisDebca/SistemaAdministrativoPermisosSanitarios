package edu.ues.aps.exception.controller;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(value = HttpStatus.NOT_FOUND)
public class NotResourceFoundExceptionHandlerController extends RuntimeException {

    public NotResourceFoundExceptionHandlerController() {
    }

    public NotResourceFoundExceptionHandlerController(String message) {
        super(message);
    }

    public NotResourceFoundExceptionHandlerController(String message, Throwable cause) {
        super(message, cause);
    }

    public NotResourceFoundExceptionHandlerController(Throwable cause) {
        super(cause);
    }

}
