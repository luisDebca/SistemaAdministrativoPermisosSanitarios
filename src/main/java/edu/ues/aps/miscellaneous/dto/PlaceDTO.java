package edu.ues.aps.miscellaneous.dto;

public class PlaceDTO implements AbstractPlaceDTO{

    private String organization_name;
    private String section_name;
    private String business_description;
    private String address;
    private String department;
    private String municipality;
    private String telephone;
    private String email;
    private String FAX;
    private String zone;
    private String availability;

    public PlaceDTO(String organization_name, String section_name, String business_description, String address, String department, String municipality, String telephone, String email, String FAX, String zone, String availability) {
        this.organization_name = organization_name;
        this.section_name = section_name;
        this.business_description = business_description;
        this.address = address;
        this.department = department;
        this.municipality = municipality;
        this.telephone = telephone;
        this.email = email;
        this.FAX = FAX;
        this.zone = zone;
        this.availability = availability;
    }

    @Override
    public String getOrganization_name() {
        return organization_name;
    }

    @Override
    public String getSection_name() {
        return section_name;
    }

    @Override
    public String getBusiness_description() {
        return business_description;
    }

    @Override
    public String getAddress() {
        return address;
    }

    @Override
    public String getDepartment() {
        return department;
    }

    @Override
    public String getMunicipality() {
        return municipality;
    }

    @Override
    public String getTelephone() {
        return telephone;
    }

    @Override
    public String getEmail() {
        return email;
    }

    @Override
    public String getFAX() {
        return FAX;
    }

    @Override
    public String getZone() {
        return zone;
    }

    @Override
    public String getAvailability() {
        return availability;
    }

    @Override
    public Boolean isValid() {
        return true;
    }
}
