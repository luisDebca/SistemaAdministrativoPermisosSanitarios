package edu.ues.aps.miscellaneous.dto;

public interface AbstractPlaceDTO {

    String getOrganization_name();

    String getSection_name();

    String getBusiness_description();

    String getAddress();

    String getDepartment();

    String getMunicipality();

    String getTelephone();

    String getEmail();

    String getFAX();

    String getZone();

    String getAvailability();

    Boolean isValid();

}
