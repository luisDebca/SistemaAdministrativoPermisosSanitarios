package edu.ues.aps.miscellaneous.dto;

public class EmptyPlaceDTO implements AbstractPlaceDTO {

    @Override
    public String getOrganization_name() {
        return "Not found";
    }

    @Override
    public String getSection_name() {
        return "Not found";
    }

    @Override
    public String getBusiness_description() {
        return "Not found";
    }

    @Override
    public String getAddress() {
        return "Not found";
    }

    @Override
    public String getDepartment() {
        return "Not found";
    }

    @Override
    public String getMunicipality() {
        return "Not found";
    }

    @Override
    public String getTelephone() {
        return "Not found";
    }

    @Override
    public String getEmail() {
        return "Not found";
    }

    @Override
    public String getFAX() {
        return "Not found";
    }

    @Override
    public String getZone() {
        return "Not found";
    }

    @Override
    public String getAvailability() {
        return "Not found";
    }

    @Override
    public Boolean isValid() {
        return false;
    }
}
