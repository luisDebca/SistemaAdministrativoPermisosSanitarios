package edu.ues.aps.miscellaneous.service;

import edu.ues.aps.miscellaneous.dto.AbstractPlaceDTO;
import edu.ues.aps.miscellaneous.dto.EmptyPlaceDTO;
import edu.ues.aps.miscellaneous.repository.PlaceDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional(readOnly = true)
public class PlaceDTOService implements PlaceDTOFinderService {

    private final PlaceDTOFinderRepository finderRepository;

    public PlaceDTOService(PlaceDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractPlaceDTO getInformation() {
        try {
            return finderRepository.getInformation();
        } catch (NoResultException e) {
            return new EmptyPlaceDTO();
        }
    }
}
