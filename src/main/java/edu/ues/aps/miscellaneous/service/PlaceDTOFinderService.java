package edu.ues.aps.miscellaneous.service;

import edu.ues.aps.miscellaneous.dto.AbstractPlaceDTO;

public interface PlaceDTOFinderService {

    AbstractPlaceDTO getInformation();
}
