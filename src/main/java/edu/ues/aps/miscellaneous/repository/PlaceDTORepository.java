package edu.ues.aps.miscellaneous.repository;

import edu.ues.aps.miscellaneous.dto.AbstractPlaceDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class PlaceDTORepository implements PlaceDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public PlaceDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractPlaceDTO getInformation() throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.miscellaneous.dto.PlaceDTO(" +
                "place.organization_name,place.section_name,place.business_description,place.address," +
                "place.department,place.municipality,place.telephone,place.email,place.fax,place.zone,place.availability)" +
                "from Place place " +
                "where place.id = 1",AbstractPlaceDTO.class)
                .getSingleResult();
    }
}
