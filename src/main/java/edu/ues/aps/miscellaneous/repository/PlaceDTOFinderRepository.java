package edu.ues.aps.miscellaneous.repository;

import edu.ues.aps.miscellaneous.dto.AbstractPlaceDTO;

import javax.persistence.NoResultException;

public interface PlaceDTOFinderRepository {

    AbstractPlaceDTO getInformation() throws NoResultException;
}
