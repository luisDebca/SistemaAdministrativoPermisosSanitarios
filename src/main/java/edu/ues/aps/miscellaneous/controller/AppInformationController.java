package edu.ues.aps.miscellaneous.controller;

import edu.ues.aps.miscellaneous.service.PlaceDTOFinderService;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/app/information")
public class AppInformationController {

    private static final String APP_INFORMATION = "app-info";

    private final PlaceDTOFinderService finderService;

    public AppInformationController(PlaceDTOFinderService finderService) {
        this.finderService = finderService;
    }

    @GetMapping({"/","/index"})
    public String showAppInformation(ModelMap model){
        model.addAttribute("info",finderService.getInformation());
        return APP_INFORMATION;
    }
}
