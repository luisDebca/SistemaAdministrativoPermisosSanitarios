package edu.ues.aps.miscellaneous.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;

@Controller
public class IndexController {

    @GetMapping(value = {"/","/index","/home"})
    public String showHomePage(){
        return "index";
    }

    @GetMapping(value = "/index/admin")
    public String showAdminIndex(){return "index_admin";}

    @GetMapping(value = "/index/visitor")
    public String showVisitor(){return "index_visitor";}

    @GetMapping(value = "/index/coordinator")
    public String showCoordinatorIndex(){return "index_coordinator";}

    @GetMapping(value = "/index/technical")
    public String showTechnicalIndex(){return "index_technical";}

    @GetMapping(value = "/index/juridical")
    public String showJuridicalIndex(){return "index_juridical";}
}
