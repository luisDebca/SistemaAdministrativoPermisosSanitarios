package edu.ues.aps.storage.dto;

import java.time.LocalDateTime;

public class EmptyFileDTO implements AbstractFileDTO{

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public String getName() {
        return "Name not found";
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public String getOriginal_name() {
        return "Name not found";
    }

    @Override
    public void setOriginal_name(String original_name) {

    }

    @Override
    public Long getSize() {
        return 0L;
    }

    @Override
    public Long getKBSize() {
        return 0L;
    }

    @Override
    public Long getMBSize() {
        return 0L;
    }

    @Override
    public Long getGBSize() {
        return 0L;
    }

    @Override
    public void setSize(Long size) {

    }

    @Override
    public String getType() {
        return "Type not found";
    }

    @Override
    public void setType(String type) {

    }

    @Override
    public String getPath() {
        return "Path not found";
    }

    @Override
    public void setPath(String path) {

    }

    @Override
    public String getCreation_date() {
        return "Date not found";
    }

    @Override
    public void setCreation_date(LocalDateTime creation_date) {

    }

    @Override
    public String getModification_date() {
        return "Date not found";
    }

    @Override
    public void setModification_date(LocalDateTime modification_date) {

    }
}
