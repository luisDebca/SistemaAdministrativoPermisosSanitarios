package edu.ues.aps.storage.dto;

import java.time.LocalDateTime;

public interface AbstractFileDTO {

    public Long getId();

    public String getName();

    public void setName(String name);

    public String getOriginal_name();

    public void setOriginal_name(String original_name);

    public Long getSize();

    public Long getKBSize();

    public Long getMBSize();

    public Long getGBSize();

    public void setSize(Long size);

    public String getType();

    public void setType(String type);

    public String getPath();

    public void setPath(String path);

    public String getCreation_date();

    public void setCreation_date(LocalDateTime creation_date);

    public String getModification_date();

    public void setModification_date(LocalDateTime modification_date);
}
