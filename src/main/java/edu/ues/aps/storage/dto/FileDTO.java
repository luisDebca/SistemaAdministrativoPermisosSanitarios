package edu.ues.aps.storage.dto;

import edu.ues.aps.utilities.formatted.datetime.DatetimeUtil;

import java.time.LocalDateTime;

public class FileDTO implements AbstractFileDTO{

    private Long id;
    private String name;
    private String original_name;
    private Long size;
    private String type;
    private String path;
    private LocalDateTime creation_date;
    private LocalDateTime modification_date;

    public FileDTO(Long id, String name, String original_name, Long size, String type, String path, LocalDateTime creation_date, LocalDateTime modification_date) {
        this.id = id;
        this.name = name;
        this.original_name = original_name;
        this.size = size;
        this.type = type;
        this.path = path;
        this.creation_date = creation_date;
        this.modification_date = modification_date;
    }

    public Long getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public void setOriginal_name(String original_name) {
        this.original_name = original_name;
    }

    public Long getSize() {
        return size;
    }

    public Long getKBSize(){
        if(size != null){
            return size/1024;
        }
        return 0L;
    }

    public Long getMBSize(){
        if(size != null){
            return size/1048576;
        }
        return 0L;
    }

    public Long getGBSize(){
        if(size != null){
            return size/1073741824;
        }
        return 0L;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getCreation_date() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(creation_date);
    }

    public void setCreation_date(LocalDateTime creation_date) {
        this.creation_date = creation_date;
    }

    public String getModification_date() {
        return DatetimeUtil.parseDatetimeToLongDateFormat(modification_date);
    }

    public void setModification_date(LocalDateTime modification_date) {
        this.modification_date = modification_date;
    }
}
