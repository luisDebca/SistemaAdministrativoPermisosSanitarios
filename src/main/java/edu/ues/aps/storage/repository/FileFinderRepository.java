package edu.ues.aps.storage.repository;

import edu.ues.aps.storage.model.AbstractFile;

import javax.persistence.NoResultException;

public interface FileFinderRepository {

    AbstractFile find(Long id_file) throws NoResultException;

    AbstractFile find(String original_name) throws NoResultException;

}
