package edu.ues.aps.storage.repository;

import edu.ues.aps.storage.dto.AbstractFileDTO;

import javax.persistence.NoResultException;
import java.util.List;

public interface FileDTOFinderRepository {

    AbstractFileDTO findByID(Long id_file) throws NoResultException;

    List<AbstractFileDTO> findAllByNameLike(String namePath);

    List<AbstractFileDTO> findAll();
}
