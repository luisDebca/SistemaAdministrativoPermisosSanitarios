package edu.ues.aps.storage.repository;

import edu.ues.aps.storage.model.File;

public interface FilePersistenceRepository {

    void save(File file);

    void merge(File file);

    void delete(Long id_file);

}
