package edu.ues.aps.storage.repository;

import edu.ues.aps.storage.dto.AbstractFileDTO;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;
import java.util.List;

@Repository
public class FileDTORepository implements FileDTOFinderRepository {

    private final SessionFactory sessionFactory;

    public FileDTORepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public AbstractFileDTO findByID(Long id_file) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.storage.dto.FileDTO(file.id,file.name,file.original_name,file.size,file.format," +
                "file.path,file.creation_date,file.modified_date) " +
                "from File file " +
                "where file.id = :id", AbstractFileDTO.class)
                .setParameter("id", id_file)
                .getSingleResult();
    }

    @Override
    public List<AbstractFileDTO> findAllByNameLike(String namePath) {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.storage.dto.FileDTO(file.id,file.name,file.original_name,file.size,file.format," +
                "file.path,file.creation_date,file.modified_date) " +
                "from File file " +
                "where file.name like :pathName", AbstractFileDTO.class)
                .setParameter("pathName", "%" + namePath + "%")
                .getResultList();
    }

    @Override
    public List<AbstractFileDTO> findAll() {
        return sessionFactory.getCurrentSession().createQuery("select new " +
                "edu.ues.aps.storage.dto.FileDTO(file.id,file.name,file.original_name,file.size,file.format," +
                "file.path,file.creation_date,file.modified_date) " +
                "from File file ", AbstractFileDTO.class)
                .getResultList();
    }
}
