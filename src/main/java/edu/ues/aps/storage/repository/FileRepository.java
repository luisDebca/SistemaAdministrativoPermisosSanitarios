package edu.ues.aps.storage.repository;

import edu.ues.aps.storage.model.AbstractFile;
import edu.ues.aps.storage.model.File;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class FileRepository implements FilePersistenceRepository, FileFinderRepository{

    private final SessionFactory sessionFactory;

    public FileRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }


    @Override
    public AbstractFile find(Long id_file) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select file " +
                "from File file " +
                "where file.id = :id",AbstractFile.class)
                .setParameter("id",id_file)
                .getSingleResult();
    }

    @Override
    public AbstractFile find(String original_name) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select file " +
                "from File file " +
                "where file.original_name = :name",AbstractFile.class)
                .setParameter("name",original_name)
                .getSingleResult();
    }

    @Override
    public void save(File file) {
        sessionFactory.getCurrentSession().persist(file);
    }

    @Override
    public void merge(File file) {
        sessionFactory.getCurrentSession().merge(file);
    }

    @Override
    public void delete(Long id_file) {
        sessionFactory.getCurrentSession().createQuery("delete from File file where file.id = :id")
                .setParameter("id",id_file)
                .executeUpdate();
    }
}
