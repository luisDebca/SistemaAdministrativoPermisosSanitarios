package edu.ues.aps.storage.model;


import java.time.LocalDateTime;

public interface AbstractFile {

    Long getId();

    void setId(Long id);

    String getOriginal_name();

    void setOriginal_name(String original_name);

    String getName();

    void setName(String name);

    String getPath();

    void setPath(String path);

    String getFormat();

    void setFormat(String format);

    Long getSize();

    void setSize(Long size);

    LocalDateTime getCreation_date();

    void setCreation_date(LocalDateTime creation_date);

    LocalDateTime getModified_date();

    void setModified_date(LocalDateTime modified_date);

}
