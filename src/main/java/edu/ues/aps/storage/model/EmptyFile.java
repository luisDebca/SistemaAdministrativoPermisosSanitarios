package edu.ues.aps.storage.model;


import java.time.LocalDateTime;

public class EmptyFile implements AbstractFile {

    @Override
    public Long getId() {
        return 0L;
    }

    @Override
    public void setId(Long id) {

    }

    @Override
    public String getOriginal_name() {
        return "not name found";
    }

    @Override
    public void setOriginal_name(String original_name) {

    }

    @Override
    public String getName() {
        return "not name found";
    }

    @Override
    public void setName(String name) {

    }

    @Override
    public String getPath() {
        return "file not exits";
    }

    @Override
    public void setPath(String path) {

    }

    @Override
    public String getFormat() {
        return "not format found";
    }

    @Override
    public void setFormat(String format) {

    }

    @Override
    public Long getSize() {
        return 0L;
    }

    @Override
    public void setSize(Long size) {

    }

    @Override
    public LocalDateTime getCreation_date() {
        return LocalDateTime.of(2000,1,1,0,0,0);
    }

    @Override
    public void setCreation_date(LocalDateTime creation_date) {

    }

    @Override
    public LocalDateTime getModified_date() {
        return LocalDateTime.of(2000,1,1,0,0,0);
    }

    @Override
    public void setModified_date(LocalDateTime modified_date) {

    }
}
