package edu.ues.aps.storage.model;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.Objects;

@Entity
@Table(name = "FILE")
public class File implements AbstractFile{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(length = 150,nullable = false)
    private String original_name;

    @Column(length = 50, nullable = false, unique = true)
    private String name;

    @Column(length = 250)
    private String path;

    @Column(length = 6, nullable = false)
    private String format;

    @Column(nullable = false)
    private Long size;

    @Column(nullable = false)
    private LocalDateTime creation_date;

    @Column
    private LocalDateTime modified_date;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getOriginal_name() {
        return original_name;
    }

    public void setOriginal_name(String original_name) {
        this.original_name = original_name;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        this.path = path;
    }

    public String getFormat() {
        return format;
    }

    public void setFormat(String format) {
        this.format = format;
    }

    public Long getSize() {
        return size;
    }

    public void setSize(Long size) {
        this.size = size;
    }

    public LocalDateTime getCreation_date() {
        return creation_date;
    }

    public void setCreation_date(LocalDateTime creation_date) {
        this.creation_date = creation_date;
    }

    public LocalDateTime getModified_date() {
        return modified_date;
    }

    public void setModified_date(LocalDateTime modified_date) {
        this.modified_date = modified_date;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        File file = (File) o;
        return Objects.equals(id, file.id) &&
                Objects.equals(name, file.name) &&
                Objects.equals(format, file.format);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, format);
    }

    @Override
    public String toString() {
        return "File{" +
                "id=" + id +
                ", original_name='" + original_name + '\'' +
                ", name='" + name + '\'' +
                ", path='" + path + '\'' +
                ", format='" + format + '\'' +
                ", size=" + size +
                ", creation_date=" + creation_date +
                ", modified_date=" + modified_date +
                '}';
    }
}
