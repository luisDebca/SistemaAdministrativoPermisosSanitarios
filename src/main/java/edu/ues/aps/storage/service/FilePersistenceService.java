package edu.ues.aps.storage.service;

import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.IOException;

public interface FilePersistenceService {

    void save(File file);

    void save(MultipartFile multipartFile) throws IOException;

}
