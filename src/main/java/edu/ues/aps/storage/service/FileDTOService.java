package edu.ues.aps.storage.service;

import edu.ues.aps.storage.dto.AbstractFileDTO;
import edu.ues.aps.storage.dto.EmptyFileDTO;
import edu.ues.aps.storage.repository.FileDTOFinderRepository;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.util.List;

@Service
@Transactional(readOnly = true)
public class FileDTOService implements FileDTOFinderService{

    private final FileDTOFinderRepository finderRepository;

    public FileDTOService(FileDTOFinderRepository finderRepository) {
        this.finderRepository = finderRepository;
    }

    @Override
    public AbstractFileDTO findById(Long id_file) {
        try {
            return finderRepository.findByID(id_file);
        } catch (NoResultException e) {
            return new EmptyFileDTO();
        }
    }

    @Override
    public List<AbstractFileDTO> findAllByNameLike(String pathName) {
        return finderRepository.findAllByNameLike(pathName);
    }

    @Override
    public List<AbstractFileDTO> findAll() {
        return finderRepository.findAll();
    }
}
