package edu.ues.aps.storage.service;

import edu.ues.aps.storage.model.AbstractFile;
import edu.ues.aps.storage.model.File;
import edu.ues.aps.storage.repository.FileFinderRepository;
import edu.ues.aps.storage.repository.FilePersistenceRepository;
import edu.ues.aps.utilities.file.reader.UploadReader;
import edu.ues.aps.utilities.file.writer.UploadWriter;
import org.apache.commons.io.FilenameUtils;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.multipart.MultipartFile;

import java.io.IOException;
import java.security.SecureRandom;
import java.time.LocalDateTime;
import java.util.Random;

@Service
public class FileService implements FilePersistenceService,FileFinderService{

    static final private String ALPHABET = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
    final private Random rng = new SecureRandom();

    private final FilePersistenceRepository persistenceRepository;
    private final FileFinderRepository finderRepository;
    private final UploadWriter uploadWriter;
    private final UploadReader uploadReader;

    public FileService(FilePersistenceRepository persistenceRepository, FileFinderRepository finderRepository, UploadWriter uploadWriter, UploadReader uploadReader) {
        this.persistenceRepository = persistenceRepository;
        this.finderRepository = finderRepository;
        this.uploadWriter = uploadWriter;
        this.uploadReader = uploadReader;
    }

    @Override
    @Transactional(readOnly = true)
    public java.io.File find(Long id_file) {
        AbstractFile file = finderRepository.find(id_file);
        return uploadReader.getFile(file.getName());
    }

    @Override
    @Transactional(readOnly = true)
    public java.io.File find(String original_name) {
        AbstractFile file = finderRepository.find(original_name);
        return uploadReader.getFile(file.getName());
    }

    @Override
    @Transactional
    public void save(java.io.File file) {
        String uuid = randomUUID(25,4,'$');
        uploadWriter.rename(file,uuid);
        saveFileRegister(file,uuid);
    }

    @Override
    @Transactional
    public void save(MultipartFile multipartFile) throws IOException {
        String uuid = randomUUID(25,4,'$');
        uuid = uuid.concat(".").concat(FilenameUtils.getExtension(multipartFile.getOriginalFilename()));
        java.io.File uploadFile = uploadReader.getFile(uuid);
        multipartFile.transferTo(uploadFile);
        saveFileRegister(uploadFile, uuid);
    }

    private void saveFileRegister(java.io.File file, String uuid){
        File fileRegister = new File();
        fileRegister.setCreation_date(LocalDateTime.now());
        fileRegister.setModified_date(LocalDateTime.now());
        fileRegister.setOriginal_name(file.getName());
        fileRegister.setFormat(FilenameUtils.getExtension(file.getName()));
        fileRegister.setPath(file.getAbsolutePath());
        fileRegister.setSize(file.length());
        fileRegister.setName(uuid);
        persistenceRepository.save(fileRegister);
    }

    private String randomUUID(int length, int spacing, char spacerChar){
        StringBuilder sb = new StringBuilder();
        int spacer = 0;
        while(length > 0){
            if(spacer == spacing){
                sb.append(spacerChar);
                spacer = 0;
            }
            length--;
            spacer++;
            sb.append(randomChar());
        }
        return sb.toString();
    }

    private char randomChar(){
        return ALPHABET.charAt(rng.nextInt(ALPHABET.length()));
    }
}
