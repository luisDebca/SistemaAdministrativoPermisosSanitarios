package edu.ues.aps.storage.service;

import edu.ues.aps.storage.dto.AbstractFileDTO;

import java.util.List;

public interface FileDTOFinderService {

    AbstractFileDTO findById(Long id_file);

    List<AbstractFileDTO> findAllByNameLike(String pathName);

    List<AbstractFileDTO> findAll();
}
