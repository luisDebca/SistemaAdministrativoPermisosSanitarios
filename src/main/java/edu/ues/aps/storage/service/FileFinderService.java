package edu.ues.aps.storage.service;

import java.io.File;

public interface FileFinderService {

    File find(Long id_file);

    File find(String original_name);
}
