package edu.ues.aps.storage.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/storage")
public class FileStoragePersistenceController {

    private static final String UPLOAD = "storage_upload";

    @GetMapping("/upload")
    public String showUploadFileForm(){
        return UPLOAD;
    }

    @PostMapping("/upload")
    public String uploadFile(){
        return "redirect:/storage/folder";
    }

    @GetMapping("/{id_file}/delete")
    public String deleteFile(@PathVariable Long id_file){
        return  "redirect:/storage/folder";
    }
}
