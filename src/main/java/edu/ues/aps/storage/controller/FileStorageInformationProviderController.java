package edu.ues.aps.storage.controller;

import edu.ues.aps.storage.service.FileDTOFinderService;
import edu.ues.aps.storage.service.FileFinderService;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import java.io.File;

@Controller
@RequestMapping("/storage")
public class FileStorageInformationProviderController {

    private static final Logger logger = LogManager.getLogger(FileStorageInformationProviderController.class);

    private static final String INDEX = "storage_index";
    private static final String INFORMATION = "storage_info";
    private static final String FOLDER = "storage_folder";

    private final FileDTOFinderService DTOFinderService;
    private final FileFinderService fileFinderService;

    public FileStorageInformationProviderController(FileDTOFinderService DTOFinderService, FileFinderService fileFinderService) {
        this.DTOFinderService = DTOFinderService;
        this.fileFinderService = fileFinderService;
    }

    @GetMapping("/index")
    public String showApsStorageIndex(){
        logger.info("GET:: Show storage file index");
        return INDEX;
    }

    @GetMapping("/{id_file}/info")
    public String showFileInformation(@PathVariable Long id_file, ModelMap model){
        logger.info("GET:: Show file {} information");
        model.addAttribute("file", DTOFinderService.findById(id_file));
        return INFORMATION;
    }

    @GetMapping("/folder")
    public String showAllFiles(ModelMap model){
        logger.info("GET:: Show storage folder");
        model.addAttribute("files", DTOFinderService.findAll());
        return FOLDER;
    }

    @GetMapping("/{id_file}/download")
    public File downloadFile(@PathVariable Long id_file){
        logger.info("GET:: Download file {}",id_file);
        return fileFinderService.find(id_file);
    }


}
