package edu.ues.aps.etl.extract;

import edu.ues.aps.process.base.model.OwnerEntity;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

@Repository
public class ExtractedOwnerPersistenceRepository implements ExtractedObjectsPersistence{

    private final SessionFactory sessionFactory;

    public ExtractedOwnerPersistenceRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public void save(Object o) {
        sessionFactory.getCurrentSession().persist(o);
    }

    @Override
    public Boolean isUnique(String id) {
        return sessionFactory.getCurrentSession().createQuery("select count(owner) " +
                "from OwnerEntity owner " +
                "where owner.name = :name",Long.class)
                .setParameter("name",id)
                .getSingleResult() == 0;
    }

    @Override
    public OwnerEntity get(String id) {
        return sessionFactory.getCurrentSession().createQuery("select owner " +
                "from OwnerEntity owner " +
                "where owner.name = :name",OwnerEntity.class)
                .setParameter("name",id)
                .getSingleResult();
    }
}
