package edu.ues.aps.etl.extract;

import edu.ues.aps.process.area.establishment.model.EstablishmentType;
import edu.ues.aps.process.base.model.Municipality;
import edu.ues.aps.process.base.model.Ucsf;
import org.hibernate.SessionFactory;
import org.springframework.stereotype.Repository;

import javax.persistence.NoResultException;

@Repository
public class RequiredColumnFinderRepository implements RequiredColumnFinder{

    private final SessionFactory sessionFactory;

    public RequiredColumnFinderRepository(SessionFactory sessionFactory) {
        this.sessionFactory = sessionFactory;
    }

    @Override
    public EstablishmentType findEstablishmentType(String type) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select etype " +
                "from EstablishmentType etype " +
                "where etype.type = :path",EstablishmentType.class)
                .setParameter("path",type)
                .getSingleResult();
    }

    @Override
    public Ucsf findUCSF(String ucsf) throws NoResultException{
        return sessionFactory.getCurrentSession().createQuery("select ucsf " +
                "from Ucsf ucsf " +
                "where ucsf.name = :path",Ucsf.class)
                .setParameter("path",ucsf)
                .getSingleResult();
    }

    @Override
    public Municipality findMunicipality(String municipality) throws NoResultException{
        return sessionFactory.getCurrentSession().createQuery("select municipality " +
                "from Municipality municipality " +
                "where municipality.name = :path",Municipality.class)
                .setParameter("path",municipality)
                .getSingleResult();
    }
}
