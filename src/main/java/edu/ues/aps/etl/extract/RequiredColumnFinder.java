package edu.ues.aps.etl.extract;

import edu.ues.aps.process.area.establishment.model.EstablishmentType;
import edu.ues.aps.process.base.model.Municipality;
import edu.ues.aps.process.base.model.Ucsf;

import javax.persistence.NoResultException;

public interface RequiredColumnFinder {

    EstablishmentType findEstablishmentType(String type) throws NoResultException;

    Ucsf findUCSF(String ucsf) throws NoResultException;

    Municipality findMunicipality(String municipality) throws NoResultException;
}
