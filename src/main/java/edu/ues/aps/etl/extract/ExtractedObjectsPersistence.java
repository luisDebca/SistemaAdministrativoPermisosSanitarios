package edu.ues.aps.etl.extract;

public interface ExtractedObjectsPersistence {

    void save(Object t);

    Boolean isUnique(String id);

    Object get(String id);

}
