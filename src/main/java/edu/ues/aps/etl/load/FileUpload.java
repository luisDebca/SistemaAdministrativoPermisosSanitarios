package edu.ues.aps.etl.load;

import edu.ues.aps.etl.transform.CellDataConverter;
import edu.ues.aps.utilities.file.reader.UploadReader;
import edu.ues.aps.utilities.file.writer.UploadWriter;
import org.apache.commons.io.FilenameUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.multipart.MultipartFile;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.IntStream;

@Controller
@RequestMapping("/load")
public class FileUpload implements InitializingBean {

    private static final Logger logger = LogManager.getLogger(FileUpload.class);

    private static final String LOAD = "load_index";
    private static final String PREVIEW = "load_preview";
    private static final String RESULT = "load_result";

    private final UploadWriter writer;
    private final UploadReader reader;
    private final List<ExcelReader> excelReaders;
    private final CellDataConverter dataConverter;
    private Map<Integer, List<CustomCell>> data;
    private Map<String, ExcelReader> readerMap;

    public FileUpload(UploadWriter writer, UploadReader reader, List<ExcelReader> excelReaders, CellDataConverter dataConverter) {
        this.writer = writer;
        this.reader = reader;
        this.excelReaders = excelReaders;
        this.dataConverter = dataConverter;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        readerMap = new HashMap<>();
        for (ExcelReader reader : excelReaders) {
            readerMap.put(reader.applicableFileType(), reader);
        }
    }

    @GetMapping({"/index", "/"})
    public String excelUpload() {
        logger.info("GET:: Show load index.");
        return LOAD;
    }

    @PostMapping("/uploadExcel")
    public String uploadFile(MultipartFile file) throws IOException {
        logger.info("POST:: Load file {}", file.getOriginalFilename());
        InputStream inputStream = file.getInputStream();
        File newFile = writer.upload(file.getOriginalFilename());
        FileOutputStream outputStream = new FileOutputStream(newFile.getAbsolutePath());
        int ch;
        while ((ch = inputStream.read()) != -1) {
            outputStream.write(ch);
        }
        outputStream.flush();
        outputStream.close();
        return String.format("redirect:/load/%s/preview", newFile.getName());
    }

    @GetMapping("{file_name}/preview")
    public String showPreview(@PathVariable String file_name, ModelMap model) throws IOException {
        loadDataFromFile(file_name);
        resolveEmptyRows();
        model.addAttribute("data", data);
        model.addAttribute("file_name", file_name);
        if (!data.isEmpty()) {
            model.addAttribute("table_titles", getTableTitles(data.get(0).size()));
        }
        return PREVIEW;
    }

    @GetMapping("{file_name}/database")
    public String loadIntoDatabase(@PathVariable String file_name, ModelMap model) throws IOException {
        loadDataFromFile(file_name);
        resolveEmptyRows();
        dataConverter.setData(data);
        dataConverter.convert();
        model.addAttribute("file_name", file_name);
        model.addAttribute("summary", dataConverter.getExtractionSummary());
        return RESULT;
    }

    private void loadDataFromFile(String file_name) throws IOException {
        File file = reader.getFile(file_name);
        String extension = FilenameUtils.getExtension(file.getAbsolutePath());
        data = readerMap.get(extension).read(file.getAbsolutePath());
    }

    private void resolveEmptyRows() {
        int maxNrCols = data.values().stream().mapToInt(List::size).max().orElse(0);
        data.values().stream().filter(ls -> ls.size() < maxNrCols).forEach(ls -> {
            IntStream.range(ls.size(), maxNrCols).forEach(i -> ls.add(new CustomCell("")));
        });
    }

    private String[] getTableTitles(int size) {
        String[] alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZ".split("");
        String[] titles = new String[size];
        int groupLetter = 0;
        for (int i = 0; i < size; i++) {
            if (i < alphabet.length) {
                titles[i] = alphabet[i];
                if (i + 1 == alphabet.length)
                    groupLetter++;
            } else {
                titles[i] = groupLetter + alphabet[i - alphabet.length];
            }
        }
        return titles;
    }

}
