package edu.ues.aps.etl.load;

import org.apache.poi.xssf.usermodel.*;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class XLSXFileReader implements ExcelReader {

    @Override
    public String applicableFileType() {
        return "xlsx";
    }

    @Override
    public Map<Integer, List<CustomCell>> read(String absolutePath) throws IOException {
        FileInputStream file = new FileInputStream(new File(absolutePath));
        Map<Integer, List<CustomCell>> data = new HashMap<>();
        try (XSSFWorkbook workbook = new XSSFWorkbook(file)) {
            XSSFSheet sheet = workbook.getSheetAt(0);
            for (int index = sheet.getFirstRowNum(); index <= sheet.getLastRowNum(); index++) {
                XSSFRow row = sheet.getRow(index);
                data.put(index, new ArrayList<>());
                if (row != null) {
                    for (int cellNumber = 0; cellNumber < row.getLastCellNum(); cellNumber++) {
                        XSSFCell cell = row.getCell(cellNumber);
                        if (cell != null) {
                            XSSFCellStyle cellStyle = cell.getCellStyle();
                            CustomCell customCell = new CustomCell();
                            XSSFColor bgColor = cellStyle.getFillForegroundColorColor();
                            if (bgColor != null) {
                                customCell.setBgColor(getUnsignedRGBColor(bgColor.getRGB()));
                            }
                            XSSFFont font = cellStyle.getFont();
                            customCell.setTextSize(font.getFontHeightInPoints() + "");
                            if (font.getBold()) {
                                customCell.setTextWeight("bold");
                            }
                            XSSFColor textColor = font.getXSSFColor();
                            if (textColor != null) {
                                customCell.setText_color(getUnsignedRGBColor(textColor.getRGB()));
                            }
                            customCell.setContent(readCellContent(cell));
                            data.get(index).add(customCell);
                        } else {
                            data.get(index).add(new CustomCell(""));
                        }
                    }
                }
            }
        }
        return data;
    }

    private String getUnsignedRGBColor(byte[] rgbColor) {
        return "rgb(" +
                (rgbColor[0] < 0 ? (rgbColor[0] + 0xff) : rgbColor[0]) + "," +
                (rgbColor[1] < 0 ? (rgbColor[1] + 0xff) : rgbColor[1]) + "," +
                (rgbColor[2] < 0 ? (rgbColor[2] + 0xff) : rgbColor[2]) + ")";
    }
}
