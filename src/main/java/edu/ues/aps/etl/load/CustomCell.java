package edu.ues.aps.etl.load;

public class CustomCell {

    private String content;
    private String text_color;
    private String bgColor;
    private String textSize;
    private String textWeight;

    public CustomCell() {
    }

    public CustomCell(String content) {
        this.content = content;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public String getText_color() {
        return text_color;
    }

    public void setText_color(String text_color) {
        this.text_color = text_color;
    }

    public String getBgColor() {
        return bgColor;
    }

    public void setBgColor(String bgColor) {
        this.bgColor = bgColor;
    }

    public String getTextSize() {
        return textSize;
    }

    public void setTextSize(String textSize) {
        this.textSize = textSize;
    }

    public String getTextWeight() {
        return textWeight;
    }

    public void setTextWeight(String textWeight) {
        this.textWeight = textWeight;
    }

    @Override
    public String toString() {
        return "CustomCell{" +
                "content='" + content + '\'' +
                ", text_color='" + text_color + '\'' +
                ", bgColor='" + bgColor + '\'' +
                ", textSize='" + textSize + '\'' +
                ", textWeight='" + textWeight + '\'' +
                '}';
    }
}
