package edu.ues.aps.etl.load;

import org.springframework.stereotype.Component;

import java.util.List;
import java.util.Map;

@Component
public class ODSFileReader implements ExcelReader{

    @Override
    public String applicableFileType() {
        return ".ods";
    }

    @Override
    public Map<Integer, List<CustomCell>> read(String absolutePath) {
        return null;
    }
}
