package edu.ues.aps.etl.load;

import org.apache.poi.hssf.usermodel.*;
import org.apache.poi.hssf.util.HSSFColor;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class XLSFileReader implements ExcelReader {

    @Override
    public String applicableFileType() {
        return "xls";
    }

    @Override
    public Map<Integer, List<CustomCell>> read(String absolutePath) throws IOException {
        FileInputStream file = new FileInputStream(new File(absolutePath));
        Map<Integer, List<CustomCell>> data = new HashMap<>();
        try (HSSFWorkbook workbook = new HSSFWorkbook(file)) {
            HSSFSheet sheet = workbook.getSheetAt(0);
            for (int index = sheet.getFirstRowNum(); index <= sheet.getLastRowNum(); index++) {
                HSSFRow row = sheet.getRow(index);
                data.put(index, new ArrayList<>());
                if (row != null) {
                    for (int cellNumber = 0; cellNumber < row.getLastCellNum(); cellNumber++) {
                        HSSFCell cell = row.getCell(cellNumber);
                        if (cell != null) {
                            CustomCell customCell = new CustomCell();
                            HSSFCellStyle cellStyle = cell.getCellStyle();
                            HSSFColor bgColor = cellStyle.getFillForegroundColorColor();
                            if (bgColor != null) {
                                customCell.setBgColor(getUnsignedRGBColor(bgColor.getTriplet()));
                            }
                            HSSFFont font = cellStyle.getFont(workbook);
                            customCell.setTextSize(font.getFontHeightInPoints() + "");
                            if (font.getBold()) {
                                customCell.setTextWeight("bold");
                            }
                            HSSFColor textColor = font.getHSSFColor(workbook);
                            if (textColor != null) {
                                customCell.setText_color(getUnsignedRGBColor(textColor.getTriplet()));
                            }
                            customCell.setContent(readCellContent(cell));
                            data.get(index).add(customCell);
                        } else {
                            data.get(index).add(new CustomCell(""));
                        }
                    }
                }
            }
        }
        return data;
    }

    private String getUnsignedRGBColor(short[] rgbColor) {
        return "rgb(" +
                rgbColor[0] + "," +
                rgbColor[1] + "," +
                rgbColor[2] + ")";
    }
}
