package edu.ues.aps.etl.transform;

import edu.ues.aps.etl.load.CustomCell;
import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.area.vehicle.model.Vehicle;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.OwnerEntity;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.InitializingBean;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Component
public class CellConverter implements InitializingBean, CellDataConverter {

    private static final Logger logger = LogManager.getLogger(CellConverter.class);

    private final List<ModelConverter> converters;
    private final ExtractionSummary extractionSummary;
    private Map<Class, ModelConverter> converterMap = new HashMap<>();

    private Class rootClassName;
    private Integer columnSize;
    private Integer rowSize;

    private Map<Integer, List<CustomCell>> data;

    public CellConverter(List<ModelConverter> converters, ExtractionSummary extractionSummary) {
        this.converters = converters;
        this.extractionSummary = extractionSummary;
    }

    @Override
    public void afterPropertiesSet() throws Exception {
        for (ModelConverter converter : converters) {
            converterMap.put(converter.appliesTo(), converter);
        }
    }

    @Override
    public void convert() {
        extractionSummary.reset();
        extractionSummary.addInfoMessage("load.start", "");
        setTableSizes();
        setRootClassName(UtilCellContent.getTableName(getCellValue(0, 0)));
        for (int row = 1; row < rowSize; row++) {
            getConverter().setUp();
            extractRowColumnValues(row);
            persistentExtractedObject();
            extractionSummary.plusCompletedRowCount();
        }
        extractionSummary.setFinishedCorrectly(true);
        extractionSummary.addInfoMessage("load.end", "");
    }

    private void extractRowColumnValues(int row) {
        for (int column = 0; column < columnSize; column++) {
            logDebuggerCellLocation(row, column);
            checkCellValue(row, column);
        }
    }

    private void checkCellValue(int row, int column) {
        try {
            if (isCellNotEmpty(column, row))
                getConverter().setProperty(UtilCellContent.getColumnName(getCellValue(column, 0)), getCellValue(column, row));
        } catch (NotValidColumnNameException e) {
            extractionSummary.addErrorMessage("load.exception", e.getMessage());
            removeInvalidColumn(column);
            logger.error(e.toString());
        }
    }

    private void removeInvalidColumn(int column) {
        for (int row = 0; row < rowSize; row++) {
            data.get(row).remove(column);
        }
        columnSize--;
    }

    @Override
    public void setData(Map<Integer, List<CustomCell>> data) {
        this.data = data;
    }

    @Override
    public ExtractionSummary getExtractionSummary() {
        return extractionSummary;
    }

    private ModelConverter getConverter() {
        return converterMap.get(rootClassName);
    }

    private void persistentExtractedObject() {
        if (getConverter().validate()) {
            getConverter().save();
            extractionSummary.plusSavedRowsCount();
        } else {
            getConverter().update();
            extractionSummary.plusUpdatedRowsCount();
        }
    }

    private void setRootClassName(String rootName) throws NotValidRootClassNameException {
        extractionSummary.addInfoMessage("load.rootClass", rootName.toUpperCase());
        switch (rootName.toUpperCase()) {
            case "ESTABLISHMENT":
                rootClassName = Establishment.class;
                break;
            case "OWNER":
                rootClassName = OwnerEntity.class;
                break;
            case "CASEFILE":
                rootClassName = Casefile.class;
                break;
            case "VEHICLE":
                rootClassName = Vehicle.class;
                break;
            default:
                extractionSummary.addErrorMessage("load.exception", String.format("Column name %s invalid", rootName.toUpperCase()));
                throw new NotValidRootClassNameException(String.format("Column name %s not valid.", rootName.toUpperCase()));
        }
    }

    private void setTableSizes() {
        rowSize = data.size();
        columnSize = data.get(0).size();
        extractionSummary.setOriginalRowCount(rowSize - 1);
        extractionSummary.setOriginalColumnCount(columnSize);
    }

    private String getCellValue(int column, int row) {
        return data.get(row).get(column).getContent();
    }

    private Boolean isCellNotEmpty(int column, int row) {
        boolean isEmpty = getCellValue(column, row).isEmpty();
        if (isEmpty)
            extractionSummary.plusEmptyCellCount();
        return !isEmpty;
    }

    private void logDebuggerCellLocation(int row, int column) {
        logger.debug("Row[{}]], Column[{}]", row, column);
        logger.debug("Column[{}], Value[{}]", UtilCellContent.getColumnName(getCellValue(column, 0)), getCellValue(column, row));
    }

}
