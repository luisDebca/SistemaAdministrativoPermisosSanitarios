package edu.ues.aps.etl.transform;

import edu.ues.aps.etl.extract.RequiredColumnFinder;
import edu.ues.aps.process.area.establishment.model.CasefileEstablishment;
import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.base.model.Address;
import edu.ues.aps.process.base.model.CasefileSection;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;

@Service
@Transactional
public class EstablishmentConverter implements ModelConverter {

    private final ModelConverter converter;
    private final RequiredColumnFinder requiredColumnFinder;
    private Establishment establishment;

    public EstablishmentConverter(@Qualifier("caseFileConverter") ModelConverter converter, RequiredColumnFinder requiredColumnFinder) {
        this.converter = converter;
        this.requiredColumnFinder = requiredColumnFinder;
    }

    @Override
    public Class appliesTo() {
        return Establishment.class;
    }

    @Override
    public void setUp() {
        establishment = new Establishment();
        establishment.setActive(true);
        establishment.setMaleEmployeeCount(0);
        establishment.setFemaleEmployeeCount(0);
        converter.setUp();
        converter.setProperty("section", CasefileSection.ESTABLISHMENT.name());
    }

    @Override
    public Establishment getObject() {
        establishment.addCasefile((CasefileEstablishment) converter.getObject());
        return establishment;
    }

    @Override
    public void setProperty(String property, String value) throws NotValidColumnNameException {
        if (UtilCellContent.getColumnLevel(property) == 0) {
            switch (property) {
                case "name":
                    establishment.setName(value);
                    break;
                case "NIT":
                case "nit":
                    establishment.setNIT(value);
                    break;
                case "telephone":
                case "phone":
                    establishment.setTelephone(value);
                    break;
                case "telephone_extra":
                    establishment.setTelephone_extra(value);
                    break;
                case "operations":
                    establishment.setOperations(value);
                    break;
                case "capital":
                    establishment.setCapital(Double.valueOf(value));
                    break;
                case "commercial_register":
                case "cr":
                    establishment.setCommercial_register(value);
                    break;
                case "type_detail":
                    establishment.setType_detail(value);
                    break;
                case "female_employee_count":
                    establishment.setFemaleEmployeeCount(Integer.valueOf(value));
                    break;
                case "male_employee_count":
                    establishment.setMaleEmployeeCount(Integer.valueOf(value));
                    break;
                case "is_active":
                    establishment.setActive(Boolean.valueOf(value));
                    break;
                case "address":
                    setAddressDetails(value);
                    break;
                case "type":
                    setEstablishmentType(value);
                    break;
                default:
                    throw new NotValidColumnNameException(String.format("Column name %s invalid.", property));
            }
        } else {
            if ("casefile".equalsIgnoreCase(UtilCellContent.getTableName(property))) {
                converter.setProperty(UtilCellContent.getColumnName(property), value);
            } else {
                throw new NotValidColumnNameException(String.format("Column name %s invalid.", UtilCellContent.getTableName(property)));
            }
        }
    }

    @Override
    public String getId() {
        return establishment.getName();
    }

    @Override
    public Boolean validate() {
        return false;
    }

    @Override
    public void save() {

    }

    @Override
    public void update() {

    }

    private void setEstablishmentType(String path) {
        try {
            establishment.setType(requiredColumnFinder.findEstablishmentType(path));
        } catch (NoResultException e) {
            establishment.setType(null);
        }
    }

    private void setAddressDetails(String fullAddress) {
        try {
            Address address = new Address();
            address.setDetails(CellValueFormatting.extractAddressDetails(fullAddress));
            address.setMunicipality(requiredColumnFinder.findMunicipality(CellValueFormatting.extractMunicipality(fullAddress)));
            establishment.setAddress(address);
        } catch (Exception e) {
            Address address = new Address();
            address.setDetails(fullAddress);
            establishment.setAddress(address);
        }
    }
}
