package edu.ues.aps.etl.transform;

import edu.ues.aps.etl.load.CustomCell;

import java.util.List;
import java.util.Map;

public interface CellDataConverter {

    void convert();

    void setData(Map<Integer, List<CustomCell>> data);

    ExtractionSummary getExtractionSummary();

}
