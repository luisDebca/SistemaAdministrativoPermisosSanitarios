package edu.ues.aps.etl.transform;

public class UtilCellContent {

    public static String getTableName(String columnName) {
        return columnName.split("\\.", -1)[0];
    }

    public static String getColumnName(String columnName) {
        return columnName.split("\\.", 2)[1];
    }

    public static Integer getColumnLevel(String columnName) {
        return columnName.split("\\.", -1).length - 1;
    }
}
