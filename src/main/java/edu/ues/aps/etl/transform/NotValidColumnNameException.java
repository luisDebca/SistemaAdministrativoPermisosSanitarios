package edu.ues.aps.etl.transform;

public class NotValidColumnNameException extends RuntimeException {

    public NotValidColumnNameException() {
    }

    public NotValidColumnNameException(String s) {
        super(s);
    }
}
