package edu.ues.aps.etl.transform;

import edu.ues.aps.process.tracing.base.model.Inspection;
import edu.ues.aps.process.tracing.base.model.InspectionResultType;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class InspectionConverter implements ModelConverter{

    private Inspection inspection;

    @Override
    public Class appliesTo() {
        return Inspection.class;
    }

    @Override
    public void setUp() {
        inspection = new Inspection();
        inspection.setInspected_on(LocalDateTime.now());
        inspection.setResult_type(InspectionResultType.INVALID);
    }

    @Override
    public Object getObject() {
        return inspection;
    }

    @Override
    public void setProperty(String propertyName, String value) throws NotValidColumnNameException{
        switch (propertyName){
            case "observation":
                inspection.setObservation(value);
                break;
            case "extension_received":
                inspection.setExtension_received(Integer.valueOf(value));
                break;
            case "memorandum_number":
            case "memo":
                inspection.setMemorandum_number(value);
                break;
            case "inspected_on":
                inspection.setInspected_on(CellValueFormatting.getDateTime(value).atStartOfDay());
                break;
            case "result":
            case "inspection_result_type":
                inspection.setResult_type(InspectionResultType.valueOf(value));
                break;
            case "score":
                inspection.setScore(Double.valueOf(value));
                break;
            case "sibasi_received":
                inspection.setSibasi_received(CellValueFormatting.getDateTime(value).atStartOfDay());
                break;
            case "technical_charge":
                inspection.setTechnical_charge(value);
                break;
            case "technical_name":
                inspection.setTechnical_name(value);
                break;
                default:
                    throw new NotValidColumnNameException(String.format("Column name %s invalid.", propertyName));
        }
    }

    @Override
    public String getId() {
        return inspection.getMemorandum_number();
    }

    @Override
    public Boolean validate() {
        return null;
    }

    @Override
    public void save() {

    }

    @Override
    public void update() {

    }
}
