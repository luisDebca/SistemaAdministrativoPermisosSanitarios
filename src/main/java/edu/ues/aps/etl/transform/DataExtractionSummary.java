package edu.ues.aps.etl.transform;

import org.springframework.context.MessageSource;
import org.springframework.stereotype.Component;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Locale;
import java.util.Map;
import java.util.stream.Collector;
import java.util.stream.Collectors;

@Component
public class DataExtractionSummary implements ExtractionSummary {

    private final MessageSource messageSource;

    private static final String ERROR = "danger";
    private static final String WARNING = "warning";
    private static final String INFO = "info";

    private Integer messageCount;

    private Integer originalRowCount;
    private Integer completedRowCount;
    private Integer withErrorRowCount;
    private Integer withWarningRowCount;
    private Integer originalColumnCount;
    private Integer savedRowsCount;
    private Integer updatedRowsCount;
    private Integer invalidColumnCount;
    private Integer emptyCellCount;
    private Boolean finishedCorrectly;

    private Map<String,String> messages = new HashMap<>();
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");

    public DataExtractionSummary(MessageSource messageSource) {
        this.messageSource = messageSource;
    }

    @Override
    public void reset() {
        messageCount = 0;
        originalRowCount = 0;
        completedRowCount = 0;
        withErrorRowCount = 0;
        withWarningRowCount = 0;
        originalColumnCount = 0;
        savedRowsCount = 0;
        updatedRowsCount = 0;
        invalidColumnCount = 0;
        emptyCellCount = 0;
        messages.clear();
    }

    @Override
    public Integer getOriginalRowCount() {
        return originalRowCount;
    }

    @Override
    public Integer getCompletedRowCount() {
        return completedRowCount;
    }

    @Override
    public Integer getWithErrorRowCount() {
        return withErrorRowCount;
    }

    @Override
    public Integer getWithWarningRowCount() {
        return withWarningRowCount;
    }

    @Override
    public Integer getOriginalColumnCount() {
        return originalColumnCount;
    }

    @Override
    public Integer getSavedRowsCount() {
        return savedRowsCount;
    }

    @Override
    public Integer getUpdatedRowsCount() {
        return updatedRowsCount;
    }

    @Override
    public Integer getInvalidColumnCount() {
        return invalidColumnCount;
    }

    @Override
    public Integer getEmptyCellCount() {
        return emptyCellCount;
    }

    @Override
    public Boolean getFinishedCorrectly() {
        return finishedCorrectly;
    }

    @Override
    public Map<String, String> getMessages() {
        return messages.entrySet().stream()
                .sorted(Map.Entry.comparingByKey())
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue,(oldValue, newValue) -> oldValue, LinkedHashMap::new));
    }

    @Override
    public void setOriginalRowCount(Integer originalRowCount) {
        this.originalRowCount = originalRowCount;
    }

    @Override
    public void setOriginalColumnCount(Integer originalColumnCount) {
        this.originalColumnCount = originalColumnCount;
    }

    @Override
    public void plusCompletedRowCount(){
        completedRowCount++;
    }

    @Override
    public void plusWithErrorRowCount(){
        withErrorRowCount++;
    }

    @Override
    public void plusWithWarningRowCount(){
        withWarningRowCount++;
    }

    @Override
    public void plusSavedRowsCount(){
        savedRowsCount++;
    }

    @Override
    public void plusUpdatedRowsCount(){
        updatedRowsCount++;
    }

    @Override
    public void plusInvalidColumnCount(){
        updatedRowsCount++;
    }

    @Override
    public void plusEmptyCellCount(){
        emptyCellCount++;
    }

    @Override
    public void setFinishedCorrectly(Boolean finishedCorrectly) {
        this.finishedCorrectly = finishedCorrectly;
    }

    @Override
    public void addInfoMessage(String pattern, String message){
        messageCount++;
        messages.put(String.format("%d-%s", messageCount, INFO),
                messageSource.getMessage(pattern,new String[]{LocalDateTime.now().format(formatter),message}, Locale.getDefault()));
    }

    @Override
    public void addWarningMessage(String pattern, String message){
        messageCount++;
        messages.put(String.format("%d-%s", messageCount, WARNING),
                messageSource.getMessage(pattern,new String[]{LocalDateTime.now().format(formatter),message}, Locale.getDefault()));
    }

    @Override
    public void addErrorMessage(String pattern, String message){
        messageCount++;
        messages.put(String.format("%d-%s", messageCount, ERROR),
                messageSource.getMessage(pattern,new String[]{LocalDateTime.now().format(formatter),message}, Locale.getDefault()));
    }
}
