package edu.ues.aps.etl.transform;

import edu.ues.aps.etl.extract.RequiredColumnFinder;
import edu.ues.aps.process.area.establishment.model.CasefileEstablishment;
import edu.ues.aps.process.area.vehicle.model.CasefileVehicle;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.base.model.CasefileSection;
import edu.ues.aps.process.base.model.CasefileStatusType;
import edu.ues.aps.process.base.model.CertificationType;
import edu.ues.aps.process.resolution.base.model.Resolution;
import edu.ues.aps.process.tracing.base.model.Tracing;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;

@Service
@Transactional
public class CaseFileConverter implements ModelConverter {

    private final ModelConverter tracingConverter;
    private final ModelConverter resolutionConverter;
    private final RequiredColumnFinder requiredColumnFinder;
    private Casefile caseFile;
    private CasefileEstablishment ce;
    private CasefileVehicle cv;

    public CaseFileConverter(RequiredColumnFinder requiredColumnFinder, @Qualifier("tracingConverter") ModelConverter tracingConverter, @Qualifier("resolutionConverter") ModelConverter resolutionConverter) {
        this.requiredColumnFinder = requiredColumnFinder;
        this.tracingConverter = tracingConverter;
        this.resolutionConverter = resolutionConverter;
    }

    @Override
    public Class appliesTo() {
        return Casefile.class;
    }

    @Override
    public void setUp() {
        caseFile = new Casefile();
        caseFile.setApply_payment(true);
        caseFile.setCreation_date(LocalDateTime.now());
        caseFile.setStatus(CasefileStatusType.VALID);
        caseFile.setCertification_type(CertificationType.FIRST);
        tracingConverter.setUp();
        resolutionConverter.setUp();
    }

    @Override
    public Casefile getObject() {
        addResolution();
        addTracing();
        validateCaseFileType();
        return caseFile.getSection().equals(CasefileSection.ESTABLISHMENT) ? ce : cv;
    }

    @Override
    public void setProperty(String propertyName, String value) throws NotValidColumnNameException{
        if (UtilCellContent.getColumnLevel(propertyName) == 0) {
            switch (propertyName) {
                case "request_folder":
                case "folder_number":
                case "folder":
                    caseFile.setRequest_folder(CellValueFormatting.getFolderNumber(value));
                    caseFile.setRequest_year(CellValueFormatting.getRequestYear(value));
                    break;
                case "request_number":
                case "request_code":
                    caseFile.setRequest_number(CellValueFormatting.getRequestNumber(value));
                    caseFile.setRequest_code(CellValueFormatting.getRequestCode(value));
                    caseFile.setRequest_year(CellValueFormatting.getRequestYear(value));
                    break;
                case "certification_duration_in_years":
                case "duration":
                    caseFile.setCertification_duration_in_years(Integer.valueOf(value));
                    break;
                case "creation_date":
                    caseFile.setCreation_date(CellValueFormatting.getDateTime(value).atStartOfDay());
                    break;
                case "ucsf":
                case "UCSF":
                    setUCSF(value);
                    break;
                case "section":
                    caseFile.setSection(CasefileSection.valueOf(value));
                    break;
                default:
                    throw new NotValidColumnNameException(String.format("Column name %s invalid.", propertyName));
            }
        } else {
            switch (UtilCellContent.getTableName(propertyName)) {
                case "requirement_control_document":
                    break;
                case "legal_review":
                    break;
                case "payment_procedure":
                    break;
                case "tracing":
                    tracingConverter.setProperty(UtilCellContent.getColumnName(propertyName), value);
                    break;
                case "resolution":
                    resolutionConverter.setProperty(UtilCellContent.getColumnName(propertyName), value);
                    break;
                default:
                    throw new NotValidColumnNameException(String.format("Column name %s invalid.", propertyName));
            }
        }
    }

    @Override
    public String getId() {
        if (caseFile.getRequest_number() == null) {
            return String.format("%s-%s", caseFile.getRequest_folder(), caseFile.getRequest_year());
        } else {
            return String.format("06%s%s-%s", caseFile.getRequest_code(), caseFile.getRequest_number(), caseFile.getRequest_year());
        }
    }

    @Override
    public Boolean validate() {
        return false;
    }

    @Override
    public void save() {

    }

    @Override
    public void update() {

    }

    private void validateCaseFileType() {
        if (caseFile.getSection().equals(CasefileSection.ESTABLISHMENT)) {
            ce = new CasefileEstablishment();
            ce.setSection(CasefileSection.ESTABLISHMENT);
            ce.setCertification_duration_in_years(3);
            copyCaseFileValues(ce);
        } else {
            cv = new CasefileVehicle();
            cv.setSection(CasefileSection.VEHICLE);
            cv.setCertification_duration_in_years(1);
            copyCaseFileValues(cv);
        }
    }

    private void copyCaseFileValues(Casefile copy) {
        copy.setStatus(caseFile.getStatus());
        copy.setCreation_date(caseFile.getCreation_date());
        copy.setApply_payment(caseFile.getApply_payment());
        copy.setCertification_start_on(caseFile.getCertification_start_on());
        copy.setCertification_type(caseFile.getCertification_type());
        copy.setControlDocument(caseFile.getControlDocument());
        copy.setLegalReviewls(caseFile.getLegalReviewls());
        copy.setMemorandums(caseFile.getMemorandums());
        copy.setPaymentProcedure(caseFile.getPaymentProcedure());
        copy.setRequest_code(caseFile.getRequest_code());
        copy.setRequest_folder(caseFile.getRequest_folder());
        copy.setRequest_number(caseFile.getRequest_number());
        copy.setRequest_year(caseFile.getRequest_year());
        copy.setResolution(caseFile.getResolution());
        copy.setTracing(caseFile.getTracing());
        copy.setUcsf(caseFile.getUcsf());
        copy.setUcsf_delivery(caseFile.getUcsf_delivery());
        copy.setValidate(caseFile.getValidate());
    }

    private void setUCSF(String path) {
        try {
            caseFile.setUcsf(requiredColumnFinder.findUCSF(path));
        } catch (NoResultException e) {
            caseFile.setUcsf(null);
        }
    }

    private void addResolution() {
        Resolution resolution = (Resolution) resolutionConverter.getObject();
        if (resolution.getResolution_number() != null) {
            caseFile.addResolution(resolution);
        }
    }

    private void addTracing() {
        Tracing tracing = (Tracing) tracingConverter.getObject();
        if (tracing.getUcsf_received_on() != null) {
            caseFile.addTracing(tracing);
        }
    }
}
