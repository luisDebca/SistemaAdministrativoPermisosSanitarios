package edu.ues.aps.etl.transform;

import edu.ues.aps.process.area.vehicle.model.TransportedContentType;
import edu.ues.aps.process.area.vehicle.model.Vehicle;
import edu.ues.aps.process.base.model.CasefileSection;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class VehicleConverter implements ModelConverter {

    private final ModelConverter converter;
    private Vehicle vehicle;

    public VehicleConverter(@Qualifier("caseFileConverter") ModelConverter converter) {
        this.converter = converter;
    }

    @Override
    public Class appliesTo() {
        return Vehicle.class;
    }

    @Override
    public void setUp() {
        vehicle = new Vehicle();
        vehicle.setActive(true);
        converter.setUp();
        converter.setProperty("section", CasefileSection.VEHICLE.name());
    }

    @Override
    public Object getObject() {
        return vehicle;
    }

    @Override
    public void setProperty(String propertyName, String value) throws NotValidColumnNameException {
        if (UtilCellContent.getColumnLevel(propertyName) == 0) {
            switch (propertyName) {
                case "license":
                    vehicle.setLicense(value);
                    break;
                case "type":
                    vehicle.setVehicle_type(value);
                    break;
                case "content":
                case "transported_content":
                    if (value.equalsIgnoreCase("TAP")) {
                        vehicle.setTransported_content(TransportedContentType.PERISHABLE);
                    } else if (value.equalsIgnoreCase("TANP")) {
                        vehicle.setTransported_content(TransportedContentType.NON_PERISHABLE);
                    } else {
                        vehicle.setTransported_content(TransportedContentType.INVALID);
                    }
                    break;
                case "employee":
                case "employee_number":
                case "employees_number":
                    vehicle.setEmployees_number(Integer.valueOf(value));
                    break;
                default:
                    throw new NotValidColumnNameException(String.format("Column name %s invalid.", propertyName));
            }
        } else {
            if ("casefile".equalsIgnoreCase(UtilCellContent.getTableName(propertyName))) {
                converter.setProperty(UtilCellContent.getColumnName(propertyName), value);
            } else {
                throw new NotValidColumnNameException(String.format("Column name %s invalid.", UtilCellContent.getTableName(propertyName)));
            }
        }
    }

    @Override
    public String getId() {
        return vehicle.getLicense();
    }

    @Override
    public Boolean validate() {
        return false;
    }

    @Override
    public void save() {

    }

    @Override
    public void update() {

    }
}
