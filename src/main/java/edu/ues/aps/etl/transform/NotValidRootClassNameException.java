package edu.ues.aps.etl.transform;

public class NotValidRootClassNameException extends RuntimeException {

    public NotValidRootClassNameException() {
    }

    public NotValidRootClassNameException(String s) {
        super(s);
    }
}
