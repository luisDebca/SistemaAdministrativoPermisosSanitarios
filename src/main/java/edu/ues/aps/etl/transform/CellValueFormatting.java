package edu.ues.aps.etl.transform;

import java.time.LocalDate;

public class CellValueFormatting {

    public static String getFolderNumber(String folder_number) {
        String folder;
        if (folder_number.contains("-")) {
            folder = folder_number.split("-", -1)[0];
        } else {
            folder = folder_number.substring(0, folder_number.length() - 2);
        }
        return folder;
    }

    public static String getRequestCode(String request_number) {
        return request_number.replaceAll("[^A-Za-z]+", "");
    }

    public static String getRequestNumber(String request_number) {
        return request_number.split("-", -1)[0].substring(2).replaceAll("\\D+", "");
    }

    public static String getRequestYear(String request_number) {
        return request_number.split("-", -1)[1];
    }

    public static LocalDate getDateTime(String datetime) {
        String[] values = {};
        if (datetime.contains("/")) {
            values = datetime.split("/");
        }else if (datetime.contains("-")) {
            values = datetime.split("-");
        }
        return LocalDate.of(getYear(values[2]),getMonth(values[1]),getDay(values[0]));
    }

    private static int getDay(String value){
        int day = Integer.valueOf(value);
        return isValidDay(day) ? day : 1;
    }

    private static int getMonth(String value){
        int month = Integer.valueOf(value);
        return isValidMonth(month) ? month : 1;
    }

    private static int getYear(String value){
        int year = Integer.valueOf(value);
        if(year < 99){
            year += 2000;
        }
        return isValidYear(year) ? year : 2000;
    }

    private static Boolean isValidDay(Integer day){
        return day > 0 && day < 32;
    }

    private static Boolean isValidMonth(Integer month){
        return month > 0 && month <  13;
    }

    private static Boolean isValidYear(Integer year){
        return year > 1990 && year < 2099;
    }

    public static String extractAddressDetails(String fullAddress){
        String details = fullAddress.split(extractMunicipality(fullAddress), -1)[0];
        return removeEndCharacter(details);
    }

    public static String extractMunicipality(String fullAddress){
        if(fullAddress.contains(",")){
            String[] split = fullAddress.split(",",-1);
            return removeEndCharacter(removeStartCharacter(split[split.length - 2]));
        }
        return "";
    }

    private static String removeEndCharacter(String str){
        while (str.substring(str.length() -1).equals(",") ||
                str.substring(str.length() -1).equals(".") ||
                str.substring(str.length() -1).equals(" ")){
            str = str.substring(0, str.length() -1);
        }
        return str;
    }

    private static String removeStartCharacter(String str){
        while (str.substring(0,1).equals(",") ||
                str.substring(0,1).equals(".") ||
                str.substring(0,1).equals(" ")){
            str = str.substring(1);
        }
        return str;
    }
}
