package edu.ues.aps.etl.transform;

public interface ModelConverter {

    Class appliesTo();

    void setUp();

    Object getObject();

    void setProperty(String propertyName, String value) throws NotValidColumnNameException;

    String getId();

    Boolean validate();

    void save();

    void update();

}
