package edu.ues.aps.etl.transform;

import edu.ues.aps.etl.extract.ExtractedObjectsPersistence;
import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.area.vehicle.model.Vehicle;
import edu.ues.aps.process.base.model.LegalEntity;
import edu.ues.aps.process.base.model.NaturalEntity;
import edu.ues.aps.process.base.model.OwnerEntity;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

@Service
@Transactional
public class OwnerConverter implements ModelConverter {

    private final ExtractedObjectsPersistence ownerPersistence;

    private OwnerEntity owner;
    private LegalEntity legalEntity;
    private NaturalEntity naturalEntity;

    private final ModelConverter establishmentModelConverter;
    private final ModelConverter vehicleModelConverter;

    private Boolean isLegalEntity;

    public OwnerConverter(ExtractedObjectsPersistence ownerPersistence, @Qualifier("establishmentConverter") ModelConverter establishmentModelConverter, @Qualifier("vehicleConverter") ModelConverter vehicleModelConverter) {
        this.ownerPersistence = ownerPersistence;
        this.establishmentModelConverter = establishmentModelConverter;
        this.vehicleModelConverter = vehicleModelConverter;
    }

    @Override
    public Class appliesTo() {
        return OwnerEntity.class;
    }

    @Override
    public void setUp() {
        owner = new OwnerEntity();
        isLegalEntity = false;
        establishmentModelConverter.setUp();
        vehicleModelConverter.setUp();
    }

    @Override
    public OwnerEntity getObject() {
        addEstablishment();
        addVehicle();
        validateOwnerType();
        return isLegalEntity ? legalEntity : naturalEntity;
    }

    @Override
    public void setProperty(String propertyName, String value) throws NotValidColumnNameException{
        if (UtilCellContent.getColumnLevel(propertyName) == 0) {
            switch (propertyName) {
                case "name":
                    owner.setName(value);
                    break;
                case "nit":
                case "NIT":
                    owner.setNit(value);
                    break;
                case "telephone":
                    owner.setTelephone(value);
                    break;
                case "email":
                    owner.setEmail(value);
                    break;
                case "fax":
                case "FAX":
                    owner.setFAX(value);
                    break;
                case "establishment.name":
                    break;
                    default:
                        throw new NotValidColumnNameException(String.format("Column name %s invalid.", propertyName));

            }
        } else {
            switch (UtilCellContent.getTableName(propertyName)) {
                case "establishment":
                    establishmentModelConverter.setProperty(UtilCellContent.getColumnName(propertyName), value);
                    break;
                case "vehicle":
                    vehicleModelConverter.setProperty(UtilCellContent.getColumnName(propertyName),value);
                    break;
                    default:
                        throw new NotValidColumnNameException(String.format("Column name %s invalid", UtilCellContent.getTableName(propertyName)));

            }
        }
    }

    @Override
    public String getId() {
        return owner.getName();
    }

    @Override
    public Boolean validate() {
        return ownerPersistence.isUnique(getId());
    }

    @Override
    public void save() {
        ownerPersistence.save(getObject());
    }

    @Override
    public void update() {
        copyOwnerValues((OwnerEntity) ownerPersistence.get(getId()));
    }

    private void validateOwnerType() {
        isLegalEntity(owner.getName());
        if (isLegalEntity) {
            legalEntity = new LegalEntity();
            copyOwnerValues(legalEntity);
        } else {
            naturalEntity = new NaturalEntity();
            copyOwnerValues(naturalEntity);
        }
    }

    private void copyOwnerValues(OwnerEntity copy) {
        copy.setName(owner.getName());
        copy.setNit(owner.getNit());
        copy.setEmail(owner.getEmail());
        copy.setFAX(owner.getFAX());
        copy.setTelephone(owner.getTelephone());
        for(Establishment establishment : owner.getEstablishments()){
            copy.addEstablishment(establishment);
        }
        for(Vehicle vehicle : owner.getVehicles()){
            copy.addVehicle(vehicle);
        }
    }

    private void isLegalEntity(String ownerName) {
        if (ownerName.contains("S.A")
                || ownerName.contains("S.V")
                || ownerName.startsWith("Indus")
                || ownerName.startsWith("Asoc")
                || ownerName.startsWith("Empr")
                || ownerName.contains("Business")
                || ownerName.contains("Org")
                || ownerName.contains("Proy")) {
            isLegalEntity = true;
        }
    }

    private void addEstablishment() {
        Establishment establishment = (Establishment) establishmentModelConverter.getObject();
        if (establishment.getName() != null) {
            owner.addEstablishment(establishment);
        }
    }

    private void addVehicle() {
        Vehicle vehicle = (Vehicle) vehicleModelConverter.getObject();
        if(vehicle.getLicense() != null){
            owner.addVehicle(vehicle);
        }
    }
}
