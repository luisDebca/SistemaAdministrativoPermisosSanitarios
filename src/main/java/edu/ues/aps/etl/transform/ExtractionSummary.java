package edu.ues.aps.etl.transform;

import java.util.Map;

public interface ExtractionSummary {

    void reset();

    Integer getOriginalRowCount();

    Integer getCompletedRowCount();

    Integer getWithErrorRowCount();

    Integer getWithWarningRowCount();

    Integer getOriginalColumnCount();

    Integer getSavedRowsCount();

    Integer getUpdatedRowsCount();

    Integer getInvalidColumnCount();

    Integer getEmptyCellCount();

    Boolean getFinishedCorrectly();

    Map<String, String> getMessages();

    void setOriginalRowCount(Integer originalRowCount);

    void setOriginalColumnCount(Integer originalColumnCount);

    void plusCompletedRowCount();

    void plusWithErrorRowCount();

    void plusWithWarningRowCount();

    void plusSavedRowsCount();

    void plusUpdatedRowsCount();

    void plusInvalidColumnCount();

    void plusEmptyCellCount();

    void setFinishedCorrectly(Boolean finishedCorrectly);

    void addInfoMessage(String pattern, String message);

    void addWarningMessage(String pattern, String message);

    void addErrorMessage(String pattern, String message);
}
