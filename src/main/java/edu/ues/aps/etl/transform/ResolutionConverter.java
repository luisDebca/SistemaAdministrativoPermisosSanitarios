package edu.ues.aps.etl.transform;

import edu.ues.aps.process.resolution.base.model.Resolution;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class ResolutionConverter implements ModelConverter {

    private Resolution resolution;

    @Override
    public Class appliesTo() {
        return Resolution.class;
    }

    @Override
    public void setUp() {
        resolution = new Resolution();
        resolution.setResolution_create_on(LocalDateTime.now());
        resolution.setDiploma_create_on(LocalDateTime.now());
    }

    @Override
    public Object getObject() {
        return resolution;
    }

    @Override
    public void setProperty(String propertyName, String value) throws NotValidColumnNameException{
        switch (propertyName){
            case "resolution_number":
            case "resolution_code":
                resolution.setResolution_code(CellValueFormatting.getRequestCode(value));
                resolution.setResolution_number(CellValueFormatting.getFolderNumber(value));
                resolution.setResolution_year(CellValueFormatting.getRequestYear(value));
                break;
            case "date":
            case "resolution_create_on":
            case "diploma_create_on":
                resolution.setResolution_create_on(CellValueFormatting.getDateTime(value).atStartOfDay());
                resolution.setDiploma_create_on(CellValueFormatting.getDateTime(value).atStartOfDay());
                break;
            default:
                throw new NotValidColumnNameException(String.format("Column name %s invalid.", propertyName));
        }
    }

    @Override
    public String getId() {
        return String.format("06%s%s-%s", resolution.getResolution_code(), resolution.getResolution_number(), resolution.getResolution_year());
    }

    @Override
    public Boolean validate() {
        return false;
    }

    @Override
    public void save() {

    }

    @Override
    public void update() {

    }
}
