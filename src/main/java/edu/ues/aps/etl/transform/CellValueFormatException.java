package edu.ues.aps.etl.transform;

public class CellValueFormatException extends RuntimeException {

    public CellValueFormatException() {
    }

    public CellValueFormatException(String s) {
        super(s);
    }
}
