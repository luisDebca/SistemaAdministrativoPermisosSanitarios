package edu.ues.aps.etl.transform;

import edu.ues.aps.process.tracing.base.model.Inspection;
import edu.ues.aps.process.tracing.base.model.Tracing;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;

@Service
@Transactional
public class TracingConverter implements ModelConverter{

    private Tracing tracing;

    private final ModelConverter firstInspectionConverter;
    private final ModelConverter firstReInspectionConverter;
    private final ModelConverter secondReInspectionConverter;
    private final ModelConverter thirdReInspectionConverter;

    public TracingConverter(@Qualifier("inspectionConverter") ModelConverter firstInspectionConverter, @Qualifier("inspectionConverter") ModelConverter firstReInspectionConverter, @Qualifier("inspectionConverter") ModelConverter secondReInspectionConverter, @Qualifier("inspectionConverter") ModelConverter thirdReInspectionConverter) {
        this.firstInspectionConverter = firstInspectionConverter;
        this.firstReInspectionConverter = firstReInspectionConverter;
        this.secondReInspectionConverter = secondReInspectionConverter;
        this.thirdReInspectionConverter = thirdReInspectionConverter;
    }

    @Override
    public Class appliesTo() {
        return Tracing.class;
    }

    @Override
    public void setUp() {
        tracing = new Tracing();
        tracing.setCreated_on(LocalDateTime.now());
        firstInspectionConverter.setUp();
        firstReInspectionConverter.setUp();
        secondReInspectionConverter.setUp();
        thirdReInspectionConverter.setUp();
    }

    @Override
    public Object getObject() {
        addInspection((Inspection) firstInspectionConverter.getObject());
        addInspection((Inspection) firstReInspectionConverter.getObject());
        addInspection((Inspection) secondReInspectionConverter.getObject());
        addInspection((Inspection) thirdReInspectionConverter.getObject());
        return tracing;
    }

    @Override
    public void setProperty(String propertyName, String value) throws NotValidColumnNameException {
        if(UtilCellContent.getColumnLevel(propertyName) == 0){
            switch (propertyName){
                case "observation":
                    tracing.setObservation(value);
                    break;
                case "return_date":
                    tracing.setReturn_date(CellValueFormatting.getDateTime(value).atStartOfDay());
                    break;
                case "shipping_date":
                    tracing.setShipping_date(CellValueFormatting.getDateTime(value).atStartOfDay());
                    break;
                case "ucsf_received_on":
                    tracing.setUcsf_received_on(CellValueFormatting.getDateTime(value).atStartOfDay());
                    break;
                default:
                    throw new NotValidColumnNameException(String.format("Column name %s invalid.", propertyName));
            }
        }else {
            if ("inspection".equals(UtilCellContent.getTableName(propertyName))) {
                String inspectionProperty = UtilCellContent.getColumnName(propertyName);
                switch (inspectionProperty){
                    case "1":
                        firstInspectionConverter.setProperty(UtilCellContent.getColumnName(inspectionProperty),value);
                        break;
                    case "2":
                        firstReInspectionConverter.setProperty(UtilCellContent.getColumnName(inspectionProperty),value);
                        break;
                    case "3":
                        secondReInspectionConverter.setProperty(UtilCellContent.getColumnName(inspectionProperty),value);
                        break;
                    case "4":
                        thirdReInspectionConverter.setProperty(UtilCellContent.getColumnName(inspectionProperty),value);
                        break;
                        default:
                            firstInspectionConverter.setProperty(inspectionProperty,value);
                }
            }
        }
    }

    @Override
    public String getId() {
        return tracing.getCreated_on().toString();
    }

    @Override
    public Boolean validate() {
        return false;
    }

    @Override
    public void save() {

    }

    @Override
    public void update() {

    }

    private void addInspection(Inspection inspection){
        if(inspection.getInspected_on() != null){
            tracing.addInspection(inspection);
        }
    }
}
