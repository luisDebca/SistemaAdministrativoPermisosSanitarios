var tableProductCount = 0;
var newProductCount = 0;

$(document).ready(function(){
	 $('#firstAddedProductBtn').on("click",function(){
    	tableProductCount = $('#registedProducts .productRow').length;
    	$('#table-form-product').append('<table id="newProductTable" class="table table-sm table-borderless"><thead><tr><th>Tipo de producto que elabora</th><th>Nombre del producto</th><th>Registro sanitario</th><th></th></tr></thead><tbody id= "productRows"></tbody></table>');
    	$(this).attr("hidden",true);
    	if(tableProductCount == 0){
    		$('#productRows').append('<tr>' +
				'<input class="form-control" id="products0.id" name="products[0].id" type="hidden"/>' +
				'<td><input class="form-control" id="products0.type" name="products[0].type" type="text" required="required"/></td>' +
				'<td><input class="form-control" id="products0.name" name="products[0].name" type="text" required="required"/></td>' +
				'<td><input class="form-control" id="products0.sanitary" name="products[0].sanitary" type="text"/></td>' +
				'<td><a role="button" class="btn btn-outline-light remove-row-btn"><i class="fa fa-times" aria-hidden="true"></i></a></td>' +
				'</tr>');
    	}else if(tableProductCount > 0){
    		$('#productRows').append('<tr>' +
				'<input class="form-control" id="products' + tableProductCount +'.id" name="products[' + tableProductCount +'].id" type="hidden"/>' +
				'<td><input class="form-control" id="products' + tableProductCount +'.type" name="products[' + tableProductCount +'].type" type="text" required="required"/></td>' +
				'<td><input class="form-control" id="products' + tableProductCount +'.name" name="products[' + tableProductCount +'].name" type="text" required="required"/></td>' +
				'<td><input class="form-control" id="products' + tableProductCount +'.sanitary" name="products[' + tableProductCount +'].sanitary" type="text"/></td>' +
				'<td><a role="button" class="btn btn-outline-light remove-row-btn"><i class="fa fa-times" aria-hidden="true"></i></a></td>' +
				'</tr>')
    	}
    	newProductCount ++;
		 $('#product-new-card-list').append('</br><button type="button" class="btn btn-outline-primary" onclick="addNewProductRow()">Agregar otro producto</button>');
    	$("#productsFormSubmitBtn").attr("hidden",false);
    });

	 $(".on-click-remove-disable-btn").on("click",function () {
		 $(this).parent().parent().parent().find("input").attr("readonly", false);
         $("#productsFormSubmitBtn").attr("hidden",false);
     });

	 $(".on-click-confirm-delete-btn").on("click",function () {
		$("#delete-confirmation-modal").modal('show');
		$("#selected_product_id").val($(this).parent().parent().parent().find(".id_product input").val());
     });

	 $("#delete-product-btn").on("click",function () {
		onClickDeleteProduct($("#selected_product_id").val());
		$("#product-list").find(".id_product input").each(function () {
			if($(this).val() == $("#selected_product_id").val()){
				$(this).parent().parent().remove();
			}
        });
         tableProductCount = $('#registedProducts .productRow').length;
         recheckProductListNames();
         $("#delete-confirmation-modal").modal('hide');
     });
});

$(document).on("click",".remove-row-btn",function () {
    $(this).parent().parent().remove();
    newProductCount --;
    recheckProductListNames();
});

function addNewProductRow(){
    var customCount = tableProductCount + newProductCount;
    $('#productRows').append('<tr>' +
		'<input class="form-control" id="products' + customCount +'.id" name="products[' + customCount +'].id" type="hidden"/>' +
		'<td><input class="form-control" id="products' + customCount +'.type" name="products[' + customCount +'].type" type="text"/></td>' +
		'<td><input class="form-control" id="products' + customCount +'.name" name="products[' + customCount +'].name" type="text"/></td>' +
		'<td><input class="form-control" id="products' + customCount +'.sanitary" name="products[' + customCount +'].sanitary" type="text"/></td>' +
		'<td><a role="button" class="btn btn-outline-light remove-row-btn"><i class="fa fa-times" aria-hidden="true"></i></a></td>' +
		'</tr>');
	newProductCount ++;
}

function onClickDeleteProduct(id_product){
    var result = getRequest("/" + pathName + "/delete-product-by-id",{id:id_product});
    result.done(function (data) {
        if(data.response_status === "SUCCESS"){
            showSnackbar("Se ha eliminado el producto correctamente.",15000,false);
        }else{
            showSnackbar("Ha ocurrido un error: " + data.response_message,15000,false);
        }
    });
    $("#legal-entity-product-table-modal").modal('hide');
    $("#product-new-card-list").parent().addClass("text-white bg-info");
    $("#product-new-card-list").addClass("text-center");
    $("#product-new-card-list").html("<h3>Se han realizado cambios en los productos, actualize la p�gina para ver los cambios</h3>")
}

function recheckProductListNames(){
	var initCount = $('#registedProducts .productRow').length - 1;
	$("#newProductTable tr").each(function () {
		$(this).find("input").each(function () {
			$(this).attr("id",$(this).attr("id").replace(/[0-9]/g,initCount));
            $(this).attr("name",$(this).attr("name").replace(/[0-9]/g,initCount));
        });
		initCount ++;
    });
}