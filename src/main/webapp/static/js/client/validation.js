$("#main").validate({
    rules: {
        "client_name":{
            required: true,
            minlength: 2,
            maxlength: 150
        },
        clientType:{
            required:true
        },
        "client_nationally":{
            required:true
        },
        "client_DUI":{
            dui:true,
            remote: {
                url: "/" + pathName + "/client/validate-dui",
                type: "get",
                data: {
                    id_client: function () {
                        return $("input[name='id_client']").val();
                    },
                    dui: function () {
                        return $("input[name='client_DUI']").val();
                    }
                }
            }
        },
        "client_email":{
            email:true,
            maxlength:100
        },
        "client_telephone":{
            minlength:8,
            maxlength:15
        },
        "client_passport":{
            maxlength:15,
            remote: {
                url: "/" + pathName + "/client/validate-passport",
                type: "get",
                data: {
                    id_client: function () {
                        return $("input[name='id_client']").val();
                    },
                    passport: function () {
                        return $("input[name='client_passport']").val();
                    }
                }
            }
        },
        "client_resident_card":{
            maxlength:15,
            remote: {
                url: "/" + pathName + "/client/validate-resident-card",
                type: "get",
                data: {
                    id_client: function () {
                        return $("input[name='id_client']").val();
                    },
                    rc: function () {
                        return $("input[name='client_resident_card']").val();
                    }
                }
            }
        }
    }
});
