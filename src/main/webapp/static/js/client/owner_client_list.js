$(document).ready(function () {

    var result = getRequest("/" + pathName + "/owner/find-by-id",{id_owner:$("#id_owner").text()});
    result.done(function (data) {
       if(data === undefined){
           $(".information-panel").html('<div class="alert alert-warning" role="alert">' +
               'No se encontro la información correcta del propietario.' +
               '</div>');
       } else{
           $("#owner_name").text(data.owner_name);
           $("#owner-extra-info").append('<p class="my-0">' + checkString(data.owner_nit) + '</p>');
           $("#owner-extra-info").append('<p class="my-0">' + checkString(data.owner_email) + '</p>');
           $("#owner-extra-info").append('<p class="my-0">' + checkString(data.owner_telephone) + '</p>')
       }
    });
});

$(document).on("click",".remove-client-btn",function () {
   $("#client-remove-confirmation-modal").modal("show");
    $("#remove-btn").attr("href","/" + pathName + "/owner/" + $("#id_owner").text() + "/client/" + $(this).parent().parent().parent().find(".id_client").text() + "/type/"+ $(this).parent().parent().parent().find(".client_type").text() +"/remove")
});