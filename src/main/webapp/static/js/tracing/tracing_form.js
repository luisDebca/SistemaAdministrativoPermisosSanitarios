jQuery.datetimepicker.setLocale('es');
jQuery("#shipping_date-date").datetimepicker({
    timepicker:false,
    format:'Y-m-d H:i'
});

jQuery("#return-date").datetimepicker({
    timepicker:false,
    format:'Y-m-d H:i',
    onShow:function( ct ){
        this.setOptions({
            minDate:jQuery('#shipping_date-date').val()?jQuery('#shipping_date-date').val():false
        })
    }
});

jQuery("#UCSF-received").datetimepicker({
    timepicker:false,
    format:'Y-m-d H:i',
    onShow:function( ct ){
        this.setOptions({
            minDate:jQuery('#return-date').val()?jQuery('#return-date').val():false
        })
    }
});

jQuery("#review-date").datetimepicker({
    timepicker:false,
    format:'Y-m-d H:i',
    onShow:function( ct ){
        this.setOptions({
            minDate:jQuery('#UCSF-received').val()?jQuery('#UCSF-received').val():false
        })
    }
});