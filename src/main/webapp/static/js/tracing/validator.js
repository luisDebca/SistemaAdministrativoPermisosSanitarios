$(document).ready(function () {
    $("#main").validate({
        rules: {
            shipping_date:{
                required:true,
                datetimeISO:true
            },
            ucsf_received_on:{
                required:true,
                datetimeISO:true
            },
            return_date:{
                required:true,
                datetimeISO:true
            },
            observation:{
                maxlength:1500
            }
        }
    })
});