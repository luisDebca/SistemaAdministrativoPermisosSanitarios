$("#main").validate({
    rules: {
        name:{
            required: true,
            minlength: 2,
            maxlength: 99
        },
        nit:{
            nit: true,
            remote: {
                url: "/" + pathName + "/owner/validate-nit",
                type: "get",
                data: {
                    id_owner: function () {
                        return $("input[name='id']").val();
                    },
                    nit: function () {
                        return $("input[name='nit']").val();
                    }
                }
            }
        },
        email:{
            email:true,
            maxlength:100
        },
        FAX:{
            maxlength:15
        },
        telephone:{
            maxlength:15
        },
        DUI:{
            dui: true,
            remote: {
                url: "/" + pathName + "/owner/validate-dui",
                type: "get",
                data: {
                    id_owner: function () {
                        return $("input[name='id']").val();
                    },
                    dui: function () {
                        return $("input[name='DUI']").val();
                    }
                }
            },
            required: {
                depends: function (element) {
                    return $("#nationally").val() == "Salvadoreņa";
                }
            }
        },
        nationally:{
            required:true
        },
        passport:{
            maxlength: 15,
            remote: {
                url: "/" + pathName + "/owner/validate-passport",
                type: "get",
                data: {
                    id_owner: function () {
                        return $("input[name='id']").val();
                    },
                    passport: function () {
                        return $("input[name='passport']").val();
                    }
                }
            },
            required: {
                depends: function (element) {
                    return $("#nationally").val() != "Salvadoreņa" && $("#residentCard").val().length == 0;
                }
            }
        },
        residentCard: {
            maxlength: 15,
            remote: {
                url: "/" + pathName + "/owner/validate-resident-card",
                type: "get",
                data: {
                    id_owner: function () {
                        return $("input[name='id']").val();
                    },
                    residentCard: function () {
                        return $("input[name='residentCard']").val();
                    }
                }
            },
            required: {
                depends: function (element) {
                    return $("#nationally").val() != "Salvadoreņa" && $("#passport").val().length == 0;
                }
            }
        }
    }
});