$("#main").validate({
    rules: {
        name:{
            required: true,
            minlength: 2,
            maxlength: 99,
            remote: {
                url: "/" + pathName + "/owner/validate-name",
                type: "get",
                data: {
                    id_owner: function () {
                        return $("input[name='id']").val();
                    },
                    name: function () {
                        return $("input[name='name']").val();
                    }
                }
            }
        },
        nit:{
            required: true,
            nit: true,
            remote: {
                url: "/" + pathName + "/owner/validate-nit",
                type: "get",
                data: {
                    id_owner: function () {
                        return $("input[name='id']").val();
                    },
                    nit: function () {
                        return $("input[name='nit']").val();
                    }
                }
            }
        },
        email:{
            email:true,
            maxlength:100
        },
        FAX:{
            maxlength:15
        },
        telephone:{
            maxlength:15
        },
        "address.details":{
            required: true,
            maxlength:499
        }
    }
});