$(document).ready(function () {
   $(".show-new-owner-type-selector-modal-btn").on("click",function () {
       $("#owner-type-confirmation-modal").modal("show");
   }) ;

   $("#owner-finder-input").on("input",function () {
      if($(this).val().length > 3){
          var result = getRequest("/" + pathName + "/find-owner-by-identifier",{param:$(this).val()});
          result.done(function (data) {
              var table = "";
             if(data.length > 0){
                 table += '<div class="alert alert-success" role="alert"><i class="fa fa-check" aria-hidden="true"></i> '+ data.length +' Resultados encontrados</div>';
                 table += '<table class="table table-hover table-sm"><thead><tr><th>Nombre</th><th>NIT</th><th>Email</th><th>Tel�fono</th></tr></thead><tbody>';
                 $.each(data,function (key, value) {
                     if(value.class_type == 'LegalEntity'){
                         table += '<tr><td><a href="/'+ pathName + '/owner/legal/' + value.id_owner +'">'+ value.owner_name +'</a></td><td>'+ value.owner_nit +'</td><td>' + value.owner_email + '</td><td>'+ value.owner_telephone +'</td></tr>';
                     }else if(value.class_type == 'NaturalEntity'){
                         table += '<tr><td><a href="/'+ pathName + '/owner/natural/' + value.id_owner +'">'+ value.owner_name +'</a></td><td>'+ value.owner_nit +'</td><td>' + value.owner_email + '</td><td>'+ value.owner_telephone +'</td></tr>';
                     }
                 });
                 table += '</tbody></table>';
             }else {
                 table += '<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation" aria-hidden="true"></i>  <strong>Sin resultados</strong></div>';
             }
             $("#search_result").html(table);
          });
      }else{
          $("#search_result").empty();
      }
   });
});