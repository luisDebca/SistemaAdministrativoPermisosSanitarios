$("#main").validate({
    rules: {
        request_folder: {
            required: true,
            number: true,
            remote: {
                url: "/" + pathName + "/caseFile/validator/folder_number",
                type: "get",
                data: {
                    id_caseFile: function () {
                        return $("input[name='id']").val();
                    },
                    number: function () {
                        return $("input[name='request_folder']").val();
                    },
                    year: function () {
                        return $("input[name='request_year']").val();
                    }
                }
            }
        },
        request_code:{
            required:true,
            maxlength:5,
            minlength: 1,
            remote: {
                url: "/" + pathName + "/caseFile/validator/request_number",
                type: "get",
                data: {
                    id_caseFile: function () {
                        return $("input[name='id']").val();
                    },
                    code: function () {
                        return $("input[name='request_code']").val();
                    },
                    number: function () {
                        return $("input[name='request_number']").val();
                    },
                    year: function () {
                        return $("input[name='request_year']").val();
                    }
                }
            }
        },
        request_number: {
            required: true,
            number: true,
            remote: {
                url: "/" + pathName + "/caseFile/validator/request_number",
                type: "get",
                data: {
                    id_caseFile: function () {
                        return $("input[name='id']").val();
                    },
                    code: function () {
                        return $("input[name='request_code']").val();
                    },
                    number: function () {
                        return $("input[name='request_number']").val();
                    },
                    year: function () {
                        return $("input[name='request_year']").val();
                    }
                }
            }
        },
        request_year: {
            request: true,
            number: true
        },
        certification_type:{
            required:true
        },
        certification_duration_in_years:{
            required:true,
            number:true
        }
    }
});