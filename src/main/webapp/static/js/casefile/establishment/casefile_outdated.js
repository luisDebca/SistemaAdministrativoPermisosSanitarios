$("#main").validate({
    rules: {
        request_code:{
            required: true,
            minlength: 1,
            maxlength: 5
        },
        request_number:{
            required: true,
            minlength: 1,
            maxlength: 12,
            digits: true
        },
        request_year:{
            required: true,
            minlength: 2,
            maxlength:4,
            digits: true
        },
        request_folder:{
            required: true,
            minlength: 1,
            maxlength: 12,
            digits: true
        },
        certification_start_on:{
            required: true
        },
        certification_duration_in_years:{
            required:true,
            digits: true,
            maxlength: 1
        },
        creation_date:{
            required:true
        },
        "ucsf.id":{
            required:true
        },
        "ucsf.sibasi.id":{
            required:true
        }
    }
});

jQuery(".date").datetimepicker({
    format:'Y-m-d H:i'
});