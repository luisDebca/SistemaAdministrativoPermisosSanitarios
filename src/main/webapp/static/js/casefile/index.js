$(document).ready(function () {
    $("#establishment-finder-input").on("input",function () {
        if($(this).val().length > 3){
            var result = getRequest("/" + pathName + "/find-case-file-by-identifier",{identifier:$(this).val()});
            result.done(function (data) {
                var table = "";
                if(data.length > 0){
                    table += '<div class="alert alert-success" role="alert"><i class="fa fa-check" aria-hidden="true"></i> '+ data.length +' Resultados encontrados</div>';
                    table += '<table class="table table-hover table-sm"><thead><tr><th>N\u00FAmero de folder</th><th>C\u00F3digo de solicitud</th><th>Secci\u00F3n</th><th>Proceso</th></tr></thead><tbody>';
                    $.each(data,function (key, value) {
                        table += '<tr><td><a href="/'+ pathName + '/caseFile/' + value.id_casefile +'/info">'+ value.request_folder +'</a></td><td>'+ value.request_identifier +'</td><td>'+ value.casefile_section +'</td><td>'+ value.current_process +'</td></tr>'
                    });
                    table += '</tbody></table>';
                } else{
                    table += '<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation" aria-hidden="true"></i>  <strong>Sin resultados</strong></div>';
                }
                $("#search_result").html(table);
            });
        }else{
            $("#search_result").empty();
        }
    });
});