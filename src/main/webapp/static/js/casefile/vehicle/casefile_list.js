$(document).ready(function () {
    var id_owner = $("#id_owner").text();
    var result = getRequest("/" + pathName + "/find-owner-by-id",{id_owner:id_owner});
    result.done(function (data) {
        $("#owner_name").html(data.owner_name);
        $("#extra-info").append("<p class='mb-0'>"+ checkString(data.owner_nit)  +"</p>");
        $("#extra-info").append("<p class='mb-0'>"+ checkString(data.owner_email) +"</p>");
        $("#extra-info").append("<p class='mb-0'>"+ checkString(data.owner_telephone) +"</p>");
    });
});

