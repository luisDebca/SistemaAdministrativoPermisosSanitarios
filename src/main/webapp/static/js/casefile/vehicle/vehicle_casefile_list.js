$(document).ready(function () {

    var result = getRequest("/" + pathName + "/vehicle/find-by-id",{id_vehicle:$("#id_vehicle").text()});
    result.done(function (data) {
        $("#vehicle_license").text(checkString(data.vehicle_license));
        $("#vehicle_owner").text(checkString(data.owner_name));
        $("#vehicle_owner").attr("href","/" + pathName + "/owner/" + data.id_owner);
        $("#vehicle_trasport").text(checkString(data.vehicle_transported_content));
        $("#vehicle_type").text(checkString(data.vehicle_type));
    });

    $("#show-new-request-confirmation-modal").on("click",function () {
       $("#new-modal").modal("show");
    });
});