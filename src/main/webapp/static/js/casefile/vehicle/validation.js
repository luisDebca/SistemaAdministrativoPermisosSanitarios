$("#main").validate({
    rules: {
        vehicle_count: {
            required: true,
            number: true
        },
        "address.details": {
            required: true,
            maxlength: 500
        },
        distribution_company: {
            maxlength: 100
        },
        distribution_destination: {
            maxlength: 150
        },
        client_presented_name: {
            maxlength: 150
        },
        client_presented_telephone: {
            maxlength: 15
        },
        marketing_place: {
            maxlength: 100
        }
    }
});