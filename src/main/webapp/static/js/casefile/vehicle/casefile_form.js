var vehicle_number = $("#number").val();
$(document).ready(function(){

    $("#number").on("input",function () {
       vehicle_number = $(this).val();
       $("#counter").text(vehicle_number);
    });

    $(".custom-control-input").on("change",function () {
        changeCounterValue($(".custom-control-input:checked").length);
    });

    $("#all").on("click",function () {
        $(".custom-control-input").each(function (key, item) {
           $(this).prop('checked', true);
        });
        changeCounterValue($(".custom-control-input:checked").length);
    });

    $("#none").on("click",function () {
        $(".custom-control-input").each(function (key, item) {
            $(this).prop('checked', false);
        });
        changeCounterValue($(".custom-control-input:checked").length);
    });

});

function changeCounterValue(selected_vehicles) {
    custom_value = $("#number").val() - selected_vehicles;
    if(custom_value >= 0){
        $("#counter").text(custom_value);
        vehicle_number = custom_value;
    }else{
        showToast("Seleccione correctamente el n\u00FAmero de veh\u00EDculos.")
    }
}