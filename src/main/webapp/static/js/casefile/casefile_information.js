$(document).ready(function () {

    $("#requirement-row-list").hide();
    $("#requirement-document-information-collapse").hide();
    $("#establishment-client-info-list").hide();
    $("#casefile-record-block").hide();

    $(".hidden-client-information-list-btn").on("click",function () {
        $("#establishment-client-info-list").hide("slow");
        $(".show-clients-information-list-btn").show();
        $(".hidden-client-information-list-btn").hide();
    });

    $(".show-requirement-row-list-btn").on("click",function () {
        var result = getRequest("/" + pathName + "/find-all-requirement-rows-from-document",{id: $("#id_casefile").text()});
        result.done(function (data) {
            $.each(data,function (key, item) {
                $(".requirement-row-list-body").append('<tr><td>' + (key + 1) + '</td>' +
                    '<td>' + item.row_requirement + '</td>' +
                    '<td>' + item.row_status + '</td>' +
                    '<td>' + item.row_observation + '</td></tr>')
            });
        });
        $("#requirement-row-list").show("slow");
        $(this).hide();
    });

    $(".to-previous-page").on("click",function () {
        window.history.back();
    });

    $("#hide-casefile-record-btn").on("click",function () {
        $("#casefile-record-block").hide("slow");
        $("#show-casefile-record-btn").show();
        $("#hide-casefile-record-btn").hide();
    })
});

$(document).on("click",".show-process-user-modification-modal-btn",function () {
    var result = getRequest("/" + pathName + "/find-process-user-record-from-casefile-modifications",{id_caseFile:$("#id_casefile").text(),id_process:$(this).parent().find(".id_process").text()});
    result.done(function (data) {
        var record_list = '<div class="list-group">';
        $.each(data,function (key, item) {
           record_list += '<div class="list-group-item list-group-item-action flex-column align-items-start">' +
               '<div class="d-flex w-100 justify-content-between"> <h6 class="mb-1">' + item.description +
               '</h6><small class="text-muted">' + item.modified_on + '</small></div>' +
               '<p class="mb-1">' + item.user_full_name + '</p></div>';
        });
        record_list += "</div>";
        $("#process-user-model-body").html(record_list);
    });
    $("#process-user-modal").modal("show");
});

$(document).on("click",".show-requirement-document-information-btn",function () {
    $("#requirement-document-information-collapse").toggle("slow",function () {
        if($(".show-requirement-document-information-btn").attr("class").includes("show-btn")){
            if($("#document_id").html().length == 0){
                var result = getRequest("/" + pathName + "/find-requirement-document-info-by-casefile",{id: $("#id_casefile").text()});
                result.done(function (data) {
                    $("#document_id").html(data.id_document);
                    $("#document-template-title").append(data.document_template_title);
                    if (data.document_status) {
                        $("#document-status").append('<span class="badge badge-success">Acceptado</span>');
                        $("#document-creation").append(data.document_created_on);
                        $("#document-acceptation").append(data.acceptance_created_on);
                    } else {
                        $("#document-status").append('<span class="badge badge-warning">Sin acceptar</span>');
                        $("#document-creation").append(data.document_created_on);
                        $("#document-acceptation").append('<span class="badge badge-default">No definido</span>');
                    }
                });
            }
            $(".show-requirement-document-information-btn").replaceWith('<a href="#" class="disabled_href show-requirement-document-information-btn hide-btn">Ocultar detalles</a>');
        }else {
            $(".show-requirement-document-information-btn").replaceWith('<a href="#" class="disabled_href show-requirement-document-information-btn show-btn">Mostrar detalles</a>');
        }
    });
});

$(document).on("click",".show-clients-information-list-btn",function () {
    $("#establishment-client-info-list").toggle("slow",function () {
        if($("#establishment-client-info-list").html().length === 0) {
            var result = getRequest("/" + pathName + "/find-all-clients-from-establishment", {id: $("#id_establishment").text()});
            result.done(function (data) {
                $.each(data, function (key, item) {
                    $("#establishment-client-info-list").append("<div class='list-group-item list-group-item-action flex-column align-items-start'>" +
                        "<div class='d-flex w-100 justify-content-between'><h5 class='mb-1'>" + item.client_name + "</h5><small class='text-muted'>" + item.client_type_text + "</small></div>" +
                        "<span class='fa-stack fa-3x'><i class='fa fa-square-o fa-stack-2x'></i><i class='fa fa-user-o fa-stack-1x'></i></span>" +
                        "<p class='mb-1'>Nacionalidad: " + item.client_nationally + "</p>" +
                        "<p class='mb-1'>DUI: " + item.client_DUI + "</p>" +
                        "<small class='text-muted'>Correo electronico: " + item.client_email + "</small><br>" +
                        "<small class='text-muted'>Telefono: " + item.client_telephone + "</small></div>");
                });
            });
        }
        if($(".show-clients-information-list-btn").attr("class").includes("show-btn")){
            $(".show-clients-information-list-btn").replaceWith('<a href="#" class="disabled_href show-clients-information-list-btn hide-btn">Ocultar autorizados</a>')
        }else {
            $(".show-clients-information-list-btn").replaceWith('<a href="#" class="disabled_href show-clients-information-list-btn show-btn">Mostrar autorizados</a>')
        }
    });
});

$(document).on("click",".show-casefile-record-btn",function () {
    $("#casefile-record-block").toggle("slow",function () {
        if($("#casefile-record-block").html().length === 0) {
            var result = getRequest("/" + pathName + "/find-casefile-process-record", {id_caseFile: $("#id_casefile").text()});
            result.done(function (data) {
                var process_table = "<table class='table table-hover table-sm'><thead><tr><th>Proceso</th><th>Descripci\u00F3n</th><th>Observaciones</th><th>Estado</th><th>Inicio</th><th>Fin</th><th></th></tr></thead><tbody>"
                $.each(data, function (key, item) {
                    var process_status = "";
                    if (item.active) {
                        process_status = '<span class="badge badge-pill badge-success">Actual</span>';
                    } else {
                        process_status = '<span class="badge badge-pill badge-secondary">Finalizado</span>';
                    }
                    process_table += "<tr><td>"+ item.process_process +"</td><td>"+ item.process_description +"</td><td>"+ item.observations +"</td><td class='cell-center-middle'>"+ process_status +"</td><td>"+ item.start_on +"</td><td>"+ item.finished_on +"</td><td><a href='#'>Ver modificaciones</a></td></tr>";
                });
                process_table += "</tbody></table>";
                $("#casefile-record-block").html(process_table);
                $("#casefile-record-block").show('slow');
            });
        }
        if($(".show-casefile-record-btn").attr("class").includes("show-btn")){
            $(".show-casefile-record-btn").replaceWith('<a href="#" class="disabled_href show-casefile-record-btn hide-btn">Ocultar procesos</a>');
        }else {
            $(".show-casefile-record-btn").replaceWith('<a href="#" class="disabled_href show-casefile-record-btn show-btn">Mostrar procesos</a>');
        }
    });
});

