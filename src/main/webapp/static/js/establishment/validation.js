$("#main_new").validate({
    rules: {
        name:{
            required: true,
            minlength: 2,
            maxlength: 100,
            remote: {
                url: "/" + pathName + "/establishment/validator/name",
                type: "get",
                data: {
                    name: function () {
                        return $("input[name='name']").val();
                    }
                }
            }
        },
        type_detail:{
            required: true,
            maxlength: 50
        },
        "address.details":{
            required:true,
            maxlength: 500,
            remote: {
                url: "/" + pathName + "/establishment/validator/address",
                type: "get",
                data: {
                    address: function () {
                        return $("input[name='address.details']").val();
                    }
                }
            }
        },
        commercial_register:{
            maxlength:25,
            remote: {
                url: "/" + pathName + "/establishment/validator/rc",
                type: "get",
                data: {
                    rc: function () {
                        return $("input[name='commercial_register']").val();
                    }
                }
            }
        },
        capital:{
            number:true
        },
        NIT:{
            nit:true
        },
        telephone:{
            minlength:8,
            maxlength:15
        },
        telephone_extra:{
            minlength:8,
            maxlength:15
        },
        FAX:{
            minlength:8,
            maxlength:15
        },
        email:{
            email:true,
            maxlength:100
        },
        operations:{
            maxlength:150
        },
        maleEmployeeCount:{
            digits: true
        },
        femaleEmployeeCount:{
            digits: true
        }
    }
});
$("#main_edit").validate({
    rules: {
        name:{
            required: true,
            minlength: 2,
            maxlength: 100,
            remote: {
                url: "/" + pathName + "/establishment/validator/name-update",
                type: "get",
                data: {
                    name: function () {
                        return $("input[name='name']").val();
                    },
                    id_establishment: function () {
                        return $("input[name='id']").val();
                    }
                }
            }
        },
        type_detail:{
            required: true,
            maxlength: 50
        },
        "address.details":{
            required:true,
            maxlength: 500,
            remote: {
                url: "/" + pathName + "/establishment/validator/address-update",
                type: "get",
                data: {
                    address: function () {
                        return $("input[name='address.details']").val();
                    },
                    id_establishment: function () {
                        return $("input[name='id']").val();
                    }
                }
            }
        },
        commercial_register:{
            maxlength:15,
            remote: {
                url: "/" + pathName + "/establishment/validator/rc-update",
                type: "get",
                data: {
                    rc: function () {
                        return $("input[name='commercial_register']").val();
                    },
                    id_establishment: function () {
                        return $("input[name='id']").val();
                    }
                }
            }
        },
        capital:{
            number:true
        },
        NIT:{
            required:true,
            nit:true,
            remote: {
                url: "/" + pathName + "/establishment/validator/nit-update",
                type: "get",
                data: {
                    nit: function () {
                        return $("input[name='NIT']").val();
                    },
                    id_establishment: function () {
                        return $("input[name='id']").val();
                    }
                }
            }
        },
        telephone:{
            minlength:8,
            maxlength:15
        },
        telephone_extra:{
            minlength:8,
            maxlength:15
        },
        FAX:{
            minlength:8,
            maxlength:15
        },
        email:{
            email:true,
            maxlength:100
        },
        operations:{
            maxlength:150
        },
        maleEmployeeCount:{
            digits: true
        },
        femaleEmployeeCount:{
            digits: true
        }
    }
});

$("#capital").on("change",function () {
   if($(this).val().includes(",")){
       $(this).val($(this).val().split(",")[0] + $(this).val().split(",")[1])
   }
});