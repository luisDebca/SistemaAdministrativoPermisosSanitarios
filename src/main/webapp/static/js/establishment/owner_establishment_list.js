$(document).ready(function () {
    var result = getRequest("/" + pathName + "/find-owner-by-id",{id_owner:$(".owner_id").text()});
    result.done(function (data) {
        $("#owner_name").html(data.owner_name);
        $(".extra_info").append("<p class='mb-0'>" + data.owner_nit? data.owner_nit:"" + "</p>");
        $(".extra_info").append("<p class='mb-0'>" + data.owner_email + "</p>");
        $(".extra_info").append("<p class='mb-0'>" + data.owner_telephone + "</p>");
    });
});
