$(document).ready(function () {
    $("#establishment-finder-input").on("input",function () {
       if($(this).val().length > 3){
           var result = getRequest("/" + pathName + "/find-establishment-by-parameter",{param:$(this).val()});
           result.done(function (data) {
               var table = "";
              if(data.length > 0){
                  table += '<div class="alert alert-success" role="alert"><i class="fa fa-check" aria-hidden="true"></i> '+ data.length +' Resultados encontrados</div>';
                  table += '<table class="table table-hover table-sm"><thead><tr><th>Nombre</th><th>Tipo</th><th>Direcci\u00F3n</th></tr></thead><tbody>';
                  $.each(data,function (key, value) {
                      table += '<tr><td><a href="/'+ pathName + '/establishment/' + value.establishment_id +'">'+ value.establishment_name +'</a></td><td>'+ value.establishment_type_detail +'</td><td>'+ value.establishment_address +'</td></tr>'
                  });
                  table += '</tbody></table>';
              } else{
                  table += '<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation" aria-hidden="true"></i>  <strong>Sin resultados</strong></div>';
              }
               $("#search_result").html(table);
           });
       } else if($(this).val().includes("%")){
           showToast("No deberias de intentar hacer injecci\u00F3n SQL.",15,false);
           $(this).val("");
       } else{
           $("#search_result").empty();
       }
    });
});