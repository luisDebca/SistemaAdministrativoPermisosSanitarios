$(document).ready(function () {

    var result = getRequest("/" + pathName + "/find-establishment-full-details",{id:$("#id_establishment").text()});
    result.done(function (data) {
        $("#establishment_name").html(data.establishment_name);
        $("#extra-info").append("<p class='mb-0'>"+ data.establishment_address +"</p>")
        $("#extra-info").append("<p class='mb-0'>"+ data.establishment_type_detail +"</p>")
    });
});

$(document).on("click",".show-first-modal",function () {
    $("#first-certification-modal").modal("show");
});

$(document).on("click",".show-renewal-modal",function () {
    $("#renewal-certification-modal").modal("show");
});

$(document).on("click",".show-cancelling-modal",function () {
    $("#cancel-modal").modal("show");
});