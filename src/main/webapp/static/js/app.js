var pathName = window.location.pathname.split("/")[1];
var selected_row;
var selected_caseFile;
var selected_owner;
var selected_bunch;
var selected_establishment;

$(document).ready(function () {
	
    //habilita los iconos por feater
    feather.replace();

    //Desabilita el comportamiento por defecto de las etiquetas <li> cuando estas tienen en su src = "#"
    //Para desabilitar este comportamiento agregar class = "disabled_href"
    $(document).on("click", "a.disabled_href", function (e) {
        e.preventDefault();
    });

    //Habilita el menu de navegación la propiedad toggle
    $('.sidemenuBtn').click(function () {
        $('.sidemenu').toggleClass('menuactive');
        $('.page-content-wrapper').toggleClass('page-content-toggle');
        $('.sidemenuBtn').toggleClass('toggle');
    });

    //Habilita bootstrapMaterialDesing, util para los snacbar
    $('body').bootstrapMaterialDesign();

    //Habilita el envio de solicitudes post con AJAX
    $(document).ajaxSend(function (e, xhr, options) {
        xhr.setRequestHeader(header, token);
    });

    //Recarga la pagina actual
    $('.information-panel').on('click', ".reload-custom-page-btn", function () {
        location.reload(true);
    });

    //Mostrar/ocultar menu
    $("#process-menu-list-btn").on("click", function () {
        $("#process-menu-list").toggle("fast", function () {
            if ($("#process-menu-list-btn").find("svg").attr("class").includes("feather-minus-circle")) {
                $("#process-menu-list-btn").html('<span data-feather="plus-circle"></span>');
            } else {
                $("#process-menu-list-btn").html('<span data-feather="minus-circle"></span>');
            }
            feather.replace();
        });
    });

    $("#register-menu-list-btn").on("click", function () {
        $("#register-menu-list").toggle("fast", function () {
            if ($("#register-menu-list-btn").find("svg").attr("class").includes("feather-minus-circle")) {
                $("#register-menu-list-btn").html('<span data-feather="plus-circle"></span>');
            } else {
                $("#register-menu-list-btn").html('<span data-feather="minus-circle"></span>');
            }
            feather.replace();
        });
    });
    
    $("#sigaps-menu-list-btn").on("click", function () {
        $("#sigaps-menu-list").toggle("fast", function () {
            if ($("#sigaps-menu-list-btn").find("svg").attr("class").includes("feather-minus-circle")) {
                $("#sigaps-menu-list-btn").html('<span data-feather="plus-circle"></span>');
            } else {
                $("sigaps-menu-list-btn").html('<span data-feather="minus-circle"></span>');
            }
            feather.replace();
        });
    });
    
    $("#settings-menu-list-btn").on("click", function () {
        $("#settings-menu-list").toggle("fast", function () {
            if ($("#settings-menu-list-btn").find("svg").attr("class").includes("feather-minus-circle")) {
                $("#settings-menu-list-btn").html('<span data-feather="plus-circle"></span>');
            } else {
                $("#settings-menu-list-btn").html('<span data-feather="minus-circle"></span>');
            }
            feather.replace();
        });
    });

    $("#op-menu-list-btn").on("click", function () {
        $("#op-menu-list").toggle("fast", function () {
            if ($("#op-menu-list-btn").find("svg").attr("class").includes("feather-minus-circle")) {
                $("#op-menu-list-btn").html('<span data-feather="plus-circle"></span>');
            } else {
                $("#op-menu-list-btn").html('<span data-feather="minus-circle"></span>');
            }
            feather.replace();
        });
    });

    $("#rec-menu-list-btn").on("click", function () {
        $("#rec-menu-list").toggle("fast", function () {
            if ($("#rec-menu-list-btn").find("svg").attr("class").includes("feather-minus-circle")) {
                $("#rec-menu-list-btn").html('<span data-feather="plus-circle"></span>');
            } else {
                $("#rec-menu-list-btn").html('<span data-feather="minus-circle"></span>');
            }
            feather.replace();
        });
    });

    $("#help-menu-list-btn").on("click", function () {
        $("#help-menu-list").toggle("fast", function () {
            if ($("#help-menu-list-btn").find("svg").attr("class").includes("feather-minus-circle")) {
                $("#help-menu-list-btn").html('<span data-feather="plus-circle"></span>');
            } else {
                $("#help-menu-list-btn").html('<span data-feather="minus-circle"></span>');
            }
            feather.replace();
        });
    });

    $(".nav-item .nav-link").each(function () {
        if (window.location.pathname.split('/')[2] == $(this).attr("href").split('/')[2]) {
            $(this).addClass('active');
        }
    });

    // Call the dataTables jQuery plugin
    var table = $("#dataTable");
    if (table.length){
        table.DataTable({
            keys: true,
            "lengthMenu": [[5, 10, 25, 50, 100, -1], [5, 10, 25, 50, 100, "Todos"]],
            dom:"<'row'<'col-sm-12 col-md-5'l><'col-sm-12 col-md-2'B><'col-sm-12 col-md-5'f>>" +
                "<'row'<'col-sm-12'tr>>" +
                "<'row'<'col-sm-12 col-md-5'i><'col-sm-12 col-md-7'p>>",
            buttons: [
                {
                    extend: 'copyHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'excelHtml5',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                {
                    extend: 'pdfHtml5',
                    orientation: 'landscape',
                    pageSize: 'LEGAL',
                    exportOptions: {
                        columns: ':visible'
                    }
                },
                'colvis'
            ]
            ,
            "language":{
                "decimal":        "",
                "emptyTable":     "No se encontraron registros",
                "info":           "Mostrando _START_ a _END_ de _TOTAL_ registros",
                "infoEmpty":      "Mostrando 0 a 0 de 0 registros",
                "infoFiltered":   "(filtrando _MAX_ de registros)",
                "infoPostFix":    "",
                "thousands":      ",",
                "lengthMenu":     "Mostrar _MENU_ registros",
                "loadingRecords": "Cargando...",
                "processing":     "Procesando...",
                "search":         "Buscar:",
                "zeroRecords":    "No se encontro resultados",
                "paginate": {
                    "first":      "Primero",
                    "last":       "Ultimo",
                    "next":       "Siguiente",
                    "previous":   "Anterior"
                },
                "aria": {
                    "sortAscending":  ": activar para ordenar columna ascendente",
                    "sortDescending": ": activar para ordenar columna descendente"
                }
            }
        });
    }

    var copybtn = $(".buttons-copy");
    var excelbtn = $(".buttons-excel");
    var pdfbtn = $(".buttons-pdf");
    var colbtn = $(".buttons-colvis");
    formatTableDataButtonsStyle(copybtn,"fa-copy");
    formatTableDataButtonsStyle(excelbtn,"fa-file-excel-o");
    formatTableDataButtonsStyle(pdfbtn, "fa-file-pdf-o");
    formatTableDataButtonsStyle(colbtn, "fa-columns");


    // Configure tooltips globally
    $('[data-toggle="tooltip"]').tooltip();

});

function formatTableDataButtonsStyle(btnRef, faStyle){
    btnRef.removeClass("btn-secondary");
    btnRef.addClass("btn-link");
    btnRef.html("<i class='fa "+ faStyle +"' ></i>");
}

//Mover solicitud al siguiente proceso
$(document).on("click","#moveRequestBtn", function () {
    disableRow(selected_row);
    $("#movetonextmodal").modal("hide");
    if(isEstablishmentTable()){
        getRequest("/" + pathName + "/process/record/move-case-file-to-next-process", {id_caseFile: selected_caseFile}, '<i class="fa fa-check" aria-hidden="true"></i> La solicitud se a movido correctamente.');
    }else if(isVehicleTable()){
        getRequest("/" + pathName + "/process/record/move-bunch-to-next-process",{id_bunch:selected_bunch},'<i class="fa fa-check" aria-hidden="true"></i> Se han movido correctamente todas las solicitudes.');
    }
});

//Mover solicitud a olvidadas
$(document).on("click","#forgottenRequestBtn", function () {
    disableRow(selected_row);
    $("#movetoforgotternmodal").modal("hide");
    if(isEstablishmentTable()){
        getRequest("/" + pathName + "/process/record/move-case-file-to-forgotten-request", {id_caseFile: selected_caseFile}, '<i class="fa fa-check" aria-hidden="true"></i> La solicitud seleccionada ha sido removida.');
    }else if(isVehicleTable()){
        getRequest("/" + pathName + "/process/record/move-bunch-to-forgotten-request",{id_bunch:selected_bunch},'<i class="fa fa-check" aria-hidden="true"></i> Todas las solicitudes seleccionadas se removieron del proceso correctamente.')
    }
});

//Mover solicitudes a canceladas
$(document).on("click","#rejectRequestBtn", function () {
    disableRow(selected_row);
    $("#movetorejectedmodal").modal("hide");
    if(isEstablishmentTable()){
        getRequest("/" + pathName + "/process/record/move-case-file-to-canceled-request", {id_caseFile: selected_caseFile}, '<i class="fa fa-check" aria-hidden="true"></i> La solicitud seleccionada fue cancelada.');
    }else if(isVehicleTable()){
        getRequest("/" + pathName + "/process/record/move-bunch-to-canceled-request",{id_bunch:selected_bunch},'<i class="fa fa-check" aria-hidden="true"></i> Todas las solicitudes seleccionadas se cancelaron en el proceso correctamente.')
    }
});

//Desabilita las opciones al modificar registro de una tabla y muestra un mensaje de recomendación para actualizar la pagina
$(document).on("click", ".on-persistence-modification-disable-options", function () {
    var selected_row = $(this).parent().parent().parent();
    $(this).tooltip('hide');
    selected_row.attr("class", "table-active");
    selected_row.find(".table-options").html('<div class="btn-group"><button type="button" class="btn btn-secondary" disabled="disabled">Opciones deshabilitadas</button></div>');
    $(".information-panel").append('<div class="alert alert-info alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Se han modificado algunos registros, <a class="disabled_href reload-custom-page-btn" href="#">Actualizar</a> la pagina para ver los cambios. </div>');
});

function disableRow(selected_row) {
    selected_row.attr("class", "table-active");
    selected_row.find(".table-options").html('<div class="btn-group"><button type="button" class="btn btn-secondary" disabled="disabled">Opciones deshabilitadas</button></div>');
    $(".information-panel").append('<div class="alert alert-info alert-dismissible fade show" role="alert"><button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>Se han modificado algunos registros, <a class="disabled_href reload-custom-page-btn" href="#">Actualizar</a> la pagina para ver los cambios. </div>');
}

/**********************************************************************************************************************/
/*                                      AJAX GET and POST method definition                                           */
/**********************************************************************************************************************/

/*
 * Token and header for post ajax request
 */
var token = $("meta[name='_csrf']").attr("content");
var header = $("meta[name='_csrf_header']").attr("content");

function postRequest(url, data, successMessage) {
    return $.post({
        url: url,
        data: data,
        contentType: "application/json",
        dataType: 'json',
        timeout: 60000
    })
        .done(function (data) {
            if (data.response_status !== undefined) {
                if (data.response_status === 'SUCCESS') {
                    if (successMessage !== undefined) {
                        showToast(successMessage, 15000, true);
                    } else {
                        showToast("Los cambios se guardaron correctamente");
                    }
                } else if (data.response_status === 'WARNING') {
                    showToast("Advertencia: " + data.response_message, 30000, true);
                } else if (data.response_status === 'ERROR') {
                    showToast("Error: " + data.response_message, 30000, true);
                } else if (data.response_status === 'ACCESS_DENIED') {
                    window.location.replace("/" + pathName + "/sessionExpired");
                } else {
                    showToast("Mensaje del servidor: " + data.response_message, 15000, true);
                }
            }
        })
        .fail(function (xhr, status, errorThrown) {
            showSnackbar("ERROR " + xhr.status + " :: " + errorThrown, 30000, true);
        });
}

function getRequest(url, data, successMessage) {
    return $.get({
        url: url,
        data: data,
        contentType: "application/json",
        dataType: 'json',
        timeout: 60000,
        beforeSend: function () {
            showToast('<i class="fa fa-spinner fa-pulse fa-lg fa-fw"></i><span class="sr-only">Cargando...</span> Trabajando...',60000,true);
        }
    })
        .done(function (data) {
            closeAllSnackbar();
            if (data.response_status !== undefined) {
                if (data.response_status === 'SUCCESS') {
                    if (successMessage !== undefined) {
                        showToast(successMessage, 15000, true);
                    }
                } else if (data.response_status === 'WARNING') {
                    showToast("Advertencia: " + data.response_message, 30000, true);
                } else if (data.response_status === 'ERROR') {
                    showToast("Error: " + data.response_message, 30000, true);
                } else if (data.response_status === 'ACCESS_DENIED') {
                    window.location.replace("/" + pathName + "/sessionExpired");
                } else {
                    showToast("Mensaje del servidor: " + data.response_message, 15000, true);
                }
            }
        })
        .fail(function (xhr, status, errorThrown) {
            closeAllSnackbar();
            showSnackbar("ERROR " + xhr.status + " :: " + errorThrown, 30000, true);
        });
    
}

function closeAllSnackbar() {
    if ($(".snackbar").length > 0){
        $(".snackbar").each(function (index) {
            $(this).snackbar("hide");
        })
    }
}
/**********************************************************************************************************************/
/*                                      Snackbar and Toast Method definition                                          */
/**********************************************************************************************************************/
function showSnackbar(message, time, html) {
    $.snackbar({
        content: message,
        timeout: time,
        htmlAllowed: html
    });
}

function showToast(message, time, html) {
    $.snackbar({
        content: message,
        timeout: time,
        style: "toast",
        htmlAllowed: html
    });
}

/**********************************************************************************************************************/
/*                                             Multi-select input definition                                          */
/**********************************************************************************************************************/

$('.multi-select-option').mousedown(function (e) {
    e.preventDefault();
    $(this).prop('selected', !$(this).prop('selected'));
    return false;
});

/**********************************************************************************************************************/
/*                                      Process index data summary methods                                            */
/**********************************************************************************************************************/
function buildEstablishmentSummaryRow(data) {
    var rows = "";
    if (data.length > 0) {
        $.each(data, function (key, value) {
            if (value.casefile_code != undefined && value.casefile_code.length > 0) {
                rows += '<tr>' +
                    '<td>' + value.casefile_code + '</td>' +
                    '<td>' + value.folder_number + '</td>' +
                    '<td>' + value.target_name + '</td>' +
                    '<td>' + value.target_type + '</td>' +
                    '<td>' + value.target_address + '</td>' +
                    '<td>' + value.owner_name + '</td></tr>';
            } else {
                rows += '<tr>' +
                    '<td>' + value.folder_number + '</td>' +
                    '<td>' + value.target_name + '</td>' +
                    '<td>' + value.target_type + '</td>' +
                    '<td>' + value.target_address + '</td>' +
                    '<td>' + value.owner_name + '</td></tr>';
            }
        });
    } else {
        rows = '<tr class="table-info"><td colspan="5" class="text-center">No se encontraron ingresos este d\u00EDa</td></tr>';
    }
    return rows;
}

function buildVehicleSummaryRow(data) {
    var rows = "";
    if (data.length > 0) {
        var current_owner_name = data[0].owner_name;
        var row_count = 0;
        var first_group_row = "";
        var group_row = "";
        $.each(data, function (key, value) {
            if (row_count === 0) {
                first_group_row = '<td>' + value.casefile_code + '</td><td>' + value.target_name + '</td><td>' + value.target_type + '</td><td>' + value.target_address + '</td>';
                row_count++;
            } else if (current_owner_name === value.owner_name) {
                row_count++;
                group_row += '<tr>' +
                    '<td>' + value.casefile_code + '</td>' +
                    '<td>' + value.target_name + '</td>' +
                    '<td>' + value.target_type + '</td>' +
                    '<td>' + value.target_address + '</td></tr>';
            } else {
                rows += '<tr><td rowspan="' + row_count + '">' + value.owner_name + '</td>' + first_group_row + '</tr>' + group_row;
                row_count = 1;
                current_owner_name = value.owner_name;
                first_group_row = '<td>' + value.casefile_code + '</td><td>' + value.target_name + '</td><td>' + value.target_type + '</td><td>' + value.target_address + '</td>';
                group_row = "";
            }
            if ((key + 1) === data.length) {
                rows += '<tr><td rowspan="' + row_count + '">' + value.owner_name + '</td>' + first_group_row + '</tr>' + group_row;
            }
        });
    } else {
        rows = '<tr class="table-info"><td colspan="5" class="text-center">No se encontraron ingresos este d\u00EDa</td></tr>';
    }
    return rows;
}

/**********************************************************************************************************************/
/*                                  Process status modification async methods                                         */
/**********************************************************************************************************************/
$(document).on("click", ".move_to_next_process_btn", function () {
    setTableSelectedRow($(this));
    if(isEstablishmentTable()){
        selected_caseFile = getTableRow4ParentId($(this),".id_caseFile");
    }else if(isVehicleTable()){
        selected_bunch = getTableRow4ParentId($(this),".id_bunch");
    }
    $("#movetonextmodal").modal("show");
});

$(document).on("click", ".move_to_forgotten_btn", function () {
    setTableSelectedRow($(this));
    if(isEstablishmentTable()){
        selected_caseFile = getTableRow4ParentId($(this),".id_caseFile");
    }else if(isVehicleTable()){
        selected_bunch = getTableRow4ParentId($(this),".id_bunch");
    }
    $("#movetoforgotternmodal").modal("show");
});

$(document).on("click", ".move_to_next_rejected_btn", function () {
    setTableSelectedRow($(this));
    if(isEstablishmentTable()){
        selected_caseFile = getTableRow4ParentId($(this),".id_caseFile");
    }else if(isVehicleTable()){
        selected_bunch = getTableRow4ParentId($(this),".id_bunch");
    }
    $("#movetorejectedmodal").modal("show");
});

$(document).on("click",".show_process_record_summary",function () {
    $(".modal").modal("hide");
    var result;
    if(isEstablishmentTable()){
        result = getRequest("/" + pathName + "/record/process/summary",{id_caseFile:getTableRow4ParentId($(this),".id_caseFile")});
    }else if (isVehicleTable()) {
        result = getRequest("/" + pathName + "/record/process/summary", {id_caseFile:getTableRow3ParentId($(this),".id_caseFile")});
    }
    result.done(function (data) {
        var body = '';
        if(data.length > 0){
            $.each(data,function (key, value) {
                var process_status = '<span class="badge badge-secondary">Finalizado</span>';
                if(value.active){
                    process_status = '<span class="badge badge-success">En curso</span>';
                }
                body += '<tr>' +
                    '<td>'+ value.process +'</td>' +
                    '<td class="cell-center-middle">'+ process_status +'</td>' +
                    '<td>'+ value.observation +'</td>' +
                    '<td>'+ value.start +'</td>' +
                    '<td>'+ value.end +'</td>' +
                    '<td class="cell-center-middle">'+ value.modifications +'</td>' +
                    '<td><a href="javascript:">Ver modificaciones</a></td></tr>';
            });
        }else {
            body += '<td class="table-secondary" colspan="7">No se encontro procesos para esta solicitud.</td>'
        }
        $("#process-summary-table-body").html(body);
    });
    $(".snackbar").snackbar("hide");
    $("#process-summary-modal").modal("show");
});
/**********************************************************************************************************************/
/*                                      Table helper methods                                                          */
/**********************************************************************************************************************/
function getTableRow4ParentId(selector,class_name){
    return selector.parent().parent().parent().parent().find(class_name).text();
}

function getTableRow3ParentId(selector,class_name){
    return selector.parent().parent().parent().find(class_name).text();
}

function setTableSelectedRow(selector) {
    selected_row = selector.parent().parent().parent().parent();
}

function isEstablishmentTable() {
    return $(".establishment-table").length > 0;
}

function isVehicleTable() {
    return $(".vehicle-table").length > 0;
}

/**********************************************************************************************************************/
/*                                            Form validation                                                         */
/**********************************************************************************************************************/
$(document).on("click", ".required-type", function () {
    $(".information-panel").append('<div class="alert alert-info" role="alert">Los campos marcador con un * son obligatorios. Complete los datos requeridos.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
});

$(document).on("click",".goback",function () {
    window.history.back();
});

/**********/
/* Utils */
/********/
function checkString(str) {
    if (str === undefined || str === null || str.length === 0) {
        return '<span class="badge badge-pill badge-secondary">No encontrado</span>';
    }
    return str;
}