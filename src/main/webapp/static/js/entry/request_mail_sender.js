$(document).ready(function () {

    $('div#froala-editor').froalaEditor({
        toolbarButtons: ['bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo']
    });

    $("#send_email_btn").on("click",function (e) {
        var document_content = $('div#froala-editor').froalaEditor('html.get');
        if(document_content.length > 0 && validateTo() && validateSubject()){
            showToast('<i class="fa fa-spinner fa-pulse fa-lg fa-fw"></i> Enviando mensaje. Por favor espere',15000,true);
            $("#email_text").val(document_content);
            $("form#main").submit();
        }else {
            showToast('<i class="fa fa-envelope-open-o" aria-hidden="true"></i> El contenido del mensaje no puede estar vacio',15000,true);
        }
    });

    function validateTo() {
        if($("#to_email_list").val().length === 0){
            showToast('<i class="fa fa-envelope-open-o" aria-hidden="true"></i> El destinatario del mensaje no puede estar vacio',5000,true);
            return false
        }
        return true;
    }

    function validateSubject() {
        if($("#email_subject").val().length === 0) {
            showToast('<i class="fa fa-envelope-open-o" aria-hidden="true"></i> El destinatario del mensaje no puede estar vacio',5000,true);
            return false
        }
        return true;
    }

    $("#files").on("change",function () {
        $("#uploaded-files-list").empty();
        for(var i = 0; i < $(this).get(0).files.length; i++){
            $("#uploaded-files-list").append('<li class="list-group-item">' + $(this).get(0).files[i].name + '</li>');
        }
    });

});