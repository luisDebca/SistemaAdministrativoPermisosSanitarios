$(document).on("click", ".request-entry-report-btn", function () {
    if (isEstablishmentTable()) {
        selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
        window.open("/" + pathName + "/dispatch/establishment/request-entry-report.pdf?id_caseFile=" + selected_caseFile);
    } else if (isVehicleTable()) {
        selected_bunch = getTableRow4ParentId($(this), ".id_bunch");
        window.open("/" + pathName + "/dispatch/vehicle/request-entry-report.pdf?id_bunch=" + selected_bunch);
    }
});

$(document).on("click", ".show-ucsf-request-delivery-modal", function () {
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    selected_bunch = getTableRow4ParentId($(this), ".id_bunch");
    setTableSelectedRow($(this));
    $("#ucsf-delivery-confirmation-modal").modal('show');
});

$(".generate-process-validator-report-btn").on("click",function () {
    window.open(encodeURI("/" + pathName + "/report/request-process-validator-"+ selected_casefile +"-for-client-"+ $("#client-select").val() +"-report.pdf"));
    $("#request-validator-client-selection-modal").modal("hide");
});

$(document).on("click", "#update-request-deliver", function () {
    if (isEstablishmentTable()) {
        getRequest("/" + pathName + "/delivery/set-request-ucsf-delivery", {id_caseFile: selected_caseFile}, "Solicitud enviada a UCSF");
    } else if (isVehicleTable()) {
        getRequest("/" + pathName + "/delivery/bunch/set-request-ucsf-delivery", {id_bunch: selected_bunch}, "Todas las solicitudes enviadas a UCSF");
    }
    disableRow(selected_row);
    $("#ucsf-delivery-confirmation-modal").modal('hide');
});

$(document).on("click", "#generate-process-validator-report-btn", function () {
    if (isEstablishmentTable()) {
        window.open(encodeURI("/" + pathName + "/dispatch/establishment/request-process-validation.pdf?id_caseFile=" + selected_caseFile + "&client_name=" + $("#client-select").val()));
    } else if (isVehicleTable()) {
        window.open(encodeURI("/" + pathName + "/dispatch/vehicle/request-process-validation.pdf?id_bunch=" + selected_bunch + "&client_name=" + $("#client-select").val()));
    }
    $("#request-validator-client-selection-modal").modal("hide");
});

$(document).on("click",".show-client-selector-modal",function () {
    selected_owner = getTableRow4ParentId($(this), ".id_owner");
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    selected_bunch = getTableRow4ParentId($(this), ".id_bunch");
    $(".modal").modal('hide');
    var result = getRequest("/" + pathName + "/owner/client/find-all-with-natural-owner", {id: selected_owner});
    result.done(function(data){
        var options = "";
        if(data.length == 0){
            options = "<option disabled='disabled'>No se encontraron personas autorizadas</option>";
            $("#generate-process-validator-report-btn").hide();
        }else{
            $.each(data,function(key,item){
                options += "<option value='" + item.client_name +"'>" + item.client_name + " - " + item.type +"</option>";
            });
            $("#generate-process-validator-report-btn").show();
        }
       $("#client-select").html(options);
    });
    $("#request-validator-client-selection-modal").modal("show");
});


$(document).on("click", ".show-vehicle-folders", function () {
    selected_bunch = getTableRow4ParentId($(this), ".id_bunch");
    window.open(encodeURI("/" + pathName + "/vehicle/vehicle-folders-report.pdf?id_bunch="+selected_bunch));
});


$(document).on("click",".show-record-cover-caseFile-report",function () {
   window.open("/" + pathName + "/request/record-cover-caseFile-report.pdf?id_caseFile=" + getTableRow4ParentId($(this),".id_caseFile"));
});

