$(document).ready(function () {

    $('div#froala-editor').froalaEditor({
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo']
    });

    if($("#document_content").val().length != 0){
        $("#editor-default-text:last-child").html($("#document_content").val());
        $(".footer-options").last().append('<button id="reverse-btn" type="button" class="btn btn-secondary reverse-btn">Deshacer cambios</button>');
    }

    $("#selected-memo").on("change",function(){
        var selected_memo = $(this).val();
        $(".memo-n").text($("#selected-memo option:selected").text());
        $(".P1").text($(".memo-" + selected_memo + " .from-name").text());
        $(".P1C").text($(".memo-" + selected_memo + " .from-charge").text());
        $(".P2").text($(".memo-" + selected_memo + " .for-name").text());
        $(".P2C").text($(".memo-" + selected_memo + " .for-charge").text());
    });

    $("#review-type-select").on("change",function () {
       $("#editor-default-text").html($("#" + $(this).val()).html());
    });

    $("#save-review-btn").on("click",function (e) {
        var document_content = $('div#froala-editor').froalaEditor('html.get');
        $("#casefile_review").val($(".id_casefile").text());
        $("#document_content").val(document_content);
        $("form#main").submit();
    });
});

$(document).on("click",".reverse-btn",function () {
    $("#editor-default-text:last-child").html($("#document_content").val());
});