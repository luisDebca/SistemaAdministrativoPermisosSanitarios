$(document).ready(function () {

    $('div#froala-editor').froalaEditor({
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo']
    });

    if($("#document_content").val().length != 0){
        $("#editor-default-text:last-child").html($("#document_content").val());
        $(".footer-options").last().append('<button id="reverse-btn" type="button" class="btn btn-secondary reverse-btn">Deshacer cambios</button>');
    }

    $("#review-type-select").on("change",function () {
        var template = "";
        if($(this).val() == 'ADMISSIBILITY'){
         template = $("#admissibility-template").html();
        }else if ($(this).val() == 'INADMISSIBILITY'){
            template = $("#inadmissibility-template").html();
        }else if($(this).val() == 'PREVENTION'){
            template = $("#prevention-template").html();
        }
        $("#editor-default-text:last-child").html(template);
    });

    $("#selected-client").on("change",function () {
       $(".selected-client-name").html($("#selected-client option:selected").text().split("<-->")[0]);
       $(".selected-client-type").html($("#selected-client option:selected").text().split("<-->")[1]);
       if($(this).val() == 'PROPIETARIO'){
           $(".owner-as-business-name").html("");
       }else {
           $(".owner-as-business-name").html("de la sociedad " + $(".owner_name").text());
       }
    });

    $("#save-review-btn").on("click",function (e) {
        var document_content = $('div#froala-editor').froalaEditor('html.get');
        $("#casefile_review").val($(".id_casefile").text());
        $("#document_content").val(document_content);
        $("form#main").submit();
    });

    $("#show-legal-review-report-preview-pdf-btn").on("click",function () {
        if($("#review-type-select").val() != undefined){
            if($("#selected-client").val() != undefined){
                window.open("/" + pathName + "/legal/first/review/report-preview.pdf?review_result=" + $("#review-type-select").val() + "&review_type=DICTUM&document_content=" + encodeURIComponent($('div#froala-editor').froalaEditor('html.get')));
            } else {
                showToast("Seleccione 'A nombre de' quien sera el documento", 15000, false);
            }
        } else {
            showToast("Seleccione el tipo de documento para continuar", 15000, false);
        }
    });

    $("#show-legal-resolution-report-preview-pdf-btn").on("click", function () {
        if ($("#review-type-select").val() != undefined) {
            if ($("#selected-client").val() != undefined) {
                window.open("/" + pathName + "/legal/first/resolution/report-preview.pdf?review_result=" + $("#review-type-select").val() + "&review_type=RESOLUTION&document_content=" + encodeURIComponent($('div#froala-editor').froalaEditor('html.get')));
            }else {
                showToast("Seleccione 'A nombre de' quien sera el documento",15000,false);
            }
        }else{
            showToast("Seleccione el tipo de documento para continuar",15000,false);
        }
    });
});

$(document).on("click",".reverse-btn",function () {
    $("#editor-default-text:last-child").html($("#document_content").val());
});