$(document).ready(function () {

    reloadCountDownColumn();

});

$(document).on("click",".generate-legal-first-review-pdf-btn",function () {
    if (isEstablishmentTable()) {
        window.open("/" + pathName + "/legal/first/review/report.pdf?id_caseFile=" + getTableRow4ParentId($(this), ".id_caseFile"));
    } else if (isVehicleTable()) {

    }
});

$(document).on("click input",".page-link,select[name='dataTable_length'],input[type='search']",function () {
    reloadCountDownColumn();
});

$(document).on("click",".generate-resolution-btn",function () {
    if (isEstablishmentTable()) {
        window.open("/" + pathName + "/legal/first/resolution/report.pdf?id_caseFile=" + getTableRow4ParentId($(this), ".id_caseFile"));
    } else if (isVehicleTable()) {

    }
});

function reloadCountDownColumn(){
    $(".timer").each(function(){
        var date = $(this).find(".finish-date-formatted").children().text();
        $(this).countdown(date, {elapse: true})
            .on('update.countdown',function(event){
                if(event.elapsed){
                    $(this).html(event.strftime('<span class="badge badge-danger">Retraso<br> %-m %!m:mes,meses; %-d %!d:dia,dias; %H:%M</span>'));
                }else{
                    if(event.offset.totalDays > 3){
                        $(this).html(event.strftime('<span class="badge badge-success">Faltan<br>%-D %!D:Dia,Dias; %H:%M:%S</span>'));
                    }else {
                        $(this).html(event.strftime('<span class="badge badge-warning">Faltan<br>%-D %!D:Dia,Dias; %H:%M:%S </span>'));
                    }
                }
            });
    });
}