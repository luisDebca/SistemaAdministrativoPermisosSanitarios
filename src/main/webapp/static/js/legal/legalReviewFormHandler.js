$(document).ready(function () {

    $('div#froala-editor').froalaEditor({
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo']
    });

    if ($("#document_content").val().length != 0) {
        $('div#froala-editor').froalaEditor('html.set',$("#document_content").val());
        $(".footer-options").last().append('<button id="reverse-btn" type="button" class="btn btn-secondary reverse-btn">Deshacer cambios</button>');
    }


    $("#selected-memo").on("change",function () {
        $(".P1").html($(".memo-" + $(this).val() + " .for-name").html());
        $(".P1C").html($(".memo-" + $(this).val() + " .for-charge").html());
        $(".P2").html($(".memo-" + $(this).val() + " .from-name").html());
        $(".P2C").html($(".memo-" + $(this).val() + " .from-charge").html());
    });

    $("#save-review-btn").on("click", function (e) {
        var document_content = $('div#froala-editor').froalaEditor('html.get');
        $("#casefile_review").val($(".id_casefile").text());
        $("#document_content").val(document_content);
        if(validateDocumentContent(document_content)){
            $("form#main").submit();
        }
    });

    function validateDocumentContent(content) {
        if (content.length === 0) {
            showToast('<i class="fa fa-flag fa-lg" aria-hidden="true"></i>\n El contenido del documento no puede estar vacio',5000,true);
            return false;
        }
        return true;
    }

});

$(document).on("click", ".reverse-btn", function () {
    $('div#froala-editor').froalaEditor('html.set','');
});