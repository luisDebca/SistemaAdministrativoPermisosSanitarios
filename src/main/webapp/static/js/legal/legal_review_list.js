$(document).ready(function () {
    reloadCountDownColumn();
});

$(document).on("click", ".generate-legal-first-review-pdf-btn", function () {
    window.open("/" + pathName + "/legal/first/review/report.pdf?id_caseFile=" + getTableRow4ParentId($(this), ".id_caseFile"));
});
$(document).on("click", ".generate-bunch-legal-first-review-pdf-btn", function () {
    window.open("/" + pathName + "/legal/first/review/report.pdf?id_caseFile=" + getTableRow3ParentId($(this), ".id_caseFile"));
});

$(document).on("click input", ".page-link,select[name='dataTable_length'],input[type='search']", function () {
    reloadCountDownColumn();
});

$(document).on("click", ".generate-resolution-btn", function () {
    window.open("/" + pathName + "/legal/first/resolution/report.pdf?id_caseFile=" + getTableRow4ParentId($(this), ".id_caseFile"));
});

$(document).on("click", ".generate-bunch-resolution-btn", function () {
    window.open("/" + pathName + "/legal/first/resolution/report.pdf?id_caseFile=" + getTableRow3ParentId($(this), ".id_caseFile"));
});

$(document).on("click", ".generate-legal-second-review-pdf-btn", function () {
    window.open("/" + pathName + "/legal/second/review/opinion-report.pdf?id_caseFile=" + getTableRow4ParentId($(this), ".id_caseFile"));
});

$(document).on("click", ".generate-bunch-second-review-pdf-btn", function () {
    window.open("/" + pathName + "/legal/second/review/opinion-report.pdf?id_caseFile=" + getTableRow3ParentId($(this), ".id_caseFile"));
});

function reloadCountDownColumn() {
    $(".timer").each(function () {
        var date = $(this).find(".delay").text();
        $(this).countdown(date, {elapse: true})
            .on('update.countdown', function (event) {
                if (event.elapsed) {
                    $(this).html(event.strftime('<span class="badge badge-danger">Retraso<br> %-m %!m:mes,meses; %-d %!d:dia,dias; %H:%M</span>'));
                } else {
                    if (event.offset.totalDays > 3) {
                        $(this).html(event.strftime('<span class="badge badge-success">Faltan<br>%-D %!D:Dia,Dias; %H:%M:%S</span>'));
                    } else {
                        $(this).html(event.strftime('<span class="badge badge-warning">Faltan<br>%-D %!D:Dia,Dias; %H:%M:%S </span>'));
                    }
                }
            });
    });
}