
var current_value;

$(document).ready(function () {

    if($(".initialized-municipality-select-list").length){
        current_value = $(".initialized-municipality-select-list").val();
        buildInitializedOptionList("/" + pathName + "/find-all.municipalities-from-department",$(".department-select-list").val(),$(".initialized-municipality-select-list"));
    }

    if($(".initialized-ucsf-select-list").length){
        current_value = $(".initialized-ucsf-select-list").val();
        buildInitializedOptionList("/" + pathName + "/find-all-ucsf-from-sibasi",$(".sibasi-select-list").val(),$(".initialized-ucsf-select-list"));
    }

    if($(".initialized-establishment-types-select-list").length){
        current_value = $(".initialized-establishment-types-select-list").val();
        buildInitializedOptionList("/" + pathName + "/find-all-types-from-section",$(".sections-select-list").val(),$(".initialized-establishment-types-select-list"));
    }

    $(".department-select-list").on("change",function () {
        buildOptionList("/" + pathName + "/find-all.municipalities-from-department",$(".department-select-list").val(),$(".municipality-select-list"));
    });

    $(".sibasi-select-list").on("change",function () {
        buildOptionList("/" + pathName + "/find-all-ucsf-from-sibasi",$(".sibasi-select-list").val(),$(".ucsf-select-list"));
    });

    $(".sections-select-list").on("change",function () {
        buildOptionList("/" + pathName + "/find-all-types-from-section",$(".sections-select-list").val(),$(".establishment-types-select-list"));
    });

    $(".employee-number").each(function () {
        var total = $(".total-employee-number").val()*1 +  $(this).val()*1;
        $(".total-employee-number").val(total);
    });

    $(".employee-number").on("input",function () {
        var current_number = 0;
        $(".employee-number").each(function () {
            current_number += $(this).val()*1;
        });
        $(".total-employee-number").val(current_number);
    });

});

function buildInitializedOptionList(url, identifier, selectRef) {
    var result = getRequest(url,{id:identifier});
    result.done(function (data) {
        var options = "";
        $.each(data,function (key, item) {
            if(item.id == current_value){
                options += "<option value='" + item.id + "' selected='selected'>" + item.value + "</option>";
            }else{
                options += "<option value='" + item.id + "'>" + item.value + "</option>";
            }
        })
        selectRef.html(options);
    })
}


function buildOptionList(url,identifier,selectRef){
    var result = getRequest(url,{id:identifier});
    result.done(function (data) {
        var options = "";
        $.each(data,function (key, item) {
            options += "<option value='"+ item.id +"'>" + item.value + "</option>";
        });
        selectRef.html(options);
    });
}

