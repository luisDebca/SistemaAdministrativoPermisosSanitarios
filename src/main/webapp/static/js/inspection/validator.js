$(document).ready(function () {
    $("#main").validate({
        rules: {
            memorandum_number:{
                required:true,
                maxlength:25
            },
            technical_name:{
                maxlength:100
            },
            technical_charge:{
                maxlength:100
            },
            observation:{
                maxlength:1000
            },
            result_type:{
                required:true
            },
            score:{
                required:true,
                number: true
            },
            extension_received:{
                required:true,
                number: true
            },
            inspected_on:{
                required:true,
                datetimeISO:true
            },
            sibasi_received:{
                required:true,
                datetimeISO:true
            }
        }
    })
});