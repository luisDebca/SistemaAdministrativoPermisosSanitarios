jQuery.datetimepicker.setLocale('es');
jQuery("#inspection-date").datetimepicker({
    format:'Y-m-d H:i',
    timepicker:false
});
jQuery("#sibasi_date").datetimepicker({
    format:'Y-m-d H:i',
    timepicker:false,
    onShow:function( ct ) {
        this.setOptions({
            minDate: jQuery('#inspection-date').val() ? jQuery('#inspection-date').val() : false
        })
    }
});