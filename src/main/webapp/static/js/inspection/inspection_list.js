$(document).on("click",".show-inspection-information-modal",function () {
    var id_inspection = $(this).parent().parent().parent().find(".id_inspection").text();
    var result = getRequest("/" + pathName + "/get-inspection-information",{id_inspection:id_inspection});
    result.done(function (data) {
       $("#i-memo").html(data.inspection_memorandum_number);
        $("#i-inspection-date").html(data.inspection_inspected_on);
        $("#i-sibasi-date").html(data.inspection_sibasi_received);
        $("#i-result").html(data.inspection_result_type);
        $("#i-score").html(data.inspection_score);
        $("#i-extension").html(data.inspection_extension_received);
        $("#i-technical").html(data.inspection_technical_name);
        $("#i-charge").html(data.inspection_technical_charge);
        $("#i-observation").html(data.inspection_observation);
    });
    $("#inspection-information-modal").modal("show");
});

$(document).on("click", "#inspection-report-btn", function () {
    setAllUser();
    $("#user-selector-modal").modal("show");
});

$(document).on("click", "#generate-pdf-btn", function () {
    $("#user-selector-modal").modal("hide");
    if (isEstablishmentTable()) {
        window.open(encodeURI("/" + pathName + "/tracing/establishment/request/" + $("#id_caseFile").text() + "/ucsf-provider-documentation-report.pdf?for_name=" + $("#user_list").val()));
    } else if (isVehicleTable()) {
        window.open(encodeURI("/" + pathName + "/tracing/vehicle/bunch/" + $("#id_bunch").text() + "/ucsf-provider-documentation-report.pdf?for_name=" + $("#user_list").val()));
    }
});

function setAllUser() {
    if ($("#user_list").html().length == 0) {
        var result = getRequest("/" + pathName + "/get-all-users-full-name");
        result.done(function (data) {
            var user_list = "";
            $.each(data, function (index, item) {
                user_list += "<option value='" + item + "'>" + item + "</option>";
            });
            $("#user_list").html(user_list);
        });
    }
}