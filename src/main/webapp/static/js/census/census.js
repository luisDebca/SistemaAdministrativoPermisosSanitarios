$(document).ready(function () {
	 $(".sibasi-select-list-census").on("change",function () {
	        buildOptionListCensus("/" + pathName + "/find-all-ucsf-from-sibasi",$(".sibasi-select-list-census").val(),$(".ucsf-select-list-census"));
	    });
});

function buildOptionListCensus(url,identifier,selectRef){
    var result = getRequest(url,{id:identifier});
    result.done(function (data) {
        var options = "<option value='0' selected>Todas</option>";
        $.each(data,function (key, item) {
            options += "<option value='"+ item.id +"'>" + item.value + "</option>";
        });
        selectRef.html(options);
    });
}
jQuery.datetimepicker.setLocale('es');
jQuery(".date").datetimepicker({
    timepicker:false,
    format:'Y-m-d'
});