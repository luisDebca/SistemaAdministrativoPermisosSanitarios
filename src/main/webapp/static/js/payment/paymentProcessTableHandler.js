$(document).ready(function () {

    reloadCountDownColumn();

    $("#generate-payment-process-report-btn").on("click", function () {
        if ($("#selected-client").val() != null) {
            window.open("/" + pathName + "/payment/payment-procedure-report.pdf?id_caseFile=" + selected_caseFile + "&id_client=" + $("#selected-client").val() + "&client_type=" + $("#selected-client option:selected").text().split(' <--> ')[1]);
        } else {
            showToast("Ha ocurrido un error al recuperar el id.", 15000, false);
        }
        $("#payment-procedure-report-modal").modal("hide");
    });

    $("#accept-delivery-casefile-btn").on("click", function () {
        $("#delivery-confirmation-modal").modal("hide");
        disableRow(selected_row);
        if (isEstablishmentTable()) {
            getRequest("/" + pathName + "/payment/check-payment-procedure-delivery-date", {id_caseFile: selected_caseFile}, "El mandamiento se actualizo a entregado al usuario correctamente.");
        } else if (isVehicleTable()) {
            getRequest("/" + pathName + "/payment/bunch/check-payment-procedure-delivery-date", {id_bunch: selected_bunch}, "Todos los mandamientos se actualizaron a entregados al usuario correctamente.");
        }
    });

    $("#accept-payment-cancellation-btn").on("click", function () {
        $("#payment-cancellation-modal").modal("hide");
        disableRow(selected_row);
        if (isEstablishmentTable()) {
            getRequest("/" + pathName + "/payment/check-payment-procedure-cancellation", {id_caseFile: selected_caseFile}, "El mandamiento de pago se actualizo a cancelado correctamente.");
        } else if ((isVehicleTable())) {
            getRequest("/" + pathName + "/payment/bunch/check-payment-procedure-cancellation", {id_bunch: selected_bunch}, "Todos los mandamientos de pago se actualizaron a cancelado correctamente.");
        }
    });

});

$(document).on("click input", ".page-link,select[name='dataTable_length'],input[type='search']", function () {
    reloadCountDownColumn();
});

$(document).on("click", ".show-generate-payment-process-modal-btn", function () {
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    selected_owner = getTableRow4ParentId($(this), ".id_owner");
    buildClientList();
});

$(document).on("click", ".show-generate-bunch-payment-process-modal-btn", function () {
    selected_caseFile = getTableRow3ParentId($(this), ".id_caseFile");
    selected_owner = getTableRow3ParentId($(this), ".id_owner");
    $(".modal").modal('hide');
    buildClientList();
});

function buildClientList() {
    var response = getRequest("/" + pathName + "/owner/client/find-all-with-natural-owner", {id: selected_owner});
    response.done(function (data) {
        var options = "";
        if (data.length == 0) {
            options = "<option disabled='disabled'>No se encontraron personas autorizadas</option>";
            $("#generate-payment-process-report-btn").hide();
        } else {
            $.each(data, function (key, value) {
                options += "<option value='" + value.id_client + "'>" + value.client_name + " <--> " + value.type + "</option>"
            });
            $("#generate-payment-process-report-btn").show();
        }
        $("#selected-client").html(options);
    });
    $("#payment-procedure-report-modal").modal("show");
}

$(document).on("click", ".show-delivery-confirmation-modal-btn", function () {
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    selected_bunch = getTableRow4ParentId($(this), ".id_bunch");
    setTableSelectedRow($(this));
    $("#delivery-confirmation-modal").modal("show");
});

$(document).on("click", ".show-payment-cancellation-confirmation-modal", function () {
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    selected_bunch = getTableRow4ParentId($(this), ".id_bunch");
    setTableSelectedRow($(this));
    $("#payment-cancellation-modal").modal("show");
});

function reloadCountDownColumn() {
    $(".timer").each(function () {
        var date = $(this).find(".delay").text();
        $(this).countdown(date, {elapse: true})
            .on('update.countdown', function (event) {
                if (event.elapsed) {
                    $(this).html(event.strftime('<span class="badge badge-danger">Retraso<br> %-m %!m:mes,meses; %-d %!d:dia,dias; %H:%M</span>'));
                } else {
                    if (event.offset.totalDays > 3) {
                        $(this).html(event.strftime('<span class="badge badge-success">Faltan<br>%-D %!D:Dia,Dias; %H:%M:%S</span>'));
                    } else {
                        $(this).html(event.strftime('<span class="badge badge-warning">Faltan<br>%-D %!D:Dia,Dias; %H:%M:%S </span>'));
                    }
                }
            });
    });
}