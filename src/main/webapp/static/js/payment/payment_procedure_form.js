$(document).ready(function(){
   $(".mouseSelectedItem").on("click",function(){
       showToast("Ha seleccionado " + $(this).text(), 5000, false);
       $(".selected-amount-preview").text($(this).text());
       $("#rate_code").val($(this).attr("id"));
   });

   $("#clean-selected-amount-preview").on("click",function(){
       $(".selected-amount-preview").text("$0.0");
   });

   $("#save-selected-amount").on("click",function(){
      if(parseFloat($(".selected-amount-preview").text().split('$')[1]) <= 0){
          $(".information-panel").append('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
              'Seleccione un valor para poder continuar' +
              '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
              '<span aria-hidden="true">&times;</span>' +
              '</button></div>')
      }else {
          document.getElementById("main-form").submit();
      }
   });

});