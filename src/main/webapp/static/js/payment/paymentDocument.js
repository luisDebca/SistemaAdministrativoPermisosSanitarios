$(document).ready(function () {
    $(".mouseSelectedItem").on("click", function () {
        showToast("Ha seleccionado " + $(this).text(), 5000, false);
        $("#value").text($(this).text());
        $("#amount").text($(this).text().split('$')[1] * $("#quantity").text());
        $("#rate_code").val($(this).attr("id"));
    });

    $("#clean-selected-amount-preview").on("click", function () {
        $("#value").text("$0.0");
        $("#amount").text("0.0");
    });

    $("#save-selected-amount").on("click", function () {
        if (parseFloat($("#value").text().split('$')[1]) <= 0) {
            $(".information-panel").append('<div class="alert alert-danger alert-dismissible fade show" role="alert">' +
                'Seleccione un valor para poder continuar' +
                '<button type="button" class="close" data-dismiss="alert" aria-label="Close">' +
                '<span aria-hidden="true">&times;</span>' +
                '</button></div>')
        } else {
            document.getElementById("main").submit();
        }
    });

});