$("#main").validate({
    rules:{
        title:{
            required: true,
            minlength:5,
            maxlength:249
        },
        subtitle:{
            minlength:5,
            maxlength:249
        },
        type: "required",
        requirements: {
            required: true
        }
    }
});
