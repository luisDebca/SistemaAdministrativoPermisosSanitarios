$(document).ready(function () {

    $(".delete-template-btn").on("click",function () {
        $("#confirmation-btn").attr("href","./delete-" + $(this).parent().parent().find(".id_template").text());
        $("#modal-delete-confirmation").modal('show');
    });
});

$(document).on("click",".delete-template-btn",function () {
    var id_template = $(this).parent().parent().parent().find(".id_template").text();
    $("#confirmation-btn").attr("href","/" + pathName + "/document-template/" + id_template + "/delete");
    $("#modal-delete-confirmation").modal('show');
});

$(document).on("click",".on-click-show-requirements-list-from-template-btn",function () {
    var id_template = $(this).parent().parent().parent().find(".id_template").text();
    var result = getRequest("/" + pathName + "/document-template/requirement/all",{id:id_template});
    result.done(function (data) {
        var requirements = "";
        $.each(data,function (key,item) {
            requirements += '<tr><td hidden="hidden" class="requirement_template_id">' + id_template + '</td><td hidden="hidden" class="requirement_requirement_id">' + item.id_requirement + '</td><td>' + (key + 1) + '</td><td>' + item.requirement_requirement + '</td><td><div class="btn-group" role="group"><a class="btn btn-outline-primary" href="/' + pathName + '/requirement/' + item.id_requirement + '/edit" role="button"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a><a class="btn btn-outline-danger disabled_href on-click-delete-requirement-template" href="#" role="button"><i class="fa fa-trash-o" aria-hidden="true"></i></a></div></td></tr>';
        });
        if(requirements.length == 0){
            requirements += '<tr class="table-warning"><td colspan="3">No se encontraron requerimientos</td></tr>';
        }
        $("#document-template-requirement-list").html(requirements);
        $("#document-template-requirements-list-modal").modal();
    })
});

$(document).on("click",".on-click-delete-requirement-template",function () {
    var requirement_id = $(this).parent().parent().parent().find(".requirement_requirement_id").text();
    var template_id = $(this).parent().parent().parent().find(".requirement_template_id").text();
    var result = getRequest("/" + pathName + "/document-template/requirement/remove",{id_requirement:requirement_id,id_template:template_id});
    result.done(function (data) {
        if(data.response_status == "SUCCESS"){
            showSnackbar("Requerimiento removido correctamente.",10000,false);
        }else{
            showSnackbar("Ha ocurrido un error: " + data.response_message);
        }
        $("#document-template-requirements-list-modal").modal('hide');
    });
});



