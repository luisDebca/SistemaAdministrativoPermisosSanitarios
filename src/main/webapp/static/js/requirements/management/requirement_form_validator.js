$(document).ready(function () {
   $("#main").validate({
        rules: {
            requirement: {
                required: true,
                    minlength: 5,
                    maxlength: 249
            }
        }
    })
});