var client_list = [];
var requirement_modified_row;

$(".add-client-btn").hide();

$(document).ready(function () {

    $(".save-requirement-row-editor-modifications-btn").on("click", function () {
        $("#requirement-edition-modal").modal('hide');
        var result;
        if(isEstablishmentTable()){
            result = postRequest("/" + pathName + "/requirement-control/update-modified-requirement-rows", JSON.stringify(buildRequirementRowJson()), "Se han guardado los cambios correctamente");
            result.fail(function (data) {showSnackbar("Ha ocurrido un error:" + data.response_message);});
        }else if (isVehicleTable()){
            result = postRequest("/" + pathName + "/requirement-control/bunch/"+ selected_bunch +"/update-modified-requirement-rows", JSON.stringify(buildRequirementRowJson()), "Se han guardado los cambios correctamente");
            result.fail(function (data) {showSnackbar("Ha ocurrido un error:" + data.response_message);});
        }
        disableRowOnAllValid();
    });

    function disableRowOnAllValid() {
        if (isAllRequirementsValid()) {
            disableRow(requirement_modified_row);
        }
    }

    function isAllRequirementsValid(){
        var isValid = true;
        $(".row_status").each(function () {
            if ($(this).val() == 'FAULT') {
                isValid = false;
            }
        });
        return isValid;
    }

    function buildRequirementRowJson(){
        var rows = [];
        $("tr.modified-row").each(function () {
            var row = {
                id_document: $(this).find(".row_document_id").text(),
                id_template: $(this).find(".row_template_id").text(),
                id_requirement: $(this).find(".row_requirement_id").text(),
                row_requirement: $(this).find(".row_requirement").text(),
                row_status: $(this).find(".row_status").val(),
                row_observation: $(this).find(".row_observation").val()
            };
            rows.push(row);
        });
        return rows;
    }

    $(".generate-requirement-document-pdf-report-btn").on("click", function () {
        var client_list = [];
        client_list.push(getApprovedBy(),getSignedBy());
        $("#requirement-document-pdf-options-modal").modal("hide");
        if(isEstablishmentTable()){
            window.open(encodeURI("/" + pathName + "/requirement-control/establishment/case-file/" + selected_caseFile + "/requirement-control-report.pdf?selected_clients_json=" + JSON.stringify(client_list)));
        }else if(isVehicleTable()){
            window.open(encodeURI("/" + pathName + "/requirement-control/vehicle/bunch/" + selected_bunch + "/case-file/requirement-control-report.pdf?selected_clients_json=" + JSON.stringify(client_list)));
        }
    });

    function getApprovedBy(){
        return {
            client_name: splitUsernameFromOption($("#approved-by-select option:selected").text()),
            type: $("#approved-by-select").val()
        };
    }

    function getSignedBy(){
        return {
            client_name: splitUsernameFromOption($("#signed-by-select option:selected").text()),
            type: $("#signed-by-select").val()
        };
    }

    $(".generate-request-reception-pdf-report-btn").on("click", function () {
        var selected_client_option = client_list[$("#client-select").val()];
        $("#request-reception-client-selection-modal").modal('hide');
        if(isEstablishmentTable()){
            window.open(encodeURI("/" + pathName + "/requirement-control/establishment/case-file/" + selected_caseFile + "/request-reception-report.pdf?" +
                "id_client=" + selected_client_option.id_client +
                "&client_name=" + selected_client_option.client_name +
                "&clientType=" + selected_client_option.clientType +
                "&client_DUI=" + selected_client_option.client_DUI +
                "&client_passport=" + selected_client_option.client_passport +
                "&client_resident_card=" + selected_client_option.client_resident_card));
        }else if(isVehicleTable()){
            window.open(encodeURI("/" + pathName + "/requirement-control/vehicle/bunch/"+ selected_bunch +"/case-file/request-reception-report.pdf?" +
                "id_client=" + selected_client_option.id_client +
                "&client_name=" + selected_client_option.client_name +
                "&clientType=" + selected_client_option.clientType +
                "&client_DUI=" + selected_client_option.client_DUI +
                "&client_passport=" + selected_client_option.client_passport +
                "&client_resident_card=" + selected_client_option.client_resident_card));
        }
    });

});

$(document).on("click", ".show-requirement-document-client-selector-modal-btn", function () {
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    selected_owner = getTableRow4ParentId($(this), ".id_owner");
    selected_bunch = getTableRow4ParentId($(this),".id_bunch");
    var result = getRequest("/" + pathName + "/owner/client/find-all-with-natural-owner", {id: selected_owner});
    result.done(function (data) {
        var options = "";
        if (data.length == 0) {
            options = "<option disabled='disabled'>No se encontraron personas autorizadas</option>";
            $(".generate-requirement-document-pdf-report-btn").hide();
            $(".add-client-btn").show();
            $(".add-client-btn").attr("href","/" + pathName +"/owner/" + selected_owner + "/client/new");
        } else {
            $.each(data, function (key, item) {
                if (item.client_type == 'PROPIETARIO') {
                    options += '<option value="' + item.clientType + '" selected="selected">' + item.client_name + " <--> " + item.type + '</option>';
                } else {
                    options += '<option value="' + item.clientType + '">' + item.client_name + " <--> " + item.type + '</option>';
                }
                $(".add-client-btn").hide();
                $(".generate-requirement-document-pdf-report-btn").show();
            });
        }
        $("#approved-by-select,#signed-by-select").html(options);
        $("#requirement-document-pdf-options-modal").modal("show");
    });
});

$(document).on("click", ".requirement-control-request-info-modal-btn", function () {
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    var result = getRequest("/" + pathName + "/requirement-control/establishment/requirement-process-information", {id: selected_caseFile});
    result.done(function (data) {
        $("#info-modal-owner-name").text(data.owner_name);
        $("#info-modal-owner-nit").text(data.owner_nit);
        $("#info-modal-estbl-name").text(data.establishment_name);
        $("#info-modal-estbl-nit").text(data.establishment_nit);
        $("#info-modal-estbl-address").text(data.establishment_address);
        $("#info-modal-estbl-type").text(data.establishment_type);
        $("#info-modal-request-received").text(data.document_created_on);
        $("#info-modal-request-req-completed").text(data.document_approved_requirement_count);
        $("#info-modal-request-req-incomplete").text(data.document_fault_requirement_count);
        $("#info-modal-request-type").text(data.casefile_certification_type);
        if (data.document_status) {
            $("#info-modal-request-status").html('<h3><span class="badge badge-success">Requerimientos Completados</span></h3>');
        } else {
            $("#info-modal-request-status").html('<h3><span class="badge badge-warning">Requerimientos incompletos</span></h3>')
        }
    });
    $("#requirement-control-request-info-modal").modal('show');
});

$(document).on("click", ".show-request-reception-client-selection-modal", function () {
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    selected_owner = getTableRow4ParentId($(this), ".id_owner");
    selected_bunch = getTableRow4ParentId($(this),".id_bunch");
    var result = getRequest("/" + pathName + "/owner/client/find-all-with-natural-owner", {id: selected_owner});
    result.done(function (data) {
        var options = "";
        if (data.length == 0) {
            options = "<option disabled='disabled'>No se encontraron personas autorizadas</option>";
            $(".generate-request-reception-pdf-report-btn").hide();
            $(".add-client-btn").show();
            $(".add-client-btn").attr("href","/" + pathName +"/owner/" + selected_owner + "/client/new");
        } else {
            client_list = data;
            $.each(data, function (key, item) {
                options += '<option value="' + key + '">' + item.client_name + " <--> " + item.type + '</option>';
            });
            $(".add-client-btn").hide();
            $(".generate-request-reception-pdf-report-btn").show();
        }
        $("#client-select").html(options);
    });
    $("#request-reception-client-selection-modal").modal('show');
});

$(document).on("click", ".show-requirement-row-editor-modal-btn", function () {
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    var result = getRequest("/" + pathName + "/requirement-control/find-all-requirement-rows-from-document", {id: selected_caseFile});
    requirement_modified_row = $(this).parent().parent().parent().parent();
    result.done(function (data) {
        buildRequirementRowTable(data);
    });
});

$(document).on("click", ".show-requirement-row-bunch-editor-modal-btn", function () {
    selected_bunch = getTableRow4ParentId($(this), ".id_bunch");
    var result = getRequest("/" + pathName + "/requirement-control/find-all-requirement-rows-from-bunch", {id: selected_bunch});
    requirement_modified_row = $(this).parent().parent().parent().parent();
    result.done(function (data) {
        buildRequirementRowTable(data);
    });
});

$(document).on("change input", ".row_status,.row_observation", function () {
    $(this).closest("tr").attr("class", "modified-row table-success");
});

function splitUsernameFromOption(option) {
    return option.split(" <--> ")[0];
}

function buildRequirementRowTable(data) {
    var table_row = "";
    if (data.length == 0) {
        table_row = '<tr class="table-danger"><td colspan="4">Error. No se encontro requerimientos relacionados con esta solicitud.</td></tr>';
    } else {
        $.each(data, function (key, item) {
            table_row += '<tr><td hidden="hidden" class="row_document_id">' + item.id_document + '</td><td hidden="hidden" class="row_template_id">' + item.id_template + '</td><td hidden="hidden" class="row_requirement_id">' + item.id_requirement + '</td><td>' + (key + 1) + '</td><td width="50%" class="row_requirement">' + item.row_requirement + '</td><td><select class="custom-select row_status on-modification-mark-requirement-row-for-update">';
            if (item.row_status_type == 'APPROVED') {
                table_row += '<option value="APPROVED" selected>SI</option><option value="NOT_APPLY">N/A</option><option value="FAULT">NO</option></select></td>';
            } else if (item.row_status_type == 'NOT_APPLY') {
                table_row += '<option value="APPROVED">SI</option><option value="NOT_APPLY" selected>N/A</option><option value="FAULT">NO</option></select></td>';
            } else {
                table_row += '<option value="APPROVED">SI</option><option value="NOT_APPLY">N/A</option><option value="FAULT" selected>NO</option></select></td>';
            }
            table_row += '<td><textarea class="form-control row_observation on-modification-mark-requirement-row-for-update" cols="15" rows="3" maxlength="249">' + item.row_observation + '</textarea></td></tr>';
        });
    }
    $("#document-requirement-table-body").html(table_row);
    $("#requirement-edition-modal").modal('show');
}


