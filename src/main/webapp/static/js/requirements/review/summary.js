var client_list = [];
$(document).ready(function () {

    var section = $("#section").text();

    $(".generate-requirement-document-pdf-report-btn").on("click", function () {
        var client_list = [];
        client_list.push(getApprovedBy(),getSignedBy());
        $("#requirement-document-pdf-options-modal").modal("hide");
        if(section == 'ESTABLISHMENT'){
            window.open(encodeURI("/" + pathName + "/requirement-control/establishment/case-file/" + selected_caseFile + "/requirement-control-report.pdf?selected_clients_json=" + JSON.stringify(client_list)));
        }else if(section == 'VEHICLE'){
            window.open(encodeURI("/" + pathName + "/requirement-control/vehicle/bunch/" + selected_bunch + "/case-file/requirement-control-report.pdf?selected_clients_json=" + JSON.stringify(client_list)));
        }
    });

    $(".generate-request-reception-pdf-report-btn").on("click", function () {
        var selected_client_option = client_list[$("#client-select").val()];
        $("#request-reception-client-selection-modal").modal('hide');
        if(section == 'ESTABLISHMENT'){
            window.open(encodeURI("/" + pathName + "/requirement-control/establishment/case-file/" + selected_caseFile + "/request-reception-report.pdf?" +
                "id_client=" + selected_client_option.id_client +
                "&client_name=" + selected_client_option.client_name +
                "&clientType=" + selected_client_option.clientType +
                "&client_DUI=" + selected_client_option.client_DUI +
                "&client_passport=" + selected_client_option.client_passport +
                "&client_resident_card=" + selected_client_option.client_resident_card));
        }else if(section == 'VEHICLE'){
            window.open(encodeURI("/" + pathName + "/requirement-control/vehicle/bunch/"+ selected_bunch +"/case-file/request-reception-report.pdf?" +
                "id_client=" + selected_client_option.id_client +
                "&client_name=" + selected_client_option.client_name +
                "&clientType=" + selected_client_option.clientType +
                "&client_DUI=" + selected_client_option.client_DUI +
                "&client_passport=" + selected_client_option.client_passport +
                "&client_resident_card=" + selected_client_option.client_resident_card));
        }
    });

    function getApprovedBy(){
        return {
            client_name: splitUsernameFromOption($("#approved-by-select option:selected").text()),
            type: $("#approved-by-select").val()
        };
    }

    function getSignedBy(){
        return {
            client_name: splitUsernameFromOption($("#signed-by-select option:selected").text()),
            type: $("#signed-by-select").val()
        };
    }

});
$(document).on("click", ".show-requirement-document-client-selector-modal-btn", function () {
    selected_caseFile = $(".id_caseFile").text();
    selected_owner = $(".id_owner").text();
    selected_bunch = $(".id_bunch").text();
    var result = getRequest("/" + pathName + "/owner/client/find-all-with-natural-owner", {id: selected_owner});
    result.done(function (data) {
        var options = "";
        if (data.length == 0) {
            options = "<option disabled='disabled'>No se encontraron personas autorizadas</option>";
            $(".generate-requirement-document-pdf-report-btn").hide();
        } else {
            $.each(data, function (key, item) {
                if (item.client_type == 'PROPIETARIO') {
                    options += '<option value="' + item.clientType + '" selected="selected">' + item.client_name + " <--> " + item.type + '</option>';
                } else {
                    options += '<option value="' + item.clientType + '">' + item.client_name + " <--> " + item.type + '</option>';
                }
                $(".generate-requirement-document-pdf-report-btn").show();
            });
        }
        $("#approved-by-select,#signed-by-select").html(options);
        $("#requirement-document-pdf-options-modal").modal("show");
    });
});

$(document).on("click", ".show-request-reception-client-selection-modal", function () {
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    selected_owner = getTableRow4ParentId($(this), ".id_owner");
    selected_bunch = getTableRow4ParentId($(this),".id_bunch");
    var result = getRequest("/" + pathName + "/owner/client/find-all-with-natural-owner", {id: selected_owner});
    result.done(function (data) {
        var options = "";
        if (data.length == 0) {
            options = "<option disabled='disabled'>No se encontraron personas autorizadas</option>";
            $(".generate-request-reception-pdf-report-btn").hide();
        } else {
            client_list = data;
            $.each(data, function (key, item) {
                options += '<option value="' + key + '">' + item.client_name + " <--> " + item.type + '</option>';
            });
            $(".generate-request-reception-pdf-report-btn").show();
        }
        $("#client-select").html(options);
    });
    $("#request-reception-client-selection-modal").modal('show');
});

function splitUsernameFromOption(option) {
    return option.split(" <--> ")[0];
}