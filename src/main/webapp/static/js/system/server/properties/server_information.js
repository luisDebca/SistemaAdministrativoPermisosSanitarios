var data = [], totalPoints = 60;
var updateInterval = 1000;
var realtime = 'on';
var yaxis = 1024;

var total_memory = 0;
var free_memory = 0;

getRequest("/" + pathName + "/system/max-memory",{}).done(function (data) {
    setYaxis(data/1024/1024);
});

$(document).ready(function () {

    $("#shutdown_server_btn").on("click",function () {
        $("#shutdown_server_confirmation_modal").modal("show");
    });

    $("#shutdown_btn").on("click",function () {
        var password = $("#system_password").val();
        var result = postRequest("/" + pathName + "/system-shutdown", {password: password});
        result.done(function (data) {
            if (data.response_status == "SUCCESS") {
                window.location.replace("/" + pathName + "/logout");
            } else {
                $("#shutdown_server_confirmation_modal").modal("hide");
                showToast("Error:" + data.response_message, 15000, false);
            }
        });
    });

    var total_memory = getRequest("/" + pathName + "/system/total-memory",{}).done(function (data) {
        //alert("total: " + data/1024/1024);
    });
    var max_memory = getRequest("/" + pathName + "/system/max-memory",{}).done(function (data) {
        //alert("max: " + data/1024/1024);
    });
    var free_memory = getRequest("/" + pathName + "/system/free-memory",{}).done(function (data) {
        //alert("free: " + data/1024/1024);
    });


    initData();

    var plot = $.plot('#real_time_chart', [getRandomData()], {
        series: {
            shadowSize: 0,
            color: 'rgb(0, 188, 212)'
        },
        grid: {
            borderColor: '#f3f3f3',
            borderWidth: 1,
            tickColor: '#f3f3f3'
        },
        lines: {
            fill: true
        },
        yaxis: {
            min: 0,
            max: yaxis
        },
        xaxis: {
            min: 0,
            max: 60
        }
    });

    function updateRealTime() {
        plot.setData([getRandomData()]);
        plot.draw();

        var timeout;
        if (realtime === 'on') {
            timeout = setTimeout(updateRealTime, updateInterval);
        } else {
            clearTimeout(timeout);
        }
    }

    updateRealTime();

    $('#realtime').on('change', function () {
        realtime = this.checked ? 'on' : 'off';
        updateRealTime();
    });
});

function getRandomData() {
    if (data.length > 0) data = data.slice(1);

    getRequest("/" + pathName + "/system/total-memory",{}).done(function (data) {
        setTotal_memory(data/1024/1024);
    });
    getRequest("/" + pathName + "/system/free-memory",{}).done(function (data) {
        setFree_memory(data/1024/1024);
    });

    var y;
    while (data.length < totalPoints) {
        y = total_memory - free_memory;
        data.push(y);
    }

    var res = [];
    for (var i = 0; i < data.length; ++i) {
        res.push([i, data[i]])
    }

    return res;
}

function initData() {
    for(var i = 0; i < totalPoints; ++i){
        data.push(0);
    }
}

function setYaxis(number) {
    yaxis = number;
}

function setTotal_memory(number){
    total_memory = number;
}

function setFree_memory(number) {
    free_memory = number;
}