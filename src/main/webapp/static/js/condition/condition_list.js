









$(document).on("click",".show-condition-info-modal",function () {
    var id_condition = $(this).parent().parent().parent().find(".id_condition").text();
    var result = getRequest("/" + pathName + "/resolution-condition/get-condition-info",{id_condition:id_condition});
    result.done(function (data) {
        $("#content").text(data.condition);
        $("#section").text(data.section);
        $("#count").text("0");
    });
    $("#condition-info-modal").modal("show");
});

$(document).on("click",".show-condition-delete-confirmation-modal",function () {
    var id_condition = $(this).parent().parent().parent().find(".id_condition").text();
    $("#condition-delete-btn").attr("href","/" + pathName + "/resolution-condition/" + id_condition + "/delete");
    $("#condition-delete-modal").modal("show");
});