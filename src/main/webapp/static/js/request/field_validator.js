$(document).ready(function () {

    var invalid_field = 0;
    var warning_field = 0;

    $("i").each(function () {
        if ($(this).attr("class").includes("text-danger")) {
            invalid_field++;
        } else if ($(this).attr("class").includes("text-warning")) {
            warning_field++;
        }
    });

    if (invalid_field > 0) {
        $(".information-panel").append('<div class="alert alert-danger" role="alert">La solicitud debe completar los datos minimos requeridos para seguir el proceso. Se encontraron ' + invalid_field + ' campos vacios!</div>');
    } else {
        $(".information-panel").append('<div class="alert alert-success" role="alert">La solicitud puede continuar con el siguiente proceso</div>');
    }

    if (warning_field > 0) {
        $(".information-panel").append('<div class="alert alert-warning" role="alert">Existen ' + warning_field + ' datos incompletos. Se recomienda completar los datos solicitados </div>')
    }

    if (invalid_field == 0) {
        $(".accept-and-continue-btn").show();
    } else {
        $(".accept-and-continue-btn").hide();
        showToast("No se puede continuar el proceso sin resolver los datos requeridos.", 15000, false);
    }

});