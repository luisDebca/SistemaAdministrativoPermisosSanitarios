$(document).ready(function () {
    var establishment_summary = getRequest("/" + pathName + "/casefile/data-completion/latest-establishment-request-summary",{});
    establishment_summary.done(function (data) {
        $("#establishment-summary").html(buildEstablishmentSummaryRow(data));
    });

    var vehicle_summary = getRequest("/" + pathName + "/casefile/data-completion/latest-vehicle-request-summary",{});
    vehicle_summary.done(function (data) {
        $("#vehicle-summary").html(buildVehicleSummaryRow(data));
    });
});