
$(document).ready(function () {

    var defaultId = $("#default-id").text();
    var valueLabel;
    var valueHref;

    $(".set-default-value-btn").on("click",function () {
        valueLabel = $(this).parent().find("p.card-text .badge");
        valueHref = $(this).attr("href");
        $("#new-value").val(valueLabel.text());
       $("#edit-value-modal").modal("show");
    });

    $("#save-default").on("click",function () {
        valueLabel.text($("#new-value").val());
        var result = getRequest("/" + pathName + valueHref,{id_conf:defaultId,value:$("#new-value").val()},'<i class="fa fa-check" aria-hidden="true"></i> Cambios Guardados correctamente.');
        $("#edit-value-modal").modal("hide");
    });
});