$("#main").validate({
    rules: {
        organization_name: {
            required: true,
            maxlength: 150
        },
        business_description:{
            maxlength: 1000
        },
        address:{
            required:true,
            maxlength:500
        },
        department:{
            required:true,
            maxlength:50
        },
        municipality:{
            required:true,
            maxlength:75
        },
        telephone:{
            maxlength:15
        },
        email:{
            maxlength:150
        },
        fax:{
            maxlength:15
        },
        zone:{
            maxlength:25
        },
        availability:{
            maxlength:100
        },
        section_name:{
            required:true,
            maxlength:150
        }
    }
});