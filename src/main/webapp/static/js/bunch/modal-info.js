$(document).on("click",".show_brief_bunch_information",function () {
    selected_owner = getTableRow4ParentId($(this), ".id_owner");
    selected_bunch = getTableRow4ParentId($(this), ".id_bunch");
    var result = getRequest("/" + pathName + "/vehicle/case-file/bunch/all", {
        id_owner: selected_owner,
        id_bunch: selected_bunch
    });
    result.done(function (data) {
        var table_body = "";
        if(data.length == 0){
            table_body = '<tr class="table-danger"><td colspan="7">No se encontraron solicitudes adjuntadas</td></tr>';
        }else{
            $.each(data,function (key, item) {
                number = item.request_number == undefined ? 'No definido' : item.request_number;
                if(item.vehicle_transported_content == 'INVALIDO'){
                    table_body += '<tr class="table-danger">'
                }else{
                    table_body += '<tr>';
                }
                table_body += '<td hidden="hidden" class="id_vehicle">'+ item.id_vehicle +'</td>' +
                    '<td hidden="hidden" class="id_caseFile">'+ item.id_caseFile +'</td>' +
                    '<td hidden="hidden" class="id_owner">' + item.id_owner + '</td>' +
                    '<td>'+ (key + 1) +'</td>' +
                    '<td>'+ number +'</td>' +
                    '<td><a href="/' + pathName + '/vehicle/' + item.id_vehicle + '/edit' + '">' + item.vehicle_license + '</a></td>' +
                    '<td>'+ item.vehicle_type +'</td>' +
                    '<td>'+ item.vehicle_transported_content +'</td>' +
                    '<td>'+ item.vehicle_employees_number +'</td>' +
                    '<td>'+ item.bunch_address +'</td>' +
                    '<td class="table-options cell-center-middle modal-bunch-options"><div class="btn-group" role="group" aria-label="grouInf">'+
                    '<a role="button" href="/'+ pathName +'/caseFile/'+ item.id_caseFile +'/info" class="btn btn-outline-secondary" data-toggle="tooltip" title="Ver detalles de solicitud"><i class="fa fa-folder-open-o"></i></a>' +
                    '<button type="button" class="btn btn-outline-secondary show_process_record_summary" data-toggle="tooltip" title="Ver resumen de proceso"><i class="fa fa-share-alt" aria-hidden="true"></i></button>' +
                    getBunchButtons(item.id_caseFile) +'</div></td></tr>';
            });
        }
        $(".snackbar").snackbar("hide");
        $("#modal_bunch_info_body").html(table_body);
        $("#all-info-btn").attr("href", "/" + pathName + "/vehicle/bunch/" + selected_bunch + "/info");
        $("#bunch-modal-info-edit").attr("href","/" + pathName + "/vehicle/bunch/"+ selected_bunch +"/casefile/edit");
        setAsyncTooltips();
        $("#bunch_info").modal("show");
    });
});

$(document).on("click", ".generate-bunch-vehicle-list-btn", function () {
    window.open("/" + pathName + "/vehicle/bunch/vehicle-list-report.pdf?id_bunch=" + selected_bunch);
});

function getBunchButtons(id_caseFile){
    if ($("#bunch-options").html() == undefined) {
        $(".modal-bunch-options").hide();
        return " ";
    } else {
        if ($("#bunch-options").html().length == 0) {
            $(".modal-bunch-options").hide();
            return " ";
        } else {
            return $("#bunch-options").html().replace("$id_caseFile", id_caseFile);
        }
    }
}

function setAsyncTooltips() {
    $(".generate-bunch-legal-first-review-pdf-btn").tooltip({ boundary: 'window',title: 'Dictamen' });
    $(".generate-bunch-resolution-btn").tooltip({ boundary: 'window',title: 'Resoluci\u00F3n' });
    $(".show-generate-bunch-payment-process-modal-btn").tooltip({ boundary: 'window',title: 'Mandamiento de pago' });
    $(".generate-bunch-second-review-pdf-btn").tooltip({ boundary: 'window',title: 'Opini\u00F3n Jur\u00EDdica' });
}