// Hook up the form so we can prevent it from being posted
var form = document.querySelector("form#main");
form.addEventListener("submit", function (ev) {
    ev.preventDefault();
    handleFormSubmit(form);
});

// Hook up the inputs to validate on the fly
var inputs = document.querySelectorAll("input, textarea, select")
for (var i = 0; i < inputs.length; ++i) {
    inputs.item(i).addEventListener("input", function (ev) {
        var errors = validate(form, constraints) || {};
        showErrorsForInput(this, errors[this.id])
    });
    $(document).on("input",".dynamic-input",function () {
       var errors = validate(form,constraints) || {};
        showErrorsForInput(this, errors[$(this).attr("id")]);
    });
}

function handleFormSubmit(form, input) {
    // validate the form aainst the constraints
    var errors = validate(form, constraints);
    console.log(errors);///////////////////////////////////////////////////////////////////////////////FOR DEBUG ///////////////////////////////////////////////////////////////////////////////////////////////////////////////
    // then we update the form to reflect the results
    showErrors(form, errors || {});
    if (!errors) {
        showSuccess();
    }
}

// Updates the inputs with the validation errors
function showErrors(form, errors) {
    // We loop through all the inputs and show the errors for that input

    _.each(form.querySelectorAll("input[name].form-control, select[name]"), function (input) {
        // Since the errors can be null if no errors were found we need to handle
        // that
        showErrorsForInput(input, errors && errors[input.id]);
    });
}

// Shows the errors for a specific input
function showErrorsForInput(input, errors) {
    // This is the root of the input
    var this_input = input
        // Find where the error messages will be insert into
        , messages = closestParent(input.parentNode, "form-group").querySelector(".messages");
    // First we remove any old messages and resets the classes
    resetFormGroup(this_input);
    // If we have errors
    if (errors) {
        // we first mark the group has having errors
        this_input.classList.add("is-invalid");
        // then we append all the errors
        _.each(errors, function (error) {
            addError(messages, error);
        });
    } else {
        // otherwise we simply mark it as success
        this_input.classList.add("is-valid");
    }
}

// Recusively finds the closest parent that has the specified class
function closestParent(child, className) {
    if (!child || child == document) {
        return null;
    }
    if (child.classList.contains(className)) {
        return child;
    } else {
        return closestParent(child.parentNode, className);
    }
}

function resetFormGroup(this_input) {
    // Remove the success and error classes
    this_input.classList.remove("is-invalid");
    this_input.classList.remove("is-valid");
    // and remove any old messages
    _.each(closestParent(this_input.parentNode, "form-group").querySelectorAll(".help-block.error"), function (el) {
        el.parentNode.removeChild(el);
    });
}

// Adds the specified error with the following markup
// <p class="help-block error">[message]</p>
function addError(messages, error) {
    var block = document.createElement("p");
    block.classList.add("help-block");
    block.classList.add("error");
    block.innerText = error;
    messages.appendChild(block);
}

function showSuccess() {
    document.getElementById("main").submit();
    //alert("Success!");
}
