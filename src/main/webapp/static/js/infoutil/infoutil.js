$(document).ready(function () {
	 $(".sibasi-select-list-infoutil").on("change",function () {
	        buildOptionListCensus("/" + pathName + "/find-all-ucsf-from-sibasi",$(".sibasi-select-list-infoutil").val(),$(".ucsf-select-list-infoutil"));
	    });
});

function buildOptionListCensus(url,identifier,selectRef){
    var result = getRequest(url,{id:identifier});
    result.done(function (data) {
        var options = "<option value='0' selected>Todas</option>";
        $.each(data,function (key, item) {
            options += "<option value='"+ item.id +"'>" + item.value + "</option>";
        });
        selectRef.html(options);
    });
}
jQuery.datetimepicker.setLocale('es');
jQuery(".date").datetimepicker({
    timepicker:false,
    format:'Y-m-d'
});