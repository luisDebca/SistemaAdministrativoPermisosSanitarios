var selected_vehicle;

$(document).on("click", ".show-to-disable-modal", function () {
    selected_vehicle = $(this).parent().parent().parent().find(".id_vehicle").text();
    $("#vehicle-disable-btn").attr("href", "/" + pathName + "/vehicle/" + selected_vehicle + "/to-disable");
    $("#vehicle-to-disable-modal").modal("show");
});

$(document).on("click", ".show-to-enable-modal", function () {
    selected_vehicle = $(this).parent().parent().parent().find(".id_vehicle").text();
    $("#vehicle-enable-btn").attr("href", "/" + pathName + "/vehicle/" + selected_vehicle + "/to-enable");
    $("#vehicle-to-enable-modal").modal("show");
});

$(document).on("click", ".show-delete-modal", function () {
    selected_vehicle = $(this).parent().parent().parent().find(".id_vehicle").text();
    $("#vehicle-delete-btn").attr("href", "/" + pathName + "/vehicle/" + selected_vehicle + "/delete");
    $("#delete-vehicle-modal").modal("show");
});