$( "#main_new" ).validate({
    rules: {
        license: {
            required: true,
            maxlength: 10,
            remote: {
                url: "/" + pathName + "/vehicle/validator/license",
                type: "get",
                data: {
                    name: function () {
                        return $("input[name='license']").val();
                    }
                }
            }
        },
        vehicle_type: {
            required: true,
            maxlength: 25
        },
        transported_content: {
            required: true
        },
        employees_number: {
            number: true
        }
    }
});
$( "#main_edit" ).validate({
    rules: {
        license: {
            required: true,
            maxlength: 10,
            remote: {
                url: "/" + pathName + "/vehicle/validator/license-update",
                type: "get",
                data: {
                    license: function () {
                        return $("input[name='license']").val();
                    },
                    id_vehicle: function () {
                        return $("input[name='id']").val();
                    }
                }
            }
        },
        vehicle_type: {
            required: true,
            maxlength: 25
        },
        transported_content: {
            required: true
        },
        employees_number: {
            number: true
        }
    }
});


