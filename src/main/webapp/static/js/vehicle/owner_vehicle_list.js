var selected_vehicle;

$(document).ready(function () {
    var result = getRequest("/" + pathName + "/find-owner-by-id", {id_owner: $("#owner_id").text()});
    result.done(function (data) {
        $("#owner_name").html(data.owner_name);
        $("#extra-info").append("<p class='mb-0'>" + data.owner_email + "</p>");
        $("#extra-info").append("<p class='mb-0'>" + data.owner_telephone + "</p>");
    });

    if($("tr.table-danger").length > 0){
       $("#information-panel").append('<div class="alert alert-info alert-dismissible fade show" role="alert"><i class="fa fa-info-circle"></i> Las vehÝculos marcados en rojo estan deshabilitados.<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>')
    }
});

$(document).on("click", ".show-to-disable-modal", function () {
    selected_vehicle = $(this).parent().parent().parent().find(".id_vehicle").text();
    $("#vehicle-disable-btn").attr("href", "/" + pathName + "/owner/" + $("#owner_id").text() + "/vehicle/" + selected_vehicle + "/to-disable");
    $("#vehicle-to-disable-modal").modal("show");
});

$(document).on("click", ".show-to-enable-modal", function () {
    selected_vehicle = $(this).parent().parent().parent().find(".id_vehicle").text();
    $("#vehicle-enable-btn").attr("href", "/" + pathName + "/owner/" + $("#owner_id").text() + "/vehicle/" + selected_vehicle + "/to-enable");
    $("#vehicle-to-enable-modal").modal("show");
});

$(document).on("click", ".show-delete-modal", function () {
    selected_vehicle = $(this).parent().parent().parent().find(".id_vehicle").text();
    $("#vehicle-delete-btn").attr("href", "/" + pathName + "/owner/" + $("#owner_id").text() + "/vehicle/" + selected_vehicle + "/delete");
    $("#delete-vehicle-modal").modal("show");
});