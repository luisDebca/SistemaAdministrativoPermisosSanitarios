$(document).ready(function(){
    $("#vehicle-finder-input").on("input",function(){
       if($(this).val().length > 3){
           var result = getRequest("/" + pathName + "/vehicle/find-by-path",{path:$(this).val()});
           result.done(function (data) {
               var table = "";
               if(data.length > 0){
                   table += '<div class="alert alert-success" role="alert"><i class="fa fa-check" aria-hidden="true"></i> '+ data.length +' Resultados encontrados</div>';
                   table += '<table class="table table-hover table-sm"><thead><tr><th>Placa</th><th>Tipo</th><th>Transporta</th><th>Propiedad</th><th>Activo</th></tr></thead><tbody>';
                   $.each(data,function (key, value) {
                       var valid_value = value.active ? '<span class="badge badge-pill badge-success"><i class="fa fa-check"></i></span>':'<span class="badge badge-pill badge-danger"><i class="fa fa-times"></i></span>';
                       table += '<tr><td><a href="'+ '/' + pathName + '/vehicle/' + value.id_vehicle + '/info' +'">' +value.vehicle_license + '</a></td><td>'+ value.vehicle_type +'</td><td>'+ value.vehicle_transported_content +'</td><td>'+ value.owner_name +'</td><td>'+ valid_value +'</td></tr>';
                   });
                   table += '</tbody></table>';
               }else {
                   table += '<div class="alert alert-warning" role="alert"><i class="fa fa-exclamation" aria-hidden="true"></i>  <strong>Sin resultados</strong></div>';
               }
               $("#search_result").html(table);
           });
       } else if($(this).val().includes("%")){
           showToast("No deberias de intentar hacer injecci\u00F3n SQL.",15,false);
           $(this).val("");
       } else {
           $("#search_result").empty();
       }
    });
});