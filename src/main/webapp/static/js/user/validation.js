$(document).ready(function () {

    var user_profiles = $("#user_profile_list").text().split(",");
    var profile_id = [];

    for(var index = 0; index < user_profiles.length; index++){
        if(index % 2 == 0) {
            profile_id.push(user_profiles[index]);
        }
    }

    $("#user_profile_multiselect option").each(function () {
        for(var index = 0; index < profile_id.length; index++){
            if ($(this).attr("value") == profile_id[index].split("=")[1]){
                $(this).attr("selected","selected");
            }
        }
    });

    $("#user_profile_multiselect").multiSelect();
    $("#new_user_profiles").multiSelect();

   $("#main").validate({
       rules: {
           first_name:{
               required: true,
               minlength: 2,
               maxlength: 99
           },
           last_name:{
               required: true,
               minlength: 2,
               maxlength: 99
           },
           designation:{
               maxlength: 10
           },
           charge:{
               maxlength: 149
           },
           email:{
               email: true,
               maxlength: 149
           },
           "user.username":{
               required: true,
               minlength: 4,
               maxlength: 15,
               remote: {
                   url: "/" + pathName + "/user/validator/username",
                   type: "get",
                   data: {
                       username: function () {
                           return $("input[name='user.username']").val();
                       },
                       id_user: function () {
                           return $("input[name='user.id']").val();
                       }
                   }
               }
           },
           "user.password":{
               required: true,
               minlength: 6,
               maxlength: 60
           },
           "user_profiles":{
               required:true
           },
           user_password_confirmation:{
               required: true,
               minlength: 6,
               equalTo: "#personal_user_password"
           }
       }
   });
});
