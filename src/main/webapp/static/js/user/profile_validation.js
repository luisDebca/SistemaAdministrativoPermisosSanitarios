$("#main").validate({
    rules: {
        first_name: {
            required: true,
            minlength: 2,
            maxlength: 99
        },
        last_name: {
            required: true,
            minlength: 2,
            maxlength: 99
        },
        designation: {
            maxlength: 10
        },
        charge: {
            maxlength: 149
        },
        email: {
            email: true,
            maxlength: 149
        },
        "user.username": {
            required: true,
            minlength: 4,
            maxlength: 15,
            remote: {
                url: "/" + pathName + "/user/validator/username",
                type: "get",
                data: {
                    username: function () {
                        return $("input[name='user.username']").val();
                    },
                    id_user: function () {
                        return $("input[name='user.id']").val();
                    }
                }
            }
        }
    }
});