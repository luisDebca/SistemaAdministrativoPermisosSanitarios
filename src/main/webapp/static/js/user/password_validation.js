$("#main").validate({
    rules: {
        new_password: {
            required: true,
            minlength: 6,
            maxlength: 60
        },
        current_password: {
            required: true,
            remote: {
                url: "/" + pathName + "/user/validator/password",
                type: "post",
                data: {
                    password: function () {
                        return $("input[name='current_password']").val();
                    }
                }
            }
        },
        current_password_confirmation: {
            required: true,
            minlength: 6,
            equalTo: "#new_password"
        }
    }
});