$(document).on("click", ".complete-process", function () {
    selected_caseFile = getTableRow4ParentId($(this), ".id_caseFile");
    selected_bunch = getTableRow4ParentId($(this), ".id_bunch");
    setTableSelectedRow($(this));
    $("#complete-process-modal").modal("show");
});

$("#complete-btn").on("click", function () {
    if (isEstablishmentTable()) {
        getRequest("/" + pathName + "/request/delivery/case-file/complete-process", {id_caseFile: selected_caseFile}, "Solicitud finalizada correctamente.");
    }else if (isVehicleTable()) {
        getRequest("/" + pathName + "/request/delivery/bunch/complete-process", {id_bunch:selected_bunch}, "Todas las solicitudes finalizadas correctamente.")
    }
    $("#complete-process-modal").modal("hide");
    disableRow(selected_row);
});