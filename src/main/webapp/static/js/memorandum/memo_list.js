$(document).ready(function () {
   $(".show-memo-remove-modal").on("click",function () {
       var id_caseFile = $(this).parent().parent().parent().find(".id_casefile").text();
       var id_memo = $(this).parent().parent().parent().find(".id_memo").text();
       $(".confirm-memo-removal-btn").attr("href","/" + pathName + "/resolution/memorandum/" + id_caseFile + "/delete/" + id_memo);
       $("#memo-remove-modal").modal("show");
   });

   $(".generate-memorandum-report").on("click",function () {
       var id_memo = $(this).parent().parent().parent().find(".id_memo").text();
       window.open("/" + pathName + "/memorandum/memo-report.pdf?id_memo=" + id_memo);
   });
});