$(document).ready(function () {
    $('div#froala-editor').froalaEditor({
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo']
    });

    var result = getRequest("/" + pathName + "/get-all-users-full-name-and-charge",{});
    result.done(function (data) {
        $.each(data,function (index,value) {
            if(value !== undefined && value !== null){
                $("#for-select").append("<option value='"+ value +"'>"+ value +"</option>");
                $("#from-select").append("<option value='"+ value +"'>"+ value +"</option>");
                $("#through-select").append("<option value='"+ value +"'>"+ value +"</option>");
            }else {
                $("#for-select").append("<option value='0' disabled='disabled'>No se encontraron usuarios</option>");
                $("#from-select").append("<option value='0' disabled='disabled'>No se encontraron usuarios</option>");
                $("#through-select").append("<option value='0' disabled='disabled'>No se encontraron usuarios</option>");
            }
        });
    });

    $("#save-memo-btn").on("click",function (e) {
        if($('div#froala-editor').froalaEditor('html.get').length == 0){
            showToast("El memorandum no puede estar vac\u00EDo",10000,false)
        }else{
            var document_content = $('div#froala-editor').froalaEditor('html.get');
            $("#document_content").val(document_content);
            $("form#main").submit();
        }
    });

    if($("#document_content").val().length != 0){
        $('div#froala-editor').froalaEditor('html.set',$("#document_content").val());
    }

    $("#for-select").on("change",function () {
       $("#for_name").val($(this).val().split(' - ')[0]);
        $("#for_charge").val($(this).val().split(' - ')[1]);
    });

    $("#from-select").on("change",function () {
        $("#from_name").val($(this).val().split(' - ')[0]);
        $("#from_charge").val($(this).val().split(' - ')[1]);
    });

    $("#through-select").on("change",function () {
        $("#through_name").val($(this).val().split(' - ')[0]);
        $("#through_charge").val($(this).val().split(' - ')[1]);
    });
});