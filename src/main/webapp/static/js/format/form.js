$('div#froala-editor').froalaEditor({
    toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo']
});

$(document).on("click",".save-format", function () {
    if ($('div#froala-editor').froalaEditor('html.get').length > 0 && validateReportFormatName() && validateFormatType()) {
        if ($('div#froala-editor').froalaEditor('html.get').length < 5000){
            $("#format").val($('div#froala-editor').froalaEditor('html.get'));
            document.getElementById("main").submit();
        } else {
            showToast("El texto de formato sobrepasa el limite.",5000,false);
        }
    }else {
        showToast("El formato no puede estar vac�o.",5000,false);
    }
});

$(".add-text-btn").on("click",function () {
    var text = $(this).data('text');
    var buffer = $('div#froala-editor').froalaEditor('html.get');
    if(buffer.length > 3){
        buffer = buffer.substring(0,buffer.length - 4)
    }
    $('div#froala-editor').froalaEditor('html.set',buffer + ' .' + text + '</b>');
});

$(document).ready(function () {

    if($("#format").val().length > 0){
        $("#editor-default-text").replaceWith($("#format").val());
    }
});

function validateReportFormatName() {
    if ($("#name").val().length){
        return true
    } else {
        showToast("Ingrese un nombre de formato valido.",5000,false);
        return false
    }
}

function validateFormatType() {
    if ($("#type").val() === "0"){
        showToast("Seleccione un tipo de formato valido.",5000,false);
        return false
    } else {
        return true;
    }
}

