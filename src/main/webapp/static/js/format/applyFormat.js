var buffer;
var editor = $('div#froala-editor');

$("#format-select").on("change",function () {
    var format = document.getElementById($(this).val()).innerHTML;
    $('div#froala-editor').froalaEditor('html.set',checkFormat(format));
});

$('#selected-client').on('change',function () {
    reformat();
});

$('#review-type-select').on('change', function () {
   reformat();
});

function reformat() {
    editor.froalaEditor('html.set',checkFormat(document.getElementById($("#format-select").val()).innerHTML));
}

function checkFormat(buffer) {
    var format = buffer;
    format = format.replace('.FECHA_ACTUAL', $('#current-date').text());
    format = format.replace('.FECHA_INICIO', $('#start-date').text());
    format = format.replace('.FECHA_FINAL', $('#end-date').text());
    format = format.replace('.FOLDER', $('#folder').text());
    format = format.replace('.SOLICITUD', $('#request').text());
    format = format.replace('.RESOLUCION', $('#resolution').text());
    format = format.replace('.PROPIETARIO_NOMBRE', $('#owner-name').text());
    format = format.replace('.ESTABLECIMIENTO_NOMBRE', $('#establishment-name').text());
    format = format.replace('.ESTABLECIMIENTO_NIT', $('#establishment-nit').text());
    format = format.replace('.ESTABLECIMIENTO_DIRECCION', $('#establishment-address').text());
    format = format.replace('.ESTABLECIMIENTO_TIPO', $('#establishment-type').text());
    format = format.replace('.VEHICULO_PLACA', $('#vehicle-licence').text());
    format = format.replace('.VEHICULO_TIPO', $('#vehicle-type').text());
    format = format.replace('.VEHICULO_TRASPORTE', $('#vehicle-transport').text());
    format = format.replace('.VEHICULO_DIRECCION', $('#vehicle-address').text());
    format = format.replace('.MEMORANDUM_NUMERO', $('#memo-number').text());
    format = format.replace('.USUARIO_NOMBRE', $("#selected-client option:selected").text().split("<-->")[0]);
    format = format.replace('.USUARIO_TIPO', $("#selected-client option:selected").text().split("<-->")[1]);
    format = format.replace('.TIPO_DOCUMENTO',$("#review-type-select option:selected").text());
    format = format.replace('.UCSF', $("#ucsf").text());
    format = format.replace('.SIBASI', $("#sibasi").text());
    return format;
}