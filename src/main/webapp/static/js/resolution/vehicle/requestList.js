var resolutionName = $("#resolution-signatory-name");
var resolutionCharge = $("#resolution-signatory-charge");
var diplomaName = $("#diploma-signatory-name");
var diplomaCharge = $("#diploma-signatory-charge");
var resolutionModal = $("#resolution-report-modal");
var diplomaModal = $("#diploma-report-modal");

$(document).on("click",".generate-resolution-report-btn",function () {
    selected_bunch = getTableRow4ParentId($(this),".id_bunch");
    resolutionModal.modal("show");
});

$(document).on("click",".generate-diploma-report-btn",function () {
    selected_bunch = getTableRow4ParentId($(this),".id_bunch");
    diplomaModal.modal("show");
});

$(document).on("click",".generate-resolution-report",function () {
    resolutionModal.modal("hide");
    if(resolutionName.val().length > 0 || resolutionCharge.val().length > 0){
        window.open(encodeURI("/" + pathName + "/resolution/vehicle/bunch/resolution-report.pdf?id_bunch=" + selected_bunch +
            "&signatory_name=" + resolutionName.val() +
            "&signatory_charge=" + resolutionCharge.val()));
    }else {
        showSnackbar("Ingrese el nombre y cargo para la resolución.",10000,false);
    }
});

$(document).on("click",".generate-diploma-report",function () {
    diplomaModal.modal("hide");
    if(diplomaName.val().length > 0 || diplomaCharge.val().length > 0){
        window.open(encodeURI("/" + pathName + "/resolution/vehicle/bunch/diploma-report.pdf?id_bunch=" + selected_bunch +
            "&signatory_name=" + diplomaName.val() +
            "&signatory_charge=" + diplomaCharge.val()));
    }else {
        showSnackbar("Ingrese el nombre y cargo para el diploma.",10000,false);
    }
});