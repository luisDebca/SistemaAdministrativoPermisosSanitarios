var selected_names = [];
var name_count = 1;
$(document).ready(function () {

    hideExtraNameInputs();

    $('#control-sheet-modal').on('hidden.bs.modal', function (e) {
        name_count = 1;
        $(".control_sheet_names").val("");
        hideExtraNameInputs();
    });

    $('#resolution-report-modal').on('hidden.bs.modal', function (e) {
        $("#signatory-name").val("");
        $("#signatory-charge").val("");
    });

    $(".generate-control-sheet-report").on("click", function () {
        $("#control-sheet-modal").modal("hide");
        selected_names = [];
        var name1 = {index: 1, name: $("#name1").val()};
        var name2 = {index: 2, name: $("#name2").val()};
        var name3 = {index: 3, name: $("#name3").val()};
        var name4 = {index: 4, name: $("#name4").val()};
        var name5 = {index: 5, name: $("#name5").val()};
        if (name1.name.length > 0) {
            selected_names.push(name1);
        }
        if (name2.name.length > 0) {
            selected_names.push(name2);
        }
        if (name3.name.length > 0) {
            selected_names.push(name3);
        }
        if (name4.name.length > 0) {
            selected_names.push(name4);
        }
        if (name5.name.length > 0) {
            selected_names.push(name5);
        }
        window.open(encodeURI("/" + pathName + "/resolution/establishment/control-sheet-report.pdf?id_caseFile=" + selected_caseFile + "&selected_names_json=" + JSON.stringify(selected_names)));
    });

    $("#add-new-control-sheet-name").on("click", function () {
        if (name_count < 5) {
            $("#control-sheet-form .form-group").eq(name_count).show();
        } else {
            $("#control-sheet-form").append('<div class="alert alert-warning" role="alert">Solo se permiten 5 nombres maximo</div>');
            $("#add-new-control-sheet-name").hide();
        }
        name_count++;
    });

    $(".generate-resolution-report").on("click", function () {
        var name = $("#signatory-name").val();
        var charge = $("#signatory-charge").val();
        $("#resolution-report-modal").modal("hide");
        if (name.length > 0 && charge.length > 0) {
            window.open(encodeURI("/" + pathName + "/resolution/establishment/resolution-report.pdf?id_resolution=" + selected_caseFile + "&signatory_name=" + name + "&signatory_charge=" + charge));
        } else {
            showToast("Ingrese el nombre y cargo correctamente.");
        }
    });

    $(".generate-diploma-report").on("click", function () {
        var name = $("#diploma-signatory-name").val();
        var charge = $("#diploma-signatory-charge").val();
        $("#diploma-report-modal").modal("hide");
        if (name.length > 0 && charge.length > 0) {
            window.open(encodeURI("/" + pathName + "/resolution/establishment/diploma-report.pdf?id_caseFile=" + selected_caseFile + "&signatory_name=" + name + "&signatory_charge=" + charge));
        } else {
            showToast("Ingrese el nombre y cargo correctamente.");
        }
    });
});

$(document).on("click", ".show-diploma-signatory-selector-modal", function () {
    selected_caseFile = getTableRow4ParentId($(this),".id_caseFile");
    $("#diploma-report-modal").modal("show");
});

$(document).on("click", ".show-control-sheet-modal", function () {
    selected_caseFile = getTableRow4ParentId($(this),".id_caseFile");
    $("#control-sheet-modal").modal("show");
});

$(document).on("click", ".show-resolution-signatory-selector-modal", function () {
    selected_caseFile = getTableRow4ParentId($(this),".id_caseFile");
    $("#resolution-report-modal").modal("show");
});

function hideExtraNameInputs() {
    $("#control-sheet-form .form-group").eq(1).hide();
    $("#control-sheet-form .form-group").eq(2).hide();
    $("#control-sheet-form .form-group").eq(3).hide();
    $("#control-sheet-form .form-group").eq(4).hide();
    $("#control-sheet-form .alert").remove();
    $("#add-new-control-sheet-name").show();
}