$(document).ready(function () {

    $('div#froala-editor').froalaEditor({
        toolbarButtons: ['fullscreen', 'bold', 'italic', 'underline', 'strikeThrough', 'subscript', 'superscript', '|', 'fontFamily', 'fontSize', 'color', 'inlineStyle', 'paragraphStyle', '|', 'paragraphFormat', 'align', 'formatOL', 'formatUL', 'outdent', 'indent', 'quote', '-', 'insertLink', 'insertImage', 'insertVideo', 'insertFile', 'insertTable', '|', 'emoticons', 'specialCharacters', 'insertHR', 'selectAll', 'clearFormatting', '|', 'print', 'help', 'html', '|', 'undo', 'redo']
    });

    $("#inspection_memo").on("change", function () {
        $("#editor-default-text:last-child").html($("#" + $(this).val()).html());
    });

    if($("#document_content").val().length > 0){
        $("#editor-default-text:last-child").html($("#document_content").val());
    }
});

$(document).on("click","#save-resolution-btn",function (e) {
    var document_content = $('div#froala-editor').froalaEditor('html.get');
    $("#document_content").val(document_content);
    $("form#main").submit();
});

