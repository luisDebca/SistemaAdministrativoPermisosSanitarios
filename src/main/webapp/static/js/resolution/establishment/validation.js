$("#main").validate({
    rules: {
        resolution_number: {
            required: true,
            digits: true,
            remote: {
                url: "/" + pathName + "/resolution/validator/resolution_number",
                type: "get",
                data: {
                    id_caseFile: function () {
                        return $("input[name='id']").val();
                    },
                    number: function () {
                        return $("input[name='resolution_number']").val();
                    },
                    code: function () {
                        return $("input[name='resolution_code']").val();
                    },
                    year: function () {
                        return $("input[name='resolution_year']").val();
                    }
                }
            }
        },
        resolution_code: {
            required: true,
            maxlength: 5,
            remote: {
                url: "/" + pathName + "/resolution/validator/resolution_number",
                type: "get",
                data: {
                    id_caseFile: function () {
                        return $("input[name='id']").val();
                    },
                    number: function () {
                        return $("input[name='resolution_number']").val();
                    },
                    code: function () {
                        return $("input[name='resolution_code']").val();
                    },
                    year: function () {
                        return $("input[name='resolution_year']").val();
                    }
                }
            }
        },
        resolution_year: {
            required: true,
            digits: true,
            maxlength: 2,
            remote: {
                url: "/" + pathName + "/resolution/validator/resolution_number",
                type: "get",
                data: {
                    id_caseFile: function () {
                        return $("input[name='id']").val();
                    },
                    number: function () {
                        return $("input[name='resolution_number']").val();
                    },
                    code: function () {
                        return $("input[name='resolution_code']").val();
                    },
                    year: function () {
                        return $("input[name='resolution_year']").val();
                    }
                }
            }
        },
        conditions: {
            required: true
        },
        inspection_memo:{
            required: true
        }
    }
});