/*
*Controlador de mensaje de bienvenida para cada pagina de inicio
* */
var date = new Date();
if(date.getHours() >= 0 && date.getHours() <= 6){
    document.getElementById("wellcome-message").innerHTML = "Buenos D\u00EDas";
}else if(date.getHours() > 6 && date.getHours() <= 11){
    document.getElementById("wellcome-message").innerHTML ="Buenos D\u00EDas";
}else if(date.getHours() > 11 && date.getHours() <= 18){
    document.getElementById("wellcome-message").innerHTML = "Buenas Tardes";
}else if (date.getHours() > 18 && date.getHours() <= 23) {
    document.getElementById("wellcome-message").innerHTML = "Buenas Noches";
}else {
    document.getElementById("wellcome-message").innerHTML = "Bienvenido";
}