var currentProcess = document.getElementById("thisProcess").textContent;
var esTodayTab = document.getElementById("es_today-count");
var esWeekTab = document.getElementById("es_week-count");
var esMonthTab = document.getElementById("es_month-count");
var esTodayTable = document.getElementById("es_today-summary");
var esWeekTable = document.getElementById("es_week-summary");
var esMonthTable = document.getElementById("es_month-summary");
var vhTodayTab = document.getElementById("vh_today-count");
var vhWeekTab = document.getElementById("vh_week-count");
var vhMonthTab = document.getElementById("vh_month-count");
var vhTodayTable = document.getElementById("vh_today-summary");
var vhWeekTable = document.getElementById("vh_week-summary");
var vhMonthTable = document.getElementById("vh_month-summary");
var result;

getRequest("/" + pathName + "/summary/count/establishment/today", {process:currentProcess}).done(function (data) {esTodayTab.innerHTML = data;});
getRequest("/" + pathName + "/summary/count/establishment/week", {process:currentProcess}).done(function (data) {esWeekTab.innerHTML = data;});
getRequest("/" + pathName + "/summary/count/establishment/month", {process:currentProcess}).done(function (data) {esMonthTab.innerHTML = data;});

getRequest("/" + pathName + "/summary/count/vehicle/today", {process:currentProcess}).done(function (data) {vhTodayTab.innerHTML = data;});
getRequest("/" + pathName + "/summary/count/vehicle/week", {process:currentProcess}).done(function (data) {vhWeekTab.innerHTML = data;});
getRequest("/" + pathName + "/summary/count/vehicle/month", {process:currentProcess}).done(function (data) {vhMonthTab.innerHTML = data;});

getRequest("/" + pathName + "/summary/request/establishment/today",{process:currentProcess})
    .done(function (data) {fillEstablishmentSummaryTable(data,esTodayTable);});

getRequest("/" + pathName + "/summary/request/vehicle/today",{process:currentProcess})
    .done(function (data) {fillVehicleSummaryTable(data,vhTodayTable);});

document.getElementById("es_today-tab").addEventListener("click", function (ev) { showEstablishmentTable("today",esTodayTable) });
document.getElementById("es_week-tab").addEventListener("click", function (ev) { showEstablishmentTable("week",esWeekTable) });
document.getElementById("es_month-tab").addEventListener("click", function (ev) { showEstablishmentTable("month",esMonthTable) });

document.getElementById("vh_today-tab").addEventListener("click", function (ev) { showVehicleTable("today",vhTodayTable) });
document.getElementById("vh_week-tab").addEventListener("click", function (ev) { showVehicleTable("week",vhWeekTable) });
document.getElementById("vh_month-tab").addEventListener("click", function (ev) { showVehicleTable("month",vhMonthTable) });


function showEstablishmentTable(date, table) {
    getRequest("/" + pathName + "/summary/request/establishment/" + date, {process:currentProcess})
        .done(function (data) {
            fillEstablishmentSummaryTable(data,table);
        });
}

function showVehicleTable(date, table) {
    getRequest("/" + pathName + "/summary/request/vehicle/" + date, {process:currentProcess})
        .done(function (data) {
            fillVehicleSummaryTable(data,table);
        });
}

function fillEstablishmentSummaryTable(data,tableBody) {
    if (data.length) {
        var body = '';
        $.each(data, function(key,value){
            body += '<tr>' +
                '<td>'+ value.folder +'</td>' +
                '<td>'+ value.request +'</td>' +
                '<td>'+ value.resolution +'</td>' +
                '<td>'+ value.target +'</td>' +
                '<td>'+ value.address +'</td>' +
                '<td>'+ value.owner +'</td>' +
                '</tr>';
        });
        tableBody.innerHTML = body;
    }else {
        tableBody.innerHTML = '<tr class="table-active text-center"><td colspan="6">Sin resultados</td></tr>';
    }
}

function fillVehicleSummaryTable(data, tableBody) {
    if(data.length) {
        var body = '';
        $.each(data, function (key, value) {
            body += '<tr>' +
                '<td>'+ value.folder +'</td>' +
                '<td>'+ value.target +'</td>' +
                '<td>'+ value.address +'</td>' +
                '<td>'+ value.owner +'</td>' +
                '</tr>';
        });
        tableBody.innerHTML = body;
    }else {
        tableBody.innerHTML = '<tr class="table-active text-center"><td colspan="4">Sin resultados</td></tr>';
    }
}