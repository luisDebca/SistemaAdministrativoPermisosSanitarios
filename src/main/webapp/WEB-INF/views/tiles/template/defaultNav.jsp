<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<nav id="mainNav" class="navbar navbar-dark fixed-top bg-dark flex-md-nowrap p-0 shadow">
	<a class="navbar-brand col-sm-3 col-md-2 mr-0" href="#">APS</a>

	<ul class="navbar-nav px-3">
		<li class="nav-item text-nowrap">
			<a class="nav-link" href="<c:url value="/my-profile"/>"><span data-feather="user"></span>
				<security:authorize access="isAuthenticated()">
					<security:authentication property="principal.username"/>
				</security:authorize>
			</a>
		</li>
	</ul>
</nav>