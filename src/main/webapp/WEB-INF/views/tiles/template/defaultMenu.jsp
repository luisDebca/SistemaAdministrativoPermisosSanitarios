<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<nav class="col-md-2 d-none d-md-block bg-light sidebar">
	<div class="sidebar-sticky pb-5">
		<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
			<span>Procesos</span>
			<a id="process-menu-list-btn" class="d-flex align-items-center text-muted disabled_href" href="javascript:">
				<span data-feather="minus-circle"></span>
			</a>
		</h6>
		<ul class="nav flex-column" id="process-menu-list">
			<li class="nav-item">
				<a class="nav-link" href="<c:url value="/index"/>">
					<span data-feather="home"></span>
					Inicio <span class="sr-only">(current)</span>
				</a>
			</li>
			<sec:authorize access="hasRole('ESTABLISHMENT_TECHNICAL') or hasRole('VEHICLE_TECHNICAL') or hasRole('TOBACCO_TECHNICAL') or hasRole('COORDINATOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/requirement-control/index"/>">
						<span data-feather="file"></span>
						Requisitos
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('ESTABLISHMENT_TECHNICAL') or hasRole('VEHICLE_TECHNICAL') or hasRole('TOBACCO_TECHNICAL') or hasRole('COORDINATOR')">
				<li class="nav-item">
                    <a class="nav-link" href="<c:url value="/case-file/request/review/index"/>">
						<span data-feather="layers"></span>
						Documentación
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('JURIDICAL') or hasRole('COORDINATOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/legal/first/review/index"/>">
						<span data-feather="shield"></span>
						Dictamenes
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('ESTABLISHMENT_TECHNICAL') or hasRole('VEHICLE_TECHNICAL') or hasRole('TOBACCO_TECHNICAL') or hasRole('COORDINATOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/payment/index"/>">
						<span data-feather="dollar-sign"></span>
						Mandamientos
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('ESTABLISHMENT_TECHNICAL') or hasRole('VEHICLE_TECHNICAL') or hasRole('TOBACCO_TECHNICAL') or hasRole('COORDINATOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/dispatch/index"/>">
						<span data-feather="chevron-down"></span>
						Ingreso
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('ESTABLISHMENT_TECHNICAL') or hasRole('VEHICLE_TECHNICAL') or hasRole('TOBACCO_TECHNICAL') or hasRole('COORDINATOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/tracing/index"/>">
						<span data-feather="check-circle"></span>
						Seguimiento
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('ESTABLISHMENT_TECHNICAL') or hasRole('VEHICLE_TECHNICAL') or hasRole('TOBACCO_TECHNICAL') or hasRole('COORDINATOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/resolution/index"/>">
						<span data-feather="folder-plus"></span>
						Resoluciones
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('COORDINATOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/external/review/index"/>">
						<span data-feather="eye"></span>
						Revisiones
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('JURIDICAL') or hasRole('COORDINATOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/legal/second/review/index"/>">
						<span data-feather="shield"></span>
						Opiniones
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('ESTABLISHMENT_TECHNICAL') or hasRole('VEHICLE_TECHNICAL') or hasRole('TOBACCO_TECHNICAL') or hasRole('COORDINATOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/archive/index"/>">
						<span data-feather="archive"></span>
						Archivos
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('ESTABLISHMENT_TECHNICAL') or hasRole('VEHICLE_TECHNICAL') or hasRole('TOBACCO_TECHNICAL') or hasRole('COORDINATOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/request/delivery/index"/>">
						<span data-feather="arrow-right-circle"></span>
						Entregas
					</a>
				</li>
			</sec:authorize>
		</ul>
		<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
			<span>Registros</span>
			<a id="register-menu-list-btn" class="d-flex align-items-center text-muted disabled_href" href="javascript:">
				<span data-feather="minus-circle"></span>
			</a>
		</h6>
		<ul id="register-menu-list" class="nav flex-column mb-2">
			<sec:authorize access="hasRole('ESTABLISHMENT_TECHNICAL') or hasRole('VEHICLE_TECHNICAL') or hasRole('TOBACCO_TECHNICAL') or hasRole('COORDINATOR') or hasRole('JURIDICAL') or hasRole('VISITOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/caseFile/index"/>">
						<span data-feather="book"></span>
						Expedientes
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/establishment/index"/>">
						<span data-feather="package"></span>
						Establecimientos
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/vehicle/index"/>">
						<span data-feather="package"></span>
						Vehículos
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="#">
						<span data-feather="package"></span>
						Tabaco
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/owner/index"/>">
						<span data-feather="users"></span>
						Propietarios
					</a>
				</li>
			</sec:authorize>
		</ul>
		
		<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
			<span>Ajustes</span>
			<a id="settings-menu-list-btn" class="d-flex align-items-center text-muted disabled_href" href="javascript:">
				<span data-feather="minus-circle"></span>
			</a>
		</h6>
		<ul id="settings-menu-list" class="nav flex-column mb-2">
            <li class="nav-item">
                <a class="nav-link" href="<c:url value="/my-profile"/>">
                    <span data-feather="user"></span>
                    Mi Perfil
                </a>
            </li>
			<li class="nav-item">
				<a class="nav-link" href="<c:url value="/document-template/all"/>">
					<span data-feather="file"></span>
					Requerimientos
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<c:url value="/resolution-condition/list"/>">
					<span data-feather="link-2"></span>
					Condiciones
				</a>
			</li>
		</ul>

		<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
			<span>Control</span>
			<a id="rec-menu-list-btn" class="d-flex align-items-center text-muted disabled_href" href="javascript:">
				<span data-feather="minus-circle"></span>
			</a>
		</h6>
		<ul id="rec-menu-list" class="nav flex-column mb-2">
			<li class="nav-item">
				<a class="nav-link" href="<c:url value="/process/index"/>">
					<span data-feather="aperture"></span>
					Procesos
				</a>
			</li>
			<sec:authorize access="hasRole('COORDINATOR') or hasRole('VISITOR')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/user-record/index"/>">
						<span data-feather="activity"></span>
						Actividad
					</a>
				</li>
			</sec:authorize>
			<sec:authorize access="hasRole('ESTABLISHMENT_TECHNICAL') or hasRole('VEHICLE_TECHNICAL') or hasRole('TOBACCO_TECHNICAL') or hasRole('COORDINATOR') or hasRole('JURIDICAL')">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/storage/index"/>">
						<span data-feather="hard-drive"></span>
						Almacenamiento
					</a>
				</li>
			</sec:authorize>
		</ul>

		<sec:authorize access="hasRole('CENSUS')">
			<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
				<span>SIGAPS</span>
				<a id="sigaps-menu-list-btn" class="d-flex align-items-center text-muted disabled_href" href="javascript:">
					<span data-feather="minus-circle"></span>
				</a>
			</h6>
			<ul id="sigaps-menu-list" class="nav flex-column mb-2">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/infoutil/infoutilestablishment"/>">
						<span data-feather="book"></span>
						Info Util Establecimiento
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/infoutil/infoUtilVehicle"/>">
						<span data-feather="book"></span>
						Info Util vehículo
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/census/censusestablishment"/>">
						<span data-feather="book"></span>
						Censo Establecimiento
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/census/censusvehicle"/>">
						<span data-feather="book"></span>
						Censo Vehículo
					</a>
				</li>
			</ul>
		</sec:authorize>

		<sec:authorize access="hasRole('USER_ADMIN')">
			<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
				<span>Administración</span>
				<a id="op-menu-list-btn" class="d-flex align-items-center text-muted disabled_href" href="javascript:">
					<span data-feather="minus-circle"></span>
				</a>
			</h6>
			<ul id="op-menu-list" class="nav flex-column mb-2">
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/user"/>">
						<span data-feather="users"></span>
						Usuarios
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/system/info"/>">
						<span data-feather="server"></span>
						Servidor
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/db/index"/>">
						<span data-feather="database"></span>
						DB
					</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="<c:url value="/app/information/index"/>">
						<span data-feather="settings"></span>
						Configuración
					</a>
				</li>
			</ul>
		</sec:authorize>

		<h6 class="sidebar-heading d-flex justify-content-between align-items-center px-3 mt-4 mb-1 text-muted">
			<span>Ayuda</span>
			<a id="help-menu-list-btn" class="d-flex align-items-center text-muted disabled_href" href="javascript:">
				<span data-feather="minus-circle"></span>
			</a>
		</h6>
		<ul id="help-menu-list" class="nav flex-column mb-5">
			<li class="nav-item">
				<a class="nav-link" href="<c:url value="/help"/>">
					<span data-feather="help-circle"></span>
					Ayuda
				</a>
			</li>
			<li class="nav-item">
				<a class="nav-link" href="<c:url value="/logout"/>">
					<span data-feather="log-out"></span>
					Salir
				</a>
			</li>
		</ul>
	</div>
</nav>
