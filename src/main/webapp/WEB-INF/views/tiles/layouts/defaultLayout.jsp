<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ page isELIgnored="false"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="javascripts" />
<tiles:importAttribute name="stylesheets" />
<tiles:importAttribute name="custom_css" />
<tiles:importAttribute name="custom_js" />

<!DOCTYPE html>
<html lang="es">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<meta name="description" content="Ventanilla de permisos sanitarios MINSAL">
<meta name="author" content="MINSAL">
<meta name="_csrf" content="${_csrf.token}"/>
<meta name="_csrf_header" content="${_csrf.headerName}"/>

<title><tiles:getAsString name="title" /></title>
<c:forEach var="css" items="${stylesheets}">
	<link rel="stylesheet" type="text/css" href='<c:url value="${css}"/>'>
</c:forEach>

<c:forEach var="css" items="${custom_css}">
	<link rel="stylesheet" type="text/css" href='<c:url value="${css}"/>'>
</c:forEach>

</head>
<body id="page-top">

	<tiles:insertAttribute name="nav" />
	<tiles:insertAttribute name="header"/>

	<div class="container-fluid">
		<div class="row">
			<tiles:insertAttribute name="menu"/>
			<tiles:insertAttribute name="body"/>
		</div>
	</div>
	<tiles:insertAttribute name="modal"/>
	<tiles:insertAttribute name="bunch"/>

	<c:forEach var="script" items="${javascripts}">
		<script type="text/javascript" src='<c:url value="${script}"/>'></script>
	</c:forEach>

	<c:forEach var="script" items="${custom_js}">
		<script type="text/javascript" src='<c:url value="${script}"/>'></script>
	</c:forEach>

</body>
</html>