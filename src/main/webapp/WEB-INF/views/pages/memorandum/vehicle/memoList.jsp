
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%--@elvariable id="id_caseFile" type="java.lang.Long"--%>
<%--@elvariable id="request" type="edu.ues.aps.process.area.base.dto.AbstractRequestDTO"--%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/resolution/index"/>'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/resolution/vehicle/all"/>'>Resoluciones</a></li>
                    <li class="breadcrumb-item">Memor�ndums</li>
                </ol>
            </nav>

            <div class="card mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-10">
                            <h5 class="card-title">Memor�ndums de solicitud de veh�culos</h5>
                            <p class="text-muted my-0">Memor�ndus contenidos por todas las solicitudes en el folder: <strong>${request.folder_number}</strong></p>
                        </div>
                        <div class="col-lg-2">
                            <div class="btn-group-vertical">
                                <a class="btn btn-outline-primary" role="button" href="<c:url value="/resolution/memorandum/bunch/${id_bunch}/new"/>"><i class="fa fa-plus"></i> Agregar Memo</a>
                                <a class="btn btn-outline-secondary" role="button" href="<%--@elvariable id="id_bunch" type="java.lang.Long"--%>
                                <c:url value="/resolution/vehicle/all"/>"><i class="fa fa-reply"></i> Regresar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover" width="100%" cellspacing="0" id="dataTable">
                    <thead>
                    <tr>
                        <th hidden = hidden></th>
                        <th hidden = hidden></th>
                        <th hidden = hidden></th>
                        <th>N�mero</th>
                        <th>Tipo</th>
                        <th>Para</th>
                        <th>De</th>
                        <th>Fecha</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="memos" type="java.util.List<edu.ues.aps.process.resolution.base.dto.AbstractMemorandumDTO>"--%>
                    <c:forEach items="${memos}" var="item">
                        <tr>
                            <td hidden="hidden" class="id_casefile">${item.id_caseFile}</td>
                            <td hidden="hidden" class="id_memo">${item.id_memo}</td>
                            <td hidden="hidden" class="number">${item.number}</td>
                            <td>${item.memo_number}</td>
                            <td>
                                <c:if test="${item.type == 'INNER_REPORT_FAVORABLE'}">A divisi�n de salud ambiental</c:if>
                                <c:if test="${item.type == 'INNER_REPORT_FAVORABLE_DELEGATE'}">A divisi�n de gesti�n de servicios de salud</c:if>
                                <c:if test="${item.type == 'INNER_REPORT'}">Memorandum interno</c:if>
                                <c:if test="${item.type == 'INVALID'}">Invalido</c:if>
                            </td>
                            <td>${item.from_name}</td>
                            <td>${item.for_name}</td>
                            <td>${item.create_on}</td>
                            <td class="cell-center-middle">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a role="button" href="<c:url value="/resolution/memorandum/bunch/${id_bunch}/memo/${item.number}/edit"/>" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top" title="Editar memorandum"><i class="fa fa-edit"></i></a>
                                    <button type="button" class="btn btn-outline-secondary generate-memorandum-report" data-toggle="tooltip" data-placement="top" title="Generar PDF"><i class="fa fa-file-pdf-o"></i></button>
                                    <button type="button" class="btn btn-outline-secondary show-memo-remove-modal" data-toggle="tooltip" data-placement="top" title="Eliminar memorandum"><i class="fa fa-trash-o"></i></button>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<!-- Memorandum remove confirmation modal -->
<div class="modal fade" id="memo-remove-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Remover memorandum</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-trash-o fa-3x"></i>
                <h6>�Remover memorandum del expediente?</h6>
                <p class="text-muted small">Una vez removido el memorandum del expediente no se perdera toda la informaci�n que este contiene.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a role="button" class="btn btn-danger confirm-memo-removal-btn" href="#">Aceptar</a>
            </div>
        </div>
    </div>
</div>