<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%--@elvariable id="formats" type="java.util.Map<java.lang.String,java.lang.String>"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/resolution/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<%--@elvariable id="id_caseFile" type="java.lang.Long"--%>
                    <c:url value="/resolution/memorandum/${id_caseFile}/list"/>'>Lista de memorandums</a></li>
                    <li class="breadcrumb-item">Memorandum</li>
                </ol>
            </nav>

            <div class="information-panel">
                <div class="alert alert-primary" role="alert">
                    <i class="fa fa-info-circle"></i> Si el usuario de destino esta registrado en el sistema puede buscar en las listas para agregar automaticamente.
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Memorandum</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Formulario de memorandum interno del �rea de permisos sanitarios</h6>
                    <hr class="my-4">
                    <%--@elvariable id="memo" type="edu.ues.aps.process.resolution.base.model.Memorandum"--%>
                    <form:form method="POST" modelAttribute="memo" id="main">
                        <form:hidden path="id"/>
                        <form:hidden path="create_on"/>
                        <form:hidden path="number"/>

                        <div class="form-row">
                            <div class="form-group col-md-6">
                                <label for="for_name">A</label>
                                <form:input type="text" class="form-control" id="for_name" placeholder="Nombre" path="for_name"/>
                                <br>
                                <form:input type="text" class="form-control" id="for_charge" placeholder="Cargo" path="for_charge"/>
                            </div>
                            <div class="form-group col-md-6 align-self-center">
                                <label for="for-select">Seleccionar desde usuarios</label>
                                <select class="custom-select" id="for-select">
                                    <option value="0" selected="selected" disabled="disabled">Nombre - Cargo</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="from_name">De</label>
                                <form:input type="text" class="form-control" id="from_name" placeholder="Nombre" path="from_name"/>
                                <br>
                                <form:input type="text" class="form-control" id="from_charge" placeholder="Cargo" path="from_charge"/>
                            </div>
                            <div class="form-group col-md-6 align-self-center">
                                <label for="from-select">Seleccionar desde usuarios</label>
                                <select class="custom-select" id="from-select">
                                    <option value="0" selected="selected" disabled="disabled">Nombre - Cargo</option>
                                </select>
                            </div>

                            <div class="form-group col-md-6">
                                <label for="through_name">Por medio de</label>
                                <form:input type="text" class="form-control" id="through_name" placeholder="Nombre" path="through_name"/>
                                <br>
                                <form:input type="text" class="form-control" id="through_charge" placeholder="Cargo" path="through_charge"/>
                            </div>
                            <div class="form-group col-md-6 align-self-center">
                                <label for="through-select">Seleccionar desde usuarios</label>
                                <select class="custom-select" id="through-select">
                                    <option value="0" selected="selected" disabled="disabled">Nombre - Cargo</option>
                                </select>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="memo_type">Tipo de memorandum</label>
                            <form:select path="type" id="memo_type" class="custom-select">
                                <form:option value="INNER_REPORT">Memorandum interno</form:option>
                                <form:option value="INNER_REPORT_FAVORABLE">A divisi�n de salud ambiental</form:option>
                                <form:option value="INNER_REPORT_FAVORABLE_DELEGATE">A divisi�n de gesti�n de servicios de salud</form:option>
                            </form:select>
                        </div>
                        <div class="form-group">
                            <label for="format-select">Formato de reporte</label>
                            <select class="custom-select" id="format-select" path="result_type">
                                <option value="INVALID" disabled="disabled" selected="selected">Seleccione una opci�n</option>
                                <option value="empty">Formato vac�o</option>
                                <c:forEach var="format" items="${formats}" varStatus="varst">
                                    <option value="${varst.index + 1}-${format.key}">${varst.index + 1} <--> ${format.key}</option>
                                </c:forEach>
                            </select>
                        </div>

                        <h5 class="text-muted pt-4">Contenido del memorandum</h5>
                        <br>
                        <div id="froala-editor">
                        </div>

                        <form:textarea id="document_content" path="document_content" class="form-control"
                                       hidden="hidden"/>

                        <br>
                        <hr class="my-4">
                        <button id="save-memo-btn" type="button" class="btn btn-primary">Guardar</button>
                        <a role="button" href="<c:url value="/resolution/memorandum/${id_caseFile}/list"/>" class="btn btn-secondary">Cancelar</a>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>

<div hidden="hidden" id="format-box">
    <div id="empty"><p>Nuevo formato vac�o</p></div>
    <c:forEach items="${formats}" var="format" varStatus="varst">
        <div id="${varst.index + 1}-${format.key}" class="editor-format">${format.value}</div>
    </c:forEach>
</div>