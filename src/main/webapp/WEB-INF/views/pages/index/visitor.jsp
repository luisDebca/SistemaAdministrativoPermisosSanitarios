<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div class="jumbotron jumbotron-fluid text-white" style='background-color: rgba(25,59,111,0.79);font-family: "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", "DejaVu Sans", Verdana, sans-serif;width: 100%;padding-left: 235px'>
    <div class="container">
        <h1 class="display-4"><strong id="wellcome-message"></strong> <security:authorize access="isAuthenticated()"><security:authentication property="principal.username"/></security:authorize></h1>
        <p class="lead">A ingresado como visitante, puede observar los diferentes procesos del �rea de permisos sanitarios (Algunas opciones estan deshabilitadas).</p>
    </div>
</div>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row text-center">
                <div class="col-lg-4">
                    <img class="rounded-circle" src="<c:url value="/static/img/index/network.png"/>" alt="Generic placeholder image" width="140" height="140">
                    <h2>Procesos</h2>
                    <p>Ver los diferentes procesos para la obtenci�n de permisos sanitarios.</p>
                    <p><a class="btn btn-secondary" href="<c:url value="/process/index"/>" role="button">Ver detalles &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <img class="rounded-circle" src="<c:url value="/static/img/index/search.png"/>" alt="Generic placeholder image" width="140" height="140">
                    <h2>Buscar</h2>
                    <p>Buscar propietarios, establecimientos o expedientes.</p>
                    <p><a class="btn btn-secondary" href="<c:url value="/owner/index"/>" role="button">Ver detalles &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <img class="rounded-circle" src="<c:url value="/static/img/index/browser.png"/>" alt="Generic placeholder image" width="140" height="140">
                    <h2>Registro de actividad</h2>
                    <p>Ver el registro de actuvidad de cada t�cnico en los procesos de ventanilla.</p>
                    <p><a class="btn btn-secondary" href="<c:url value="/user-record/index"/> " role="button">Ver detalles &raquo;</a></p>
                </div>
            </div>
        </div>
    </div>
</main>