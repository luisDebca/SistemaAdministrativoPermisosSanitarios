<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="security" uri="http://www.springframework.org/security/tags"%>

<div class="jumbotron jumbotron-fluid text-white" style='background-color: rgba(25,59,111,0.79);font-family: "Lucida Grande", "Lucida Sans Unicode", "Lucida Sans", "DejaVu Sans", Verdana, sans-serif;width: 100%;padding-left: 235px'>
    <div class="container">
        <h1 class="display-4"><strong id="wellcome-message"></strong> <security:authorize access="isAuthenticated()"><security:authentication property="principal.username"/></security:authorize></h1>
        <p class="lead">A ingresado como t�cnico del �rea de permisos sanitarios.</p>
    </div>
</div>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row text-center">
                <div class="col-lg-4">
                    <img class="rounded-circle" src="<c:url value="/static/img/index/search.png"/>" alt="Generic placeholder image" width="140" height="140">
                    <h2>Buscar</h2>
                    <p>Buscar propietarios, establecimientos o expedientes.</p>
                    <p><a class="btn btn-secondary" href="#" role="button">Ver detalles &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <img class="rounded-circle" src="<c:url value="/static/img/index/gear.png"/>" alt="Generic placeholder image" width="140" height="140">
                    <h2>Procesos</h2>
                    <p>Seleccione un proceso del menu para continuar.</p>
                    <p><a class="btn btn-secondary" href="#" role="button">Ver detalles &raquo;</a></p>
                </div>
                <div class="col-lg-4">
                    <img class="rounded-circle" src="<c:url value="/static/img/index/document.png"/>" alt="Generic placeholder image" width="140" height="140">
                    <h2>Solicitudes</h2>
                    <p>Ver solicitudes ingresadas recientemente.</p>
                    <p><a class="btn btn-secondary" href="#" role="button">Ver detalles &raquo;</a></p>
                </div>
            </div>
        </div>
    </div>
</main>