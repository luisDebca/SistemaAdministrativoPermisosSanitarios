<%--@elvariable id="owner" type="edu.ues.aps.process.base.dto.OwnerEntityDTO"--%>
<%--@elvariable id="bunch" type="edu.ues.aps.process.area.vehicle.dto.AbstractVehicleBunchDTO"--%>
<%--@elvariable id="verbose_date_time" type="java.lang.String"--%>
<%--@elvariable id="request" type="edu.ues.aps.process.legal.establishment.dto.LegalFirstReviewRequestDTO"--%>
<%--@elvariable id="formats" type="java.util.Map<java.lang.String,java.lang.String>"--%>
<%--@elvariable id="vehicles" type="java.lang.String"--%>
<%--@elvariable id="vehicles_type" type="java.lang.String"--%>
<%--@elvariable id="vehicles_certification" type="java.lang.String"--%>

<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/legal/second/review/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/legal-second/review/vehicle/all"/>">Opiniones</a></li>
                    <li class="breadcrumb-item active">Formulario de opini�n jur�dica</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Opini�n jur�dica</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Resumen del expediente ${bunch.folder_number}</h6>
                    <hr class="my-4">
                    <div id="current-date" hidden="hidden">${verbose_date_time}</div>
                    <div id="vehicle-licence" hidden="hidden">${vehicles}</div>
                    <div id="vehicle-type" hidden="hidden">${vehicles_type}</div>
                    <div id="vehicles_certification" hidden="hidden">${vehicles_certification}</div>
                    <table class="table table-sm">
                        <tbody>
                        <tr>
                            <th scope="row">Propietario</th>
                            <td id="owner-name">${owner.owner_name}</td>
                        </tr>
                        <tr>
                            <th scope="row">Propietario NIT</th>
                            <td id="owner-nit">${owner.owner_nit}</td>
                        </tr>
                        <tr>
                            <th scope="row">Direcci�n de permanencia</th>
                            <td id="vehicle-address">${bunch.address_details}</td>
                        </tr>
                        <tr>
                            <th scope="row">Los alimentos son distribuidos por la empresa</th>
                            <td>${bunch.distribution_company}</td>
                        </tr>
                        <tr>
                            <th scope="row">Direcci�n de empresa distribuidora</th>
                            <td>${bunch.distribution_destination}</td>
                        </tr>
                        <tr>
                            <th scope="row">Lugares de comercializaci�n o destino final</th>
                            <td>${bunch.marketing_place}</td>
                        </tr>
                        <tr>
                            <th scope="row">Nombre de la persona que presenciara la inspecci�n</th>
                            <td>${bunch.client_presented_name}</td>
                        </tr>
                        <tr>
                            <th scope="row">Tel�fono de la persona que presenciara la inspecci�n</th>
                            <td>${bunch.client_presented_telephone}</td>
                        </tr>
                        </tbody>
                    </table>


                    <%--@elvariable id="licenses" type="java.util.List<java.lang.String>"--%>
                    <hr class="my-4">
                    <h6 class="card-subtitle mb-2 text-muted">Veh�culos contenidos</h6>
                    <c:forEach items="${licenses}" var="item">
                        <span class="badge badge-primary">${item}</span>
                    </c:forEach>
                </div>
            </div>
            <br>

            <div class="card">
                <div class="card-body">
                    <%--@elvariable id="review" type="edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO"--%>
                    <form:form id="main" method="post" modelAttribute="review">
                        <form:hidden id="casefile_review" path="id_casefile"/>
                        <form:hidden path="id_review"/>
                        <div class="row">
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label for="selected-client" class="col-sm-4 col-form-label">A nombre de</label>
                                    <select class="custom-select col-sm-6" id="selected-client">
                                        <option value="INVALID" disabled="disabled" selected="selected">Seleccione una opci�n</option>
                                            <%--@elvariable id="client_list" type="java.util.List<edu.ues.aps.process.base.dto.AbstractClientDTO>"--%>
                                        <c:forEach items="${client_list}" var="item">
                                            <option value="${item.clientType}">${item.client_name}<--> ${item.type}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-4 col-form-label" for="selected-memo">Memor�ndum</label>
                                    <select class="custom-select col-sm-6" id="selected-memo">
                                        <option value="INVALID" disabled="disabled" selected="selected">Seleccione una opci�n
                                        </option>
                                            <%--@elvariable id="client_list" type="java.util.List<edu.ues.aps.process.area.establishment.dto.ClientDTO>"--%>
                                        <c:forEach items="${memos}" var="item">
                                            <option value="${item.id_memo}">${item.memo_number}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                                <div class="form-group row">
                                    <label for="format-select" class="col-sm-4 col-form-label">Formato de reporte</label>
                                    <select class="custom-select col-sm-6" id="format-select" path="result_type">
                                        <option value="INVALID" disabled="disabled" selected="selected">Seleccione una opci�n</option>
                                        <option value="empty">Formato vac�o</option>
                                        <c:forEach var="format" items="${formats}" varStatus="varst">
                                            <option value="${varst.index + 1}-${format.key}">${varst.index + 1} <--> ${format.key}</option>
                                        </c:forEach>
                                    </select>
                                </div>
                            </div>
                            <div class="col-lg-6">
                                <div class="form-group row">
                                    <label for="for_name" class="col-sm-2 col-form-label">A</label>
                                    <div class="col-sm-10">
                                        <form:input type="text" class="form-control" id="for_name" path="to_name" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="for_charge" class="col-sm-2 col-form-label">Cargo</label>
                                    <div class="col-sm-10">
                                        <form:input type="text" class="form-control" id="for_charge" path="to_charge"/>
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="for_name" class="col-sm-2 col-form-label">De</label>
                                    <div class="col-sm-10">
                                        <form:input type="text" class="form-control" id="for_name" path="from_name" />
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label for="for_charge" class="col-sm-2 col-form-label">Cargo</label>
                                    <div class="col-sm-10">
                                        <form:input type="text" class="form-control" id="for_charge" path="from_charge"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <div id="froala-editor">
                        </div>

                        <form:textarea id="document_content" path="document_content" class="form-control"
                                       hidden="hidden"/>

                        <hr class="my-4">
                        <button id="save-review-btn" type="button" class="btn btn-primary">Guardar</button>
                        <a href="<c:url value="/legal/first/review/vehicle/all"/> " role="button"
                           class="btn btn-secondary">Cancelar</a>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>

<div hidden="hidden" id="format-box">
    <div id="empty"><p>Nuevo formato vac�o</p></div>
    <c:forEach items="${formats}" var="format" varStatus="varst">
        <div id="${varst.index + 1}-${format.key}" class="editor-format">${format.value}</div>
    </c:forEach>
</div>