<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/legal/first/review/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Revisi�n del coordinador para veh�culos</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover vehicle-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>N� Solicitudes</th>
                        <th>Propietario</th>
                        <th>Compa�ia de distribuci�n</th>
                        <th>Destino de distribuci�n</th>
                        <th>Lugar de mercancia</th>
                        <th>Direcci�n de estancia</th>
                        <th>Fecha inicio</th>
                        <th>Tiempo faltante</th>
                        <th>Dictamen</th>
                        <th>Resoluci�n</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.legal.vehicle.dto.AbstractVehicleLegalReview>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td class="id_bunch" hidden="hidden">${item.id_bunch}</td>
                            <td class="id_owner" hidden="hidden">${item.id_owner}</td>
                            <td class="cell-center-middle">${item.request_folder}</td>
                            <td class="cell-center-middle"><h5>${item.request_count}</h5></td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.bunch_distribution_company}</td>
                            <td>${item.bunch_distribution_destination}</td>
                            <td>${item.bunch_marketing_place}</td>
                            <td>${item.bunch_address}</td>
                            <td class="finish-date-formatted">${item.request_create_on}</td>
                            <td class="timer cell-center-middle">
                                <div hidden="hidden" class="delay">${item.delay_date}</div>
                            </td>
                            <td class="cell-center-middle">
                                <c:if test="${item.dictum_result == 'ADMISIBILIDAD'}"><span
                                        class="badge badge-success">ADMISIBILIDAD</span></c:if>
                                <c:if test="${item.dictum_result == 'INADMISIBILIDAD'}"><span
                                        class="badge badge-danger">INADMISIBILIDAD</span></c:if>
                                <c:if test="${item.dictum_result == 'PREVENCI�N'}"><span
                                        class="badge badge-warning">PREVENCI�N</span></c:if>
                                <c:if test="${item.dictum_result == 'INVALIDO'}"><span
                                        class="badge badge-secondary">No<br>especificado</span></c:if>
                            </td>
                            <td class="cell-center-middle">
                                <c:if test="${item.resolution_result == 'ADMISIBILIDAD'}"><span
                                        class="badge badge-success">ADMISIBILIDAD</span></c:if>
                                <c:if test="${item.resolution_result == 'INADMISIBILIDAD'}"><span
                                        class="badge badge-danger">INADMISIBILIDAD</span></c:if>
                                <c:if test="${item.resolution_result == 'PREVENCI�N'}"><span
                                        class="badge badge-warning">PREVENCI�N</span></c:if>
                                <c:if test="${item.resolution_result == 'INVALIDO'}"><span
                                        class="badge badge-secondary">No<br>especificado</span></c:if>
                            </td>
                            <td class="table-options cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group btn-options">
                                        <a href="#"
                                           class="btn btn-outline-secondary disabled_href show_brief_bunch_information"
                                           data-toggle="tooltip" title="Ver solicitudes contenidas"><i class="fa fa-th"
                                                                                                       aria-hidden="true"></i></a>
                                        <c:if test="${item.resolution_created}">
                                            <a href="<c:url value="/legal/first/resolution/vehicle/bunch/${item.id_bunch}/edit"/>"
                                               class="btn btn-outline-secondary"
                                               data-toggle="tooltip" title="Editar dictamen"><i class="fa fa-edit"
                                                                                                aria-hidden="true"></i></a>
                                        </c:if>
                                        <c:if test="${not item.resolution_created}">
                                            <a href="<c:url value="/legal/first/resolution/vehicle/bunch/${item.id_bunch}/new"/>"
                                               class="btn btn-outline-secondary"
                                               data-toggle="tooltip" title="Nuevo dictamen"><i class="fa fa-plus"
                                                                                               aria-hidden="true"></i></a>
                                        </c:if>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <c:if test="${item.resolution_created}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary disabled_href move_to_next_process_btn"
                                                    data-toggle="tooltip" title="Pasar a mandamientos de pago"><i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                        </c:if>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                    aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
<div hidden="hidden" id="bunch-options">
    <a role="button" href="javascript:" class="btn btn-outline-secondary disabled_href generate-bunch-legal-first-review-pdf-btn"><i class="fa fa-file-pdf-o"></i></a>
    <a role="button" href="javascript:" class="btn btn-outline-secondary disabled_href generate-bunch-resolution-btn"><i class="fa fa-file-pdf-o"></i></a>
</div>