<%--@elvariable id="verbose_date_time" type="java.lang.String"--%>
<%--@elvariable id="request" type="edu.ues.aps.process.legal.establishment.dto.LegalFirstReviewRequestDTO"--%>
<%--@elvariable id="formats" type="java.util.Map<java.lang.String,java.lang.String>"--%>

<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/legal/first/review/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/legal/first/resolution/establishment/list"/>">Revici�n
                        del coordinador</a></li>
                    <li class="breadcrumb-item active">Formulario de resoluci�n jur�dica</li>
                </ol>
            </nav>

            <div class="information-panel">
                <c:if test="${request.id_casefile == 0 or request.id_establishment == 0 or request.id_owner == 0}">
                    <div class="alert alert-danger" role="alert">
                        <i class="fa fa-warning"></i>  No se encontro el recurso solicitado. <a href="<c:url value="/legal/first/resolution/establishment/list"/>" class="alert-link">Volver atras</a>
                    </div>
                </c:if>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Revisi�n del coordinador para resoluci�n</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Resumen del expediente ${request.folder_number}</h6>
                    <hr class="my-4">
                    <div class="bs-callout bs-callout-info">
                        <p class="id_casefile" hidden="hidden">${request.id_casefile}</p>
                        <p class="id_establishment" hidden="hidden">${request.id_establishment}</p>
                        <p class="id_owner" hidden="hidden">${request.id_owner}</p>
                        <p id="current-date" hidden="hidden">${verbose_date_time}</p>
                        <table class="table table-sm">
                            <tbody>
                            <tr>
                                <th scope="row">Nombre Propietario</th>
                                <td id="owner-name">${request.owner_name}</td>
                            </tr>
                            <tr>
                                <th scope="row">Nombre Establecimiento</th>
                                <td id="establishment-name">${request.establishment_name}</td>
                            </tr>
                            <tr>
                                <th scope="row">NIT Establecimiento</th>
                                <td id="establishment-nit">${request.establishment_nit}</td>
                            </tr>
                            <tr>
                                <th scope="row">Tipo de establecimiento (escrito por el usuario)</th>
                                <td id="establishment-type">${request.establishment_type_detail}</td>
                            </tr>
                            <tr>
                                <th scope="row">Tipo de establecimiento (Rubro)</th>
                                <td id="establishment-section">${request.establishment_section}/${request.establishment_type}</td>
                            </tr>
                            <tr>
                                <th scope="row">Direcci�n del establecimiento</th>
                                <td id="establishment-address">${request.establishment_address}</td>
                            </tr>
                            <tr>
                                <th scope="row">Tipo de permiso solicitado</th>
                                <td id="request-type">${request.casefile_certification_type}</td>
                            </tr>
                            <tr>
                                <th scope="row">SIBASI / UCSF</th>
                                <td id="sibasi">${request.sibasi_name}/${request.ucsf_name}</td>
                            </tr>
                            <tr>
                                <th scope="row">Inicio de solicitud</th>
                                <td id="ucsf">${request.document_created_on}</td>
                            </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <br>

            <div class="card">
                <div class="card-body">
                    <%--@elvariable id="review" type="edu.ues.aps.process.legal.base.dto.LegalReviewFormDTO"--%>
                    <form:form id="main" method="post" modelAttribute="review">
                        <form:hidden id="casefile_review" path="id_casefile"/>
                        <form:hidden path="id_review"/>
                        <div class="form-group row">
                            <label for="review-type-select" class="col-sm-3 col-form-label">Tipo de resoluci�n</label>
                            <form:select class="custom-select col-sm-4" id="review-type-select" path="result_type">
                                <option value="INVALID" disabled="disabled" selected="selected">Seleccione una opci�n
                                </option>
                                <%--@elvariable id="result_types" type="java.util.Map"--%>
                                <form:options items="${result_types}"/>
                            </form:select>
                        </div>
                        <div class="form-group row">
                            <label for="selected-client" class="col-sm-3 col-form-label">A nombre de</label>
                            <select class="custom-select col-sm-4" id="selected-client">
                                <option value="INVALID" disabled="disabled" selected="selected">Seleccione una opci�n
                                </option>
                                    <%--@elvariable id="client_list" type="java.util.List<edu.ues.aps.process.base.dto.AbstractClientDTO>"--%>
                                <c:forEach items="${client_list}" var="item">
                                    <option value="${item.clientType}">${item.client_name}
                                        <--> ${item.type}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <div class="form-group row">
                            <label for="format-select" class="col-sm-3 col-form-label">Formato de reporte</label>
                            <select class="custom-select col-sm-6" id="format-select" path="result_type">
                                <option value="INVALID" disabled="disabled" selected="selected">Seleccione una opci�n</option>
                                <option value="empty">Formato vac�o</option>
                                <c:forEach var="format" items="${formats}" varStatus="varst">
                                    <option value="${varst.index + 1}-${format.key}">${varst.index + 1} <--> ${format.key}</option>
                                </c:forEach>
                            </select>
                        </div>
                        <br>
                        <div id="froala-editor">
                        </div>

                        <form:textarea id="document_content" path="document_content" class="form-control"
                                       hidden="hidden"/>

                        <div class="footer-options">
                            <button id="save-review-btn" type="button" class="btn btn-primary">Guardar</button>
                            <a href="<c:url value="/legal/first/resolution/establishment/list"/> " role="button"
                               class="btn btn-secondary">Cancelar</a>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>

<div hidden="hidden" id="format-box">
    <div id="empty"><p>Nuevo formato vac�o</p></div>
    <c:forEach items="${formats}" var="format" varStatus="varst">
        <div id="${varst.index + 1}-${format.key}" class="editor-format">${format.value}</div>
    </c:forEach>
</div>
