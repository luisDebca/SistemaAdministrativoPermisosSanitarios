<!-- Move to next process validator modal -->
<div class="modal fade" id="movetonextmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="movetotitle">Mover a</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-exchange fa-2x" aria-hidden="true"></i>
                <h5 class="modal-title">�Confirma mover al proceso de revisi�n de solicitudes?</h5>
                <small class="text-muted">Una vez la solicitud cambia de proceso no se podra seguir la modificaci�n en este paso y se movera a la lista del siguiente proceso.</small>
                <input type="text" id="toMoveId" class="form-control" hidden="hidden"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="moveRequestBtn" type="button" class="btn btn-primary">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<!-- Move to forgotten validator modal -->
<div class="modal fade" id="movetoforgotternmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="movetoforgottentitle">Remover solicitud del proceso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i>
                <h5 class="modal-title">�Confirma remover solicitud del proceso?</h5>
                <small class="text-muted">Las solicitudes que se remuvan del proceso pueden integrarce nuevamente desde este punto.</small>
                <input type="text" id="toForgottenId" class="form-control" hidden="hidden"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="forgottenRequestBtn" type="button" class="btn btn-warning">Confirmar</button>
            </div>
        </div>
    </div>
</div>
<!-- Move to rejected validator modal -->
<div class="modal fade" id="movetorejectedmodal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="rejectedmodaltitle">Cancelar solicitud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i>
                <h5 class="modal-title">�Confirma cancelar solicitud?</h5>
                <small class="text-muted">Las solicitudes canceladas no podran continuar con el proceso.</small>
                <input type="text" id="toRejectId" class="form-control" hidden="hidden"/>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="rejectRequestBtn" type="button" class="btn btn-danger">Confirmar</button>
            </div>
        </div>
    </div>
</div>

<!-- Add observation to process -->
<div class="modal fade" id="process-observation-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="process-observation-title">Agregar observaciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <h5><i class="fa fa-pencil" aria-hidden="true"></i> Observaciones</h5>
                <form>
                    <div class="form-group">
                        <label for="exampleFormControlTextarea1">Agregar observaciones al proceso actual</label>
                        <textarea class="form-control" id="exampleFormControlTextarea1" rows="5"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>

<div class="modal fade " id="process-summary-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="process-summary-modal-title">Resumen de proceso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th scope="col">Proceso</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Observaciones</th>
                        <th scope="col">Inicio</th>
                        <th scope="col">Fin</th>
                        <th scope="col">Modificaciones</th>
                        <th scope="col">--------------</th>
                    </tr>
                    </thead>
                    <tbody id="process-summary-table-body"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>