<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<%--@elvariable id="id_owner" type="java.lang.Long"--%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/app/information/index"/>'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/configuration/report-format/index"/>'>Formatos</a></li>
                    <li class="breadcrumb-item">Formulario de reporte</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">

                    <h5 class="card-title">Formulario de formato para reportes</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Complete los datos solicitados.</h6>
                    <hr class="my-4">

                    <%--@elvariable id="format" type="edu.ues.aps.process.settings.model.ReportFormat"--%>
                    <form:form id="main" method="post" modelAttribute="format">
                        <form:hidden path="id"/>
                        <form:hidden path="protected"/>
                        <form:hidden path="lastModified"/>

                        <div class="form-group row">
                            <label for="name" class="col-sm-2 col-form-label">Nombre</label>
                            <div class="col-sm-6">
                                <form:input path="reportName" type="text" class="form-control" id="name"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="type" class="col-sm-2 col-form-label">Reporte</label>
                            <div class="col-sm-4">
                                <form:select path="reportType" class="custom-select" id="type">
                                    <option value="0" selected="selected" disabled="disabled">Seleccione una opci�n</option>
                                    <form:option value="DICTUM">Formato para dictamenes jur�dicos</form:option>
                                    <form:option value="COORDINATOR">Formato para resoluciones del jur�dico</form:option>
                                    <form:option value="RESOLUTION">Formato para resoluci�n de solicitud</form:option>
                                    <form:option value="OPINION">Formato para opini�n jur�dica</form:option>
                                    <form:option value="MEMO">Formato para memor�ndums</form:option>
                                    <form:option value="SIMPLE">Otros formatos</form:option>
                                </form:select>
                            </div>
                        </div>
                        <div class="btn-toolbar my-2" role="toolbar" aria-label="Toolbar with button groups">
                            <div class="btn-group mr-2" role="group" aria-label="First group">
                                <button type="button" data-text="FECHA_ACTUAL" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Fecha actual"><i class="fa fa-clock-o" aria-hidden="true"></i></button>
                                <button type="button" data-text="FECHA_INICIO" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Fecha inicio permiso"><i class="fa fa-calendar-check-o" aria-hidden="true"></i></button>
                                <button type="button" data-text="FECHA_FINAL" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Fecha final permiso"><i class="fa fa-calendar-times-o" aria-hidden="true"></i></button>
                            </div>
                            <div class="btn-group mr-2" role="group" aria-label="Second group">
                                <button type="button" data-text="FOLDER" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="N�mero de folder"><i class="fa fa-chain-broken" aria-hidden="true"></i></button>
                                <button type="button" data-text="SOLICITUD" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="N�mero de solicitud"><i class="fa fa-chain-broken" aria-hidden="true"></i></button>
                                <button type="button" data-text="RESOLUCION" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="N�mero de resoluci�n"><i class="fa fa-chain-broken" aria-hidden="true"></i></button>
                            </div>
                            <div class="btn-group mr-2" role="group" aria-label="Third group">
                                <button type="button" data-text="PROPIETARIO_NOMBRE" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Propietario"><i class="fa fa-user-o" aria-hidden="true"></i></button>
                                <button type="button" data-text="USUARIO_NOMBRE" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Nombre de Autorizado"><i class="fa fa-users" aria-hidden="true"></i></button>
                                <button type="button" data-text="USUARIO_TIPO" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Tipo de Autorizado"><i class="fa fa-users" aria-hidden="true"></i></button>
                            </div>
                            <div class="btn-group mr-2" role="group" aria-label="Fourt group">
                                <button type="button" data-text="ESTABLECIMIENTO_NOMBRE" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Nombre establecimiento"><i class="fa fa-building-o" aria-hidden="true"></i></button>
                                <button type="button" data-text="ESTABLECIMIENTO_NIT" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="NIT establecimiento"><i class="fa fa-building-o" aria-hidden="true"></i></button>
                                <button type="button" data-text="ESTABLECIMIENTO_DIRECCION" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Direcci�n establecimiento"><i class="fa fa-building-o" aria-hidden="true"></i></button>
                                <button type="button" data-text="ESTABLECIMIENTO_TIPO" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Tipo establecimiento"><i class="fa fa-building-o" aria-hidden="true"></i></button>
                            </div>

                            <div class="btn-group mr-2" role="group" aria-label="Five group">
                                <button type="button" data-text="VEHICULO_PLACA" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Placa veh�culo"><i class="fa fa-truck" aria-hidden="true"></i></button>
                                <button type="button" data-text="VEHICULO_TIPO" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Tipo veh�culo"><i class="fa fa-truck" aria-hidden="true"></i></button>
                                <button type="button" data-text="VEHICULO_TRASPORTE" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Transporte de alimentos"><i class="fa fa-truck" aria-hidden="true"></i></button>
                                <button type="button" data-text="VEHICULO_DIRECCION" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Direcci�n de permanencia"><i class="fa fa-truck" aria-hidden="true"></i></button>
                            </div>

                            <div class="btn-group mr-2" role="group" aria-label="Six group">
                                <button type="button" data-text="MEMORANDUM_NUMERO" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="N�mero de memorandum"><i class="fa fa-italic" aria-hidden="true"></i></button>
                            </div>

                            <div class="btn-group mr-2" role="group" aria-label="Seven group">
                                <button type="button" data-text="TIPO_DOCUMENTO" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="Documento de tipo ..."><i class="fa fa-bookmark-o" aria-hidden="true"></i></button>
                            </div>

                            <div class="btn-group" role="group" aria-label="Eight group">
                                <button type="button" data-text="UCSF" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="UCSF"><i class="fa fa-gg" aria-hidden="true"></i></button>
                                <button type="button" data-text="SIBASI" class="btn btn-outline-secondary add-text-btn" data-toggle="tooltip" data-placement="top" title="SIBASI"><i class="fa fa-gg-circle" aria-hidden="true"></i></button>
                            </div>
                        </div>

                        <div id="froala-editor">
                            <p id="editor-default-text">Formato del reporte aqui!</p>
                        </div>

                        <form:textarea hidden="hidden" path="format" id="format" class="form-control" />

                        <hr class="my-4">
                        <button type="button" class="btn btn-primary save-format">Guardar</button>
                        <a href="<c:url value="/configuration/report-format/index"/>" role="button" class="btn btn-secondary">Cancelar</a>

                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>