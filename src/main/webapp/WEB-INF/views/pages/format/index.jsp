<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/app/information/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item">Formato de reportes</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-sm-10">
                            <h5 class="card-title">Formato de reportes</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Modifique los reportes que se muestran en el proceso.</h6>
                        </div>
                        <div class="col-sm-2">
                            <a href="<c:url value="/configuration/report-format/new"/>" role="button" class="btn btn-outline-primary"><i class="fa fa-plus"></i> Nuevo formato</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover establishment-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th>Nombre</th>
                        <th>Contenido</th>
                        <th>Protegido</th>
                        <th>Ultima modificación</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    <%--@elvariable id="formats" type="java.util.List<edu.ues.aps.process.settings.dto.AbstractReportFormatDTO>"--%>
                    <c:forEach items="${formats}" var="item">
                        <tr>
                           <td hidden="hidden" class="id_report">${item.id}</td>
                            <td>${item.name}</td>
                            <td>${item.format}</td>
                            <td class="cell-center-middle">
                                <c:if test="${not item.protection}"><i class="fa fa-times fa-2x"></i></c:if>
                                <c:if test="${item.protection}"><i class="fa fa-check fa-2x"></i></c:if>
                            </td>
                            <td class="cell-center-middle">${item.modifiedOn}</td>
                            <td class="cell-center-middle table-options">
                                <div class="btn-group" role="group">
                                    <a href="<c:url value="/configuration/report-format/${item.id}/edit"/>"
                                       class="btn btn-outline-secondary" data-toggle="tooltip" title="Editar formato"><i
                                            class="fa fa-pencil-square-o "></i></a>
                                    <sec:authorize access="hasRole('USER_ADMIN') or hasRole('COORDINATOR')">
                                        <c:if test="${item.protection}">
                                            <a href="<c:url value="/configuration/report-format/${item.id}/unlock"/> "
                                               class="btn btn-outline-secondary" data-toggle="tooltip"
                                               title="Quitar protección"><i class="fa fa-unlock"></i></a>
                                        </c:if>
                                        <c:if test="${not item.protection}">
                                            <a href="<c:url value="/configuration/report-format/${item.id}/lock"/> "
                                               class="btn btn-outline-secondary" data-toggle="tooltip"
                                               title="Agregar protección"><i class="fa fa-lock"></i></a>
                                        </c:if>
                                    </sec:authorize>
                                    <c:if test="${not item.protection}">
                                        <a href="<c:url value="/configuration/report-format/${item.id}/delete"/> "
                                           class="btn btn-outline-secondary" data-toggle="tooltip"
                                           title="Eliminar formato"><i class="fa fa-trash"></i></a>
                                    </c:if>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
