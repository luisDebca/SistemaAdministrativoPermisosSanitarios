<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/vehicle/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/vehicle/list"/>">�rea de
                        vehiculos</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Informaci�n de veh�culo</li>
                </ol>
            </nav>
            <%--@elvariable id="vehicle" type="edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO"--%>

            <c:if test="${not vehicle.active}">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Deshabilitado!</strong> Este veh�culo se encuentra actualmente deshabilitado.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>

            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-primary rounded box-shadow">
                <div class="lh-100">
                    <h4 class="mb-0 text-white lh-100">Informaci�n de veh�culo <strong>${vehicle.vehicle_license}</strong></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h5 class="border-bottom border-gray pb-2 mb-0">Informaci�n completa</h5>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Placa</strong>
                                ${vehicle.vehicle_license}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Tipo</strong>
                                ${vehicle.vehicle_type}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Transporta productos</strong>
                                ${vehicle.vehicle_transported_content}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">N�mero de empleados</strong>
                                ${vehicle.vehicle_employees_number}
                            </p>
                        </div>

                        <a role="button" class="btn btn-outline-primary btn-lg btn-block" href="<c:url value="/vehicle/${vehicle.id_vehicle}/edit"/>"><i class="fa fa-edit"></i> Editar</a>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h5 class="border-bottom border-gray pb-2 mb-0">Registros</h5>

                        <div class="media text-muted pt-3">
                            <div class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">Permisos sanitarios</strong>
                                    <a href="<c:url value="/vehicle/${vehicle.id_vehicle}/caseFile/list"/>">Ver
                                        todos</a>
                                </div>
                                <span class="d-block">Permisos sanitarios registados para este veh�culo</span>
                            </div>
                        </div>

                    </div>
                    <button type="button" class="btn btn-outline-secondary btn-lg btn-block goback"><i class="fa fa-reply"></i> Atras</button>
                </div>
            </div>
        </div>
    </div>
</main>