<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fomr" uri="http://www.springframework.org/tags/form" %>

<%--@elvariable id="id_owner" type="java.lang.Long"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/vehicle/index"/>">Inicio</a></li>
                    <c:if test="${not empty id_owner}">
                        <li class="breadcrumb-item"><a href="<c:url value="/owner/${id_owner}/vehicle/casefile/list"/>">Solicitudes de propietario</a></li>
                    </c:if>
                    <c:if test="${empty id_owner}">
                        <li class="breadcrumb-item"><a href="<c:url value="/vehicle/casefile/list"/>">Solicitudes de veh�culos</a></li>
                    </c:if>
                    <li class="breadcrumb-item active">Formulario de solicitud de vehiculos</li>
                </ol>
            </nav>

            <div class="information-panel">
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>Recomendaci�n</strong> Ingrese los datos completos del veh�culo para una mejor
                    sistematizaci�n del proceso.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">

                    <h5 class="card-title">Solicitudes de veh�culos</h5>
                    <h6 class="card-subtitle mb-4 text-muted">La siguiente informaci�n se aplica a todos los veh�culos que esten incluidos en esta solicitud.</h6>
                    <hr class="my-4">
                    <%--@elvariable id="bunch" type="edu.ues.aps.process.area.vehicle.model.VehicleBunch"--%>
                    <form:form id="main" modelAttribute="bunch">
                        <fomr:hidden path="id"/>
                        <form:hidden path="vehicle_count"/>
                        <form:hidden path="address.id"/>
                        <div class="form-group row">
                            <label for="distribution-name" class="col-sm-3 col-form-label">Los alimentos son distribuidos por la empresa</label>
                            <div class="col-sm-8">
                                <form:input id="distribution-name" class="form-control" path="distribution_company"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="distribution-address" class="col-sm-3 col-form-label">Direcci�n de empresa distribuidora</label>
                            <div class="col-sm-8">
                                <form:input id="distribution-address" class="form-control" path="distribution_destination"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="place" class="col-sm-3 col-form-label">Lugares de comercializaci�n o destino final</label>
                            <div class="col-sm-8">
                                <form:input id="place" class="form-control" path="marketing_place"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="client-name" class="col-sm-3 col-form-label">Nombre de la persona que presenciara la inspecci�n</label>
                            <div class="col-sm-8">
                                <form:input id="client-name" class="form-control" path="client_presented_name"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="client-tel" class="col-sm-3 col-form-label">Tel�fono</label>
                            <div class="col-sm-3">
                                <form:input id="client-tel" class="form-control" path="client_presented_telephone"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-sm-3 col-form-label">Direcci�n de permanencia</label>
                            <div class="col-sm-8">
                                <form:input type="text" class="form-control" id="address" path="address.details"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3 col-form-label">Departamento</label>
                            <div class="col-4">
                                    <%--@elvariable id="departments" type="java.util.List<edu.ues.aps.process.base.dto.MapDTO>"--%>
                                <form:select
                                        class="custom-select department-select-list"
                                        path="address.municipality.department.id"
                                        items="${departments}"
                                        itemLabel="value"
                                        itemValue="id"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3 col-form-label">Municipio</label>
                            <div class="col-4">
                                    <%--@elvariable id="municipalities" type="java.util.List<edu.ues.aps.process.base.dto.MapDTO>"--%>
                                <form:select
                                        class="custom-select municipality-select-list initialized-municipality-select-list"
                                        path="address.municipality.id"
                                        items="${municipalities}"
                                        itemValue="id"
                                        itemLabel="value"/>
                            </div>
                        </div>

                        <hr class="my-4">
                        <button type="submit " class="btn btn-primary">Guardar</button>
                        <c:if test="${not empty id_owner}">
                            <a href="<c:url value="/owner/${id_owner}/vehicle/casefile/list"/>" role="button" class="btn btn-secondary">Cancelar</a>
                        </c:if>
                        <c:if test="${empty id_owner}">
                            <a href="<c:url value="/vehicle/casefile/list"/>" role="button" class="btn btn-secondary">Cancelar</a>
                        </c:if>

                    </form:form>

                </div>
            </div>
        </div>
    </div>
</main>