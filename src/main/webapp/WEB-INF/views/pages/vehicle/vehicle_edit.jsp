<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="id_owner" type="java.lang.Long"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/vehicle/index"/>">Inicio</a></li>
                    <c:if test="${empty id_owner}">
                        <li class="breadcrumb-item"><a href="<c:url value="/vehicle/list"/>">Veh�culos</a></li>
                    </c:if>
                    <c:if test="${not empty id_owner}">
                        <li class="breadcrumb-item"><a href="<c:url value="/owner/${id_owner}/vehicle/list"/>">Veh�culos</a></li>
                    </c:if>
                    <li class="breadcrumb-item active">Formulario de veh�culos</li>
                </ol>
            </nav>

            <div class="information-panel">
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>Recomendaci�n</strong> Ingrese los datos completos del veh�culo para una mejor
                    sistematizaci�n del proceso.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">

                    <h5 class="card-title">Formulario de veh�culos</h5>
                    <h6 class="card-subtitle mb-4 text-muted">Complete los datos requeridos del veh�culo</h6>
                    <hr class="my-4">
                    <%--@elvariable id="vehicle" type="edu.ues.aps.process.area.vehicle.model.AbstractVehicle"--%>
                    <form:form id="main_edit" method="post" modelAttribute="vehicle">
                        <form:hidden path="id"/>

                        <div class="form-group row">
                            <label for="licence" class="col-sm-2 col-form-label">Placa</label>
                            <div class="col-sm-4">
                                <form:input type="text" class="form-control" id="licence" path="license"/>
                            </div>
                            <div class="col-sm-3">
                                <select id="license_type" class="custom-select">
                                    <option value="" selected="true" disabled="true">Seleccione una opci�n</option>
                                    <option value="A">A</option>
                                    <option value="AB">AB</option>
                                    <option value="C">C</option>
                                    <option value="M">M</option>
                                    <option value="MB">MB</option>
                                    <option value="N">N</option>
                                    <option value="O">O</option>
                                    <option value="P">P</option>
                                    <option value="RE">RE</option>
                                    <option value="MI">MI</option>
                                    <option value="CD">CD</option>
                                    <option value="CC">CC</option>
                                    <option value="PR">PR</option>
                                    <option value="CR">CR</option>
                                    <option value="D">D</option>
                                    <option value="">Vac�o</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="type" class="col-sm-2 col-form-label">Tipo de veh�culo</label>
                            <div class="col-sm-5">
                                <form:input type="text" class="form-control" id="type" path="vehicle_type"/>
                            </div>
                            <div class="col-sm-4">
                                <select id="kind" class="custom-select">
                                    <option value="" selected="true" disabled="true">Seleccione una opci�n</option>
                                    <option value="FURG�N">FURG�N</option>
                                    <option VALUE="FURG�N REFRIGERADO">FURG�N REFRIGERADO</option>
                                    <option value="AUTOM�VIL">AUTOMOVIL</option>
                                    <option value="CAMI�N LIVIANO">CAMI�N LIVIANO</option>
                                    <option value="PICK UP">PICK UP</option>
                                    <option value="PANEL">PANEL</option>
                                    <option value="CAMI�N PESADO">CAMI�N PESADO</option>
                                    <option value="AUTOB�S">AUTOBUS</option>
                                    <option value="MOTOCICLETA">MOTOCICLETA</option>
                                    <option value="MICROB�S">MICROBUS</option>
                                    <option value="CABEZAL">CABEZAL</option>
                                    <option value="REMOLQUE">REMOLQUE</option>
                                    <option value="TOLVA">TOLVA</option>
                                    <option value="CISTERNA">CISTERNA</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="content" class="col-sm-2 col-form-label">Transporta alimentos</label>
                            <div class="col-sm-5">
                                <form:select class="custom-select" id="content" path="transported_content">
                                    <option value="INVALID" selected="selected" disabled="disabled">Seleccione una opci�n</option>
                                    <form:option value="PERISHABLE">Perecedero</form:option>
                                    <form:option value="NON_PERISHABLE">No perecedero</form:option>
                                </form:select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="number" class="col-sm-2 col-form-label">N�mero de empleados</label>
                            <div class="col-sm-2">
                                <form:input type="number" class="form-control" id="number" path="employees_number" min="1" max="99"/>
                            </div>
                        </div>

                        <div class="form-group row ml-1">
                            <div class="custom-control custom-checkbox">
                                <form:checkbox class="custom-control-input" id="customCheck1"  path="active" checked="true"/>
                                <label class="custom-control-label" for="customCheck1">El veh�culo esta activo</label>
                            </div>
                        </div>

                        <hr class="my-4">

                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <c:if test="${empty id_owner}">
                            <a role="button" class="btn btn-secondary" href="<c:url value="/vehicle/list"/>">Cancelar</a>
                        </c:if>
                        <c:if test="${not empty id_owner}">
                            <a role="button" class="btn btn-secondary" href="<c:url value="/owner/${id_owner}/vehicle/list"/>">Cancelar</a>
                        </c:if>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>