<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row">
                <div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary">
                    <h3><i class="fa fa-truck" aria-hidden="true"></i> VehÝculos</h3></div>
            </div>

            <div class="alert alert-primary" role="alert">
                <i class="fa fa-info-circle" aria-hidden="true"></i> Si la busqueda no tiene resultados puede ver <a
                    href="<c:url value="/vehicle/list"/>">"Todos los vehÝculos"</a>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-truck"></i> Buscar VehÝculos</h5>
                    <p class="card-text">Ingrese la placa del vehÝculo para buscar.</p>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                        <input id="vehicle-finder-input" type="text" class="form-control" placeholder="Buscar..." aria-label="search" aria-describedby="basic-addon1">
                    </div>
                    <div id="search_result" class="md-2 p-2"></div>
                    <a role="button" href="<c:url value="/vehicle/list"/>" class="btn btn-outline-primary"><i class="fa fa-list"></i> Ver todos</a>
                </div>
            </div>
        </div>
    </div>
</main>