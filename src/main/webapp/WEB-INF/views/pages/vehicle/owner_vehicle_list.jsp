<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="id_owner" type="java.lang.Long"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/owner/index"/>'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/owner/${id_owner}"/>'>Propietario</a></li>
                    <li class="breadcrumb-item">Veh�culos</li>
                </ol>
            </nav>

            <div id="information-panel"></div>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div id="owner_id" hidden="hidden">${id_owner}</div>
                            <h5 class="card-title">Veh�culos registrados propiedad de</h5>
                            <h6><a id="owner_name" href="<c:url value="/owner/${id_owner}"/>"></a></h6>
                            <div class="text-muted small" id="extra-info"></div>
                        </div>
                        <div class="col-6 text-right">
                            <a role="button" href="<c:url value="/owner/${id_owner}/vehicle/new"/>" class="btn btn-outline-primary"><i class="fa fa-plus"></i> Agregar nuevo veh�culo</a>
                            <a href="<c:url value="/owner/${id_owner}/vehicle/casefile/list"/>" class="btn btn-outline-primary"><i class="fa fa-folder-open-o"></i> Ver expedientes</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-sm table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th>Placa</th>
                        <th>Tipo</th>
                        <th>Contenido</th>
                        <th>Empleados</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="vehicles" type="java.util.List<edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO>"--%>
                    <c:forEach items="${vehicles}" var="item">
                        <tr <c:if test="${not item.active}">class = "table-danger"</c:if>>
                            <td hidden="hidden" class="id_vehicle">${item.id_vehicle}</td>
                            <td><a href="<c:url value="/vehicle/${item.id_vehicle}/info"/>">${item.vehicle_license}</a></td>
                            <td>${item.vehicle_type}</td>
                            <td>${item.vehicle_transported_content}</td>
                            <td class="cell-center-middle">${item.vehicle_employees_number}</td>
                            <td class="cell-center-middle">
                                <div class="btn-group" role="group" aria-label="table options">
                                    <a role="button" href="<c:url value="/vehicle/${item.id_vehicle}/info"/>" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top" title="Mostras informaci�n"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                                    <a role="button" href="<c:url value="/owner/${id_owner}/vehicle/${item.id_vehicle}/edit"/>" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top" title="Editar veh�culo"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <c:if test="${item.active}">
                                        <button type="button" class="btn btn-outline-secondary show-to-disable-modal" data-toggle="tooltip" data-placement="top" title="Deshabilitar veh�culo"><i class="fa fa-minus-square-o" aria-hidden="true"></i></button>
                                    </c:if>
                                    <c:if test="${not item.active}">
                                        <button type="button" class="btn btn-outline-secondary show-to-enable-modal" data-toggle="tooltip" data-placement="top" title="Habilitar veh�culo"><i class="fa fa-check-square-o" aria-hidden="true"></i></button>
                                    </c:if>
                                    <button type="button" class="btn btn-outline-secondary show-delete-modal" data-toggle="tooltip" data-placement="top" title="Eliminar veh�culo"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<!-- Vehicle to disable Modal -->
<div class="modal fade" id="vehicle-to-disable-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="disable-title">Deshabilitar veh�culo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-info-circle fa-2x"></i>
                <h5>�Acepta deshabilitar este veh�culo?</h5>
                <p class="text-muted small">
                    Los veh�culos deshabilitados no podran iniciar un proceso de solicitud de permiso sanitario, este veh�culo podra ser habilitado despues si asi se requiere.
                </p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a id="vehicle-disable-btn" href="#" role="button" class="btn btn-warning">Aceptar</a>
            </div>
        </div>
    </div>
</div>

<!-- Vehicle to enable Modal -->
<div class="modal fade" id="vehicle-to-enable-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="enable-title">Habilitar veh�culo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-info-circle fa-2x"></i>
                <h5>�Acepta habilitar este veh�culo?</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a id="vehicle-enable-btn" href="#" role="button" class="btn btn-primary">Aceptar</a>
            </div>
        </div>
    </div>
</div>

<!-- Delete vehicle Modal -->
<div class="modal fade" id="delete-vehicle-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="delete-title">Eliminar veh�culo</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-warning fa-2x"></i>
                <h5>�Esta seguro en eliminar este veh�culo?</h5>
                <p class="text-muted small">Si borra este veh�culo perdera todos los datos referentes a el y se borrara el registro de este veh�culo en los permisos que este contenido.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a id="vehicle-delete-btn" href="#" role="button" class="btn btn-danger">Aceptar</a>
            </div>
        </div>
    </div>
</div>
