<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/vehicle/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/vehicle/list"/>">�rea de
                        vehiculos</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Informaci�n de grupo de solicitudes</li>
                </ol>
            </nav>
            <%--@elvariable id="vehicles" type="java.util.List<edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO>"--%>
            <%--@elvariable id="bunch" type="edu.ues.aps.process.area.vehicle.dto.AbstractVehicleBunchDTO"--%>

            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-dark rounded box-shadow">
                <div class="lh-100">
                    <h4 class="mb-0 text-white lh-100">Informaci�n del grupo de veh�culos</h4>
                </div>
            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h5 class="border-bottom border-gray pb-2 mb-0">Informaci�n completa</h5>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">N�mero original de veh�culos en la solicitud</strong>
                        ${bunch.vehicle_count}
                    </p>
                </div>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Los productos son distribuidos por</strong>
                        ${bunch.distribution_company}
                    </p>
                </div>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Los productos son distribuidos a</strong>
                        ${bunch.distribution_destination}
                    </p>
                </div>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Lugar de distribuci�n</strong>
                        ${bunch.marketing_place}
                    </p>
                </div>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Direcci�n de permanencia</strong>
                        ${bunch.address_details}
                    </p>
                </div>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Nombre de la persona presente en la inspecci�n</strong>
                        ${bunch.client_presented_name}
                    </p>
                </div>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Tel�fono de la persona presente en la inspecci�n</strong>
                        ${bunch.client_presented_telephone}
                    </p>
                </div>

                <p class="d-block text-right mt-3">
                    <a href="<%--@elvariable id="id_bunch" type="java.lang.Long"--%>
                    <c:url value="/vehicle/case-file/bunch/${id_bunch}/edit"/>">Editar
                        veh�culo</a>
                </p>
            </div>

            <div class="list-group">
                <c:forEach var="item" items="${vehicles}">
                    <a href="<c:url value="/vehicle/${item.id_vehicle}/info"/>" class="list-group-item list-group-item-action flex-column align-items-start">
                        <div class="d-flex w-100 justify-content-between">
                            <h5 class="mb-1">${item.vehicle_license}</h5>
                            <small>${item.vehicle_type}</small>
                        </div>
                        <p class="mb-1">Propietario: ${item.owner_name}</p>
                        <small>${item.vehicle_transported_content}</small>
                    </a>
                </c:forEach>
            </div>
            <div class="btn-group mt-3" role="group" aria-label="Basic example">
                <button type="button" class="btn btn-outline-secondary goback">Volver</button>
            </div>
        </div>
    </div>
</main>