<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:importAttribute name="stylesheets" />
<html>
<head>
    <title>Sesión Invalida</title>
    <c:forEach var="css" items="${stylesheets}">
        <link rel="stylesheet" type="text/css" href='<c:url value="${css}"/>'>
    </c:forEach>
</head>
<body style="position:absolute;width: 100%;top: 25%;">
<div class="container-fluid">
    <div class="row">
        <div class="col-10 mx-auto">
            <div class="card bg-danger text-white">
                <div class="card-body text-center">
                    <i class="fa fa-ban fa-5x" aria-hidden="true"></i>
                    <h1 class="card-title">Sesión invalida</h1>
                    <div class="card-text">Ya tiene una sessión iniciada o su sesión a expirado.</div>
                    <br>
                    <a href="<c:url value="/login"/>" role="button" class="btn btn-light">Salir</a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>
