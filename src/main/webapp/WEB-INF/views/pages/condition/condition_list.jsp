<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="id_establishment" type="java.lang.Long"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/"/>'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/resolution/index"/>'>Resoluciones</a></li>
                    <li class="breadcrumb-item">Condiciones</li>
                </ol>
            </nav>

            <div class="card mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-8 text-left">
                            <div class="card-title"><h5>Condiciones registradas para resoluciones de permisos</h5></div>
                            <h6 class="card-subtitle mb-2 text-muted">Configuraci�n de las condiciones manejadas por los t�cnicos.</h6>
                        </div>
                        <div class="col-4 text-right">
                            <a href="<c:url value="/resolution-condition/new"/>" class="btn btn-outline-secondary mt-3"><i class="fa fa-plus"></i> Nueva condici�n para resoluciones</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover" width="100%" cellspacing="0" id="dataTable">
                    <thead>
                    <tr>
                        <th hidden></th>
                        <th>Condici�n</th>
                        <th>�rea</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="conditions" type="java.util.List<edu.ues.aps.process.resolution.base.dto.AbstractConditionDTO>"--%>
                    <c:forEach items="${conditions}" var="item">
                        <tr>
                            <td class="id_condition" hidden>${item.id_condition}</td>
                            <td>${item.condition}</td>
                            <td class="cell-center-middle">${item.section}</td>
                            <td class="cell-center-middle">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <button type="button" class="btn btn-outline-secondary show-condition-info-modal" data-toggle="tooltip" data-placement="top" title="Ver detalles"><i class="fa fa-folder-open-o" aria-hidden="true"></i></button>
                                    <a href="<c:url value="/resolution-condition/${item.id_condition}/edit"/>" role="button" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <button type="button" class="btn btn-outline-secondary show-condition-delete-confirmation-modal" data-toggle="tooltip" data-placement="top" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></button>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<!-- Condition information model -->
<div class="modal fade" id="condition-info-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="condition-info-title">Informaci�n</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <div class="form-group row">
                        <label for="content" class="col-sm-2 col-form-label font-weight-bold">Contenido</label>
                        <div class="col-sm-10">
                            <p id="content"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="section" class="col-sm-2 col-form-label font-weight-bold">�rea</label>
                        <div class="col-sm-10">
                            <p id="section"></p>
                        </div>
                    </div>
                    <div class="form-group row">
                        <label for="count" class="col-sm-2 col-form-label font-weight-bold">N�mero de usos</label>
                        <div class="col-sm-10">
                            <p id="count"></p>
                        </div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Condition delete confirmation modal -->
<div class="modal fade" id="condition-delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="condition-delete-title">Eliminar condici�n</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-warning fa-2x"></i>
                <h5>�Confirma eliminar la condici�n seleccionada?</h5>
                <p class="text-muted small">Las condiciones que se eliminen no se podran seleccionar nuevamente.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a href="#" id="condition-delete-btn" role="button" class="btn btn-danger">Aceptar</a>
            </div>
        </div>
    </div>
</div>