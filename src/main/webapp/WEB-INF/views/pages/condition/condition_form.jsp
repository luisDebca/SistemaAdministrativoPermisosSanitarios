<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/" />'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/resolution/index"/>'>Resoluci�n</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/resolution-condition/list"/>'>Condiciones</a></li>
                    <li class="breadcrumb-item">Formulario de condiciones</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Formulario de condiciones para resoluciones</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Complete la condici�n.</h6>
                    <hr class="my-4">
                    <div class="btn-group mt-2 mb-4" role="group" aria-label="Basic example">
                        <button id="add-years" type="button" class="btn btn-outline-secondary"><i class="fa fa-clock-o"></i> Duraci�n del permiso.</button>
                        <button id="add-end" type="button" class="btn btn-outline-secondary"><i class="fa fa-calendar"></i> Fecha final del permiso</button>
                    </div>
                    <%--@elvariable id="condition" type="edu.ues.aps.process.resolution.base.model.Condition"--%>
                    <form:form id="main" method="POST" modelAttribute="condition">
                        <form:hidden path="id"/>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Condici�n</label>
                            <div class="col-8">
                                <form:textarea
                                        id="condition"
                                        class="form-control"
                                        path="condition"
                                        rows="4"
                                        cols="50"
                                        maxlength="2500"
                                />
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>
                        <div class="form-group row">
                            <label class="col-2 col-form-label">�rea</label>
                            <div class="col-4">
                                <form:select path="section" class="custom-select">
                                    <option value="INVALID" selected="selected" disabled="disabled">Seleccione una opci�n</option>
                                    <form:option value="ESTABLISHMENT">Establecimientos</form:option>
                                    <form:option value="VEHICLE">Veh�culos</form:option>
                                    <form:option value="TOBACCO">Tabaco</form:option>
                                </form:select>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>
                        <hr class="my-4">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="<c:url value="/resolution-condition/list"/>" class="btn btn-secondary">Cancelar</a>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>

