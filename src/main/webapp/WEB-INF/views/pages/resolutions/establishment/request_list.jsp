<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/resolution/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Resoluciones y diplomas</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">

                <table class="table table-hover establishment-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>Solicitud</th>
                        <th>Resoluci�n</th>
                        <th>Establecimiento</th>
                        <th>Tipo</th>
                        <th>Rubro</th>
                        <th>NIT</th>
                        <th>Direcci�n</th>
                        <th>Propietario</th>
                        <th>SIBASI/UCSF</th>
                        <th>Permiso</th>
                        <th>Recepci�n</th>
                        <th>Fecha de resoluci�n</th>
                        <th>Memorandums</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.resolution.establishment.dto.EstablishmentRequestResolutionDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td class="id_owner" hidden="hidden">${item.id_owner}</td>
                            <td class="id_establishment" hidden="hidden">${item.id_establishment}</td>
                            <td class="id_caseFile" hidden="hidden">${item.id_caseFile}</td>
                            <td><a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>">${item.folder_number}</a>
                            </td>
                            <td><a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>">${item.request_number}</a>
                            </td>
                            <td>
                                <c:if test="${not item.resolution_created}">
                                    <span class="badge badge-secondary">No creada</span>
                                </c:if>
                                    ${item.resolution_number}
                            </td>
                            <td>
                                <a href="<c:url value="/establishment/${item.id_establishment}"/>">${item.establishment_name}</a>
                            </td>
                            <td>${item.establishment_type_detail}</td>
                            <td>${item.establishment_section}/${item.establishment_type}</td>
                            <td>${item.establishment_nit}</td>
                            <td>${item.establishment_address}</td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.SIBASI}/${item.UCSF}</td>
                            <td>${item.certification_type}</td>
                            <td>${item.caseFile_createOn}</td>
                            <td>${item.resolution_date}</td>
                            <td class="cell-center-middle">
                                <c:if test="${item.memo_count > 0}"><span class="badge badge-primary">${item.memo_count} Memos</span></c:if>
                                <c:if test="${item.memo_count == 0}"><span class="badge badge-secondary">Sin<br>Memorandums</span></c:if>
                            </td>
                            <td class="table-options cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="grouInf">
                                        <a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>"
                                           role="button" class="btn btn-outline-secondary" data-toggle="tooltip"
                                           data-placement="top" title="Ver detalles del expediente"><i
                                                class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                                        <c:if test="${item.memo_count > 0 and item.resolution_created}">
                                            <a href="javascript:" role="button" class="btn btn-outline-secondary show-control-sheet-modal"
                                               data-toggle="tooltip" data-placement="top" title="Generar hoja de control de salida de expedientes"><i
                                                    class="fa fa-file-code-o" aria-hidden="true"></i></a>
                                        </c:if>
                                        <a href="<c:url value="/resolution/memorandum/${item.id_caseFile}/list"/>"
                                           role="button" class="btn btn-outline-secondary" data-toggle="tooltip"
                                           data-placement="top" title="Ir a memorandums"><i class="fa fa-files-o"
                                                                                            aria-hidden="true"></i></a>
                                        <c:if test="${item.resolution_created}">
                                            <a href="#" role="button"
                                               class="btn btn-outline-secondary disabled_href show-resolution-signatory-selector-modal"
                                               data-toggle="tooltip" data-placement="top" title="Generar resoluci�n"><i
                                                    class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                            <a href="#" role="button"
                                               class="btn btn-outline-secondary disabled_href show-diploma-signatory-selector-modal"
                                               data-toggle="tooltip" data-placement="top" title="Generar diploma"><i
                                                    class="fa fa-file-powerpoint-o" aria-hidden="true"></i></a>
                                            <a href="<c:url value="/resolution/establishment/${item.id_caseFile}/edit"/>"
                                               role="button" class="btn btn-outline-secondary"
                                               data-toggle="tooltip" data-placement="top" title="Editar resoluci�n"><i
                                                    class="fa fa-pencil-square-o" aria-hidden="true"></i></a>

                                        </c:if>
                                        <c:if test="${not item.resolution_created}">
                                            <a href="<c:url value="/resolution/establishment/${item.id_caseFile}/new"/>"
                                               role="button" class="btn btn-outline-secondary"
                                               data-toggle="tooltip" data-placement="top" title="Crear resoluci�n"><i
                                                    class="fa fa-plus" aria-hidden="true"></i></a>
                                        </c:if>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <c:if test="${item.resolution_created and item.memo_count > 0}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary move_to_next_process_btn"
                                                    data-toggle="tooltip" title="Pasar a revisi�n de permisos"><i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                        </c:if>
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                    aria-hidden="true"></i>
                                        </button>
                                        <button type="button"
                                                class="btn btn-outline-secondary show_process_record_summary"
                                                data-toggle="tooltip" title="Ver resumen de proceso"><i
                                                class="fa fa-share-alt" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<!-- Control sheet report modal -->
<div class="modal fade" id="control-sheet-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="controlsheetmodaltitle">Hoja de control</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Ingrese el nombre de los autorizados en la hoja de control</h5>
                <form id="control-sheet-form">
                    <div class="form-group">
                        <label for="name1">Persona autirizada 1</label>
                        <input type="text" class="form-control control_sheet_names" id="name1" maxlength="100">
                    </div>

                    <div class="form-group">
                        <label for="name2">Persona autirizada 2</label>
                        <input type="text" class="form-control control_sheet_names" id="name2" maxlength="100">
                    </div>

                    <div class="form-group">
                        <label for="name3">Persona autirizada 3</label>
                        <input type="text" class="form-control control_sheet_names" id="name3" maxlength="100">
                    </div>

                    <div class="form-group">
                        <label for="name4">Persona autirizada 4</label>
                        <input type="text" class="form-control control_sheet_names" id="name4" maxlength="100">
                    </div>

                    <div class="form-group">
                        <label for="name5">Persona autirizada 5</label>
                        <input type="text" class="form-control control_sheet_names" id="name5" maxlength="100">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button id="add-new-control-sheet-name" class="btn btn-secondary" type="button">Agregar nombre</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary generate-control-sheet-report">Generar</button>
            </div>
        </div>
    </div>
</div>

<!-- Resolution report modal -->
<div class="modal fade" id="resolution-report-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="resolution-report-title">Resoluci�n</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Ingrese los datos del firmante del documento</h5>
                <form>
                    <div class="form-group">
                        <label for="signatory-name">Nombre</label>
                        <input type="text" class="form-control" id="signatory-name">
                    </div>
                    <div class="form-group">
                        <label for="signatory-charge">Cargo</label>
                        <input type="text" class="form-control" id="signatory-charge">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary generate-resolution-report">Generar</button>
            </div>
        </div>
    </div>
</div>

<!-- Diploma report modal -->
<div class="modal fade" id="diploma-report-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="diploma-report-title">Diploma</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Ingrese los datos del firmante del diploma</h5>
                <form>
                    <div class="form-group">
                        <label for="diploma-signatory-name">Nombre</label>
                        <input type="text" class="form-control" id="diploma-signatory-name">
                    </div>
                    <div class="form-group">
                        <label for="diploma-signatory-charge">Cargo</label>
                        <input type="text" class="form-control" id="diploma-signatory-charge">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary generate-diploma-report">Generar</button>
            </div>
        </div>
    </div>
</div>