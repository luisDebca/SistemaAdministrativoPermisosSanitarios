<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/resolution/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Resoluciones y diplomas</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover vehicle-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>N� Solicitudes</th>
                        <th>Propietario</th>
                        <th>Compa�ia de distribuci�n</th>
                        <th>Destino de distribuci�n</th>
                        <th>Lugar de mercancia</th>
                        <th>Direcci�n de estancia</th>
                        <th>Fecha de resoluci�n</th>
                        <th>Memorandums</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionRequestDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td hidden="hidden" class="id_owner">${item.id_owner}</td>
                            <td hidden="hidden" class="id_bunch">${item.id_bunch}</td>
                            <td class="cell-center-middle">${item.request_folder}</td>
                            <td class="cell-center-middle"><h5>${item.request_count}</h5></td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.bunch_distribution_company}</td>
                            <td>${item.bunch_distribution_destination}</td>
                            <td>${item.bunch_marketing_place}</td>
                            <td>${item.bunch_address}</td>
                            <td class="cell-center-middle">${item.resolution_date}</td>
                            <td class="cell-center-middle">
                                <c:if test="${item.memo_count > 0}"><span class="badge badge-primary">${item.memo_count} Memos</span></c:if>
                                <c:if test="${item.memo_count == 0}"><span class="badge badge-secondary">Sin<br>Memorandums</span></c:if>
                            </td>
                            <td class="cell-center-middle table-options">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="groupInf">
                                        <a href="javascript:" class="btn btn-outline-secondary disabled_href show_brief_bunch_information"
                                           data-toggle="tooltip" title="Ver solicitudes contenidas"><i class="fa fa-th" aria-hidden="true"></i></a>
                                        <a href="<c:url value="/resolution/memorandum/bunch/${item.id_bunch}/all"/>"
                                           role="button" class="btn btn-outline-secondary" data-toggle="tooltip"
                                           data-placement="top" title="Ir a memorandums"><i class="fa fa-files-o" aria-hidden="true"></i></a>
                                        <c:if test="${not item.resolution_created}">
                                            <a href="<c:url value="/resolution/vehicle/bunch/${item.id_bunch}/new"/>"
                                               class="btn btn-outline-secondary" data-toggle="tooltip" title="Crear resoluci�n">
                                                <i class="fa fa-plus" aria-hidden="true"></i></a>
                                        </c:if>
                                        <c:if test="${item.resolution_created}">
                                            <a href="<c:url value="/resolution/vehicle/bunch/${item.id_bunch}/edit"/>"
                                               class="btn btn-outline-secondary" data-toggle="tooltip" title="Editar resoluci�n">
                                                <i class="fa fa-edit" aria-hidden="true"></i></a>
                                            <a href="javascript:" class="btn btn-outline-secondary generate-resolution-report-btn"
                                               data-toggle="tooltip" title="Generar resoluciones">
                                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                            <a href="javascript:" class="btn btn-outline-secondary generate-diploma-report-btn"
                                               data-toggle="tooltip" title="Generar diplomas">
                                                <i class="fa fa-file-pdf-o" aria-hidden="true"></i></a>
                                        </c:if>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <c:if test="${item.resolution_created and item.memo_count > 0}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary disabled_href move_to_next_process_btn"
                                                    data-toggle="tooltip" title="Pasar a revisi�n de permisos"><i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                        </c:if>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                    aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<!-- Resolution report modal -->
<div class="modal fade" id="resolution-report-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="resolution-report-title">Resoluci�n</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Ingrese los datos del firmante del documento</h5>
                <form>
                    <div class="form-group">
                        <label for="resolution-signatory-name">Nombre</label>
                        <input type="text" class="form-control" id="resolution-signatory-name">
                    </div>
                    <div class="form-group">
                        <label for="resolution-signatory-charge">Cargo</label>
                        <input type="text" class="form-control" id="resolution-signatory-charge">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary generate-resolution-report">Generar</button>
            </div>
        </div>
    </div>
</div>

<!-- Diploma report modal -->
<div class="modal fade" id="diploma-report-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="diploma-report-title">Diploma</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h5>Ingrese los datos del firmante del diploma</h5>
                <form>
                    <div class="form-group">
                        <label for="diploma-signatory-name">Nombre</label>
                        <input type="text" class="form-control" id="diploma-signatory-name">
                    </div>
                    <div class="form-group">
                        <label for="diploma-signatory-charge">Cargo</label>
                        <input type="text" class="form-control" id="diploma-signatory-charge">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary generate-diploma-report">Generar</button>
            </div>
        </div>
    </div>
</div>