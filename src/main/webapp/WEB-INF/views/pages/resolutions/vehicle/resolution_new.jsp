<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%--@elvariable id="inspections" type="java.util.List<edu.ues.aps.process.resolution.base.dto.AbstractResolutionFormDTO>"--%>
<%--@elvariable id="conditions" type="java.util.List<edu.ues.aps.process.resolution.base.model.Condition>"--%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/resolution/index"/>'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/resolution/establishment/list"/>'>Resolutiones</a></li>
                    <li class="breadcrumb-item">Formulario de resoluci�n</li>
                </ol>
            </nav>

            <div class="information-panel">
                <c:if test="${empty inspections}">
                    <div id="info-empty-error-alert" class="alert alert-warning" role="alert">
                        <i class="fa fa-warning"></i> Para generar la resoluci�n es necesario al menos uno de los memor�ndums de la UCSF correspondiente, de lo contrario puede iniciar uno vac�o seleccionando la opci�n en 'Inspecci�n base'.
                    </div>
                </c:if>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Formulario de resoluciones</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Complete el contenido de la resoluci�n y selecciones las condiciones contenidas dentro de el documento.</h6>

                    <h5 class="text-muted pt-4">Veh�culos contenidos</h5>
                    <hr class="my-2">

                    <table class="table">
                        <thead class="thead-dark">
                        <tr>
                            <th scope="col">N�</th>
                            <th scope="col">Placa</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">Transporte de alimentos</th>
                            <th scope="col">Solicitud</th>
                            <th scope="col">N�mero resoluci�n</th>
                            <th scope="col">C�digo</th>
                            <th scope="col">A�o</th>
                        </tr>
                        </thead>
                        <tbody>
                        <%--@elvariable id="resolutions" type="java.util.List<edu.ues.aps.process.resolution.vehicle.dto.AbstractVehicleResolutionFormDTO>"--%>
                        <c:forEach var="item" items="${resolutions}" varStatus="varst">
                            <tr>
                                <td>${varst.index + 1}</td>
                                <td>${item.license}</td>
                                <td>${item.vehicleType}</td>
                                <td>${item.contentType}</td>
                                <td>${item.certificationType}</td>
                                <td>${item.number + varst.index}</td>
                                <td>${item.code}</td>
                                <td>${item.year}</td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <%--@elvariable id="resolution" type="edu.ues.aps.process.resolution.base.model.Resolution"--%>
                    <form:form modelAttribute="resolution" method="POST" id="main">
                        <form:hidden path="id"/>
                        <form:textarea id="document_content" path="content" class="form-control" hidden="hidden"/>

                        <h5 class="text-muted pt-4">Condiciones de permiso</h5>
                        <hr class="my-2">
                        <c:if test="${empty conditions}">
                            <div class="alert alert-warning" role="alert">
                                No se encontraron condiciones disponibles. <a
                                    href="<c:url value="/resolution-condition/new"/>" class="alert-link">click aqui</a>.
                                para agregar una nueva condici�n.
                            </div>
                        </c:if>

                        <c:forEach items="${conditions}" var="item" varStatus="varst">
                            <div class="custom-control custom-checkbox pr-4">
                                <c:if test="${resolution.conditions.contains(item)}">
                                    <form:checkbox class="custom-control-input resolution_conditions" id="customCheck${varst.index}" path="conditions" value="${item.id}" checked="checked" form="main"/>
                                </c:if>
                                <c:if test="${not resolution.conditions.contains(item)}">
                                    <form:checkbox class="custom-control-input resolution_conditions" id="customCheck${varst.index}" path="conditions" value="${item.id}" form="main"/>
                                </c:if>
                                <label class="custom-control-label" for="customCheck${varst.index}">${item.condition}</label>
                            </div>
                            <br>
                        </c:forEach>
                    </form:form>
                    <hr class="my-2">
                    <div class="form-group row mt-4">
                        <label for="inspection_memo" class="col-sm-2 col-form-label">Inspecci�n base</label>
                        <div class="col-sm-4">
                            <select class="custom-select" id="inspection_memo" name="inspection_memo">
                                <option disabled="disabled" selected="selected">Seleccione una opci�n</option>
                                <option value="new-template">Iniciar desde cero</option>
                                <c:forEach var="item" items="${inspections}" varStatus="varst">
                                    <option value="memo-${varst.index}">${item.memo}</option>
                                </c:forEach>
                            </select>
                        </div>
                    </div>

                    <h5 class="text-muted pt-4">Contenido de la resoluci�n</h5>
                    <hr class="my-3">
                    <div id="froala-editor">
                        <p id="editor-default-text">Seleccione un tipo de resoluci�n</p>
                    </div>

                    <hr id="form-button" class="my-4">

                    <c:if test="${not empty conditions}">
                        <input type="submit" id="save-resolution-btn" class="btn btn-primary" value="Guardar" form="main">
                    </c:if>
                    <a href="<c:url value="/resolution/vehicle/all"/>" role="button" class="btn btn-secondary">Cancelar</a>

                    <c:forEach items="${inspections}" var="item" varStatus="varst">
                        <div id="memo-${varst.index}" class="default_resolution_content" hidden="hidden">
                            <p><strong style="font-weight: bold">El Infrascrito Director de la Regi�n de Salud Metropolitana,</strong>
                            dependencia del Ministerio de Salud, con base a los Articulos 83 y 86, del C�digo de Salud,
                                previos los tr�mites legales y visto el informe de inspecci�n sanitaria, realizada por la ${item.inspector_name},
                             ${item.inspector_charge}, procedente de la Direcci�n de la Unidad Comunitaria de Salud Familiar ${item.ucsf},
                                por medio del Memor�ndum N� ${item.memo} , asi como haber tenido a la vista los detalles de la naturaleza de
                                las actividades a realizar descritos en la documentaci�n anexa a la solicitud, esta Direcci�n Resuelve: Otorgar
                                .CERTIFICACION Permiso Sanitario al veh�culo para el transporte de alimentos .TRASPORTE:
                            </p>
                        </div>
                    </c:forEach>
                    <div id="new-template" class="default_resolution_content" hidden="hidden">
                        <p><strong style="font-weight: bold">El Infrascrito Director de la Regi�n de Salud Metropolitana,</strong>
                            dependencia del Ministerio de Salud, con base a los Articulos 83 y 86, del C�digo de Salud,
                            previos los tr�mites legales y visto el informe de inspecci�n sanitaria, realizada por la ___________________________________,
                            _________________________, procedente de la Direcci�n de la Unidad Comunitaria de Salud Familiar ______________,
                            por medio del Memor�ndum N� ___________________ , asi como haber tenido a la vista los detalles de la naturaleza de
                            las actividades a realizar descritos en la documentaci�n anexa a la solicitud, esta Direcci�n Resuelve: Otorgar
                            .CERTIFICACION Permiso Sanitario al veh�culo para el transporte de alimentos .TRASPORTE:
                        </p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>