<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value='/document-template/all'/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value='/requirement/all'/>">Requerimientos</a></li>
                    <li class="breadcrumb-item active">Nuevo requerimiento</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">
                    <%--@elvariable id="requirement" type="edu.ues.aps.process.requirement.base.model.Requirement"--%>
                    <form:form id="main" modelAttribute="requirement" method="POST">

                        <form:hidden path="id"/>

                        <h5 class="card-title">Nuevo requerimiento</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Ingrese el requerimiento a crear.</h6>
                        <hr class="my-4">

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Requerimiento</label>
                            <div class="col-8">
                                <form:textarea
                                        class="form-control"
                                        path="requirement"
                                        rows="3"
                                        maxlength="500"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <hr class="my-4">

                        <input type="submit" class="btn btn-primary" value="Guardar">
                        <a href='<c:url value="/requirement/all"/>' class="btn btn-secondary">Cancelar</a>
                    </form:form>
                </div>
            </div>

        </div>
    </div>
</main>
