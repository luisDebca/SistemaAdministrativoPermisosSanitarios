<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/index"/>'>Inicio</a></li>
                    <li class="breadcrumb-item"><a>Hojas de control de requisitos</a></li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-8">
                            <h5 class="card-title">Hojas de requisitos</h5>
                            <h6 class="card-subtitle text-muted">Configuraci�n de las hojas de requisitos para revisi�n de requerimientos.</h6>
                        </div>
                        <div class="col-4 text-right">
                            <div class="btn-group-vertical">
                                <a class="btn btn-outline-secondary" href="<c:url value="/document-template/new"/>" role="button"><i class="fa fa-plus"></i> Nueva hoja de requisitos</a>
                                <a class="btn btn-outline-secondary" href="<c:url value="/requirement/all"/>" role="button"><i class="fa fa-list"></i> Ver Requerimientos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden></th>
                        <th>T�tulo</th>
                        <th>Subtitulo</th>
                        <th>�rea</th>
                        <th>Versi�n</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="templates" type="java.util.List"--%>
                    <c:forEach items="${templates}" var="item">
                        <tr>
                            <td class="id_template" hidden="hidden">${item.id_template}</td>
                            <td>${item.title}</td>
                            <td>${item.subtitle}</td>
                            <td>${item.type}</td>
                            <td align="center">${item.version}</td>
                            <td class="table-options cell-center-middle">
                                <div class="btn-group" role="group">
                                    <a role="button" href="#"
                                       class="btn btn-outline-secondary disabled_href on-click-show-requirements-list-from-template-btn"
                                       data-toggle="tooltip" title="Ver lista de requerimientos"><i class="fa fa-list"
                                                                                                    aria-hidden="true"></i></a>
                                    <a role="button" class="btn btn-outline-secondary"
                                       href="<c:url value="/document-template/${item.id_template}/edit"/>"
                                       data-toggle="tooltip" title="Editar Hoja de requerimientos"><i
                                            class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    <a role="button" class="btn btn-outline-secondary"
                                       href="<c:url value="/document-template/${item.id_template}/new-version"/>"
                                       data-toggle="tooltip" title="Crear nueva versi�n de esta hoja de requisitos"><i
                                            class="fa fa-files-o" aria-hidden="true"></i></a>
                                    <a role="button" href="#" class="btn btn-outline-secondary disabled_href delete-template-btn"
                                       data-toggle="tooltip" title="Eliminar"><i class="fa fa-trash-o"
                                                                                 aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>


<!-- DOCUMENT TEMPLATE REQUIREMENT LIST MODAL -->
<div class="modal fade" id="document-template-requirements-list-modal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Requisitos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>N�</th>
                        <th>Requerimiento</th>
                        <th></th>
                    </tr>
                    </thead>
                    <tbody id="document-template-requirement-list"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Document template elimination confirmation-->
<div class="modal fade" id="modal-delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Eliminaci�n del Modelo de Hoja de requisitos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i><br>
                <h5>�Esta seguro de eliminar esta hoja de requisitos?</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a id="confirmation-btn" class="btn btn-danger" href="#">Aceptar</a>
            </div>
        </div>
    </div>
</div>