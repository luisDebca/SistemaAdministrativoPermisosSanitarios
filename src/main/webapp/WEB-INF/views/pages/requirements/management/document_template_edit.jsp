<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/document-template/all"/>'>Inicio</a></li>
                    <li class="breadcrumb-item active">Editar hoja de requisitos</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">
                    <%--@elvariable id="template" type="edu.ues.aps.process.requirement.base.model.DocumentTemplate"--%>
                    <form:form id="main" method="POST" modelAttribute="template">
                    <form:hidden path="id"/>
                    <form:hidden path="version"/>

                    <h5 class="card-title">Editar hoja de requisitos</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Ingrese los datos requeridos para nueva hoja.</h6>
                    <hr class="my-4">

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Titulo</label>
                        <div class="col-8">
                            <form:input class="form-control" type="text" path="title"/>
                        </div>
                        <div class="required-type" data-toggle="tooltip" data-placement="top"
                             title="Este campo es obligatorio">*
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Subtitulo</label>
                        <div class="col-8">
                            <form:input class="form-control" type="text" path="subtitle"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Tipo de documento</label>
                        <div class="col-4">
                            <form:select path="type" class="custom-select">
                                <form:option value="INVALID" disabled="true"
                                             selected="true">Seleccione una opci�n</form:option>
                                <form:option value="ESTABLISHMENT">Establecimientos</form:option>
                                <form:option value="VEHICLE">Veh�culos</form:option>
                                <form:option value="TOBACCO">Tabaco</form:option>
                            </form:select>
                        </div>
                        <div class="required-type" data-toggle="tooltip" data-placement="top"
                             title="Este campo es obligatorio">*
                        </div>
                    </div>

                    <hr class="my-4">
                    <h6 class="card-subtitle mb-2 text-muted">Seleccione los requerimientos contenidos en esta hoja</h6>
                    <p class="text-muted small">Seleccione al menos un requerimiento para el modelo de hoja de
                        requisitos.</p>
                        <%--@elvariable id="requirements" type="java.util.List<edu.ues.aps.process.requirement.base.dto.AbstractRequirementDTO>"--%>
                        <%--@elvariable id="ids" type="java.util.List<java.lang.Long>"--%>
                        <c:if test="${empty requirements}">
                            <div class="alert alert-warning" role="alert">
                                Requisitos no encontrados. Click aqui para <a href="<c:url value="/requirement/new"/>" class="alert-link">Agregar nuevo</a> Requisito.
                            </div>
                        </c:if>
                    <c:forEach items="${requirements}" var="item">
                        <div class="custom-control custom-checkbox mx-2 my-3">
                            <c:if test="${ids.contains(item.id_requirement)}">
                                <form:checkbox class="custom-control-input" id="requirement-${item.id_requirement}"
                                               path="requirements" value="${item.id_requirement}" checked="checked"/>
                            </c:if>
                            <c:if test="${not ids.contains(item.id_requirement)}">
                                <form:checkbox class="custom-control-input" id="requirement-${item.id_requirement}"
                                               path="requirements" value="${item.id_requirement}"/>
                            </c:if>
                            <label class="custom-control-label"
                                   for="requirement-${item.id_requirement}">${item.requirement_requirement}</label>
                        </div>
                    </c:forEach>

                    <hr class="my-4">

                    <button type="submit" class="btn btn-primary">Actualizar</button>
                    <a href='<c:url value="/document-template/all"/>' class="btn btn-secondary">Cancelar</a>
                </div>
                </form:form>
            </div>
        </div>
    </div>
</main>