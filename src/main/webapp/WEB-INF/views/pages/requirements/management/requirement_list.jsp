<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/document-template/all"/>'>Inicio</a></li>
                    <li class="breadcrumb-item"><a>Requerimientos</a></li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-8">
                            <h5 class="card-title">Requerimientos</h5>
                            <h6 class="card-subtitle text-muted">Requisitos para ingreso de solicitudes.</h6>
                        </div>
                        <div class="col-4 text-right">
                            <div class="btn-group-vertical">
                                <a class="btn btn-outline-secondary" href="<c:url value="/requirement/new"/>"
                                   role="button"><i class="fa fa-plus"></i> Nuevo requerimiento</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th>Requerimiento</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="requirements" type="java.util.List<edu.ues.aps.process.requirement.base.dto.AbstractRequirementDTO>"--%>
                    <c:forEach var="item" items="${requirements}">
                        <tr>
                            <td hidden="hidden" class="id_requirement">${item.id_requirement}</td>
                            <td>${item.requirement_requirement}</td>
                            <td class="cell-center-middle">
                                <div class="btn-group" role="group">
                                    <a role="button" href="<c:url value="/requirement/${item.id_requirement}/edit"/>" class="btn btn-outline-secondary" data-toggle="tooltip" title="Editar"><i class="fa fa-edit" aria-hidden="true"></i></a>
                                    <a role="button" href="#" class="btn btn-outline-secondary disabled_href show-delete-confirmation-modal" data-toggle="tooltip" title="Eliminar"><i class="fa fa-trash-o" aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<!-- Requirement delete confirmation-->
<div class="modal fade" id="modal-delete-confirmation" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Eliminaci�n de requerimientos</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i><br>
                <h5>�Esta seguro de eliminar este requerimiento?</h5>
                <p class="text-muted small">Se eliminaran todos los registros del requerimiento dentro de las hojas de requisitos.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a id="confirmation-btn" class="btn btn-danger" href="#">Aceptar</a>
            </div>
        </div>
    </div>
</div>