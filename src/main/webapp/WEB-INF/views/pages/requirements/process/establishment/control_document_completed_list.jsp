<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/requirement-control/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item">Hojas de control de requisitos completas de establecimientos</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover establishment-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>Establecimiento</th>
                        <th>Tipo</th>
                        <th>Rubro</th>
                        <th>NIT</th>
                        <th>Direcci�n</th>
                        <th>Propietario</th>
                        <th>SIBASI/UCSF</th>
                        <th>Permiso</th>
                        <th>Recepci�n</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    <%--@elvariable id="documents" type="java.util.List<edu.ues.aps.process.requirement.establishment.dto.EstablishmentControlDocumentDTO>"--%>
                    <c:forEach items="${documents}" var="item">
                        <tr>
                            <td class="id_owner" hidden="hidden">${item.id_owner}</td>
                            <td class="id_establishment" hidden="hidden">${item.id_establishment}</td>
                            <td class="id_caseFile" hidden="hidden">${item.id_caseFile}</td>
                            <td><a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>">${item.folder_number}</a>
                            </td>
                            <td>
                                <a href="<c:url value="/establishment/${item.id_establishment}"/>">${item.establishment_name}</a>
                            </td>
                            <td>${item.establishment_type_detail}</td>
                            <td>${item.establishment_section}/${item.establishment_type}</td>
                            <td>${item.establishment_nit}</td>
                            <td>${item.establishment_address}</td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.SIBASI}/${item.UCSF}</td>
                            <td>${item.certification_type}</td>
                            <td>${item.caseFile_createOn}</td>
                            <td class="table-options cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="grouInf">
                                        <button type="button"
                                                class="btn btn-outline-secondary show-requirement-document-client-selector-modal-btn"
                                                data-toggle="tooltip" title="Generar hoja de requerimientos"><i
                                                class="fa fa-file-pdf-o"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary show-request-reception-client-selection-modal"
                                                data-toggle="tooltip" title="Generar acta de recepci�n"><i
                                                class="fa fa-file-pdf-o"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary requirement-control-request-info-modal-btn"
                                                data-toggle="tooltip" title="Ver detalles de solicitud"><i
                                                class="fa fa-folder-open-o"></i></button>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_next_process_btn"
                                                data-toggle="tooltip" title="Pasar a revisi�n de la solicitud"><i
                                                class="fa fa-arrow-circle-right"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary show_process_record_summary"
                                                data-toggle="tooltip" title="Ver resumen de proceso"><i class="fa fa-share-alt" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
<!-- Requirement document information modal -->
<div class="modal fade" id="requirement-control-request-info-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">Informaci�n</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body modal-info-text" id="requirement-control-request-info-modal-body">
                <div class=" bs-callout bs-callout-info modal-info-text-body-field">
                    <h4>Informaci�n del propietario</h4>
                    <div>Nombre</div>
                    <p id="info-modal-owner-name"></p>
                    <div>NIT</div>
                    <p id="info-modal-owner-nit"></p>
                </div>
                <div class=" bs-callout bs-callout-info modal-info-text-body-field">
                    <h4>Informaci�n del establecimiento</h4>
                    <div>Nombre</div>
                    <p id="info-modal-estbl-name"></p>
                    <div>NIT</div>
                    <p id="info-modal-estbl-nit"></p>
                    <div>Direcci�n</div>
                    <p id="info-modal-estbl-address"></p>
                    <div>Tipo de establecimiento</div>
                    <p id="info-modal-estbl-type"></p>
                </div>
                <div class=" bs-callout bs-callout-info modal-info-text-body-field">
                    <h4>Informaci�n de la solicitud</h4>
                    <div>Tipo de solicitud</div>
                    <p id="info-modal-request-type"></p>
                    <div>Fecha de recibimiento de la documentaci�n</div>
                    <p id="info-modal-request-received"></p>
                    <div>N�mero de requerimientos completados</div>
                    <p id="info-modal-request-req-completed"></p>
                    <div>N�mero de requerimientos imcompletos</div>
                    <p id="info-modal-request-req-incomplete"></p>
                    <div>Estado de la solicitud</div>
                    <div id="info-modal-request-status"></div>
                </div>

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>

<!-- Requirement control document report client selector modal-->
<div class="modal fade" id="requirement-document-pdf-options-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PDF Opciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-12">
                        <label for="approved-by-select">Nombre del inscrito</label>
                        <select id="approved-by-select" class="custom-select"></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label for="signed-by-select">Nombre del firmante</label>
                        <select id="signed-by-select" class="custom-select"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a role="button" class="btn btn-primary add-client-btn" href="javascript:">Agregar representante</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary generate-requirement-document-pdf-report-btn">Generar PDF
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Request reception report client selector modal-->
<div class="modal fade" id="request-reception-client-selection-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PDF Opciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-12">
                        <label for="client-select">Nombre del firmante</label>
                        <select id="client-select" class="custom-select"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a role="button" class="btn btn-primary add-client-btn" href="javascript:">Agregar representante</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary generate-request-reception-pdf-report-btn">Generar PDF
                </button>
            </div>
        </div>
    </div>
</div>

