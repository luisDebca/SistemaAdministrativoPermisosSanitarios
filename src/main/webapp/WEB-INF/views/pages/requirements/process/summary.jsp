<%--@elvariable id="accepted" type="java.lang.Boolean"--%>
<%--@elvariable id="template" type="edu.ues.aps.process.requirement.base.dto.AbstractDocumentTemplateDTO"--%>
<%--@elvariable id="base" type="edu.ues.aps.process.base.dto.AbstractBaseDTO"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div hidden="hidden">
                <div class="id_caseFile">${base.id_caseFile}</div>
                <div class="id_bunch">${base.id_group}</div>
                <div class="id_owner">${base.id_owner}</div>
                <div id="section">${base.sectionType}</div>
            </div>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/requirement-control/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Resumen de proceso</li>
                </ol>
            </nav>

            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-secondary rounded box-shadow">
                <div class="lh-100">
                    <h4 class="mb-0 text-white lh-100">Resumen de proceso:<strong> Folder ${base.folder }</strong></h4>
                </div>
            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h5 class="border-bottom border-gray pb-2 mb-0">Hoja de requerimientos ${accepted ? 'completa':'incompleta'}</h5>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">${template.title}</strong>
                        ${template.subtitle}
                    </p>
                </div>

                <table class="table table-striped">
                    <thead>
                    <tr>
                        <td>N�</td>
                        <td>Requerimiento</td>
                        <td>Selecci�n</td>
                        <td>Observaciones</td>
                    </tr>
                    </thead>
                    <tbody>
                        <%--@elvariable id="rows" type="java.util.List<edu.ues.aps.process.requirement.base.dto.RequirementRowDTO>"--%>
                        <c:forEach items="${rows}" var="item" varStatus="varst">
                            <tr>
                                <td>${varst.index + 1}</td>
                                <td>${item.row_requirement}</td>
                                <td>${item.row_status}</td>
                                <td>${item.row_observation}</td>
                            </tr>
                        </c:forEach>
                    </tbody>
                </table>

                <h5>Reportes</h5>
                <hr>
                <div class="btn-group" role="group" aria-label="Basic example">
                    <button type="button" class="btn btn-outline-primary show-requirement-document-client-selector-modal-btn"><i class="fa fa-file-pdf-o"></i> Hoja de requisitos</button>
                    <c:if test="${accepted}"><button type="button" class="btn btn-outline-primary show-request-reception-client-selection-modal"><i class="fa fa-file-pdf-o"></i> Acta de recepci�n</button></c:if>
                </div>
            </div>
        </div>
    </div>
</main>

<!-- Requirement control document report client selector modal-->
<div class="modal fade" id="requirement-document-pdf-options-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PDF Opciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-12">
                        <label for="approved-by-select">Nombre del inscrito</label>
                        <select id="approved-by-select" class="custom-select"></select>
                    </div>
                </div>
                <div class="form-group">
                    <div class="col-12">
                        <label for="signed-by-select">Nombre del firmante</label>
                        <select id="signed-by-select" class="custom-select"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary generate-requirement-document-pdf-report-btn">Generar PDF
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Request reception report client selector modal-->
<div class="modal fade" id="request-reception-client-selection-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PDF Opciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="col-12">
                        <label for="client-select">Nombre del firmante</label>
                        <select id="client-select" class="custom-select"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary generate-request-reception-pdf-report-btn">Generar PDF
                </button>
            </div>
        </div>
    </div>
</div>

