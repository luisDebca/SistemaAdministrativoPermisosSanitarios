<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/requirement-control/index"/>'>Inicio</a></li>
                    <li class="breadcrumb-item active">Nueva hoja de requisitos</li>
                </ol>
            </nav>

            <%--@elvariable id="document" type="edu.ues.aps.process.requirement.base.model.RequirementControlDocument"--%>
            <form:form id="main" class="validate_form input" modelAttribute="document" method="POST">
                <c:choose>
                    <c:when test="${empty document.requirementRows }"><div class="alert alert-danger" role="alert">
                        <strong>Error</strong> Este documento no contiene requerimientos. <a href="<c:url value="/requirement/allx"/>" class="alert-link">Agregar requerimientos</a></div>
                    </c:when>

                    <c:otherwise>
                        <div class="card">
                            <div class="card-body">
                                <h5 class="card-title">${document.requirementRows.size() eq 0? 'La hoja de requisitos seleccionada no es valida': document.requirementRows.get(0).template.template.title}</h5>
                                <p class="text-muted">${document.requirementRows.size() eq 0? ' ': document.requirementRows.get(0).template.template.subtitle}</p>
                                <table class="table table-hover" id="table">
                                    <thead>
                                    <tr>
                                        <th>N�</th>
                                        <th>Requisito</th>
                                        <th>Estado</th>
                                        <th>Observaci�n</th>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <form:hidden path="id" />
                                    <c:forEach var="row" items="${document.requirementRows}" varStatus="loop">
                                        <form:hidden path="requirementRows[${loop.index}].template.requirement.id" />
                                        <form:hidden path="requirementRows[${loop.index}].template.template.id" />
                                        <form:hidden path="requirementRows[${loop.index}].template.template.subtitle"/>
                                        <form:hidden path="requirementRows[${loop.index}].template.template.title"/>
                                        <form:hidden path="requirementRows[${loop.index}].template.template.type"/>
                                        <form:hidden path="requirementRows[${loop.index}].template.template.version"/>
                                        <form:hidden path="requirementRows[${loop.index}].template.requirement.requirement"/>
                                        <tr>
                                            <th scope="row" class="text-center">${loop.index + 1 }</th>
                                            <td class="text-left">${row.template.requirement.requirement}</td>
                                            <td class="text-center" width="10%">
                                                <div  class="form-group">
                                                    <form:select path="requirementRows[${loop.index}].status" class="custom-select">
                                                        <form:option value="APPROVED" selected="selected">SI</form:option>
                                                        <form:option value="FAULT">NO</form:option>
                                                        <form:option value="NOT_APPLY">N/A</form:option>
                                                    </form:select>
                                                </div>
                                            </td>
                                            <td width="30%">
                                                <div class="form-group">
                                                    <form:textarea
                                                            class="form-control"
                                                            maxlength="249"
                                                            rows="1"
                                                            path="requirementRows[${loop.index}].observation"/>
                                                </div>
                                            </td>
                                        </tr>

                                    </c:forEach>
                                    </tbody>
                                </table>
                                <hr class="my-4">
                                <div class="footer-options">
                                    <input type="submit" class="btn btn-primary" value="Guardar" />
                                    <a role="button" class="btn btn-secondary" href='<c:url value = "/requirement-control/index"/>'>Cancelar</a>
                                </div>

                            </div>
                        </div>
                    </c:otherwise>
                </c:choose>
            </form:form>
        </div>
    </div>
</main>