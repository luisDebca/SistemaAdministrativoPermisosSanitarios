<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/requirement-control/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item">Hojas de control de requisitos completas de veh�culos</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover vehicle-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>N� Solicitudes</th>
                        <th>Propietario</th>
                        <th>Compa�ia de distribuci�n</th>
                        <th>Destino de distribuci�n</th>
                        <th>Lugar de mercancia</th>
                        <th>Direcci�n de estancia</th>
                        <th>Revisi�n de requerimientos</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.requirement.vehicle.dto.AbstractControlDocumentProcessDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td hidden="hidden" class="id_owner">${item.id_owner}</td>
                            <td hidden="hidden" class="id_bunch">${item.id_bunch}</td>
                            <td class="cell-center-middle">${item.request_folder}</td>
                            <td class="cell-center-middle"><h4>${item.request_count}</h4></td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.bunch_distribution_company}</td>
                            <td>${item.bunch_distribution_destination}</td>
                            <td>${item.bunch_marketing_place}</td>
                            <td>${item.bunch_address}</td>
                            <td>${item.document_create_on}</td>
                            <td class="table-options cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="grouInf">
                                        <a href="#"
                                           class="btn btn-outline-secondary disabled_href show_brief_bunch_information"
                                           data-toggle="tooltip" title="Ver solicitudes contenidas"><i class="fa fa-th"
                                                                                                       aria-hidden="true"></i></a>
                                        <button type="button"
                                                class="btn btn-outline-secondary show-requirement-document-client-selector-modal-btn"
                                                data-toggle="tooltip" title="Generar hoja de requerimientos"><i
                                                class="fa fa-file-pdf-o"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary show-request-reception-client-selection-modal"
                                                data-toggle="tooltip" title="Generar acta de recepci�n"><i
                                                class="fa fa-file-pdf-o"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary requirement-control-request-info-modal-btn"
                                                data-toggle="tooltip" title="Ver detalles de solicitud"><i
                                                class="fa fa-folder-open-o"></i></button>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_next_process_btn"
                                                data-toggle="tooltip" title="Pasar a revisi�n de la solicitud"><i
                                                class="fa fa-arrow-circle-right"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                    aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<div hidden="hidden" id="bunch-options"></div>

<!-- Requirement control document report client selector modal-->
<div class="modal fade" id="requirement-document-pdf-options-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PDF Opciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-8 col-form-label" for="approved-by-select">Nombre del inscrito</label>
                    <div class="col-12">
                        <select id="approved-by-select" class="custom-select"></select>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-8 col-form-label" for="signed-by-select">Nombre del firmante</label>
                    <div class="col-12">
                        <select id="signed-by-select" class="custom-select"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a role="button" class="btn btn-primary add-client-btn" href="javascript:">Agregar representante</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary generate-requirement-document-pdf-report-btn">Generar PDF
                </button>
            </div>
        </div>
    </div>
</div>

<!-- Request reception report client selector modal-->
<div class="modal fade" id="request-reception-client-selection-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PDF Opciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="col-8 col-form-label" for="client-select">Nombre del firmante</label>
                    <div class="col-12">
                        <select id="client-select" class="custom-select"></select>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a role="button" class="btn btn-primary add-client-btn" href="javascript:">Agregar representante</a>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button type="button" class="btn btn-primary generate-request-reception-pdf-report-btn">Generar PDF
                </button>
            </div>
        </div>
    </div>
</div>
