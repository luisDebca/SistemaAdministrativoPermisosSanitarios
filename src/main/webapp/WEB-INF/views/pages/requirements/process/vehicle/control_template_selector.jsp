<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid justify-content-center">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/requirement-control/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item active">Hojas de requisitos para revisi�n</li>
                </ol>
            </nav>


            <div class="row content justify-content-md-center">
                <%--@elvariable id="templates" type="java.util.List<edu.ues.aps.process.requirement.base.dto.DocumentTemplateDTO>"--%>
                <c:if test="${empty templates}">
                    <div class="alert alert-warning" role="alert">
                        <strong>Atenci�n!</strong> No se encontro ninguna hoja de revisiones
                        <button type="button" class="btn btn-warning">Reportar Error</button>
                    </div>
                </c:if>

                <c:forEach var="item" items="${templates}">
                    <a class="col-md-3 div-content template-type-btn"
                       href='<%--@elvariable id="id_bunch" type="java.lang.Long"--%>
                       <c:url value="/requirement-control/template/${item.id_template}/bunch/${id_bunch}/new"/>'>
                        <i class="fa fa-truck fa-2x" aria-hidden="true"></i>
                        <div class="content-title">
                            <c:if test="${fn:length(item.title) > 100}">${fn:substring(item.title,0,150)}...</c:if>
                            <c:if test="${not (fn:length(item.title) > 100)}">${item.title}</c:if>
                            <p class="id_template" hidden="hidden">${item.id_template}</p>
                            <p>${item.subtitle}</p>
                            <p>versi�n ${item.version}</p></div>
                    </a>
                </c:forEach>
                    <a href="<c:url value="/requirement-control/index"/>" role="button" class="btn btn-secondary btn-lg btn-block mt-3">Volver a inicio</a>
            </div>

        </div>
    </div>
</main>