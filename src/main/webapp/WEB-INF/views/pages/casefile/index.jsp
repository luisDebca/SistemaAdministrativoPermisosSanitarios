<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row">
                <div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary">
                    <h3><i class="fa fa-folder-open-o" aria-hidden="true"></i> Expedientes</h3></div>
            </div>

            <div class="alert alert-primary" role="alert">
                <i class="fa fa-info-circle" aria-hidden="true"></i> Si la busqueda no tiene resultados puede ver <a href="<c:url value="/caseFile/list"/>">"Todos los expedientes"</a>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-folder-open-o"></i> Buscar expedientes</h5>
                    <p class="card-text">Buscar expedientes por n�mero de folder o c�digo de expediente</p>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-search" aria-hidden="true"></i></span>
                        </div>
                        <input id="establishment-finder-input" type="text" class="form-control" placeholder="Buscar..." aria-label="search" aria-describedby="basic-addon1">
                    </div>
                    <div id="search_result" class="md-2 p-2"></div>
                    <a role="button" href="<c:url value="/caseFile/list"/>" class="btn btn-outline-primary"><i class="fa fa-list"></i> Ver todos</a>
                    <div class="row mt-4">
                        <div class="col-6">
                            <div class="card bg-light">
                                <div class="card-body">
                                    <div class="list-group">
                                        <a href="javascript:" class="list-group-item list-group-item-action active">
                                             <i class="fa fa-building-o"></i> Establecimientos
                                        </a>
                                        <a class="list-group-item list-group-item-action" href="javascript:"><i class="fa fa-cogs"></i> Solicitudes en proceso</a>
                                        <a class="list-group-item list-group-item-action" href="javascript:"><i class="fa fa-calendar-check-o"></i> Permisos Activos</a>
                                        <a class="list-group-item list-group-item-action" href="javascript:"><i class="fa fa-calendar-times-o"></i> Permisos Vencidos</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-6">
                            <div class="card bg-light">
                                <div class="card-body">
                                    <div class="list-group">
                                        <a href="javascript:" class="list-group-item list-group-item-action active">
                                            <i class="fa fa-truck"></i> Veh�culos
                                        </a>
                                        <a class="list-group-item list-group-item-action" href="javascript:"><i class="fa fa-cogs"></i> Solicitudes en proceso</a>
                                        <a class="list-group-item list-group-item-action" href="javascript:"><i class="fa fa-calendar-check-o"></i> Permisos Activos</a>
                                        <a class="list-group-item list-group-item-action" href="javascript:"><i class="fa fa-calendar-times-o"></i> Permisos Vencidos</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>