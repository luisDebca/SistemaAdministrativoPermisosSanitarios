<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/caseFile/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Expedientes</li>
                </ol>
            </nav>

            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th>N�mero de folder</th>
                        <th>C�digo de expediente</th>
                        <th>Fecha de creaci�n</th>
                        <th>Secci�n</th>
                        <th>Proceso actual</th>
                        <th>Estado</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td hidden="hidden" class="casefile_id">${item.id_casefile}</td>
                            <td>${item.request_folder}</td>
                            <td>${empty item.request_identifier ? 'No definido': item.request_identifier}</td>
                            <td>${item.creation_date}</td>
                            <td>${item.casefile_section}</td>
                            <td>${item.current_process}</td>
                            <td>${item.status}</td>
                            <td class="cell-center-middle">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a role="button" href="<c:url value="/caseFile/${item.id_casefile}/info"/>" class="btn btn-outline-secondary show-condition-info-modal" data-toggle="tooltip" data-placement="top" title="Ver detalles"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                                    <a href="<c:url value="/caseFile/${item.id_casefile}/edit"/>" role="button" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

            </div>
    </div>
</main>