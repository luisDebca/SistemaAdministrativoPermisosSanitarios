<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/caseFile/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Informaci�n de expediente</li>
                </ol>
            </nav>
            <%--@elvariable id="info" type="edu.ues.aps.process.area.base.dto.AbstractInfo"--%>

            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-primary rounded box-shadow">
                <div class="lh-100">
                    <h4 class="mb-0 text-white lh-100">Folder <strong>${info.folder}</strong></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h5 class="border-bottom border-gray pb-2 mb-0">Informaci�n completa para solicitud de ${info.section}</h5>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">${info.section}</strong>
                                ${info.name}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Propietario</strong>
                                ${info.owner}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">N�mero de solicitud</strong>
                                ${info.request}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Solicitud</strong>
                                ${info.type}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Fecha de creaci�n</strong>
                                ${info.created}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Fecha de inicio de permiso</strong>
                                ${info.start}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Fecha de expiraci�n del permiso</strong>
                                ${info.end}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Duraci�n del permiso</strong>
                                ${info.duration} a�os
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">UCSF</strong>
                                ${info.ucsf}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">SIBASI</strong>
                                ${info.sibasi}
                            </p>
                        </div>

                        <a role="button" class="btn btn-outline-primary btn-lg btn-block" href="<c:url value="/caseFile/${info.id}/edit"/>"><i class="fa fa-edit"></i> Editar</a>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h5 class="border-bottom border-gray pb-2 mb-0">Estado de solicitud <span class="badge badge-secondary">${info.status}</span></h5>
                        <p class="text-muted">${info.process}</p>
                        <c:if test="${info.processId < 140}">
                            <div class="progress" style="height: 40px;">
                                <div class="progress-bar bg-info progress-bar-striped" role="progressbar" style="width: ${(10*info.processId)/14}%;" aria-valuenow="${info.processId}" aria-valuemin="1" aria-valuemax="140"><strong>${info.processProgress} %</strong></div>
                            </div>
                        </c:if>
                        <c:if test="${info.processId == 140}">
                            <div class="progress" style="height: 40px;">
                                <div class="progress-bar bg-success progress-bar-striped" role="progressbar" style="width: ${info.certificationProgress}%;" aria-valuenow="${info.certificationProgress}" aria-valuemin="0.0" aria-valuemax="100.0"><strong>${info.certificationProgress} %</strong></div>
                            </div>
                            <p class="my-2"><i class="fa fa-clock-o"></i> ${info.duration * 365 - info.certificationDays} d�as para finalizar permiso.</p>
                        </c:if>
                        <c:if test="${info.processId == 150}">
                            <div class="progress" style="height: 40px;">
                                <div class="progress-bar bg-danger progress-bar-striped" role="progressbar" style="width: ${(10*info.processId)/14}%;" aria-valuenow="${info.duration*365}" aria-valuemin="1" aria-valuemax="${info.duration*365}"><strong>Permiso vencido</strong></div>
                            </div>
                        </c:if>
                        <hr class="my-4">
                        <div class="list-group">
                            <a role="button" class="list-group-item list-group-item-action active" href="<c:url value="/${info.sectionType}/${info.target}/info"/>">Ver ${info.section}</a>
                            <c:if test="${info.processId > 10}"><a href="<c:url value="/requirement-control/request/${info.id}/summary"/>" class="list-group-item list-group-item-action">Revisi�n de requerimientos</a></c:if>
                            <c:if test="${info.processId > 20}"><a href="javascript:" class="list-group-item list-group-item-action">Revisi�n de documentaci�n</a></c:if>
                            <c:if test="${info.processId > 30}"><a href="javascript:" class="list-group-item list-group-item-action">Dictamen Jur�dico</a></c:if>
                            <c:if test="${info.processId > 40}"><a href="javascript:" class="list-group-item list-group-item-action">Resoluci�n Jur�dica</a></c:if>
                            <c:if test="${info.processId > 50}"><a href="javascript:" class="list-group-item list-group-item-action">Mandamiento de pago</a></c:if>
                            <c:if test="${info.processId > 70}"><a href="javascript:" class="list-group-item list-group-item-action">Ingreso de solicitud</a></c:if>
                            <c:if test="${info.processId > 80}"><a href="javascript:" class="list-group-item list-group-item-action">Seguimiento</a></c:if>
                            <c:if test="${info.processId > 90}"><a href="javascript:" class="list-group-item list-group-item-action">Resoluci�n y diploma</a></c:if>
                            <c:if test="${info.processId > 110}"><a href="javascript:" class="list-group-item list-group-item-action">Opini�n Jur�dica</a></c:if>
                        </div>
                    </div>
                    <button type="button" class="btn btn-outline-secondary btn-lg btn-block goback"><i class="fa fa-reply"></i> Atras</button>
                </div>
            </div>
        </div>
    </div>
</main>