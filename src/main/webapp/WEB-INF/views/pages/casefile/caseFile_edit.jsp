<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%--@elvariable id="casefile" type="edu.ues.aps.process.base.model.Casefile"--%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/caseFile/list"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/caseFile/${casefile.id}/info"/>">Informaci�n</a></li>
                    <li class="breadcrumb-item active">Editar expediente</li>
                </ol>
            </nav>

            <div class="information-panel">
                <c:if test="${casefile.id == 0}">
                    <div class="alert alert-danger" role="alert">
                        No se encontro el expediente solicitado.
                    </div>
                </c:if>
            </div>

            <div class="card">
                <div class="card-body">
                    <form:form id="main" method="post" modelAttribute="casefile">
                        <form:hidden path="id"/>
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title">Expediente n�mero de
                                    folder ${casefile.request_folder}-${casefile.request_year}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">Modificaci�n de expediente</h6>
                                <hr class="my-4">
                                <form:hidden path="id"/>
                                <form:hidden path="creation_date"/>
                                <form:hidden path="certification_start_on"/>
                                <form:hidden path="status"/>
                                <form:hidden path="section"/>
                                <form:hidden path="validate"/>
                                <form:hidden path="ucsf_delivery"/>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">N�mero de folder</label>
                                    <div class="col-sm-2">
                                        <form:input
                                                type="text"
                                                class="form-control"
                                                path="request_folder"/>
                                    </div>
                                    <div class="required-type" data-toggle="tooltip" data-placement="top"
                                         title="Este campo es obligatorio">*
                                    </div>
                                    <small id="folder-help" class="form-text text-muted">Este valor es auto generado,
                                        cambios en estos pueden provocar errores en los procesos.
                                    </small>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">C�digo de solicitud</label>
                                    <div class="col-sm-2">
                                        <form:input
                                                type="text"
                                                class="form-control"
                                                path="request_code"/>
                                    </div>
                                    <div class="required-type" data-toggle="tooltip" data-placement="top"
                                         title="Este campo es obligatorio">*
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">N�mero de solicitud</label>
                                    <div class="col-sm-2">
                                        <form:input
                                                type="text"
                                                class="form-control"
                                                path="request_number"/>
                                    </div>
                                    <div class="required-type" data-toggle="tooltip" data-placement="top"
                                         title="Este campo es obligatorio">*
                                    </div>
                                    <small id="number-help" class="form-text text-muted">Este valor es auto generado,
                                        cambios en estos pueden provocar errores en los procesos.
                                    </small>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">A�o de solicitud</label>
                                    <div class="col-sm-2">
                                        <form:input
                                                type="number"
                                                class="form-control"
                                                path="request_year"
                                                min="1" max="99"/>
                                    </div>
                                    <div class="required-type" data-toggle="tooltip" data-placement="top"
                                         title="Este campo es obligatorio">*
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Tipo de solicitud</label>
                                    <div class="col-sm-4">
                                        <form:select class="custom-select" path="certification_type">
                                            <form:option value="FIRST">Primera vez</form:option>
                                            <form:option value="RENEWAL">Renovaci�n</form:option>
                                        </form:select>
                                    </div>
                                    <div class="required-type" data-toggle="tooltip" data-placement="top"
                                         title="Este campo es obligatorio">*
                                    </div>
                                </div>

                                    <%--@elvariable id="sibasi_list" type="java.util.List<edu.ues.aps.process.base.dto.AbstractMapDTO>"--%>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">SIBASI</label>
                                    <div class="col-sm-4">
                                        <form:select
                                                class="custom-select sibasi-select-list"
                                                path="ucsf.sibasi.id"
                                                items="${sibasi_list}"
                                                itemLabel="value"
                                                itemValue="id"/>
                                    </div>
                                    <div class="required-type" data-toggle="tooltip" data-placement="top"
                                         title="Este campo es obligatorio">*
                                    </div>
                                </div>

                                    <%--@elvariable id="ucsf_list" type="java.util.List<edu.ues.aps.process.base.dto.AbstractMapDTO>"--%>
                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">UCSF</label>
                                    <div class="col-sm-4">
                                        <form:select
                                                class="custom-select ucsf-select-list"
                                                path="ucsf.id"
                                                items="${ucsf_list}"
                                                itemLabel="value"
                                                itemValue="id"/>
                                    </div>
                                    <div class="required-type" data-toggle="tooltip" data-placement="top"
                                         title="Este campo es obligatorio">*
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-sm-2 col-form-label">Duraci�n del permiso (a�os)</label>
                                    <div class="col-sm-3">
                                        <form:input
                                                type="number"
                                                class="col-3 form-control"
                                                path="certification_duration_in_years"
                                                min="1"
                                                max="5"/>
                                    </div>
                                    <div class="required-type" data-toggle="tooltip" data-placement="top"
                                         title="Este campo es obligatorio">*
                                    </div>
                                </div>

                                <div class="col-auto my-1">
                                    <div class="form-check">
                                        <form:checkbox
                                                class="form-check-input"
                                                path="apply_payment"/>
                                        <label class="form-check-label">Aplica para mandamiento de pago</label>
                                    </div>
                                </div>
                                <hr class="my-4">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <a href="<c:url value="/caseFile/${casefile.id}/info"/> " role="button"
                                   class="btn btn-secondary">Cancelar</a>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>