<%--@elvariable id="vehicles" type="java.util.List<edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO>"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/vehicle/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Completar datos de la solicitud</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <%--@elvariable id="bunch" type="edu.ues.aps.process.area.vehicle.dto.VehicleBunchFormDTO"--%>
            <div class="card">
                <div class="card-body">

                    <h5 class="card-title">Nuevo proceso de certificaci�n para veh�culos</h5>
                    <h6 class="card-subtitle mb-4 text-muted">Complete los datos requeridos del grupo de veh�culos a solicitar</h6>
                    <hr class="my-4">
                    <form:form id="main" method="post" modelAttribute="bunch">

                        <div class="form-group row">
                            <label for="number" class="col-sm-3 col-form-label">N�mero de veh�culos</label>
                            <div class="col-sm-2">
                                <form:input id="number" type="number" class="form-control" min="1" max="10000" path="vehicle_count" value="${vehicles.size()}"/>
                                <small id="number-help-block" class="form-text text-muted">N�mero de veh�culos incluidos en la solicitud.</small>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="distribution-name" class="col-sm-3 col-form-label">Los alimentos son distribuidos por la empresa</label>
                            <div class="col-sm-8">
                                <form:input id="distribution-name" class="form-control" path="distribution_company"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="distribution-address" class="col-sm-3 col-form-label">Direcci�n de empresa distribuidora</label>
                            <div class="col-sm-8">
                                <form:input id="distribution-address" class="form-control" path="distribution_destination"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="place" class="col-sm-3 col-form-label">Lugares de comercializaci�n o destino final</label>
                            <div class="col-sm-8">
                                <form:input id="place" class="form-control" path="marketing_place"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="client-name" class="col-sm-3 col-form-label">Nombre de la persona que presenciara la inspecci�n</label>
                            <div class="col-sm-8">
                                <form:input id="client-name" class="form-control" path="client_presented_name"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="client-tel" class="col-sm-3 col-form-label">Tel�fono</label>
                            <div class="col-sm-3">
                                <form:input id="client-tel" class="form-control" path="client_presented_telephone"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="address" class="col-sm-3 col-form-label">Direcci�n de permanencia</label>
                            <div class="col-sm-8">
                                <form:input type="text" class="form-control" id="address" path="address.details"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3 col-form-label">Departamento</label>
                            <div class="col-4">
                                    <%--@elvariable id="departments" type="java.util.List<edu.ues.aps.process.base.dto.MapDTO>"--%>
                                <form:select
                                        class="custom-select department-select-list"
                                        path="address.municipality.department.id"
                                        items="${departments}"
                                        itemLabel="value"
                                        itemValue="id"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-3 col-form-label">Municipio</label>
                            <div class="col-4">
                                    <%--@elvariable id="municipalities" type="java.util.List<edu.ues.aps.process.base.dto.MapDTO>"--%>
                                <form:select
                                        class="custom-select municipality-select-list initialized-municipality-select-list"
                                        path="address.municipality.id"
                                        items="${municipalities}"
                                        itemValue="id"
                                        itemLabel="value"/>
                            </div>
                        </div>

                        <hr class="my-4">
                        <h5 class="card-title">Selecci�n de veh�culos</h5>
                        <h6 class="card-subtitle mb-4 text-muted">Seleccione los veh�culos que se incluyen en este proceso, si no se selecciona los veh�culos se crearan nuevos.</h6>

                        <div class="row">
                            <div class="col-6">
                                <c:if test="${empty vehicles}">
                                    <div class="alert alert-warning" role="alert">
                                        No se tiene registro de veh�culos disponibles para este proceso.
                                    </div>
                                </c:if>
                                <c:forEach var="item" items="${vehicles}" varStatus="loop">
                                    <c:if test="${item.active}">
                                        <div class="custom-control custom-checkbox">
                                            <form:checkbox class="custom-control-input" id="vehicle-${loop.index}" path="selected_vehicles" value="${item.id_vehicle}"/>
                                            <label class="custom-control-label h5" for="vehicle-${loop.index}"><span class="badge badge-primary">${item.vehicle_license}</span></label>
                                        </div>
                                    </c:if>
                                </c:forEach>
                                <button id="all" type="button" class="btn btn-secondary mt-3">Incluir todos</button>
                                        <button id="none" type="button" class="btn btn-secondary mt-3">Remover todos</button>
                            </div>
                            <div class="col-6">
                                <div class="card">
                                    <div class="card-body text-center">
                                        <p>N�mero de veh�culos nuevos</p>
                                        <div id="counter" class="h1">0</div>
                                        <p class="text-muted small">Este es el n�mero de veh�culos nuevos a crear para la solicitud.</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <hr class="my-4">

                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a role="button" class="btn btn-secondary" href="<c:url value="/vehicle/index"/>">Cancelar</a>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>