<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%--@elvariable id="caseFile" type="edu.ues.aps.process.base.model.Casefile"--%>
<%--@elvariable id="id_owner" type="java.lang.Long"--%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/vehicle/index"/>">Inicio</a></li>
                    <c:if test="${not empty id_owner}">
                        <li class="breadcrumb-item"><a href="<c:url value="/owner/${id_owner}"/>">Propietario</a></li>
                        <li class="breadcrumb-item"><a href="<c:url value="/owner/${id_owner}/vehicle/casefile/list"/>">Expedientes
                            veh�culos</a></li>
                    </c:if>
                    <c:if test="${empty id_owner}">
                        <li class="breadcrumb-item"><a href="<c:url value="/vehicle/list"/>">Veh�culos</a></li>
                    </c:if>
                    <li class="breadcrumb-item active">Editar expediente</li>
                </ol>
            </nav>

            <div class="information-panel">
                <c:if test="${caseFile.id == 0}">
                    <div class="alert alert-danger" role="alert">
                        No se encontro el expediente solicitado.
                    </div>
                </c:if>
            </div>

            <div class="card">
                <div class="card-body">
                    <form:form id="main" method="post"
                               modelAttribute="caseFile"><%--@elvariable id="request_count" type="java.lang.Long"--%>
                        <form:hidden path="id"/>
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title">Expediente n�mero de
                                    folder ${caseFile.request_folder}-${caseFile.request_year}</h5>
                                <h6 class="card-subtitle mb-2 text-muted">Modificaci�n de expediente</h6>
                                <p class="small text-muted">Estos cambios se aplicar�n a ${request_count}
                                    expedientes.</p>
                                <p>Veh�culos con placa:
                                        <%--@elvariable id="licenses" type="java.util.List<java.lang.String>"--%>
                                    <c:forEach var="item" items="${licenses}"> <span
                                            class="badge badge-primary">${item}</span> </c:forEach>
                                </p>
                                <hr class="my-4">
                                <form:hidden path="creation_date"/>
                                <form:hidden path="certification_start_on"/>
                                <form:hidden path="status"/>
                                <form:hidden path="section"/>
                                <form:hidden path="request_folder"/>
                                <form:hidden path="request_number"/>
                                <div class="form-group col-md-4">
                                    <label>C�digo de solicitud</label>
                                    <form:input
                                            type="text"
                                            class="col-3 form-control"
                                            path="request_code"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>A�o de solicitud</label>
                                    <form:input
                                            type="number"
                                            class="col-3 form-control"
                                            path="request_year"
                                            min="1" max="99"/>
                                </div>
                                <div class="form-group col-md-4">
                                    <label>Tipo de solicitud</label>
                                    <br>
                                    <form:select class="custom-select" path="certification_type">
                                        <form:option value="FIRST">Primera vez</form:option>
                                        <form:option value="RENEWAL">Renovaci�n</form:option>
                                    </form:select>
                                </div>

                                    <%--@elvariable id="sibasi_list" type="java.util.List<edu.ues.aps.process.base.dto.AbstractMapDTO>"--%>
                                <div class="form-group col-md-4">
                                    <label>SIBASI</label>
                                    <br>
                                    <form:select class="custom-select sibasi-select-list" path="ucsf.sibasi.id"
                                                 items="${sibasi_list}" itemLabel="value" itemValue="id"/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label>UCSF</label>
                                    <br>
                                        <%--@elvariable id="ucsf_list" type="java.util.List<edu.ues.aps.process.base.dto.AbstractMapDTO>"--%>
                                    <form:select class="custom-select ucsf-select-list initialized-ucsf-select-list"
                                                 path="ucsf.id"
                                                 items="${ucsf_list}" itemLabel="value" itemValue="id"/>
                                </div>

                                <div class="form-group col-md-4">
                                    <label>Duraci�n del permiso (a�os)</label>
                                    <form:input
                                            type="number"
                                            class="col-3 form-control"
                                            path="certification_duration_in_years"
                                            min="1"
                                            max="5"
                                    />
                                </div>

                                <div class="col-auto my-1">
                                    <div class="form-check">
                                        <form:checkbox
                                                class="form-check-input"
                                                path="apply_payment"/>
                                        <label class="form-check-label">Aplica para mandamiento de pago</label>
                                        <div class="messages invalid-feedback"></div>
                                    </div>
                                </div>
                                <hr class="my-4">
                                <button type="submit" class="btn btn-primary">Guardar</button>
                                <c:if test="${empty id_owner}">
                                    <a href="<c:url value="/vehicle/list"/> " role="button"
                                       class="btn btn-secondary">Cancelar</a>
                                </c:if>
                                <c:if test="${not empty id_owner}">
                                    <a href="<c:url value="/owner/${id_owner}/vehicle/casefile/list"/> " role="button"
                                       class="btn btn-secondary">Cancelar</a>
                                </c:if>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>