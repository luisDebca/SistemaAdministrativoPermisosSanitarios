<%--@elvariable id="id_vehicle" type="java.lang.Long"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/vehicle/index"/>'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/vehicle/${id_vehicle}/info"/>'>Veh�culo</a></li>
                    <li class="breadcrumb-item">Registro de expedientes</li>
                </ol>
            </nav>

            <c:set var="count" value="0"/>
            <c:forEach items="${caseFiles}" var="item">
                <c:if test="${item.status == 'Invalido' and count == '0'}">
                    <c:set var="count" value="1"/>
                </c:if>
            </c:forEach>

            <div class="card mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h5 class="card-title">Expedientes del veh�culo</h5>
                            <div hidden="hidden" id="id_vehicle">${id_vehicle}</div>
                            <h6><a href="<c:url value="/vehicle/${id_vehicle}/info"/>" id="vehicle_license"></a></h6>
                            <div id="extra-info" class="small text-muted">
                                <a id="vehicle_owner" href="javascript:"></a>
                                <p id="vehicle_type" class="my-0"></p>
                                <p id="vehicle_trasport" class="my-0"></p>
                            </div>
                        </div>
                        <div class="col-6 text-right">
                            <div class="btn-group-vertical">
                                <div class="btn-group" role="group" aria-label="options">
                                    <a id="show-new-request-confirmation-modal" href="javascript:" role="button" class="btn btn-outline-primary"><i class="fa fa-plus" aria-hidden="true"></i> Nueva solicitud</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover vehicle-table" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>Fecha de ingreso</th>
                        <th>Tipo de solicitud</th>
                        <th>Estado de solicitud</th>
                        <th>Fecha de inicio del permiso</th>
                        <th>Estado de permiso</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="caseFiles" type="java.util.List<edu.ues.aps.process.area.vehicle.dto.AbstractVehicleCaseFileDTO>"--%>
                    <c:forEach var="item" items="${caseFiles}">
                        <tr>
                            <td hidden="hidden" class="id_owner">${item.id_owner}</td>
                            <td hidden="hidden" class="id_bunch">${item.id_bunch}</td>
                            <td hidden="hidden" class="id_casefile">${item.id_caseFile}</td>
                            <td hidden="hidden" class="id_vehicle">${item.id_vehicle}</td>
                            <td><a href="<c:url value="/vehicle/bunch/${item.id_bunch}/info"/>">${item.folder_number}</a></td>
                            <td>${item.creation_date}</td>
                            <td>${item.certification_type}</td>
                            <td class="cell-center-middle">
                                <c:if test="${item.status == 'En proceso'}">
                                    <span class="badge badge-pill badge-secondary">En proceso</span>
                                </c:if>
                                <c:if test="${item.status == 'Valido'}">
                                    <span class="badge badge-pill badge-success">Valido</span>
                                </c:if>
                                <c:if test="${item.status == 'Expirado'}">
                                    <span class="badge badge-pill badge-danger">Expirado</span>
                                </c:if>
                                <c:if test="${item.status == 'Invalido'}">
                                    <span class="badge badge-pill badge-danger">Invalido</span>
                                </c:if>
                            </td>
                            <td class="cell-center-middle">${item.certification_start_on}</td>
                            <td>${item.current_process}</td>
                            <td class="cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group">
                                        <a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>" class="btn btn-outline-secondary" data-toggle="tooltip" title="Ver detalles"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                                        <a href="#" class="btn btn-outline-secondary disabled_href show_brief_bunch_information" data-toggle="tooltip" title="Ver solicitudes contenidas"><i class="fa fa-th" aria-hidden="true"></i></a>
                                        <a href="<c:url value="/requirement-control/vehicle/bunch/${item.id_bunch}/template/selector"/>" class="btn btn-outline-secondary" data-toggle="tooltip" title="Iniciar proceso de revisi�n de requerimientos"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
                                        <button type="button" class="btn btn-outline-secondary show-cancelling-modal" data-toggle="tooltip" title="Detener proceso"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <a href="<c:url value="/establishment/casefile/${item.id_caseFile}"/>" class="btn btn-outline-secondary" data-toggle="tooltip" title="Ver registro de procesos"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<!-- New process -->
<div class="modal fade" id="new-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="new-title">Nueva solicitud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-question-circle-o fa-2x"></i>
                <h5>�Continuar con nueva solicitud?</h5>
                <p class="small text-muted">Seleccione la opci�n para continuar con la creaci�n de la nueva solicitud.</p>
                <div class="btn-group-vertical">
                    <a href="javascript:" role="button" class="btn btn-outline-primary">Solo este veh�culo</a>
                    <a href="javascript:" role="button" class="btn btn-outline-primary">Agregar m�s veh�culos</a>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<!-- Cancel process -->
<div class="modal fade" id="cancel-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cancel-title">Cancelar solicitud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-warning fa-2x"></i>
                <h5>�Cancelar solicitud?</h5>
                <p class="small text-muted">Una ves cancelado el proceso no se podra continuar con la certificaci�n del permiso.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger">Cancelar solicitud</button>
            </div>
        </div>
    </div>
</div>
