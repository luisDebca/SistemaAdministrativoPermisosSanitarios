<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Completar datos de la solicitud</li>
                </ol>
            </nav>

            <%--@elvariable id="casefile" type="edu.ues.aps.process.area.establishment.model.CasefileEstablishment"--%>
            <div class="card">
                <div class="card-body">
                    <form:form id="main" method="post" modelAttribute="casefile">
                        <form:hidden path="id"/>
                        <div class="row">
                            <div class="col">
                                <h5 class="card-title">Expediente de establecimiento</h5>
                                <h6 class="card-subtitle mb-2 text-muted">Ingrese los datos iniciales para el expediente</h6>
                                <div class="form-group col-md-4">
                                    <label >C�digo de solicitud</label>
                                    <form:input
                                            type="text"
                                            class="col-2 form-control"
                                            path="request_code"/>
                                    <small class="form-text text-muted">El c�digo especificado depende del tipo de establecimiento inscrito por el usuario.</small>
                                    <div class="messages invalid-feedback"></div>
                                </div>
                                <div class="form-group col-md-4">
                                    <label >Tipo de solicitud</label>
                                    <br>
                                    <form:select class="custom-select" path="certification_type">
                                        <form:option value="FIRST">Primera vez</form:option>
                                        <form:option value="RENEWAL">Renovaci�n</form:option>
                                    </form:select>
                                    <div class="messages invalid-feedback"></div>
                                </div>

                                <div class="form-group col-md-4">
                                    <label >Duraci�n del permiso</label>
                                    <form:input
                                            type="number"
                                            class="col-2 form-control"
                                            path="certification_duration_in_years"
                                            min = "1"
                                            max = "5"
                                    />
                                    <div class="messages invalid-feedback"></div>
                                </div>

                                <div class="col-auto my-1">
                                    <div class="form-check">
                                        <form:checkbox
                                                class="form-check-input"
                                                path="apply_payment"/>
                                        <label class="form-check-label">Aplica para mandamiento de pago</label>
                                        <div class="messages invalid-feedback"></div>
                                    </div>
                                </div>
                                <button type="submit" class="btn btn-primary footer-options">Guardar</button>
                            </div>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>