<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%--@elvariable id="casefile" type="edu.ues.aps.process.area.establishment.dto.CasefileEstablishmentInfoDTO"--%>
<%--@elvariable id="establishment" type="edu.ues.aps.process.area.establishment.dto.EstablishmentDTO"--%>
<%--@elvariable id="owner" type="edu.ues.aps.process.base.dto.OwnerEntityDTO"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/establishment/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/owner/${owner.id_owner}/establishment"/>">Propietario</a></li>
                    <li class="breadcrumb-item to-previous-page"><a href="<c:url value="/establishment/${establishment.establishment_id}/caseFile/all"/>">Expedientes de establecimientos</a></li>
                    <li class="breadcrumb-item active">Informaci�n</li>
                </ol>
            </nav>

            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-secondary rounded box-shadow">
                <div class="lh-100">
                    <h4 class="mb-0 text-white lh-100">Informaci�n del expediente ${casefile.casefile_request_folder}</h4>
                </div>
            </div>

            <c:if test="${casefile.id_casefile == 0}">
                <div class="alert alert-danger" role="alert">
                    <strong>Ning�n Resultado: </strong> El expediente no fue encontrado...
                </div>
            </c:if>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h6 class="border-bottom border-gray pb-2 mb-0">Datos del propietario</h6>
                <div id="id_owner" hidden="hidden">${owner.id_owner}</div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Nombre</strong>
                        ${owner.owner_name}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">NIT</strong>
                        ${owner.owner_nit}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Telefono</strong>
                        ${owner.owner_telephone}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Correo Electronico</strong>
                        ${owner.owner_email}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">FAX</strong>
                        ${owner.owner_fax}
                    </p>
                </div>
                <small class="d-block text-right mt-3">
                    <a href="#">Ir a propietario</a>
                </small>
            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h6 class="border-bottom border-gray pb-2 mb-0">Establecimiento</h6>
                <div id="id_establishment" hidden="hidden">${establishment.establishment_id}</div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Nombre</strong>
                        ${establishment.establishment_name}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Direcci�n</strong>
                        ${establishment.establishment_address}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">NIT</strong>
                        ${establishment.establishment_nit}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Tipo de establecimiento escrito por el usuario</strong>
                        ${establishment.establishment_type_detail}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Rubro</strong>
                        ${establishment.establishment_type}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Telefono</strong>
                        ${establishment.establishment_telephone}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Correo Electronico</strong>
                        ${establishment.establishment_email}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">FAX</strong>
                        ${establishment.establishment_fax}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Horario de operaciones</strong>
                        ${establishment.establishment_operations}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">N�mero de empleados</strong>
                        Masculinos: ${establishment.establishment_male_employee_count} , Femeninos: ${establishment.establishment_female_employee_count}
                    </p>
                </div>
                <small class="d-block text-right mt-3">
                    <a href="<c:url value="/establishment/edit-${establishment.establishment_id}"/>">Ir a establecimiento</a>
                </small>
                <small class="d-block text-right mt-3">
                    <a href="#" class="disabled_href show-clients-information-list-btn show-btn">Mostrar autorizados</a>
                </small><br>
                <div id="establishment-client-info-list"></div>

            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h6 class="border-bottom border-gray pb-2 mb-0">Expediente</h6>
                <div id="id_casefile" hidden="hidden">${casefile.id_casefile}</div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">C�digo de expediente</strong>
                        <c:if test="${empty casefile.casefile_request_code}"><span class="badge badge-pill badge-secondary">No especificado</span></c:if>
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Duraci�n del permiso</strong>
                        ${casefile.casefile_certification_duration_in_years} a�os
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Tipo de permiso</strong>
                        ${casefile.casefile_certification_type}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">SIBASI / UCSF</strong>
                        <c:if test="${empty casefile.sibasi_name}"><span class="badge badge-pill badge-secondary">No especificado</span></c:if><c:if test="${not empty casefile.sibasi_name}">${casefile.sibasi_name}</c:if> / <c:if test="${empty casefile.ucsf_name}"><span class="badge badge-pill badge-secondary">No especificado</span></c:if><c:if test="${not empty casefile.ucsf_name}">${casefile.ucsf_name}</c:if>
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Aplica para mandamiento de pago</strong>
                        <c:if test="${casefile.casefile_apply_payment}">SI</c:if><c:if test="${not casefile.casefile_apply_payment}">NO</c:if>
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Fecha de inicio del permiso:</strong>
                        <c:if test="${empty casefile.casefile_certification_start_on}"><span class="badge badge-pill badge-secondary">El permiso no ha sido completado</span></c:if><c:if test="${not empty casefile.casefile_certification_start_on}">${casefile.casefile_certification_start_on}</c:if>
                    </p>
                </div>
                <small class="d-block text-right mt-3">
                    <a href="<c:url value="/caseFile/${casefile.id_casefile}/edit"/>">Editar expediente</a>
                </small>
                <small class="d-block text-right mt-3">
                    <a href="#" class="disabled_href show-casefile-record-btn show-btn">Mostrar procesos</a>
                </small>
                <div id="casefile-record-block" class="mt-3"></div>
            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h6 class="border-bottom border-gray pb-2 mb-0">Requerimientos</h6>
                <small class="d-block text-right mt-3">
                    <a href="#" class="disabled_href show-requirement-document-information-btn show-btn">Mostrar detalles</a>
                </small>
                <div id="requirement-document-information-collapse">
                    <div id="document_id" hidden="hidden"></div>
                    <div class="media text-muted pt-3">
                        <p id="document-template-title" class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Tipo de solicitud</strong>
                        </p>
                    </div>
                    <div class="media text-muted pt-3">
                        <p id="document-status" class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Estado de la solicitud</strong>
                        </p>
                    </div>
                    <div class="media text-muted pt-3">
                        <p id="document-creation" class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Fecha de creaci�n del documento</strong>
                        </p>
                    </div>
                    <div class="media text-muted pt-3">
                        <p id="document-acceptation" class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                            <strong class="d-block text-gray-dark">Fecha de acceptaci�n</strong>
                        </p>
                    </div>

                    <small class="d-block text-right mt-3">
                        <a href="#" class="disabled_href show-requirement-row-list-btn show-btn">Mostrar requerimientos</a>
                    </small>

                    <table id="requirement-row-list" class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th>N�</th>
                            <th>Requerimiento</th>
                            <th>Estado</th>
                            <th>Observaci�n</th>
                        </tr>
                        </thead>
                        <tbody class="requirement-row-list-body"></tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="process-user-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header"><h5 class="modal-title" id="exampleModalLabel">Registro del proceso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body" id="process-user-model-body">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>