<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<%--@elvariable id="case_files" type="java.util.List<edu.ues.aps.process.area.establishment.dto.AbstractCasefileInformationDTO>"--%>
<%--@elvariable id="id_establishment" type="java.lang.Long"--%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/establishment/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/establishment/list"/>'>Establecimientos</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/establishment/${id_establishment}"/>'>Establecimiento</a></li>
                    <li class="breadcrumb-item">Registro de expedientes</li>
                </ol>
            </nav>

            <div class="card mb-3">
                <div class="card-body">
                   <div class="row">
                       <div class="col-6">
                           <h5 class="card-title">Expedientes del establecimiento</h5>
                           <h6 class="card-text"><a href="<c:url value="/establishment/${id_establishment}"/>" id="establishment_name"></a></h6>
                           <div id="extra-info" class="small text-muted"></div>
                           <div id="id_establishment" hidden="hidden">${id_establishment}</div>
                       </div>
                       <div class="col-6 text-right">
                           <div class="btn-group-vertical">
                               <div class="btn-group" role="group" aria-label="options">
                                   <c:if test="${empty case_files}">
                                       <button type="button" class="btn btn-outline-primary show-first-modal"><i class="fa fa-plus" aria-hidden="true"></i> Nuevo ingreso</button>
                                   </c:if>
                                   <c:if test="${not empty case_files}">
                                       <c:if test="${case_files.get(case_files.size() - 1).casefile_status != 'En proceso'}">
                                           <button type="button" class="btn btn-outline-primary show-renewal-modal"><i class="fa fa-repeat" aria-hidden="true"></i> Renovaci�n</button>
                                       </c:if>
                                       <c:if test="${case_files.get(case_files.size() - 1).casefile_status == 'En proceso'}">
                                           <button type="button" class="btn btn-outline-secondary" disabled="disabled"><i class="fa fa-clock-o" aria-hidden="true"> Actualmente con solicitud en proceso</i></button>
                                       </c:if>
                                   </c:if>
                               </div>
                               <div class="btn-group" role="group" aria-label="options">
                                   <a href="<c:url value="/establishment/${id_establishment}/caseFile/outdated/new"/>" class="btn btn-outline-secondary" role="button"><i class="fa fa-calendar-times-o" aria-hidden="true"></i> Registro pasado</a>
                               </div>
                           </div>
                       </div>
                   </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover establishment-table" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>C�digo de expediente</th>
                        <th>N�mero de folder</th>
                        <th>Fecha de ingreso</th>
                        <th>Tipo de solicitud</th>
                        <th>Estado de solicitud</th>
                        <th>Fecha de inicio del permiso</th>
                        <th>Estado de permiso</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach var="item" items="${case_files}">
                        <tr>
                            <td hidden="hidden" class="id_owner">${item.id_owner}</td>
                            <td hidden="hidden" class="id_establishment">${item.id_establishment}</td>
                            <td hidden="hidden" class="id_caseFile">${item.id_casefile}</td>
                            <td>
                                <c:if test="${empty item.casefile_code}">No definido</c:if>
                                <c:if test="${not empty item.casefile_code}">${item.casefile_code}</c:if>
                            </td>
                            <td>${item.folder_number}</td>
                            <td>${item.casefile_date}</td>
                            <td>${item.casefile_type}</td>
                            <td class="cell-center-middle">
                                <c:if test="${item.casefile_status == 'En proceso'}">
                                    <span class="badge badge-pill badge-secondary">En proceso</span>
                                </c:if>
                                <c:if test="${item.casefile_status == 'Valido'}">
                                    <span class="badge badge-pill badge-success">Valido</span>
                                </c:if>
                                <c:if test="${item.casefile_status == 'Expirado'}">
                                    <span class="badge badge-pill badge-danger">Expirado</span>
                                </c:if>
                                <c:if test="${item.casefile_status == 'Invalido'}">
                                    <span class="badge badge-pill badge-danger">Invalido</span>
                                </c:if>
                            </td>
                            <td class="cell-center-middle">${item.casefile_start_on}</td>
                            <td>${item.current_process}</td>
                            <td class="cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group">
                                        <a href="<c:url value="/caseFile/${item.id_casefile}/info"/>" class="btn btn-outline-secondary" data-toggle="tooltip" title="Ver detalles"><i class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                                        <a href="<c:url value="/caseFile/${item.id_casefile}/edit"/>" role="button" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top" title="Editar"><i class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <c:if test="${item.current_process_type == 'PROCESS_START'}">
                                            <a href="<c:url value="/requirement-control/establishment/case-file/${item.id_casefile}/template/selector"/>" class="btn btn-outline-secondary" data-toggle="tooltip" title="Iniciar proceso de revisi�n de requerimientos"><i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i></a>
                                        </c:if>
                                        <button type="button" class="btn btn-outline-secondary show-cancelling-modal" data-toggle="tooltip" title="Detener proceso"><i class="fa fa-ban" aria-hidden="true"></i></button>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <a href="javascript:" class="btn btn-outline-secondary show_process_record_summary" data-toggle="tooltip" title="Ver registro de procesos"><i class="fa fa-share-alt" aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<!--First certification process modal-->
<div class="modal fade" id="first-certification-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="first-title">Nuevo ingreso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-question-circle-o fa-2x"></i>
                <h5>�Crear nuevo proceso de certificaci�n para nuevo ingreso?</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a role="button" href="<c:url value="/establishment/${id_establishment}/caseFile/first/new"/>" class="btn btn-primary">Crear nuevo</a>
            </div>
        </div>
    </div>
</div>

<!--Renewal certification process modal-->
<div class="modal fade" id="renewal-certification-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="renewal-title">Renovaci�n</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-question-circle-o fa-2x"></i>
                <h5>�Crear nuevo proceso de certificaci�n para renovaci�n?</h5>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a role="button" href="<c:url value="/establishment/${id_establishment}/caseFile/renewal/new"/>" class="btn btn-primary">Crear renovaci�n</a>
            </div>
        </div>
    </div>
</div>

<!-- Cancel process -->
<div class="modal fade" id="cancel-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cancel-title">Cancelar solicitud</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-warning fa-2x"></i>
                <h5>�Cancelar solicitud?</h5>
                <p class="small text-muted">Una ves cancelado el proceso no se podra continuar con la certificaci�n del permiso.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-danger">Cancelar solicitud</button>
            </div>
        </div>
    </div>
</div>

<!-- Process record modal -->
<div class="modal fade " id="process-summary-modal" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="process-summary-modal-title">Resumen de proceso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-striped table-sm">
                    <thead>
                    <tr>
                        <th scope="col">Proceso</th>
                        <th scope="col">Estado</th>
                        <th scope="col">Observaciones</th>
                        <th scope="col">Inicio</th>
                        <th scope="col">Fin</th>
                        <th scope="col">Modificaciones</th>
                        <th scope="col">--------------</th>
                    </tr>
                    </thead>
                    <tbody id="process-summary-table-body"></tbody>
                </table>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Salir</button>
            </div>
        </div>
    </div>
</div>