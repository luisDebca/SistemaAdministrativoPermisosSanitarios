<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%--@elvariable id="id_establishment" type="java.lang.Long"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/establishment/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/establishment/${id_establishment}/caseFile/all"/>">Establecimiento</a></li>
                    <li class="breadcrumb-item active">Registrar solicitudes pasadas</li>
                </ol>
            </nav>

            <div class="information-panel">
                <div class="alert alert-info" role="alert">
                    <i class="fa fa-info-circle"></i> Este formulario es para solicitudes terminadas que no fueron ingresadas el sistema.
                </div>
            </div>

            <%--@elvariable id="casefile" type="edu.ues.aps.process.area.establishment.model.AbstractCasefileEstablishment"--%>
            <div class="card">
                <div class="card-body">
                    <form:form id="main" method="post" modelAttribute="casefile">
                        <form:hidden path="id"/>
                        <form:hidden path="certification_type"/>
                        <form:hidden path="section"/>
                        <h5 class="card-title">Expediente de establecimiento</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Ingreso de expediente terminado para establecimientos</h6>
                        <hr class="my-4">

                        <div class="form-group row">
                            <label for="request_code" class="col-sm-2 col-form-label">C�digo de solicitud</label>
                            <div class="col-sm-2">
                                <form:input type="text" class="form-control" id="request_code" path="request_code"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="request_number" class="col-sm-2 col-form-label">N�mero de solicitud</label>
                            <div class="col-sm-2">
                                <form:input type="number" class="form-control" id="request_number" path="request_number"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="request_year" class="col-sm-2 col-form-label">A�o de solicitud</label>
                            <div class="col-sm-2">
                                <form:input type="number" class="form-control" id="request_year" path="request_year"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="folder_number" class="col-sm-2 col-form-label">N�mero de folder</label>
                            <div class="col-sm-2">
                                <form:input type="number" class="form-control" id="folder_number" path="request_folder"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="creation_date" class="col-sm-2 col-form-label">Fecha de creaci�n</label>
                            <div class="col-sm-5">
                                <form:input type="text" class="form-control date" id="creation_date" path="creation_date"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="certification_date" class="col-sm-2 col-form-label">Fecha de inicio de certificaci�n</label>
                            <div class="col-sm-5">
                                <form:input type="text" class="form-control date" id="certification_date" path="certification_start_on"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="certification_year" class="col-sm-2 col-form-label">Duraci�n del permiso (A�os)</label>
                            <div class="col-sm-2">
                                <form:input type="number" class="form-control" id="certification_year" path="certification_duration_in_years"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="sibasi_list" class="col-sm-2 col-form-label">SIBASI</label>
                            <div class="col-sm-6">
                                <form:select class="custom-select sibasi-select-list " id="sibasi_list"
                                             path="ucsf.sibasi.id">
                                    <option value="0" selected="selected" disabled="disabled">Seleccione una opci�n</option>
                                    <%--@elvariable id="sibasi_list" type="java.util.List<edu.ues.aps.process.base.dto.AbstractMapDTO>"--%>
                                    <form:options items="${sibasi_list}" itemLabel="value" itemValue="id"/>
                                </form:select>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="ucsf_list" class="col-sm-2 col-form-label">UCSF</label>
                            <div class="col-sm-6">
                                <form:select class="custom-select initialized-ucsf-select-list ucsf-select-list"
                                             id="ucsf_list" path="ucsf.id">
                                    <option value="0" selected="selected" disabled="disabled">Seleccione una opci�n</option>
                                    <%--@elvariable id="ucsf_list" type="java.util.List<edu.ues.aps.process.base.dto.AbstractMapDTO>"--%>
                                    <form:options items="${ucsf_list}" itemValue="id" itemLabel="value"/>
                                </form:select>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="status" class="col-sm-2 col-form-label">Estado actual del expediente</label>
                            <div class="col-sm-6">
                                <form:select class="custom-select" id="status" path="status">
                                    <option value="0" selected="selected" disabled="disabled">Seleccione una opci�n</option>
                                    <form:option value="IN_PROCESS">En proceso de solicitud del permiso</form:option>
                                    <form:option value="VALID">El permiso esta v�lido</form:option>
                                    <form:option value="EXPIRED">El permiso esta vencido</form:option>
                                </form:select>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="custom-control custom-checkbox">
                            <form:checkbox class="custom-control-input" id="apply" path="apply_payment"/>
                            <label class="custom-control-label" for="apply">Aplica para mandamiento de pago</label>
                        </div>

                        <hr class="my-4">

                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a role="button" class="btn btn-secondary" href="
                        <c:url value="/establishment/${id_establishment}/caseFile/all"/>">Cancelar</a>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>