<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<!--VEHICLE BUNCH MODAL INFORMATION-->
<div class="modal fade bd-example-modal-lg" id="bunch_info" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="bunc_info_label">Solicitudes contenidas</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <table class="table table-sm table-striped">
                    <thead>
                    <tr><th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>N�</th>
                        <th>Solicitud</th>
                        <th>Placa</th>
                        <th>Tipo</th>
                        <th>Transporta</th>
                        <th>N� Empleados</th>
                        <th>Permanencia</th>
                        <th class="modal-bunch-options">Opciones</th>
                    </tr>
                    </thead>
                    <tbody id="modal_bunch_info_body">

                    </tbody>
                </table>
            </div>
            <div class="modal-footer">
                <div class="btn-group float-left" role="group" aria-label="Basic example">
                    <a id="bunch-modal-info-edit" role="button" href="javascript:" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="right"
                            title="Editar solicitud"><i class="fa fa-edit"></i></a>
                    <button type="button" class="btn btn-outline-secondary generate-bunch-vehicle-list-btn"
                            data-toggle="tooltip" data-placement="right" title="Generar lista de veh�culos"><i
                            class="fa fa-file-pdf-o"></i>
                    </button>
                </div>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <a href="#" id="all-info-btn" role="button" class="btn btn-primary">Ver m�s</a>
            </div>
        </div>
    </div>
</div>