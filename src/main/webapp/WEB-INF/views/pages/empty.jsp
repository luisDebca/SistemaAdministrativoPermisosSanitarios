<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="col-12">
            <div class="alert alert-light" role="alert">
                Vista no terminada <a href="<c:url value="/index"/>" class="alert-link">Ir a inicio</a>.
            </div>
        </div>
    </div>
</main>