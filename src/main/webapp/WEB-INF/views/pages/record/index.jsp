<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/caseFile/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Registros</li>
                </ol>
            </nav>

            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>Sección</th>
                        <th>Proceso</th>
                        <th>Modificación</th>
                        <th>Por</th>
                        <th>Descripción</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="items" type="java.util.List<edu.ues.aps.process.record.base.dto.AbstractHistoryDTO>"--%>
                    <c:forEach items="${items}" var="item">
                        <tr>
                            <td class="id_user" hidden="hidden">${item.id_user}</td>
                            <td class="id_caseFile" hidden="hidden">${item.id_caseFile}</td>
                            <td class="id_process" hidden="hidden">${item.id_process}</td>
                            <td class="text-center">
                                    ${item.caseFile_folder_number}
                                <c:if test="${empty item.caseFile_folder_number}">
                                    <span class="badge badge-dark">Sistema</span>
                                </c:if>
                            </td>
                            <td class="text-center">
                                <c:if test="${item.caseFile_section == 'Invalido'}">
                                    <span class="badge badge-dark">----</span>
                                </c:if>
                                <c:if test="${item.caseFile_section != 'Invalido'}">
                                    ${item.caseFile_section}
                                </c:if>
                            </td>
                            <td>${item.process}</td>
                            <td>${item.modification_date}</td>
                            <td>${item.user_full_name}</td>
                            <td>${item.description}</td>
                            <td class="cell-center-middle">
                                <div class="btn-group" role="group" aria-label="Basic example">
                                    <a href="javascript:" role="button" class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top" title="Ver todos los registros de este usuario"><i class="fa fa-user-o"></i></a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>

        </div>
    </div>
</main>
