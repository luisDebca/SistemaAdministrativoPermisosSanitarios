<%--@elvariable id="config" type="edu.ues.aps.process.settings.dto.AbstractConfigurationDTO"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row">
                <div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary"><h3><i class="fa fa-gears" aria-hidden="true"></i> Valores por defecto</h3></div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">N�meros por defecto del sistema</h5>
                            <h6 class="card-subtitle text-muted">A partir de los estos valores se cargan los n�meros generados por el sistema.</h6>
                            <p class="text-info">Los campos que tienen valor cero no se tomar�n en cuenta para el auto incremento.</p>
                            <p class="text-info">Fecha de configuraci�n ${config.date}.</p>
                            <div hidden="hidden" id="default-id">${config.id}</div>
                            <hr class="my-4">
                            <div class="card-columns">
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">Folder de establecimientos</h5>
                                        <p class="card-text">Valor actual: <span class="badge badge-pill badge-primary">${config.establishment_folder_number}</span></p>
                                        <a href="/configuration/default-value/establishment-folder-number" class="btn btn-outline-secondary set-default-value-btn disabled_href">Modificar</a>
                                    </div>
                                </div>
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">Solicitud de establecimientos</h5>
                                        <p class="card-text">Valor actual: <span class="badge badge-pill badge-primary">${config.establishment_request_number}</span></p>
                                        <a href="/configuration/default-value/establishment-request-number" class="btn btn-outline-secondary set-default-value-btn disabled_href">Modificar</a>
                                    </div>
                                </div>
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">Resoluci�n de establecimientos</h5>
                                        <p class="card-text">Valor actual: <span class="badge badge-pill badge-primary">${config.establishment_resolution_number}</span></p>
                                        <a href="/configuration/default-value/establishment-resolution-number" class="btn btn-outline-secondary set-default-value-btn disabled_href">Modificar</a>
                                    </div>
                                </div>
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">Folder de veh�culos</h5>
                                        <p class="card-text">Valor actual: <span class="badge badge-pill badge-primary">${config.vehicle_folder_number}</span></p>
                                        <a href="/configuration/default-value/vehicle-folder-number" class="btn btn-outline-secondary set-default-value-btn disabled_href">Modificar</a>
                                    </div>
                                </div>
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">Solicitud de veh�culos</h5>
                                        <p class="card-text">Valor actual: <span class="badge badge-pill badge-primary">${config.vehicle_request_number}</span></p>
                                        <a href="/configuration/default-value/vehicle-request-number" class="btn btn-outline-secondary set-default-value-btn disabled_href">Modificar</a>
                                    </div>
                                </div>
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">Resoluci�n de veh�culos</h5>
                                        <p class="card-text">Valor actual: <span class="badge badge-pill badge-primary">${config.vehicle_resolution_number}</span></p>
                                        <a href="/configuration/default-value/vehicle-resolution-number" class="btn btn-outline-secondary set-default-value-btn disabled_href">Modificar</a>
                                    </div>
                                </div>
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">Mandamientos de pago</h5>
                                        <p class="card-text">Valor actual: <span class="badge badge-pill badge-primary">${config.payment_number}</span></p>
                                        <a href="/configuration/default-value/payment-procedure" class="btn btn-outline-secondary set-default-value-btn disabled_href">Modificar</a>
                                    </div>
                                </div>
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">Dictamen y Opini�n Jur�dica</h5>
                                        <p class="card-text">Valor actual: <span class="badge badge-pill badge-primary">${config.legal_memorandum_number}</span></p>
                                        <a href="/configuration/default-value/legal-memorandum" class="btn btn-outline-secondary set-default-value-btn disabled_href">Modificar</a>
                                    </div>
                                </div>
                                <div class="card" style="width: 18rem;">
                                    <div class="card-body">
                                        <h5 class="card-title">Memor�ndums</h5>
                                        <p class="card-text">Valor actual: <span class="badge badge-pill badge-primary">${config.memorandum_number}</span></p>
                                        <a href="/configuration/default-value/memorandum" class="btn btn-outline-secondary set-default-value-btn disabled_href">Modificar</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

<div id="edit-value-modal" class="modal fade bd-example-modal-sm" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-sm">
        <div class="modal-content">
            <div class="modal-body">
                <form id="default-form" action="javascript:">
                    <div class="form-group">
                        <label for="new-value" class="col-form-label">Nuevo valor:</label>
                        <input type="number" class="form-control" id="new-value" min="0" max="99999">
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="save-default" type="button" class="btn btn-primary">Guardar</button>
            </div>
        </div>
    </div>
</div>