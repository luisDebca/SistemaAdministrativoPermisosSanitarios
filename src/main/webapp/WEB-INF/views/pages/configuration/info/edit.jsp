<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value='/app/information/index'/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Actualizar informaci�n</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">
                    <%--@elvariable id="info" type="edu.ues.aps.process.base.model.Place"--%>
                    <form:form id="main" modelAttribute="info" method="POST">

                        <form:hidden path="id"/>

                        <h5 class="card-title">Informaci�n</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Modificar informaci�n.</h6>
                        <hr class="my-4">

                        <div class="form-group row">
                            <label for="f0" class="col-sm-2 col-form-label">Mostrar nombre como</label>
                            <div class="col-sm-6">
                                <form:input path="organization_name" id="f0" type="text"  class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="f1" class="col-sm-2 col-form-label">Descripci�n</label>
                            <div class="col-sm-8">
                                <form:textarea path="business_description" id="f1" type="text"  class="form-control"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="f2" class="col-sm-2 col-form-label">Direcci�n</label>
                            <div class="col-sm-8">
                                <form:input id="f2" type="text"  class="form-control " path="address"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="f3" class="col-sm-2 col-form-label">Departamento</label>
                            <div class="col-sm-4">
                                <form:input id="f3" type="text"  class="form-control " path="department"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="f4" class="col-sm-2 col-form-label">Municipio</label>
                            <div class="col-sm-4">
                                <form:input id="f4" type="text"  class="form-control " path="municipality"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="f5" class="col-sm-2 col-form-label">Tel�fono</label>
                            <div class="col-sm-4">
                                <form:input id="f5" type="text"  class="form-control " path="telephone"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="f6" class="col-sm-2 col-form-label">Correo electronico</label>
                            <div class="col-sm-4">
                                <form:input id="f6" type="text"  class="form-control " path="email"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="f7" class="col-sm-2 col-form-label">FAX</label>
                            <div class="col-sm-4">
                                <form:input id="f7" type="text"  class="form-control " path="fax"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="f8" class="col-sm-2 col-form-label">Zona</label>
                            <div class="col-sm-4">
                                <form:input id="f8" type="text"  class="form-control " path="zone"/>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="f9" class="col-sm-2 col-form-label">Horarios</label>
                            <div class="col-sm-6">
                                <form:input id="f9" type="text"  class="form-control " path="availability" />
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="f10" class="col-sm-2 col-form-label">Secci�n</label>
                            <div class="col-sm-4">
                                <form:input id="f10" type="text"  class="form-control " path="section_name"/>
                            </div>
                        </div>


                        <hr class="my-4">

                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href='<c:url value="/app/information/index"/>' class="btn btn-secondary">Cancelar</a>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>
