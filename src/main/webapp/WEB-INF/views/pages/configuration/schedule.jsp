<%--@elvariable id="confi" type="edu.ues.aps.miscellaneous.dto.AbstractPlaceDTO"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row">
                <div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary"><h3><i class="fa fa-gears" aria-hidden="true"></i> Ajuste de tareas Programadas</h3></div>
            </div>

            <div class="row">
                <div class="col">
                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">Tareas</h5>
                            <h6 class="card-subtitle text-muted">Se presentan los ajustes que se pueden hacer en aquellas tareas que se ejecutan en segundo plano</h6>
                            <hr class="my-4">

                            <form>
                                <div class="form-group row">
                                    <label for="hours" class="col-sm-2 col-form-label">Hora de Ejecucion</label>
                                    <div class="col-sm-3" >
                                        <input type="text" class="form-control scheduledhours" id="hours" name="hoursScheduled" autocomplete="off" />
                                    </div>
                                    <button class="btn btn-primary" type="submit" value="" >Ingresar</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>