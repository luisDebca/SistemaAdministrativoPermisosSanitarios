<%--@elvariable id="info" type="edu.ues.aps.miscellaneous.dto.AbstractPlaceDTO"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row">
                <div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary"><h3><i class="fa fa-gears" aria-hidden="true"></i> Configuraci�n</h3></div>
            </div>

            <c:if test="${not info.isValid()}">
                <div class="alert alert-danger" role="alert">
                    <i class="fa fa-warning"></i> No se ha encontrado la informaci�n correcta <a href="<c:url value="/app/information/init"/>" class="alert-link"> Click aqui! </a> para ingresar los datos correctos.
                </div>
            </c:if>

            <div class="row">
                <div class="col">
                    <div class="card mb-3">
                        <div class="card-body">
                            <h5 class="card-title">Informaci�n</h5>
                            <h6 class="card-subtitle text-muted">Aqu� se muestra la informaci�n perteneciente a la empresa, estos datos se utilizan en la generaci�n de reportes y para mejorar la experiencia del usuario. Configure los campos correctamente.</h6>
                            <hr class="my-4">
                            <form>
                                <div class="form-group row">
                                    <label for="f1" class="col-sm-2 col-form-label">Mostrar nombre como</label>
                                    <div class="col-sm-10">
                                        <input id="f1" type="text" readonly class="form-control form-control-plaintext" value="${info.section_name}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="f2" class="col-sm-2 col-form-label">Direcci�n</label>
                                    <div class="col-sm-10">
                                        <input id="f2" type="text" readonly class="form-control form-control-plaintext" value="${info.address}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="f3" class="col-sm-2 col-form-label">Departamento</label>
                                    <div class="col-sm-10">
                                        <input id="f3" type="text" readonly class="form-control form-control-plaintext" value="${info.department}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="f4" class="col-sm-2 col-form-label">Municipio</label>
                                    <div class="col-sm-10">
                                        <input id="f4" type="text" readonly class="form-control form-control-plaintext" value="${info.municipality}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="f5" class="col-sm-2 col-form-label">Tel�fono</label>
                                    <div class="col-sm-10">
                                        <input id="f5" type="text" readonly class="form-control form-control-plaintext" value="${info.telephone}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="f6" class="col-sm-2 col-form-label">Correo electronico</label>
                                    <div class="col-sm-10">
                                        <input id="f6" type="text" readonly class="form-control form-control-plaintext" value="${info.email}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="f7" class="col-sm-2 col-form-label">FAX</label>
                                    <div class="col-sm-10">
                                        <input id="f7" type="text" readonly class="form-control form-control-plaintext" value="${info.FAX}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="f8" class="col-sm-2 col-form-label">Zona</label>
                                    <div class="col-sm-10">
                                        <input id="f8" type="text" readonly class="form-control form-control-plaintext" value="${info.zone}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="f9" class="col-sm-2 col-form-label">Horarios</label>
                                    <div class="col-sm-10">
                                        <input id="f9" type="text" readonly class="form-control form-control-plaintext" value="${info.availability}">
                                    </div>
                                </div>
                                <div class="form-group row">
                                    <label for="f10" class="col-sm-2 col-form-label">Secci�n</label>
                                    <div class="col-sm-10">
                                        <input id="f10" type="text" readonly class="form-control form-control-plaintext" value="${info.organization_name}">
                                    </div>
                                </div>

                                <c:if test="${info.isValid()}">
                                    <hr class="my-4">
                                    <a role="button" class="btn btn-primary" href="<c:url value="/app/information/edit"/>"><i class="fa fa-chevron-right"></i> Editar informaci�n</a>
                                </c:if>
                            </form>

                            <hr class="my-4">
                            <h5 class="card-subtitle">Configuraci�n de valores predefinidos.</h5>
                            <p class="card-text">Establecer los valores por defecto para los valores autogenerados por el sistema.</p>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a role="button" class="btn btn-primary " href="<c:url value="/configuration/default-values/index"/>"><i class="fa fa-chevron-right"></i> Valores por defecto</a>
                            </div>

                            <hr class="my-4">
                            <h5 class="card-subtitle">Configuraci�n de tareas del sistema.</h5>
                            <p class="card-text">Agregar, modificar eliminar tareas programadas del sistema.</p>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a role="button" class="btn btn-primary " href="<c:url value="/scheduled/tasks"/>"><i class="fa fa-chevron-right"></i> Ajuste de tareas</a>
                            </div>

                            <hr class="my-4">
                            <h5 class="card-subtitle">Formato de reportes.</h5>
                            <p class="card-text">Agregar, modificar eliminar formatos a los reportes generado en el sistema.</p>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <a role="button" class="btn btn-primary " href="<c:url value="/configuration/report-format/index"/>"><i class="fa fa-chevron-right"></i> Formatos</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>