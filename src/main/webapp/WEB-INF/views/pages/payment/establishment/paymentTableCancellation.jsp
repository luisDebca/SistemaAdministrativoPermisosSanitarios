<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/payment/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Mandamientos de pago pendientes de cancelaci�n</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover establishment-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>Establecimiento</th>
                        <th>Tipo</th>
                        <th>Rubro</th>
                        <th>NIT</th>
                        <th>Direcci�n</th>
                        <th>Propietario</th>
                        <th>SIBASI/UCSF</th>
                        <th>Permiso</th>
                        <th>Recepci�n</th>
                        <th>Tiempo restante</th>
                        <th>Estado mandamiento</th>
                        <th>Costo</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.payment.establishment.dto.AbstractEstablishmentPaymentRequestDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td class="id_owner" hidden="hidden">${item.id_owner}</td>
                            <td class="id_establishment" hidden="hidden">${item.id_establishment}</td>
                            <td class="id_caseFile" hidden="hidden">${item.id_caseFile}</td>
                            <td><a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>">${item.folder_number}</a>
                            </td>
                            <td>
                                <a href="<c:url value="/establishment/${item.id_establishment}"/>">${item.establishment_name}</a>
                            </td>
                            <td>${item.establishment_type_detail}</td>
                            <td>${item.establishment_section}/${item.establishment_type}</td>
                            <td>${item.establishment_nit}</td>
                            <td>${item.establishment_address}</td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.SIBASI}/${item.UCSF}</td>
                            <td>${item.certification_type}</td>
                            <td>${item.caseFile_createOn}</td>
                            <c:if test="${item.cancelled}">
                                <td class="cell-center-middle">
                                    <span class="badge badge-success">Cancelado:<br>${item.cancelled_on}</span>
                                </td>
                            </c:if>
                            <c:if test="${not item.cancelled}">
                                <td class="timer cell-center-middle">
                                    <div hidden="hidden" class="delay">${item.delay_date}</div>
                                </td>
                            </c:if>
                            <td class="cell-center-middle">
                                <c:if test="${item.delivery and item.created and not item.cancelled_on and not item.expired}"><span
                                        class="badge badge-secondary">Entregado</span></c:if>
                                <c:if test="${item.delivery and item.created and item.cancelled and not item.expired}"><span
                                        class="badge badge-success">Cancelado</span></c:if>
                                <c:if test="${item.delivery and item.created and not item.cancelled and item.expired}"><span
                                        class="badge badge-danger">Retrasado</span></c:if>
                                <c:if test="${item.delivery and item.created and item.cancelled and item.expired}"><span
                                        class="badge badge-success">Cancelado<br>con retraso</span></c:if>
                            </td>
                            <td class="cell-center-middle">$ ${item.payment_cost}</td>
                            <td class="table-options cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="grouInf">
                                        <a role="button" href="<c:url value="/caseFile/${item.id_caseFile}/info"/>"
                                           class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top"
                                           title="Ver detalles del expediente"><i class="fa fa-folder-open-o"
                                                                                  aria-hidden="true"></i></a>
                                        <c:if test="${not item.cancelled}">
                                            <a role="button" href="#"
                                               class="btn btn-outline-secondary disabled_href show-payment-cancellation-confirmation-modal"
                                               data-toggle="tooltip" data-placement="top" title="Marcar como cancelado"><i
                                                    class="fa fa-check-circle-o" aria-hidden="true"></i></a>
                                        </c:if>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <c:if test="${item.cancelled}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary move_to_next_process_btn"
                                                    data-toggle="tooltip" title="Pasar a revisi�n de la solicitud"><i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                        </c:if>
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                    aria-hidden="true"></i>
                                        </button>
                                        <button type="button"
                                                class="btn btn-outline-secondary show_process_record_summary"
                                                data-toggle="tooltip" title="Ver resumen de proceso"><i
                                                class="fa fa-share-alt" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="payment-cancellation-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="cancellation-title">Marcar como cancelado</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-money fa-2x" aria-hidden="true"></i>
                <h5>�Acepta la cancelaci�n del mandamiento de pago?</h5>
                <h6 class="text-muted small">Al dar click en aceptar se marcara como recibido el comprobante de pago de
                    la colecturia autorizada</h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" class="btn btn-primary" id="accept-payment-cancellation-btn">Aceptar</button>
            </div>
        </div>
    </div>
</div>