<%--@elvariable id="request_info" type="edu.ues.aps.process.payment.establishment.dto.PaymentProcessDTO"--%>
<%--@elvariable id="recommended_value" type="java.lang.String"--%>
<%--@elvariable id="rates" type="java.util.List<edu.ues.aps.process.payment.base.dto.PaymentRateDTO>"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/payment/index"/>">Mandamientos de pago</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/payment/establishment/pending/delivery"/>">Pendientes
                        de entrega</a></li>
                    <li class="breadcrumb-item active">Precios del fondo de actividades especiales del ministerio de
                        salud
                    </li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col">
                            <h5 class="card-title">Selecci�n del monto a pagar por tramite</h5>
                            <p hidden="hidden" id="recommended_value">${recommended_value}</p>
                            <p><strong>Nombre del establecimiento: </strong><a href="#" data-toggle="tooltip"
                                                                               data-placement="top"
                                                                               title="Ver informaci�n">${request_info.establishment_name}</a>
                            </p>
                            <p><strong>Direccio�n del establecimiento: </strong>${request_info.establishment_address}
                            </p>
                            <p><strong>Propietario: </strong><a href="#" data-toggle="tooltip" data-placement="top"
                                                                title="Ver informaci�n">${request_info.owner_name}</a>
                            </p>
                            <p><strong>Tipo de establecimiento: </strong>${request_info.establishment_type_details}</p>
                            <p><strong>Solicitud de permiso para: </strong>${request_info.casefile_certification_type}
                            </p>
                            <p><strong>Capital registrado: </strong>$${request_info.establishment_capital}</p>
                        </div>
                        <div class="col text-center">
                            <small class="text-muted">Cantidad seleccionada</small>
                            <h1 class="selected-amount-preview">$0.0</h1>
                            <%--@elvariable id="payment_rate" type="edu.ues.aps.process.payment.base.dto.PaymentRateDTO"--%>
                            <form:form method="post" modelAttribute="payment_rate" id="main-form">
                                <form:hidden path="rate_code"/>
                            </form:form>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-primary" id="save-selected-amount">Aceptar
                                </button>
                                <button type="button" class="btn btn-secondary" id="clean-selected-amount-preview">
                                    Cancelar
                                </button>
                            </div>
                            <br><br><br>
                            <a href="<c:url value="/payment/establishment/pending/delivery"/>" role="button"
                               class="btn btn-secondary">Regresar a lista</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card">
                <div class="card-body">
                    <p>${rates.get(0).rate_description}</p>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th align="center">Menor de $5,000.00</th>
                            <th align="center">Entre $5,000.00 y $25,000.00</th>
                            <th align="center">Entre $25,000.00 y $100,000.00</th>
                            <th align="center">Mayor de $100,000.00</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="${rates.get(0).rate_code}" align="center" class="mouseSelectedItem">
                                $${rates.get(0).rate_cost}</td>
                            <td id="${rates.get(1).rate_code}" align="center" class="mouseSelectedItem">
                                $${rates.get(1).rate_cost}</td>
                            <td id="${rates.get(2).rate_code}" align="center" class="mouseSelectedItem">
                                $${rates.get(2).rate_cost}</td>
                            <td id="${rates.get(3).rate_code}" align="center" class="mouseSelectedItem">
                                $${rates.get(3).rate_cost}</td>
                        </tr>
                        </tbody>
                    </table>
                    <p>${rates.get(4).rate_description}</p>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th align="center">Menor de $500.00</th>
                            <th align="center">Entre $500.00 y $1,000.00</th>
                            <th align="center">Entre $1,000.00 y $5,000.00</th>
                            <th align="center">Mayor de $5,000.00</th>
                        </tr>
                        </thead>
                        <tbody>
                        <tr>
                            <td id="${rates.get(4).rate_code}" align="center" class="mouseSelectedItem">
                                $${rates.get(4).rate_cost}</td>
                            <td id="${rates.get(5).rate_code}" align="center" class="mouseSelectedItem">
                                $${rates.get(5).rate_cost}</td>
                            <td id="${rates.get(6).rate_code}" align="center" class="mouseSelectedItem">
                                $${rates.get(6).rate_cost}</td>
                            <td id="${rates.get(7).rate_code}" align="center" class="mouseSelectedItem">
                                $${rates.get(7).rate_cost}</td>
                        </tr>
                        </tbody>
                    </table>
                    <p>Precios seg�n tipo de establecimientos</p>
                    <table class="table table-bordered">
                        <thead>
                        <tr>
                            <th>Concepto</th>
                            <th>Monto a pagar</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${rates}" var="item">
                            <c:if test="${item.rate_maximum == 0 and item.rate_minimum == 0}">
                                <tr>
                                    <td>${item.rate_description}</td>
                                    <td id="${item.rate_code}" align="center" class="mouseSelectedItem">
                                        $${item.rate_cost}</td>
                                </tr>
                            </c:if>
                        </c:forEach>
                        </tbody>
                    </table>

                </div>
            </div>
        </div>
    </div>
</main>