<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/payment/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Mandamientos de pago pendientes de creaci�n y entrega</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover vehicle-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>N� Solicitudes</th>
                        <th>Propietario</th>
                        <th>Compa�ia de distribuci�n</th>
                        <th>Destino de distribuci�n</th>
                        <th>Lugar de mercancia</th>
                        <th>Direcci�n de estancia</th>
                        <th>Tiempo restante</th>
                        <th>Estado mandamiento</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.payment.vehicle.dto.AbstractVehiclePaymentProcessDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td hidden="hidden" class="id_owner">${item.id_owner}</td>
                            <td hidden="hidden" class="id_bunch">${item.id_bunch}</td>
                            <td class="cell-center-middle">${item.request_folder}</td>
                            <td class="cell-center-middle"><h5>${item.request_count}</h5></td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.bunch_distribution_company}</td>
                            <td>${item.bunch_distribution_destination}</td>
                            <td>${item.bunch_marketing_place}</td>
                            <td>${item.bunch_address}</td>
                            <td class="timer cell-center-middle">
                                <div hidden="hidden" class="delay">${item.delay_date}</div>
                            </td>
                            <td class="cell-center-middle">
                                <c:if test="${not item.created and not item.delivery}"><span
                                        class="badge badge-secondary">Pendiente<br>Creaci�n</span></c:if>
                                <c:if test="${item.created and not item.delivery}"><span class="badge badge-secondary">Pendiente<br>Entrega</span></c:if>
                                <c:if test="${item.delivery and item.created}"><span class="badge badge-success">Entregado</span></c:if>
                            </td>
                            <td class="cell-center-middle table-options">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="groupInf">
                                        <a href="javascript:"
                                           class="btn btn-outline-secondary disabled_href show_brief_bunch_information"
                                           data-toggle="tooltip" title="Ver solicitudes contenidas"><i class="fa fa-th"
                                                                                                       aria-hidden="true"></i></a>
                                        <c:if test="${not item.created}">
                                            <a role="button"
                                               href="<c:url value="/payment/vehicle/bunch/${item.id_bunch}/new"/>"
                                               class="btn btn-outline-secondary"
                                               data-toggle="tooltip" title="Crear mandamiento de pago"><i
                                                    class="fa fa-plus"></i></a>
                                        </c:if>
                                        <c:if test="${item.created}">
                                            <a role="button"
                                               href="<c:url value="/payment/vehicle/bunch/${item.id_bunch}/edit"/>"
                                               class="btn btn-outline-secondary"
                                               data-toggle="tooltip" title="Editar mandamiento de pago"><i
                                                    class="fa fa-edit"></i></a>
                                        </c:if>
                                        <c:if test="${item.created and not item.delivery}">
                                            <a role="button" href="javascript:"
                                               class="btn btn-outline-secondary show-delivery-confirmation-modal-btn"
                                               data-toggle="tooltip" title="Marcar como entregado"><i
                                                    class="fa fa-check-square-o"></i></a>
                                        </c:if>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <c:if test="${item.created and item.delivery}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary disabled_href move_to_next_process_btn"
                                                    data-toggle="tooltip"
                                                    title="Pasar a mandamientos de pago pendientes de cancelaci�n"><i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                        </c:if>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                    aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<div hidden="hidden" id="bunch-options">
    <a role="button" href="javascript:" class="btn btn-outline-secondary show-generate-bunch-payment-process-modal-btn"><i class="fa fa-file-pdf-o"></i></a>
</div>

<!-- PAYMENT PROCEDURE REPORT MODAL -->
<div class="modal fade" id="payment-procedure-report-modal" tabindex="-1" role="dialog"
     aria-labelledby="SelectorModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Generar Mandamiento de pago</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="selected-client">Seleccione el usuario que firmara el documento</label>
                    <select id="selected-client" class="custom-select"></select>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="generate-payment-process-report-btn" type="button" class="btn btn-primary">Generar</button>
            </div>
        </div>
    </div>
</div>
<!-- PAYMENT PROCEDURE DELIVERY CONFIRMATION -->
<div class="modal fade" id="delivery-confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="delivery-confirmation-title">Entrega de mandamiento de pago</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-handshake-o fa-2x" aria-hidden="true"></i>
                <h5>�Confirmar entrega de mandamiento de pago a usuario?</h5>
                <h6 class="text-muted small">Una vez se marque el documento como entregado a usuario se podra continuar
                    con el proceso. Este paso no se puede revertir</h6>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="accept-delivery-casefile-btn" type="button" class="btn btn-primary">Aceptar</button>
            </div>
        </div>
    </div>
</div>