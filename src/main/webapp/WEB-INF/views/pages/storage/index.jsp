<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row">
                <div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary"><h3><i class="fa fa-hdd-o" aria-hidden="true"></i>
                    Almacenamiento de archivos</h3></div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"></h5>
                            <h6 class="card-subtitle mb-2 text-muted">Almacenamiento interno</h6>
                            <p class="card-text">Mantener todos los archivos en un solo lugar. Disponibles para el grupo de trabajo.</p>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <button type="button" class="btn btn-info btn-lg"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Subir archivos</button>
                                <a role="button" href="<c:url value="/storage/folder"/>" class="btn btn-secondary btn-lg"><i class="fa fa-folder-o" aria-hidden="true"></i>
                                    Ver carpeta</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>