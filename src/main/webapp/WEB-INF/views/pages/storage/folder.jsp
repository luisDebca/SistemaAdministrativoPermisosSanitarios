<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/storage/index"/>'>Inicio</a></li>
                    <li class="breadcrumb-item">Folder principal</li>
                </ol>
            </nav>

            <div class="information-panel">
                <c:if test="${empty files}">
                    <div class="alert alert-info" role="alert">
                        La carpeta de archivos esta vac�a. <a href="<c:url value="/storage/upload"/>" class="alert-link">Click aqui</a> para subir archivos.
                    </div>
                </c:if>
            </div>

            <div class="row">
                <%--@elvariable id="files" type="java.util.List<edu.ues.aps.storage.dto.AbstractFileDTO>"--%>
                <c:forEach var="item" items="${files}">
                    <div class="col-2 mb-4">
                        <div class="card">
                            <img class="card-img-top w-25" src="<c:url value="/static/img/file/doc.png"/>" style="margin: auto;padding: 1rem 0">
                            <div class="card-body">
                                <h6 class="card-title">${item.original_name}</h6>
                            </div>
                        </div>
                    </div>
                </c:forEach>
            </div>

        </div>
    </div>
</main>