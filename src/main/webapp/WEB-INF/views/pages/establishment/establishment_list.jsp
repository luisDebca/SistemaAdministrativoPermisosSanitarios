<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="sec" uri="http://www.springframework.org/security/tags"%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/establishment/index"/>'>Inicio</a></li>
                    <li class="breadcrumb-item">Establecimientos</li>
                </ol>
            </nav>

            <div class="information-panel">
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    �<strong>C�mo ver los expedientes</strong> del establecimiento?: Click en el icono <i
                        class="fa fa-book fa-2x"></i> para ver todos los expedientes registrados del establecimiento.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th>Nombre</th>
                        <th>Direcci�n</th>
                        <th>Rubro</th>
                        <th>Tipo</th>
                        <th>Propietario</th>
                        <th>Contactar</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="establishments" type="java.util.List<edu.ues.aps.process.area.establishment.dto.EstablishmentDTO>"--%>
                    <c:forEach items="${establishments}" var="item">
                        <tr <c:if test="${!item.isActive()}">class="table-danger" </c:if>>
                            <td hidden="hidden" class="id_establishment">${item.establishment_id}</td>
                            <td><a href="<c:url value="/establishment/${item.establishment_id}"/>">${item.establishment_name}</a></td>
                            <td>${item.establishment_address}</td>
                            <td>${item.establishment_type}</td>
                            <td>${item.establishment_type_detail}</td>
                            <td><a href="<c:url value="/owner/${item.owner_id}"/>">${item.establishment_owner_name}</a></td>
                            <td>
                                <ul style="list-style: none">
                                    <c:if test="${not empty item.establishment_telephone}">
                                        <li>Telefono: ${item.establishment_telephone}</li>
                                    </c:if>
                                    <c:if test="${not empty item.establishment_email}">
                                        <li>Correo Electronico: ${item.establishment_email}</li>
                                    </c:if>
                                    <c:if test="${not empty item.establishment_fax}">
                                        <li>FAX: ${item.establishment_fax}</li>
                                    </c:if>
                                </ul>
                            </td>
                            <td class="cell-center-middle">
                                <div class="btn-group-vertical table-options">
                                    <div class="btn-group" role="group">
                                        <a role="button" href="<c:url value="/establishment/${item.establishment_id}"/>"
                                           class="btn btn-outline-secondary" data-toggle="tooltip" title="Ver detalles"><i
                                                class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                                        <a role="button" href="<c:url value="/establishment/${item.establishment_id}/edit"/>"
                                           class="btn btn-outline-secondary" data-toggle="tooltip" title="Editar"><i
                                                class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                        <sec:authorize access="hasRole('CENSUS')">
                                            <c:if test="${item.isActive()}">
                                                <a role="button" href="javascript:" class="btn btn-outline-secondary set-establishment-active-btn" data-toggle="tooltip" title="Deshabilitar Establecimiento"><i
                                                        class="fa fa-times" aria-hidden="true"></i></a>
                                            </c:if>
                                            <c:if test="${!item.isActive()}">
                                                <a role="button" href="javascript:" class="btn btn-outline-secondary set-establishment-active-btn" data-toggle="tooltip" title="Habilitar Establecimiento"><i
                                                        class="fa fa-check" aria-hidden="true"></i></a>
                                            </c:if>
                                        </sec:authorize>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <a role="button" href="<c:url value="/establishment/${item.establishment_id}/caseFile/all"/>"
                                           class="btn btn-outline-secondary" data-toggle="tooltip"
                                           title="Ver expedientes"><i class="fa fa-book"
                                                                      aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
