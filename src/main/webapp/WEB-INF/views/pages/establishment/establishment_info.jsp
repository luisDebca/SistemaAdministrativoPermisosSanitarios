<%--@elvariable id="establishment" type="edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/establishment/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/establishment/list"/>">Establecimientos</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Informaci�n de establecimiento</li>
                </ol>
            </nav>

            <c:if test="${not establishment.isActive()}">
                <div class="alert alert-danger alert-dismissible fade show" role="alert">
                    <strong>Deshabilitado!</strong> Este establecimiento se encuentra actualmente deshabilitado.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </c:if>

            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-primary rounded box-shadow">
                <div class="lh-100">
                    <h4 class="mb-0 text-white lh-100">Informaci�n de establecimiento: <strong>${establishment.establishment_name}</strong></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h6 class="border-bottom border-gray pb-2 mb-0">Informaci�n completa</h6>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Nombre</strong>
                                ${establishment.establishment_name}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Direcci�n</strong>
                                ${establishment.establishment_address}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">NIT</strong>
                                ${establishment.establishment_nit}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Tipo de establecimiento (Escrito por el usuario)</strong>
                                ${establishment.establishment_type_detail}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Secci�n</strong>
                                ${establishment.establishment_type}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Registro de comercio</strong>
                                ${establishment.commercial_register}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Capital</strong>
                                $ ${establishment.establishment_capital}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Tel�fono</strong>
                                ${establishment.establishment_telephone}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">FAX</strong>
                                ${establishment.establishment_fax}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Correo electr�nico</strong>
                                ${establishment.establishment_email}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Horario de operaciones</strong>
                                ${establishment.establishment_operations}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">N�mero de empleados masculinos</strong>
                                ${establishment.establishment_male_employee_count}
                            </p>
                        </div>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">N�mero de empleados femeninos</strong>
                                ${establishment.establishment_female_employee_count}
                            </p>
                        </div>

                        <a role="button" class="btn btn-outline-primary btn-lg btn-block" href="<c:url value="/establishment/${establishment.establishment_id}/edit"/>"><i class="fa fa-edit"></i> Editar</a>
                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h6 class="border-bottom border-gray pb-2 mb-0">Registros</h6>

                        <div class="media text-muted pt-3">
                            <div class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">Permisos sanitarios</strong>
                                    <a href="<c:url value="/establishment/${establishment.establishment_id}/caseFile/all"/>">Ver
                                        todos</a>
                                </div>
                                <span class="d-block">Permisos sanitarios registados para este establecimiento</span>
                            </div>
                        </div>

                    </div>

                    <button type="button" class="btn btn-outline-secondary btn-lg btn-block goback"><i class="fa fa-reply"></i> Atras</button>
                </div>
            </div>
        </div>
    </div>
</main>