<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="id_owner" type="java.lang.Long"--%>
<%--@elvariable id="establishment" type="edu.ues.aps.process.area.establishment.model.Establishment"--%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/owner/index"/>">Inicio</a></li>
                    <c:if test="${empty id_owner}">
                        <li class="breadcrumb-item"><a href="<c:url value="/establishment/list"/>">Establecimientos</a>
                        </li>
                    </c:if>
                    <c:if test="${not empty id_owner}">
                        <li class="breadcrumb-item"><a href="<c:url value="/owner/${id_owner}/establishment"/>">Establecimientos</a>
                        </li>
                    </c:if>
                    <li class="breadcrumb-item active">Formulario de establecimiento</li>
                </ol>
            </nav>

            <div class="information-panel">
                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                    <strong>Recomendaci�n</strong> Ingrese los datos completos del establecimiento para una mejor
                    sistematizaci�n del proceso.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <div class="card">
                <div class="card-body">

                    <h5 class="card-title">Formulario de establecimiento</h5>
                    <h6 class="card-subtitle mb-4 text-muted">Complete los datos requeridos del establecimiento</h6>
                    <hr class="my-4">

                    <form:form id="main_new" method="POST" modelAttribute="establishment">

                        <form:hidden path="id"/>
                        <form:hidden path="address.id"/>
                        <form:hidden path="owner.id"/>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Nombre del establecimiento</label>
                            <div class="col-8">
                                <form:input
                                        class="form-control"
                                        type="text"
                                        path="name"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Tipo de establecimiento (escrito por el usuario)</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="text"
                                        path="type_detail"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Rubro</label>
                            <div class="col-4">
                                    <%--@elvariable id="sections" type="java.util.List<edu.ues.aps.process.base.dto.MapDTO>"--%>
                                <form:select
                                        class="custom-select sections-select-list"
                                        path="type.section.id"
                                        items="${sections}"
                                        itemLabel="value"
                                        itemValue="id"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Especificaci�n del tipo de establecimiento</label>
                            <div class="col-4">
                                    <%--@elvariable id="establishment_types" type="java.util.List<edu.ues.aps.process.base.dto.MapDTO>"--%>
                                <form:select
                                        class="custom-select establishment-types-select-list initialized-establishment-types-select-list"
                                        path="type.id"
                                        items="${establishment_types}"
                                        itemValue="id"
                                        itemLabel="value"/>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-2 col-form-label">Direcci�n</label>
                            <div class="col-8">
                                <form:input
                                        id="address_details"
                                        class="form-control"
                                        type="text"
                                        path="address.details"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Departamento</label>
                            <div class="col-4">
                                    <%--@elvariable id="departments" type="java.util.List<edu.ues.aps.process.base.dto.MapDTO>"--%>
                                <form:select
                                        class="custom-select department-select-list"
                                        path="address.municipality.department.id"
                                        items="${departments}"
                                        itemLabel="value"
                                        itemValue="id"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Municipio</label>
                            <div class="col-4">
                                    <%--@elvariable id="municipalities" type="java.util.List<edu.ues.aps.process.base.dto.MapDTO>"--%>
                                <form:select
                                        class="custom-select municipality-select-list initialized-municipality-select-list"
                                        path="address.municipality.id"
                                        items="${municipalities}"
                                        itemValue="id"
                                        itemLabel="value"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Inscrito al RC</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="text"
                                        path="commercial_register"/>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-2 col-form-label">Capital</label>
                            <div class="input-group col-4">
                                <div class="input-group-prepend">
                                    <span class="input-group-text">$</span>
                                </div>
                                <form:input
                                        type="text"
                                        class="form-control"
                                        aria-label="Amount (to the nearest dollar)"
                                        path="capital"/>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-2 col-form-label">NIT de la empresa</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="text"
                                        path="NIT"
                                        placeholder="0000-000000-000-0"/>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-2 col-form-label">Tel�fono</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="tel"
                                        path="telephone"
                                        placeholder="0000-0000, (000) 000-0000"/>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-2 col-form-label">Tel�fono 2</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="tel"
                                        path="telephone_extra"
                                        placeholder="0000-0000, (000) 000-0000"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">FAX</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="text"
                                        path="FAX"
                                        placeholder="0000-0000, (000) 000-0000"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Correo Electr�nico</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="email"
                                        path="email"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Horario de servicio</label>
                            <div class="col-6">
                                <form:input
                                        class="form-control"
                                        type="text"
                                        path="operations"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">N�mero de empleados masculinos</label>
                            <div class="col-2">
                                <form:input
                                        class="form-control employee-number"
                                        type="number"
                                        id="empM"
                                        path="maleEmployeeCount"/>
                            </div>


                            <label class="col-2 col-form-label">N�mero de empleados femeninos</label>
                            <div class="col-2">
                                <form:input
                                        class="form-control employee-number"
                                        type="number"
                                        id="empF"
                                        path="femaleEmployeeCount"/>
                            </div>

                            <label for="totalEmp" class="col-2 col-form-label">Total de empleados</label>
                            <div class="col-2">
                                <input class="form-control total-employee-number" type="text" id="totalEmp" min="0"
                                       readonly>
                            </div>
                        </div>

                        <div class="form-group row ml-3">
                            <div class="custom-control custom-checkbox">
                                <form:checkbox class="custom-control-input" id="active" path="active" checked="true"/>
                                <label class="custom-control-label" for="active">Establecimiento actualmente funcionando</label>
                            </div>
                        </div>
                        <hr class="my-4">
                        <div class="mt-3">
                            <button type="submit" class="btn btn-primary">Guardar</button>
                            <c:if test="${empty id_owner}">
                                <a role="button" class="btn btn-secondary"
                                   href="<c:url value='/establishment/list'/>">Cancelar</a>
                            </c:if>
                            <c:if test="${not empty id_owner}">
                                <a role="button" class="btn btn-secondary"
                                   href="<c:url value='/owner/${id_owner}/establishment'/>">Cancelar</a>
                            </c:if>
                        </div>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>