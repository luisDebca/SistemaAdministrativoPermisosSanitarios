<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--@elvariable id="id_owner" type="java.lang.Long"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/owner/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/owner/${id_owner}"/>'>Propietario</a></li>
                    <li class="breadcrumb-item">Establecimientos</li>
                </ol>
            </nav>

            <div class="information-panel">
                <div class="alert alert-info alert-dismissible fade show" role="alert">
                    �<strong>C�mo ver los expedientes</strong> del establecimiento?: Click en el icono <i
                        class="fa fa-book fa-2x"></i> para ver todos los expedientes registrados del establecimiento.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            </div>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h5 class="card-title">Lista de establecimientos registrados propiedad</h5>
                            <h6><a href="<c:url value="/owner/${id_owner}"/>" id="owner_name"></a></h6>
                            <div class="card-subtitle mb-2 text-muted small extra_info">
                            </div>
                            <div class="owner_id" hidden="hidden">${id_owner}</div>
                        </div>
                        <div class="col-6 text-right">
                            <a href="<c:url value="/owner/${id_owner}/establishment/new"/>" role="button"
                               class="btn btn-outline-primary mt-3"><i class="fa fa-plus"></i> Agregar establecimiento</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-sm table-hover" id="dataTable" width="100%" cellspacing="0">
                    <thead>
                    <tr>
                        <th>Nombre del establecimiento</th>
                        <th>Direcci�n del establecimiento</th>
                        <th>Tipo de establecimiento</th>
                        <th>Propietario</th>
                        <th>Contactar</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="establishments" type="java.util.List<edu.ues.aps.process.area.establishment.dto.EstablishmentDTO>"--%>
                    <c:forEach items="${establishments}" var="item">
                        <tr>
                            <td><a href="<c:url value="/establishment/${item.establishment_id}/info"/>">${item.establishment_name}</a></td>
                            <td>${item.establishment_address}</td>
                            <td>${item.establishment_type}</td>
                            <td><a href="<c:url value="/owner/${item.owner_id}"/>">${item.establishment_owner_name}</a></td>
                            <td>
                                <ul style="list-style: none">
                                    <c:if test="${not empty item.establishment_telephone}">
                                        <li>Telefono: ${item.establishment_telephone}</li>
                                    </c:if>
                                    <c:if test="${not empty item.establishment_email}">
                                        <li>Correo Electronico: ${item.establishment_email}</li>
                                    </c:if>
                                    <c:if test="${not empty item.establishment_fax}">
                                        <li>FAX: ${item.establishment_fax}</li>
                                    </c:if>
                                </ul>
                            </td>
                            <td class="cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group">
                                        <a href="<c:url value="/establishment/${item.establishment_id}"/>"
                                           class="btn btn-outline-secondary" data-toggle="tooltip" title="Ver detalles"><i
                                                class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                                        <a href="<c:url value="/establishment/${item.establishment_id}/edit" />"
                                           class="btn btn-outline-secondary" data-toggle="tooltip" title="Editar"><i
                                                class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="btn-group" role="group">
                                        <a href="<c:url value="/establishment/${item.establishment_id}/caseFile/all"/>"
                                           class="btn btn-outline-secondary" data-toggle="tooltip"
                                           title="Ver expedientes"><i class="fa fa-book"
                                                                      aria-hidden="true"></i></a>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
