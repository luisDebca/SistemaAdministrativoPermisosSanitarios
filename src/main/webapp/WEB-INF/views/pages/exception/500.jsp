<%--@elvariable id="message" type="java.lang.String"--%>
<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<tiles:importAttribute name="stylesheets" />
<html>
<head>
    <title>Error de servidor</title>
    <c:forEach var="css" items="${stylesheets}">
        <link rel="stylesheet" type="text/css" href='<c:url value="${css}"/>'>
    </c:forEach>
</head>
<body style="position:absolute;width: 100%;top: 25%;">
<div class="container-fluid">
    <div class="row">
        <div class="col-10 mx-auto">
            <div class="card bg-danger text-white">
                <div class="card-body text-center">
                    <i class="fa fa-warning fa-4x" aria-hidden="true"></i>
                    <h1 class="card-title">500</h1>
                    <div class="card-text">A ocurrido un error inesperado. De continuar este error contacte con el equipo de mantenimiento.</div>
                    <small class="text-white">ERROR: ${message}</small>
                    <br>

                    <a href="<c:url value="/index"/>" role="button" class="btn btn-light mt-3">Inicio</a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>