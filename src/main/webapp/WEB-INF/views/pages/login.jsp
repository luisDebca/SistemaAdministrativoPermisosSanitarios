 <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://tiles.apache.org/tags-tiles" prefix="tiles"%>

<tiles:importAttribute name="javascript" />
<tiles:importAttribute name="stylesheets" />

<!DOCTYPE html>
<html lang="es">
<head>
    <meta charset="ISO-8859-1">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="APS MINSAL">
    <meta name="author" content="">
    <title>APS-Ingreso</title>

    <c:forEach items="${stylesheets}" var="css">
        <link rel="stylesheet" type="text/css" href='<c:url value="${css}"/>'>
    </c:forEach>

</head>
    <body class="text-center">
    <form class="form-signin" action="${loginUrl}" method="post">
        <h1 class="h3 mb-3 font-weight-normal">APS</h1>
        <c:if test="${param.error != null}">
            <div class="alert alert-danger">
                <p>Nombre de usuario o contrase�a invalida.</p>
            </div>
        </c:if>
        <c:if test="${param.logout != null}">
            <div class="alert alert-success">
                <p>Su sesi�n se ha cerrrado correctamente.</p>
            </div>
        </c:if>
        <label for="username" class="sr-only">Nombre del usuario</label>
        <input type="text" id="username" name="username"  class="form-control" placeholder="Nombre de usuario" required autofocus>
        <label for="password" class="sr-only">Password</label>
        <input type="password" id="password" name="password" class="form-control mt-3" placeholder="Contrase�a" required>
        <div class="checkbox mb-3">
            <label>
                <input type="checkbox" id="rememberme" name="remember-me"> Recordarme
            </label>
            <input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
        </div>
        <button class="btn btn-lg btn-primary btn-block" type="submit">Ingresar</button>
        <p class="mt-5 mb-3 text-muted">&copy;Versi�n 2.0 2017-2018</p>
    </form>
    <c:forEach items="${javascript}" var="script">
        <script type="text/javascript" src='<c:url value="${script}"/>'></script>
    </c:forEach>

    </body>
</html>