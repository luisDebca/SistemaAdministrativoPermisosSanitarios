<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%--@elvariable id="owner" type="edu.ues.aps.process.base.model.LegalEntity"--%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/owner/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/owner/legal/${owner.id}"/>'>Propietario</a></li>
                    <li class="breadcrumb-item">Productos</li>
                </ol>
            </nav>

            <div class="alert alert-info alert-dismissible fade show" role="alert">
                <strong>Para agregar productos</strong>
                <ol>
                    <li>Click en 'Agregar otro producto'</li>
                    <li>Ingrese los datos del producto</li>
                    <li>Seleccione guardar</li>
                </ol>
                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>

            <form:form modelAttribute="owner" method="POST">
                <div class="row">
                    <div class="col">
                        <div class="card">
                            <div class="card-body">
                                <form:hidden path="id"/>
                                <form:hidden path="email"/>
                                <form:hidden path="telephone"/>
                                <form:hidden path="FAX"/>
                                <form:hidden path="name"/>
                                <form:hidden path="nit"/>
                                <table class="table table-borderless table-sm" id="registedProducts">
                                    <thead>
                                    <tr>
                                        <th hidden="hidden"></th>
                                        <th>Tipo de producto que elabora</th>
                                        <th>Nombre del producto</th>
                                        <th>Registro sanitario</th>
                                    </tr>
                                    </thead>
                                    <tbody id="product-list">
                                    <c:if test="${empty owner.products}">
                                        <tr class="table-info">
                                            <td colspan="3">No se tienen ning�n registro de productos para estas
                                                oficinas centrales
                                            </td>
                                        </tr>
                                    </c:if>
                                    <c:forEach items="${owner.products}" var="item" varStatus="loop">
                                        <tr class="productRow">
                                            <td hidden="hidden" class="id_product"><form:hidden
                                                    path="products[${loop.index}].id"/></td>
                                            <td><form:input class="form-control" path="products[${loop.index}].type"
                                                            type="text" readonly="true"/></td>
                                            <td><form:input class="form-control" path="products[${loop.index}].name"
                                                            type="text" readonly="true"/></td>
                                            <td><form:input class="form-control" path="products[${loop.index}].sanitary"
                                                            type="text" readonly="true"/></td>
                                            <td>
                                                <div class="btn-group" role="group" aria-label="Options">
                                                    <a class="btn btn-outline-primary disabled_href on-click-remove-disable-btn"
                                                       href="#" role="button" data-toggle="tooltip"
                                                       data-placement="right" title="Editar producto"><i
                                                            class="fa fa-pencil-square-o" aria-hidden="true"></i></a>
                                                    <a class="btn btn-outline-danger disabled_href on-click-confirm-delete-btn"
                                                       href="#" role="button" data-toggle="tooltip"
                                                       data-placement="right" title="Eliminar producto"><i
                                                            class="fa fa-trash-o" aria-hidden="true"></i></a>
                                                </div>
                                            </td>
                                        </tr>
                                    </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    <div class="col">
                        <div class="card">
                            <div class="card-body" id="product-new-card-list">
                                <div id="table-form-product"></div>
                                <input id="productsFormSubmitBtn" class="btn btn-outline-primary my-1 mx-1"
                                       type="submit"
                                       value="Guardar" hidden="hidden"/>
                                <button id="firstAddedProductBtn" type="button"
                                        class="btn btn-outline-primary my-1 mx-1">Agregar
                                    nuevo producto
                                </button>
                                <a href="<c:url value="/owner/${owner.id}"/>" role="button"
                                   class="btn btn-outline-secondary">Cancelar</a>
                            </div>
                        </div>
                    </div>
                </div>
            </form:form>

        </div>
    </div>
</main>

<div class="modal fade" id="delete-confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Eliminar producto</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i>
                <h5>�Accepta eliminar el producto seleccionado?</h5>
                <input id="selected_product_id" class="form-control" hidden="hidden">
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button type="button" id="delete-product-btn" class="btn btn-primary">Aceptar</button>
            </div>
        </div>
    </div>
</div>