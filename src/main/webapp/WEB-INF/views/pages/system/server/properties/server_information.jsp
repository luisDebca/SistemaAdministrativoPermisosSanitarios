<%--@elvariable id="storage" type="edu.ues.aps.system.server.properties.controller.ServerStorageInformationWrapper"--%>
<%--@elvariable id="properties" type="java.util.Properties"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
                <div class="lh-100">
                    <h4 class="mb-0 text-white lh-100">INFORMACI�N DE SERVIDOR</h4>
                    <small>APS Version 2.0</small>
                </div>
            </div>

            <h5 class="text-muted">Uso de la Memoria</h5>
            <div id="real_time_chart" class="flot-chart"></div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h6 class="border-bottom border-gray pb-2 mb-0">Estado del Sistema</h6>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Espacio total</strong>
                        ${storage.total_space_megabytes} MB
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Espacio libre</strong>
                        ${storage.free_space_megabytes} MB
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Espacio usable</strong>
                        ${storage.usable_space_megabytes} MB
                    </p>
                </div>
                <small class="d-block text-right mt-3">
                    <a href="#" class="disabled_href" id="shutdown_server_btn">Apagar servidor</a>
                </small>
            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h6 class="border-bottom border-gray pb-2 mb-0">Propieades del Sistema</h6>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Operating system name</strong>
                        ${properties.get("os.name")}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Operating system version</strong>
                        ${properties.get("os.version")}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Operating system architecture</strong>
                        ${properties.get("os.arch")}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">User working directory</strong>
                        ${properties.get("user.dir")}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">User home directory</strong>
                        ${properties.get("user.home")}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">User account name</strong>
                        ${properties.get("user.name")}
                    </p>
                </div>
            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h6 class="border-bottom border-gray pb-2 mb-0">Propieades JAVA</h6>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Java Version</strong>
                        ${properties.get("java.version")}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Java home directory</strong>
                        ${properties.get("java.home")}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Java class path</strong>
                        ${properties.get("java.class.path")}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 small lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Java Vendor</strong>
                        ${properties.get("java.vendor.url")}
                    </p>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="shutdown_server_confirmation_modal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content text-center">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Apagar Servidor</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <form>
                <div class="modal-body">
                    <i class="fa fa-exclamation-triangle fa-2x" aria-hidden="true"></i>
                    <p>El servidor cerrara la aplicaci�n y se apagara, el trabajo que no se ha guardado se perdera</p>
                    <p>�Comfirma apagar el servidor?</p>

                    <div class="form-group">
                        <label for="system_password">System Password</label>
                        <input type="password" class="form-control" id="system_password" placeholder="Password">
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                    <button id="shutdown_btn" type="button" class="btn btn-primary">Confirmar</button>
                </div>
            </form>
        </div>
    </div>
</div>