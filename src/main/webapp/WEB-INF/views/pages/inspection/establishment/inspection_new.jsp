<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%--@elvariable id="id_tracing" type="java.lang.Long"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/tracing/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/tracing/establishment/all"/>">Seguimiento</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/tracing/${id_tracing}/inspection/all"/>">Inspecciones</a></li>
                    <li class="breadcrumb-item active">Inspecci�n</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">
                    <%--@elvariable id="inspection" type="edu.ues.aps.process.tracing.base.model.AbstractInspection"--%>
                    <form:form method="post" modelAttribute="inspection" id="main">

                        <h5 class="card-title">Registro de Inspecci�n</h5>
                        <h6 class="card-subtitle mb-2 text-muted">Ingrese los datos requeridos para la inspecci�n.</h6>
                        <hr class="my-4">

                        <form:hidden path="id"/>

                        <div class="form-group row">
                            <label for="memorandum" class="col-sm-2 col-form-label">N�mero de memor�ndum</label>
                            <div class="col-sm-4">
                                <form:input type="text" class="form-control" id="memorandum" path="memorandum_number" val=""/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="memorandum" class="col-sm-2 col-form-label">Nombre del t�cnico</label>
                            <div class="col-sm-6">
                                <form:input type="text" class="form-control" id="technicalname" path="technical_name"/>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="memorandum" class="col-sm-2 col-form-label">Cargo del t�cnico</label>
                            <div class="col-sm-6">
                                <form:input type="text" class="form-control" id="technicalcharge"
                                            path="technical_charge"/>
                            </div>
                        </div>


                        <div class="form-group row">
                            <label for="memorandum" class="col-sm-2 col-form-label">Observaci�n</label>
                            <div class="col-sm-10">
                                <form:textarea class="form-control" id="observation" rows="3" path="observation"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-sm-2 col-form-label">Resultado de la inspecci�n</label>
                            <div class="col-sm-5">
                                <form:select class="custom-select" path="result_type">
                                    <form:option value="INVALID" selected="true"
                                                 disabled="true">Seleccione una opci�n</form:option>
                                    <form:option value="FAVORABLE">Favorable</form:option>
                                    <form:option value="RECOMMENDATIONS">Recomendaci�n</form:option>
                                    <form:option value="UNDELIVERED">Sin informe</form:option>
                                </form:select>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="memorandum" class="col-sm-2 col-form-label">Nota obtenida (%)</label>
                            <div class="col-sm-2 input-group">
                                <form:input type="number" class="form-control" id="score" path="score" min="0"
                                            max="100"/>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon2">%</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="memorandum" class="col-sm-2 col-form-label">Prorroga recibida (en dias)</label>
                            <div class="col-sm-4 input-group">
                                <form:input type="number" class="form-control" id="extension" path="extension_received"
                                            min="0" max="100"/>
                                <div class="input-group-append">
                                    <span class="input-group-text" id="basic-addon3">Dias</span>
                                </div>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="memorandum" class="col-sm-2 col-form-label">Fecha de realizaci�n de
                                inspecci�n</label>
                            <div class="col-sm-5">
                                <form:input type="text" class="form-control date" id="inspection-date"
                                            path="inspected_on" autocomplete="off"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="memorandum" class="col-sm-2 col-form-label">Fecha de recibido por SIBASI</label>
                            <div class="col-sm-5">
                                <form:input type="text" class="form-control date" id="sibasi_date"
                                            path="sibasi_received" autocomplete="off"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <hr class="my-4">
                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="<c:url value="/tracing/${id_tracing}/inspection/all"/>" role="button" class="btn btn-secondary">Cancelar</a>

                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>