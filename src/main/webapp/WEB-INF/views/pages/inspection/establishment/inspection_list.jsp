<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="id_tracing" type="java.lang.Long"--%>
<%--@elvariable id="inspections" type="java.util.List<edu.ues.aps.process.tracing.base.dto.AbstractInspectionDTO>"--%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/tracing/establishment/all"/>">Registro de
                        inspecciones</a></li>
                    <li class="breadcrumb-item active">Inspecciones</li>
                </ol>
            </nav>

            <c:if test="${inspections.size() >= 4}">
                <div class="alert alert-danger" role="alert">
                    El n�mero maximo de reinspecciones permitidas es de 3, no se pueden agregar m�s reinspecciones.
                </div>
            </c:if>

            <div class="information-panel"></div>

            <div class="card mb-3">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h5 class="card-title">Registro de inspecciones</h5>
                            <div id="extra-info" class="text-muted">Inspecciones realizadas por la UCSF.</div>
                            <div id="id_caseFile" hidden="hidden">${id_tracing}</div>
                        </div>
                        <div class="col-6 text-right">
                            <div class="btn-group-vertical">
                                <div class="btn-group" role="group" aria-label="options">
                                    <c:if test="${inspections.size() < 4}">
                                        <a class="btn btn-outline-secondary"
                                           href="<c:url value="/tracing/${id_tracing}/inspection/new"/>"
                                           role="button"><i class="fa fa-plus"></i> Nueva inspecci�n</a>
                                    </c:if>
                                    <c:if test="${inspections.size() > 0}">
                                        <button id="inspection-report-btn" type="button"
                                                class="btn btn-outline-secondary"><i class="fa fa-file-pdf-o"></i> Hoja
                                            de revisiones
                                        </button>
                                    </c:if>
                                </div>
                                <div class="btn-group" role="group" aria-label="options">
                                    <a class="btn btn-outline-secondary"
                                       href="<c:url value="/tracing/establishment/all"/>" role="button"><i
                                            class="fa fa-reply"></i> Volver</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover establishment-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th>Inspecci�n</th>
                        <th>Resultado de la inspecci�n</th>
                        <th>Inspecci�n realizada</th>
                        <th>Recibido por SIBASI</th>
                        <th>Plazo otorgado (Dias)</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <c:forEach items="${inspections}" var="item" varStatus="loop">
                        <tr>
                            <td hidden="hidden" class="id_inspection">${item.id_inspection}</td>
                            <td>
                                <c:if test="${loop.first}">
                                    <span class="badge badge-primary">Inspecci�n</span>
                                </c:if>
                                <c:if test="${loop.index > 0 and loop.index <= 2 }">
                                    <span class="badge badge-warning">Reinspecci�n ${loop.index}</span>
                                </c:if>
                                <c:if test="${loop.index == 3}">
                                    <span class="badge badge-danger">Reinspecci�n ${loop.index}</span>
                                </c:if>
                            </td>
                            <td>${item.inspection_result_type}</td>
                            <td>${item.inspection_inspected_on}</td>
                            <td>${item.inspection_sibasi_received}</td>
                            <td>${item.inspection_extension_received}</td>
                            <td class="cell-center-middle">
                                <div class="btn-group btn-options">
                                    <a href="#" class="btn btn-outline-secondary disabled_href show-inspection-information-modal" role="button" data-toggle="tooltip"
                                       data-placement="top" title="Ver detalles"><i class="fa fa-folder-open-o"
                                                                                    aria-hidden="true"></i></a>
                                    <a href="<c:url value="/tracing/${id_tracing}/inspection/${item.id_inspection}/edit"/> "
                                       class="btn btn-outline-secondary" role="button" data-toggle="tooltip"
                                       data-placement="top" title="Editar inspecci�n"><i class="fa fa-pencil"
                                                                                         aria-hidden="true"></i></a>
                                    <a href="<c:url value="/tracing/${id_tracing}/inspection/${item.id_inspection}/delete"/> "
                                       class="btn btn-outline-secondary" role="button" data-toggle="tooltip"
                                       data-placement="top" title="Eliminar inspecci�n"><i class="fa fa-trash-o"
                                                                                           aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>
<!-- Registed user selector modal -->
<div class="modal fade" id="user-selector-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="user-selector-title">Generar reporte</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    <label for="user_list">T�cnico responsable de elaborar la resoluci�n</label>
                    <select class="custom-select" id="user_list"></select>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="generate-pdf-btn" type="button" class="btn btn-primary">Generar PDF</button>
            </div>
        </div>
    </div>
</div>

<!-- INSPECTION INFORMATION MODAL -->
<div class="modal fade" id="inspection-information-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="inspection-info-title">Informaci�n de inspecci�n</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <dl class="row">
                    <dt class="col-sm-3 mb-3">N�mero de memorandum</dt>
                    <dd class="col-sm-9 mb-3" id="i-memo"></dd>

                    <dt class="col-sm-3 mb-3">Fecha de inspecci�n</dt>
                    <dd class="col-sm-9 mb-3" id="i-inspection-date"></dd>

                    <dt class="col-sm-3 mb-3">Fecha de recibido por SIBASI</dt>
                    <dd class="col-sm-9 mb-3" id="i-sibasi-date"></dd>

                    <dt class="col-sm-3 mb-3">Resultado de inspecci�n</dt>
                    <dd class="col-sm-9 mb-3" id="i-result"></dd>

                    <dt class="col-sm-3 mb-3">Nota de inspecci�n</dt>
                    <dd class="col-sm-9 mb-3" id="i-score"></dd>

                    <dt class="col-sm-3 mb-3">Dias extras recibidos</dt>
                    <dd class="col-sm-9 mb-3" id="i-extension"></dd>

                    <dt class="col-sm-3 mb-3">Nombre de inspector</dt>
                    <dd class="col-sm-9 mb-3" id="i-technical"></dd>

                    <dt class="col-sm-3 mb-3">Cargo del inspector</dt>
                    <dd class="col-sm-9 mb-3" id="i-charge"></dd>

                    <dt class="col-sm-3 mb-3">Observaci�n</dt>
                    <dd class="col-sm-9 mb-3" id="i-observation"></dd>
                </dl>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>