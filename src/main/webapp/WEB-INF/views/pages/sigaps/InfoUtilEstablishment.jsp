<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
		<div class="container-fluid">
        	<div class="row">
				<div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary"><h3><i class="fa fa-line-chart" aria-hidden="true"></i> Info Util Establecimientos</h3></div>
			</div>
			<div class="row">
				<div class="col-8 m-auto">
					<div class="card mb-3 bg-secondary">
						<div class="card-body">
							<div class="card">
								<div class="card-body">
								    <h5 class="card-title">Reporte de Info Util</h5>
									<h6 class="card-subtitle mb-2 text-muted">Seleccione los parametros del reporte</h6>
									<hr class="my-4">
        						<form action="infoutilestablishmentexcel" method="GET">
        							<div class="form-group row">
											<label for="until-date" class="col-sm-2 col-form-label">Fecha a consultar</label>
											<div class="col-sm-4">
												<input type="text" class="form-control date" id="until-date" name="until" autocomplete="off" required>								
											</div>
										</div>
										
										<div class="form-group row">
											<label for="state" class="col-sm-2 col-form-label">Estado del permiso</label>
											<div class="col-sm-5">
												<select required="required" id="state" name="state" class="custom-select">
													<option value="" selected="selected" disabled="disabled">Seleccione una opci�n</option>
													<option value="VALID">Vigentes</option>
													<option value="INVALID">Vencidos</option>
													<option value="IN_PROCESS">En Proceso</option>
												</select>
											</div>
										</div>
        								
        								<div class="form-group row">
											<label for="state" class="col-sm-2 col-form-label">Categoria</label>
											<div class="col-sm-5">
												<select required="required" id="category" name="category" class="form-control">
												   <option value="0">TODAS</option>
												   <option value="1">Bodega</option>
												   <option value="2">Caf�/Cafeteria/Cafet�n</option>
												   <option value="3">Cafetin Escolar/Chalet Escolar/Cafetin Universitario</option>
												   <option value="4">Carniceria</option>
												   <option value="5">Chalet/Kiosko</option>
												   <option value="6">Comedor</option>
												   <option value="7">Envasadora/Empacadora</option>
												   <option value="8">Fabrica/Planta/Procesadora/Beneficio/Molienda</option>
												   <option value="9">Hotel</option>
												   <option value="10">Molino</option>
												   <option value="11">Otros Establecimiento</option>
												   <option value="12">Panaderia/Pasteleria</option>
												   <option value="13">Pupuseria</option>
												   <option value="14">Restaurante/Bar</option>
												   <option value="15">Sala de venta</option>
												   <option value="16">Supermercado/Tienda/Tienda de conveniencia</option>
												</select>
											</div>
										</div>
										
										<hr class="my-4">
										<div class="btn-group float-right" role="group" aria-label="Basic example">
											<button type="submit" class="btn btn-primary"><i class="fa fa-file-excel-o"></i> Generar excel</button>
											<button type="reset" class="btn btn-secondary"><i class="fa fa-eraser" aria-hidden="true"></i> Limpiar</button>
										</div>
        							        																										
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
        </div>
    </div>
</main>