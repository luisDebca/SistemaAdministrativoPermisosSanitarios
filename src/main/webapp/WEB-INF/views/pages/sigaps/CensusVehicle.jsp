<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
	<div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
		<div class="container-fluid">
			<div class="row">
				<div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary"><h3><i class="fa fa-area-chart" aria-hidden="true"></i> Censos VehÝculos</h3></div>
			</div>

			<div class="row">
				<div class="col-8 m-auto">
					<div class="card mb-3 bg-secondary">
						<div class="card-body">
							<div class="card">
								<div class="card-body">

									<h5 class="card-title">Reporte de censos</h5>
									<h6 class="card-subtitle mb-2 text-muted">Seleccione los parametros del reporte</h6>
									<hr class="my-4">
									<form id="main" action="censusvehicleexcel" method="GET">

										<div class="form-group row">
											<label for="until-date" class="col-sm-2 col-form-label">Fecha a consultar</label>
											<div class="col-sm-4">
												<input type="text" class="form-control date" id="until-date" name="until" autocomplete="off" required>
											</div>
										</div>

										<hr class="my-4">
										<div class="btn-group float-right" role="group" aria-label="Basic example">
											<button type="submit" class="btn btn-primary"><i class="fa fa-file-excel-o"></i> Generar excel</button>
											<button type="reset" class="btn btn-secondary"><i class="fa fa-eraser" aria-hidden="true"></i> Limpiar</button>
										</div>
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</main>

