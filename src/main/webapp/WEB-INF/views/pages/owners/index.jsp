<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row">
                <div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary">
                    <h3><i class="fa fa-user-o" aria-hidden="true"></i> Propietarios</h3></div>
            </div>

            <div class="alert alert-primary" role="alert">
                <i class="fa fa-info-circle" aria-hidden="true"></i> Si la busqueda no tiene resultados puede ver <a
                    href="<c:url value="/owner/list"/>">"Todos los propietarios aqui"</a>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-user-o"></i> Buscar propietario</h5>
                    <p class="card-text">Ingrese el nombre, direcci�n o NIT del propietario para la busqueda</p>
                    <div class="input-group mb-3">
                        <div class="input-group-prepend">
                            <span class="input-group-text" id="basic-addon1"><i class="fa fa-search"
                                                                                aria-hidden="true"></i></span>
                        </div>
                        <input id="owner-finder-input" type="text" class="form-control" placeholder="Buscar..."
                               aria-label="search" aria-describedby="basic-addon1">
                    </div>
                    <div id="search_result" class="md-2 p-2"></div>
                    <button type="button" class="btn btn-outline-primary show-new-owner-type-selector-modal-btn"><i class="fa fa-plus"></i> Nuevo
                        Propietario
                    </button>
                    <a role="button" href="<c:url value="/owner/list"/>" class="btn btn-outline-primary"><i class="fa fa-list-ol"></i> Ver todos</a>
                    <a role="button" href="<c:url value="/client/all"/>" class="btn btn-outline-secondary"><i class="fa fa-list-ul"></i> Usuarios registrados</a>
                </div>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="owner-type-confirmation-modal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Tipo de propietario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <a id="owner-natural-select-btn" href="<c:url value="/owner/natural/new"/>"
                   class="btn btn-outline-secondary btn-lg btn-block">
                    <i class="fa fa-user-o fa-lg"></i> Persona natural</a>
                <br>
                <br>
                <a id="owner-legal-select-btn" href="<c:url value="/owner/legal/new"/>"
                   class="btn btn-outline-secondary btn-lg btn-block">
                    <i class="fa fa-building-o fa-lg"></i> Persona Jur�dica</a>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
            </div>
        </div>
    </div>
</div>
