<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/owner/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/owner/list"/>">Propietarios</a></li>
                    <li class="breadcrumb-item active">Editar Propietario</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">

                    <h5 class="card-title">Editar propietario</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Ingrese los datos requeridos siguientes.</h6>
                    <hr class="my-4">
                    <%--@elvariable id="owner" type="edu.ues.aps.process.base.model.LegalEntity"--%>
                    <form:form modelAttribute="owner" method="POST" id="main">

                    <form:hidden path="id"/>
                    <form:hidden path="address.id"/>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Nombre</label>
                        <div class="col-8">
                            <form:input class="form-control" type="text" path="name"/>
                        </div>
                        <div class="required-type" data-toggle="tooltip" data-placement="top"
                             title="Este campo es obligatorio">*
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Direcci�n</label>
                        <div class="col-6">
                            <form:input class="form-control" type="text" path="address.details"/>
                        </div>
                        <div class="required-type" data-toggle="tooltip" data-placement="top"
                             title="Este campo es obligatorio">*
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Departamento</label>
                        <div class="col-4">
                                <%--@elvariable id="departments" type="java.util.List"--%>
                            <form:select class="custom-select department-select-list"
                                         path="address.municipality.department.id"
                                         items="${departments}"
                                         itemLabel="value"
                                         itemValue="id"/>
                        </div>
                        <div class="required-type" data-toggle="tooltip" data-placement="top"
                             title="Este campo es obligatorio">*
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Municipio</label>
                        <div class="col-4">
                                <%--@elvariable id="municipalities" type="java.util.List"--%>
                            <form:select
                                    class="custom-select municipality-select-list initialized-municipality-select-list"
                                    path="address.municipality.id"
                                    items="${municipalities}"
                                    itemLabel="value"
                                    itemValue="id"/>
                        </div>
                        <div class="required-type" data-toggle="tooltip" data-placement="top"
                             title="Este campo es obligatorio">*
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">NIT</label>
                        <div class="col-4">
                            <form:input class="form-control" type="text" path="nit"/>
                        </div>
                        <div class="required-type" data-toggle="tooltip" data-placement="top"
                             title="Este campo es obligatorio">*
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">N�mero de tel�fono</label>
                        <div class="col-4">
                            <form:input class="form-control" type="text" path="telephone"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">FAX</label>
                        <div class="col-4">
                            <form:input class="form-control" type="text" path="FAX"/>
                        </div>
                    </div>

                    <div class="form-group row">
                        <label class="col-2 col-form-label">Correo Electr�nico</label>
                        <div class="col-6">
                            <form:input class="form-control" type="email" path="email"/>
                        </div>
                    </div>

                    <hr class="my-4">
                    <button type="submit" class="btn btn-primary active" aria-pressed="true">Guardar</button>
                    <a href="<c:url value="/owner/index"/>" class="btn btn-secondary active" aria-pressed="true">Cancelar</a>
                </div>
                </form:form>
            </div>
        </div>
    </div>
</main>