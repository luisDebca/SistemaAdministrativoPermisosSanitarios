<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value = "/owner/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item active">Propietarios</li>
                </ol>
            </nav>

            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th>Nombre</th>
                        <th>NIT</th>
                        <th>Contacto</th>
                        <th>Establecimientos</th>
                        <th>Veh�culos</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="owners" type="java.util.List<edu.ues.aps.process.base.dto.AbstractOwnerEntityDTO>"--%>
                    <c:forEach items="${owners}" var="item">
                        <tr>
                            <td hidden="hidden" class="id_owner">${item.id_owner}</td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td><c:if test="${empty item.owner_nit}">No ingresado</c:if> ${item.owner_nit}</td>
                            <td><c:if
                                    test="${empty item.owner_email and empty item.owner_fax and empty item.owner_telephone}">No se tienen contactos</c:if>
                                <ul style="list-style: none">
                                    <c:if test="${not empty item.owner_telephone}">
                                        <li>Tel�fono: ${item.owner_telephone}</li>
                                    </c:if>
                                    <c:if test="${not empty item.owner_fax}">
                                        <li>FAX: ${item.owner_fax}</li>
                                    </c:if>
                                    <c:if test="${not empty item.owner_email}">
                                        <li>Email: ${item.owner_email}</li>
                                    </c:if>
                                </ul>
                            </td>
                            <td class="cell-center-middle"><h3>${item.establishmentCount}</h3></td>
                            <td class="cell-center-middle"><h3>${item.vehicleCount}</h3></td>
                            <td class="cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="owner_options">
                                        <c:if test="${item.class_type == 'LegalEntity'}">
                                            <a href="<c:url value="/owner/legal/${item.id_owner}"/>" role="button"
                                               class="btn btn-outline-secondary" data-toggle="tooltip"
                                               data-placement="top" title="Ver detalles de propietario"><i
                                                    class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                                            <a href="<c:url value="/owner/legal/${item.id_owner}/edit"/>" role="button"
                                               class="btn btn-outline-secondary" data-toggle="tooltip"
                                               data-placement="top" title="Editar propietario"><i class="fa fa-pencil"
                                                                                                  aria-hidden="true"></i></a>
                                        </c:if>
                                        <c:if test="${item.class_type == 'NaturalEntity'}">
                                            <a href="<c:url value="/owner/natural/${item.id_owner}"/>" role="button"
                                               class="btn btn-outline-secondary" data-toggle="tooltip"
                                               data-placement="top" title="Ver detalles de propietario"><i
                                                    class="fa fa-asterisk" aria-hidden="true"></i></a>
                                            <a href="<c:url value="/owner/natural/${item.id_owner}/edit"/>"
                                               role="button" class="btn btn-outline-secondary" data-toggle="tooltip"
                                               data-placement="top" title="Editar propietario"><i class="fa fa-pencil"
                                                                                                  aria-hidden="true"></i></a>
                                        </c:if>
                                        <a href="#" role="button" class="btn btn-outline-secondary"
                                           data-toggle="tooltip" data-placement="top" title="Eliminar"><i
                                                class="fa fa-trash-o" aria-hidden="true"></i></a>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="process_options">
                                        <a href="<c:url value="/owner/${item.id_owner}/establishment"/>" role="button"
                                           class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top"
                                           title="Ver establecimientos"><i class="fa fa-building-o"
                                                                           aria-hidden="true"></i></a>
                                        <a href="<c:url value="/owner/${item.id_owner}/vehicle/list"/>" role="button" class="btn btn-outline-secondary"
                                           data-toggle="tooltip" data-placement="top" title="Ver veh�culos"><i
                                                class="fa fa-truck" aria-hidden="true"></i></a>
                                        <a href="<c:url value="/owner/${item.id_owner}/client/all"/>" role="button" class="btn btn-outline-secondary"
                                           data-toggle="tooltip" data-placement="top" title="Ver usuarios"><i
                                                class="fa fa-male" aria-hidden="true"></i></a>
                                        <c:if test="${item.class_type == 'LegalEntity'}">
                                            <a href="<c:url value="/owner/${item.id_owner}/product/add"/>" role="button"
                                               class="btn btn-outline-secondary" data-toggle="tooltip"
                                               data-placement="top" title="Agregar/Editar productos"><i
                                                    class="fa fa-product-hunt" aria-hidden="true"></i></a>
                                        </c:if>
                                        <c:if test="${item.class_type == 'NaturalEntity'}">
                                            <a href="#" role="button" class="btn btn-outline-secondary"
                                               data-toggle="tooltip" data-placement="top"
                                               title="Covertir persona natural a jur�dica"><i
                                                    class="fa fa-circle-o-notch" aria-hidden="true"></i></a>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>