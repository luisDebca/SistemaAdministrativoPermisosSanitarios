<%--@elvariable id="owner" type="edu.ues.aps.process.base.dto.AbstractLegalEntityDTO"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/owner/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/owner/list"/>">Propietarios</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Informaci�n de Propietario</li>
                </ol>
            </nav>

            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-info rounded box-shadow">
                <div class="lh-100">
                    <h4 class="mb-0 text-white lh-100">Informaci�n de propietario <strong>${owner.owner_name}</strong></h4>
                </div>
            </div>

            <div class="row">
                <div class="col-lg-6">
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h6 class="border-bottom border-gray pb-2 mb-0">Informaci�n</h6>

                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Nombre</strong>
                                ${owner.owner_name}
                            </p>
                        </div>
                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Direcci�n</strong>
                                ${owner.owner_address}
                            </p>
                        </div>
                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">NIT</strong>
                                ${owner.owner_nit}
                            </p>
                        </div>
                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Tel�fono</strong>
                                ${owner.owner_telephone}
                            </p>
                        </div>
                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">FAX</strong>
                                ${owner.owner_fax}
                            </p>
                        </div>
                        <div class="media text-muted pt-3">
                            <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <strong class="d-block text-gray-dark">Correo electr�nico</strong>
                                ${owner.owner_email}
                            </p>
                        </div>

                        <a role="button" class="btn btn-outline-primary btn-lg btn-block" href="<c:url value="/owner/legal/${owner.id_owner}/edit"/>"><i class="fa fa-edit"></i> Editar</a>

                    </div>
                </div>
                <div class="col-lg-6">
                    <div class="my-3 p-3 bg-white rounded box-shadow">
                        <h6 class="border-bottom border-gray pb-2 mb-0">Registros</h6>
                        <div class="media text-muted pt-3">
                            <div class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">Establecimientos</strong>
                                    <a href="<c:url value="/owner/${owner.id_owner}/establishment"/>">Ver todos</a>
                                </div>
                                <span class="d-block">${owner.establishmentCount} Establecimientos registados para este propietario</span>
                            </div>
                        </div>
                        <div class="media text-muted pt-3">
                            <div class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">Veh�culos</strong>
                                    <a href="<c:url value="/owner/${owner.id_owner}/vehicle/list"/>">Ver todos</a>
                                </div>
                                <span class="d-block">${owner.vehicleCount} Veh�culos registados para este propietario</span>
                            </div>
                        </div>
                        <div class="media text-muted pt-3">
                            <div class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">Usuarios</strong>
                                    <a href="<c:url value="/owner/${owner.id_owner}/client/all"/>">Ver todos</a>
                                </div>
                                <span class="d-block">${owner.client_count} Usuarios de ventanilla registados para este propietario</span>
                            </div>
                        </div>
                        <div class="media text-muted pt-3">
                            <div class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                                <div class="d-flex justify-content-between align-items-center w-100">
                                    <strong class="text-gray-dark">Productos</strong>
                                    <a href="<c:url value="/owner/${owner.id_owner}/product/add"/>">Ver todos</a>
                                </div>
                                <span class="d-block">${owner.productCount} Productos registados para este propietario</span>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-outline-secondary btn-lg btn-block goback"><i class="fa fa-reply"></i> Atras</button>
                </div>
            </div>
        </div>
    </div>
</main>
