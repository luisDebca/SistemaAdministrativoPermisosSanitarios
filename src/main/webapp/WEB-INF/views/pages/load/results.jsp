<%--@elvariable id="file_name" type="java.lang.String"--%>
<%--@elvariable id="summary" type="edu.ues.aps.etl.transform.ExtractionSummary"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">
            <%--@elvariable id="data" type="java.util.Map<String,java.util.List<edu.ues.aps.etl.load.CustomCell>>"--%>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/load/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Vista previa</li>
                </ol>
            </nav>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h5 class="card-title"><i class="fa fa-database"></i> Resultados</h5>
                            <p class="text-muted">Resultados de carga del archivo <strong>${file_name}</strong>.</p>
                            <p class="text-muted my-1">N�mero de registros: <strong>${summary.originalRowCount}</strong></p>
                            <p class="text-muted my-1">N�mero de columnas: <strong>${summary.originalColumnCount}</strong></p>
                            <p class="text-muted my-1">N�mero de registros completados: <strong>${summary.completedRowCount}</strong></p>
                            <p class="text-muted my-1">N�mero de celdas vac�as: <strong>${summary.emptyCellCount}</strong></p>
                            <p class="text-muted my-1">N�mero de registros con posibles problemas: <strong>${summary.withWarningRowCount}</strong></p>
                            <p class="text-muted my-1">N�mero de registros con errores: <strong>${summary.withErrorRowCount}</strong></p>
                            <p class="text-muted my-1">N�mero de registros guardados: <strong>${summary.savedRowsCount}</strong></p>
                            <p class="text-muted my-1">N�mero de registros actualizados: <strong>${summary.updatedRowsCount}</strong></p>
                            <c:if test="${summary.finishedCorrectly}">
                                <div class="p-3 mb-2 mt-4 bg-primary text-white rounded h6"><i class="fa fa-check-circle-o"></i> Extracci�n finalizada correctamente.</div>
                            </c:if>
                            <c:if test="${not summary.finishedCorrectly}">
                                <div class="p-3 mb-2 mt-4 bg-danger text-white rounded h6"><i class="fa fa-warning"></i> Error durante la extracci�n de datos.</div>
                            </c:if>

                        </div>
                        <div class="col-6 text-right">
                            <a href="<c:url value="/load/index"/>" role="button" class="btn btn-outline-secondary"><i class="fa fa-reply"></i> Inicio</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="card mt-3">
                <div class="card-body">
                    <h5 class="card-title"><i class="fa fa-terminal"></i> Registro de proceso</h5>
                    <table class="table">
                        <c:forEach var="item" items="${summary.messages}">
                            <tr>
                                <td class="bg-${item.key.split("-")[1]} h5 text-uppercase cell-center-middle">${item.key}</td>
                                <td>${item.value}</td>
                            </tr>
                        </c:forEach>
                    </table>
                </div>
            </div>

        </div>
    </div>
</main>