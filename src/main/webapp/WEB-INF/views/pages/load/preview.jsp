<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">
            <%--@elvariable id="data" type="java.util.Map<String,java.util.List<edu.ues.aps.etl.load.CustomCell>>"--%>
            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/load/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Vista previa</li>
                </ol>
            </nav>

            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h5 class="card-title"><i class="fa fa-table"></i> Vista previa</h5>
                            <p class="text-muted">Vista previa de los datos contenidos en el archivo cargado.</p>
                            <p>N�mero de registros: ${data.size()}</p>
                            <strong>Antes de continuar:</strong>
                            <ul class="text-danger">
                                <li>La primera fila de cada columna debe contener el nombre del campo especifico de la base de datos.</li>
                                <li>Evitar formatos extra�os.</li>
                                <li>Evitar datos repetidos.</li>
                                <li>Evitar campos vacios</li>
                            </ul>
                        </div>
                        <div class="col-6 text-right">
                            <a href="<%--@elvariable id="file_name" type="java.lang.String"--%>
                            <c:url value="/load/${file_name}/database"/>" role="button" class="btn btn-outline-primary"><i class="fa fa-database"></i> Cargar en DB</a>
                            <a href="<c:url value="/load/index"/>" role="button" class="btn btn-outline-secondary">Cancelar</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row mt-4">
                <div class="col-lg-12">
                    <c:if test="${empty data}">
                        <div class="alert alert-warning" role="alert">
                            No se encontraron registros en el archivo.
                        </div>
                    </c:if>
                    <c:if test="${not empty data}">
                    <div class="table-responsive">
                        <table class="table table-hover" width="100%" id="datatable" cellspacing="0">
                            <thead>
                            <tr>
                                <td class="table-dark cell-center-middle"><h5>#</h5></td>
                                <%--@elvariable id="table_titles" type="java.lang.String[]"--%>
                                <c:forEach items="${table_titles}" var="item">
                                    <td class="table-dark cell-center-middle"><h5>${item}</h5></td>
                                </c:forEach>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach items="${data}" var="row" varStatus="varst">
                                <tr>
                                    <td class="table-dark cell-center-middle">${varst.index + 1}</td>
                                    <c:forEach items="${row.value}" var="cell">
                                        <td style="background-color:${cell.bgColor};color:${cell.text_color};
                                                font-weight:${cell.textWeight};font-size:${cell.textSize} pt;">
                                                ${cell.content}
                                        </td>
                                    </c:forEach>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    </c:if>
                </div>
            </div>
        </div>
    </div>
</main>