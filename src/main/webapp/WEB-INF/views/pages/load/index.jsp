<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row">
                <div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary"><h3><i class="fa fa-file-excel-o" aria-hidden="true"></i>
                    Carga de archivos</h3></div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title"></h5>
                            <h6 class="card-subtitle mb-2 text-muted">Carga de archivos externos</h6>
                            <p class="card-text">Cargar registros en la base de datos desde archivos ODS, XLS, XLSX.</p>
                            <div class="btn-group" role="group" aria-label="Basic example">
                                <c:url value="/load/uploadExcel" var="uploadFileUrl"/>
                                <form method="post" enctype="multipart/form-data" action="<c:url value='/load/uploadExcel?${_csrf.parameterName}=${_csrf.token}' />">
                                    <div class="form-group">
                                        <label for="excelFile">Seleccione el archivo.</label>
                                        <input id="excelFile" type="file" name="file" class="form-control-file" accept=".xls,.xlsx" required="required"/>
                                    </div>
                                    <button type="submit" class="btn btn-info btn-lg"><i class="fa fa-cloud-upload" aria-hidden="true"></i> Cargar archivo</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>