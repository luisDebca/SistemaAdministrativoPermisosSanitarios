<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/request/delivery/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item">Entrega de permisos</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover establishment-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>Solicitud</th>
                        <th>Resolución</th>
                        <th>Establecimiento</th>
                        <th>Tipo</th>
                        <th>Rubro</th>
                        <th>NIT</th>
                        <th>Dirección</th>
                        <th>Propietario</th>
                        <th>SIBASI/UCSF</th>
                        <th>Permiso</th>
                        <th>Recepción</th>
                        <th>Inicio permiso</th>
                        <th>Vencimiento</th>
                        <th>Estado de permiso</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>

                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.delivery.establishment.dto.AbstractEstablishmentCompletedRequestDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td class="id_owner" hidden="hidden">${item.id_owner}</td>
                            <td class="id_establishment" hidden="hidden">${item.id_establishment}</td>
                            <td class="id_caseFile" hidden="hidden">${item.id_caseFile}</td>
                            <td><a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>">${item.folder_number}</a>
                            </td>
                            <td><a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>">${item.request_number}</a>
                            </td>
                            <td>
                                <a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>">${item.resolution_number}</a>
                            </td>
                            <td>
                                <a href="<c:url value="/establishment/${item.id_establishment}"/>">${item.establishment_name}</a>
                            </td>
                            <td>${item.establishment_type_detail}</td>
                            <td>${item.establishment_section}/${item.establishment_type}</td>
                            <td>${item.establishment_nit}</td>
                            <td>${item.establishment_address}</td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.SIBASI}/${item.UCSF}</td>
                            <td>${item.certification_type}</td>
                            <td class="cell-center-middle">${item.caseFile_createOn}</td>
                            <td class="cell-center-middle">${item.certification_start}</td>
                            <td class="cell-center-middle">${item.certification_end}</td>
                            <td class="cell-center-middle">
                                <c:if test="${not item.user_delivery}"><span
                                        class="badge badge-secondary">${item.certification_status}</span></c:if>
                                <c:if test="${item.user_delivery}"><span
                                        class="badge badge-primary">${item.certification_status}</span></c:if>
                            </td>
                            <td class="table-options cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="grouInf">
                                        <a role="button" href="<c:url value="/caseFile/${item.id_caseFile}/info"/>"
                                           class="btn btn-outline-secondary" data-toggle="tooltip"
                                           title="Ver solicitud"><i
                                                class="fa fa-folder-open-o"></i></a>
                                        <c:if test="${not item.user_delivery}">
                                            <a role="button" href="javascript:"
                                               class="btn btn-outline-secondary complete-process"
                                               data-toggle="tooltip" title="Marcar como entregado"><i
                                                    class="fa fa-check"></i></a>
                                        </c:if>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <c:if test="${item.user_delivery}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary move_to_next_process_btn"
                                                    data-toggle="tooltip" title="Finalizar proceso de certificación del permiso"><i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                        </c:if>
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                    aria-hidden="true"></i>
                                        </button>
                                        <button type="button"
                                                class="btn btn-outline-secondary show_process_record_summary"
                                                data-toggle="tooltip" title="Ver resumen de proceso"><i
                                                class="fa fa-share-alt" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<!-- PROCESS COMPLETION MODAL -->
<div class="modal fade" id="complete-process-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="complete-process-title">Completar proceso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-2x fa-question-circle-o"></i>
                <h5 class="card-title">¿Completar proceso de certificación de permiso sanitario?</h5>
                <p class="text-muted">Todos los procesos han sido finalizados y la resolución y diplomas fueron
                    entregadas al usuario.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="complete-btn" type="button" class="btn btn-primary">Finalizar</button>
            </div>
        </div>
    </div>
</div>