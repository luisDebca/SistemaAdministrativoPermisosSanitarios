<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/request/delivery/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Entrega de permisos</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover vehicle-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>N� Solicitudes</th>
                        <th>Propietario</th>
                        <th>Compa�ia de distribuci�n</th>
                        <th>Destino de distribuci�n</th>
                        <th>Lugar de mercancia</th>
                        <th>Direcci�n de estancia</th>
                        <th>Inicio de permiso</th>
                        <th>Vencimiento</th>
                        <th>Estado del permiso</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.delivery.vehicle.dto.AbstractVehicleDeliveryDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td hidden="hidden" class="id_owner">${item.id_owner}</td>
                            <td hidden="hidden" class="id_bunch">${item.id_bunch}</td>
                            <td class="cell-center-middle">${item.request_folder}</td>
                            <td class="cell-center-middle"><h5>${item.request_count}</h5></td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.bunch_distribution_company}</td>
                            <td>${item.bunch_distribution_destination}</td>
                            <td>${item.bunch_marketing_place}</td>
                            <td>${item.bunch_address}</td>
                            <td>${item.certification_start}</td>
                            <td>${item.certification_end}</td>
                            <td class="cell-center-middle"><span class="badge badge-primary">${item.certification_status}</span></td>
                            <td class="cell-center-middle table-options">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="groupInf">
                                        <a href="#"
                                           class="btn btn-outline-secondary disabled_href show_brief_bunch_information"
                                           data-toggle="tooltip" title="Ver solicitudes contenidas"><i class="fa fa-th"
                                                                                                       aria-hidden="true"></i></a>
                                        <c:if test="${not item.isUserDelivered()}">
                                            <a role="button" href="javascript:"
                                               class="btn btn-outline-secondary complete-process"
                                               data-toggle="tooltip" title="Marcar como entregado"><i
                                                    class="fa fa-check"></i></a>
                                        </c:if>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <c:if test="${item.isUserDelivered()}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary disabled_href move_to_next_process_btn"
                                                    data-toggle="tooltip" title="Finalizar proceso de certificaci�n del permiso"><i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                        </c:if>
                                        <c:if test="${not item.isUserDelivered()}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary disabled_href move_to_forgotten_btn"
                                                    data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                    class="fa fa-minus" aria-hidden="true"></i></button>
                                            <button type="button"
                                                    class="btn btn-outline-secondary disabled_href move_to_next_rejected_btn"
                                                    data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                        aria-hidden="true"></i>
                                            </button>
                                        </c:if>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<!-- PROCESS COMPLETION MODAL -->
<div class="modal fade" id="complete-process-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="complete-process-title">Completar proceso</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-2x fa-question-circle-o"></i>
                <h5 class="card-title">�Completar proceso de certificaci�n de permiso sanitario?</h5>
                <p class="text-muted">Todos los procesos han sido finalizados y la resoluci�n y diplomas fueron
                    entregadas al usuario.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="complete-btn" type="button" class="btn btn-primary">Finalizar</button>
            </div>
        </div>
    </div>
</div>