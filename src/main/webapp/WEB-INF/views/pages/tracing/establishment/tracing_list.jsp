<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Ingreso de solicitudes</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover establishment-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>Solicitud</th>
                        <th>Establecimiento</th>
                        <th>Tipo</th>
                        <th>Rubro</th>
                        <th>NIT</th>
                        <th>Direcci�n</th>
                        <th>Propietario</th>
                        <th>SIBASI/UCSF</th>
                        <th>Permiso</th>
                        <th>Recepci�n</th>
                        <th>Enviado</th>
                        <th>Recibido</th>
                        <th>Retornado</th>
                        <th>Inspecciones</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.tracing.establishment.dto.AbstractTracingEstablishmentDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td class="id_owner" hidden="hidden">${item.id_owner}</td>
                            <td class="id_establishment" hidden="hidden">${item.id_establishment}</td>
                            <td class="id_caseFile" hidden="hidden">${item.id_caseFile}</td>
                            <td><a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>">${item.folder_number}</a>
                            </td>
                            <td><a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>">${item.request_number}</a>
                            </td>
                            <td>
                                <a href="<c:url value="/establishment/${item.id_establishment}"/>">${item.establishment_name}</a>
                            </td>
                            <td>${item.establishment_type_detail}</td>
                            <td>${item.establishment_section}/${item.establishment_type}</td>
                            <td>${item.establishment_nit}</td>
                            <td>${item.establishment_address}</td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.SIBASI}/${item.UCSF}</td>
                            <td>${item.certification_type}</td>
                            <td class="cell-center-middle">${item.caseFile_createOn}</td>
                            <td class="cell-center-middle">${item.send_date}</td>
                            <td class="cell-center-middle">${item.received_date}</td>
                            <td class="cell-center-middle">${item.return_date}</td>
                            <td class="cell-center-middle">
                                <c:if test="${not item.tracing}"><span
                                        class="badge badge-secondary">Sin Seguimiento</span></c:if>
                                <c:if test="${item.tracing and item.inspection_count == 0}"><span
                                        class="badge badge-secondary">Sin Inspecciones</span></c:if>
                                <c:if test="${item.tracing and item.inspection_count > 0}"><span
                                        class="badge badge-primary">${item.inspection_count} Inspecciones</span></c:if>
                            </td>
                            <td class="table-options cell-center-middle">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="grouInf">
                                        <a href="<c:url value="/caseFile/${item.id_caseFile}/info"/>"
                                           role="button" class="btn btn-outline-secondary" data-toggle="tooltip"
                                           data-placement="top" title="Ver informaci�n de la solicitud"><i
                                                class="fa fa-folder-open-o" aria-hidden="true"></i></a>
                                        <c:if test="${not item.tracing}">
                                            <a href="<c:url value="/tracing/${item.id_caseFile}/new"/>"
                                               class="btn btn-outline-secondary"
                                               data-toggle="tooltip" title="Registrar seguimiento de solicitud"><i
                                                    class="fa fa-plus" aria-hidden="true"></i></a>
                                        </c:if>
                                        <c:if test="${item.tracing}">
                                            <a href="<c:url value="/tracing/${item.id_caseFile}/edit"/>"
                                               class="btn btn-outline-secondary"
                                               data-toggle="tooltip" title="Editar seguimiento de solicitud"><i
                                                    class="fa fa-edit" aria-hidden="true"></i></a>
                                            <a href="<c:url value="/tracing/${item.id_caseFile}/inspection/all"/>"
                                               class="btn btn-outline-secondary"
                                               data-toggle="tooltip" title="Ver inspecciones"><i class="fa fa-eye"
                                                                                                 aria-hidden="true"></i></a>
                                        </c:if>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <c:if test="${item.tracing and item.inspection_count > 0}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary move_to_next_process_btn"
                                                    data-toggle="tooltip" title="Pasar a revisi�n de la solicitud"><i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                        </c:if>
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                    aria-hidden="true"></i>
                                        </button>
                                        <button type="button"
                                                class="btn btn-outline-secondary show_process_record_summary"
                                                data-toggle="tooltip" title="Ver resumen de proceso"><i
                                                class="fa fa-share-alt" aria-hidden="true"></i></button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>