<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/tracing/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Seguimiento de solicitudes</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover vehicle-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>N� Solicitudes</th>
                        <th>Propietario</th>
                        <th>Compa�ia de distribuci�n</th>
                        <th>Destino de distribuci�n</th>
                        <th>Lugar de mercancia</th>
                        <th>Direcci�n de estancia</th>
                        <th>Enviado</th>
                        <th>Recibido</th>
                        <th>Retornado</th>
                        <th>Inspecciones</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.tracing.vehicle.dto.AbstractVehicleTracingProcessDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td hidden="hidden" class="id_owner">${item.id_owner}</td>
                            <td hidden="hidden" class="id_bunch">${item.id_bunch}</td>
                            <td class="cell-center-middle">${item.request_folder}</td>
                            <td class="cell-center-middle"><h5>${item.request_count}</h5></td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.bunch_distribution_company}</td>
                            <td>${item.bunch_distribution_destination}</td>
                            <td>${item.bunch_marketing_place}</td>
                            <td>${item.bunch_address}</td>
                            <td class="cell-center-middle">${item.delivery_date}</td>
                            <td class="cell-center-middle">${item.received_date}</td>
                            <td class="cell-center-middle">${item.return_date}</td>
                            <td class="cell-center-middle">
                                <c:if test="${not item.is_tracing}"><span
                                        class="badge badge-secondary">Sin Seguimiento</span></c:if>
                                <c:if test="${item.is_tracing and item.inspection_count == 0}"><span
                                        class="badge badge-secondary">Sin Inspecciones</span></c:if>
                                <c:if test="${item.is_tracing and item.inspection_count > 0}"><span
                                        class="badge badge-primary">${item.inspection_count} Inspecciones</span></c:if>
                            </td>
                            <td class="cell-center-middle table-options">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="groupInf">
                                        <a href="javascript:"
                                           class="btn btn-outline-secondary disabled_href show_brief_bunch_information"
                                           data-toggle="tooltip" title="Ver solicitudes contenidas"><i class="fa fa-th"
                                                                                                       aria-hidden="true"></i></a>
                                        <c:if test="${not item.is_tracing}">
                                            <a href="<c:url value="/tracing/bunch/${item.id_bunch}/new"/>"
                                               class="btn btn-outline-secondary"
                                               data-toggle="tooltip" title="Registrar seguimiento de solicitud"><i
                                                    class="fa fa-plus" aria-hidden="true"></i></a>
                                        </c:if>
                                        <c:if test="${item.is_tracing}">
                                            <a href="<c:url value="/tracing/bunch/${item.id_bunch}/edit"/>"
                                               class="btn btn-outline-secondary"
                                               data-toggle="tooltip" title="Editar seguimiento de solicitud"><i
                                                    class="fa fa-edit" aria-hidden="true"></i></a>
                                            <a href="<c:url value="/tracing/bunch/${item.id_bunch}/inspection/all"/>"
                                               class="btn btn-outline-secondary"
                                               data-toggle="tooltip" title="Ver inspecciones"><i class="fa fa-eye"
                                                                                                 aria-hidden="true"></i></a>
                                        </c:if>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <c:if test="${item.is_tracing and item.inspection_count > 0}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary disabled_href move_to_next_process_btn"
                                                    data-toggle="tooltip"
                                                    title="Pasar a revisiones externas de la solicitud"><i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                        </c:if>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                    aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>