<%--@elvariable id="id_bunch" type="java.lang.Long"--%>
<%--@elvariable id="id_tracing" type="java.lang.Long"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="from" uri="http://www.springframework.org/tags/form" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/tracing/vehicle/all"/>">Seguimiento de
                        solicitudes</a></li>
                    <li class="breadcrumb-item active">Seguimiento</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Registro de seguiminto</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Ingrese los datos iniciales para el seguimiento de las revisiones de la solicitud.</h6>
                    <hr class="my-4">
                    <%--@elvariable id="tracing" type="edu.ues.aps.process.tracing.base.model.AbstractTracing"--%>
                    <from:form method="post" modelAttribute="tracing" id="main">

                        <form:hidden path="id"/>
                        <form:hidden path="created_on"/>

                        <div class="form-group row">
                            <label for="shipping_date-date" class="col-sm-2 col-form-label" >Fecha de envio</label>
                            <div class="col-sm-5">
                                <form:input type="text" class="form-control date" id="shipping_date-date"
                                            path="shipping_date" autocomplete="off"/>
                                <small id="date0help" class="form-text text-muted">El expediente fue enviado a UCSF.</small>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="return-date" class="col-sm-2 col-form-label">Fecha de recibido</label>
                            <div class="col-sm-5">
                                <form:input type="text" class="form-control date" id="return-date"
                                            path="return_date" autocomplete="off"/>
                                <small id="date2help" class="form-text text-muted">Fecha de recibido por el encargado de revisión.</small>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="UCSF-received" class="col-sm-2 col-form-label">Fecha de revisión</label>
                            <div class="col-sm-5">
                                <form:input type="text" class="form-control date" id="UCSF-received"
                                            path="ucsf_received_on" autocomplete="off"/>
                                <small id="date2help" class="form-text text-muted">Fecha de finalización de la revisión.</small>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="return-date" class="col-sm-2 col-form-label">Fecha de entrega</label>
                            <div class="col-sm-5">
                                <form:input type="text" class="form-control date" id="review-date"
                                            path="review_date" autocomplete="off"/>
                                <small id="date3help" class="form-text text-muted">Fecha de entrega a técnico para resoluciones.</small>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="observation" class="col-sm-2 col-form-label">Observación</label>
                            <div class="col-sm-10">
                                <form:textarea class="form-control" id="observation" rows="3" path="observation"/>
                            </div>
                        </div>

                        <hr class="my-4">

                        <button type="submit" class="btn btn-primary">Guardar</button>
                        <a href="<c:url value="/tracing/vehicle/all"/>" role="button" class="btn btn-secondary">Cancelar</a>

                    </from:form>
                </div>
            </div>
        </div>
    </div>
</main>