<%--@elvariable id="bunch" type="edu.ues.aps.process.area.vehicle.dto.AbstractVehicleBunchDTO"--%>
<%--@elvariable id="caseFile" type="java.util.List<edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO>"--%>
<%--@elvariable id="vehicles" type="java.util.List<edu.ues.aps.process.area.vehicle.dto.AbstractVehicleDTO>"--%>
<%--@elvariable id="clients" type="java.util.List<edu.ues.aps.process.base.dto.AbstractClientDTO>"--%>
<%--@elvariable id="natural_owner" type="edu.ues.aps.process.base.dto.AbstractNaturalEntityDTO"--%>
<%--@elvariable id="legal_owner" type="edu.ues.aps.process.base.dto.AbstractLegalEntityDTO"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<c:set var="success" value="<i class='fa fa-check-circle-o fa-lg text-success'></i>"/>
<c:set var="danger" value="<i class='fa fa-ban fa-lg text-danger'></i>"/>
<c:set var="warning" value="<i class='fa fa-exclamation-circle fa-lg text-warning'></i>"/>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/case-file/request/review/index"/>">Inicio</a>
                    </li>
                    <li class="breadcrumb-item"><a href="<c:url value="/case-file/establishment/request/review/list"/>">Revisi�n
                        de solicitudes</a></li>
                    <li class="breadcrumb-item active">Revisi�n</li>
                </ol>
            </nav>

            <div class="information-panel">
                <c:if test="${bunch.id_bunch == 0}">
                    <div class="alert alert-danger" role="alert">
                        <strong>No se encontro el expediente solicitado.</strong>
                    </div>
                </c:if>
            </div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Revisi�n previa a ingreso</h5>
                    <h5 class="card-subtitle mb-2 text-muted">Datos requeridos del expediente para continuar
                        proceso</h5>
                    <hr class="my-4">
                    <p id="ib_bunch" hidden="hidden">${bunch.id_bunch}</p>
                    <h5>Informaci�n de la solicitud ${bunch.folder_number}</h5>
                    <p>Solicitud para <strong>${bunch.caseFiles_count}</strong> veh�culos</p>
                    <p>Compa�ia de
                        distribuci�n: ${bunch.distribution_company} ${empty bunch.distribution_company ? danger:success}</p>
                    <p>Destino de
                        distribuci�n: ${bunch.distribution_destination} ${empty bunch.distribution_destination ? danger:success}</p>
                    <p>Lugar de mercancia: ${bunch.marketing_place} ${empty bunch.marketing_place ? danger:success}</p>
                    <h6 class="text-muted">Persona presente en la inspecci�n</h6>
                    <p>Nombre: ${bunch.client_presented_name} ${empty bunch.client_presented_name ? danger:success}</p>
                    <p>
                        Tel�fono: ${bunch.client_presented_telephone} ${empty bunch.client_presented_telephone ? warning:success}</p>
                    <p>Direcci�n de
                        permanencia: ${bunch.address_details} ${empty bunch.address_details ? danger:success}</p>


                    <hr class="my-4">
                    <h5 class="card-subtitle mb-2 text-muted">Propietario</h5>
                    <c:if test="${natural_owner.id_owner != 0 and not empty natural_owner.id_owner}">
                        <p>Nombre: ${natural_owner.owner_name} ${empty natural_owner.owner_name ? danger:success}</p>
                        <p>NIT: ${natural_owner.owner_nit} ${empty natural_owner.owner_nit ? warning:success}</p>
                        <p>DUI: ${natural_owner.owner_dui} ${empty natural_owner.owner_dui ? danger:success}</p>
                        <c:if test="${natural_owner.owner_nationally == 'Salvadore�a'}">
                            <p>
                                Pasaporte: ${natural_owner.owner_passport} ${empty natural_owner.owner_passport ? warning:success}</p>
                            <p>Tarjeta de
                                residente: ${natural_owner.owner_resident_card} ${empty natural_owner.owner_resident_card ? warning:success}</p>
                        </c:if>
                        <p>
                            Nacionalidad: ${natural_owner.owner_nationally} ${empty natural_owner.owner_nationally ? danger:success}</p>
                        <p>
                            Tel�fono: ${natural_owner.owner_telephone} ${empty natural_owner.owner_telephone ? warning:success}</p>
                        <p>Correo
                            electronico: ${natural_owner.owner_email} ${empty natural_owner.owner_email ? warning:success}</p>
                        <p>FAX: ${natural_owner.owner_fax} ${empty natural_owner.owner_fax ? warning:success}</p>
                        <p class="d-block text-right mt-3">
                            <a href="<c:url value="/owner/${natural_owner.id_owner}"/>">Ir a propietario</a>
                        </p>
                    </c:if>

                    <c:if test="${legal_owner.id_owner != 0 and not empty legal_owner.id_owner}">
                        <p>Nombre: ${legal_owner.owner_name} ${empty legal_owner.owner_name ? danger:success}</p>
                        <p>
                            Direcci�n: ${legal_owner.owner_address} ${empty legal_owner.owner_address ? danger:success}</p>
                        <p>NIT: ${legal_owner.owner_nit} ${empty legal_owner.owner_nit ? danger:success}</p>
                        <p>
                            Tel�fono: ${legal_owner.owner_telephone} ${empty legal_owner.owner_telephone ? warning:success}</p>
                        <p>Correo
                            electronico: ${legal_owner.owner_email} ${empty legal_owner.owner_email ? warning:success}</p>
                        <p>FAX: ${legal_owner.owner_fax} ${empty legal_owner.owner_fax ? warning:success}</p>
                        <p class="d-block text-right mt-3">
                            <a href="<c:url value="/owner/${legal_owner.id_owner}"/>">Ir a propietario</a>
                        </p>
                    </c:if>

                    <hr class="my-4">
                    <h5 class="card-subtitle mb-2 text-muted">Solicitudes contenidas</h5>
                    <table class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th scope="col">N�</th>
                            <th scope="col">Folder</th>
                            <th scope="col">Permiso</th>
                            <th scope="col">Duraci�n del permiso</th>
                            <th scope="col">UCSF</th>
                            <th scope="col">SIBASI</th>
                            <th>--------------------</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${caseFile}" var="item" varStatus="varst">
                            <tr>
                                <td>${varst.index + 1}</td>
                                <td>${item.request_folder}</td>
                                <td>${item.certification_type}</td>
                                <td>${item.certification_duration} a�os</td>
                                <td>${item.ucsf} ${empty item.ucsf ? danger:success}</td>
                                <td>${item.sibasi} ${empty item.sibasi ? danger:success}</td>
                                <td><a href="<c:url value="/caseFile/${item.id_casefile}/edit"/>">Editar solicitud</a>
                                </td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>
                    <div class="text-right pr-2"><a role="button" class="btn btn-link" href="<c:url value="/vehicle/bunch/${bunch.id_bunch}/casefile/edit"/>">Modificar grupo de solicitudes</a></div>

                    <hr class="my-4">
                    <h5 class="card-subtitle mb-2 text-muted">Veh�culos contenidos</h5>
                    <table class="table table-striped table-sm">
                        <thead>
                        <tr>
                            <th scope="col">N�</th>
                            <th scope="col">Placa</th>
                            <th scope="col">Transporta</th>
                            <th scope="col">Tipo</th>
                            <th scope="col">N� empleados</th>
                            <th scope="col">Propietario</th>
                            <th>--------------------</th>
                        </tr>
                        </thead>
                        <tbody>
                        <c:forEach items="${vehicles}" var="item" varStatus="varst">
                            <tr>
                                <td>${varst.index + 1}</td>
                                <td>${item.vehicle_license}</td>
                                <td>${item.vehicle_transported_content} ${empty item.vehicle_transported_content or item.vehicle_transported_content == 'INVALIDO' ? danger:success}</td>
                                <td>${item.vehicle_type} ${empty item.vehicle_type ? danger:success}</td>
                                <td>${item.vehicle_employees_number} ${empty item.vehicle_employees_number or item.vehicle_employees_number == 0 ? danger:success}</td>
                                <td>${item.owner_name}</td>
                                <td><a href="<c:url value="/vehicle/${item.id_vehicle}/edit"/>">Editar veh�culo</a></td>
                            </tr>
                        </c:forEach>
                        </tbody>
                    </table>

                    <hr class="my-4">
                    <h5 class="card-subtitle mb-2 text-muted">Datos requeridos de los usuarios para continuar
                        proceso</h5>
                    <c:if test="${empty clients}">
                        <div class="alert alert-danger" role="alert">
                            <i class="fa fa-warning"></i> No se encontraron <strong>representantes, apoderados o personas autorizadas</strong> relacionadas con esta solicitud, de continuar pueden ocurrir
                            errores en los procesos siguientes.
                        </div>
                        <div class="text-right pr-2"><a role="button" class="btn btn-link" href="<c:url value="/owner/${bunch.id_owner}/client/all"/>">Agregar representante, apoderado, etc..</a></div>
                    </c:if>
                    <c:if test="${not empty clients}">
                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th scope="col">N�</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Nacionalidad</th>
                                <th scope="col">DUI</th>
                                <th scope="col">Tel�fono</th>
                                <th scope="col">Email</th>
                                <th scope="col">Pasaporte</th>
                                <th scope="col">Carnet de residente</th>
                                <th scope="col">-------------</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="item" items="${clients}" varStatus="varst">
                                <tr>
                                    <td>${varst.index + 1}</td>
                                    <td>${item.client_name} ${empty item.client_name ? danger:success}</td>
                                    <td>${item.client_nationally} ${empty item.client_nationally ? danger:success}</td>
                                    <td>
                                        <c:if test="${item.client_nationally == 'Salvadore�a'}">
                                            <p>DUI: ${item.client_DUI} ${empty item.client_DUI ? warning:success}</p>
                                        </c:if>
                                    </td>
                                    <td>${item.client_telephone} ${empty item.client_telephone ? warning:success}</td>
                                    <td>${item.client_email} ${empty item.client_email ? warning:success}</td>
                                    <td>
                                        <c:if test="${item.client_nationally != 'Salvadore�a'}">
                                            ${item.client_passport} ${empty item.client_passport ? warning:success}
                                        </c:if>
                                    </td>
                                    <td>
                                        <c:if test="${item.client_nationally != 'Salvadore�a'}">
                                            ${item.client_resident_card} ${empty item.client_resident_card ? warning:success}
                                        </c:if>
                                    </td>
                                    <td><a href="<c:url value="/client/${item.id_client}/edit"/>">Editar usuario</a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <hr class="my-4">
                    <a role="button"
                       href="<c:url value="/case-file/vehicle/request/bunch/${bunch.id_bunch}/validated"/>"
                       class="btn btn-primary accept-and-continue-btn">Marcar como revisado</a>
                    <a role="button" class="btn btn-secondary"
                       href="<c:url value="/case-file/vehicle/request/review/list"/>">Cancelar</a>
                </div>
            </div>
        </div>
    </div>
</main>
