<%--@elvariable id="caseFile" type="edu.ues.aps.process.area.base.dto.AbstractCaseFileDTO"--%>
<%--@elvariable id="establishment" type="edu.ues.aps.process.area.establishment.dto.AbstractEstablishmentDTO"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/case-file/request/review/index"/>">Inicio</a>
                    </li>
                    <li class="breadcrumb-item"><a href="<c:url value="/case-file/establishment/request/review/list"/>">Solicitudes
                        de establecimientos</a></li>
                    <li class="breadcrumb-item active">Revisi�n</li>
                </ol>
            </nav>

            <div class="information-panel">
                <c:if test="${caseFile.id_casefile == 0}">
                    <div class="alert alert-danger" role="alert">
                        <strong>No se encontro el expediente solicitado.</strong>
                    </div>
                </c:if>
            </div>

            <div class="card">
                <div class="card-body">
                    <c:set var="success" value="<i class='fa fa-check-circle-o fa-lg text-success'></i>"/>
                    <c:set var="danger" value="<i class='fa fa-ban fa-lg text-danger'></i>"/>
                    <c:set var="warning" value="<i class='fa fa-exclamation-circle fa-lg text-warning'></i>"/>
                    <h5 class="card-title">Revisi�n previa a ingreso</h5>
                    <h5 class="card-subtitle mb-2 text-muted">Datos requeridos del expediente para continuar
                        proceso</h5>
                    <p id="caseFile_id" hidden="hidden">${caseFile.id_casefile}</p>
                    <p>N�mero de folder: ${caseFile.request_folder}</p>
                    <p>Duraci�n del permiso: ${caseFile.certification_duration}
                        a�os ${empty caseFile.certification_duration ? danger:success}</p>
                    <p>UCSF: ${caseFile.ucsf} ${empty caseFile.ucsf ? danger:success}</p>
                    <p>SIBASI: ${caseFile.sibasi} ${empty caseFile.sibasi ? danger:success}</p>
                    <p class="d-block text-right mt-3">
                        <a href="<c:url value="/caseFile/${caseFile.id_casefile}/info"/>">Ir a expediente</a>
                    </p>
                    <hr class="my-4">
                    <h5 class="card-subtitle mb-2 text-muted">Datos requeridos del propietario para continuar
                        proceso</h5>
                    <%--@elvariable id="natural" type="edu.ues.aps.process.base.dto.AbstractNaturalEntityDTO"--%>
                    <c:if test="${natural.id_owner != 0 and not empty natural.id_owner}">
                        <p>Nombre: ${natural.owner_name} ${empty natural.owner_name ? danger:success}</p>
                        <p>NIT: ${natural.owner_nit} ${empty natural.owner_nit ? warning:success}</p>
                        <p>DUI: ${natural.owner_dui} ${empty natural.owner_dui ? danger:success}</p>
                        <c:if test="${natural.owner_nationally == 'Salvadore�a'}">
                            <p>
                                Pasaporte: ${natural.owner_passport} ${empty natural.owner_passport ? warning:success}</p>
                            <p>Tarjeta de
                                residente: ${natural.owner_resident_card} ${empty natural.owner_resident_card ? warning:success}</p>
                        </c:if>
                        <p>
                            Nacionalidad: ${natural.owner_nationally} ${empty natural.owner_nationally ? danger:success}</p>
                        <p>Tel�fono: ${natural.owner_telephone} ${empty natural.owner_telephone ? warning:success}</p>
                        <p>Correo electronico: ${natural.owner_email} ${empty natural.owner_email ? warning:success}</p>
                        <p>FAX: ${natural.owner_fax} ${empty natural.owner_fax ? warning:success}</p>
                        <p class="d-block text-right mt-3">
                            <a href="<c:url value="/owner/${natural.id_owner}"/>">Ir a propietario</a>
                        </p>
                    </c:if>
                    <%--@elvariable id="legal" type="edu.ues.aps.process.base.dto.AbstractLegalEntityDTO"--%>
                    <c:if test="${legal.id_owner != 0 and not empty legal.id_owner}">
                        <p>Nombre: ${legal.owner_name} ${empty legal.owner_name ? danger:success}</p>
                        <p>Direcci�n: ${legal.owner_address} ${empty legal.owner_address ? danger:success}</p>
                        <p>NIT: ${legal.owner_nit} ${empty legal.owner_nit ? danger:success}</p>
                        <p>Tel�fono: ${legal.owner_telephone} ${empty legal.owner_telephone ? warning:success}</p>
                        <p>Correo electronico: ${legal.owner_email} ${empty legal.owner_email ? warning:success}</p>
                        <p>FAX: ${legal.owner_fax} ${empty legal.owner_fax ? warning:success}</p>
                        <p class="d-block text-right mt-3">
                            <a href="<c:url value="/owner/${legal.id_owner}"/>">Ir a propietario</a>
                        </p>
                    </c:if>
                    <hr class="my-4">
                    <h5 class="card-subtitle mb-2 text-muted">Datos requeridos del establecimiento para continuar
                        proceso</h5>
                    <p>
                        Nombre: ${establishment.establishment_name} ${empty establishment.establishment_name ? danger:success}</p>
                    <p>
                        Direcci�n: ${establishment.establishment_address} ${empty establishment.establishment_address ? danger:success}</p>
                    <p>
                        NIT: ${establishment.establishment_nit} ${empty establishment.establishment_nit ? danger:success}</p>
                    <p>
                        Tipo: ${establishment.establishment_type_detail} ${empty establishment.establishment_type_detail ? danger:success}</p>
                    <p>
                        Rubro: ${establishment.establishment_type} ${empty establishment.establishment_type ? warning:success}</p>
                    <p>Registo
                        comercial: ${establishment.commercial_register} ${empty establishment.commercial_register ? warning:success}</p>
                    <p>Tel�fono
                        1: ${establishment.establishment_telephone} ${empty establishment.establishment_telephone ? warning:success}</p>
                    <p>Tel�fono
                        2: ${establishment.establishment_telephone_extra} ${empty establishment.establishment_telephone_extra ? warning:success}</p>
                    <p>Correo
                        electronico: ${establishment.establishment_email} ${empty establishment.establishment_email ? warning:success}</p>
                    <p>
                        FAX: ${establishment.establishment_fax} ${empty establishment.establishment_fax ? warning:success}</p>
                    <p>Horario de
                        operaciones: ${establishment.establishment_operations} ${empty establishment.establishment_operations ? danger:success}</p>
                    <p>Capital
                        registrado: ${establishment.establishment_capital} ${empty establishment.establishment_capital ? danger:success}</p>
                    <p>N�mero de empleados
                        masculinos: ${establishment.establishment_male_employee_count} ${empty establishment.establishment_male_employee_count ? danger:success}</p>
                    <p>N�mero de empleados
                        femeninos: ${establishment.establishment_female_employee_count} ${empty establishment.establishment_female_employee_count ? danger:success}</p>
                    <p class="d-block text-right mt-3">
                        <a href="<c:url value="/establishment/${establishment.establishment_id}"/>">Ir a
                            establecimiento</a>
                    </p>
                    <hr class="my-4">
                    <h5 class="card-subtitle mb-2 text-muted">Datos requeridos de los usuarios para continuar
                        proceso</h5>
                    <%--@elvariable id="clients" type="java.util.List<edu.ues.aps.process.base.dto.AbstractClientDTO>"--%>
                    <c:if test="${empty clients}">
                        <div class="alert alert-danger" role="alert">
                            No se encontraron <strong>representantes, apoterados o personas autorizadas</strong> relacionadas con esta solicitud, de continuar pueden ocurrir
                            errores en los procesos siguientes.
                        </div>
                        <p class="d-block text-right mt-3">
                            <c:if test="${legal.id_owner != 0}"><a href="<c:url value="/owner/${legal.id_owner}/client/all"/>">Agregar representante, apoderado, etc...</a></c:if>
                            <c:if test="${natural.id_owner != 0}"><a href="<c:url value="/owner/${natural.id_owner}/client/all"/>">Agregar representante, apoderado, etc...</a></c:if>
                        </p>
                    </c:if>
                    <c:if test="${not empty clients}">
                        <table class="table table-striped table-sm">
                            <thead>
                            <tr>
                                <th scope="col">N�</th>
                                <th scope="col">Nombre</th>
                                <th scope="col">Nacionalidad</th>
                                <th scope="col">DUI</th>
                                <th scope="col">Tel�fono</th>
                                <th scope="col">Email</th>
                                <th scope="col">Pasaporte</th>
                                <th scope="col">Carnet de residente</th>
                                <th scope="col">-------------</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:forEach var="item" items="${clients}" varStatus="varst">
                                <tr>
                                    <td>${varst.index + 1}</td>
                                    <td>${item.client_name} ${empty item.client_name ? danger:success}</td>
                                    <td>${item.client_nationally} ${empty item.client_nationally ? danger:success}</td>
                                    <td>
                                        <c:if test="${item.client_nationally == 'Salvadore�a'}">
                                            <p>DUI: ${item.client_DUI} ${empty item.client_DUI ? warning:success}</p>
                                        </c:if>
                                    </td>
                                    <td>${item.client_telephone} ${empty item.client_telephone ? warning:success}</td>
                                    <td>${item.client_email} ${empty item.client_email ? warning:success}</td>
                                    <td>
                                        <c:if test="${item.client_nationally != 'Salvadore�a'}">
                                            ${item.client_passport} ${empty item.client_passport ? warning:success}
                                        </c:if>
                                    </td>
                                    <td>
                                        <c:if test="${item.client_nationally != 'Salvadore�a'}">
                                            ${item.client_resident_card} ${empty item.client_resident_card ? warning:success}
                                        </c:if>
                                    </td>
                                    <td><a href="<c:url value="/client/${item.id_client}/edit"/>">Editar usuario</a>
                                    </td>
                                </tr>
                            </c:forEach>
                            </tbody>
                        </table>
                    </c:if>
                    <hr class="my-4">
                    <a role="button"
                       href="<c:url value="/case-file/establishment/request/${caseFile.id_casefile}/validated"/>"
                       class="btn btn-primary accept-and-continue-btn">Marcar como revisado</a>
                    <a role="button" class="btn btn-secondary"
                       href="<c:url value="/case-file/establishment/request/review/list"/>">Cancelar</a>
                </div>
            </div>
        </div>
    </div>
</main>
