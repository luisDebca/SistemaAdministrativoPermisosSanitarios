<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row">
                <div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary"><h3><i class="fa fa-exchange" aria-hidden="true"></i>
                    Procesos</h3></div>
            </div>
            <div class="row">
                <div class="col">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Procesos</h5>
                            <h6 class="card-subtitle mb-2 text-muted">Resumen de procesos activos</h6>

                            <div class="list-group">
                                <%--@elvariable id="process" type="java.util.List<edu.ues.aps.process.record.base.dto.AbstractProcessSummaryDTO>"--%>
                                <c:forEach var="item" items="${process}">
                                    <a href="javascript:" class="list-group-item list-group-item-action">
                                        <div class="d-flex w-100 justify-content-between">
                                            <h5 class="mb-1">${item.process}</h5>
                                            <small class="text-muted">${item.requestCounter} solicitudes en cola.</small>
                                        </div>
                                        <p class="mb-1">${item.description}</p>
                                        <small class="text-muted">Click aqui para ir al proceso.</small>
                                    </a>
                                </c:forEach>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>