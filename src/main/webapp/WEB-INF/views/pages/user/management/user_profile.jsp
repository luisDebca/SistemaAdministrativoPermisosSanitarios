<%--@elvariable id="user" type="edu.ues.aps.users.management.model.AbstractPersonal"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-purple rounded box-shadow">
                <div class="lh-100">
                    <h4 class="mb-0 text-white lh-100">Mi Perfil</h4>
                </div>
            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h6 class="border-bottom border-gray pb-2 mb-0">Información personal</h6>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Nombre</strong>
                        ${user.first_name}
                    </p>
                </div>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Apellidos</strong>
                        ${user.last_name}
                    </p>
                </div>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Cargo</strong>
                        ${user.charge}
                    </p>
                </div>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Correo eléctronico</strong>
                        ${user.email}
                    </p>
                </div>
                <p class="d-block text-right mt-3">
                    <a href="<c:url value="/edit-profile"/>" id="edit-uset">Editar perfil</a>
                </p>
            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h6 class="border-bottom border-gray pb-2 mb-0">Información de usuario</h6>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Nombre de usuario</strong>
                        ${user.user.username}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Fecha de ingreso</strong>
                        ${user.user.joining_date}
                    </p>
                </div>
                <p class="d-block text-right mt-3">
                    <a href="<c:url value="/edit-password"/>" id="pass-edit">Cambiar contraseña</a>
                </p>
                <p class="d-block text-right mt-3">
                    <a href="<c:url value="/logout"/>" id="log-out">Cerrar Sesión</a>
                </p>
            </div>
        </div>
    </div>
</main>