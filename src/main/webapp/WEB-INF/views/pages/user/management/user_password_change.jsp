<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value='/'/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value='/my-profile'/>">Mi perfil</a></li>
                    <li class="breadcrumb-item active">Cambiar contraseña</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">

                    <%--@elvariable id="password" type="edu.ues.aps.users.management.controller.PasswordDTO"--%>
                    <form:form id="main" method="POST" modelAttribute="password">
                        <h5>Información de usuario</h5>
                        <hr class="my-4">

                        <div class="form-group row">
                            <label for="current_password" class="col-2 col-form-label">Contraseña actual</label>
                            <div class="col-4">
                                <form:input
                                        id="current_password"
                                        class="form-control"
                                        type="password"
                                        path="current_password"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="new_password" class="col-2 col-form-label">Nueva contraseña</label>
                            <div class="col-4">
                                <form:input
                                        id="new_password"
                                        class="form-control"
                                        type="password"
                                        path="new_password"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="current_password_confirmation" class="col-2 col-form-label">Confirmar nueva
                                contraseña</label>
                            <div class="col-4">
                                <input
                                        id="current_password_confirmation"
                                        name="current_password_confirmation"
                                        class="form-control"
                                        type="password"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <hr class="my-4">
                        <input type="submit" value="Actualizar" class="btn btn-primary"/>
                        <a class="btn btn-secondary" href="<c:url value='/my-profile' />">Cancelar</a>
                    </form:form>

                </div>
            </div>
        </div>
    </div>
</main>