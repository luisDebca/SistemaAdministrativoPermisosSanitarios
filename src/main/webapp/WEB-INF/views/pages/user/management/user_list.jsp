<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value = "/"/>'>Inicio</a></li>
                    <li class="breadcrumb-item active">Gesti�n de personal de ventanilla</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-8">
                            <h5 class="card-title">Usuarios registrados</h5>
                            <h6 class="card-subtitle text-muted">Usuarios registrados en el sistema.</h6>
                        </div>
                        <div class="col-4 text-right">
                            <div class="btn-group-vertical">
                                <a href="<c:url value="/user/new"/>" role="button" class="btn btn-outline-secondary"><i
                                        class="fa fa-plus"></i> Agregar nuevo
                                    usuario</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover" id="dataTable" cellspacing="0" width="100%">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th>Usuario</th>
                        <th>Apellidos</th>
                        <th>Nombre</th>
                        <th>Correo Electronico</th>
                        <th>Registrado</th>
                        <th>Ultimo ingreso</th>
                        <th>Activo</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="users" type="java.util.List<edu.ues.aps.users.management.dto.AbstractPersonalUserDTO>"--%>
                    <c:forEach items="${users}" var="item">
                        <tr>
                            <td class="id_personal" hidden>${item.id_personal}</td>
                            <td class="username" align="center">${item.user_username}</td>
                            <td>${item.personal_last_name}</td>
                            <td>${item.personal_first_name}</td>
                            <td>${item.personal_email}</td>
                            <td>${item.user_joining_date_formatted}</td>
                            <td>${item.user_last_login_formatted}</td>
                            <td align="center">
                                <div class="custom-control custom-checkbox my-1 mr-sm-2">
                                    <c:if test="${item.user_is_active}">
                                        <i class="fa fa-check fa-lg"></i>
                                    </c:if>
                                    <c:if test="${not item.user_is_active}">
                                        <i class="fa fa-times fa-lg"></i>
                                    </c:if>
                                </div>
                            </td>
                            <td class="cell-center-middle">
                                <div class="btn-group" role="group">
                                    <a href="<c:url value='/user/${item.id_personal}/edit'/>"
                                       class="btn btn-outline-secondary" data-toggle="tooltip" title="Editar Usuario"><i
                                            class="fa fa-pencil-square-o "></i></a>
                                    <a href="<c:url value='/user/${item.id_personal}/info'/>"
                                       class="btn btn-outline-secondary" data-toggle="tooltip"
                                       title="Mostrar detalles del usuario"><i class="fa fa-info-circle"
                                                                               aria-hidden="true"></i></a>
                                        <%--@elvariable id="loggedinuser" type="java.lang.String"--%>
                                    <c:if test="${item.user_username != loggedinuser}">
                                        <a href="javascript:"
                                           class="btn btn-outline-secondary delete-user-btn" data-toggle="tooltip"
                                           title="Eliminar Usuario"><i class="fa fa-trash"></i></a>
                                    </c:if>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="user-delete-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel"
     aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="user-delete-title">Eliminar usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-warning fa-2x"></i>
                <h5>�Eliminar usuario?</h5>
                <p class="text-muted">Se eliminara permanentemente el usuario del sistema. Se recomienda deshabilitar
                    los usuarios en lugar de eliminarlos permanentement.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a href="javascript:" id="delete-btn" role="button" class="btn btn-danger">Eliminar</a>
            </div>
        </div>
    </div>
</div>

