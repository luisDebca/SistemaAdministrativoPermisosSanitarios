<%--@elvariable id="personal" type="edu.ues.aps.users.management.dto.AbstractPersonalUserDTO"--%>
<%--@elvariable id="profiles" type="java.util.List<edu.ues.aps.users.management.dto.ProfileDTO>"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value = "/user" />'>Inicio</a></li>
                    <li class="breadcrumb-item active">Detalles del usuario</li>
                </ol>
            </nav>

            <div class="row">
                <div class="col-sm-6">
                    <div class="card">
                            <div class="card-body">
                                <h4 class="card-title">Informacion Personal</h4>
                                <hr class="my-4">
                                <fieldset disabled>
                                    <div class="form-group">
                                        <label for="personal-name">Nombre</label>
                                        <input id="personal-name" type="text" class="form-control"
                                               value="${personal.personal_first_name}"
                                           readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="personal-surname">Apellido</label>
                                        <input id="personal-surname" type="text" class="form-control"
                                               value="${personal.personal_last_name}"
                                           readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="personal-charge">Cargo</label>
                                        <input id="personal-charge" type="text" class="form-control"
                                               value="${personal.personal_charge}"
                                           readonly>
                                    </div>
                                    <div class="form-group">
                                        <label for="personal-email">CorreoElectronico</label>
                                        <input id="personal-email" type="text" class="form-control"
                                               value="${personal.personal_email}" readonly>
                                    </div>
                                </fieldset>
                                <a href="<c:url value="/user"/>" role="button" class="btn btn-outline-secondary mt-4"><i
                                        class="fa fa-reply"></i> Lista
                                    de usuarios</a>
                            </div>
                    </div>
                </div>
                <div class="col-sm-6">
                    <div class="card">
                        <fieldset disabled>
                            <div class="card-body">
                                <h4 class="card-title">Informacion de Usuario</h4>
                                <hr class="my-4">
                                <div class="form-group">
                                    <label for="username">Nombre de Usuario</label>
                                    <input id="username" type="text" class="form-control"
                                           value="${personal.user_username}" readonly>
                                </div>

                                <label class="col-md-3 control-lable">Roles</label>
                                <c:forEach var="item" items="${profiles}">
                                <ul>
                                    <li>${item.profile_type}</li>
                                </ul>
                                </c:forEach>
                            </div>
                        </fieldset>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>