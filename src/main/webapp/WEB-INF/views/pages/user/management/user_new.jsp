<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value='/'/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value='/user'/>">Usuarios Registrados</a></li>
                    <li class="breadcrumb-item active">Nuevo Usuario</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">
                    <%--@elvariable id="personal" type="edu.ues.aps.users.management.model.Personal"--%>
                    <form:form id="main" method="POST" modelAttribute="personal">
                        <form:hidden path="id"/>
                        <form:hidden path="user.id"/>

                        <h5>Informaci�n Personal</h5>
                        <hr class="my-4">
                        <div class="form-group row">
                            <label class="col-2 col-form-label">Nombres</label>
                            <div class="col-6">
                                <form:input
                                        id="personal_first_name"
                                        class="form-control"
                                        type="text"
                                        path="first_name"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Apellidos</label>
                            <div class="col-6">
                                <form:input
                                        id="personal_last_name"
                                        class="form-control"
                                        type="text"
                                        path="last_name"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Denominaci�n</label>
                            <div class="col-md-2">
                                <form:select id="personal_designation" path="designation" class="custom-select">
                                    <form:option value="">Ninguno</form:option>
                                    <form:option value="Sr.">Se�or</form:option>
                                    <form:option value="Sra.">Se�ora</form:option>
                                    <form:option value="Srta.">Se�orita</form:option>
                                    <form:option value="Lic.">Licenciado / Licenciada</form:option>
                                    <form:option value="Ing.">Ingeniero / Ingeniera</form:option>
                                    <form:option value="Prof.">Profesor</form:option>
                                    <form:option value="Dir.">Director</form:option>
                                </form:select>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Cargo</label>
                            <div class="col-6">
                                <form:input
                                        id="personal_charge"
                                        class="form-control"
                                        type="text"
                                        path="charge"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Correo Electronico</label>
                            <div class="col-6">
                                <form:input
                                        id="personal_email"
                                        class="form-control"
                                        type="email"
                                        path="email"/>
                            </div>
                        </div>

                        <h5>Informaci�n de usuario</h5>
                        <hr class="my-4">

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Nombre de Usuario</label>
                            <div class="col-4">
                                <form:input
                                        id="personal_user_username"
                                        class="form-control"
                                        type="text"
                                        path="user.username"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Contrase�a</label>
                            <div class="col-4">
                                <form:input
                                        id="personal_user_password"
                                        class="form-control"
                                        type="password"
                                        path="user.password"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label for="personal_user_password_confirmation" class="col-2 col-form-label">Confirmar
                                Contrase�a</label>
                            <div class="col-4">
                                <input
                                        id="personal_user_password_confirmation"
                                        name="user_password_confirmation"
                                        class="form-control"
                                        type="password"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <h5>Permisos</h5>
                        <hr class="my-4">

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Permiso para ingresar</label>
                            <div class="col-4">
                                <label class="custom-control custom-checkbox">
                                    <form:checkbox class="form-check-input" path="user.is_active"/>
                                    <span class="form-check-label">Activar usuario</span>
                                </label>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Asignaci�n de Permisos</label>
                            <div class="col-4">
                                    <%--@elvariable id="profiles" type="java.util.Map"--%>
                                <div class="form-group">
                                    <form:select id="new_user_profiles" path="user.profiles" class="form-control"
                                                 multiple="multiple" items="${profiles}"/>
                                </div>
                            </div>
                        </div>

                        <hr class="my-4">
                        <input type="submit" value="Registrar" class="btn btn-primary"/>
                        <a class="btn btn-secondary" href="<c:url value='/user' />">Cancelar</a>

                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>