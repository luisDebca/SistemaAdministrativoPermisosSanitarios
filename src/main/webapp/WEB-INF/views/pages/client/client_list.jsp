<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="id_establishment" type="java.lang.Long"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/owner/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item">Usuarios de ventanilla de permisos sanitarios</li>
                </ol>
            </nav>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <h5 class="card-title">Listado de usuarios de ventanilla de permisos registrados</h5>
                            <p class="text-muted">Representantes legales, Apoderados y Personal autorizado.</p>
                        </div>
                        <div class="col-6 text-right">
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover" width="100%" cellspacing="0" id="dataTable">
                    <thead>
                    <tr>
                        <th hidden></th>
                        <th>Nombre</th>
                        <th>Nacionalidad</th>
                        <th>Identificaci�n</th>
                        <th>Contacto</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="clients" type="java.util.List<edu.ues.aps.process.base.dto.AbstractClientDTO>"--%>
                    <c:forEach items="${clients}" var="item">
                        <tr>
                            <td class="id_client" hidden>${item.id_client}</td>
                            <td>${item.client_name}</td>
                            <td>${item.client_nationally}</td>
                            <td>
                                <ul style="list-style: none">
                                    <c:if test="${not empty item.client_DUI}">
                                        <li>DUI: ${item.client_DUI}</li>
                                    </c:if>
                                    <c:if test="${not empty item.client_passport}">
                                        <li>Pasaporte: ${item.client_passport}</li>
                                    </c:if>
                                    <c:if test="${not empty item.client_resident_card}">
                                        <li>CR: ${item.client_resident_card}</li>
                                    </c:if>
                                    <c:if test="${empty item.client_DUI and empty item.client_passport and empty item.client_resident_card}">Sin identificaci�n</c:if>
                                </ul>
                            </td>
                            <td>
                                <ul style="list-style: none">
                                    <c:if test="${not empty item.client_telephone}">
                                        <li>Tel�fono: ${item.client_telephone}</li>
                                    </c:if>
                                    <c:if test="${not empty item.client_email}">
                                        <li>Correo Electronico: ${item.client_email}</li>
                                    </c:if>
                                    <c:if test="${empty item.client_email and empty item.client_telephone}">
                                        <li>Sin contactos</li>
                                    </c:if>
                                </ul>
                            </td>
                            <td align="center" class="cell-center-middle">
                                <div class="btn-group" role="group">
                                    <a class="btn btn-outline-secondary" role="button"
                                       href="<c:url value="/client/${item.id_client}/info"/>"
                                       data-toggle="tooltip" title="Ver informaci�n"><i class="fa fa-folder-open-o"
                                                                                       aria-hidden="true"></i></a>
                                    <a class="btn btn-outline-secondary" role="button"
                                       href="<c:url value="/client/${item.id_client}/edit"/>"
                                       data-toggle="tooltip" title="Editar"><i class="fa fa-pencil-square-o"
                                                                                       aria-hidden="true"></i></a>
                                    <a class="btn btn-outline-secondary disabled_href delete-client-btn"
                                       role="button" href="#" data-toggle="tooltip" title="Eliminar"><i
                                            class="fa fa-trash" aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="client-delete-confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Eliminar usuario</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-warning fa-3x"></i>
                <h5>�Desea eliminar el usuario permanentemente?</h5>
                <p class="text-muted small">Si se elimina el usuario se perderan todos los datos referentes a el y no podra ser usado en procesos futuros.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a id="delete-btn" role="button" href="#" class="btn btn-danger">Aceptar</a>
            </div>
        </div>
    </div>
</div>