<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--@elvariable id="id_owner" type="java.lang.Long"--%>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/owner/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/client/all" />'>Usuarios</a></li>
                    <li class="breadcrumb-item">Usuarios de ventanilla de permisos sanitarios</li>
                </ol>
            </nav>

            <div class="information-panel">
            </div>

            <div class="card mb-4">
                <div class="card-body">
                    <div class="row">
                        <div class="col-6">
                            <div id="id_owner" hidden="hidden">${id_owner}</div>
                            <h5 class="card-title">Listado de usuarios registrados por el propietario</h5>
                            <h6><a id="owner_name" href="<c:url value="/owner/${id_owner}"/>"></a></h6>
                            <div id="owner-extra-info" class="small text-muted"></div>
                        </div>
                        <div class="col-6 text-right">
                            <a class="btn btn-outline-primary" href="<c:url value="/owner/${id_owner}/client/new"/> "
                               role="button"><i class="fa fa-plus"></i> Agregar nuevo</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="table-responsive">
                <table class="table table-hover" width="100%" cellspacing="0" id="dataTable">
                    <thead>
                    <tr>
                        <th hidden></th>
                        <th hidden></th>
                        <th>Nombre</th>
                        <th>Denominaci�n</th>
                        <th>Nacionalidad</th>
                        <th>Identificaci�n</th>
                        <th>Contacto</th>
                        <th>Acciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="clients" type="java.util.List<edu.ues.aps.process.base.dto.AbstractClientDTO>"--%>
                    <c:forEach items="${clients}" var="item">
                        <tr>
                            <td class="id_client" hidden>${item.id_client}</td>
                            <td class="client_type" hidden>${item.clientType}</td>
                            <td>${item.client_name}</td>
                            <td>${item.type}</td>
                            <td>${item.client_nationally}</td>
                            <td>
                                <ul style="list-style: none">
                                    <c:if test="${not empty item.client_DUI}">
                                        <li>DUI: ${item.client_DUI}</li>
                                    </c:if>
                                    <c:if test="${not empty item.client_passport}">
                                        <li>Pasaporte: ${item.client_passport}</li>
                                    </c:if>
                                    <c:if test="${not empty item.client_resident_card}">
                                        <li>CR: ${item.client_resident_card}</li>
                                    </c:if>
                                    <c:if test="${empty item.client_DUI and empty item.client_passport and empty item.client_resident_card}">Sin identificaci�n</c:if>
                                </ul>
                            </td>
                            <td>
                                <ul style="list-style: none">
                                    <c:if test="${not empty item.client_telephone}">
                                        <li>Tel�fono: ${item.client_telephone}</li>
                                    </c:if>
                                    <c:if test="${not empty item.client_email}">
                                        <li>Correo Electronico: ${item.client_email}</li>
                                    </c:if>
                                    <c:if test="${empty item.client_email and empty item.client_telephone}">
                                        <li>Sin contactos</li>
                                    </c:if>
                                </ul>
                            </td>
                            <td align="center" class="cell-center-middle">
                                <div class="btn-group" role="group">
                                    <a class="btn btn-outline-secondary" role="button"
                                       href="<c:url value="/owner/${id_owner}/client/${item.id_client}/info"/>"
                                       data-toggle="tooltip" title="Ver informaci�n"><i class="fa fa-folder-open-o"
                                                                                       aria-hidden="true"></i></a>
                                    <a class="btn btn-outline-secondary" role="button"
                                       href="<c:url value="/owner/${id_owner}/client/${item.id_client}/type/${item.clientType}/edit"/>"
                                       data-toggle="tooltip" title="Editar"><i class="fa fa-pencil-square-o"
                                                                                       aria-hidden="true"></i></a>
                                    <a class="btn btn-outline-secondary disabled_href remove-client-btn"
                                       role="button" href="#" data-toggle="tooltip" title="Remover"><i
                                            class="fa fa-trash" aria-hidden="true"></i></a>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<div class="modal fade" id="client-remove-confirmation-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLongTitle">Remover usuarios de la lista</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-warning fa-3x"></i>
                <h5>�Desea remover el usuario de la lista?</h5>
                <p class="text-muted small">Si se remueve el usuario de la lista no podra ser utilizado en los procesos que se lleven a cabo por los establecimientos y veh�culos asociados al propietario.</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <a id="remove-btn" role="button" href="#" class="btn btn-warning">Aceptar</a>
            </div>
        </div>
    </div>
</div>