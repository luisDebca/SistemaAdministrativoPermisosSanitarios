<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>

<%--@elvariable id="id_owner" type="java.lang.Long"--%>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href='<c:url value="/owner/index" />'>Inicio</a></li>
                    <li class="breadcrumb-item"><a href='<c:url value="/client/all"/>'>Usuarios</a></li>
                    <c:if test="${not empty id_owner}">
                        <li class="breadcrumb-item"><a
                                href='<c:url value="/owner/${id_owner}/client/all"/>'>Usuarios registrados</a></li>
                    </c:if>
                    <li class="breadcrumb-item">Editar usuario</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="card">
                <div class="card-body">
                    <h5 class="card-title">Formulario para personas autorizadas</h5>
                    <h6 class="card-subtitle mb-2 text-muted">Ingrese los datos solicitados</h6>
                    <hr class="my-4">
                    <%--@elvariable id="client" type="edu.ues.aps.process.base.dto.ClientDTO"--%>
                    <form:form modelAttribute="client" method="POST" id="main">
                        <form:hidden path="id_client"/>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Nombre completo</label>
                            <div class="col-8">
                                <form:input
                                        class="form-control"
                                        type="text"
                                        path="client_name"
                                        required="required"/>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-2 col-form-label">Denominaci�n</label>
                            <div class="col-4">
                                <form:select id="type" path="clientType" class="custom-select">
                                    <option value="INVALID" disabled="disabled">Seleccione una opci�n</option>
                                    <form:option value="LEGAL">REPRESENTANTE LEGAL</form:option>
                                    <form:option value="APODERADO">APODERADO</form:option>
                                    <form:option value="AUTORIZADO">PERSONA AUTORIZADA</form:option>
                                </form:select>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>


                        <div class="form-group row">
                            <label class="col-2 col-form-label">Nacionalidad</label>
                            <div class="col-4">
                                <form:select id="nationally" path="client_nationally" class="custom-select">
                                    <form:option value="Salvadore�a">Salvadore�a</form:option>
                                    <form:option value="Afgano">Afgano</form:option>
                                    <form:option value="Alem�n">Alem�n</form:option>
                                    <form:option value="�rabe">�rabe</form:option>
                                    <form:option value="Argentino">Argentino</form:option>
                                    <form:option value="Australiano">Australiano</form:option>
                                    <form:option value="Belga">Belga</form:option>
                                    <form:option value="Boliviano">Boliviano</form:option>
                                    <form:option value="Brasilero">Brasilero</form:option>
                                    <form:option value="Camboyano">Camboyano</form:option>
                                    <form:option value="Canadiense">Canadiense</form:option>
                                    <form:option value="Chileno">Chileno</form:option>
                                    <form:option value="Chino">Chino</form:option>
                                    <form:option value="Colombiano">Colombiano</form:option>
                                    <form:option value="Coreano">Coreano</form:option>
                                    <form:option value="Costarricense">Costarricense</form:option>
                                    <form:option value="Cubano">Cubano</form:option>
                                    <form:option value="Dan�s">Dan�s</form:option>
                                    <form:option value="Ecuatoriano">Ecuatoriano</form:option>
                                    <form:option value="Egipcio">Egipcio</form:option>
                                    <form:option value="Espa�ol">Espa�ol</form:option>
                                    <form:option value="Estadounidense">Estadounidense</form:option>
                                    <form:option value="Estonio">Estonio</form:option>
                                    <form:option value="Etiope">Etiope</form:option>
                                    <form:option value="Filipino">Filipino</form:option>
                                    <form:option value="Finland�s">Finland�s</form:option>
                                    <form:option value="Franc�s">Franc�s</form:option>
                                    <form:option value="Gal�s">Gal�s</form:option>
                                    <form:option value="Griego">Griego</form:option>
                                    <form:option value="Guatemalteco">Guatemalteco</form:option>
                                    <form:option value="Haitiano">Haitiano</form:option>
                                    <form:option value="Holand�s">Holand�s</form:option>
                                    <form:option value="Hondure�o">Hondure�o</form:option>
                                    <form:option value="Indon�s">Indon�s</form:option>
                                    <form:option value="Ingl�s">Ingl�s</form:option>
                                    <form:option value="Irland�s">Irland�s</form:option>
                                    <form:option value="Israeli">Israeli</form:option>
                                    <form:option value="Italiano">Italiano</form:option>
                                    <form:option value="Japon�s">Japon�s</form:option>
                                    <form:option value="Jordano">Jordano</form:option>
                                    <form:option value="Laosiano">Laosiano</form:option>
                                    <form:option value="Let�n">Let�n</form:option>
                                    <form:option value="Leton�s">Leton�s</form:option>
                                    <form:option value="Malayo">Malayo</form:option>
                                    <form:option value="Marroqu�">Marroqu�</form:option>
                                    <form:option value="Mexicano">Mexicano</form:option>
                                    <form:option value="Nicaragense">Nicaragense</form:option>
                                    <form:option value="Noruego">Noruego</form:option>
                                    <form:option value="Neoceland�s">Neoceland�s</form:option>
                                    <form:option value="Paname�o">Paname�o</form:option>
                                    <form:option value="Paraguayo">Paraguayo</form:option>
                                    <form:option value="Peruano">Peruano</form:option>
                                    <form:option value="Polaco">Polaco</form:option>
                                    <form:option value="Portugu�s">Portugu�s</form:option>
                                    <form:option value="Puertorrique�o">Puertorrique�o</form:option>
                                    <form:option value="Dominicano">Dominicano</form:option>
                                    <form:option value="Rumano">Rumano</form:option>
                                    <form:option value="Ruso">Ruso</form:option>
                                    <form:option value="Sueco">Sueco</form:option>
                                    <form:option value="Suizo">Suizo</form:option>
                                    <form:option value="Tailand�s">Tailand�s</form:option>
                                    <form:option value="Taiwanes">Taiwanes</form:option>
                                    <form:option value="Turco">Turco</form:option>
                                    <form:option value="Ucraniano">Ucraniano</form:option>
                                    <form:option value="Uruguayo">Uruguayo</form:option>
                                    <form:option value="Venezolano">Venezolano</form:option>
                                    <form:option value="Vietnamita">Vietnamita</form:option>
                                </form:select>
                            </div>
                            <div class="required-type" data-toggle="tooltip" data-placement="top"
                                 title="Este campo es obligatorio">*
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">N�mero de DUI</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="text"
                                        path="client_DUI"
                                        required="required"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Pasaporte</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="text"
                                        path="client_passport"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Carnet de residente</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="text"
                                        path="client_resident_card"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Correo Electr�nico</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="email"
                                        path="client_email"/>
                            </div>
                        </div>

                        <div class="form-group row">
                            <label class="col-2 col-form-label">Tel�fono</label>
                            <div class="col-4">
                                <form:input
                                        class="form-control"
                                        type="tel"
                                        path="client_telephone"/>

                            </div>
                        </div>
                        <hr class="my-4">
                        <button type="submit" class="btn btn-primary">Actualizar</button>
                        <c:if test="${empty id_owner}">
                            <a href="<c:url value = '/client/all'/>"
                               class="btn btn-secondary">Cancelar</a>
                        </c:if>
                        <c:if test="${not empty id_owner}">
                            <a href="<c:url value = '/owner/${id_owner}/client/all'/>"
                               class="btn btn-secondary">Cancelar</a>
                        </c:if>
                    </form:form>
                </div>
            </div>
        </div>
    </div>
</main>