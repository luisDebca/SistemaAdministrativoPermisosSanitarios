<%--@elvariable id="client" type="edu.ues.aps.process.base.dto.AbstractClientDTO"--%>
<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/owner/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/client/all"/>">Usuarios</a></li>
                    <li class="breadcrumb-item active" aria-current="page">Informaci�n</li>
                </ol>
            </nav>

            <div class="d-flex align-items-center p-3 my-3 text-white-50 bg-dark rounded box-shadow">
                <div class="lh-100">
                    <h4 class="mb-0 text-white lh-100">Informaci�n de usuario de ventanilla <strong>${client.client_name}</strong></h4>
                </div>
            </div>

            <div class="my-3 p-3 bg-white rounded box-shadow">
                <h6 class="border-bottom border-gray pb-2 mb-0">Informaci�n</h6>

                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Nombre</strong>
                        ${client.client_name}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Nacionalidad</strong>
                        ${client.client_nationally}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">DUI</strong>
                        ${client.client_DUI}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Pasaporte</strong>
                        ${client.client_passport}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Tarjeta de residencia</strong>
                        ${client.client_resident_card}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Correo electr�nico</strong>
                        ${client.client_email}
                    </p>
                </div>
                <div class="media text-muted pt-3">
                    <p class="media-body pb-3 mb-0 lh-125 border-bottom border-gray">
                        <strong class="d-block text-gray-dark">Tel�fono</strong>
                        ${client.client_telephone}
                    </p>
                </div>
                <p class="d-block text-right mt-3">
                    <a href="<c:url value="#"/>">Editar usuario</a>
                </p>
            </div>
        </div>
    </div>
</main>