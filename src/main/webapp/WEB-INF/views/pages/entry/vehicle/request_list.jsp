<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/dispatch/index"/>">Inicio</a></li>
                    <li class="breadcrumb-item active">Ingreso de solicitudes de veh�culos</li>
                </ol>
            </nav>

            <div class="information-panel"></div>

            <div class="table-responsive">
                <table class="table table-hover vehicle-table" width="100%" id="dataTable" cellspacing="0">
                    <thead>
                    <tr>
                        <th hidden="hidden"></th>
                        <th hidden="hidden"></th>
                        <th>Folder</th>
                        <th>N� Solicitudes</th>
                        <th>Propietario</th>
                        <th>Compa�ia de distribuci�n</th>
                        <th>Destino de distribuci�n</th>
                        <th>Lugar de mercancia</th>
                        <th>Direcci�n de estancia</th>
                        <th>Permiso</th>
                        <th>SIBASI/UCSF</th>
                        <th>Entrega UCSF</th>
                        <th>Opciones</th>
                    </tr>
                    </thead>
                    <tbody>
                    <%--@elvariable id="requests" type="java.util.List<edu.ues.aps.process.dispatch.vehicle.dto.AbstractVehicleRequestDispatchDTO>"--%>
                    <c:forEach items="${requests}" var="item">
                        <tr>
                            <td hidden="hidden" class="id_owner">${item.id_owner}</td>
                            <td hidden="hidden" class="id_bunch">${item.id_bunch}</td>
                            <td class="cell-center-middle">${item.request_folder}</td>
                            <td class="cell-center-middle"><h5>${item.request_count}</h5></td>
                            <td><a href="<c:url value="/owner/${item.id_owner}"/>">${item.owner_name}</a></td>
                            <td>${item.bunch_distribution_company}</td>
                            <td>${item.bunch_distribution_destination}</td>
                            <td>${item.bunch_marketing_place}</td>
                            <td>${item.bunch_address}</td>
                            <td>${item.certification_type}</td>
                            <td>${item.sibasi} / ${item.ucsf}</td>
                            <td class="cell-center-middle">
                                <c:if test="${not item.ucsf_delivery}"><span class="badge badge-secondary">Pendiente<br>Entrega</span></c:if>
                                <c:if test="${item.ucsf_delivery}"><span
                                        class="badge badge-success">Enviado a<br>UCSF</span></c:if>
                            </td>
                            <td class="cell-center-middle table-options">
                                <div class="btn-group-vertical">
                                    <div class="btn-group" role="group" aria-label="groupInf">
                                        <a href="javascript:"
                                           class="btn btn-outline-secondary disabled_href show_brief_bunch_information"
                                           data-toggle="tooltip" title="Ver solicitudes contenidas"><i class="fa fa-th"
                                                                                                       aria-hidden="true"></i></a>
                                        <a href="<c:url value="/dispatch/email-sender"/> " role="button"
                                           class="btn btn-outline-secondary" data-toggle="tooltip" data-placement="top"
                                           title="Enviar solicitud por email a UCSF"><i class="fa fa-envelope-o"
                                                                                        aria-hidden="true"></i></a>
                                        <a href="javascript:" role="button"
                                           class="btn btn-outline-secondary show-vehicle-folders"
                                           data-toggle="tooltip" data-placement="top"
                                           title="Generar Portada de Expediente"><i class="fa fa-file-pdf-o"
                                                                                  aria-hidden="true"></i></a>
                                        <a href="javascript:" role="button"
                                           class="btn btn-outline-secondary request-entry-report-btn"
                                           data-toggle="tooltip" data-placement="top"
                                           title="Generar comprobante de ingreso"><i class="fa fa-file-pdf-o"
                                                                                     aria-hidden="true"></i></a>
                                        <a href="javascript:" role="button"
                                           class="btn btn-outline-secondary show-client-selector-modal"
                                           data-toggle="tooltip" data-placement="top"
                                           title="Generar comprobante de proceso"><i class="fa fa-file-pdf-o"
                                                                                     aria-hidden="true"></i></a>
                                        <c:if test="${not item.ucsf_delivery}">
                                            <a href="#"
                                               class="btn btn-outline-secondary disabled_href show-ucsf-request-delivery-modal"
                                               data-toggle="tooltip" title="Marcar envio a UCSF"><i class="fa fa-send-o"
                                                                                                    aria-hidden="true"></i></a>
                                        </c:if>
                                    </div>
                                    <div class="btn-group" role="group" aria-label="groupConf">
                                        <c:if test="${item.ucsf_delivery}">
                                            <button type="button"
                                                    class="btn btn-outline-secondary disabled_href move_to_next_process_btn"
                                                    data-toggle="tooltip" title="Pasar a seguimiento de solicitudes"><i
                                                    class="fa fa-arrow-circle-right"></i></button>
                                        </c:if>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_forgotten_btn"
                                                data-toggle="tooltip" title="Remover solicitud del proceso"><i
                                                class="fa fa-minus" aria-hidden="true"></i></button>
                                        <button type="button"
                                                class="btn btn-outline-secondary disabled_href move_to_next_rejected_btn"
                                                data-toggle="tooltip" title="Cancelar solicitud"><i class="fa fa-ban"
                                                                                                    aria-hidden="true"></i>
                                        </button>
                                    </div>
                                </div>
                            </td>
                        </tr>
                    </c:forEach>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</main>

<div hidden="hidden" id="bunch-options"></div>

<!-- Request process validator report client selector modal-->
<div class="modal fade" id="request-validator-client-selection-modal" tabindex="-1" role="dialog">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">PDF Opciones</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="form-group">
                    <label for="client-select" class="col-8 col-form-label">Nombre del solicitante</label>
                    <form>
                        <div class="col-12">
                            <select id="client-select" class="custom-select"></select>
                        </div>
                    </form>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cerrar</button>
                <button id="generate-process-validator-report-btn" type="button" class="btn btn-primary">Generar PDF
                </button>
            </div>
        </div>
    </div>
</div>

<!-- UCSF DELIVERY CONFIRMATION MODAL -->
<div class="modal fade" id="ucsf-delivery-confirmation-modal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Confirmar envio a UCSF</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body text-center">
                <i class="fa fa-send-o fa-2x"></i>
                <h5>�Marcar envio de expediente a UCSF?</h5>
                <p class="text-muted">Para continuar el proceso se debe enviar el expediente a la UCSF seleccionada</p>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
                <button id="update-request-deliver" type="button" class="btn btn-primary">Aceptar</button>
            </div>
        </div>
    </div>
</div>