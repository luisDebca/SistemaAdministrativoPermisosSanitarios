<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>


<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <nav aria-label="breadcrumb">
                <ol class="breadcrumb">
                    <li class="breadcrumb-item"><a href="<c:url value="/"/>">Inicio</a></li>
                    <li class="breadcrumb-item"><a href="<c:url value="/dispatch/index"/>">Ingreso de solicitudes</a>
                    </li>
                    <li class="breadcrumb-item active">Envio de correos</li>
                </ol>
            </nav>

            <div class="card">
                <div class="card-body">
                    <form id="main" method="post"
                          action="<c:url value='/dispatch/email-sender?${_csrf.parameterName}=${_csrf.token}'/>"
                          enctype="multipart/form-data">
                        <div class="row">
                            <div class="col-8">
                                <div class="form-group">
                                    <label>Para</label>
                                    <small class="text-muted">(Ingrese los correos de los destinatarios)</small>
                                    <input type="text" id="to_email_list" name="email.to" class="form-control">
                                </div>
                                <div class="form-group">
                                    <label>Asunto</label>
                                    <input id="email_subject" type="text" class="form-control" name="email.subject">
                                </div>
                                <div class="form-group">
                                    <label>CC</label>
                                    <input type="text" id="cc_email_list" name="email.cc" class="form-control">
                                </div>
                                <div class="custom-file mb-2">
                                    <input type="file" class="custom-file-input" id="files" name="files" multiple="multiple">
                                    <label class="custom-file-label" for="files">Seleccione el archivo a subir</label>
                                </div>

                                <div class="col mb-4">
                                    <div class="list-group" id="uploaded-files-list"></div>
                                </div>
                            </div>
                            <div class="col-4">
                                <img src="<c:url value="/static/img/email-icon.png"/>" width="300px" height="210px">
                            </div>
                            <div class="col-12">
                                <div id="froala-editor">
                                </div>
                                <textarea id="email_text" class="form-control" name="email.text"
                                          hidden="hidden"></textarea>
                            </div>
                            <div class="col">
                                <br><br>
                                <button id="send_email_btn" type="button" class="btn btn-primary">Enviar</button>
                                <button type="reset" class="btn btn-secondary">Limpiar</button>
                                <a href="<c:url value="/dispatch/index"/> " class="btn btn-secondary">Cancelar</a>

                            </div>
                        </div>

                    </form>
                </div>
            </div>
        </div>
    </div>
</main>
