<%@ page contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<main role="main" class="col-md-9 ml-sm-auto col-lg-10 px-4">
    <div class="d-flex justify-content-between flex-wrap flex-md-nowrap align-items-center pt-3 pb-2 mb-3 border-bottom">
        <div class="container-fluid">

            <div class="row">
                <div class="col shadow-sm p-3 mb-3 bg-white rounded text-secondary"><h3><i class="fa fa-paper-plane-o"
                                                                                           aria-hidden="true"></i>
                    Ingreso y envio de solicitudes</h3></div>
            </div>
            <%--@elvariable id="process" type="java.lang.String"--%>
            <div hidden="hidden" id="thisProcess">${process}</div>
            <div class="row">
                <div class="col">
                    <div class="card mb-3">
                        <div class="card-body">
                            <h5><i class="fa fa-building-o" aria-hidden="true"></i> Ultimos ingresos de establecimientos
                            </h5>
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item"><a class="nav-link active" id="es_today-tab" data-toggle="tab"
                                                        href="#es_today" role="tab" aria-controls="today"
                                                        aria-selected="true">Hoy <span id="es_today-count"
                                                                                       class="badge badge-pill badge-primary">0</span></a>
                                </li>
                                <li class="nav-item"><a class="nav-link" id="es_week-tab" data-toggle="tab"
                                                        href="#es_week" role="tab" aria-controls="week"
                                                        aria-selected="false">Esta semana <span id="es_week-count"
                                                                                                class="badge badge-pill badge-primary">0</span></a>
                                </li>
                                <li class="nav-item"><a class="nav-link" id="es_month-tab" data-toggle="tab"
                                                        href="#es_month" role="tab" aria-controls="month"
                                                        aria-selected="false">Este mes <span id="es_month-count"
                                                                                             class="badge badge-pill badge-primary">0</span></a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent1">
                                <div class="tab-pane show active" id="es_today" role="tabpanel"
                                     aria-labelledby="es_today-tab">
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">Folder</th>
                                            <th scope="col">Solicitud</th>
                                            <th scope="col">Resoluci�n</th>
                                            <th scope="col">Establecimiento</th>
                                            <th scope="col">Direcci�n</th>
                                            <th scope="col">Propietario</th>
                                        </tr>
                                        </thead>
                                        <tbody id="es_today-summary"></tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="es_week" role="tabpanel" aria-labelledby="es_week-tab">...
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">Folder</th>
                                            <th scope="col">Solicitud</th>
                                            <th scope="col">Resoluci�n</th>
                                            <th scope="col">Establecimiento</th>
                                            <th scope="col">Direcci�n</th>
                                            <th scope="col">Propietario</th>
                                        </tr>
                                        </thead>
                                        <tbody id="es_week-summary"></tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="es_month" role="tabpanel" aria-labelledby="es_month-tab">
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">Folder</th>
                                            <th scope="col">Solicitud</th>
                                            <th scope="col">Resoluci�n</th>
                                            <th scope="col">Establecimiento</th>
                                            <th scope="col">Direcci�n</th>
                                            <th scope="col">Propietario</th>
                                        </tr>
                                        </thead>
                                        <tbody id="es_month-summary"></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="mt-4">
                                <a href="<c:url value="/dispatch/establishment/list"/>" role="button"
                                   class="btn btn-outline-secondary">Ver todos</a>
                            </div>
                        </div>
                    </div>

                    <div class="card mb-3">
                        <div class="card-body">
                            <h5><i class="fa fa-truck" aria-hidden="true"></i> Ultimos ingresos de veh�culos</h5>
                            <ul class="nav nav-tabs" id="myTab2" role="tablist">
                                <li class="nav-item"><a class="nav-link active" id="vh_today-tab" data-toggle="tab"
                                                        href="#vh_today" role="tab" aria-controls="today"
                                                        aria-selected="true">Hoy <span id="vh_today-count"
                                                                                       class="badge badge-pill badge-primary">0</span></a>
                                </li>
                                <li class="nav-item"><a class="nav-link" id="vh_week-tab" data-toggle="tab"
                                                        href="#vh_week" role="tab" aria-controls="week"
                                                        aria-selected="false">Esta semana <span id="vh_week-count"
                                                                                                class="badge badge-pill badge-primary">0</span></a>
                                </li>
                                <li class="nav-item"><a class="nav-link" id="vh_month-tab" data-toggle="tab"
                                                        href="#vh_month" role="tab" aria-controls="month"
                                                        aria-selected="false">Este mes <span id="vh_month-count"
                                                                                             class="badge badge-pill badge-primary">0</span></a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent2">
                                <div class="tab-pane show active" id="vh_today" role="tabpanel"
                                     aria-labelledby="vh_today-tab">
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">Folder</th>
                                            <th scope="col">Veh�culos</th>
                                            <th scope="col">Direcci�n de permanencia</th>
                                            <th scope="col">Propietario</th>
                                        </tr>
                                        </thead>
                                        <tbody id="vh_today-summary"></tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="vh_week" role="tabpanel" aria-labelledby="vh_week-tab">...
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">Folder</th>
                                            <th scope="col">Veh�culos</th>
                                            <th scope="col">Direcci�n de permanencia</th>
                                            <th scope="col">Propietario</th>
                                        </tr>
                                        </thead>
                                        <tbody id="vh_week-summary"></tbody>
                                    </table>
                                </div>
                                <div class="tab-pane" id="vh_month" role="tabpanel" aria-labelledby="vh_month-tab">
                                    <table class="table table-sm">
                                        <thead>
                                        <tr>
                                            <th scope="col">Folder</th>
                                            <th scope="col">Veh�culos</th>
                                            <th scope="col">Direcci�n de permanencia</th>
                                            <th scope="col">Propietario</th>
                                        </tr>
                                        </thead>
                                        <tbody id="vh_month-summary"></tbody>
                                    </table>
                                </div>
                            </div>
                            <div class="mt-4">
                                <a href="<c:url value="/dispatch/vehicle/all"/>" role="button"
                                   class="btn btn-outline-secondary">Ver todos</a>
                            </div>
                        </div>
                    </div>

                    <div class="card">
                        <div class="card-body">
                            <h5><i class="fa fa-exchange" aria-hidden="true"></i> Envio de expedientes a UCSF</h5>
                            <table class="table table-sm">
                                <thead>
                                <tr>
                                    <th>SIBASI</th>
                                    <th>UCSF</th>
                                    <th>N�mero de solicitudes enviadas</th>
                                    <th>Numero de solicitudes recibidas</th>
                                    <th>Totales</th>
                                </tr>
                                </thead>
                                <tbody id="UCSF-summary"></tbody>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>