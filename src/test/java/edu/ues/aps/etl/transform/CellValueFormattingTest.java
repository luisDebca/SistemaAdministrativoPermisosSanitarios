package edu.ues.aps.etl.transform;

import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;

public class CellValueFormattingTest {

    private String folder_number;
    private String request_number;
    private String bad_folder_number;
    private String bad_request_number;

    @BeforeMethod
    public void setUp() {
        folder_number = "101-18";
        bad_folder_number = "10218";
        request_number = "06ABCD501-19";
        bad_request_number = "06H 01-17";
    }

    @Test
    public void testGetFolderNumber() {
        Assert.assertEquals(CellValueFormatting.getFolderNumber(folder_number),"101");
        Assert.assertEquals(CellValueFormatting.getFolderNumber(bad_folder_number),"102");
    }

    @Test
    public void testGetRequestCode() {
        Assert.assertEquals(CellValueFormatting.getRequestCode(request_number),"ABCD");
        Assert.assertEquals(CellValueFormatting.getRequestCode(bad_request_number),"H");
    }

    @Test
    public void testGetRequestNumber() {
        Assert.assertEquals(CellValueFormatting.getRequestNumber(request_number),"501");
        Assert.assertEquals(CellValueFormatting.getRequestNumber(bad_request_number),"01");
    }

    @Test
    public void testGetRequestYear() {
        Assert.assertEquals(CellValueFormatting.getRequestYear(folder_number),"18");
        Assert.assertEquals(CellValueFormatting.getRequestYear(request_number),"19");
        Assert.assertEquals(CellValueFormatting.getRequestYear(bad_request_number),"17");
    }

    @Test
    public void testGetDateTime() {
        Assert.assertEquals(CellValueFormatting.getDateTime("15/05/17"), LocalDate.of(2017,5,15));
        Assert.assertEquals(CellValueFormatting.getDateTime("18-12-18"), LocalDate.of(2018,12,18));
        Assert.assertEquals(CellValueFormatting.getDateTime("10/9/2018"),LocalDate.of(2018,9,10));
        Assert.assertEquals(CellValueFormatting.getDateTime("20-1-2019"),LocalDate.of(2019,1,20));
    }

    @Test
    public void testExtractAddressDetails(){
        String fullAddress =  "Cimas de san bartolo I, Pasaje 2 grupo 10, casa #5, Ilopango, San Salvador";
        String complexAddress = "Avenida los polacos #445 ,San Marcos, San Miguel";
        Assert.assertEquals(CellValueFormatting.extractAddressDetails(fullAddress),"Cimas de san bartolo I, Pasaje 2 grupo 10, casa #5");
        Assert.assertEquals(CellValueFormatting.extractMunicipality(fullAddress),"Ilopango");
        Assert.assertEquals(CellValueFormatting.extractAddressDetails(complexAddress),"Avenida los polacos #445");
        Assert.assertEquals(CellValueFormatting.extractMunicipality(complexAddress),"San Marcos");
    }
}