package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RequirementControlTemplateRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("DocumentTemplate.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Requirement.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("RequirementControlTemplate.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private RequirementControlTemplateCrudRepository crudRepository;

    @Autowired
    private RequirementDTOFinderRepository finderRepository;

    @Test
    public void testRemoveRequirementFromTemplate(){
        Assert.assertEquals(finderRepository.findAllRequirementsFromTemplate(1L).size(),2);
        crudRepository.delete(1L,1L);
        Assert.assertEquals(finderRepository.findAllRequirementsFromTemplate(1L).size(),1);
    }

    @Test
    public void testFindAllFromTemplate(){
        Assert.assertEquals(crudRepository.findAllFromTemplate(1L).size(),2);
        Assert.assertEquals(crudRepository.findAllFromTemplate(2L).size(),2);
        Assert.assertEquals(crudRepository.findAllFromTemplate(3L).size(),4);
        Assert.assertNotNull(crudRepository.findAllFromTemplate(1L).get(0).getRequirement().getRequirement());
        Assert.assertNotNull(crudRepository.findAllFromTemplate(1L).get(0).getTemplate().getTitle());
    }


}
