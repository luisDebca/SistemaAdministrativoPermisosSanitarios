package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import edu.ues.aps.process.requirement.base.dto.RequirementRowDTO;
import edu.ues.aps.process.requirement.base.model.RequirementStatusType;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.util.List;

public class RequirementRowDTORepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        return new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("RequirementDocumentDTO.xml"));
    }

    @Autowired
    private RequirementRowDTOFinderRepository finderRepository;

    @Autowired
    private RequirementRowPersistenceRepository persistenceRepository;

    @Test
    public void testFindAllRequirementRowsFromDocument(){
        Assert.assertEquals(finderRepository.findAllRowsFromDocument(1L).size(),3);
        Assert.assertEquals(finderRepository.findAllRowsFromDocument(2L).size(),3);
        Assert.assertEquals(finderRepository.findAllRowsFromDocument(3L).size(),3);
        Assert.assertEquals(finderRepository.findAllRowsFromDocument(4L).size(),3);
    }

    @Test
    public void testUpdateRequirementRow(){
        RequirementRowDTO rowDTO1 = new RequirementRowDTO(2L,2L,2L,"Requirement","edited observation", RequirementStatusType.APPROVED);
        RequirementRowDTO rowDTO2 = new RequirementRowDTO(4L,2L,2L,"Requirement","edited observation", RequirementStatusType.APPROVED);
        persistenceRepository.updateRequirementRow(rowDTO1);
        persistenceRepository.updateRequirementRow(rowDTO2);
        List<RequirementRowDTO> rows = finderRepository.findAllRowsFromDocument(2L);
        for (RequirementRowDTO row: rows){
            Assert.assertEquals(row.getRow_status_type(),RequirementStatusType.APPROVED);
        }
    }
}
