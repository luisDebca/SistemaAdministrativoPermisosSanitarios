package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import edu.ues.aps.process.base.model.Casefile;
import edu.ues.aps.process.requirement.base.model.*;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;
import java.util.List;

public class RequirementControlDocumentPersistenceRepositoryTest extends EntityRepositoryImplementTest {

    @Override
    protected IDataSet getDataset() throws Exception {
        return new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("RequirementControlDocumentPersistence.xml"));
    }

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    public void testSaveControlDocumentWithRequirementRows(){
        /*Casefile casefile = sessionFactory.getCurrentSession().createQuery("select casefile from Casefile casefile where casefile.id = 1",Casefile.class).getSingleResult();
        List<Requirement> requirements = sessionFactory.getCurrentSession().createQuery("select req from Requirement req",Requirement.class).getResultList();
        DocumentTemplate template = sessionFactory.getCurrentSession().createQuery("select template from DocumentTemplate template where template.id = 1",DocumentTemplate.class).getSingleResult();
        List<RequirementControlTemplate> templates = sessionFactory.getCurrentSession().createQuery("select templates from RequirementControlTemplate  templates",RequirementControlTemplate.class).getResultList();
        RequirementControlDocument document = new RequirementControlDocument();
        document.setStatus(true);
        document.setControl_document_created_on(LocalDateTime.now());
        casefile.addControlDocument(document);

//        sessionFactory.getCurrentSession().saveOrUpdate(casefile);
        sessionFactory.getCurrentSession().persist(document);
        sessionFactory.getCurrentSession().flush();

        document = sessionFactory.getCurrentSession().createQuery("select doc from RequirementControlDocument doc where doc.id = 1",RequirementControlDocument.class).getSingleResult();

        RequirementRow row1 = new RequirementRow();
        row1.setStatus(RequirementStatusType.APPROVED);
        row1.setObservation("obs__b");
        row1.setTemplate(templates.get(0));

        RequirementRow row2 = new RequirementRow();
        row2.setStatus(RequirementStatusType.FAULT);
        row2.setObservation("vvv___vvv");
        row2.setTemplate(templates.get(1));

        document.addRequirementRow(row1);
        document.addRequirementRow(row2);

        sessionFactory.getCurrentSession().merge(document);
        sessionFactory.getCurrentSession().flush();*/
    }
}
