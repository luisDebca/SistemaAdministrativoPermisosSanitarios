package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ControlDocumentDTORepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Process.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("RequirementDocumentDTO.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private ControlDocumentDTOFinderRepository finderRepository;

    @Test
    public void testFindAllRequestWithCompleteRequirements(){
        Assert.assertEquals(finderRepository.findAllRequest(true).size(),2);
        Assert.assertEquals(finderRepository.findAllRequest(false).size(),1);
    }
}
