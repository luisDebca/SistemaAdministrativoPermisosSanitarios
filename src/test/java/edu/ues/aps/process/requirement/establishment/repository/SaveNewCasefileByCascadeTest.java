package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import edu.ues.aps.process.area.establishment.model.CasefileEstablishment;
import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.area.establishment.model.EstablishmentType;
import edu.ues.aps.process.base.model.*;
import edu.ues.aps.process.requirement.base.model.RequirementControlDocument;
import edu.ues.aps.process.requirement.base.model.RequirementControlTemplate;
import edu.ues.aps.process.requirement.base.model.RequirementRow;
import edu.ues.aps.process.requirement.base.model.RequirementStatusType;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

public class SaveNewCasefileByCascadeTest extends EntityRepositoryImplementTest {

    @Autowired
    EstablishmentControlDocumentPersistenceRepository persistenceRepository;
    private OwnerEntity owner;
    private Establishment establishment;
    private CasefileEstablishment casefile;
    private RequirementControlDocument document;
    @Autowired
    private SessionFactory sessionFactory;

    @Override
    protected IDataSet getDataset() throws Exception {
        return new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("CasefileByCascadePersistence.xml"));
    }

    @Test
    public void testSaveOwnerEstablishmentCasefileDocumentByCascade() {
        initializeNaturalOwner();
        initializeEstablishment();
        initializeCasefile();
        initializeDocument();
        initializeRequirementRows();
        sessionFactory.getCurrentSession().persist(owner);
        initializeClientList();
        sessionFactory.getCurrentSession().flush();
    }

    public void initializeNaturalOwner() {
        owner = new NaturalEntity();
        owner.setName("Mar villa");
        owner.setNit("1244-457789-652-1");
    }

    public void initializeEstablishment() {
        establishment = new Establishment();
        establishment.setName("Villa Rest");
        establishment.setNIT("4577-854451-021-2");
        establishment.setActive(true);
        Address address = new Address();
        address.setDetails("...");
        address.setMunicipality(sessionFactory.getCurrentSession().get(Municipality.class, 1L));
        establishment.setAddress(address);
        establishment.setType(sessionFactory.getCurrentSession().get(EstablishmentType.class, 1L));
        owner.addEstablishment(establishment);
    }

    public void initializeCasefile() {
        casefile = new CasefileEstablishment();
        casefile.setApply_payment(true);
        casefile.setCertification_type(CertificationType.FIRST);
        casefile.setCertification_duration_in_years(casefile.getCertificationDurationInYears());
        casefile.setSection(CasefileSection.ESTABLISHMENT);
        casefile.setCreation_date(LocalDateTime.now());
        casefile.setStatus(CasefileStatusType.IN_PROCESS);
        establishment.addCasefile(casefile);
    }

    public void initializeDocument() {
        document = new RequirementControlDocument();
        document.setStatus(true);
        document.setAcceptance_document_created_on(LocalDateTime.now());
        document.setControl_document_created_on(LocalDateTime.now());
        casefile.addControlDocument(document);
    }

    public void initializeRequirementRows() {
        RequirementRow row = new RequirementRow(document, sessionFactory.getCurrentSession().createQuery("select template " +
                "from RequirementControlTemplate template " +
                "where template.id.requirement_id = 1 " +
                "and template.id.template_id = 1", RequirementControlTemplate.class).getSingleResult());
        row.setStatus(RequirementStatusType.APPROVED);
        row.setObservation("obs");
        document.addRequirementRow(row);
    }

    private void initializeClientList() {
        Client c1 = new Client();
        c1.setName("Carl");
        c1.setDUI("45113645-9");
        c1.setNationally("Salvadoreņa");
        sessionFactory.getCurrentSession().persist(c1);
        Client c2 = new Client();
        c2.setName("Bob");
        c2.setNationally("Salvadoreņa");
        c2.setDUI("45220397-8");
        sessionFactory.getCurrentSession().persist(c2);
        sessionFactory.getCurrentSession().update(establishment);
    }


}
