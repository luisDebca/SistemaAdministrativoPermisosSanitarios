package edu.ues.aps.process.requirement.establishment.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;

public class RequirementDocumentDTORepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        return new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("RequirementDocumentDTO.xml"));
    }

    @Autowired
    private RequirementDocumentInfoDTOFinderRepository finderRepository;

    @Test
    public void testFindRequirementDocumentDTOByCasefileId(){
        Assert.assertNotNull(finderRepository.findDocumentInfoById(1L));
        Assert.assertNotNull(finderRepository.findDocumentInfoById(2L));
        Assert.assertNotNull(finderRepository.findDocumentInfoById(3L));
        Assert.assertNotNull(finderRepository.findDocumentInfoById(4L));
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testFindAnonExistentRequirementDocumentDTO(){
        Assert.assertNull(finderRepository.findDocumentInfoById(99L));
    }
}
