package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import edu.ues.aps.process.requirement.base.model.DocumentTemplateType;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class DocumentTemplateRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        return new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("DocumentTemplate.xml"));
    }

    @Autowired
    private DocumentTemplateDTOFinderRepository finderRepository;

    @Test
    public void testFindAllDocumentTemplates(){
        Assert.assertEquals(finderRepository.findAll().size(),4);
    }

    @Test
    public void testFindAllTemplatesByType(){
        Assert.assertEquals(finderRepository.findAllByType(DocumentTemplateType.ESTABLISHMENT).size(),2);
        Assert.assertEquals(finderRepository.findAllByType(DocumentTemplateType.VEHICLE).size(),2);
        Assert.assertEquals(finderRepository.findAllByType(DocumentTemplateType.TOBACCO).size(),0);
    }
}
