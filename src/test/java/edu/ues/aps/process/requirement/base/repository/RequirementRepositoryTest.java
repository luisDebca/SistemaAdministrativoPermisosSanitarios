package edu.ues.aps.process.requirement.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class RequirementRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("DocumentTemplate.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Requirement.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("RequirementControlTemplate.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private RequirementDTOFinderRepository finderRepository;

    @Test
    public void testFindAllRequirementsFromTemplate(){
        Assert.assertEquals(finderRepository.findAllRequirementsFromTemplate(1l).size(),2);
        Assert.assertEquals(finderRepository.findAllRequirementsFromTemplate(2l).size(),2);
        Assert.assertEquals(finderRepository.findAllRequirementsFromTemplate(3l).size(),4);
        Assert.assertEquals(finderRepository.findAllRequirementsFromTemplate(4l).size(),0);
    }
    
}
