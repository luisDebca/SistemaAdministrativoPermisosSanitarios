package edu.ues.aps.process.payment.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import edu.ues.aps.process.area.establishment.model.CasefileEstablishment;
import edu.ues.aps.process.base.model.Casefile;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

import static org.testng.Assert.*;

public class PaymentProcedureRepositoryTest extends EntityRepositoryImplementTest {

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Department.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Municipality.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Section.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("EstablishmentType.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Vehicle.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Process.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("PaymentRates.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Owner.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Establishment.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("PaymentProcedureProcess.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private PaymentProcedureQuantityRepository quantityRepository;

    @Autowired
    private PaymentProcedureSummaryRepository summaryRepository;

    @Test
    public void testGetPaymentProcedureCountSince() {
        assertEquals(quantityRepository.getPaymentProcedureCountSince(CasefileEstablishment.class,LocalDateTime.of(2018,1,1,0,0,0)).intValue(),5);
    }

    @Test
    public void testGetRaisedMoneyFromPaymentProcessForEstablishmentSince() {
        assertEquals(quantityRepository.getRaisedMoneyFromPaymentProcessSince(CasefileEstablishment.class,LocalDateTime.of(2018,1,1,0,0,0)),305.0);
    }

    @Test
    public void testGetRaisedMoneyFromPaymentProcessForVehiclesSince() {
        //TODO NOT IMPLEMENTED
    }

    @Test
    public void testFindPaymentProcedureSummaryListForEstablishmentsSince() {
        //TODO NOT IMPLEMENTED
    }

    @Test
    public void testFindPaymentProcedureSummaryListForVehiclesSince() {
        //TODO NOT IMPLEMENTED
    }
}