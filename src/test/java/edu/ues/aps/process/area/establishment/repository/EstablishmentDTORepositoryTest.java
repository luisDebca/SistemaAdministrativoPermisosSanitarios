package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;

public class EstablishmentDTORepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        return new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("EstablishmentDetailsDTO.xml"));
    }

    @Autowired
    private EstablishmentDTOFinderRepository finderRepository;

    @Test
    public void testFindAllEstablishmentDetailsDTO(){
        Assert.assertEquals(finderRepository.findAll().size(),3);
    }

}
