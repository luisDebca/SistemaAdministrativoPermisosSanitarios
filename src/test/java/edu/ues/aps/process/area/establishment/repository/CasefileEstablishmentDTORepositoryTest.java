package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import edu.ues.aps.process.area.establishment.model.CasefileEstablishment;
import edu.ues.aps.process.area.establishment.model.Establishment;
import edu.ues.aps.process.base.model.*;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;
import java.time.LocalDateTime;

public class CasefileEstablishmentDTORepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        return new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("CasefileEstablishmentDTO.xml"));
    }

    @Autowired
    private EstablishmentCaseFileCaseFileDTORepository finderRepository;

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    public void testFindCasefileEstablishmentById(){
        Assert.assertNotNull(finderRepository.find(1L));
        Assert.assertNotNull(finderRepository.find(2L));
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testFindAnonExistentCasefileEstablishmentThrowsNoResultException(){
        Assert.assertNull(finderRepository.find(99L));
    }
}
