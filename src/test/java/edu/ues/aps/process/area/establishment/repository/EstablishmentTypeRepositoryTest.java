package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class EstablishmentTypeRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Section.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("EstablishmentType.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private EstablishmentTypeDTOFinderRepository crudRepository;

    @Test
    public void testFindAllEstablishmentTypes(){
        Assert.assertEquals(crudRepository.findAll().size(),10);
    }

    @Test
    public void testFindAllEstablishmentTypeFromSection(){
        Assert.assertEquals(crudRepository.findAllTypesFromSection(1L).size(),3);
        Assert.assertEquals(crudRepository.findAllTypesFromSection(2L).size(),1);
        Assert.assertEquals(crudRepository.findAllTypesFromSection(3L).size(),6);
    }
}
