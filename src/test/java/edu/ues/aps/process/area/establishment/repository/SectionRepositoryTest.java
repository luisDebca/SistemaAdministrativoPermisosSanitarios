package edu.ues.aps.process.area.establishment.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SectionRepositoryTest extends EntityRepositoryImplementTest {

    @Override
    protected IDataSet getDataset() throws Exception {
        return new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Section.xml"));
    }

    @Autowired
    private SectionDTOFinderRepository sectionDTOFinderRepository;

    @Test
    public void testFindAllSections(){
        Assert.assertEquals(sectionDTOFinderRepository.findAll().size(),3);
    }
}
