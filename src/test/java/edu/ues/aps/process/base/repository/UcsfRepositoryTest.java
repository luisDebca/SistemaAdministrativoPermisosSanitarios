package edu.ues.aps.process.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class UcsfRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
            new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("SIBASI.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("UCSF.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private UcsfDTOFinderRepository ucsfDTOFinderRepository;

    @Test
    public void testFindAllUcsf() {
        Assert.assertEquals(ucsfDTOFinderRepository.findAll().size(),4);
    }

    @Test
    public void testFindAllFromSibasi(){
        Assert.assertEquals(ucsfDTOFinderRepository.findAllUcsfFromSibasi(1L).size(),1);
        Assert.assertEquals(ucsfDTOFinderRepository.findAllUcsfFromSibasi(9L).size(),0);
    }

}
