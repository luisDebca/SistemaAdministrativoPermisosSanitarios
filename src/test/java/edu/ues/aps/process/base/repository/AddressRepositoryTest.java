package edu.ues.aps.process.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class AddressRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Department.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Municipality.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private AddressFinderRepository finderRepository;

    @Test
    public void testFindAllDepartments(){
        Assert.assertEquals(finderRepository.findAllDepartments().size(),14);
    }

    @Test
    public void testFindAllMunicipalities(){
        Assert.assertEquals(finderRepository.findAllMunicipalities().size(),14);
    }

    @Test
    public void testFindAllMunicipalitiesFromDepartment(){
        Assert.assertEquals(finderRepository.findAllMunicipalitiesFromDepartment(1l).size(),1);
        Assert.assertEquals(finderRepository.findAllMunicipalitiesFromDepartment(99l).size(),0);
    }
}
