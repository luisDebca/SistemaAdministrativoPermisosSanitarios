package edu.ues.aps.process.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;

public class OwnerDTORepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Department.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Municipality.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Address.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Owner.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private OwnerDTOFinderRepository finderRepository;

    @Test
    public void testFindOwnerEntityDTOById(){
        Assert.assertNotNull(finderRepository.findById(1L));
        Assert.assertNotNull(finderRepository.findById(5L));
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testFindNonExistOwnerEntityThrowsNoResultException(){
        Assert.assertNull(finderRepository.findById(99L));
    }

}
