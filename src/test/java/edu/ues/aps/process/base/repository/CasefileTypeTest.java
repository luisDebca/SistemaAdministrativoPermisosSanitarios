package edu.ues.aps.process.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import edu.ues.aps.process.area.establishment.model.CasefileEstablishment;
import edu.ues.aps.process.area.vehicle.model.CasefileVehicle;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;

public class CasefileTypeTest extends EntityRepositoryImplementTest {

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Establishment.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Vehicle.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("CasefileType.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private SessionFactory sessionFactory;

    @Test
    public void testGetCasefileTypeQuery(){
        Assert.assertEquals(getClass(1L).getSimpleName(),CasefileEstablishment.class.getSimpleName());
        Assert.assertEquals(getClass(2L).getSimpleName(),CasefileEstablishment.class.getSimpleName());
        Assert.assertEquals(getClass(3L).getSimpleName(),CasefileVehicle.class.getSimpleName());
        Assert.assertEquals(getClass(4L).getSimpleName(),CasefileVehicle.class.getSimpleName());
        Assert.assertEquals(getClass(5L).getSimpleName(),CasefileVehicle.class.getSimpleName());
        Assert.assertEquals(getClass(6L).getSimpleName(),CasefileVehicle.class.getSimpleName());
    }

    private Class getClass(Long id_casefile) throws NoResultException {
        return sessionFactory.getCurrentSession().createQuery("select type(casefile) " +
                "from Casefile casefile where casefile.id = :id",Class.class)
                .setParameter("id",id_casefile)
                .getSingleResult();
    }

}
