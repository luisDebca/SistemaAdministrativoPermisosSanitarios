package edu.ues.aps.process.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import edu.ues.aps.process.base.model.AbstractOwner;
import edu.ues.aps.process.base.model.LegalEntity;
import edu.ues.aps.process.base.model.NaturalEntity;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertNotNull;
import static org.testng.Assert.assertNull;

public class OwnerRepositoryTest extends EntityRepositoryImplementTest {

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Department.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Municipality.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Address.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Owner.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("CasefileOwner.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private OwnerCrudRepository crudRepository;

    @Qualifier("legalOwnerFinderRepository")
    @Autowired
    private OwnerFinderRepository legalFinderRepository;

    @Qualifier("naturalOwnerFinderRepository")
    @Autowired
    private OwnerFinderRepository naturalFinderRepository;

    @Test
    public void testFindById() {
        assertNotNull(naturalFinderRepository.findOwnerById(1L));
        assertNotNull(legalFinderRepository.findOwnerById(5L));
    }

    @Test
    public void testNonExistOwnerByIdReturnsAEmptyOwner() {
        assertEquals(naturalFinderRepository.findOwnerById(99L).getId().intValue(),0);
        assertEquals(legalFinderRepository.findOwnerById(99L).getId().intValue(),0);
    }

    @Test
    public void testFindLegalEntityById() {
        assertNotNull(crudRepository.findLegalEntityById(5L));
        assertNotNull(crudRepository.findLegalEntityById(6L));
        assertNotNull(crudRepository.findLegalEntityById(7L));
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testFindNonExistLegalEntityThrowNoResultException(){
        assertNull(crudRepository.findLegalEntityById(1L));
    }

}