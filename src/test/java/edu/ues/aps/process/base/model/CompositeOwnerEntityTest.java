package edu.ues.aps.process.base.model;

import org.mockito.InjectMocks;
import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class CompositeOwnerEntityTest {

    @InjectMocks
    private CompositeOwnerEntity compositeOwnerEntity;

    @Spy
    private List<AbstractOwner> ownerList;

    @BeforeMethod
    public void setUp(){
        ownerList = new ArrayList<>();
        initializeOwnerList();
    }

    @Test
    public void testCompositeOwnerListWithTreeObjectsExpectedOnlyOneObject(){
        compositeOwnerEntity = new CompositeOwnerEntity(ownerList);
        Assert.assertEquals(compositeOwnerEntity.getCompositeNumber(),3);
        Assert.assertEquals(compositeOwnerEntity.getComposite_owner().getName(),"Mary Kerr||Hilary Yates||Imelda Hyde");
    }

    @Test
    public void testCompositeOwnerListWithTreeEqualsObjectsExpectedSameObject(){
        ownerList.remove(0);
        ownerList.remove(0);
        compositeOwnerEntity = new CompositeOwnerEntity(ownerList);
        Assert.assertEquals(compositeOwnerEntity.getCompositeNumber(),1);
        Assert.assertEquals(compositeOwnerEntity.getComposite_owner().getName(),"Imelda Hyde");
    }

    @Test
    public void testCompositeOwnerListWithOneObjectExpectedSameObject(){
        ownerList.remove(0);
        ownerList.remove(0);
        ownerList.remove(0);
        ownerList.remove(0);
        compositeOwnerEntity = new CompositeOwnerEntity(ownerList);
        Assert.assertEquals(compositeOwnerEntity.getCompositeNumber(),1);
        Assert.assertEquals(compositeOwnerEntity.getComposite_owner().getName(),"Imelda Hyde");
    }

    @Test
    public void testCompositeOwnerListEmptyExpectedEmptyOwner(){
        compositeOwnerEntity = new CompositeOwnerEntity(Collections.emptyList());
        Assert.assertEquals(compositeOwnerEntity.getCompositeNumber(),0);
        Assert.assertEquals(compositeOwnerEntity.getComposite_owner().getId().intValue(),0);
    }

    private void initializeOwnerList(){
        AbstractOwner o1 = new OwnerEntity();
        o1.setId(1L);
        o1.setName("Mary Kerr");
        o1.setNit("9011-080414-765-6");
        o1.setEmail("Vivamus.euismod@justoProinnon.ca");
        o1.setTelephone("(986) 620-4839");
        o1.setFAX("(184) 326-1695");
        AbstractOwner o2 = new OwnerEntity();
        o2.setId(2L);
        o2.setName("Hilary Yates");
        o2.setNit("9474-063992-691-3");
        o2.setEmail("sed.sem@aliquetlobortisnisi.org");
        o2.setTelephone("(649) 586-7161");
        o2.setFAX("(988) 559-6207");
        AbstractOwner o3 = new OwnerEntity();
        o3.setId(3L);
        o3.setName("Imelda Hyde");
        o3.setNit("5262-479616-563-4");
        o3.setEmail("ornare.facilisis.eget@arcu.org");
        o3.setTelephone("(861) 676-7106");
        o3.setFAX("(785) 326-2008");

        ownerList.add(o1);
        ownerList.add(o2);
        ownerList.add(o3);
        ownerList.add(o3);
        ownerList.add(o3);
    }
}
