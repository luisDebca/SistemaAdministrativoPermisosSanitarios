package edu.ues.aps.process.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class SibasiRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
      return new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("SIBASI.xml"));
    }

    @Autowired
    private SibasiDTOFinderRepository sibasiDTOFinderRepository;

    @Test
    public void testFindAllSibasis(){
        Assert.assertEquals(sibasiDTOFinderRepository.findAll().size(),4);
    }
}
