package edu.ues.aps.process.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;

public class ProductRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Address.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Owner.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Product.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private ProductCrudRepository productCrudRepository;

    @Autowired
    private ProductDTOFinderRepository productDTOFinderRepository;

    @Test
    public void testFindAllProductsFromLegalEntity(){
        Assert.assertEquals(productCrudRepository.findAllProductsFromOwner(5L).size(),2);
        Assert.assertEquals(productCrudRepository.findAllProductsFromOwner(6L).size(),5);
        Assert.assertEquals(productCrudRepository.findAllProductsFromOwner(7L).size(),0);
    }

    @Test
    public void testFindAllProductsFromLegalEntityAsDTO(){
        Assert.assertEquals(productDTOFinderRepository.findAllProductsFromLegalEntity(5L).size(),2);
        Assert.assertEquals(productDTOFinderRepository.findAllProductsFromLegalEntity(6L).size(),5);
        Assert.assertEquals(productDTOFinderRepository.findAllProductsFromLegalEntity(7L).size(),0);
    }

    @Test
    public void testDeleteProducts(){
        Assert.assertEquals(productDTOFinderRepository.findAllProductsFromLegalEntity(6L).size(),5);
        productCrudRepository.delete(3L);
        Assert.assertEquals(productDTOFinderRepository.findAllProductsFromLegalEntity(6L).size(),4);
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testDeleteNonExistentProduct(){
        productCrudRepository.delete(99L);
    }
}
