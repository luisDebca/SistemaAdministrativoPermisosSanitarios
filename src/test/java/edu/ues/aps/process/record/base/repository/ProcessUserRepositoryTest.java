package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class ProcessUserRepositoryTest extends EntityRepositoryImplementTest {

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Process.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("CasefileProcess.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private ProcessUserFinderRepository finderRepository;

    @Test
    public void testFindAllFromCasefileProcess() {
        assertEquals(finderRepository.findAll(new CasefileProcess.CasefileProcessId(1L,10L)).size(),5);
        assertEquals(finderRepository.findAll(new CasefileProcess.CasefileProcessId(2L,10L)).size(),3);
        assertEquals(finderRepository.findAll(new CasefileProcess.CasefileProcessId(2L,20L)).size(),2);
        assertEquals(finderRepository.findAll(new CasefileProcess.CasefileProcessId(3L,10L)).size(),3);
        assertEquals(finderRepository.findAll(new CasefileProcess.CasefileProcessId(3L,20L)).size(),3);
        assertEquals(finderRepository.findAll(new CasefileProcess.CasefileProcessId(3L,30L)).size(),4);
        assertEquals(finderRepository.findAll(new CasefileProcess.CasefileProcessId(3L,40L)).size(),1);
    }

    @Test
    public void testFindAllFromUser() {
        assertEquals(finderRepository.findAll("Rose").size(),3);
        assertEquals(finderRepository.findAll("Ronan").size(),2);
        assertEquals(finderRepository.findAll("Velma").size(),3);
        assertEquals(finderRepository.findAll("Priscilla").size(),2);
        assertEquals(finderRepository.findAll("Lael").size(),1);
        assertEquals(finderRepository.findAll("Davis").size(),2);
        assertEquals(finderRepository.findAll("Nathaniel").size(),3);
        assertEquals(finderRepository.findAll("Carolyn").size(),1);
        assertEquals(finderRepository.findAll("Jayme").size(),2);
        assertEquals(finderRepository.findAll("Ali").size(),2);
    }
}