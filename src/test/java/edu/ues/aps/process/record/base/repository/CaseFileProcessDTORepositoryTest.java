package edu.ues.aps.process.record.base.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import edu.ues.aps.process.record.base.model.CasefileProcess;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class CaseFileProcessDTORepositoryTest extends EntityRepositoryImplementTest {

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Process.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("CasefileProcess.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private CaseFileProcessDTOFinderRepository finderRepository;

    @Test
    public void testFindAllProcessFromCasefile() {
        assertEquals(finderRepository.findAllProcessFromCaseFile(1L).size(),2);
        assertEquals(finderRepository.findAllProcessFromCaseFile(2L).size(),3);
        assertEquals(finderRepository.findAllProcessFromCaseFile(3L).size(),5);
    }

    @Test
    public void testFindAllProcessUserFromCasefileProcess(){
        CasefileProcess.CasefileProcessId casefile1Process1 = new CasefileProcess.CasefileProcessId(1L,10L);
        CasefileProcess.CasefileProcessId casefile2Process1 = new CasefileProcess.CasefileProcessId(2L,10L);
        CasefileProcess.CasefileProcessId casefile2Process2 = new CasefileProcess.CasefileProcessId(2L,20L);
        CasefileProcess.CasefileProcessId casefile3Process1 = new CasefileProcess.CasefileProcessId(3L,10L);
        CasefileProcess.CasefileProcessId casefile3Process2 = new CasefileProcess.CasefileProcessId(3L,20L);
        CasefileProcess.CasefileProcessId casefile3Process3 = new CasefileProcess.CasefileProcessId(3L,30L);
        CasefileProcess.CasefileProcessId casefile3Process4 = new CasefileProcess.CasefileProcessId(3L,40L);
        assertEquals(finderRepository.findAllProcessUserFromCaseFileProcess(casefile1Process1).size(),5);
        assertEquals(finderRepository.findAllProcessUserFromCaseFileProcess(casefile2Process1).size(),3);
        assertEquals(finderRepository.findAllProcessUserFromCaseFileProcess(casefile2Process2).size(),2);
        assertEquals(finderRepository.findAllProcessUserFromCaseFileProcess(casefile3Process1).size(),3);
        assertEquals(finderRepository.findAllProcessUserFromCaseFileProcess(casefile3Process2).size(),3);
        assertEquals(finderRepository.findAllProcessUserFromCaseFileProcess(casefile3Process3).size(),4);
        assertEquals(finderRepository.findAllProcessUserFromCaseFileProcess(casefile3Process4).size(),1);
    }
}