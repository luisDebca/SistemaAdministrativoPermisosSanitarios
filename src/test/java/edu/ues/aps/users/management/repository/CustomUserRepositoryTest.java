package edu.ues.aps.users.management.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import java.time.LocalDateTime;

public class CustomUserRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Personal.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Profile.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("User.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private UserCrudRepository userCrudRepository;

    @Autowired
    private CustomUserRepository customUserRepository;

    //TODO NO LOGGING UPDATE DATE ERROR 001
    @Test
    public void testUpdateLastLoginDate(){
         Assert.assertEquals(userCrudRepository.findByUsername("George").getLast_login(), LocalDateTime.of(2017,06,15,19,44,51));
         customUserRepository.updateLastLoggingToCurrentDate(1l);
     }
}
