package edu.ues.aps.users.management.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

public class ProfileRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Personal.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Profile.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("User.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private ProfileCrudRepository profileCrudRepository;

    @Autowired UserProfileRelationsRepository userProfileRelationsRepository;

    @Test
    public void testFindAllProfiles(){
        Assert.assertEquals(profileCrudRepository.findAll().size(),9);
    }

    @Test
    public void testFindAllUserProfiles(){
        Assert.assertEquals(userProfileRelationsRepository.findAllProfiles(1l).size(),1);
        Assert.assertEquals(userProfileRelationsRepository.findAllProfiles(5l).size(),2);
    }
}
