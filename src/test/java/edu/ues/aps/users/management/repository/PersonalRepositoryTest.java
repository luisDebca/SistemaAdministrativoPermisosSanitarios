package edu.ues.aps.users.management.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.testng.Assert;
import org.testng.annotations.Test;

public class PersonalRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Personal.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Profile.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("User.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private PersonalWithoutUserRepository personalWithoutUserRepository;

    @Autowired
    private UserCrudRepository userCrudRepository;

    @Test
    public void testFindAllPersonalWithUserDisabled(){
        Assert.assertEquals(personalWithoutUserRepository.findAll().size(),0);
        userCrudRepository.deleteUser("Todd");
        Assert.assertEquals(personalWithoutUserRepository.findAll().size(),1);
    }

}
