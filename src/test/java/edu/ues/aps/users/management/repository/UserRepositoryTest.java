package edu.ues.aps.users.management.repository;

import edu.ues.aps.configuration.test.EntityRepositoryImplementTest;
import org.dbunit.dataset.CompositeDataSet;
import org.dbunit.dataset.IDataSet;
import org.dbunit.dataset.xml.FlatXmlDataSetBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.testng.Assert;
import org.testng.annotations.Test;

import javax.persistence.NoResultException;

public class UserRepositoryTest extends EntityRepositoryImplementTest{

    @Override
    protected IDataSet getDataset() throws Exception {
        IDataSet[] datasets = new IDataSet[] {
            new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Personal.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("Profile.xml")),
                new FlatXmlDataSetBuilder().build(this.getClass().getClassLoader().getResourceAsStream("User.xml"))
        };
        return new CompositeDataSet(datasets);
    }

    @Autowired
    private UserCrudRepository userCrudRepository;

    @Autowired
    private UserProfileRelationsRepository userProfileRelationsRepository;

    @Test
    public void testFindByUsername(){
        Assert.assertNotNull(userCrudRepository.findByUsername("Todd"));
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testFindNonExistentUserByUsername(){
        Assert.assertNull(userCrudRepository.findByUsername("will"));
    }

    @Test
    public void testFindUserProfiles(){
        Assert.assertEquals(userProfileRelationsRepository.findAllProfiles(1l).size(),1);
        Assert.assertEquals(userProfileRelationsRepository.findAllProfiles(5l).size(),2);
    }

    @Test(expectedExceptions = NoResultException.class)
    public void testDeleteUser(){
        Assert.assertNotNull(userCrudRepository.findByUsername("Todd"));
        userCrudRepository.deleteUser("Todd");
        Assert.assertNull(userCrudRepository.findByUsername("Todd"));
    }
}
