package edu.ues.aps.utilities.file.directory;

import org.mockito.Mock;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class ApplicationDirectoryHandlerTest {

    @Mock
    private ApplicationDirectoryHandler directoryInformationProvider;

    @BeforeMethod
    public void setUp(){
        directoryInformationProvider = new ApplicationDirectoryHandler();
    }

    @Test
    public void testIsSourceDirectoryExist() {
        assertEquals(directoryInformationProvider.isSourceDirectoryExist(),true);
    }

    @Test
    public void testGetSourceDirectoryAbsolutePath() {
        assertEquals(directoryInformationProvider.getSourceDirectoryAbsolutePath(), System.getProperty("user.home") + "/aps_server/sources");
    }

    @Test
    public void testGetSystemFileDelimiter() {
        assertEquals(directoryInformationProvider.getSystemFileDelimiter(),"/");
    }
}