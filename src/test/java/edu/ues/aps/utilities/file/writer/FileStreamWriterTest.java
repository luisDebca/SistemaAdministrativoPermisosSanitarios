package edu.ues.aps.utilities.file.writer;

import edu.ues.aps.utilities.file.directory.ApplicationDirectoryHandler;
import org.mockito.InjectMocks;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class FileStreamWriterTest {

    private ApplicationDirectoryHandler directoryHandler;

    @InjectMocks
    private FileStreamWriter writer;

    @BeforeMethod
    public void setUp() {
        directoryHandler = new ApplicationDirectoryHandler();
        writer = new FileStreamWriter(directoryHandler);
        writer.writeFlatName("FileStreamReaderTestFile.aps", "Test file content");
    }

    @AfterMethod
    public void setDown() {
        writer.delete("FileStreamReaderTestFile.aps");
    }

    @Test
    public void testIsFileExist() {
        assertTrue(writer.isFileExist("FileStreamReaderTestFile.aps"));
        assertFalse(writer.isFileExist("NoExistingFile.aps"));
    }

    @Test
    public void testWriteFileWithOutSpecificName() {
        //TODO UNFINISHED TEST METHOD
    }

    @Test
    public void testWriteFileWithName() {
        //TODO UNFINISHED TEST METHOD
    }
}