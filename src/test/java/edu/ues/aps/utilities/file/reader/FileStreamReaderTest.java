package edu.ues.aps.utilities.file.reader;

import edu.ues.aps.utilities.file.directory.ApplicationDirectoryHandler;
import edu.ues.aps.utilities.file.writer.FileStreamWriter;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class FileStreamReaderTest {

    private ApplicationDirectoryHandler directoryHandler;
    private FileStreamReader reader;
    private FileStreamWriter writer;

    @BeforeMethod
    public void setUp(){
        directoryHandler = new ApplicationDirectoryHandler();
        reader = new FileStreamReader(directoryHandler);
        writer = new FileStreamWriter(directoryHandler);
        writer.writeFlatName("FileStreamReaderTestFile.aps","Test file content");
    }

    @AfterMethod
    public void setDown(){
        writer.delete("FileStreamReaderTestFile.aps");
    }

    @Test
    public void testIsFileExist() {
        assertTrue(reader.isFileExist("FileStreamReaderTestFile.aps"));
        assertFalse(reader.isFileExist("NoExistingFile.aps"));
    }

    @Test
    public void testReadFileContent() {
        assertEquals(reader.readFileContent("FileStreamReaderTestFile.aps"),"Test file content");
    }
}