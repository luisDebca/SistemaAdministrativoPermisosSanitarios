package edu.ues.aps.utilities.http.response;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SingleStringResponseTest {

    @Test
    public void testNewSingleStringResponseAsUndefinedResponse(){
        SingleStringResponse response = new SingleStringResponse();
        Assert.assertEquals(response.getResponse_message(),"");
        Assert.assertEquals(response.getResponse_status(),ResponseStatusType.UNDEFINED.name());
    }

    @Test
    public void testNewSingleStringResponseAsSuccessResponse(){
        SingleStringResponse response = new SingleStringResponse().asSuccessResponse();
        Assert.assertEquals(response.getResponse_message(),"");
        Assert.assertEquals(response.getResponse_status(),ResponseStatusType.SUCCESS.name());
    }

    @Test
    public void testNewSingleStringResponseAsWarningResponse(){
        SingleStringResponse response = new SingleStringResponse().asWarningResponse();
        Assert.assertEquals(response.getResponse_message(),"");
        Assert.assertEquals(response.getResponse_status(),ResponseStatusType.WARNING.name());
    }

    @Test
    public void testNewSingleStringResponseAsErrorResponse(){
        SingleStringResponse response = new SingleStringResponse().asErrorResponse();
        Assert.assertEquals(response.getResponse_message(),"");
        Assert.assertEquals(response.getResponse_status(),ResponseStatusType.ERROR.name());
    }
}
