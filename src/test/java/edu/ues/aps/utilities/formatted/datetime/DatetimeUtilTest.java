package edu.ues.aps.utilities.formatted.datetime;

import org.mockito.Spy;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

public class DatetimeUtilTest {

    @Spy
    private List<LocalDateTime> boundary_conditions = new ArrayList<>();

    @BeforeClass
    public void setUp(){
        boundary_conditions.add(LocalDateTime.of(2017,10,31,16,00));
        boundary_conditions.add(LocalDateTime.of(2018,01,15,12,30));
        boundary_conditions.add(LocalDateTime.of(2019,02,20,23,00));
        boundary_conditions.add(LocalDateTime.of(2020,05,10,06,59));
    }

    @Test
    public void testParseDatetimeToVerboseDatetimeFormat(){
        Assert.assertEquals(DatetimeUtil.parseDatetimeToVerboseDate(boundary_conditions.get(0)),"Diecis�is horas con Cero minutos del d�a Treinta y uno de Octubre Dos mil diecisiete");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToVerboseDate(boundary_conditions.get(1)),"Doce horas con Treinta minutos del d�a Quince de Enero Dos mil dieciocho");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToVerboseDate(boundary_conditions.get(2)),"Veintitr�s horas con Cero minutos del d�a Veinte de Febrero Dos mil diecinueve");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToVerboseDate(boundary_conditions.get(3)),"Seis horas con Cincuenta y nueve minutos del d�a Diez de Mayo Dos mil veinte");
    }

    @Test
    public void testParseDatetimeToVerboseDatetimeFormatWithNullValue(){
        Assert.assertEquals(DatetimeUtil.parseDatetimeToVerboseDate(null),DatetimeUtil.DATETIME_NOT_DEFINED);
    }

    @Test
    public void testParseDatetimeToShortDateFormat(){
        Assert.assertEquals(DatetimeUtil.parseDatetimeToShortFormat(boundary_conditions.get(0)),"31/10/2017 16:0:0");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToShortFormat(boundary_conditions.get(1)),"15/1/2018 12:30:0");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToShortFormat(boundary_conditions.get(2)),"20/2/2019 23:0:0");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToShortFormat(boundary_conditions.get(3)),"10/5/2020 6:59:0");
    }

    @Test
    public void testParseDatetimeToShortDateWithNullValue(){
        Assert.assertEquals(DatetimeUtil.parseDatetimeToShortFormat(null),DatetimeUtil.DATETIME_NOT_DEFINED);
    }

    @Test
    public void testParseDateToShortDateFormat(){
        Assert.assertEquals(DatetimeUtil.parseDateToShortFormat(boundary_conditions.get(0).toLocalDate()),"31/10/2017");
        Assert.assertEquals(DatetimeUtil.parseDateToShortFormat(boundary_conditions.get(1).toLocalDate()),"15/1/2018");
        Assert.assertEquals(DatetimeUtil.parseDateToShortFormat(boundary_conditions.get(2).toLocalDate()),"20/2/2019");
        Assert.assertEquals(DatetimeUtil.parseDateToShortFormat(boundary_conditions.get(3).toLocalDate()),"10/5/2020");
    }

    @Test
    public void testParseDateToShortDateWithNull(){
        Assert.assertEquals(DatetimeUtil.parseDateToShortFormat(null),DatetimeUtil.DATE_NOT_DEFINED);
    }

    @Test
    public void testParseDatetimeToLongDateFormat(){
        Assert.assertEquals(DatetimeUtil.parseDatetimeToLongDateFormat(boundary_conditions.get(0)),"31 de Octubre 2017 16:0:0");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToLongDateFormat(boundary_conditions.get(1)),"15 de Enero 2018 12:30:0");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToLongDateFormat(boundary_conditions.get(2)),"20 de Febrero 2019 23:0:0");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToLongDateFormat(boundary_conditions.get(3)),"10 de Mayo 2020 6:59:0");
    }

    @Test
    public void testParseDatetimeToLongDateWithNull(){
        Assert.assertEquals(DatetimeUtil.parseDatetimeToLongDateFormat(null),DatetimeUtil.DATETIME_NOT_DEFINED);
    }

    @Test
    public void testParseDateToLongDateFormat(){
        Assert.assertEquals(DatetimeUtil.parseDateToLongDateFormat(boundary_conditions.get(0).toLocalDate()),"31 de Octubre 2017");
        Assert.assertEquals(DatetimeUtil.parseDateToLongDateFormat(boundary_conditions.get(1).toLocalDate()),"15 de Enero 2018");
        Assert.assertEquals(DatetimeUtil.parseDateToLongDateFormat(boundary_conditions.get(2).toLocalDate()),"20 de Febrero 2019");
        Assert.assertEquals(DatetimeUtil.parseDateToLongDateFormat(boundary_conditions.get(3).toLocalDate()),"10 de Mayo 2020");
    }

    @Test
    public void testParseDateToLongDateFormatWithNull(){
        Assert.assertEquals(DatetimeUtil.parseDateToLongDateFormat(null),DatetimeUtil.DATE_NOT_DEFINED);
    }

    @Test
    public void testParseDateToISODateFormat(){
        Assert.assertEquals(DatetimeUtil.parseDateToISODateFormat(boundary_conditions.get(0).toLocalDate()),"31-10-2017");
        Assert.assertEquals(DatetimeUtil.parseDateToISODateFormat(boundary_conditions.get(1).toLocalDate()),"15-1-2018");
        Assert.assertEquals(DatetimeUtil.parseDateToISODateFormat(boundary_conditions.get(2).toLocalDate()),"20-2-2019");
        Assert.assertEquals(DatetimeUtil.parseDateToISODateFormat(boundary_conditions.get(3).toLocalDate()),"10-5-2020");
    }

    @Test
    public void testParseDateToISODateFormatWithNull(){
        Assert.assertEquals(DatetimeUtil.parseDateToISODateFormat(null),DatetimeUtil.DATE_NOT_DEFINED);
    }

    @Test
    public void testParseDatetimeToHtmlFormat(){
        Assert.assertEquals(DatetimeUtil.parseDatetimeToHtmlFormat(boundary_conditions.get(0)),"2017/10/31 16:0:0");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToHtmlFormat(boundary_conditions.get(1)),"2018/1/15 12:30:0");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToHtmlFormat(boundary_conditions.get(2)),"2019/2/20 23:0:0");
        Assert.assertEquals(DatetimeUtil.parseDatetimeToHtmlFormat(boundary_conditions.get(3)),"2020/5/10 6:59:0");
    }

}
