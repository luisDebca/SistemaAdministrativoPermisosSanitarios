package edu.ues.aps.configuration.test;

import edu.ues.aps.configuration.database.HibernateTestConfiguration;
import org.dbunit.database.DatabaseDataSourceConnection;
import org.dbunit.database.IDatabaseConnection;
import org.dbunit.dataset.IDataSet;
import org.dbunit.operation.DatabaseOperation;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTransactionalTestNGSpringContextTests;
import org.testng.annotations.BeforeMethod;

import javax.sql.DataSource;

@ContextConfiguration(classes = {HibernateTestConfiguration.class})
public abstract class EntityRepositoryImplementTest extends AbstractTransactionalTestNGSpringContextTests{

    @Autowired
    DataSource dataSource;

    @BeforeMethod
    public void setUp() throws Exception{
        IDatabaseConnection databaseConnection = new DatabaseDataSourceConnection(dataSource);
        DatabaseOperation.CLEAN_INSERT.execute(databaseConnection, getDataset());
    }

    protected abstract IDataSet getDataset() throws Exception;
}
