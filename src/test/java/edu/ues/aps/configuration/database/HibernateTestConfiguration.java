package edu.ues.aps.configuration.database;

import org.hibernate.cfg.AvailableSettings;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.ComponentScans;
import org.springframework.context.annotation.Configuration;
import org.springframework.jdbc.datasource.DriverManagerDataSource;
import org.springframework.orm.hibernate5.HibernateTransactionManager;
import org.springframework.orm.hibernate5.LocalSessionFactoryBean;
import org.springframework.transaction.annotation.EnableTransactionManagement;

import javax.sql.DataSource;
import java.util.Properties;

@Configuration
@EnableTransactionManagement
@ComponentScans(value = {
        @ComponentScan("edu.ues.aps.process.base.repository"),
        @ComponentScan("edu.ues.aps.users.management.repository"),
        @ComponentScan("edu.ues.aps.process.requirement.base.repository"),
        @ComponentScan("edu.ues.aps.process.requirement.establishment.repository"),
        @ComponentScan("edu.ues.aps.process.area.base.repository"),
        @ComponentScan("edu.ues.aps.process.area.establishment.repository"),
        @ComponentScan("edu.ues.aps.process.area.vehicle.repository"),
        @ComponentScan("edu.ues.aps.process.record.base.repository"),
        @ComponentScan("edu.ues.aps.process.legal.base.repository"),
        @ComponentScan("edu.ues.aps.process.legal.establishment.repository"),
        @ComponentScan("edu.ues.aps.process.payment.base.repository"),
        @ComponentScan("edu.ues.aps.etl.extract")
})
public class HibernateTestConfiguration {

    @Bean
    public HibernateTransactionManager transactionManager() {
        HibernateTransactionManager transactionManager = new HibernateTransactionManager();
        transactionManager.setSessionFactory(sessionFactory().getObject());
        return transactionManager;
    }

    @Bean
    public LocalSessionFactoryBean sessionFactory(){
        LocalSessionFactoryBean sessionFactoryBean = new LocalSessionFactoryBean();
        sessionFactoryBean.setDataSource(dataSource());
        sessionFactoryBean.setPackagesToScan("edu.ues.aps.users.management.model",
                "edu.ues.aps.process.base.model",
                "edu.ues.aps.process.area.establishment.model",
                "edu.ues.aps.process.area.vehicle.model",
                "edu.ues.aps.process.legal.base.model",
                "edu.ues.aps.process.payment.base.model",
                "edu.ues.aps.process.record.base.model",
                "edu.ues.aps.process.requirement.base.model",
                "edu.ues.aps.process.resolution.base.model",
                "edu.ues.aps.process.tracing.base.model",
                "edu.ues.aps.storage.model");
        sessionFactoryBean.setHibernateProperties(hibernateProperties());
        return sessionFactoryBean;
    }

    @Bean(name = "dataSource")
    public DataSource dataSource() {
        DriverManagerDataSource dataSource = new DriverManagerDataSource();
        dataSource.setDriverClassName("org.h2.Driver");
        dataSource.setUrl("jdbc:h2:mem:test;MODE=MYSQL;" +
                "DB_CLOSE_DELAY=-1;" +
                "DB_CLOSE_ON_EXIT=FALSE;" +
                "REFERENTIAL_INTEGRITY=FALSE");
        dataSource.setUsername("sa");
        dataSource.setPassword("");
        return dataSource;
    }

    private Properties hibernateProperties(){
        Properties properties = new Properties();
        properties.put(AvailableSettings.DIALECT,"org.hibernate.dialect.H2Dialect");
        properties.put(AvailableSettings.HBM2DDL_AUTO,"create-drop");
        properties.put(AvailableSettings.SHOW_SQL,true);
        return properties;
    }
}
